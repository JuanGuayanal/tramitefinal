<link href="/styles/intranet.css" rel="stylesheet" type="text/css" />
<?
$titulo_pag="Encuestas";
$img_titulo_mod="encuestas_tit.gif";
include_once("auth.php");
begin_session();
entry_rigth();
include_once('modulos.php');
include_once('modulosadmin/claseModulos.inc.php');
$modulos = new Modulos;
//require('header.php');
//include('armador.php');
include_once('modulosadmin/claseEncuestaPopud.inc.php');

$modEnc = new Encuesta;

switch($_POST['accion']){
	case $modEnc->arr_accion[EJECUTA_VOTACION]:
		$modEnc->PollCollector($_POST['idPoll'], $_POST['idOption']);
		$modEnc->PollResults($_POST['idPoll']);
		break;
	case $modEnc->arr_accion[MUESTRA_RESULTADOS]:
	    $modEnc->PollResults($_POST['idPoll']);
		break;
	default:
		$modEnc->PollList();
		break;
}

//include('footer.php');
?>
