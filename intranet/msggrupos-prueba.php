<?
include_once('modulosadmin/claseIntranet.inc.php');
//$pool_function = "ono_semanal();";

$objIntranet = new Intranet();

$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

$objIntranet->Header('Mensajes Grupales',false,array('suspEmb'));
$objIntranet->Body('msggrupos_tit.gif');

if($objIntranet->director()){

	include('modulosadmin/claseMensajesPrueba.inc.php');
	$modMsg = new Mensajes();

	if($modMsg){
		switch ($accion){
			case $modMsg->arr_accion[FRM_ENVIA_MSG_GRUPAL]:
				$modMsg->FormEnviaMsgGrupal();
				break;
			case $modMsg->arr_accion[ENVIA_MSG_GRUPAL]:
				$modMsg->EnviaMsgGrupal($_POST['desTitulo'],$_POST['idGrupo'],$_POST['desMensaje'],$_FILES['adjunto']);
				break;
			default:
				$modMsg->FormEnviaMsgGrupal();
				break;
		}
	}
}
else {
echo "No tiene permisos para enviar mensajes grupales";
echo "<br><br><br><br><br><br><br><br><br>";
}

$objIntranet->Footer();
?>
