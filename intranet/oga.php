<html>
<head>
<title>Directorio Telef�nico CONVENIO_SITRADOC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<?=$abs_path;?>/styles/intranet.css" type="text/css">
<link href="../../../styles/intranet.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0">
<table width="555" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="textored" align="right"> <div align="left"><strong>DIRECTORIO TELEF&Oacute;NICO</strong></div></td>
    <td class="tit-documento" align="right"><a href='javascript:window.close()'>CERRAR</a></td>
  </tr>
  <tr> 
    <td colspan="2" align="right" class="textored"> <div align="left"><strong>OFICINA 
        GENERAL DE ADMINISTRACI&Oacute;N</strong></div></td>
  </tr>
  <tr> 
    <td colspan="2" class="tree"> <table width="600" class="tabla-encuestas">
        <tr class="con-tabla3" > 
          <td width="200" valign="middle"> <div align="center"><strong>DEPENDENCIA</strong></div></td>
          <td width="200" > <div align="center"><strong>NOMBRE</strong></div></td>
          <td width="100"> <div align="center"><strong>CARGO</strong></div></td>
          <td width="80"> <div align="center"><strong>TELEFONOS VIRTUALES</strong></div></td>
          <td width="60"> <div align="center"><strong>ANEXO NUEVO</strong></div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=2 valign="top" class="con-tablita" ><strong>OFICINA 
            GENERAL DE ADMINISTRACION</strong></td>
          <td width="200" valign="top" class="con-tablita" >C.P.C. CROMWEL ARTEMIO 
            ALVA INFANTE</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Director 
              General</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">252</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Gabriela Seperak</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">616-2221</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">221<br>
              253 </div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Direcci&oacute;n 
            Ejecutiva</strong></td>
          <td width="200" valign="top" class="con-tablita" >DR. DIEGO TOLMOS</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Director</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">261</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Ejecuci&oacute;n 
            Coactiva </strong></td>
          <td width="200" valign="top" class="con-tablita" >DR. PERCY CARLOS SALAS 
            FERRO</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Director<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">260<br>
              264 </div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=2 valign="top" class="con-tablita" ><strong>Log&iacute;stica</strong></td>
          <td width="200" valign="top" class="con-tablita" >ING. SIBONEY MU�OZ 
            TOIA</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Directora</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita" > <div align="center">265</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Doris Tello</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">616-2223</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">223<br>
              258 </div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - CITES</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. JORGE PELAEZ</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">230</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - Compras</strong></td>
          <td width="200" valign="top" class="con-tablita" >SRTA. ROSARIO QUESADA 
            UTRILLA</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">259</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - Patrimonio</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. JUAN JOSE GARCIA</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">231</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - Servicios</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. EDWIN REVILLA 
            GARCIA</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">232</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - Transportes</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. PACHERRE</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Encargado</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">129</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Log&iacute;stica 
            - Almacen</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. LUIS RAMIREZ LLONTOP</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">131</div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=2 valign="top" class="con-tablita" ><strong>Programaci&oacute;n 
            y Presupuesto P&uacute;blico</strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. ROBERTO ZEGARRA 
            MENDEZ</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Director</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">254</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Flor Mu�oz</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">616-2226</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">226</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong> O.P.P.P. 
            - Programaci&oacute;n</strong></td>
          <td width="200" valign="top" class="con-tablita" >Econ. Rael Rojas Palac�n</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">262</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong> O.P.P.P. 
            - E. Financiera</strong></td>
          <td width="200" valign="top" class="con-tablita" >Decir� Quiroz</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Administrativo</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">263</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>O.P.P.P. - 
            Caja</strong></td>
          <td width="200" valign="top" class="con-tablita" >Iris Polleri</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Cajera</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">103</div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=4 valign="top" class="con-tablita" ><strong>Recursos 
            Humanos </strong></td>
          <td width="200" valign="top" class="con-tablita" >SR. HERNAN VELASQUEZ 
            SOLORZANO</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Director</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">256</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Maritza Fern�ndez</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">257<br>
              272 </div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Javier Urrunaga</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"> <div align="center">266</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Marialena Reyes Durand</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Asistente 
              Social</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">233</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong> OR.R.H.H. 
            - Constancias</strong></td>
          <td width="200" valign="top" class="con-tablita" >Ana Tejada</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Profesional</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">160</div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=2 valign="top" class="con-tablita" ><strong>Tr&aacute;mite 
            Documentario</strong></td>
          <td width="200" valign="top" class="con-tablita" >SRA. GINA PINEDO DELGADO</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Directora</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">616-2224</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">224</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Alejandrina Huarcaya</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">111<br>
              105 </div></td>
        </tr>
        <tr > 
          <td width="200" rowspan=3 valign="top" class="con-tablita" ><strong>Financiera</strong></td>
          <td width="200" valign="top" class="con-tablita" >CPC. LUCY CASTRO RIOS</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Directora</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">268</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Cristina Escobedo</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Secretaria<br>
              FAX </div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">269<br>
              270 </div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" >Andrea Rumiche</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">T�cnico</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"> <div align="center">287</div></td>
        </tr>
        <tr> 
          <td width="200" rowspan=2 valign="top" class="con-tablita" ><strong>Central 
            Telef&oacute;nica </strong></td>
          <td width="200" rowspan=2 valign="top" class="con-tablita" >PILAR HERNANDEZ 
            LOYOLA</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Operadora</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">616-2222</div></td>
          <td width="60" valign="top" class="con-tablita" > <div align="center">222</div></td>
        </tr>
        <tr> 
          <td width="100" valign="top" class="con-tablita" > <div align="center">Operadora</div></td>
          <td width="80" valign="top" class="con-tablita"> <div align="center">616-2229</div></td>
          <td width="60" valign="top" class="con-tablita" > <div align="center">299</div></td>
        </tr>
        <tr > 
          <td width="200" valign="top" class="con-tablita" ><strong>Seguridad</strong></td>
          <td width="200" valign="top" class="con-tablita" >Agente Seguridad</td>
          <td width="100" valign="top" class="con-tablita" > <div align="center">Agente</div></td>
          <td width="80" valign="top" class="con-tablita" > <div align="center">-</div></td>
          <td width="60" valign="top" class="con-tablita"  x:num> <div align="center">271</div></td>
        </tr>
      </table></td>
  </tr>
</table>

</body>
</html>