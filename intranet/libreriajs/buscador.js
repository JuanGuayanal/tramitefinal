
/* Buscador Texto por Default */

function TextoBuscador(id_input, valor) {
	if($.trim($(id_input).val()) == "") {
		$(id_input).val(valor);
	}

	$(id_input).focus(function() {
		if($(id_input).val() == valor) {
			$(id_input).val("");
		}
	});

	$(id_input).blur(function() {
		if($.trim($(id_input).val()) == "") {
			$(id_input).val(valor);
		}
	});
}