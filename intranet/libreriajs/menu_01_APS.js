var estilo_Nivel0=new Array("MEN_nivel_0_OUT","MEN_nivel_0_OVER","MEN_nivel_0_ACTIVO","MEN_nivel_0_OUT2","MEN_nivel_0_OVER2");
var estilo_Nivel1=new Array("MEN_nivel_1_OUT","MEN_nivel_1_OVER","MEN_nivel_1_ACTIVO","MEN_nivel_1_OUT2","MEN_nivel_1_OVER2");
var estilo_Nivel2=new Array("MEN_nivel_2_OUT","MEN_nivel_2_OVER","MEN_nivel_2_ACTIVO","MEN_nivel_2_OUT2","MEN_nivel_2_OVER2");
var estilo_Nivel3=new Array("MEN_nivel_3_OUT","MEN_nivel_3_OVER","MEN_nivel_3_ACTIVO","MEN_nivel_3_OUT2","MEN_nivel_3_OVER2");
var estilo_Nivel4=new Array("MEN_nivel_4_OUT","MEN_nivel_4_OVER","MEN_nivel_4_ACTIVO","MEN_nivel_4_OUT2","MEN_nivel_4_OVER2");
var estilo_Nivel5=new Array("MEN_nivel_5_OUT","MEN_nivel_5_OVER","MEN_nivel_5_ACTIVO","MEN_nivel_5_OUT2","MEN_nivel_5_OVER2");
var estilo_Nivel6=new Array("MEN_nivel_6_OUT","MEN_nivel_6_OVER","MEN_nivel_6_ACTIVO","MEN_nivel_6_OUT2","MEN_nivel_6_OVER2");
var estilo_Nivel7=new Array("MEN_nivel_7_OUT","MEN_nivel_7_OVER","MEN_nivel_7_ACTIVO","MEN_nivel_7_OUT2","MEN_nivel_7_OVER2");

var gruposEstilo=new Array(estilo_Nivel0,estilo_Nivel1,estilo_Nivel2,estilo_Nivel3,estilo_Nivel4,estilo_Nivel5,estilo_Nivel6,estilo_Nivel7);


function getNextSibling(startBrother){
  endBrother=startBrother.nextSibling;
  while(endBrother.nodeType!=1){
    endBrother = endBrother.nextSibling;
  }
  return endBrother;
}

function cambiarDisplay(div_seleccionado,div_desplegar) 
{
	if (!document.getElementById) return false;
	var id = div_seleccionado.getAttribute('id');
	var nivel = div_seleccionado.className.substring(11,10)
	var fila = document.getElementById(div_desplegar);
	var filaActiva = document.getElementById(id);
	var hermano = getNextSibling(filaActiva.firstChild);

    if(fila!=null){ 
        if (fila.style.display != "none") {
            fila.style.display = "none"; //ocultar div
            
			Eventos(div_seleccionado,0);
			hermano.className='MEN_desplegar_'+nivel+'_OFF';
			
            filaActiva.onmouseout=function(){
				Eventos(div_seleccionado,0);
            }
			
            filaActiva.onmouseover=function(){
				Eventos(div_seleccionado,1);
				
            }
        } else {
            fila.style.display = ""; //mostrar div
			Eventos(div_seleccionado,2);
			hermano.className='MEN_desplegar_'+nivel+'_ON';

			filaActiva.onmouseout=function(){
				Eventos(div_seleccionado,2)
            }
			
            filaActiva.onmouseover=function(){
				Eventos(div_seleccionado,2)
            }
        }
    }
	else {
		Eventos(div_seleccionado,1)
       // div.className=gruposEstilo[nivel_estilo][4];
        filaActiva.onmouseout=function(){
			Eventos(div_seleccionado,1)
            //this.className=gruposEstilo[nivel_estilo][4];
        }
        filaActiva.onmouseover=function(){
			Eventos(div_seleccionado,1)
            //this.className=gruposEstilo[nivel_estilo][4];
        } 
    }      
}


function Eventos(div_seleccionado,estado){

	var nivel = div_seleccionado.className.substring(11,10)
	var vineta_estado = gruposEstilo[nivel][estado].substring(12);
	//alert("nivel --> "+nivel+"  Estado --> "+estado+"  Vi�eta Estado --> "+vineta_estado)
	if(vineta_estado=="OVER2"){vineta_estado="OVER"};
	if(vineta_estado=="OUT2"){vineta_estado="OUT"};
    div_seleccionado.className=gruposEstilo[nivel][estado];
	div_seleccionado.firstChild.className='MEN_vineta_'+nivel+'_'+vineta_estado;
}

function EnlaceMenuPricipal(url,target)
{
    target=target.toLowerCase();
    if(target=='_blank')window.open(url,'newWindow','menubar=0,toolbar=0,scrollbars=yes,resizable=1,status=0,top=283,left=400,width=356,height=412');
    if(target=='_self')self.location.href=url;
    if(target=='_top')top.location.href=url;
    if(target=='_parent')parent.location.href=url;
}

function ExpandTreeMagic(variable) 
{
    try 
    {
        var regexS = "[\\?&]"+variable+"=([^&#]*)";
        var valor="";
        var regex = new RegExp( regexS );
        var tmpURL = window.location.href;
        var results = regex.exec( tmpURL );
        if( results == null )
            valor="";
        else
            valor="jsJerarquia"+results[1];
            
        var classnamejer;
        if (document.getElementById) 
        {
            e=document.getElementById(valor);
            if(e!=null)
            {		   
                classnamejer=e.className;
                //alert(classnamejer);
                if(classnamejer=='MEN_nivel_0_OUT'||classnamejer=='MEN_nivel_0_OUT2')
                {
                    //alert(e.className);  
                    e.className="MEN_nivel_0_OVER2";
                    cambiarDisplay(e,'WEB_N1_'+e.getAttribute('id').substring(11),0);
                }
                if(classnamejer=='MEN_nivel_1_OUT'||classnamejer=='MEN_nivel_1_OUT2')
                {
                    var id_nivel0=e.parentNode.getAttribute('id').substring(7);
                    var div=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div!=null)
                    {
                        cambiarDisplay(div,'WEB_N1_'+id_nivel0,0);
                        cambiarDisplay(e,'WEB_N2_'+e.getAttribute('id').substring(11),1);
                    }
                }
                if(classnamejer=='MEN_nivel_2_OUT'||classnamejer=='MEN_nivel_2_OUT2')
                {
                    var id_nivel0=e.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            cambiarDisplay(e,'WEB_N3_'+e.getAttribute('id').substring(11),2);
                        }
                    }
                }
                if(classnamejer=='MEN_nivel_3_OUT'||classnamejer=='MEN_nivel_3_OUT2')
                {
                    var id_nivel0=e.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            var id_nivel2=e.parentNode.getAttribute('id').substring(7);
                            var div_nivel2=document.getElementById('jsJerarquia'+id_nivel2);
                            if(div_nivel2!=null)
                            {
                                cambiarDisplay(div_nivel2,'WEB_N3_'+id_nivel2,2);                                        
                                cambiarDisplay(e,'WEB_N4_'+e.getAttribute('id').substring(11),3);                                        
                            }
                        }
                    }
                }
                if(classnamejer=='MEN_nivel_4_OUT'||classnamejer=='MEN_nivel_4_OUT2')
                {
                    var id_nivel0=e.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            var id_nivel2=e.parentNode.parentNode.getAttribute('id').substring(7);
                            var div_nivel2=document.getElementById('jsJerarquia'+id_nivel2);
                            if(div_nivel2!=null)
                            {
                                cambiarDisplay(div_nivel2,'WEB_N3_'+id_nivel2,2);                                        
                                var id_nivel3=e.parentNode.getAttribute('id').substring(7);
                                var div_nivel3=document.getElementById('jsJerarquia'+id_nivel3);
                                if(div_nivel3!=null)
                                {
                                    cambiarDisplay(div_nivel3,'WEB_N4_'+id_nivel3,3);                                        
                                    cambiarDisplay(e,'WEB_N5_'+e.getAttribute('id').substring(11),4);
                                }                                        
                            }
                        }
                    }
                }
                if(classnamejer=='MEN_nivel_5_OUT'||classnamejer=='MEN_nivel_5_OUT2')
                {
                    var id_nivel0=e.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            var id_nivel2=e.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                            var div_nivel2=document.getElementById('jsJerarquia'+id_nivel2);
                            if(div_nivel2!=null)
                            {
                                cambiarDisplay(div_nivel2,'WEB_N3_'+id_nivel2,2);                                        
                                var id_nivel3=e.parentNode.parentNode.getAttribute('id').substring(7);
                                var div_nivel3=document.getElementById('jsJerarquia'+id_nivel3);
                                if(div_nivel3!=null)
                                {
                                    cambiarDisplay(div_nivel3,'WEB_N4_'+id_nivel3,3);                                        
                                    var id_nivel4=e.parentNode.getAttribute('id').substring(7);
                                    var div_nivel4=document.getElementById('jsJerarquia'+id_nivel4);
                                    if(div_nivel4!=null)
                                    {
                                        cambiarDisplay(div_nivel4,'WEB_N5_'+id_nivel4,4);                                        
                                        cambiarDisplay(e,'WEB_N6_'+e.getAttribute('id').substring(11),5);
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                if(classnamejer=='MEN_nivel_6_OUT'||classnamejer=='MEN_nivel_6_OUT2')
                {
                    var id_nivel0=e.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            var id_nivel2=e.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                            var div_nivel2=document.getElementById('jsJerarquia'+id_nivel2);
                            if(div_nivel2!=null)
                            {
                                cambiarDisplay(div_nivel2,'WEB_N3_'+id_nivel2,2);                                        
                                var id_nivel3=e.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                                var div_nivel3=document.getElementById('jsJerarquia'+id_nivel3);
                                if(div_nivel3!=null)
                                {
                                    cambiarDisplay(div_nivel3,'WEB_N4_'+id_nivel3,3);                                        
                                    var id_nivel4=e.parentNode.parentNode.getAttribute('id').substring(7);
                                    var div_nivel4=document.getElementById('jsJerarquia'+id_nivel4);
                                    if(div_nivel4!=null)
                                    {
                                        cambiarDisplay(div_nivel4,'WEB_N5_'+id_nivel4,4);                                        
                                        var id_nivel5=e.parentNode.getAttribute('id').substring(7);
                                        var div_nivel5=document.getElementById('jsJerarquia'+id_nivel5);
                                        if(div_nivel5!=null)
                                        {
                                            cambiarDisplay(div_nivel5,'WEB_N6_'+id_nivel5,5);                                        
                                            cambiarDisplay(e,'WEB_N7_'+e.getAttribute('id').substring(11),6);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(classnamejer=='MEN_nivel_7_OUT'||classnamejer=='MEN_nivel_7_OUT2')
                {
//		                    alert(e.getAttribute('id'));
                    var id_nivel0=e.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                    var div_nivel0=document.getElementById('jsJerarquia'+id_nivel0);
                    if(div_nivel0!=null)
                    {
                        cambiarDisplay(div_nivel0,'WEB_N1_'+id_nivel0,0);
                        var id_nivel1=e.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                        var div_nivel1=document.getElementById('jsJerarquia'+id_nivel1);
                        if(div_nivel1!=null)
                        {
                            cambiarDisplay(div_nivel1,'WEB_N2_'+id_nivel1,1);
                            var id_nivel2=e.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                            var div_nivel2=document.getElementById('jsJerarquia'+id_nivel2);
                            if(div_nivel2!=null)
                            {
                                cambiarDisplay(div_nivel2,'WEB_N3_'+id_nivel2,2);                                        
                                var id_nivel3=e.parentNode.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                                var div_nivel3=document.getElementById('jsJerarquia'+id_nivel3);
                                if(div_nivel3!=null)
                                {
                                    cambiarDisplay(div_nivel3,'WEB_N4_'+id_nivel3,3);                                        
                                    var id_nivel4=e.parentNode.parentNode.parentNode.getAttribute('id').substring(7);
                                    var div_nivel4=document.getElementById('jsJerarquia'+id_nivel4);
                                    if(div_nivel4!=null)
                                    {
                                        cambiarDisplay(div_nivel4,'WEB_N5_'+id_nivel4,4);                                        
                                        var id_nivel5=e.parentNode.parentNode.getAttribute('id').substring(7);
                                        var div_nivel5=document.getElementById('jsJerarquia'+id_nivel5);
                                        if(div_nivel5!=null)
                                        {
                                            cambiarDisplay(div_nivel5,'WEB_N6_'+id_nivel5,5);                                        
                                            var id_nivel6=e.parentNode.getAttribute('id').substring(7);
                                            var div_nivel6=document.getElementById('jsJerarquia'+id_nivel6);
                                            if(div_nivel6!=null)
                                            {
                                                cambiarDisplay(div_nivel6,'WEB_N7_'+id_nivel6,6);                                        
                                                cambiarDisplay(e,'WEB_N8_'+e.getAttribute('id').substring(11),7);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //.....
            }        		   
        } 
    } 
    catch(er) 
    {
        //alert(er.message);
    }
}