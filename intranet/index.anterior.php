<?
require('auth.php');
include('modulosadmin/claseAutenticacionUsuarioExterno.inc.php');

$autenticacion=new AutenticacionUsuarioExterno;

if ($autenticacion->checkIP()){
	session_name("cod_val");
	session_start();
	$_POST['code']=$_SESSION["code_sha"];
	session_destroy();
	session_unset();
	unset($_SESSION);
}

begin_session();

if(status_secure()){
	header("Location: $INDEX_PAGE");
	exit;
}else{
	if($_POST){
		$username = $_POST['uname'];
		$userpass = $_POST['upass'];
	
		if ($autenticacion->checkIP()){			
			if($autenticacion->validaIntento()){
				if(autentifica()){
					// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
					$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
					header("Location: $destination");
					exit;
				}
			}
		}else{
			if(autentifica()){
				// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
				$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
				header("Location: $destination");
				exit;
			}
		}	
	}
}
session_name("cod_val");
session_start();
?>
<html>
<head>
<title>Intranet CONVENIO_SITRADOC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<script language="JavaScript" type="text/javascript">
<!--

function setFocus()
{
    document.frmLogin.uname.focus();
}

function submit_login()
{
    if (document.frmLogin.uname.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.uname.focus();
        return false;
    } else if (document.frmLogin.upass.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.upass.focus();
        return false;
    } else if (document.frmLogin.codigo.value == "") {
        alert('Ingrese su C�digo de Acceso');
        document.frmLogin.codigo.focus();
        return false;
	} else if (document.frmLogin.answer.value == "") {
        alert('Ingrese su Datos de Autenticaci�n');
        document.frmLogin.answer.focus();
        return false;
	} else if (document.frmLogin.cod_val.value == "") {
        alert('Ingrese el C�digo Autenticaci�n de la Imagen');
        document.frmLogin.cod_val.focus();
        return false;
    } else {
        return true;
    }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="setFocus()">
<table width="779" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="779" bgcolor="#032D61" class="textowhite" height="30"><img src="img/800x600/nportada/header.portada.gif" width="780" height="30" border="0" usemap="#Map"></td>
  </tr>
  <tr>
    <td height="86" background="/img/800x600/nportada/img_cabecera.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"> 
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="280" height="180" colspan="3" align="right" valign="bottom" class="bdr-up"><img src="img/800x600/nportada/tit.intranet.izq.gif" width="277" height="298"><br></td>
          <td width="250" align="center" valign="top" background="img/800x600/nportada/fondo.CONVENIO_SITRADOC.gif"> <table width="500" border="0" cellspacing="0" cellpadding="4">
              <form action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="frmLogin">
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="3"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="3"></td>
                </tr>
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td colspan="3"></td>
                </tr>
				<tr> 
                  <td width="10">&nbsp;</td>
                  <td colspan="3"><img src="img/800x600/nportada/tit.autentifica.gif" width="355" height="29"></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="td-lb-login"><img src="img/800x600/nportada/tit.usuario.gif" width="97" height="25"></td>
                  <td> <input type="text" name="uname" class="ip-login" value="<?echo $_POST['uname'];?>"> 
                  </td>
                </tr>
                <?php if ($autenticacion->checkIP()){ ?>
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td>&nbsp; <script >
					function cerrar_frame(){
					popCal.style.visibility = "hidden";//se oculta el iframe dentro de la capa popcal
					}
					</script> <div id="popCal" style="left: 440px; top: 250px;  
					VISIBILITY: hidden;  WIDTH: 10px;  
					POSITION: absolute" onClickCleaned="event.cancelBubble=true"
					onMouseOut="cerrar_frame();"
					> 
                      <iframe src="/teclado_virtual.html" name="popFrame" width="386" height="180" scrolling="No" frameborder="0" id="popFrame"> 
                      </iframe>
                    </div>
                    <script event="onclick()" for="document" type="text/javascript">
					popCal.style.visibility = "hidden";//se oculta el iframe dentro de la capa popcal
					</script> </td>
                  <td class="td-lb-login"><img src="img/800x600/nportada/tit.clave.gif" width="97" height="25"></td>
                  <td> <input name="upass" type="password" class="ip-login" size="10" maxlength="50" readonly> 
                    <input name="imageField" type="image" src="/img/keyboard.gif" border="0" 
				  onClick="popFrame.fPopCalendar(popCal,upass);return false" title="Presione para visualizar el teclado "> 
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="td-lb-login"><b>C&oacute;digo de Autenticaci&oacute;n:</b></td>
                  <td><input name="codigo" type="password" class="ip-login" id="codigo" value="<?echo $_POST['codigo'];?>"></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <?php $autenticacion->muestraRegistroHTML(); ?>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="td-lb-login"><b>C&oacute;digo de Validaci&oacute;n:</b></td>
                  <td><input name="code_val" type="text" class="ip-login" value="" size="5" maxlength="5"> 
                    <img src="CaptchaImage.php" title="Codigo de comprobacion" > 
                    <input name="code" type="hidden" id="code" value="0"> </td>
                </tr>
                <?php } else {?>
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="td-lb-login"><img src="img/800x600/nportada/tit.clave.gif" width="97" height="25"></td>
                  <td> <input name="upass" type="password" class="ip-login" maxlength="50"> 
                  </td>
                </tr>
                <?php } ?>
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="td-lb-login"><input name="numero_intento" type="hidden" id="numero_intento" value="<?php echo $_REQUEST['numero_intento'];?>"></td>
                  <td> <table width="150" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="100%">&nbsp;</td>
                        <td> <input name="bSubmit" type="submit" value="Ingresar" class="submit" onClick="return submit_login();" /> 
                        </td>
                      </tr>
                    </table></td>
                </tr>
                <?php if ($autenticacion->checkIP()){ ?>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="textored">Para ingresar la contrase�a, 
                    deber� presionar el �cono de Teclado para usar el Teclado 
                    Virtual</td>
                </tr>
                <?php } ?>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="textored">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="textored"><?php echo $_REQUEST['mensaje_error']; ?>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td colspan="2" class="textored">&nbsp;</td>
                </tr>
              </form>
            </table></td>
        </tr>
		<tr bgcolor="#032D61" height="30">
          <td align="justify" class="tree">&nbsp;</td>
          <td align="justify" class="tree">&nbsp;</td>
          <td align="justify" class="tree">&nbsp;</td>
          <td align="center" valign="top">&nbsp;</td>
        </tr>
        <tr height="30">
          <td colspan="4" align="justify" class="tree"><img src="img/800x600/nportada/footer.portada.gif" width="780" height="31"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="481,2,520,28" href="http://www.CONVENIO_SITRADOC.gob.pe/" target="_blank" alt="Ir al Portal del Ministerio de la Producc��n">
  <area shape="rect" coords="524,2,594,28" href="javascript: window.open('https://correoweb.CONVENIO_SITRADOC.gob.pe/','newWindow','menubar=yes,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=yes,top=0,left=0,width=900,height=600'); void('');" alt="Ir a CorreoWeb del CONVENIO_SITRADOC">
  <area shape="rect" coords="598,2,767,29" href="mailto:intranet@CONVENIO_SITRADOC.gob.pe" alt="Env�a aqu� tus comentarios y/o sugerencias para mejorar nuestro servicio">
</map>
</body>
</html>
<?
session_destroy(); 
?>
