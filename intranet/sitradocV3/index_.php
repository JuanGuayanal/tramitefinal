<?php
session_name('intProd');
session_start();
//CARGO POR DEFECTO CIERTAS FUNCIONES QUE ME AYUDAN EN DETERMINADOS PROCESOS.
if (strtoupper(substr(PHP_OS, 0, 3)) === 'LIN') {
    require $_SERVER['DOCUMENT_ROOT'].'/sitradocV3/funciones/funciones.php';
} else {
    require $_SERVER['DOCUMENT_ROOT'].'\sitradocV3\funciones\funciones.php';
}

//OBTENGO EL MODULO PARA LUEGO CARGAR LIBRERIAS RESPECTO AL MODULO DESIGNADO Y PORTERIORMENTE CREO UN OBEJTO EN BASE AL MODULO
if($_GET){
	$modulo=explode('_',$_GET['accion']);//PARA OBTENER EL MODULO
	$load_modulo=$modulo[0];
	require $_SERVER['DOCUMENT_ROOT'].'/sitradocV3/controller/'.$load_modulo.'.php';
	switch ($modulo[0]){
		case 'correspondencia':$correspondencia=new Correspondencia();
		break;
		case 'acceso':$acceso=new Acceso();
		break;
		case 'directorio':$directorio=new Directorio();
		break;
		case 'utilitarios':$utilitarios=new Utilitarios();
		break;
	}
	
}
//DEVUELVE EL RESULTADO DE UN METODO DE OBEJTO DEL MODELO CREADO Y ENVIA A LA VISTA PARA SER PROCESADOS. 
if($_GET){
	switch ($_GET['accion']){
		case 'correspondencia_busqueda_lista_personal';
			echo $correspondencia->busca_destinatario();
		break;
		case 'correspondencia_domicilio_personal';
			$_POST['id']	=(isset($_POST['id']))				?$_POST['id']:0;
			echo $correspondencia->lista_personal_domicilios($_POST['id']);
		break;
		case 'correspondencia_formas_de_envio';
			echo $correspondencia->formas_de_envio();
		break;
		case 'correspondencia_ubigeo';
			$_POST['cod_depa']	=(isset($_POST['cod_depa']))	?$_POST['cod_depa']:0;
			$_POST['cod_prov']	=(isset($_POST['cod_prov']))	?$_POST['cod_prov']:0;
			echo $correspondencia->ubigeo($_GET['ubigeo'],$_POST['cod_depa'],$_POST['cod_prov']);
		break;
		case 'correspondencia_combo_clase_documentos';
			echo $correspondencia->combo_clase_documentos($_GET['accion']);
		break;
		case 'correspondencia_combo_usuario_clase_documentos';
			$_POST['coddep']	=(isset($_POST['coddep']))		?$_POST['coddep']:0;
			echo $correspondencia->combo_clase_documentos($_GET['accion'],$_POST['coddep']);
		break;
		case 'correspondencia_combo_anios';
			echo $correspondencia->combo_anios();
		break;
		case 'correspondencia_combo_dependencias';
			$miDepencencia=(isset($_POST['midep']))?$_POST['midep']:0;
			echo $correspondencia->combo_dependencias($miDepencencia);
		break;
		case 'correspondencia_multiple_busca_documento':
			$_POST['coddep']	=(isset($_POST['coddep']))		?$_POST['coddep']:0;
			$_POST['tipo']		=(isset($_POST['tipo']))		?$_POST['tipo']:0;
			$_POST['clasedoc']	=(isset($_POST['clasedoc']))	?$_POST['clasedoc']:0;
			$_POST['numdoc']	=(isset($_POST['numdoc']))		?$_POST['numdoc']:0;
			$_POST['anio']		=(isset($_POST['anio']))		?$_POST['anio']:0;
			$_POST['siglas']	=(isset($_POST['siglas']))		?$_POST['siglas']:0;
			$_REQUEST['iDisplayStart']	=(isset($_REQUEST['iDisplayStart']))		?$_REQUEST['iDisplayStart']:0;
			$_REQUEST['iDisplayLength']	=(isset($_REQUEST['iDisplayLength']))		?$_REQUEST['iDisplayLength']:10;
			echo $correspondencia->multiple_busca_documento($_POST['coddep'],$_POST['tipo'],$_POST['clasedoc'],$_POST['numdoc'],$_POST['anio'],$_POST['siglas'],'',$_REQUEST['iDisplayStart'],$_REQUEST['iDisplayLength']);
		break;
		case 'directorio_personalxml':
			echo $directorio->listadoPersonalXML();
		break;
		case 'acceso_perfil':
			//echo $acceso->cambio_contrasena('dos','uno','tres');
			require $_SERVER['DOCUMENT_ROOT'].'/sitradocV3/vistas/acceso/perfil.php';
		break;
		case 'utilitarios_destinoDocumento':
			$_POST['idclasedoc']		=(isset($_POST['idclasedoc']))			?$_POST['idclasedoc']:0;
			$_POST['iddependencia']		=(isset($_POST['iddependencia']))		?$_POST['iddependencia']:0;
			$_POST['anio']				=(isset($_POST['anio']))				?$_POST['anio']:'';
			$_POST['numero']			=(isset($_POST['numero']))				?$_POST['numero']:'';
			echo $utilitarios->destinoDocumento($_POST['iddependencia'],$_POST['idclasedoc'],$_POST['numero'],$_POST['anio']);
		break;
		case 'utilitarios_dependencias_por_usuario':
			$_POST['cod_usuario']		=(isset($_SESSION['cod_usuario']))?$_SESSION['cod_usuario']:'';
			echo $utilitarios->dependencias_por_usuario($_POST['cod_usuario']);
		break;
		case 'utilitarios_trabajadoresDependencia':
			$_POST['Destino']		=(isset($_POST['Destino']))?$_POST['Destino']:'32';
			echo $utilitarios->trabajadoresDependencia($_POST['Destino']);
		break;
	}
}
?>