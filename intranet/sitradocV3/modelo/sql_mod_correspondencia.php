<?php
require 'datosGlobales.php';
class Sql_mod_correspondencia{
	protected $server=CJDBMSSQL;
	protected $user=CJDBMSSQLUSER;
	protected $pass=CJDBMSSQLPASS;
	protected $db='DB_TRAMITE_DOCUMENTARIO';
	protected $link;
	protected $consulta;
	
	public function __construct(){
		$this->conectar();
	}
	protected function conectar(){
		$this->link = mssql_connect($this->server,$this->user,$this->pass);
		mssql_select_db($this->db, $this->link);
	}
	public function ejecutar($consulta,$crud=NULL){
		$this->consulta=mssql_query($consulta);
/*		switch (strtolower($crud)){
			case 'i':
				echo "ESTA CONSULTA PARA HACER UN INSERT";
			break;
			case 'u':
				echo "ESTA CONSULTA PARA HACER UN UPDATE";
			break;
			case 'd':
				echo "ESTA CONSULTA PARA HACER UN DELETE";
			break;
			default:*/
				return $this->listado();
/*			break;
		}*/
	}
	public function listado(){
		$resultado=array();
		$resultado['filas_afectadas']=mssql_rows_affected($this->link);
		while ($row = mssql_fetch_assoc ($this->consulta)) {
			$resultado['lista'][]=$row;
		}
		return $resultado;
	}
//------------------------SENTENCIAS SQL---------------------------
	public function lista_personal($cadena){	//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'LISTA_PERSONAL','$cadena'";
		return $this->ejecutar($sql);
	}
	public function lista_personal_domicilios($id){	//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'DOMICILIOS','',$id";
		return $this->ejecutar($sql);
	}
	public function total_personal(){			//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'TOTAL_PERSONAL'";
		return $this->ejecutar($sql);
	}
	public function ubigeo($ubigeo,$cod_depa,$cod_prov){			//SE APLICA PARA EL COMBO FROMAS DE ENVIO DE CORRESPONDENCIA
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_ubigeo '$ubigeo','$cod_depa','$cod_prov'";
		return $this->ejecutar($sql);
	}
	public function formas_de_envio(){			//SE APLICA PARA EL COMBO FROMAS DE ENVIO DE CORRESPONDENCIA
		$sql="EXECUTE SP_CO_FORMA_ENVIO";
		return $this->ejecutar($sql);
	}
	public function combo_clase_documentos($coddep,$tipo,$idclasedoc,$numdoc,$anio,$iddeporigen){			//SE APLICA PARA EL COMBO CLASE DE DOCUMENTO
		$sql="EXECUTE sp_get_documento 'LISTA_CLASE_DOCS',$coddep,$tipo,$idclasedoc,'$numdoc','$anio',$iddeporigen";
		return $this->ejecutar($sql);
	}
	public function combo_anios(){						//SE APLICA PARA EL COMBO AÑOS
		$sql="EXECUTE sp_get_anio";
		return $this->ejecutar($sql);
	}
	public function combo_dependencias($id=0,$execpciones=0){	//SE APLICA PARA EL COMBO DEPENDENCIAS
		$sql="EXECUTE sp_get_dependencias $id,'POR-CODIGO','',$execpciones";
		return $this->ejecutar($sql);
	}
	public function multiple_busca_documento($coddep,$tipo,$idclasedoc,$numdoc,$anio,$iddeporigen,$asunto,$start,$len){
		$sql="EXECUTE sp_get_documento 'BUSCAR',$coddep,$tipo,$idclasedoc,'$numdoc','$anio',$iddeporigen,'$asunto',$start,$len";
		return $this->ejecutar($sql);
	}
	public function m_ambito(){
		$sql="SELECT ID_AMBITO AS ID, AMBITO AS DESCRIPCION FROM CORRESPONDENCIA_AMBITO WHERE FLG_HABILITADO=1";
		return $this->ejecutar($sql);
	}
	public function m_prioridad($id_ambito){
		$sql=sprintf("SELECT ID_PRIORIDAD AS ID, PRIORIDAD AS DESCRIPCION FROM CORRESPONDENCIA_PRIORIDAD WHERE FLG_HABILITADO=1 AND ID_AMBITO=%d",$id_ambito);
		return $this->ejecutar($sql);
	}
	public function m_combo_servpri(){
		$sql="EXECUTE SP_CORRESPONDENCIA_PRIORIDAD 'COMBO_SERVICIOS'";
		return $this->ejecutar($sql);
	}
	public function m_verificar_servpri($id_servicio=0,$id_prioridad=0){
		$sql=sprintf("SELECT COUNT(ID_SERVPRI)AS EXITO FROM CORRESPONDENCIA_SERV_PRI WHERE ID_SERVICIO=%d AND ID_PRIORIDAD=%d",$id_servicio,$id_prioridad);
		return $this->ejecutar($sql);
	}
	public function m_motivo($id_estado){
		$sql=sprintf("SELECT ID, DESCRIPCION FROM motivo WHERE ID_ESTADO_CORRESPONDENCIA=%d AND ACTIVO=1",$id_estado);
		return $this->ejecutar($sql);
	}
//---------------------------------------------------------------------
}
?>