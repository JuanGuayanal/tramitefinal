<?php
if (strtoupper(substr(PHP_OS, 0, 3)) === 'LIN') {
    require $_SERVER['DOCUMENT_ROOT'].'/sitradocV3/modelo/sql_mod_utilitarios.php';
} else {
    require $_SERVER['DOCUMENT_ROOT'].'\sitradocV3\modelo\sql_mod_utilitarios.php';
}

class Utilitarios{
	public $sql;
	public function __construct(){
		$this->sql=new Sql_mod_utilitarios();
	}
	public function destinoDocumento($dep=0,$clase=0,$doc='',$anio=''){
		$lista=$this->sql->destinoDocumento($dep,$clase,$doc,$anio);	
		$destino=array();
		foreach($lista['lista'] as $row){
			$destino[]=array('DEPENDENCIA'=>utf8_encode($row['DEPENDENCIA']));
		}
		return json_encode($destino);
	}
	public function dependencias_por_usuario($usuario){
		$destino=array();
		if($_POST){//SI SE HA ENVIADO LA VARIABLE "FILTRO" POR METODO POST
				$lista=$this->sql->m_dependencias(0,'POR-USUARIO',$usuario);
				$row=array();
				if(is_array($lista['lista'])){//SI SE OBTUVO UN JUEGO DE REGISTROS
					foreach($lista['lista'] as $fila){
						//$row[]=array('<input type="hidden" value="'.$fila['ID'].'">',utf8_encode($fila['DEPENDENCIA']),$fila['DESCRIPCION'],'<input type="checkbox" name="b_destinatario_check" class="b_destinatario_check">');
					$destino[]=array('ID'=>$fila['ID'],'DESCRIPCION'=>$fila['DESCRIPCION'],'DEPENDENCIA'=>utf8_encode($fila['DEPENDENCIA']));
					}
					$output['iTotalDisplayRecords']=$lista['filas_afectadas'];
				}
				$output['aaData']=$row;
		}
		//return json_encode( $output );
		return json_encode($destino);	
	}
	public function trabajadoresDependencia($coddep){
		$lista=$this->sql->m_trabajadoresDependencia($coddep);	
		$trabajadores=array();
		foreach($lista['lista'] as $row){
			$trabajadores[]=array('ID'=>$row['CODIGO_TRABAJADOR'],'DESCRIPCION'=>utf8_encode($row['TRABAJADOR']));
		}
		return json_encode($trabajadores);
	}
}
?>