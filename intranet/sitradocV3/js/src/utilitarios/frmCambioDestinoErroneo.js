$(document).ready(function() {
	var padres=[];
	var indice1=0;
	//OBTENEMSO UN ARREGLO CON LOS DISTINTOS PADRES QUE PUEDAD TENER UN DOCUMENTO
	$('#registros .movimiento').each(function(){
		$(this).children('select').each(function(){
			if(indice1 != $(this).attr('pri')){
				padres.push($(this).attr('pri'));
				indice1=$(this).attr('pri');
			}
		})
	})

	//RECORREMOS LOS SELESCT DEPENDIENDO DEL PADRE AL QUE PERTENECE
		for(i=0;i < padres.length;i++)
		{
			var actualxPadre=[];// SE ALAMCENA LAS DEPENDENCIA DESTINO POR PADRE
			//EJEMPLO ID_DOCUMENTO_PADRE ES 100 Y FUE DERIVADO A DEPENDENCIA(1,2,3)
			$('.movimiento select[pri='+padres[i]+']').each(function()
			{
				actualxPadre.push($(this).val());
			})
			//RECORREMOS TODOS LOS SELECT DE UN PADREA Y ELIMINAMOS LA OPCION DE LOS SELECT HERMANOS.
			$('.movimiento select[pri='+padres[i]+']').each(function()
			{	
				var queda=[];
				queda.push($(this).val());
				var remover=[];
				remover=borrar(actualxPadre,queda);
				for(m=0;m<remover.length;m++)
				{
					$(this).children('option[value='+remover[m]+']').remove();
				}
			})
		}
	

	//ACCIONES DEL BOTON GUARDAR
	$('#btn_guardar').click(function(e){
		$coindicencias=0;
		var i=0;
		for(i=0;i<padres.length;i++)
		{
			var idxPadre=[];
			var cantxPadre=$('.movimiento select[pri='+padres[i]+']').length;
			$('.movimiento select[pri='+padres[i]+']').each(function()
			{
				idxPadre.push($(this).val());
			})
			idxPadre=a_unico(idxPadre);
			if(idxPadre.length < cantxPadre){$coindicencias++;}
		}
		if($coindicencias==0){
			if(confirm("\u00BFEst\u00e1 seguro de guardar los cambios?"))
			{
				$("form[name=frmCambioDestino]").attr('action','/institucional/aplicativos/oad/sitradocV2/index.php?accion=CambiarDestinoErroneo');
			}
			else{ return false;}
		}
		else{
			alert("Verificar destino.");
			return false;
		}
	})
	$('#btn_cancelar').click(function(e){
		e.preventDefault();
		location.href='index.php?accion=frmReasignaDoc&menu=frmReasignaDoc&subMenu=frmReasignaDoc';
	})
	//ESTA FUNCION DEVUELVE UN ARRAY CON ELEMNOS DE UNO RESTADO CON OTRO ARRAY
	function borrar(array1,array2){
		var rpta=[];
		for(r=0;r<array1.length;r++)
		{
			for(j=0;j<array2.length;j++)
			{
				if(array1[r]!=array2[j]){rpta.push(array1[r])};
			}
		}
		return rpta;
	}
	//ESTA FUCNCION DEVUELVE UN ARRAY CON ELEMENTOS UNICOS
	function a_unico(ar){ 
		//Código creado por EspacioWebmasters.com 
		//puedes copiarlo citando la fuente 
		//Declaramos las variables 
		var ya=false,v="",aux=[].concat(ar),r=Array(); 
		//Buscamos en el mismo Array si 
		//cada elemento tiene uno repetido 
		for (var i in aux){ // 
			v=aux[i]; 
			ya=false; 
			for (var a in aux){ 
				//Preguntamos si es el primer elemento 
				//o si ya se recorrió otro igual 
				//Si es el primero se asigna true a la variable "ya"
				//Si no es el primero, se le da valor vacio 
				if (v==aux[a]){ 
					if (ya==false){ 
						ya=true; 
					} 
					else{ 
						aux[a]=""; 
					} 
				} 
			} 
		} 
		//Aquí ya tenemos los valores duplicados 
		//convertidos en valores vacios 
		//Solo falta crear otro Array con los valores 
		//que quedaron sin contar los vacios 
		for (var a in aux){ 
			if (aux[a]!=""){ 
				r.push(aux[a]); 
			} 
		} 
		//Retornamos el Array creado 
		return r; 
	} 
});