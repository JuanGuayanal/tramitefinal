$(document).ready(function(){
	
	var giCount = 1;
	var oTabled_=$('#b_trabajadores').dataTable({
		"bPaginate": false,
		"sScrollY": "307px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
                //"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
				"sEmptyTable":	"Ning&uacute;n trabajador seleccionado.",
   				"sInfo":		"_TOTAL_ Trabajador(es) seleccionados.",
				"sInfoEmpty":	"0 Trabajadores seleccionados."
            },
			"bAutoWidth": true,
			"bJQueryUI": true
		})
  	var oTabled= $('#b_documentos').dataTable( {
		"bPaginate": false,
		"sScrollY": "320px",
		"bFilter": true,
		"bLengthChange": false,
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/institucional/aplicativos/oad/sitradocV2/index.php?accion=ListaReasignar",
		"sServerMethod": "POST",
		"bSort": false,
		"fnServerParams": function ( aoData ) {
      	aoData.push({ "name": "termino","value":$("#termino").val()});
    	},
		"fnPreDrawCallback": function( ) {
			$("#b_documentos_filter").remove();
		},
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            },
	  "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
		//return iStart +" to "+ iEnd;
		$('#trabajadores option').remove();
		oTabled_.fnClearTable();
		if(iTotal>0){
			$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=ListaReasignar&ltrab=1',
				type:'POST',
				data:{termino:$("#termino").val()},
				success: function(data){$('#trabajadores').append(data);}
			})
		}
	  },
	  "bJQueryUI": true
    } );
	$('input[name=requiere_documento]').click(function(e){
		if($('input[name=requiere_documento]:checked').val()==0){
			$("#id_clase_documento").css('display','none');
			$('#id_clase_documento').val(74);
			$("#tr_asunto").css('display','none');
			$("#tr_acciones").css('display','table-row');
		}
		if($('input[name=requiere_documento]:checked').val()==1){
			$("#id_clase_documento").css('display','inline-block');
		}
	})
	$('#id_clase_documento').change(function(){
		if($(this).val()==74){
			$("#tr_asunto").addClass('std_oculto')
			$("#tr_acciones").css('display','table-row');
		}
		else{
			$("#tr_asunto").removeClass('std_oculto')
			$("#tr_asunto").css('display','table-row');
			$("#tr_acciones").css('display','none');
		}
	})
	$("#buscar").click(function(e){//
		$('#indicativo').val(pad($('#indicativo').val(),5))
		$('#termino').val($('#clase').val()+' '+$('#indicativo').val()+'-'+$('#anio').val()+'-MIDIS/PNCM/'+$('#unidad').val());
		oTabled.fnFilter( '')
		e.preventDefault();
	})
	 $('#indicativo').keydown(function(e) {
		if(e.keyCode==13){
			if($('#indicativo').val()!='')
			{
		$('#indicativo').val(pad($('#indicativo').val(),5))
		$('#termino').val($('#clase').val()+' '+$('#indicativo').val()+'-'+$('#anio').val()+'-MIDIS/PNCM/'+$('#unidad').val());

			oTabled.fnFilter('')
			e.preventDefault();
			}
		}
	});
	 $('#buscar').keyup(function(e) {
		if(e.keyCode==13){
			$('#indicativo').val(pad($('#indicativo').val(),5))
			$('#termino').val($('#clase').val()+' '+$('#indicativo').val()+'-'+$('#anio').val()+'-MIDIS/PNCM/'+$('#unidad').val());
			$('input[name=busca]').val(1);
			if(($('input[name=fecini]').val()=='')||($('input[name=fecfin]').val()=='')){
			alert('Debe seleccionar un rango de fechas.')
			}
			else{oTabled.fnFilter( '' );}
		}
	});
	function pad (str, max) {
	  str = str.toString();
	  return str.length < max ? pad("0" + str, max) : str;
	}
	$('#cerrar_mensaje').click(function(e){
		e.preventDefault();
		$("#mensaje").remove();
	})
	$("#b_documentos").delegate("tr td a","click",function(e){//
		e.preventDefault();
			var caracteristicas = "height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0";
      		nueva=window.open($(this).attr('href'), 'Popup', caracteristicas);
      	return false;
	})
	$('#agregar').click(function(e){
		e.preventDefault();
		if($('#trabajadores').val()!=''){
			var contador=0;
			var existe=0;
			$("#b_trabajadores tbody tr").each(function(){
				if($(this).children('td:nth-child(1)').children('input').val()==$('#trabajadores').val()){
					existe++;
				}
			})
			if(existe==0){
				oTabled_.fnAddData( [
						'<input type="hidden" value="'+$('#trabajadores').val()+'" name="id_trabajador[]">'+$('#trabajadores option:selected').html(),
						'<a href="#" class="eliminar_fila"> X </a>' ] );
				giCount++;
			}
		}
	})
	$("body").delegate(".eliminar_fila", "click", function(e) {
		e.preventDefault();
		var row = $(this).closest('tr');
		var nRow = row[0];
		oTabled_.fnDeleteRow(nRow); 
		 
	});

	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	function btn_reasignar(){
		var seleccionados=0;
		var n = $( "input:checked[class=marcar_uno]" ).length;
		//if(($('#b_trabajadores tbody tr').size()>0)&&(n>0)){
		if(($('#b_trabajadores tbody tr').size()>0)){
			if($('#b_trabajadores_info').text()=='0 Trabajadores seleccionados.'){
				$('#btn_reasignar').attr('disabled','disabled');
				$('#btn_reasignar').css('opacity','0.5');
			}
			else{
				$('#btn_reasignar').removeAttr('disabled');
				$('#btn_reasignar').css('opacity','1');
			}
			
		}
		else{
			$('#btn_reasignar').attr('disabled','disabled')
			$('#btn_reasignar').css('opacity','0.5')
		}
	}
	$('#btn_reasignar').click(function(){
		$("#frm_reasignar").attr('action','/institucional/aplicativos/oad/sitradocV2/index.php?accion=reasignaDoc&crud=asignar');
		var mensaje='';
		if($("#b_trabajadores tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'\nNo hay trabajadores en su lista para la asignacion';
		if(mensaje=='')
			if(confirm('\u00BFEst\u00e1 seguro que desea asignar a los trabajadores de la lista?'))
			{
				var id_trabajador=[];
				$("input[name='id_trabajador[]']").each(function(index){
						id_trabajador[index]=$(this).val();
				})
				$.ajax({
				  url: "/institucional/aplicativos/oad/sitradocV2/index.php?accion=reasignaDoc&crud=asignar",
				  type:'post',
				  data: {
					'id_trabajador':id_trabajador
				  },
				  success: function( data ) {
					  var exito=0;
					  exito=data;
						if(exito==1){oTabled.fnDraw();}
						else alert('Se produjo un error al momento de realizar la transacci\u00f3n.');
				  }
				});
			return false;
			}
			else return false;
		else{
			alert(mensaje)
			return false;
		}
	})
	$('#btn_eliminar_asignacion').click(function(){
		$("#frm_reasignar").attr('action','/institucional/aplicativos/oad/sitradocV2/index.php?accion=reasignaDoc&crud=eliminar');
		var mensaje='';
		var contado_lista_eliminar=0;
		$("input[name='id_movtrab[]']").each( 
			function(){if (this.checked){contado_lista_eliminar++;}}
		);
		if(contado_lista_eliminar==0)mensaje=mensaje+'\nNo hay registros marcados para eliminar asignacion';
		if(mensaje=='')
			if(confirm('\u00BFEst\u00e1 seguro que desea asignar a los trabajadores de la lista?'))
			{
				var id_movtrab=[];
				$("input[name='id_movtrab[]']").each(function(index){
					if (this.checked){
						id_movtrab[index]=$(this).val();					
					}
				})
				$.ajax({
				  url: "/institucional/aplicativos/oad/sitradocV2/index.php?accion=reasignaDoc&crud=eliminar",
				  type:'post',
				  data: {
					'id_movtrab':id_movtrab
				  },
				  success: function( data ) {
					  var exito=0;
					  	exito=data;
						if(exito==1){oTabled.fnDraw();}
						else alert('Se produjo un error al momento de realizar la transacci\u00f3n.');
				  }
				});
				return false;
			}
			else return false;
		else{
			alert(mensaje)
			return false;
		}
	})
	$('body').delegate('.marcar_uno','click',function(){
		check_hijo($('#marcar_todos'),$(".marcar_uno"))
		
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_destinatario_seleccionar_todos"
	$('body').delegate('#marcar_todos',"click",function(e){
		check_padre($('#marcar_todos'),$(".marcar_uno"))
	
	})	
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}	
});