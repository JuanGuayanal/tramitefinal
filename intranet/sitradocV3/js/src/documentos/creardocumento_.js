$(document).ready(function(e) {
	var buscador_destinos='<div id="std_mis_destinos"><div class="std_titulo_popup">Busca destinatarios</div><div><div class="left"></div><div class="right"><button id="b_destinatario_agregar" title="Solo se agregaran los registros marcados con check">Agregar a destinatarios</button></div><div class="limpiar"></div></div><br><br><div id="std_div_table"><table id="b_destinatario" style="width:100%;"><thead><tr><th width="1"></th><th>DEPENDENCIA</th><th>SIGLAS.</th><th><input type="checkbox" id="b_destinatario_seleccionar_todos"></th></tr></thead></table></div></div>';
	$("body").prepend('<div id="std_modal"></div><div id="std_popup"><div class="std_cerrar_popup">x</div>'+buscador_destinos+'</div>');
	
	//EVENTOS POPUP
	$("body").delegate("#std_modal, .std_cerrar_popup", "click", function() {
	  $("#std_modal, #std_popup").hide();
	});
	//EVENTOS BOTONES
	$("#std_btn_buscar_destinatario").click(function(e){
		e.preventDefault();
		//oTable.fnAdjustColumnSizing();
		$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
		$("#std_mis_documentos").hide();
		//$("#b_destinatario_seleccionar_todos").removeAttr('checked');
	})
	//OBJETO DATATABLE DESTINOS
  	var oTable= $('#b_destinatario').dataTable( {
		"bAutoWidth": false,
		"sScrollY": "375px",
	  	"bPaginate": false,
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/sitradocV3/index.php?accion=utilitarios_dependencias_por_usuario",
		"sServerMethod": "POST",
		"bSort": false,
		"bRetrieve": true,
		"fnServerParams": function ( aoData ) {
      	aoData.push(
				{ "name": "filtro", "value": $("#b_destinatario_filtro_text").val() } 
				);
    	},
		"fnDrawCallback": function( oSettings ) {
			$("#b_destinatario_filter").remove();
		},
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            }
    } );
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON ID "b_destinatario_seleccionar_todos"
	$('body').delegate('#b_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "b_destinatario_check"
	$("body").delegate(".b_destinatario_check","click",function(){
		check_hijo($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_destinatario_seleccionar_todos"
	$('body').delegate('#t_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	})		
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_destinatario_check"
	$("body").delegate(".t_destinatario_check","click",function(){
		check_hijo($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	});
	//ACCION QUE SE DESENCADENA AL MOMENTO DE DARLE CLICK EN EL BOTON AGREGAR A DESTINATARIOS
	$("#b_destinatario_agregar").click(function(e){
		e.preventDefault();
		var contador=0;
		var contador_nocheck=0;
		$("#b_destinatario tbody tr").each(function(){
			$(this).children("td:nth-child(4)").each(function(){
				if($(this).children('input').is(':checked')){
					contador=agregar($(this).parent().children("td:nth-child(1)").children("input").val(),$(this).parent().children("td:nth-child(2)").text(),$(this).parent().children("td:nth-child(3)").text())+contador;
					contador_nocheck++;
				}				
			});
		})
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
		if(contador_nocheck==0){
			alert('No ha seleccionado ningun destinatario');
		}
		else{
			alert('Se agrego '+contador+' destinatario(s) a su lista.');
		}
		
	});
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR
	$("#t_eliminar_destinatraio").click(function(){
		eliminar()
	});
	
	$("#idClaseDoc").change(function(){
		if($(this).val()==32){
			$("#panel_hojaderivacion").removeClass("std_oculto");
		}
		else{
			$("#panel_hojaderivacion").addClass("std_oculto");
		}
	})
	
	
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_destinatarios) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar(codigo,nombre,cod_dpto){
		var contador=0;
		var existe=0;
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_destinatarios tbody").prepend('<tr codigo="'+codigo+'" domicilio="0"><td><input type="hidden" value="'+codigo+'" name="destinatario_final[]">'+nombre+'</td><td>'+cod_dpto+'</td><td class="border-left"><input type="checkbox" class="t_destinatario_check"></td></tr>');
			contador++;
		} 
		return contador;
	}
	//ELIMINARA LOS REGISTROS DE LA LISTA DONDE SE ENCUENTRAN LOS DESTINATARIOS SELECCIONADOS CHEKEADOS
	function eliminar(){
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).children("td:nth-child(3)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_destinatario_seleccionar_todos,.t_destinatario_check").removeAttr('checked');
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
	}
	
	
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}
});