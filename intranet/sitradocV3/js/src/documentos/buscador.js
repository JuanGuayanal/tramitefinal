$(document).ready(function(e) {
	var modal_finalizar=$("#modal_finalizar").dialog({
			modal: true,
			closeOnEscape: false,
			width: 500,
			height: 300,
			resizable: false,
			draggable:true,
			autoOpen: false,
			close: function( event, ui ) {limpiar_modal();}
		});
	var modal_derivar=$("#modal_derivar").dialog({
			modal: true,
			closeOnEscape: false,
			width: 700,
			height: 600,
			resizable: false,
			draggable:true,
			autoOpen: false,
			close: function( event, ui ) {limpiar_modal();}
		});
	var modal_acciones=$("#modal_deracciones").dialog({
			autoOpen: false,
				modal: true,
				closeOnEscape: false,
				width: 650,
				height: 350,
				resizable: false,
				draggable:true,
				close: function( event, ui ) {
					var input_text=$("[chekado=1]");
					input_text.val('');
					var acciones_marcadas=0;
					$("[name='checkbox[]']").each(function(){
						var accion=$(this);
						if(accion.is(':checked')){
							acciones_marcadas++;
							if(input_text.val()==''){
								input_text.val(accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(accion.val());
							}
							else{
								input_text.val(input_text.val()+','+accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(input_text.parent().children("[name='arr_derlista[acciones][]']").val()+','+accion.val());
							}
						}
					})
					if(acciones_marcadas==0){input_text.parent().children("[name='arr_derlista[acciones][]']").val('');};
					input_text.removeAttr('chekado');
					$("[name='checkbox[]']").removeAttr('checked');
				}
			});
	$("#btn_dercancelar, #btn_fincancelar").click(function(){
		limpiar_modal();
		modal_finalizar.dialog( "close" );
		modal_derivar.dialog( "close" );
	})
	$("#btn_aceptarAcciones").click(function(){
		modal_acciones.dialog("close");
	})
	$("#btn_aceptar").click(function(e){
		e.preventDefault();
		var cont_ids=0;
		var cont_aceptados=0;
		var mensaje='';
		$("input[name='ids[]']").each( 
			function(){
				if (this.checked){
					cont_ids=cont_ids+1;
					if($(this).attr("estado")=='ACEPTADO' || $(this).attr("estado")=='ASIGNADO'){cont_aceptados=cont_aceptados+1;}
				}
			}
		);
		if(cont_ids==0){mensaje='Debe seleccionar al menos un registro.';}
		if(cont_aceptados>0){mensaje='Ya hay un documento aceptado.';}
		if(mensaje==''){
			var mis_ids = '';//ids que se seleccionan
			$("input[name='ids[]']").each( 
				function(){if (this.checked){mis_ids += this.value+'-';}}
			);
			$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=aceptaDocDir',
				type:'POST',
				data:{ids:mis_ids},
				success: function(data){
					if(data=='EXITO'){
						alert('Transacci\u00f3n relizada satisfactoriamente');
					}
					else{
						alert('Se produjo un error al momento de realizar la transacci\u00f3n.');
					}
					oTabled.fnDraw();
				}
			});
		}
		else{alert(mensaje);}
	})
	//accion que abre abre el modal de finalizar
	$("#btn_finalizar").click(function(e){
		e.preventDefault();
		var cont_ids=0;
		var cont_aceptados=0;
		var mensaje='';
		$("input[name='ids[]']").each( 
			function(){
				if (this.checked){
					cont_ids=cont_ids+1;
					if($(this).attr("estado")=='ACEPTADO'){cont_aceptados=cont_aceptados+1;}
				}
			}
		);
		if(cont_ids==0){mensaje='Debe seleccionar al menos un registro.';}
		else{
			if(cont_aceptados==cont_ids){mensaje='';}
			else{mensaje='El documento no a sido aceptado o se encuentra pendiente en el Trabajador.';}
		}
		
		if(mensaje==''){
			modal_finalizar.dialog( "open" );
		}
		else{alert(mensaje);}
	})
	//del modal finalizar, boton finalizar
	$("#btn_finfinalizar").click(function(){
		var mis_ids = '';//ids que se seleccionan
		var mensaje='';
		$("input[name='ids[]']").each(function(){
			if (this.checked){mis_ids += this.value+'-';}
		});
		if(mis_ids=='')mensaje=mensaje+'Debe seleccionar al menos un documento.';
		if($('#txt_finobs').val()=='')mensaje=mensaje+'\nDebe escribir el motivo de archivo.';
		if(mensaje==''){
			if(confirm('Esta seguro que desea finalizar los documentos seleccionados.')){
				$.ajax({
					async:false,
					url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=finalizaDocDir',
					type:'POST',
					data:{ids:mis_ids,obs:$("#txt_finobs").val()},
					success: function(data){
						if(data=='EXITO'){
							alert('Transacci\u00f3n relizada satisfactoriamente');
							modal_finalizar.dialog("close");
							oTabled.fnDraw();
						}
						else alert('Se produjo un error al momento de realizar la transacci\u00f3n.');
					}
				});
			}
		}
		else alert(mensaje);
	})
	$("#btn_delegar").click(function(e){
		e.preventDefault();
		$("#titulo_modal").html('ASIGNAR DOCUMENTO');
		$(".opt_asignar").show();
		$(".opt_derivar").hide();
		$(".opt_anexar").hide();
		$(".opt_reiterar").hide();
		var cont_ids=0;
		var cont_aceptados=0;
		var mensaje='';
		$("input[name='ids[]']").each( 
			function(){
				if (this.checked){
					cont_ids=cont_ids+1;
					if($(this).attr("estado")=='ACEPTADO' || $(this).attr("estado")=='ASIGNADO'){cont_aceptados=cont_aceptados+1;}
				}
			}
		);
		
		if(cont_ids==0){mensaje='Debe seleccionar al menos un registro.';}
		else{
			if(cont_aceptados<cont_ids){mensaje='Debe aceptar el documento.';}
		}
		if(mensaje==''){
			oTabled_list_derdocumentos.fnClearTable();
			modal_derivar.dialog('open');
			$("input[name='ids[]']").each( 
				function(){
					if (this.checked){
						oTabled_list_derdocumentos.fnAddData( [$(this).parent().parent().children('td:eq(0)').html()+'<input type="hidden" value="'+$(this).parent().parent().children('td:last-child').children('input').val()+'">','<a href="#" class="btn_dereliminardoc"> X </a>'] );
					}
				}
			);
		}
		else{alert(mensaje);}
	})
	$('#btn_agregartrab').click(function(e){
		e.preventDefault();
		if($('#sel_trabajador').val()!='none'){
			var contador=0;
			var existe=0;
			$("#list_trabajadores tbody tr").each(function(){
				if($(this).children('td:nth-child(1)').children('input').val()==$('#sel_trabajador').val()){
					existe++;
				}
			})
			if(existe==0){
				oTabled_list_trabajadores.fnAddData( [
					'<input type="hidden" value="'+$('#sel_trabajador').val()+'" name="arr_derlista[idtrabajador][]">'+$('#sel_trabajador option:selected').html(),'<input type="hidden" name="arr_derlista[acciones][]"/><input type="text" class="text_deracciones"/>','<input type="text" name="arr_derlista[obs][]"/>','<a href="#" class="btn_eliminartrab"> X </a>' ] );
			}
		}
	})
	//del modal asignar, este boton elimina un destino de la lista
	$("body").delegate(".btn_eliminartrab","click",function(e){
		e.preventDefault();
		oTabled_list_trabajadores.fnDeleteRow($(this).parent().parent().index());
	})
	//del modal asignar, boton asignar
	$("#btn_asignar").click(function(){
		var mensaje='';
		if($("#list_derdocumentos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un documento para realizar la operaci\u00f3n.\n';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		//if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';
		if($("#list_trabajadores tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un trabajador de destino.\n';
		if(mensaje==''){	
			if(confirm('Esta seguro que desea asignar los documentos seleccionados')){
				var destinos=[];
				var acciones=[];
				var observaciones=[];
				var documentos=[];
				$("#list_trabajadores tbody tr").each(function(index){
						destinos[index]=$(this).children('td:eq(0)').children('input').val();
						acciones[index]=$(this).children('td:eq(1)').children('input[type="hidden"]').val();
						observaciones[index]=$(this).children('td:eq(2)').children('input').val();
				})
				$("#list_derdocumentos tbody tr").each(function(index){
						documentos[index]=$(this).children('td:eq(0)').children('input[type="hidden"]').val();
				})
				var objeto = {destinos:destinos,acciones:acciones,observaciones:observaciones};
				var data = {'idclasedoc':  $("#sel_derclasedoc").val(),
							  'asunto':  $("#txt_derasunto").val(),
							  'objeto':objeto,
							  'documentos':documentos,
							  'obsdoc':$('#txt_derobs').val()};
				$.ajax({
						type:           'post',
						async:			false,
						cache:          false,
						url:            '/institucional/aplicativos/oad/sitradocV2/index.php?accion=delegaDocDir',
						data:           data,
						success: function(data){
							location.reload(true);
							}
					   });
			}
		}
		else alert(mensaje);
	})
	//REITERAR
	$("#btn_reiterar").click(function(e){
		e.preventDefault();
		$("#titulo_modal").html('REITERAR DOCUMENTO');
		$(".opt_asignar").hide();
		$(".opt_derivar").show();
		$(".opt_anexar").hide();
		$(".opt_reiterar").show();
		$("#btn_derderivar, #btn_derderivaragrupar").hide();
		var cont_ids=0;
		var cont_aceptados=0;
		var mensaje='';
		$("input[name='ids[]']").each( 
			function(){
				if (this.checked){
					cont_ids=cont_ids+1;
				}
			}
		);
		if(cont_ids==0){mensaje='Debe seleccionar al menos un registro.';}
		if(mensaje==''){
			oTabled_list_derdocumentos.fnClearTable();
			modal_derivar.dialog('open');
			$("input[name='ids[]']").each( 
				function(){
					if (this.checked){
						oTabled_list_derdocumentos.fnAddData( [$(this).parent().parent().children('td:eq(0)').html()+'<input type="hidden" value="'+$(this).parent().parent().children('td:last-child').children('input').val()+'">','<a href="#" class="btn_dereliminardoc"> X </a>'] );
					}
				}
			);
		}
		else{alert(mensaje);}
	})
	$("#btn_derreiterar").click(function()
	{
		var mensaje='';
		if($("#list_derdocumentos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un documento para realizar la operaci\u00f3n.\n';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';
		if($("#list_derdestinos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos una oficina de destino.\n';
		if(mensaje==''){	
			if(confirm('Esta seguro que desea derivar los documentos seleccionados')){
				var destinos=[];
				var acciones=[];
				var observaciones=[];
				var documentos=[];
				$("#list_derdestinos tbody tr").each(function(index){
						destinos[index]=$(this).children('td:eq(0)').children('input').val();
						acciones[index]=$(this).children('td:eq(1)').children('input[type="hidden"]').val();
						observaciones[index]=$(this).children('td:eq(2)').children('input').val();
				})
				$("#list_derdocumentos tbody tr").each(function(index){
						documentos[index]=$(this).children('td:eq(0)').children('input[type="hidden"]').val();
				})
				var objeto = {destinos:destinos,acciones:acciones,observaciones:observaciones};
				var data = {'idclasedoc':  $("#sel_derclasedoc").val(),
							  'asunto':  $("#txt_derasunto").val(),
							  'objeto':objeto,
							  'documentos':documentos};
				$.ajax({
						type:           'post',
						async:			false,
						cache:          false,
						url:            '/institucional/aplicativos/oad/sitradocV2/index.php?accion=DerivaMultDocMultDest&tipoder=REITERATIVO',
						data:           data,
						success: function(data){
							location.reload(true);
							}
					   });
			}
		}
		else alert(mensaje);
	})
	//boton que abre el modal de derivar
	$("#btn_derivar").click(function(e){//se efectuara todo la intefaz de derivacion
		e.preventDefault();
		$("#titulo_modal").html('DERIVAR DOCUMENTO');
		$(".opt_asignar").hide();
		$(".opt_derivar").show();
		$(".opt_reiterar").hide();
		var cont_ids=0;
		var cont_aceptados=0;
		var mensaje='';
		$("input[name='ids[]']").each( 
			function(){
				if (this.checked){
					cont_ids=cont_ids+1;
					if($(this).attr("estado")=='ACEPTADO'){cont_aceptados=cont_aceptados+1;}
				}
			}
		);
		if(cont_ids==0){mensaje='Debe seleccionar al menos un registro.';}
		else{
			if(cont_aceptados==cont_ids){mensaje='';}
			else{mensaje='No se puede derivar el documento, debido a que NO ha sido ACEPTADO o el trabajador al cual fue asignado lo mantiende como PENDIENTE.';}			
		}

		if(mensaje==''){
			oTabled_list_derdocumentos.fnClearTable();
			modal_derivar.dialog('open');
			$("input[name='ids[]']").each( 
				function(){
					if (this.checked){
						oTabled_list_derdocumentos.fnAddData( [$(this).parent().parent().children('td:eq(0)').html()+'<input type="hidden" value="'+$(this).parent().parent().children('td:last-child').children('input').val()+'">','<a href="#" class="btn_dereliminardoc"> X </a>'] );
					}
				}
			);
		}
		else{alert(mensaje);}
	})
	//del modal derivar, boton la cual agrega oficina destino a la lista para poder ser derivado
	$('#btn_deragregardestino').click(function(e){
		e.preventDefault();
		if($('#sel_derdestino').val()!='none'){
			var contador=0;
			var existe=0;
			$("#list_derdestinos tbody tr").each(function(){
				if($(this).children('td:nth-child(1)').children('input').val()==$('#sel_derdestino').val()){
					existe++;
				}
			})
			if(existe==0){
				oTabled_list_derdestinos.fnAddData( [
					'<input type="hidden" value="'+$('#sel_derdestino').val()+'" name="arr_derlista[iddestino][]">'+$('#sel_derdestino option:selected').html(),'<input type="hidden" name="arr_derlista[acciones][]"/><input type="text" class="text_deracciones"/>','<input type="text" name="arr_derlista[obs][]"/>','<a href="#" class="btn_dereliminardes"> X </a>' ] );
			}
		}
	})
	//del modal derivar y aasignar, caja de texto que al hacer click abre las posibles acciones a tomar
	$("body").delegate(".text_deracciones","click",function(){
			$(this).attr('chekado',1);
			var acciones_ya_marcadas = $(this).parent().children("[name='arr_derlista[acciones][]']").val().split(",");
			if($(this).parent().children("[name='arr_derlista[acciones][]']").val()==''){
			}
			else{
				$("[name='checkbox[]']").removeAttr('checked');
				$.each(acciones_ya_marcadas, function (ind, elem) { 
					
					$("[name='checkbox[]']").each(function(ar,arr){
						var accion=$(this);
						if(elem==accion.val()){
							accion.attr('checked','checked');
						}
					})
				});
			}
			modal_acciones.dialog( "open");
	})
	//del modal derivar, este boton elimina un destino de la lista
	$("body").delegate(".btn_dereliminardes","click",function(e){
		e.preventDefault();
		oTabled_list_derdestinos.fnDeleteRow($(this).parent().parent().index());
	})
	//del modal derivar, boton derivar
	$("#btn_derderivar").click(function()
	{
		var mensaje='';
		if($("#list_derdocumentos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un documento para realizar la operaci\u00f3n.\n';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';
		if($("#list_derdestinos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos uan oficina de destino.\n';
		if(mensaje==''){			
			if(confirm('Esta seguro que desea derivar los documentos seleccionados')){
				var destinos=[];
				var acciones=[];
				var observaciones=[];
				var documentos=[];
				$("#list_derdestinos tbody tr").each(function(index){
						destinos[index]=$(this).children('td:eq(0)').children('input').val();
						acciones[index]=$(this).children('td:eq(1)').children('input[type="hidden"]').val();
						observaciones[index]=$(this).children('td:eq(2)').children('input').val();
				})
				$("#list_derdocumentos tbody tr").each(function(index){
						documentos[index]=$(this).children('td:eq(0)').children('input[type="hidden"]').val();
				})
				var objeto = {destinos:destinos,acciones:acciones,observaciones:observaciones};
				var data = {'idclasedoc':  $("#sel_derclasedoc").val(),
							  'asunto':  $("#txt_derasunto").val(),
							  'objeto':objeto,
							  'documentos':documentos};
				$.ajax({
						type:           'post',
						async:			false,
						cache:          false,
						url:            '/institucional/aplicativos/oad/sitradocV2/index.php?accion=DerivaMultDocMultDest&tipoder=NORMAL',
						data:           data,
						success: function(data){
							location.reload(true);
							}
					   });
			}
		}
		else alert(mensaje);
	})
	$("#btn_derderivaragrupar").click(function()
	{
		var mensaje='';
		if($("#list_derdocumentos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un documento para realizar la operaci\u00f3n.\n';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		//if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';
		if($("#list_derdestinos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos una oficina de destino.\n';
		if(mensaje==''){	
			if(confirm('Esta seguro que desea derivar los documentos seleccionados')){
				var destinos=[];
				var acciones=[];
				var observaciones=[];
				var documentos=[];
				$("#list_derdestinos tbody tr").each(function(index){
						destinos[index]=$(this).children('td:eq(0)').children('input').val();
						acciones[index]=$(this).children('td:eq(1)').children('input[type="hidden"]').val();
						observaciones[index]=$(this).children('td:eq(2)').children('input').val();
				})
				$("#list_derdocumentos tbody tr").each(function(index){
						documentos[index]=$(this).children('td:eq(0)').children('input[type="hidden"]').val();
				})
				var objeto = {destinos:destinos,acciones:acciones,observaciones:observaciones};
				var data = {'idclasedoc':  $("#sel_derclasedoc").val(),
							  'asunto':  $("#txt_derasunto").val(),
							  'objeto':objeto,
							  'documentos':documentos,
							  'obsdoc':$('#txt_derobs').val()};
				$.ajax({
						type:           'post',
						async:			false,
						cache:          false,
						url:            '/institucional/aplicativos/oad/sitradocV2/index.php?accion=DerivaMultDocMultDest&tipoder=AGRUPAR',
						data:           data,
						success: function(data){
							location.reload(true);
							}
					   });
			}
		}
		else alert(mensaje);
	})
	$("#btn_anexar").click(function()
	{
		var mensaje='';
		if($("#list_derdocumentos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos un documento para realizar la operaci\u00f3n.\n';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		//if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';
		if($("#list_derdestinos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos una oficina de destino.\n';
		if(mensaje==''){	
			if(confirm('Esta seguro que desea derivar y anexar los documentos seleccionados')){
				var destinos=[];
				var acciones=[];
				var observaciones=[];
				var documentos=[];
				$("#list_derdestinos tbody tr").each(function(index){
						destinos[index]=$(this).children('td:eq(0)').children('input').val();
						acciones[index]=$(this).children('td:eq(1)').children('input[type="hidden"]').val();
						observaciones[index]=$(this).children('td:eq(2)').children('input').val();
				})
				$("#list_derdocumentos tbody tr").each(function(index){
						documentos[index]=$(this).children('td:eq(0)').children('input[type="hidden"]').val();
				})
				var objeto = {destinos:destinos,acciones:acciones,observaciones:observaciones};
				var data = {'idclasedoc':  $("#sel_derclasedoc").val(),
							  'asunto':  $("#txt_derasunto").val(),
							  'objeto':objeto,
							  'documentos':documentos,
							  'obsdoc':$('#txt_derobs').val()};
				$.ajax({
						type:           'post',
						async:			false,
						cache:          false,
						url:            '/institucional/aplicativos/oad/sitradocV2/index.php?accion=DerivaMultDocMultDest&tipoder=ANEXAR',
						data:           data,
						success: function(data){
							location.reload(true);
							}
					   });
			}
		}
		else alert(mensaje);
	})
	if($("#marcar_todos").is(':checked')){check_padre($("#marcar_todos"),$('.marcar_uno'));}
	$("#marcar_todos").click(function(){
		check_padre($(this),$('.marcar_uno'));
	})
	$(".marcar_uno").click(function(){
		check_hijo($("#marcar_todos"),$(".marcar_uno"));
	})
   var oTabled=$('#b_buscador').dataTable({
		 "bFilter": true,
		 "bLengthChange": true,
		"sPaginationType": "full_numbers",	
		"oLanguage": {"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"},
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/institucional/aplicativos/oad/sitradocV2/index.php?accion=BuscadorSecretaria",
		"sServerMethod": "POST",
		"bSort": false,
		"bRetrieve": false,
		"fnServerParams": function ( aoData ) {
      	aoData.push(
				{ "name": "FechaIni", "value": $("#FechaIni").val() },
				{ "name": "FechaFin", "value": $("#FechaFin").val() },
				{ "name": "tipodDoc", "value": $("#tipodDoc").val() },
				{ "name": "indicativo", "value": $("#indicativo").val() },
				{ "name": "anyo3", 		"value": $("#anyo3").val() },
				{ "name": "siglasDep", "value": $("#siglasDep").val() },
				{ "name": "nroTD", "value": $("#nroTD").val() },
				{ "name": "checkTodos", "value": $("#checkTodos").val()},
				{ "name": "checkAsunto", "value": $("#checkAsunto").val()},
				{ "name": "checkRazon", "value": $("#checkRazon").val()},
				{ "name": "checkTrab", "value": $("#checkTrab").val()},
				{ "name": "checkExt", "value": $("#checkExt").val()},
				{ "name": "checkInt", "value": $("#checkInt").val()},
				{ "name": "checkAceptados", "value": $("#checkAceptados").val()},
				{ "name": "checkNoAceptados", "value": $("#checkNoAceptados").val()},
				{ "name": "tipEstado", "value": $("input[name=tipEstado]:checked").val()}
				);	
    	},
		"fnDrawCallback": function( oSettings ) {$('#bg_todos_registros').removeAttr('checked');},
		"bJQueryUI": true
	});
	$.fn.dataTableSettings[0].oApi._fnLog = function(settings, level, msg, tn) {
		alert('Se Prodijo un error en la conexion.');
		location.reload(true);
	}
	$("#nroTD").keypress(function(e){
        if(e.which == 13) {
            $("#btn_buscar").click();
        }
    })
	$("#btn_buscar").click(function(e){
		e.preventDefault();
		if($('input[name=checkTodos]').is(':checked')){$('#checkTodos').val(1)}else{$('#checkTodos').val(0)}
		if($('input[name=checkAsunto]').is(':checked')){$('#checkAsunto').val(1);}else{$('#checkAsunto').val(0)}
		if($('input[name=checkRazon]').is(':checked')){$('#checkRazon').val(1);}else{$('#checkRazon').val(0)}
		if($('input[name=checkTrab]').is(':checked')){$('#checkTrab').val(1);}else{$('#checkTrab').val(0)}
		if($('input[name=checkExt]').is(':checked')){$('#checkExt').val(1);}else{$('#checkExt').val(0)}
		if($('input[name=checkInt]').is(':checked')){$('#checkInt').val(1);}else{$('#checkInt').val(0)}
		if($('input[name=checkAceptados]').is(':checked')){$('#checkAceptados').val(1);}else{$('#checkAceptados').val(0)}
		if($('input[name=checkNoAceptados]').is(':checked')){$('#checkNoAceptados').val(1);}else{$('#checkNoAceptados').val(0)}
		oTabled.fnDraw();	
	})
	//
	$('body').delegate('#bg_todos_registros',"click",function(e){
		check_padre($('#bg_todos_registros'),$(".bg_un_registro"))
	})
	$("body").delegate(".bg_un_registro","click",function(){
		check_hijo($('#bg_todos_registros'),$(".bg_un_registro"))
	});
	
	var oTabled_list_derdestinos=$('#list_derdestinos').dataTable({
		"bPaginate": false,
		//"sScrollY": "160px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
                //"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
				"sEmptyTable":	"Ning&uacute;n destino seleccionado.",
   				"sInfo":		"_TOTAL_ destinos seleccionados.",
				"sInfoEmpty":	"0 Destinos seleccionados."
            },
		"bJQueryUI": true
		})
	var oTabled_list_derdocumentos=$('#list_derdocumentos').dataTable({
		"bPaginate": false,
		//"sScrollY": "160px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
				"sEmptyTable":	"Ning&uacute;n documento seleccionado.",
   				"sInfo":		"_TOTAL_ documentos seleccionados.",
				"sInfoEmpty":	"0 Documentos seleccionados."
            },
		"bJQueryUI": true
		})
	var oTabled_list_trabajadores=$('#list_trabajadores').dataTable({
		"bPaginate": false,
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
				"sEmptyTable":	"Ning&uacute;n trabajador seleccionado.",
   				"sInfo":		"_TOTAL_ trabajadores seleccionados.",
				"sInfoEmpty":	"0 Trabajadores seleccionados."
            },
		"bJQueryUI": true
		})
/*	$("#b_buscador").delegate("tbody tr td:not(:nth-child(1)","click",function(e){//showDetailFlujoDocDir HojaRuta
		var ruta='/institucional/aplicativos/oad/sitradocV2/index.php?accion=showDetailFlujoDocDir&id='+$(this).parent().children('td:last').children('input').val();
		var caracteristicas = "height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0";
      	nueva=window.open(ruta, 'Popup', caracteristicas);
	})*/
	$("#b_buscador").delegate(".imp_hr","click",function(e){//showDetailFlujoDocDir HojaRuta
		e.preventDefault();
		var ruta='/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id='+$(this).parent().parent().children('td:last').children('input').val();
		var caracteristicas = "height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0";
      	nueva=window.open(ruta, 'Popup', caracteristicas);
	})
	$("input[name='tipEstado']").change(function(){
		if($(this).val()==1){
			$('.opt_pendientes').show();
			$('.opt_derivados').hide();
		}
		else{
			$('.opt_pendientes').hide();
			$('.opt_derivados').show();
		}
		oTabled.fnDraw();
	})
	//del modal
	$("body").delegate(".btn_dereliminardoc","click",function(e){
		e.preventDefault();
		oTabled_list_derdocumentos.fnDeleteRow($(this).parent().parent().index());
		identificador=$(this).parent().parent().children('td:eq(0)').children('input[type=hidden]').val();
		$(".input[type=checkbox][name='ids[]'][value="+identificador+"").removeAttr('checked');
	})
	$("#btn_dercancelar, #btn_fincancelar").click(function(){
		limpiar_modal();
	})
	function limpiar_modal(){	
		$("#txt_finobs").val('');
		$("#sel_derclasedoc").val('none');
		$("#txt_derasunto").val('');
		$("#txt_derobs").val('');
		$("#sel_trabajador").val('none');
		oTabled_list_trabajadores.fnClearTable();
		$("#sel_derdestino").val('none');
		oTabled_list_derdestinos.fnClearTable();
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}
});