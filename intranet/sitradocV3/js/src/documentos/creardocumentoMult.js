$(document).ready(function(){
	capa='<div class="popup_trans"></div>';
	$("body").prepend('<div class="popup_trans"></div>');
	$("#b_destinatario_agregar").css("position","relative")	
	
	$("ul.combo_check li").click(function(e){
		$(this).children('ul').css('display','block');
		$(".popup_trans").css("display","block");
	})
	$("ul.combo_check li button").click(function(e){
		e.preventDefault();
	})
	/*$("ul.combobox_check li span").mouseover(function(e){
		$(this).children('ul').css('display','none');
		e.preventDefault();
	})*/
	$(".popup_trans").click(function(){
		$("ul.combo_check li ul").css('display','none');
		$(".popup_trans").css("display","none");
		$("#b_destinatario_seleccionar_todos").removeAttr('checked');
		$(".b_destinatario_check").removeAttr('checked');
	})
	/*$("body:not(ul.combobox_check li)").click(function(e){
		$("ul.combobox_check li ul").css('display','none');
		e.preventDefault();
	})*/
	
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR
	$("#t_eliminar_destinatraio").click(function(){
		eliminar()
	});
	
	$("#idClaseDoc").change(function(){
		if($(this).val()==32){
			$("#panel_hojaderivacion").removeClass("std_oculto");
		}
		else{
			$("#panel_hojaderivacion").addClass("std_oculto");
		}
	})
	
	$("#b_destinatario_agregar").click(function(e){
		e.preventDefault();
		var contador=0;
		var contador_nocheck=0;
		$("ul#b_destinatario li").each(function(){
				if($(this).children('input').is(':checked')){
					contador=agregar($(this).children("input").val(),$(this).text())+contador;
					contador_nocheck++;
				}
		})
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
		$("ul.combo_check li ul").css('display','none');
		$("#b_destinatario_seleccionar_todos").removeAttr('checked');
		$(".b_destinatario_check").removeAttr('checked');
		$(".popup_trans").css("display","none");
		if(contador_nocheck==0){
			alert('No ha seleccionado ningun destinatario');
		}
		else{
			alert('Se agrego '+contador+' destinatario(s) a su lista.');
		}
	});
	$("input[name=bSubmit]").click(function(e){
		$("#dependencia").val(1);
		if($("#t_destinatarios tbody tr").size()==0){
			alert("Debe seleccionar la dependencia destino");
			return false;dependencia
		}
		else{
		return true;
		}
	})
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON ID "b_destinatario_seleccionar_todos"
	$('body').delegate('#b_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "b_destinatario_check"
	$("body").delegate(".b_destinatario_check","click",function(){
		check_hijo($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_destinatario_seleccionar_todos"
	$('body').delegate('#t_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	})		
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_destinatario_check"
	$("body").delegate(".t_destinatario_check","click",function(){
		check_hijo($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	});
	
	
	
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_destinatarios) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar(codigo,nombre){
		var contador=0;
		var existe=0;
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_destinatarios tbody").prepend('<tr codigo="'+codigo+'"><td><input type="hidden" value="'+codigo+'" name="destinatario_final[]">'+nombre+'</td><td class="border-left"><input type="checkbox" class="t_destinatario_check"></td></tr>');
			contador++;
		} 
		return contador;
	}
	//ELIMINARA LOS REGISTROS DE LA LISTA DONDE SE ENCUENTRAN LOS DESTINATARIOS SELECCIONADOS CHEKEADOS
	function eliminar(){
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).children("td:nth-child(2)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_destinatario_seleccionar_todos,.t_destinatario_check").removeAttr('checked');
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
	}
	
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}
});