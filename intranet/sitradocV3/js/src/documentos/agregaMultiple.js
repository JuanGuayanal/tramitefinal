$(document).ready(function(e) {
		var modal_acciones=$("#modal_deracciones").dialog({
				autoOpen:false,
				modal: true,
				closeOnEscape: true,
				width: 650,
				height: 350,
				resizable: false,
				draggable:true,
				close: function( event, ui ) {
					var input_text=$("[chekado=1]");
					input_text.val('');
					var acciones_marcadas=0;
					$("[name='checkbox[]']").each(function(){
						var accion=$(this);
						if(accion.is(':checked')){
							acciones_marcadas++;
							if(input_text.val()==''){
								input_text.val(accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(accion.val());
							}
							else{
								input_text.val(input_text.val()+','+accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(input_text.parent().children("[name='arr_derlista[acciones][]']").val()+','+accion.val());
							}
						}
					})
					if(acciones_marcadas==0){input_text.parent().children("[name='arr_derlista[acciones][]']").val('');};
					input_text.removeAttr('chekado');
					$("[name='checkbox[]']").removeAttr('checked');
				}
			});
	$("#btn_aceptarAcciones").click(function(){
		modal_acciones.dialog("close");
	})
	//boton que abre el modal de derivar
	//del modal derivar, boton la cual agrega oficina destino a la lista para poder ser derivado
	$('#btn_deragregardestino').click(function(e){
		e.preventDefault();
		if($('#sel_derdestino').val()!='none'){
			var contador=0;
			var existe=0;
			$("#list_derdestinos tbody tr").each(function(){
				if($(this).children('td:nth-child(1)').children('input').val()==$('#sel_derdestino').val()){
					existe++;
				}
			})
			if(existe==0){
				oTabled_list_derdestinos.fnAddData( [
					'<input type="hidden" value="'+$('#sel_derdestino').val()+'" name="arr_derlista[iddestino][]">'+$('#sel_derdestino option:selected').html(),'<input type="hidden" name="arr_derlista[acciones][]"/><input type="text" class="text_deracciones"/>','<input type="text" name="arr_derlista[obs][]"/>','<a href="#" class="btn_dereliminardes"> X </a>' ] );
			}
		}
	})
	//del modal derivar y aasignar, caja de texto que al hacer click abre las posibles acciones a tomar
	$("body").delegate(".text_deracciones","click",function(){
			$(this).attr('chekado',1);
			var acciones_ya_marcadas = $(this).parent().children("[name='arr_derlista[acciones][]']").val().split(",");
			if($(this).parent().children("[name='arr_derlista[acciones][]']").val()==''){
			}
			else{
				$("[name='checkbox[]']").removeAttr('checked');
				$.each(acciones_ya_marcadas, function (ind, elem) { 
					
					$("[name='checkbox[]']").each(function(ar,arr){
						var accion=$(this);
						if(elem==accion.val()){
							accion.attr('checked','checked');
						}
					})
				});
			}
			modal_acciones.dialog( "open" );
	})
	//del modal derivar, este boton elimina un destino de la lista
	$("body").delegate(".btn_dereliminardes","click",function(e){
		e.preventDefault();
		oTabled_list_derdestinos.fnDeleteRow($(this).parent().parent().index());
	})
	//del modal derivar, boton derivar
	$("#btn_ingresar").click(function(e){
		e.preventDefault();
		var mensaje='';
		if($("#sel_derclasedoc").val()=='none')mensaje=mensaje+'- Debe seleccionar la clase de documento.\n';
		if($("#txt_derasunto").val()=='')mensaje=mensaje+'- Debe escribir un asunto para el documento.\n';					
		//if($("#list_derdestinos tbody tr").length==0)mensaje=mensaje+'- Debe seleccionar al menos uan oficina de destino.\n';
		if($("#list_derdestinos tbody tr td.dataTables_empty").length==1)mensaje=mensaje+'- Debe seleccionar al menos uan oficina de destino.\n';
		if(mensaje==''){	
		if(confirm('Esta seguro que deseas derivar los documentos seleccionados')){
			
				$("#frmAddDocMul").submit();
			}
		}
		else alert(mensaje);
	})
	var oTabled_list_derdestinos=$('#list_derdestinos').dataTable({
		"bPaginate": false,
		//"sScrollY": "160px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
                //"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
				"sEmptyTable":	"Ning&uacute;n destino seleccionado.",
   				"sInfo":		"_TOTAL_ destinos seleccionados.",
				"sInfoEmpty":	"0 Destinos seleccionados."
            },
		"bJQueryUI": true
		})
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}
});