$(document).ready(function() {
	function getGET(){
	   var loc = document.location.href;
	   var getString = loc.split('?')[1];
	   var GET = getString.split('&');
	   var get = {};//this object will be filled with the key-value pairs and returned.
	
	   for(var i = 0, l = GET.length; i < l; i++){
		  var tmp = GET[i].split('=');
		  get[tmp[0]] = unescape(decodeURI(tmp[1]));
	   }
	   return get;
	}
	var get = getGET();
	if(typeof get.popup!='undefined'){
		$("#frmAddPersona").attr("action","/institucional/aplicativos/oad/directorio/index.php?popup=1");
		$("#accion").val($("#accion").val()+'&popup=1');
		//$("#menu_directorio, .WEB_CONTE_bloqueModulos5, .WEB_MAQUE_Fondo1, .WEB_MAQUE_Fondo2, .WEB_MAQUE_Fondo4, .WEB_CONTE_sombraHorizontalArriba, .WEB_CONTE_grupoSombraArriba").css("display","none");
		//$(".WEB_MAQUE_Alineacion1").css("width","650px");
		//$(".WEB_MAQUE_Fondo3, .WEB_MAQUE_f3_c1").css("width","auto");
		//$(".WEB_MAQUE_Alineacion1").css("padding","0");
		//$(".WEB_CONTE_sombraVerticalIzquierdo, .WEB_CONTE_sombraVerticalDerecho").css("height","0");
		//$(".WEB_MAQUE_f3_c1, .WEB_CONTE_bloqueModulos6").css("padding","0");
		//$("body").css("background","none");
		window.parent.$().mostrarIframe();
	}
	$("input[type=text], textarea").css('text-transform','uppercase');
	$("input[type=text]").each(function(){
		$(this).val($(this).val().toUpperCase());
	})
	$("textarea").each(function(){
		$(this).val($(this).val().toUpperCase());
	})
	//BLOQUEAR EVENTOS POR DEFECTOS DE BOTONES Y AGREG ACCIONES EN SU EVENTO CLICK
	$("#btn_agregar").click(function(e){
	//enviar una peticion ajax y que devuelva si encuentra alguna coindicencia.
		e.preventDefault();
		oTabled.fnClearTable();
		var error=$.trim($("input[name='nombres']").val())+$.trim($("input[name='apellidos']").val())+$.trim($("textarea[name='razonsocial']").val());
		if(error!=''){
			$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=Lista&crud=BUSCAR_COINCIDENCIAS',
				type:'POST',
				data:{nombres:$("input[name='nombres']").val(),apellidos:$("input[name='apellidos']").val(),razon_social:$("textarea[name='razonsocial']").val()},
				dataType: "json",
				success: function(data){
							if(data.length == 0){
								$("form[name='frmAddPersona']").submit();
							}
							else{
								for(var i = 0; i < data.length; i++) {
									var option = data[i.toString()];
									oTabled.fnAddData( [i+1,option]);
								}  
							$("#modal_coincidencias").dialog({
									modal: true,
									closeOnEscape: true,
									width: 400,
									height: 250,
									resizable: false,
									draggable:true
								});
							}
						}
			});
		}
		else{
			$("form[name='frmAddPersona']").submit();
		}
	})
	$("#btn_noseguro").click(function(e){
		e.preventDefault();
		$("#modal_coincidencias").dialog( "destroy" );
	});
	$("#btn_siseguro").click(function(e){
		e.preventDefault();
		$("form[name='frmAddPersona']").submit();
	});
	var oTabled=$('#list_coincidencia').dataTable({
		"bPaginate": false,
		"sScrollY": "350px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
				"sEmptyTable":	"No hay coincicencias encontradas.",
   				"sInfo":		"_TOTAL_ coindicencias encontradas.",
				"sInfoEmpty":	"0 coincicencias encontradas."
            },
		"bJQueryUI": true
		})
});