$(document).ready(function() {
	function getGET(){
	   var loc = document.location.href;
	   var getString = loc.split('?')[1];
	   var GET = getString.split('&');
	   var get = {};//this object will be filled with the key-value pairs and returned.
	
	   for(var i = 0, l = GET.length; i < l; i++){
		  var tmp = GET[i].split('=');
		  get[tmp[0]] = unescape(decodeURI(tmp[1]));
	   }
	   return get;
	}
	var get = getGET();
	if(typeof get.popup!='undefined'){
		$("#frmAddPersona2").attr("action","/institucional/aplicativos/oad/directorio/index.php?popup=1");
		//$("#accion").val($("#accion").val()+'&popup=1');
		//$("#menu_directorio, .WEB_CONTE_bloqueModulos5, .WEB_MAQUE_Fondo1, .WEB_MAQUE_Fondo2, .WEB_MAQUE_Fondo4, .WEB_CONTE_sombraHorizontalArriba, .WEB_CONTE_grupoSombraArriba").css("display","none");
		//$(".WEB_MAQUE_Alineacion1").css("width","650px");
		//$(".WEB_MAQUE_Fondo3, .WEB_MAQUE_f3_c1").css("width","auto");
		//$(".WEB_MAQUE_Alineacion1").css("padding","0");
		//$(".WEB_CONTE_sombraVerticalIzquierdo, .WEB_CONTE_sombraVerticalDerecho").css("height","0");
		//$(".WEB_MAQUE_f3_c1, .WEB_CONTE_bloqueModulos6").css("padding","0");
		//$("body").css("background","none");
		window.parent.$().mostrarIframe();
	}
	$("input[type=text], textarea").css('text-transform','uppercase');
	$("input[type=text]").each(function(){
		$(this).val($(this).val().toUpperCase());
	})
	$("textarea").each(function(){
		$(this).val($(this).val().toUpperCase());
	})
	//BLOQUEAR EVENTOS POR DEFECTOS DE BOTONES Y AGREG ACCIONES EN SU EVENTO CLICK
	$("#btn_agregar").click(function(e){
	//enviar una peticion ajax y que devuelva si encuentra alguna coindicencia.
		e.preventDefault();
		oTabled.fnClearTable();
		var error=$.trim($("input[name='nombres']").val())+$.trim($("input[name='apellidos']").val())+$.trim($("textarea[name='razonsocial']").val());
		if(error!=''){
			$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=Lista&crud=BUSCAR_COINCIDENCIAS',
				type:'POST',
				data:{nombres:$("input[name='nombres']").val(),apellidos:$("input[name='apellidos']").val(),razon_social:$("textarea[name='razonsocial']").val()},
				dataType: "json",
				success: function(data){
							if(data.length == 0){
								$("form[name='frmAddPersona2']").submit();
							}
							else{
								for(var i = 0; i < data.length; i++) {
									var option = data[i.toString()];
									oTabled.fnAddData( [i+1,option]);
								}  
							$("#modal_coincidencias").dialog({
									modal: true,
									closeOnEscape: true,
									width: 600,
									height: 470,
									resizable: false,
									draggable:true
								});
							}
						}
			});
		}
		else{
			$("form[name='frmAddPersona2']").submit();
		}
	})
	$("#btn_noseguro").click(function(e){
		e.preventDefault();
		$("#modal_coincidencias").dialog( "destroy" );
	});
	$("#btn_siseguro").click(function(e){
		e.preventDefault();
		$("form[name='frmAddPersona2']").submit();
	});
	var oTabled=$('#list_coincidencia').dataTable({
		"bPaginate": false,
		"sScrollY": "100px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
				"sEmptyTable":	"No hay coincicencias encontradas.",
   				"sInfo":		"_TOTAL_ coindicencias encontradas.",
				"sInfoEmpty":	"0 coincicencias encontradas."
            },
		"bJQueryUI": true
		})
	$("form[name='frmAddPersona2']").submit(function(){
		
		$("input[type=text]").each(function(){
			$(this).val($(this).val().toUpperCase());
		})
		$("textarea").each(function(){
			$(this).val($(this).val().toUpperCase());
		})
		if(confirm('\u00BFDesea Guardar este Registro?')){
			var mensaje='';
			if($("#tipPersona").val()=='none')mensaje=mensaje+'\nDebe seleccionar el tipo de persona.';
			if($("#tipIdent").val()=='none')mensaje=mensaje+'\nDebe registrar el tipo de indentificaci\u00F3n.';
			if($("#ruc").val()=='')mensaje=mensaje+'\nDebe registrar el n\u00FAmero de identificaci\u00F3n.';
			if($("#ruc").val()!=''){
				$.ajax({
					async:false,
					url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=Lista&crud=BUSCAR_NRODOCUMENTO',
					type:'POST',
					data:{ruc:$("#ruc").val()},
					success: function(data){
								if(data>0)mensaje=mensaje+'\nYa existe un registro con el mismo Nro de Documento.';
								else{
									if(($("select[name='tipIdent']").val()==8)&&($("#ruc").val().length!=11))mensaje=mensaje+'\nEl Nro de RUC debe contener 11 digitos\u00F3n.';
									if(($("select[name='tipIdent']").val()==1)&&($("#ruc").val().length!=8))mensaje=mensaje+'\nEl Nro de DNI debe contener 8 digitos\u00F3n.';
								}
							}
				});
			}
			if($("#tipPersona").val()=='1'){
				if($("#nombres").val()=='')mensaje=mensaje+'\nDebe registrar el nombre.';
				if($("#apellidos").val()=='')mensaje=mensaje+'\nDebe registrar el apellido.';
			}
			if($("#tipPersona").val()=='2'){
				if($("textarea[name='razonsocial']").val()=='')mensaje=mensaje+'\nDebe registrar la Raz\u00F3n Social.';
			}
			if(($("imput[name='codDepa']").val()=='none') || ($("imput[name='codProv']").val()=='none')
			|| ($("imput[name='codDist']").val()=='none')){mensaje=mensaje+'\nDebe registrar el Ubigeo.';}
			if(mensaje==''){
				return true;
			}
			else {
				alert(mensaje);
				return false;
			}
		}else
			return false;
	});
});

/*	if(confirm('¿Desea Guardar este Registro?')){
		
		MM_validateForm('sector','Sector','Sel','tipPersona','Tipo de Persona','Sel','tipIdent','Tipo de Identificación','Sel',{/literal}{if $tipPersona==1}'nombres','Nombre','R','apellidos','Apellidos','R','ruc','DNI','R',{else}'razonsocial','Razón Social','R','ruc','RUC','R',{/if}'direccion','Dirección','R');
		a = document.MM_returnValue;
		//busca(pForm);
		tamanyoCadena(pForm);
		b = document.MM_returnValue;
		alert(a)
		alert(b)
		if (a ==true && b ==true)
			return document.MM_returnValue;
		else
			return false;
		{literal}
	}else
		return false;*/