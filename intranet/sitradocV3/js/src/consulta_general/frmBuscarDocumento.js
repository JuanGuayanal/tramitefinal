$(document).ready(function() {
	//$(".para_externos, .externos, .internos").css('display','none');
	$("input[name=id_tipo_documento]").val(1);
  	var oTabled= $('#b_documentos').dataTable( {
		"sPaginationType": "full_numbers",
		"bAutoWidth": false,
		"sScrollY": "320px",
		"bFilter": true,
		"bLengthChange": false,
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/institucional/aplicativos/oad/sitradocV2/index.php?accion=ListaEspBuscaDoc",
		"sServerMethod": "POST",
		"bSort": false,
		"fnServerParams": function ( aoData ) {
      	aoData.push( 
				{ "name": "busca",	 			"value":$.trim($('input[name=busca]').val())		},
				{ "name": "n_registro",	 		"value":$.trim($('input[name=n_registro]').val())	},
				{ "name": "n_indicativo", 		"value":$.trim($('input[name=n_indicativo]').val())	},
				{ "name": "id_cdi",				"value":$('#clase_doc').val()						},
				{ "name": "asunto", 			"value":$.trim($('textarea[name=asunto]').val())	},
				{ "name": "razon_social", 		"value":$.trim($('input[name=razon_social]').val())	},
				{ "name": "id_tipoasunto", 		"value":$('select[name=id_tipoasunto]').val()		},
				{ "name": "fecini", 			"value":$.trim($('input[name=fecini]').val())		},
				{ "name": "fecfin", 			"value":$.trim($('input[name=fecfin]').val())		},
				{ "name": "id_tipo_documento", 	"value":$.trim($('input[name=tipo_cd]:checked').val())},
				{ "name": "coddep", 			"value":$('select[name=coddep]').val()				}
				);
    	},
		"fnDrawCallback": function( oSettings ) {
			$("#b_documentos_filter").remove();
			$("#b_documentos_paginate span a").click(function(){
			$("#b_documento_seleccionar_todos").removeAttr("checked")
			})
		},
		"aoColumns": [ 
			{"bVisible":  false },
			{ "bVisible": false },
			null,
			null,
			null,
			null,
			null,
			null
		],
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            },
		"bJQueryUI": true
    } );
	$("#buscar").click(function(e){//
		$('input[name=busca]').val(1);
		if(($('input[name=fecini]').val()=='')||($('input[name=fecfin]').val()=='')){
		alert('Debe seleccionar un rango de fechas.')
		}
		else{
			oTabled.fnFilter( '' )
		} 
		e.preventDefault();
	})
	 $('#buscar').keyup(function(e) {
		if(e.keyCode==13){
			$('input[name=busca]').val(1);
			if(($('input[name=fecini]').val()=='')||($('input[name=fecfin]').val()=='')){
			alert('Debe seleccionar un rango de fechas.')
			}
			else{oTabled.fnFilter( '' );}
		}
	});
	$("#b_documentos").delegate("tr td a","click",function(e){//
		e.preventDefault();
			var caracteristicas = "height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0";
      		nueva=window.open($(this).attr('href'), 'Popup', caracteristicas);
      	return false;
	})
/*	$("#buscar").click(function(){
		
		$.ajax({
				async:false,
				dataType: "json",
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=ListaEspBuscaDoc',
				data:{	n_registro:$.trim($('input[name=n_registro]').val()),
						n_indicativo:$.trim($('input[name=n_indicativo]').val()),
						id_cdi:$('select[name=id_cdi]').val(),
						asunto:$.trim($('textarea[name=asunto]').val()),
						razon_social:$.trim($('input[name=razon_social]').val()),
						id_tipoasunto:$('select[name=id_tipoasunto]').val(),
						fecini:$.trim($('input[name=fecini]').val()),
						fecfin:$.trim($('input[name=fecfin]').val())
					},
				type:'POST',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var row = data[i];  
						alert(row[1])
					}   
				}
			});
	})*/
	$("input[name=tipo_cd]").click(function(){
		if($(this).val()==0){
			$('select[name=id_cdi]').addClass('std_oculto')
			$('select[name=id_cdi]').removeAttr('id')
			$('select[name=id_cde]').removeClass('std_oculto')
			$('select[name=id_cde]').attr('id','clase_doc')
			$("input[name=id_tipo_documento]").val(0);
			$(".para_externos, .para_nexternos").css('display','none');
			$(".externos").css('display','none');
			$(".internos").css('display','none');
			$('input[name=n_registro]').val('');
			$('select[name=coddep]').val(0);
		}
		if($(this).val()==1){
			$('select[name=id_cdi]').addClass('std_oculto')
			$('select[name=id_cdi]').removeAttr('id')
			$('select[name=id_cde]').removeClass('std_oculto')
			$('select[name=id_cde]').attr('id','clase_doc')
			$("input[name=id_tipo_documento]").val(1);
			$(".para_externos").css('display','table-row');
			$(".para_nexternos").css('display','inline-block');
			$(".externos").css('display','inline');
			$(".internos").css('display','none');
			$('select[name=coddep]').val(0);
		}
		if($(this).val()==4){
			$('select[name=id_cde]').addClass('std_oculto')
			$('select[name=id_cde]').removeAttr('id')
			$('select[name=id_cdi]').removeClass('std_oculto')
			$('select[name=id_cdi]').attr('id','clase_doc')
			$("input[name=id_tipo_documento]").val(4);
			$(".para_externos, .para_nexternos").css('display','none');
			$(".externos").css('display','none');
			$(".internos").css('display','inline');
			$('input[name=n_registro]').val('');
		}
	})
	$("#exportar_excel").click(function(){
		$('input[name=tipo_reporte]').val(1);
		$('#formulario').submit();
	});
	$("#exportar_pdf").click(function(){
		$('input[name=tipo_reporte]').val(2);
		$('#formulario').submit();
	});
});