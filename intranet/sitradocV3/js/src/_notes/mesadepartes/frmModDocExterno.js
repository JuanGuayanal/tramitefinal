// JavaScript Document
$(document).ready(function() {
$(function() {
		function log(id,documento,direccion,nombre) {
			$("#id_persona").val(id);
			$("#_ruc").val(documento);
			$("textarea[name=_domicilio]").val(direccion);
			$("textarea[name=_nombre]").val(nombre);
		}
	
		$.ajax({
			//url: "/sitradocV3/js/plugins/jquery-ui-1.8.6/demos/autocomplete/london.xml",
			url: "/sitradocV3/index.php?accion=directorio_personalxml",
			dataType: "xml",
			success: function( xmlResponse ) {
				var data = $( "personal", xmlResponse ).map(function() {
					return {
						value: $( "nombre", this ).text() + " ( " +
							($.trim( $( "documento", this ).text())+' )'),
						id: $( "id", this ).text(),
						documento:$( "documento", this ).text(),
						direccion:$( "direccion", this).text(),
						nombre:$( "nombre", this).text()
					};
				}).get();
				$( "#personal" ).autocomplete({
					source: data,
					minLength: 4,
					select: function( event, ui ) {
						log(ui.item.id,ui.item.documento,ui.item.direccion,ui.item.nombre);
					}
				});
			},
			error: function (request, status, error) {
				alert("Error al cargar la informacion.");
			}
		});
	});
	
});