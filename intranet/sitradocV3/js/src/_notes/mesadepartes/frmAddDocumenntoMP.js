$(document).ready(function() {
	//BLOQUEAR EVENTOS POR DEFECTOS DE BOTONES Y AGREG ACCIONES EN SU EVENTO CLICK
	$("#std_btn_agrega_dep").click(function(e){
		e.preventDefault();
		if($("#dependencia").val() != "none"){
			var contador=0;
			$("#std_lista_dependencias tbody tr").each(function(){
				if($(this).attr("codigo")==$("#dependencia").val()){contador++;}
			})
			if(contador==0){agregar_dep();}
		}
	})
	function agregar_dep(){
		var contador=$("#std_lista_dependencias tbody tr").size();
		contador=contador+1;
		$("#std_lista_dependencias tbody").append('<tr codigo="'+$("#dependencia").val()+'"><td valign="middle">'+contador+'</td><td valign="middle"><input type="hidden" value="'+$("#dependencia").val()+'" name="dependencias_multiples[]"/>'+$("#dependencia option:selected").html()+'</td><td valign="middle"><input type="text" name="foliosv[]" values="1" size="2" maxlength="4" class="ipseln"/></td><td valign="middle"><textarea name="obsv[]" rows="2" class="iptxtn"></textarea></td><td valign="middle"><a href="#">X</a></td></tr>');
	}
	
	//BLOQUEO COMPORTAMIENTO PRO DEFECTO DE LINK
	$("#std_lista_dependencias tbody").delegate("tr td a", "click", function(e) {
	  	e.preventDefault();
		$(this).parent().parent().remove();
	});
	$("#Derivar").click(function(){
		if($("#Derivar").is(':checked')){
			$(".tr_derivar").removeClass('std_oculto');
		}
		else{
			$(".tr_derivar").addClass('std_oculto');
		}
	})
	$("input[type=submit]").click(function(){
		var mensaje='Se han detectado los siguientes errores:\n';
		if($("#id_persona").val()==''){mensaje=mensaje+'- Razon Social o Personal debe contener un valor.\n';}
		if($("#asunto").val()==''){mensaje=mensaje+'- Asunto es un dato obligatorio.\n';}
		if($("#indicativo").val()==''){mensaje=mensaje+'- Nro Indicativo es un dato obligatorio.\n';}
		if($("#Derivar").is(':checked') && ($("#std_lista_dependencias tbody tr").size()==0)){mensaje=mensaje+'- Debe seleccionar al menos una dependencia de destino.\n';}
		
		if($("#id_persona").val()!=''){
			if($("#asunto").val()!=''){
				if($("#indicativo").val()!=''){
					if($("#Derivar").is(':checked')){
						if($("#std_lista_dependencias tbody tr").size()==0){
							alert(mensaje);return false;
						}else{ return true;}
					}else{return true;}
				}else{alert(mensaje);return false;}
			}else{alert(mensaje);return false;}
		}else{alert(mensaje);return false;}
	})
});