$(document).ready(function(){
	$(".btn_eliminar_adj").click(function(e){//AL MOMENTO DE HACER CHANGE EN EL COMBO
		e.preventDefault();
		var valor=$(this).attr("id");
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=EliminarDocumento&doc=adjunto',
				type:'POST',
				data:{idAdj:valor},
				success: function(data){
					alert(data) ; 
					location.reload(true);
				}
			});
	})

	$(".btn_eliminar_ext").click(function(e){//AL MOMENTO DE HACER CHANGE EN EL COMBO
		e.preventDefault();
		var valor=$(this).attr("id");
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=EliminarDocumento&doc=externo',
				type:'POST',
				data:{idDoc:valor},
				success: function(data){
					alert(data) ; 
					location.reload(true);
				}
			});
	})

});