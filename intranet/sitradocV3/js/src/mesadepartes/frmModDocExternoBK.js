// JavaScript Document
$(document).ready(function() {
$(function() {
	$("textarea[name=_nombre]").val($("#RazonSocial option:selected").html());
	//$("#id_persona").val($("#RazonSocial").val());
	
		function log(id,documento,direccion,nombre) {
			$("#id_persona").val(id);
			$("#_ruc").val(documento);
			$("textarea[name=_domicilio]").val(direccion);
			$("textarea[name=_nombre]").val(nombre);
		}
	
		$.ajax({
			//url: "/sitradocV3/js/plugins/jquery-ui-1.8.6/demos/autocomplete/london.xml",
			url: "/sitradocV3/index.php?accion=directorio_personalxml",
			dataType: "xml",
			success: function( xmlResponse ) {
				var data = $( "personal", xmlResponse ).map(function() {
					return {
						value: $( "nombre", this ).text() + " ( " +
							($.trim( $( "documento", this ).text())+' )'),
						id: $( "id", this ).text(),
						documento:$( "documento", this ).text(),
						direccion:$( "direccion", this).text(),
						nombre:$( "nombre", this).text()
					};
				}).get();
				$( "#personal" ).autocomplete({
					source: data,
					minLength: 4,
					select: function( event, ui ) {
						log(ui.item.id,ui.item.documento,ui.item.direccion,ui.item.nombre);
					}
				});
			},
			error: function (request, status, error) {
				alert("Error al cargar la informacion.");
			}
		});
	});
	$("#adjuntar").click(function(){
		var mensaje='';
		if($('select[name=Destino]').val()=='none'){mensaje=mensaje+'Debe seleccionar un destino.\n';}
		if($('select[name=Persona]').val()=='none'){mensaje=mensaje+'Debe seleccionar una persona.\n';}
		if($('select[name=Documento]').val()=='none'){mensaje=mensaje+'Debe seleccionar el tipo de documento.\n';}
		if(($('input[name=Folios]').val()==0) || ($.trim($('input[name=Folios]').val())=='')){mensaje=mensaje+'Debe registrar la cantidad de folios.\n';}
		if($('input[name=id_persona]').val()==''){mensaje=mensaje+'Debe seleccionar el remitente.\n';}
		if($.trim($('textarea[name=Contenido]').val())==''){mensaje=mensaje+'Es necesario escribir el asunto.\n';}
		
		if(($('select[name=Destino]').val()=='none') || ($('select[name=Persona]').val()=='none') ||($('select[name=Documento]').val()=='none') ||  ($('input[name=id_persona]').val()=='') || ($.trim($('textarea[name=Contenido]').val())=='')||(($('input[name=Folios]').val()==0) || ($.trim($('input[name=Folios]').val())==''))){
			alert(mensaje)
			return false;
		}
		else{
				if(confirm('\u00BFEst\u00e1 seguro que desea Adjuntar el Doc. Asociado?')){
					return true;
				}
				else{return false;}
		}
	})
	$("#modificar_adjunto").click(function(){
		var mensaje='';
		if($('select[name=Destino]').val()=='none'){mensaje=mensaje+'Debe seleccionar un destino.\n';}
		if($('select[name=Persona]').val()=='none'){mensaje=mensaje+'Debe seleccionar una persona.\n';}
		if($('select[name=tipDocumento]').val()=='none'){mensaje=mensaje+'Debe seleccionar el tipo de documento.\n';}
		if(($('input[name=folios]').val()==0) || ($.trim($('input[name=folios]').val())=='')){mensaje=mensaje+'Debe registrar la cantidad de folios.\n';}
		if($('input[name=id_persona]').val()==''){mensaje=mensaje+'Debe seleccionar el remitente.\n';}
		if($.trim($('textarea[name=contenido]').val())==''){mensaje=mensaje+'Es necesario escribir el asunto.\n';}
		
		if(($('select[name=Destino]').val()=='none') || ($('select[name=Persona]').val()=='none') ||($('select[name=tipDocumento]').val()=='none') ||  ($('input[name=id_persona]').val()=='') || ($.trim($('textarea[name=contenido]').val())=='')||(($('input[name=folios]').val()==0) || ($.trim($('input[name=folios]').val())==''))){
			alert(mensaje)
			return false;
		}
		else{
			return true;
		}
	})
	$("select[name=Destino]").change(function(){//AL MOMENTO DE HACER CHANGE EN EL COMBO
		var valor=$(this).val();
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=utilitarios_trabajadoresDependencia',
				type:'POST',
				data:{Destino:valor},
				success: function(data){
					$("select[name=Persona] option").remove();
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("select[name=Persona]").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
	})
	var oTabled_c=$('#lcorrespondencia').dataTable({
		"bPaginate": false,
		/*"sScrollY": "200px",*/
		"bFilter": false,
		"bSort": false,
		"bInfo": false,
		"oLanguage": {
                //"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
				"sEmptyTable":	"No existe correspondencia pendiente."
            }
		})
	mostrar_correspondencia();
	$('input[name=respuesta]').click(function(e){
		mostrar_correspondencia();
	})
	function mostrar_correspondencia(){
		if($('input[name=respuesta]:checked').val()==1){
			$("#tr_lcorrespondencia").removeClass('std_oculto');
		}
		if($('input[name=respuesta]:checked').val()==2){
			$("#tr_lcorrespondencia").addClass('std_oculto');
		}	
	}
});