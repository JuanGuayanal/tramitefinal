$(document).ready(function() {
	var modal_acciones=$("#modal_deracciones").dialog({
			autoOpen: false,
				modal: true,
				closeOnEscape: false,
				width: 650,
				height: 350,
				resizable: false,
				draggable:true,
				close: function( event, ui ) {
					var input_text=$("[chekado=1]");
					input_text.val('');
					var acciones_marcadas=0;
					$("[name='checkbox[]']").each(function(){
						var accion=$(this);
						if(accion.is(':checked')){
							acciones_marcadas++;
							if(input_text.val()==''){
								input_text.val(accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(accion.val());
							}
							else{
								input_text.val(input_text.val()+','+accion.attr('nombre'));
								input_text.parent().children("[name='arr_derlista[acciones][]']").val(input_text.parent().children("[name='arr_derlista[acciones][]']").val()+','+accion.val());
							}
						}
					})
					if(acciones_marcadas==0){input_text.parent().children("[name='arr_derlista[acciones][]']").val('');};
					input_text.removeAttr('chekado');
					$("[name='checkbox[]']").removeAttr('checked');
				}
			});
	$("body").delegate(".text_deracciones","click",function(){
			$(this).attr('chekado',1);
			var acciones_ya_marcadas = $(this).parent().children("[name='arr_derlista[acciones][]']").val().split(",");
			if($(this).parent().children("[name='arr_derlista[acciones][]']").val()==''){
			}
			else{
				$("[name='checkbox[]']").removeAttr('checked');
				$.each(acciones_ya_marcadas, function (ind, elem) { 
					
					$("[name='checkbox[]']").each(function(ar,arr){
						var accion=$(this);
						if(elem==accion.val()){
							accion.attr('checked','checked');
						}
					})
				});
			}
			modal_acciones.dialog( "open");
	})
	$("#btn_aceptarAcciones").click(function(){
		modal_acciones.dialog("close");
	})
	//BLOQUEAR EVENTOS POR DEFECTOS DE BOTONES Y AGREG ACCIONES EN SU EVENTO CLICK
/*	$("#std_btn_agrega_dep").click(function(e){
		e.preventDefault();
		if($("#dependencia").val() != "none"){
			var contador=0;
			$("#std_lista_dependencias tbody tr").each(function(){
				if($(this).attr("codigo")==$("#dependencia").val()){contador++;}
			})
			if(contador==0){agregar_dep();}
		}
	})
	function agregar_dep(){
		var contador=$("#std_lista_dependencias tbody tr").size();
		contador=contador+1;
		$("#std_lista_dependencias tbody").append('<tr codigo="'+$("#dependencia").val()+'"><td valign="middle">'+contador+'</td><td valign="middle"><input type="hidden" value="'+$("#dependencia").val()+'" name="dependencias_multiples[]"/>'+$("#dependencia option:selected").html()+'</td><td valign="middle"><input type="text" name="foliosv[]" values="1" size="2" maxlength="5" class="iptxtn"/></td><td valign="middle"><textarea name="obsv[]" rows="3" class="iptxtn" cols="70"></textarea></td><td valign="middle"><a href="#">X</a></td></tr>');

	}
	$("#std_lista_dependencias tbody").delegate("tr td a", "click", function(e) {
	  	e.preventDefault();
		$(this).parent().parent().remove();
	});
	*/
	$('#std_btn_agrega_dep').click(function(e){
		e.preventDefault();
		if($('#dependencia').val()!='none'){
			var contador=0;
			var existe=0;
			$("#lista_dependencias tbody tr").each(function(){
				if($(this).children('td:nth-child(1)').children('input').val()==$('#dependencia').val()){
					existe++;
				}
			})
			if(existe==0){
			contador++;
		oTabled_listadependencias.fnAddData( [
		'<input type="hidden" value="'+$("#dependencia").val()+'" name="dependencias_multiples[]"/>'+$("#dependencia option:selected").html(),'<input type="text" name="foliosv[]" values="1" size="2" maxlength="5" class="iptxtn"/>','<input type="hidden" name="arr_derlista[acciones][]"/><input type="text" class="text_deracciones iptxtn"/>','<input type="text" name="obsv[]" class="iptxtn" />','<a href="#" class="btn_eliminardep"> X </a>' ]);
			
			}
		}
	})
	$("body").delegate(".btn_eliminardep","click",function(e){
		e.preventDefault();
		oTabled_listadependencias.fnDeleteRow($(this).parent().parent().index());
	})

	//BLOQUEO COMPORTAMIENTO PRO DEFECTO DE LINK
	$("#Derivar").click(function(){
		if($("#Derivar").is(':checked')){
			$(".tr_derivar").removeClass('std_oculto');
		}
		else{
			$(".tr_derivar").addClass('std_oculto');
		}
	})
	$('#indicativo').keydown(function(e){
		if(e.keyCode==13){
			return false;
		}
	});
	$("input[type=submit]").click(function(){
		var mensaje='Se han detectado los siguientes errores:\n';
		if($("#id_persona").val()==''){mensaje=mensaje+'- Razon Social o Personal debe contener un valor.\n';}
		if($("#asunto").val()==''){mensaje=mensaje+'- Asunto es un dato obligatorio.\n';}
		if($("#indicativo").val()==''){mensaje=mensaje+'- Nro Indicativo es un dato obligatorio.\n';}
		if($("#Derivar").is(':checked') && ($("#lista_dependencias tbody tr td.dataTables_empty").length==1)){mensaje=mensaje+'- Debe seleccionar al menos una unidad de destino.\n';}
		
		if($("#id_persona").val()!=''){
			if($("#asunto").val()!=''){
				if($("#indicativo").val()!=''){
					if($("#Derivar").is(':checked')){
						if($("#lista_dependencias tbody tr td.dataTables_empty").length==1){
							alert(mensaje);return false;
						}else{ return true;}
					}else{return true;}
				}else{alert(mensaje);return false;}
			}else{alert(mensaje);return false;}
		}else{alert(mensaje);return false;}
	})
	var oTabled_listadependencias=$('#lista_dependencias').dataTable({
		"bPaginate": false,
		//"sScrollY": "160px",
		"bFilter": false,
		"bSort": false,
		"oLanguage": {
				"sEmptyTable":	"Ning&uacute;n  seleccionado.",
   				"sInfo":		"_TOTAL_  seleccionadas.",
				"sInfoEmpty":	"0  seleccionados."
            },
		"bJQueryUI": true
		})
	$("textarea[name=_nombre]").val($("#RazonSocial option:selected").html());
	//$("#id_persona").val($("#RazonSocial").val());
	
		function log(id,documento,direccion,nombre) {
			$("#id_persona").val(id);
			$("#_ruc").val(documento);
			$("textarea[name=_domicilio]").val(direccion);
			$("textarea[name=_nombre]").val(nombre);
		}
    $( "#personal" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          //url: "http://gd.geobytes.com/AutoCompleteCity",
		  url: "/institucional/aplicativos/oad/sitradocV2/index.php?accion=Lista&crud=BUSCAR_PERSONAL",
          dataType: "json",
		  type:'POST',
          data: {
            //q: request.term
			buscar:$("#personal").val()
          },
          success: function( data ) {
           // response( data );
				response($.map(data, function (item) {
				return { 
						value: item.nombre + " ( " +($.trim( item.documento)+' )'),
						id: item.id,
						documento:item.documento,
						direccion:item.direccion,
						nombre:item.nombre
				};
				}))
          },
			error: function (request, status, error) {
				alert("Error al cargar la informacion.");
			}
        });
      },
      minLength: 3,
      select: function( event, ui ) {
		  log(ui.item.id,ui.item.documento,ui.item.direccion,ui.item.nombre);
      }
    });
	$("#adjuntar").click(function(){
		var mensaje='';
		if($('select[name=Destino]').val()=='none'){mensaje=mensaje+'Debe seleccionar un destino.\n';}
		if($('select[name=Persona]').val()=='none'){mensaje=mensaje+'Debe seleccionar una persona.\n';}
		if($('select[name=Documento]').val()=='none'){mensaje=mensaje+'Debe seleccionar el tipo de documento.\n';}
		if(($('input[name=Folios]').val()==0) || ($.trim($('input[name=Folios]').val())=='')){mensaje=mensaje+'Debe registrar la cantidad de folios.\n';}
		if($('input[name=id_persona]').val()==''){mensaje=mensaje+'Debe seleccionar el remitente.\n';}
		if($.trim($('textarea[name=Contenido]').val())==''){mensaje=mensaje+'Es necesario escribir el asunto.\n';}
		
		if(($('select[name=Destino]').val()=='none') || ($('select[name=Persona]').val()=='none') ||($('select[name=Documento]').val()=='none') ||  ($('input[name=id_persona]').val()=='') || ($.trim($('textarea[name=Contenido]').val())=='')||(($('input[name=Folios]').val()==0) || ($.trim($('input[name=Folios]').val())==''))){
			alert(mensaje)
			return false;
		}
		else{
				if(confirm('\u00BFEst\u00e1 seguro que desea Adjuntar el Doc. Asociado?')){
					return true;
				}
				else{return false;}
		}
	})
	$("#modificar_adjunto").click(function(){
		var mensaje='';
		if($('select[name=Destino]').val()=='none'){mensaje=mensaje+'Debe seleccionar un destino.\n';}
		if($('select[name=Persona]').val()=='none'){mensaje=mensaje+'Debe seleccionar una persona.\n';}
		if($('select[name=tipDocumento]').val()=='none'){mensaje=mensaje+'Debe seleccionar el tipo de documento.\n';}
		if(($('input[name=folios]').val()==0) || ($.trim($('input[name=folios]').val())=='')){mensaje=mensaje+'Debe registrar la cantidad de folios.\n';}
		if($('input[name=id_persona]').val()==''){mensaje=mensaje+'Debe seleccionar el remitente.\n';}
		if($.trim($('textarea[name=contenido]').val())==''){mensaje=mensaje+'Es necesario escribir el asunto.\n';}
		
		if(($('select[name=Destino]').val()=='none') || ($('select[name=Persona]').val()=='none') ||($('select[name=tipDocumento]').val()=='none') ||  ($('input[name=id_persona]').val()=='') || ($.trim($('textarea[name=contenido]').val())=='')||(($('input[name=folios]').val()==0) || ($.trim($('input[name=folios]').val())==''))){
			alert(mensaje)
			return false;
		}
		else{
			return true;
		}
	})
	$("select[name=Destino]").change(function(){//AL MOMENTO DE HACER CHANGE EN EL COMBO
		var valor=$(this).val();
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=utilitarios_trabajadoresDependencia',
				type:'POST',
				data:{Destino:valor},
				success: function(data){
					$("select[name=Persona] option").remove();
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("select[name=Persona]").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
	})
	var oTabled_c=$('#lcorrespondencia').dataTable({
		"bPaginate": false,
		/*"sScrollY": "200px",*/
		"bFilter": false,
		"bSort": false,
		"bInfo": false,
		"oLanguage": {
                //"sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
				"sEmptyTable":	"No existe correspondencia pendiente."
            }
		})
	mostrar_correspondencia();
	$('input[name=respuesta]').click(function(e){
		mostrar_correspondencia();
	})
	function mostrar_correspondencia(){
		if($('input[name=respuesta]:checked').val()==1){
			$("#tr_lcorrespondencia").removeClass('std_oculto');
		}
		if($('input[name=respuesta]:checked').val()==2){
			$("#tr_lcorrespondencia").addClass('std_oculto');
		}	
	}
	$("#lnk_regper").click(function(e){
		e.preventDefault();
		$("#modal_regper").dialog({
									modal: true,
									closeOnEscape: true,
									width: 800,
									height: 550,
									resizable: false,
									draggable:true
								});
		$("#iframe_regper").css('display','none');
		$("#iframe_regper").attr("src","/institucional/aplicativos/oad/directorio/index.php?accion=frmAddPersona2&menu=frmAddPersona&popup=1");
	})
});

$.fn.mostrarIframe = function()
        {
            $("#iframe_regper").css('display','block'); 
        }