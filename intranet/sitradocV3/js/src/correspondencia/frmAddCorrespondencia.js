// JavaScript Document

$(document).ready(function() {
		//$( "#dialog:ui-dialog" ).dialog( "destroy" );
$( "#dialog-confirm" ).dialog({
      resizable: false,
      height:100,
      modal: true,
	  autoOpen:false,
      buttons: {
        "Si": function() {
          $( this ).dialog( "close" );
		  $('#reqRespuesta').val(1);
		  $('#frm_insertaCorrespondencia').submit();
        },
		"No": function() {
          $( this ).dialog( "close" );
		  $('#reqRespuesta').val(2);
		  $('#frm_insertaCorrespondencia').submit();
        },
        Cancel: function() {
          $( this ).dialog( "close" );
		  $('#reqRespuesta').val(0);
        }
      }
    });
	//AGREGANDO ELEMENTOS
	var buscador_destinos='<div id="std_mis_destinos"><div class="std_titulo_popup">Busca destinatarios</div><div><div class="left">Criterios de b&uacute;squeda<input type="text" id="b_destinatario_filtro_text"><button id="b_btn_destinatario_buscar">Buscar</button></div><div class="right"><button id="b_destinatario_agregar" title="Solo se agregaran los registros marcados con check">Agregar a destinatarios</button></div><div class="limpiar"></div></div><br><br><div id="std_div_table"><table id="b_destinatario" style="width:100%;"><thead><tr><th width="1"></th><th>DESTINO</th><th></th><th>DPTO.</th><th></th><th>PROV.</th><th></th><th>DIST.</th><th>DIREC.</th><th><input type="checkbox" id="b_destinatario_seleccionar_todos"></th></tr></thead></table></div></div>';
	var buscador_documentos='<div id="std_mis_documentos"><div class="std_titulo_popup">Busca documentos</div><div><div class="left"><select id="std_cbo_doc"><option value="1">Externo-Expediente</option><option value="2">Interno Pendiente</option><option value="3">Derivados</option></select><br><span class="solo_interno"><select id="i_clase_doc"><option value="0">TODOS</option></select></span><input type="text" id="b_documento_filtro_text">-<select id="i_anio"></select><span class="solo_interno">-MIDIS/<select id="i_dependencia"><option value="0">TODOS</option></select></span><button id="b_btn_documento_buscar">Buscar</button></div><div class="right"><button id="b_documento_agregar" title="Solo se agregaran los registros marcados con check">Agregar a documentos</button></div><div class="limpiar"></div></div><br><br><div id="std_div_table_"><table id="b_documentos"><thead><tr><th width="1"></th><th width="50">DOCUMENTO</th><th width="150">INDICATIVO</th><th width="480">ASUNTO</th><th>FECHA</th><th><input type="checkbox" id="b_documento_seleccionar_todos"></th></tr></thead></table></div></div>';
	$("body").prepend('<div id="std_modal"></div><div id="std_popup"><div class="std_cerrar_popup">x</div>'+buscador_destinos+buscador_documentos+'</div>');	
	
	//LLENADO DE COMBOS PARA EL FORMULARIO DE BUSQUEDA DE DOCUMENTOS
	combos_bdocs();
	function combos_bdocs(){
		//LLENAR COMBO CLASE DOCUMENTO POR DEPENDENCIA DEL USUARIO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_usuario_clase_documentos',
				type:'POST',
				data:{coddep:$("#i_coddep").val()},
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#idClaseDoc").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENAMOS EL COMBO DE FORMAS DE ENVIO
		
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_formas_de_envio',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];  
						$("#formas_envio").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENAMOS EL COMBO DE AMBITO DE ENVIO
		var ServiciosPri=new Array();
		function llenarServicios(data){
			$.each(data, function (index, value) {
    			ServiciosPri.push([value["ID_SERVPRI"], value["SERVICIO"].toString(), value["PRIORIDAD"].toString(), value["ID_SERVICIO"].toString(), value["ID_PRIORIDAD"].toString() ]);
			});
			

				//$("#idAmbito").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
			alert(servicios[0])
			alert(servicios[1])
			alert(servicios[2])
			alert(servicios[3])
			alert(servicios[4])
		}
		
		
		/*$.ajax({  PRUEBA
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_servpri',
				success: function(data){
					llenarServicios(data)
					//for(var i = 0; i < data.length; i++) {
					//	var option = data[i.toString()];  
					//	$("#idAmbito").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					//}   
				}
			});*/
		//LLENAMOS EL COMBO DEPARTAMENTO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_ubigeo&&ubigeo=DEPA',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];  
						$("#departamento_ind").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO CLASE DE DOCUMENTO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_clase_documentos',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_clase_doc").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO AÑOS
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_anios',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_anio").append('<option value="'+option.DESCRIPCION+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO DEPENDENCIA
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_dependencias',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_dependencia").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
	}
	//AL MOMENTO DE SELECCIONAR UN DEPARTAMENTO
	$("#departamento_ind").change(function() {
		$("#provincia_ind option").remove();
		$("#distrito_ind option").remove();
		//LLENA EL COMBO PROVINCIA
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_ubigeo&&ubigeo=PROV',
				data:{ cod_depa:$(this).val()},
				type:'POST',
				success: function(data){
					$("#provincia_ind").append('<option value="999999"></option>')
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#provincia_ind").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		// LLENA EL COMBO DISTRITO SEGUN LA PROVINCIA
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_ubigeo&&ubigeo=DIST',
				data:{ cod_depa:$("#departamento_ind").val(), cod_prov:$("#provincia_ind").val()},
				type:'POST',
				success: function(data){
					
					$("#distrito_ind").append('<option value="999999"></option>');
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#distrito_ind").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}
				}
			});
	});
	//AL MOMENTO DE SELECCIONAR UNA PROVINCIA
	$("#provincia_ind").change(function() {
		$("#distrito_ind option").remove();
		//LLENA EL COMBO DISTRITO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_ubigeo&&ubigeo=DIST',
				data:{ cod_depa:$("#departamento_ind").val(), cod_prov:$("#provincia_ind").val()},
				type:'POST',
				success: function(data){
					$("#distrito_ind").append('<option value="999999"></option>');
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#distrito_ind").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}
				}
			});
	});
	//AL  MOMENTO DE SELECCIONAR AMBITO
/*	$("#idAmbito").change(function() {
			$("#idPrioridad option").remove();
			$.ajax({
				async:false,
				dataType: "json",
				type:'POST',
				data:{idAmbito:$(this).val()},
				url: '/sitradocV3/index.php?accion=correspondencia_combo_prioridad',
				success: function(data){
					if(data.length>1){
						for(var i = 0; i < data.length; i++) {
							var option = data[i.toString()];  
							$("#idPrioridad").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
						} 
					}
				}
			});
	});*/
	//CUANDO EL FORMULARIO DE BUSQUEDA DE DOCUMENTOS ESTE LISTO
	$(".solo_interno").hide();
	$("#std_cbo_doc").change(function(){
		$("#b_documento_filtro_text").val('');
		//if($(this).val()=="1" || $(this).val()=="3"){
		if($(this).val()=="1"){
			$(".solo_interno").hide();
		}
		else{
			$(".solo_interno").show();
		}
		oTabled.fnDraw();
		$("#b_documento_seleccionar_todos").removeAttr('checked');
	})
	//EVENTOS POPUP
	$("body").delegate("#std_modal, .std_cerrar_popup", "click", function() {
	  $("#std_modal, #std_popup").hide();
	});
	//EVENTOS BOTONES
	$("#std_btn_buscar_destinatario").click(function(e){
		e.preventDefault();
		//oTable.fnAdjustColumnSizing();
		$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
		$("#std_mis_documentos").hide();
		//$("#b_destinatario_seleccionar_todos").removeAttr('checked');
	})
	$("input[name='std_vincular']").click(function(){
		if($("input[name='std_vincular']:checked").val()=="1"){
			$("#b_documento_seleccionar_todos").removeAttr('checked');
			oTabled.fnAdjustColumnSizing();
			$("#std_modal, #std_popup, #std_mis_documentos").fadeIn();
			$("#std_mis_destinos").hide();
			$("#solo_vincular").show();
		}
		else{
			$("#t_detalle_documento").html('Cantidad de destinatarios: <strong>0</strong>');
			$("#solo_vincular").hide('slow');
			$("#t_documentos tbody tr").remove();
		}
	})
	//$("input[type=submit]").attr("disabled","disabled");
	/*$("input[type=submit]").click(function(e){
		$("#opt_destinatarios").val($("#body_t_destinatarios tr").size());
	})*/

	//OBJETO DATATABLE DESTINOS
  	var oTable= $('#b_destinatario').dataTable( {
		"bAutoWidth": false,
		"sScrollY": "375px",
	  	"bPaginate": false,
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/sitradocV3/index.php?accion=correspondencia_busqueda_lista_personal",
		"sServerMethod": "POST",
		"bSort": false,
		"bRetrieve": true,
		"fnServerParams": function ( aoData ) {
      	aoData.push( 
				//{ "name": "tipo", "value": $("#b_destinatario_tipo").val() },
				{ "name": "filtro", "value": $("#b_destinatario_filtro_text").val() } 
				);
    	},
		"fnDrawCallback": function( oSettings ) {
			$("#b_destinatario_filter").remove();
		},
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            }
			,"bJQueryUI": true
    } );
	//PARA QUE EL BUSCADOR DE DESTINATARIOS FUNCIONE PRESIONANDO ENTER, LUEGO SE ACTUALIZA EL OBJETO DATATABLE
	 $('#b_destinatario_filtro_text').keyup(function(e) {
		if($(this).val().length>=4){
			if(e.keyCode==13){
				oTable.fnDraw();
			}
			$("#b_destinatario_seleccionar_todos").removeAttr('checked');
		}
	});
		
	//PARA QUE EL BUSCADOR DE DESTINATARIOS FUNCIONE CON EL BOTON BUSCAR
	$("body").delegate("#b_btn_destinatario_buscar","click",function(e){//BOTON BUSCAR MISMO EVENTO DEL ENTER
		e.preventDefault();
		if($('#b_destinatario_filtro_text').val().length >=4){
			oTable.fnDraw();
			$("#b_destinatario_seleccionar_todos").removeAttr('checked');
		}
	})
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON ID "b_destinatario_seleccionar_todos"
	$('body').delegate('#b_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "b_destinatario_check"
	$("body").delegate(".b_destinatario_check","click",function(){
		check_hijo($('#b_destinatario_seleccionar_todos'),$(".b_destinatario_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_destinatario_seleccionar_todos"
	$('body').delegate('#t_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	})		
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_destinatario_check"
	$("body").delegate(".t_destinatario_check","click",function(){
		check_hijo($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	});
	//ACCION QUE SE DESENCADENA AL MOMENTO DE DARLE CLICK EN EL BOTON AGREGAR A DESTINATARIOS
	$("#b_destinatario_agregar").click(function(e){
		e.preventDefault();
		var contador=0;
		var contador_nocheck=0;
		$("#b_destinatario tbody tr").each(function(){
			$(this).children("td:nth-child(10)").each(function(){
				if($(this).children('input').is(':checked')){
					contador=agregar($(this).parent().children("td:nth-child(1)").children("input").val(),$(this).parent().children("td:nth-child(2)").text(),$(this).parent().children("td:nth-child(3)").children("input").val(),$(this).parent().children("td:nth-child(4)").text(),$(this).parent().children("td:nth-child(5)").children("input").val(),$(this).parent().children("td:nth-child(6)").text(),$(this).parent().children("td:nth-child(7)").children("input").val(),$(this).parent().children("td:nth-child(8)").text(),$(this).parent().children("td:nth-child(9)").text())+contador;
					contador_nocheck++;
				}				
			});
		})
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
		if(contador_nocheck==0){
			alert('No ha seleccionado ningun destinatario');
		}
		else{
			alert('Se agrego '+contador+' destinatario(s) a su lista.');
		}
		
	});
	
	
	//OBJETO DATATABLE DOCUMENTOS PARA DERIVAR
  	var oTabled= $('#b_documentos').dataTable( {
		"sPaginationType": "full_numbers",
		"bAutoWidth": false,
		"sScrollY": "320px",
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/sitradocV3/index.php?accion=correspondencia_multiple_busca_documento",
		"sServerMethod": "POST",
		"bSort": false,
		"bRetrieve": true,
		"fnServerParams": function ( aoData ) {
      	aoData.push( 
				{ "name": "coddep",	 	"value":$("#i_coddep").val()						},
				{ "name": "tipo", 		"value":$("#std_cbo_doc").val() },
				{ "name": "clasedoc",	"value":$("#i_clase_doc").val() 					},
				{ "name": "numdoc", 	"value":$("#b_documento_filtro_text").val() 		},
				{ "name": "anio", 		"value":$("#i_anio").val() 							},
				{ "name": "siglas", 	"value":$("#i_dependencia").val() 					}
				);
    	},
		"fnDrawCallback": function( oSettings ) {
			$("#b_documentos_filter").remove();
			$("#b_documentos_paginate span a").click(function(){
			$("#b_documento_seleccionar_todos").removeAttr("checked")
			})
		},
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            },
			"bJQueryUI": true
    } );
	
	 $('#b_documento_filtro_text').keyup(function(e) {
		if(e.keyCode==13){
			oTabled.fnDraw();
		}
		$("#b_documento_seleccionar_todos").removeAttr('checked');
	});
	//PARA QUE EL BUSCADOR DE DOCUMENTOS FUNCIONE CON EL BOTON BUSCAR
	$("body").delegate("#b_btn_documento_buscar","click",function(e){//
		e.preventDefault();
		oTabled.fnDraw();
		$("#b_documento_seleccionar_todos").removeAttr('checked');
	})
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON ID "b_documento_seleccionar_todos"
	$('body').delegate('#b_documento_seleccionar_todos',"click",function(e){
		check_padre($('#b_documento_seleccionar_todos'),$(".b_documento_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "b_documento_check"
	$("body").delegate(".b_documento_check","click",function(){
		check_hijo($('#b_documento_seleccionar_todos'),$(".b_documento_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_documento_seleccionar_todos"
	$('body').delegate('#t_documento_seleccionar_todos',"click",function(e){
		check_padre($('#t_documento_seleccionar_todos'),$(".t_documenento_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_documento_check"
	$("body").delegate(".t_documenento_check","click",function(){
		check_hijo($('#t_documento_seleccionar_todos'),$(".t_documenento_check"))
	});
	
	//ACCION QUE SE DESENCADENA AL MOMENTO DE DARLE CLICK EN EL BOTON AGREGAR A DOCUMENTOS
	$("#b_documento_agregar").click(function(e){
		e.preventDefault();
		var contador=0;
		var contador_nocheck=0;
		$("#b_documentos tbody tr").each(function(){
			$(this).children("td:nth-child(6)").each(function(){
				if($(this).children('input').is(':checked')){
					contador=agregar_docs($(this).parent().children("td:nth-child(1)").children("input").val(),$(this).parent().children("td:nth-child(2)").text(),$(this).parent().children("td:nth-child(3)").html(),$(this).parent().children("td:nth-child(4)").text(),$(this).parent().children("td:nth-child(5)").text())+contador;
					contador_nocheck++;
				}
			});
		})
		$("#t_detalle_documento").html('Cantidad de documentos a vincular: <strong>'+$("#t_documentos tbody tr").size()+'</strong>');
		if(contador_nocheck==0){
			alert('No ha seleccionado ningun documento.');
		}
		else{
			alert('Se agrego '+contador+' documento(s) a su lista para asociar.');
		}
		
	});
	
	//OBJETO DATATABLE DOCUMENTOS PARA DERIVAR

	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}	
	
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_destinatarios) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar(codigo,nombre,cod_dpto,dpto,cod_prov,prov,cod_dist,dist,dir){
		var contador=0;
		var existe=0;
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_destinatarios tbody").prepend('<tr codigo="'+codigo+'" domicilio="0"><td><input type="hidden" value="'+nombre+'" name="destinatario_final[]">'+nombre+'</td><td><input type="hidden" value="'+cod_dpto+'" name="codigo_departamento[]">'+dpto+'</td><td><input type="hidden" value="'+cod_prov+'" name="codigo_provincia[]">'+prov+'</td><td><input type="hidden" value="'+cod_dist+'" name="codigo_distrito[]">'+dist+'</td><td class="border-left"><input type="hidden" value="'+dir+'" name="direccion_addres[]">'+dir+'</td><td class="border-left"><input type="checkbox" class="t_destinatario_check"></td></tr>');
			contador++;
		} 
		return contador;
	}
	//ELIMINARA LOS REGISTROS DE LA LISTA DONDE SE ENCUENTRAN LOS DESTINATARIOS SELECCIONADOS CHEKEADOS
	function eliminar(){
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).children("td:nth-child(6)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_destinatario_seleccionar_todos,.t_destinatario_check").removeAttr('checked');
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
	}
	
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_documentos) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar_docs(codigo,tipodoc,asunto,indicativo,fecha){
		var contador=0;
		var existe=0;
		$("#t_documentos tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_documentos tbody").prepend('<tr codigo="'+codigo+'" title="'+codigo+'"><td class="border-left"><input type="hidden" value="'+codigo+'" name="documentov[]">'+tipodoc+'</td><td class="border-left">'+asunto+'</td><td class="border-left">'+indicativo+'</td><td class="border-left">'+fecha+'</td><td class="border-left"><input type="checkbox" class="t_documenento_check"></td></tr>');
			contador++;
		} 
		return contador;
	}
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR
	$("#t_eliminar_destinatraio").click(function(){
		eliminar()
	});
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR documento
	$("#t_eliminar_documento").click(function(){
		eliminar_doc()
	});
	//ELIMINARA LOS REGISTROS DE LA LISTA DOCUMENTOS DONDE SE ENCUENTRAN LOS DOCS SELECCIONADOS CHEKEADOS
	function eliminar_doc(){
		$("#t_documentos tbody tr").each(function(){	
			if($(this).children("td:nth-child(5)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_documento_seleccionar_todos,.t_documento_check").removeAttr('checked');
		$("#t_detalle_documento").html('Cantidad de documentos a vincular: <strong>'+$("#t_documentos tbody tr").size()+'</strong>');
	}

	//OCULTA MULTIPLE Y MUESTRA INDIVIDUAL
	$("#std_btn_individual").click(function(e){
		e.preventDefault();
		eliminar();//
		$("#individual").show("fast");
	})
	//BOTONES AGREGAR A MI LISTA OCULTAR PANEL(AMBOS INDIVIDUAL)
	var contador_ind=0;
	$("#std_btn_agregar_ind").click(function(e){
		e.preventDefault();
		if($("#destinatario_ind").val()!='' && $("#direccion_ind").val()!='' && $("#departamento_ind").val()!='999999' && $("#provincia_ind").val()!='999999' && $("#distrito_ind").val()!='999999'){
			contador_ind++;
			var codigo_ind=('I0000'+contador_ind).slice(-5);
			agregar(codigo_ind,$("#destinatario_ind").val().toUpperCase(),$("#departamento_ind").val(),$("#departamento_ind option:selected").html(),$("#provincia_ind").val(),$("#provincia_ind option:selected").html(),$("#distrito_ind").val(),$("#distrito_ind option:selected").html(),$("#direccion_ind").val())			
			alert("Se agrego "+$("#destinatario_ind").val()+" a la lista de destinatarios.");
			$("#individual").hide('fast');
			$("#destinatario_ind").val('');
			$("#direccion_ind").val('');
			$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
		}
		else{
			alert('Para poder agregar este destinatario a la lista, es necesario llenar los campos "Destinatario" , "Departamento" , "Provincia" , "Distrito" y "Direccion"');
		}
	})
	$("#std_btn_ocultar_panel_ind").click(function(e){
		e.preventDefault();
		$("#individual").hide('fast');
		$("#destinatario_ind").val('');
		$("#direccion_ind").val('');
	})
	//SUMIT
	//CLICK EN EL BOTON RESPODER
	//$("#frm_responder").submit(function(e){
	$("input[name=paquete]").click(function(){
		validar_sobre_paquete();
	})
	$("#insertar").click(function(e){
		var mensaje='';
		//e.preventDefault();
		$("#idClaseDoc").val($.trim($("#idClaseDoc").val()));
		$("#asunto").val($.trim($("#asunto").val()));
		$("input[name=folio]").val($.trim($("input[name=folio]").val()));
		//if($("#idAmbito").val()==0){mensaje=mensaje+'No ha seleccionado el tipo de servicio.\n';}
		if($("#idPrioridad").val()==0){mensaje=mensaje+'No ha seleccionado la prioridad del servicio.\n';}
		if(($("#idClaseDoc").val()=='none')){mensaje=mensaje+'No ha seleccionado clase de documento\n';}
		if($("#asunto").val()==''){mensaje=mensaje+'No ha escrito asunto.\n';}
		if($("#body_t_destinatarios tr").size()==0){mensaje=mensaje+'No ha seleccionado destinatario(s).\n';}
		if(($("input[name=paquete]:checked").val()==2 && $("input[name=folio]").val()==0)|| $("input[name=folio]").val()==''){mensaje=mensaje+'Por favor escriba la cantidad de folios.\n';}
		if(
			($("#idClaseDoc").val()!='none')
			&&($("#asunto").val()!='')
			&&($("#body_t_destinatarios tr").size()>0)
			//&&($("#idAmbito").val()>0)
			&&($("#idPrioridad").val()>0)
			&&(($("input[name=paquete]:checked").val()==2 && $("input[name=folio]").val()!='' && $("input[name=folio]").val()>0)||($("input[name=paquete]:checked").val()==1 && $("input[name=folio]").val()!=''))
			&&($("#body_t_destinatarios tr").size()>0)
			)
		{	
	
	var existe_ServPri=0;
	var existe_redio=1;
		var departamento = '';
		var provincia = ''; 
		var distrito = ''; 
		$("input[name='codigo_departamento[]']").each( 
			function(){departamento += this.value+',';}
		);
		$("input[name='codigo_provincia[]']").each( 
			function(){provincia += this.value+',';}
		);
		$("input[name='codigo_distrito[]']").each( 
			function(){distrito += this.value+',';}
		);
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=VerificarPrioridad',
				type:'POST',
				data:{idPrioridad:$("#idPrioridad").val(),codigo_departamento:departamento,
						codigo_provincia:provincia,codigo_distrito:distrito},
				success: function(data){
					if(data!='' && (data=='EXITO' || data=='TODOS' || data=='NO TODOS')){
						existe_ServPri=1;
						if(data=='TODOS'){existe_redio=1;}
						if(data=='NO TODOS'){existe_redio=0;}
					}
					else{
						alert("El ubigeo del destinatario esta fuera del alcance de la prioridad.");
						return false;	
					}
				}
			})
		if(existe_ServPri>0){
			if(existe_redio==0){alert("Se cambiar\u00e1 la prioridad a urgente para los distritos diferentes de Lima Metropolitana.");}
/*			if(($("select[name=idPrioridad]").val()>0)&&($("select[name=idPrioridad]").val()==3)){
				alert("Se cambiar\u00e1 la prioridad a urgente para los distritos diferentes de Lima radio urbana.");	
			}*/
		
		$("#opt_destinatarios").val($("#body_t_destinatarios tr").size());	
		var mensaje='';
		//e.preventDefault();
		$("#idClaseDoc").val($.trim($("#idClaseDoc").val()));
		$("#asunto").val($.trim($("#asunto").val()));
		$("input[name=folio]").val($.trim($("input[name=folio]").val()));
		//if($("#idAmbito").val()==0){mensaje=mensaje+'No ha seleccionado el tipo de servicio.\n';}
		if($("#idPrioridad").val()==0){mensaje=mensaje+'No ha seleccionado la prioridad del servicio.\n';}
		if(($("#idClaseDoc").val()=='none')){mensaje=mensaje+'No ha seleccionado clase de documento\n';}
		if($("#asunto").val()==''){mensaje=mensaje+'No ha escrito asunto.\n';}
		if(($("input[name=paquete]:checked").val()==2 && $("input[name=folio]").val()==0)|| $("input[name=folio]").val()==''){mensaje=mensaje+'Por favor escriba la cantidad de folios.\n';}
		if($("#body_t_destinatarios tr").size()==0){mensaje=mensaje+'No ha seleccionado destinatario(s).\n';}
		if(
			($("#idClaseDoc").val()!='none')
			&&($("#asunto").val()!='')
			&&($("#body_t_destinatarios tr").size()>0)
			//&&($("#idAmbito").val()>0)
			&&($("#idPrioridad").val()>0)
			&&(($("input[name=paquete]:checked").val()==2 && $("input[name=folio]").val()!='' && $("input[name=folio]").val()>0)||($("input[name=paquete]:checked").val()==1 && $("input[name=folio]").val()!=''))
			&&($("#body_t_destinatarios tr").size()>0)
			)
		{
			/*
			if(confirm('\u00BFDesea Guardar este Registro?')){
				//$("#frm_insertaCorrespondencia").submit();	
				return true;		}
			else{
				return false;
			}*/
			$( "#dialog-confirm" ).dialog( "open" );
			return false;
				
		}else{alert(mensaje);return false;}
		}
		else{
			return false;
		}
	}
	else{
		alert(mensaje);return false;
		return false;	
	}
	})
	validar_sobre_paquete();
	function validar_sobre_paquete(){
		var seleccionado=$("input[name=paquete]:checked").val();
		if(seleccionado==2){
			if($("#idClaseDoc").val()==71){
				$("#idClaseDoc").val('none');
			}
			$("#idClaseDoc").children('option[value="71"]').hide();
		}
		else{
			$("#idClaseDoc").children('option[value="71"]').show();
		}
	}
});
