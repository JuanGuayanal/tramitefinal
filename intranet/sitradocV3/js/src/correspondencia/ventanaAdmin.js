// JavaScript Document

$(document).ready(function() {
	var guia_admision='<table id="p_Guia_Admision"><tr><td>Gu&iacute;a de Admisi&oacute;n</td><td><input type="text" name="guia_1" /></td></tr><tr><td colspan="2"><button id="btn_generar_admision">Generar Gu&iacute;a de Admisi&oacute;n</button></td></tr></table>';
	var retorno_de_cargo='<table cellpadding="2" cellspacing="4" id="p_Retorno"><tr><td>Fecha retorno cargo</td><td><input type="text" class="datepicker_" maxlength="10" name="FechaRetCargo"/></td></tr><tr><td>Fecha de entrega al destino</td><td><input type="text" class="datepicker_" maxlength="10" name="FechaRecDestino"/></td></tr><tr><td>Gu&iacute;a de Devoluci&oacute;n</td><td><input type="text" name="guia_devolucion"/></td></tr><tr><td>Estado de la Correspondencia</td><td><select name="flag" id="flag"></select> </td></tr><tr class="std_oculto frm_motivo"><td>Motivo</td><td><select name="estadoNot" id="estadoNot"></select></td></tr><tr class="std_oculto frm_observaciones"><td>Observaciones</td><td><textarea name="Observaciones" cols="60" rows="3" id="Observaciones"></textarea></td></tr><tr><td colspan="2"><button id="btn_retorno">Grabar datos de Retorno de Cargo</button></td></tr></table>';
	var detalles='<table id="p_detalles" class="tabla_default texto"><tr><td>Fecha de Creaci&oacute;n:</td><td id="dCre"></td></tr><tr><td>Documento:</td><td id="dDoc"></td></tr><tr><td>N&deg;:</td><td id="dNum"></td></tr><tr><td>Destino:</td><td id="dDestino"></td></tr><tr><td>Ubigeo:</td><td id="dUbigeo"></td></tr><tr><td>Direcci&oacute;n:</td><td id="dDir"></td></tr><tr><td>Recepcionado en OGDA-ME:</td><td id="dRec"></td></tr><tr><td>Prioridad:</td><td id="prioridad"></td></tr><tr><td>Tipo de Servicio:</td><td id="tipoServ"></td></tr><tr><td>Entregado a Courier:</td><td id="dEnt"></td></tr><tr><td>Estado:</td><td id="dEst"></td></tr><tr><td>Fecha Entrega/Visita:</td><td id="dFec"></td></tr><tr><td valign="top">Motivo:</td><td><textarea cols="60" rows="5" readonly="readonly" id="dMot"></textarea></td></tr><tr><td>Retorno de Cargo:</td><td id="dRet"></td></tr><tr><td>Orden de Recepci&oacute;n:</td><td id="dGrec"></td></tr><tr><td>Gu&iacute;a de Admisi&oacute;n:</td><td id="dGadm"></td></tr></table>';
	var entrega_courier='<div id="std_mis_destinos"><div class="std_titulo_popup"></div><br><table id="p_Entrega_Courier"><tr><td>Fecha Entrega a Courier</td><td><input type="text" name="fecha_entrega_courier" class="datepicker_" /></td></tr><tr><td>Orden de Recepci&oacute;n</td><td><input type="text" name="guia_recepcion" /></td></tr><tr><td colspan="2"><button id="btn_entregar_courier"></button></td></tr></table>'+guia_admision+retorno_de_cargo+detalles+'</div>';
	
	$("body").prepend('<div id="std_modal"></div><div id="std_popup"><div class="std_cerrar_popup">x</div>'+entrega_courier+'</div>');
	$("#flag").append($("#id_selFlag").html());
	$("input[name=FechaRetCargo]").val($("#id_dia_defecto").val());
	$("input[name=fecha_entrega_courier]").val($("#id_hoy").val());
	//$("input[name=FechaRecDestino]").val($("#id_dia_defecto").val());
	$("#id_selFlag").parent().remove();
	$( ".datepicker_" ).datepicker({ dateFormat: "dd/mm/yy"})
	//EVENTOS POPUP
	$("body").delegate("#std_modal, .std_cerrar_popup", "click", function() {
	  $("#std_modal, #std_popup").hide();
	});
	$("#btn_aceptar").click(function(e){
		e.preventDefault();
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		$.ajax({
				async:false,
				//dataType: "json",
				//url: '/sitradocV3/index.php?accion=correspondencia_aceptar',
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'ACEPTAR'},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
	})
	$("#btn_devolver").click(function(e){
		e.preventDefault();
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){
			alert("Dede seleccionar una correspondencia.");
		}
		else{
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'DEVOLVER'},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
		}
	})
	$("#btn_enviar_pendientes").click(function(e){
		e.preventDefault();
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){
			alert("Dede seleccionar una correspondencia.");
		}
		else{
		$.ajax({
				async:false,
				//dataType: "json",
				//url: '/sitradocV3/index.php?accion=correspondencia_aceptar',
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'REGRESAR_A_PENDIENTES'},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
		}
	})
	$("#btn_enviar_aceptados").click(function(e){
		e.preventDefault();
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){
			alert("Dede seleccionar una correspondencia.");
		}
		else{
		$.ajax({
				async:false,
				//dataType: "json",
				//url: '/sitradocV3/index.php?accion=correspondencia_aceptar',
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'REGRESAR_A_ACEPTADOS'},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
		}
	})
	$("#abrir_entregar_courier").click(function(e){
		e.preventDefault();
		$(".std_titulo_popup").text('Entrega Courier');
		$("#btn_entregar_courier").text('Entregar al Courier');
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){ alert('Debe seleccionar al menos una correspondencia para entregar al courier.');}
		else{
			$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
			$("#p_Entrega_Courier").show();
			$("#p_Guia_Admision").hide();
			$("#p_Retorno").hide();
			$("#p_detalles").hide();
		}
		ajustar_modal();
	})
	$("#btn_entregar_courier").click(function(e){
		e.preventDefault();
		if($("input[name=fecha_entrega_courier]").val()==''){alert("Debe seleccionar la fecha de env\u00edo.");}
		else{
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=entregaCourier',
				type:'POST',
				data:{ids:query_string,fecha_entrega_courier:$("input[name=fecha_entrega_courier]").val(),guia_recepcion:$("input[name=guia_recepcion]").val()},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
		}
	})
	$("#abrir_mentregar_courier").click(function(e){
		e.preventDefault();
		$(".std_titulo_popup").text('Modificar Entrega Courier');
		$("#btn_entregar_courier").text('Modificar Entrega Courier')
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){ alert('Debe seleccionar al menos una correspondencia para modificar entrega al courier.');}
		else{
			$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
			$("#p_Entrega_Courier").show();
			$("#p_Guia_Admision").hide();
			$("#p_Retorno").hide();
			$("#p_detalles").hide();
		}
		ajustar_modal();
	})
	$("#abrir_guia_admision").click(function(e){
		e.preventDefault();
		$(".std_titulo_popup").text('Agregar Gu\u00eda de Admisi\u00f3n');
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){ alert('Debe seleccionar al menos una correspondencia para poder agregar la guia de admision.');}
		else{
			$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
			$("#p_Entrega_Courier").hide();
			$("#p_Retorno").hide();
			$("#p_Guia_Admision").show();
			$("#p_detalles").hide();
		}
		ajustar_modal();
	})
	$("#btn_generar_admision").click(function(e){
		e.preventDefault();
		if($("input[name=guia_1]").val()==''){alert("Debe escribir el numero de guia.");}
		else{
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		$.ajax({
				async:false,
				//dataType: "json",
				//url: '/sitradocV3/index.php?accion=correspondencia_aceptar',
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'AGREGAR_GUIA1',guia_1:$("input[name=guia_1]").val()},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			})
		}
	})
	$("#abrir_frm_retorno").click(function(e){
		e.preventDefault();
		$(".std_titulo_popup").text('Agregar Retorno de Cargo');
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){ alert('Debe seleccionar al menos una correspondencia para poder agregar datos de retorno del cargo.');}
		else{
			$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
			$("#p_Entrega_Courier").hide();
			$("#p_Retorno").show();
			$("#p_Guia_Admision").hide();
			$("#p_detalles").hide();
		}
		ajustar_modal();
	})
	$("#flag").change(function(){
		$("#estadoNot option").remove();
		if(($(this).val()=='3')||($(this).val()=='4')){
			$.ajax({
					async:false,
					dataType: "json",
					url: '/sitradocv3/index.php?accion=correspondencia_motivo',
					type:'POST',
					data:{flag:$("#flag").val()},
					success: function(data){
						for(var i = 0; i < data.length; i++) {
							var option = data[i.toString()];
							$("#estadoNot").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
							}   
						}
					});
			$(".frm_motivo").css('display','table-row');
			$(".frm_observaciones").css('display','none')
		}
		else{
			$(".frm_motivo").css('display','none');
			if($(this).val()=='2'){
				$(".frm_observaciones").css('display','block')
			}
			else{
				$(".frm_observaciones").css('display','none')
			}
			}
		ajustar_modal();
		})
	$("#estadoNot").change(function(){
		if(($(this).val()=='3')||($(this).val()=='17')){
			$(".frm_observaciones").css('display','table-row')
		}
		else{
			$(".frm_observaciones").css('display','none')
		}
	})
	$("#btn_retorno").click(function(e){
		e.preventDefault();
		mensaje='';
		if($("input[name=FechaRetCargo]").val()==''){mensaje=mensaje+'Debe escribir la fecha de retorno del cargo.\n'}
		if($("#flag").val()=='none'){mensaje=mensaje+'Debe seleccionar estado de la correspondencia.'}
		if(($("input[name=FechaRetCargo]").val()=='') || ($("#flag").val()=='none')){alert(mensaje);}
		else{
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		$.ajax({
				async:false,
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=RetornoCargo',
				type:'POST',
				data:{ids:query_string,FechaRetCargo:$("input[name=FechaRetCargo]").val(),
					flag:$("#flag").val(),estadoNot:$("#estadoNot").val(),
					Observaciones:$("textarea[name=Observaciones]").val(),
					desFechaIni:$("input[name=desFechaIni]").val(),desFechaFin:$("input[name=desFechaFin]").val(),
					FechaRecDestino:$("input[name=FechaRecDestino]").val(),
					guia_devolucion:$("input[name=guia_devolucion]").val()
					},
				success: function(data){
						if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			})
		}
	})
	$("#btn_enviar_entregados").click(function(e){
		e.preventDefault();
		var query_string = ''; 
		$("input[name='ids[]']").each( 
			function() 
			{if (this.checked){query_string += this.value+',';}}
		);
		if(query_string==''){
			alert("Dede seleccionar una correspondencia.");
		}
		else{
		$.ajax({
				async:false,
				//dataType: "json",
				//url: '/sitradocV3/index.php?accion=correspondencia_aceptar',
				url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=CrudCorrespondencia',
				type:'POST',
				data:{ids:query_string,crud:'REGRESAR_A_ENVIADOS'},
				success: function(data){
					if(data=='EXITO'){
					pagina=$("input[name=pagina]").val();
					document.frmBuscar.reset();document.frmBuscar.page.value=pagina;document.frmBuscar.action=document.frmBuscar.action+'?'+'menu=SumarioDir'+'&subMenu=SumarioDir';submitForm('searchNoti');
					}
					else{
						alert('Se produjo un error al momento de realizar la transaccion.');
					}
				}
			});	
		}
	})
	$(".btn_detalle").click(function(e){
		e.preventDefault();
		$("#dCre").text($(this).attr("dCre"));
		$("#dDoc").text($(this).attr("dDoc"));
		$("#dNum").text($(this).attr("dNum"));
		$("#dDestino").text($(this).attr("dDest"));
		$("#dUbigeo").text($(this).attr("dUbi"));
		$("#dDir").text($(this).attr("dDir"));
		$("#dRec").text($(this).attr("dRec"));
		$("#dEnt").text($(this).attr("dEnt"));
		$("#dEst").text($(this).attr("dEst"));
		$("#dFec").text($(this).attr("dFec"));
		$("#dMot").val($(this).attr("dMot"));
		$("#dRet").text($(this).attr("dRet"));
		$("#dGrec").text($(this).attr("dGrec"));
		$("#dGadm").text($(this).attr("dGadm"));
		$("#prioridad").text($(this).attr("prioridad"));
		$("#tipoServ").text($(this).attr("tipoServ"));
		
		$(".std_titulo_popup").text('Detalle de correspondencia');
		$("#std_modal, #std_popup, #std_mis_destinos").fadeIn();
		$("#p_Entrega_Courier").hide();
		$("#p_Retorno").hide();
		$("#p_Guia_Admision").hide();
		$("#p_detalles").show();
		ajustar_modal();
	})
	function ajustar_modal(){
		$(".std_titulo_popup").css('width','auto');
		$("#std_popup").css('height','auto');
		$("#std_popup").css('margin-top','0');
		$("#std_popup").css('width','auto');
		var alto= $("#std_popup").height();
		var ancho= $("#std_popup").width();
		$("#std_popup").css('margin-top',(alto/2)*(-1));
		$("#std_popup").css('margin-left',(ancho/2)*(-1));
	}
});