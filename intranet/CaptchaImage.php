<?php
/******************************************************************
	Captcha for Phoo Gadget in Jaws used in Comments for photos
	Script original de  Wieland E. Kublun < www.kublun.com >
	
	04 / 08 / 2005
	Update, Corregido y Aumentado.
	Erik E. Corona V. www.e-corona.org (ecorona@e-corona.org)
	Si encuentra soporte para fuentes lo hace con un TTF y basado
	en un fondo de pixeles (se ve mas elegante)
*******************************************************************/
session_name("cod_val");
session_start();

//generar los digitos aleatorios
/*for ($i = 0; $i < 5; $i++) {
$numeal[$i] = rand(0,9);
}
$digit = (string) "$numeal[0]$numeal[1]$numeal[2]$numeal[3]$numeal[4]";
*/

$digit = sha1(microtime() * mktime());
$digit = substr($digit,0,5);

$code = sha1($digit);
$code = (string) $code;

// Registra el codigo

$_SESSION["code_sha"] = "$code";

//Tamanio de la imagen a generar
$ancho=70;
$alto=20;

$image = imagecreate($ancho, $alto); // Lets create the image

$white    = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
$black = imagecolorallocate($image,0,0,0);
$gray    = imagecolorallocate($image, 0xC0, 0xC0, 0xC0);
$darkgray = imagecolorallocate($image, 0x50, 0x50, 0x50);

//semilla aleatoria.
srand((double)microtime()*1000000);

//Generar los pixeles de fondo.
//generando 400 pixeles
for ($i = 0; $i < 800; $i++) {
  $xp = rand(0,$ancho);
  $yp = rand(0,$alto);  
  imagesetpixel($image, $xp, $yp, $gray);
}

//verifica que haya soporte para fuentes ttf
$fuentes= false;

//if (function_exists('imagettftext')) 
//	$fuentes = true; 

//cambiar la ruta para fuentes
putenv('GDFONTPATH=' . realpath('./'));
$x=-10;

//Print the digits for image
for ($i = 0; $i < 5; $i++)
{
	if ($fuentes) //si hay soporte de fuentes
	{
		$x = $x + rand(19, 24);
		$y = rand(17 , 18); 
		$angl = rand(-25,25);
		imagettftext($image, 14, $angl, $x, $y, $darkgray, "AcidDreamer", $numeal[$i]);
	}
	else //si no hay soporte.
	{
		$fnt = rand(3,5);
		$x = $x + rand(10 , 15);
		$y = rand(3 , 6);
//	    imagestring($image, 5, $x, $y, $numeal[$i] , $darkgray); 
		imagestring($image, 5, $x, 3, $digit[$i] , $darkgray); 
	}
}

// Crear imagen
header("Content-type: image/png");
imagepng($image);
imagedestroy($image);
?>
