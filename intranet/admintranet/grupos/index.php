<?

// lineas agregadas para deshabilitar este modulo

error_reporting(E_ALL);
include_once('modulosadmin/claseIntranet.inc.php');

$menu = null;
$subMenu = null;
$accion = null;
$id = null;

$objIntranet = new Intranet();

include_once('modulosadmin/claseModulosAdmin.inc.php');
$modAdm = new ModulosAdmin($menu);

if($modAdm){

// fin

$titulo_pag="Inserta Grupo";
$img_titulo_mod="grupos_tit.gif";
include('header.php');
include('armador.php');


?>
<table width="540" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="tree">&nbsp;</td>
  </tr>
  <tr> 
    <td class="tree"><a href="../index.php" class="tree">Administración</a> &gt;&gt; 
      <a href="index.php" class="tree">Grupos</a></td>
  </tr>
  <tr> 
    <td class="tree">&nbsp;</td>
  </tr>
</table>
<?
require('modulosadmin/claseGrupos.inc.php');
$modGrupo = new Grupos;

// Barriendo las Variables GET y POST
if($_POST){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST))
		$$key = $val;
}

if($_GET){
	reset ($_GET);
	while (list ($key, $val) = each ($_GET))
		$$key = $val;
}
	
switch($accion){
	case ACCION_BUSCAR_FALSE:
		$modGrupo->muestraGrupoIndexHTML();
		break;
	case ACCION_BUSCAR_TRUE:
		$modGrupo->muestraGrupoIndexHTML($cod_grupo);
		break;
	case ACCION_INSERTAR_FALSE:
		$modGrupo->insertaGrupoHTML(array(0 => $cod_grupo, 1 => $des_grupo, 2 => $ind_activo),
									$id_dependencia, $id_subdependencia, $cod_usuarios);
		break;
	case ACCION_INSERTAR_TRUE:
		$modGrupo->enviaDatosGrupo($cod_grupo, $des_grupo, $ind_activo, $cod_usuarios);
		break;
	case ACCION_MODIFICAR_FALSE:
		$modGrupo->insertaGrupoHTML(array(0 => $cod_grupo, 1 => $des_grupo, 2 => $ind_activo),
									$id_dependencia, $id_subdependencia, $cod_usuarios, $accion, $id);
		break;
	case ACCION_MODIFICAR_TRUE:
		$modGrupo->enviaDatosGrupo($cod_grupo, $des_grupo, $ind_activo, $cod_usuarios, $id);
		break;
}

// lineas agregadas para deshabilitar este modulo

}else{
        $objIntranet->Header('Administración de Modulos',false,array('suspEmb'));
        $objIntranet->Body('modulosadmin_tit.gif');
        $objIntranet->errors .= '- Usted no tiene los permisos necesarios para utilizar este módulo.';
        Modulos::muestraMensajeInfo($objIntranet->errors);
//        $objIntranet->Footer();
}

//fin


?>
<p>&nbsp;</p>
<?
include('footer.php');
?>
