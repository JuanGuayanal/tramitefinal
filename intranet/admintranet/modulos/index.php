<?
error_reporting(E_ALL);
include_once('modulosadmin/claseIntranet.inc.php');

$menu = null;
$subMenu = null;
$accion = null;
$id = null;

$objIntranet = new Intranet();

if(is_null($menu)&&is_null($subMenu)&&is_null($accion)){
	$menu = (array_key_exists('menu',$_POST)) ? $_POST['menu'] : null;
	$subMenu = (array_key_exists('subMenu',$_POST)) ? $_POST['subMenu'] : null;
	$accion = (array_key_exists('accion',$_POST)) ? $_POST['accion'] : null;
	$id = (array_key_exists('id',$_POST)) ? $_POST['id'] : null;
}
if(is_null($menu)&&is_null($subMenu)&&is_null($accion)){
	$menu = (array_key_exists('menu',$_GET)) ? $_GET['menu'] : null;
	$subMenu = (array_key_exists('subMenu',$_GET)) ? $_GET['subMenu'] : null;
	$accion = (array_key_exists('accion',$_GET)) ? $_GET['accion'] : null;
	$id = (array_key_exists('id',$_GET)) ? $_GET['id'] : null;
}

include_once('modulosadmin/claseModulosAdmin.inc.php');
$modAdm = new ModulosAdmin($menu);

if($modAdm){

	if ($accion!=$modAdm->arr_accion['AGREGA_MODULO']&&$accion!=$modAdm->arr_accion['MODIFICA_MODULO']){
		$objIntranet->Header('Administración de Modulos',false,array('suspEmb'));
		$objIntranet->Body('modulosadmin_tit.gif');
	}
	switch ($accion){
		case $modAdm->arr_accion['FRM_BUSCA_MODULO']:
			$modAdm->FormBuscaModulo();
			break;
		case $modAdm->arr_accion['BUSCA_MODULO']:
			$modAdm->BuscaModulo($_POST['page'],$_POST['idModTipo'],$_POST['desNombre']);
			break;
		case $modAdm->arr_accion['FRM_AGREGA_MODULO']:
			if(count($_POST)>0)
				$modAdm->FrmManipulaModulo(null,$_POST['desTitCor'],$_POST['desTitLar'],$_POST['idModTipo'],$_POST['idModPadre'],$_POST['desModulo'],$_POST['desImagen'],
								   $_POST['desIcono'],$_POST['numOrden'],$_POST['indActivo'],$_POST['desPath'],$_POST['idTarget'],$_POST['desAtribTarget'],$_POST['idDep'],$_POST['idSDep'],(array_key_exists('users',$_POST)) ? $_POST['users'] : null,$_POST['reLoad']);
			else
				$modAdm->FrmManipulaModulo();
			break;
		case $modAdm->arr_accion['AGREGA_MODULO']:
			$modAdm->AgregaModulo($_POST['desTitCor'],$_POST['desTitLar'],$_POST['idModTipo'],$_POST['idModPadre'],$_POST['desModulo'],$_POST['desImagen'],
								   $_POST['desIcono'],$_POST['numOrden'],$_POST['indActivo'],$_POST['desPath'],$_POST['idTarget'],$_POST['desAtribTarget'],(array_key_exists('users',$_POST)) ? $_POST['users'] : null);
			break;
		case $modAdm->arr_accion['FRM_MODIFICA_MODULO']:
			if($id){
				if(count($_POST)>0)
					$modAdm->FrmManipulaModulo($id,$_POST['desTitCor'],$_POST['desTitLar'],$_POST['idModTipo'],$_POST['idModPadre'],$_POST['desModulo'],$_POST['desImagen'],
									   $_POST['desIcono'],$_POST['numOrden'],$_POST['indActivo'],$_POST['desPath'],$_POST['idTarget'],$_POST['desAtribTarget'],$_POST['idDep'],$_POST['idSDep'],(array_key_exists('users',$_POST)) ? $_POST['users'] : null,$_POST['reLoad']);
				else
					$modAdm->FrmManipulaModulo($id);
			}else
				$modAdm->MuestraStatTrans($modAdm->arr_accion['MALA_TRANS']);
			break;
		case $modAdm->arr_accion['MODIFICA_MODULO']:
			if($id){
				$modAdm->ModificaModulo($_POST['id'],$_POST['desTitCor'],$_POST['desTitLar'],$_POST['idModTipo'],$_POST['idModPadre'],$_POST['desModulo'],$_POST['desImagen'],
									   $_POST['desIcono'],$_POST['numOrden'],$_POST['indActivo'],$_POST['desPath'],$_POST['idTarget'],$_POST['desAtribTarget'],(array_key_exists('users',$_POST)) ? $_POST['users'] : null);
			}else{
				$objIntranet->Header('Administración de Modulos',false,array('suspEmb'));
				$objIntranet->Body('modulosadmin_tit.gif');
				$modAdm->MuestraStatTrans($modAdm->arr_accion['MALA_TRANS']);
				$objIntranet->Footer();	
			}
			break;
		case $modAdm->arr_accion['BUENA_TRANS']:
			$modAdm->MuestraStatTrans($accion);
			break;
		case $modAdm->arr_accion['MALA_TRANS']:
			$modAdm->MuestraStatTrans($accion);
			break;
		default:
			$modAdm->MuestraIndex();
			break;
	}
	if ($accion!=$modAdm->arr_accion['AGREGA_MODULO']&&$accion!=$modAdm->arr_accion['MODIFICA_MODULO'])
		$objIntranet->Footer();
}else{
	$objIntranet->Header('Administración de Modulos',false,array('suspEmb'));
	$objIntranet->Body('modulosadmin_tit.gif');
	$objIntranet->errors .= '- Usted no tiene los permisos necesarios para utilizar este módulo.';
	Modulos::muestraMensajeInfo($objIntranet->errors);
	$objIntranet->Footer();	
}

?>
