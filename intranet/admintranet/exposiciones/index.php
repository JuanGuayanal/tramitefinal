<?
$titulo_pag="Inserta Exposicion";
$img_titulo_mod="exposiciones_tit.gif";
include('header.php');
include('armador.php');
?>
<table width="540" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="tree">&nbsp;</td>
  </tr>
  <tr> 
    <td class="tree"><a href="../index.php" class="tree">Miscelaneas</a> &gt;&gt; 
      <a href="index.php" class="tree">Exposiciones</a></td>
  </tr>
  <tr> 
    <td class="tree">&nbsp;</td>
  </tr>
</table>
<?
require('modulosadmin/claseExposiciones.inc.php');
$modExpo = new Exposiciones;

// Barriendo las Variables GET y POST
if($_POST){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST))
		$$key = $val;
}

if($_GET){
	reset ($_GET);
	while (list ($key, $val) = each ($_GET))
		$$key = $val;
}
	
switch($accion){
	case ACCION_BUSCAR_FALSE:
		$modExpo->muestraExposicionIndexHTML();
		break;
	case ACCION_BUSCAR_TRUE:
		$modExpo->muestraExposicionIndexHTML($txt_busca);
		break;
	case ACCION_INSERTAR_FALSE:
		$modExpo->insertaExposicionHTML();
		break;
	case ACCION_INSERTAR_TRUE:
		$modExpo->enviaDatosExposicion($tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $exp_dia, $exp_mes, $exp_anyo, $num_diapositivas, $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar);
		break;
	case ACCION_MODIFICAR_FALSE:
		$modExpo->insertaExposicionHTML($id);
		break;
	case ACCION_MODIFICAR_TRUE:
		$modExpo->enviaDatosExposicion($tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $exp_dia, $exp_mes, $exp_anyo, $num_diapositivas, $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar, $id);
		break;
}

?>
<p>&nbsp;</p>
<?
include('footer.php');
?>