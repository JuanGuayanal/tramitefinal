<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
<div>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td><a href="http://www.php.net/"><img border="0" alt="PHP Logo" src="/phpinfo.php?=PHPE9568F34-D428-11d2-A769-00AA001ACF42" /></a>
          <h1>PHP Version 5.2.6</h1></td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>System </td>
        <td>Windows NT Q-9D1936F3F3734 5.1 build 2600 </td>
      </tr>
      <tr>
        <td>Build Date </td>
        <td>May 2 2008 18:01:20 </td>
      </tr>
      <tr>
        <td>Configure Command </td>
        <td>cscript /nologo configure.js &quot;--enable-snapshot-build&quot;   &quot;--with-gd=shared&quot; &quot;--with-extra-includes=C:\Program Files (x86)\Microsoft   SDK\Include;C:\PROGRA~2\MICROS~2\VC98\ATL\INCLUDE;C:\PROGRA~2\MICROS~2\VC98\INCLUDE;C:\PROGRA~2\MICROS~2\VC98\MFC\INCLUDE&quot;   &quot;--with-extra-libs=C:\Program Files (x86)\Microsoft   SDK\Lib;C:\PROGRA~2\MICROS~2\VC98\LIB;C:\PROGRA~2\MICROS~2\VC98\MFC\LIB&quot; </td>
      </tr>
      <tr>
        <td>Server API </td>
        <td>Apache 2.0 Handler </td>
      </tr>
      <tr>
        <td>Virtual Directory Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Configuration File (php.ini) Path </td>
        <td>C:\WINDOWS </td>
      </tr>
      <tr>
        <td>Loaded Configuration File </td>
        <td>C:\WINDOWS\php.ini </td>
      </tr>
      <tr>
        <td>PHP API </td>
        <td>20041225 </td>
      </tr>
      <tr>
        <td>PHP Extension </td>
        <td>20060613 </td>
      </tr>
      <tr>
        <td>Zend Extension </td>
        <td>220060519 </td>
      </tr>
      <tr>
        <td>Debug Build </td>
        <td>no </td>
      </tr>
      <tr>
        <td>Thread Safety </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Zend Memory Manager </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>IPv6 Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Registered PHP Streams </td>
        <td>php, file, data, http, ftp, compress.zlib </td>
      </tr>
      <tr>
        <td>Registered Stream Socket Transports </td>
        <td>tcp, udp </td>
      </tr>
      <tr>
        <td>Registered Stream Filters </td>
        <td>convert.iconv.*, string.rot13, string.toupper, string.tolower,   string.strip_tags, convert.*, consumed, zlib.* </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td><a href="http://www.zend.com/"><img border="0" alt="Zend logo" src="/phpinfo.php?=PHPE9568F35-D428-11d2-A769-00AA001ACF42" /></a> This program   makes use of the Zend Scripting Language   Engine:<br />
          Zend&nbsp;Engine&nbsp;v2.2.0,&nbsp;Copyright&nbsp;(c)&nbsp;1998-2008&nbsp;Zend&nbsp;Technologies<br /></td>
      </tr>
    </tbody>
  </table>
  <br />
  <hr />
  <h1><a href="/phpinfo.php?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000">PHP   Credits</a></h1>
  <hr />
  <h1>Configuration</h1>
  <h2>PHP Core</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>allow_call_time_pass_reference</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>allow_url_fopen</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>allow_url_include</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>always_populate_raw_post_data</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>arg_separator.input</td>
        <td>&amp;</td>
        <td>&amp;</td>
      </tr>
      <tr>
        <td>arg_separator.output</td>
        <td>&amp;</td>
        <td>&amp;</td>
      </tr>
      <tr>
        <td>asp_tags</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>auto_append_file</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>auto_globals_jit</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>auto_prepend_file</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>browscap</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>default_charset</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>default_mimetype</td>
        <td>text/html</td>
        <td>text/html</td>
      </tr>
      <tr>
        <td>define_syslog_variables</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>disable_classes</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>disable_functions</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>display_errors</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>display_startup_errors</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>doc_root</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>docref_ext</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>docref_root</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>enable_dl</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>error_append_string</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>error_log</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>error_prepend_string</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>error_reporting</td>
        <td>6135</td>
        <td>6135</td>
      </tr>
      <tr>
        <td>expose_php</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>extension_dir</td>
        <td>C:/AppServ\php5\ext</td>
        <td>C:/AppServ\php5\ext</td>
      </tr>
      <tr>
        <td>file_uploads</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>highlight.bg</td>
        <td>#FFFFFF</td>
        <td>#FFFFFF</td>
      </tr>
      <tr>
        <td>highlight.comment</td>
        <td>#FF8000</td>
        <td>#FF8000</td>
      </tr>
      <tr>
        <td>highlight.default</td>
        <td>#0000BB</td>
        <td>#0000BB</td>
      </tr>
      <tr>
        <td>highlight.html</td>
        <td>#000000</td>
        <td>#000000</td>
      </tr>
      <tr>
        <td>highlight.keyword</td>
        <td>#007700</td>
        <td>#007700</td>
      </tr>
      <tr>
        <td>highlight.string</td>
        <td>#DD0000</td>
        <td>#DD0000</td>
      </tr>
      <tr>
        <td>html_errors</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>ignore_repeated_errors</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>ignore_repeated_source</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>ignore_user_abort</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>implicit_flush</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>include_path</td>
        <td>.;C:\php5\pear</td>
        <td>.;C:\php5\pear</td>
      </tr>
      <tr>
        <td>log_errors</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>log_errors_max_len</td>
        <td>1024</td>
        <td>1024</td>
      </tr>
      <tr>
        <td>magic_quotes_gpc</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>magic_quotes_runtime</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>magic_quotes_sybase</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>mail.force_extra_parameters</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>max_execution_time</td>
        <td>30</td>
        <td>30</td>
      </tr>
      <tr>
        <td>max_input_nesting_level</td>
        <td>64</td>
        <td>64</td>
      </tr>
      <tr>
        <td>max_input_time</td>
        <td>60</td>
        <td>60</td>
      </tr>
      <tr>
        <td>memory_limit</td>
        <td>24M</td>
        <td>24M</td>
      </tr>
      <tr>
        <td>open_basedir</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>output_buffering</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>output_handler</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>post_max_size</td>
        <td>80M</td>
        <td>80M</td>
      </tr>
      <tr>
        <td>precision</td>
        <td>12</td>
        <td>12</td>
      </tr>
      <tr>
        <td>realpath_cache_size</td>
        <td>16K</td>
        <td>16K</td>
      </tr>
      <tr>
        <td>realpath_cache_ttl</td>
        <td>120</td>
        <td>120</td>
      </tr>
      <tr>
        <td>register_argc_argv</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>register_globals</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>register_long_arrays</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>report_memleaks</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>report_zend_debug</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>safe_mode</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>safe_mode_exec_dir</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>safe_mode_gid</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>safe_mode_include_dir</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>sendmail_from</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>sendmail_path</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>serialize_precision</td>
        <td>100</td>
        <td>100</td>
      </tr>
      <tr>
        <td>short_open_tag</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>SMTP</td>
        <td>localhost</td>
        <td>localhost</td>
      </tr>
      <tr>
        <td>smtp_port</td>
        <td>25</td>
        <td>25</td>
      </tr>
      <tr>
        <td>sql.safe_mode</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>track_errors</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>unserialize_callback_func</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>upload_max_filesize</td>
        <td>200M</td>
        <td>200M</td>
      </tr>
      <tr>
        <td>upload_tmp_dir</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>user_dir</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>variables_order</td>
        <td>EGPCS</td>
        <td>EGPCS</td>
      </tr>
      <tr>
        <td>xmlrpc_error_number</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>xmlrpc_errors</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>y2k_compliance</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>zend.ze1_compatibility_mode</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_apache2handler" id="module_apache2handler">apache2handler</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Apache Version </td>
        <td>Apache/2.2.8 (Win32) PHP/5.2.6 </td>
      </tr>
      <tr>
        <td>Apache API Version </td>
        <td>20051115 </td>
      </tr>
      <tr>
        <td>Server Administrator </td>
        <td>root@localhost </td>
      </tr>
      <tr>
        <td>Hostname:Port </td>
        <td>localhost:80 </td>
      </tr>
      <tr>
        <td>Max Requests </td>
        <td>Per Child: 0 - Keep Alive: on - Max Per Connection: 100 </td>
      </tr>
      <tr>
        <td>Timeouts </td>
        <td>Connection: 300 - Keep-Alive: 5 </td>
      </tr>
      <tr>
        <td>Virtual Server </td>
        <td>No </td>
      </tr>
      <tr>
        <td>Server Root </td>
        <td>C:/AppServ/Apache2.2 </td>
      </tr>
      <tr>
        <td>Loaded Modules </td>
        <td>core mod_win32 mpm_winnt http_core mod_so mod_actions mod_alias   mod_asis mod_auth_basic mod_authn_default mod_authn_file mod_authz_default   mod_authz_groupfile mod_authz_host mod_authz_user mod_autoindex mod_cgi mod_dir   mod_env mod_imagemap mod_include mod_isapi mod_log_config mod_mime   mod_negotiation mod_setenvif mod_userdir mod_php5 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>engine</td>
        <td>1</td>
        <td>1</td>
      </tr>
      <tr>
        <td>last_modified</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>xbithack</td>
        <td>0</td>
        <td>0</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>Apache Environment</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Variable</th>
        <th>Value</th>
      </tr>
      <tr>
        <td>HTTP_ACCEPT </td>
        <td>*/* </td>
      </tr>
      <tr>
        <td>HTTP_REFERER </td>
        <td>http://localhost/ </td>
      </tr>
      <tr>
        <td>HTTP_ACCEPT_LANGUAGE </td>
        <td>es </td>
      </tr>
      <tr>
        <td>HTTP_USER_AGENT </td>
        <td>Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0) </td>
      </tr>
      <tr>
        <td>HTTP_ACCEPT_ENCODING </td>
        <td>gzip, deflate </td>
      </tr>
      <tr>
        <td>HTTP_HOST </td>
        <td>localhost </td>
      </tr>
      <tr>
        <td>HTTP_CONNECTION </td>
        <td>Keep-Alive </td>
      </tr>
      <tr>
        <td>PATH </td>
        <td>C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem </td>
      </tr>
      <tr>
        <td>SystemRoot </td>
        <td>C:\WINDOWS </td>
      </tr>
      <tr>
        <td>COMSPEC </td>
        <td>C:\WINDOWS\system32\cmd.exe </td>
      </tr>
      <tr>
        <td>PATHEXT </td>
        <td>.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH </td>
      </tr>
      <tr>
        <td>WINDIR </td>
        <td>C:\WINDOWS </td>
      </tr>
      <tr>
        <td>SERVER_SIGNATURE </td>
        <td>&lt;address&gt;Apache/2.2.8 (Win32) PHP/5.2.6 Server at localhost   Port 80&lt;/address&gt; </td>
      </tr>
      <tr>
        <td>SERVER_SOFTWARE </td>
        <td>Apache/2.2.8 (Win32) PHP/5.2.6 </td>
      </tr>
      <tr>
        <td>SERVER_NAME </td>
        <td>localhost </td>
      </tr>
      <tr>
        <td>SERVER_ADDR </td>
        <td>127.0.0.1 </td>
      </tr>
      <tr>
        <td>SERVER_PORT </td>
        <td>80 </td>
      </tr>
      <tr>
        <td>REMOTE_ADDR </td>
        <td>127.0.0.1 </td>
      </tr>
      <tr>
        <td>DOCUMENT_ROOT </td>
        <td>C:/AppServ/www </td>
      </tr>
      <tr>
        <td>SERVER_ADMIN </td>
        <td>root@localhost </td>
      </tr>
      <tr>
        <td>SCRIPT_FILENAME </td>
        <td>C:/AppServ/www/phpinfo.php </td>
      </tr>
      <tr>
        <td>REMOTE_PORT </td>
        <td>1120 </td>
      </tr>
      <tr>
        <td>GATEWAY_INTERFACE </td>
        <td>CGI/1.1 </td>
      </tr>
      <tr>
        <td>SERVER_PROTOCOL </td>
        <td>HTTP/1.1 </td>
      </tr>
      <tr>
        <td>REQUEST_METHOD </td>
        <td>GET </td>
      </tr>
      <tr>
        <td>QUERY_STRING </td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>REQUEST_URI </td>
        <td>/phpinfo.php </td>
      </tr>
      <tr>
        <td>SCRIPT_NAME </td>
        <td>/phpinfo.php </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>HTTP Headers Information</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th colspan="2">HTTP Request Headers</th>
      </tr>
      <tr>
        <td>HTTP Request </td>
        <td>GET /phpinfo.php HTTP/1.1 </td>
      </tr>
      <tr>
        <td>Accept </td>
        <td>*/* </td>
      </tr>
      <tr>
        <td>Referer </td>
        <td>http://localhost/ </td>
      </tr>
      <tr>
        <td>Accept-Language </td>
        <td>es </td>
      </tr>
      <tr>
        <td>User-Agent </td>
        <td>Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0) </td>
      </tr>
      <tr>
        <td>Accept-Encoding </td>
        <td>gzip, deflate </td>
      </tr>
      <tr>
        <td>Host </td>
        <td>localhost </td>
      </tr>
      <tr>
        <td>Connection </td>
        <td>Keep-Alive </td>
      </tr>
      <tr>
        <th colspan="2">HTTP Response Headers</th>
      </tr>
      <tr>
        <td>X-Powered-By </td>
        <td>PHP/5.2.6 </td>
      </tr>
      <tr>
        <td>Keep-Alive </td>
        <td>timeout=5, max=100 </td>
      </tr>
      <tr>
        <td>Connection </td>
        <td>Keep-Alive </td>
      </tr>
      <tr>
        <td>Transfer-Encoding </td>
        <td>chunked </td>
      </tr>
      <tr>
        <td>Content-Type </td>
        <td>text/html </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_bcmath" id="module_bcmath">bcmath</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>BCMath support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_calendar" id="module_calendar">calendar</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Calendar support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_com_dotnet" id="module_com_dotnet">com_dotnet</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>COM support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <th>DCOM support</th>
        <th>disabled</th>
      </tr>
      <tr>
        <th>.Net support</th>
        <th>enabled</th>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>com.allow_dcom</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>com.autoregister_casesensitive</td>
        <td>1</td>
        <td>1</td>
      </tr>
      <tr>
        <td>com.autoregister_typelib</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>com.autoregister_verbose</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>com.code_page</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>com.typelib_file</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_ctype" id="module_ctype">ctype</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>ctype functions </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_curl" id="module_curl">curl</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>cURL support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>cURL Information </td>
        <td>libcurl/7.16.0 OpenSSL/0.9.8g zlib/1.2.3 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_date" id="module_date">date</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>date/time support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>&quot;Olson&quot; Timezone Database Version </td>
        <td>2008.2 </td>
      </tr>
      <tr>
        <td>Timezone Database </td>
        <td>internal </td>
      </tr>
      <tr>
        <td>Default timezone </td>
        <td>America/New_York </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>date.default_latitude</td>
        <td>31.7667</td>
        <td>31.7667</td>
      </tr>
      <tr>
        <td>date.default_longitude</td>
        <td>35.2333</td>
        <td>35.2333</td>
      </tr>
      <tr>
        <td>date.sunrise_zenith</td>
        <td>90.583333</td>
        <td>90.583333</td>
      </tr>
      <tr>
        <td>date.sunset_zenith</td>
        <td>90.583333</td>
        <td>90.583333</td>
      </tr>
      <tr>
        <td>date.timezone</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_dom" id="module_dom">dom</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>DOM/XML </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>DOM/XML API Version </td>
        <td>20031129 </td>
      </tr>
      <tr>
        <td>libxml Version </td>
        <td>2.6.32 </td>
      </tr>
      <tr>
        <td>HTML Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>XPath Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>XPointer Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Schema Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>RelaxNG Support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_filter" id="module_filter">filter</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Input Validation and Filtering </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Revision </td>
        <td>$Revision: 1.52.2.42 $ </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>filter.default</td>
        <td>unsafe_raw</td>
        <td>unsafe_raw</td>
      </tr>
      <tr>
        <td>filter.default_flags</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_ftp" id="module_ftp">ftp</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>FTP support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_gd" id="module_gd">gd</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>GD Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>GD Version </td>
        <td>bundled (2.0.34 compatible) </td>
      </tr>
      <tr>
        <td>FreeType Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>FreeType Linkage </td>
        <td>with freetype </td>
      </tr>
      <tr>
        <td>FreeType Version </td>
        <td>2.1.9 </td>
      </tr>
      <tr>
        <td>T1Lib Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>GIF Read Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>GIF Create Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>JPG Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>PNG Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>WBMP Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>XBM Support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_hash" id="module_hash">hash</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>hash support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Hashing Engines </td>
        <td>md2 md4 md5 sha1 sha256 sha384 sha512 ripemd128 ripemd160 ripemd256   ripemd320 whirlpool tiger128,3 tiger160,3 tiger192,3 tiger128,4 tiger160,4   tiger192,4 snefru gost adler32 crc32 crc32b haval128,3 haval160,3 haval192,3   haval224,3 haval256,3 haval128,4 haval160,4 haval192,4 haval224,4 haval256,4   haval128,5 haval160,5 haval192,5 haval224,5 haval256,5 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_iconv" id="module_iconv">iconv</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>iconv support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>iconv implementation </td>
        <td>&quot;libiconv&quot; </td>
      </tr>
      <tr>
        <td>iconv library version </td>
        <td>1.11 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>iconv.input_encoding</td>
        <td>ISO-8859-1</td>
        <td>ISO-8859-1</td>
      </tr>
      <tr>
        <td>iconv.internal_encoding</td>
        <td>ISO-8859-1</td>
        <td>ISO-8859-1</td>
      </tr>
      <tr>
        <td>iconv.output_encoding</td>
        <td>ISO-8859-1</td>
        <td>ISO-8859-1</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_json" id="module_json">json</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>json support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>json version </td>
        <td>1.2.1 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_libxml" id="module_libxml">libxml</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>libXML support </td>
        <td>active </td>
      </tr>
      <tr>
        <td>libXML Version </td>
        <td>2.6.32 </td>
      </tr>
      <tr>
        <td>libXML streams </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_mbstring" id="module_mbstring">mbstring</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Multibyte Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Multibyte string engine </td>
        <td>libmbfl </td>
      </tr>
      <tr>
        <td>Multibyte (japanese) regex support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Multibyte regex (oniguruma) version </td>
        <td>4.4.4 </td>
      </tr>
      <tr>
        <td>Multibyte regex (oniguruma) backtrack check </td>
        <td>On </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>mbstring extension makes use of &quot;streamable kanji code filter and   converter&quot;, which is distributed under the GNU Lesser General Public License   version 2.1.</th>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>mbstring.detect_order</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mbstring.encoding_translation</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>mbstring.func_overload</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>mbstring.http_input</td>
        <td>pass</td>
        <td>pass</td>
      </tr>
      <tr>
        <td>mbstring.http_output</td>
        <td>pass</td>
        <td>pass</td>
      </tr>
      <tr>
        <td>mbstring.internal_encoding</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mbstring.language</td>
        <td>neutral</td>
        <td>neutral</td>
      </tr>
      <tr>
        <td>mbstring.strict_detection</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>mbstring.substitute_character</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_mysql" id="module_mysql">mysql</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>MySQL Support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Active Persistent Links </td>
        <td>0 </td>
      </tr>
      <tr>
        <td>Active Links </td>
        <td>0 </td>
      </tr>
      <tr>
        <td>Client API version </td>
        <td>5.0.51a </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>mysql.allow_persistent</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>mysql.connect_timeout</td>
        <td>60</td>
        <td>60</td>
      </tr>
      <tr>
        <td>mysql.default_host</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysql.default_password</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysql.default_port</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysql.default_socket</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysql.default_user</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysql.max_links</td>
        <td>Unlimited</td>
        <td>Unlimited</td>
      </tr>
      <tr>
        <td>mysql.max_persistent</td>
        <td>Unlimited</td>
        <td>Unlimited</td>
      </tr>
      <tr>
        <td>mysql.trace_mode</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_mysqli" id="module_mysqli">mysqli</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>MysqlI Support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Client API library version </td>
        <td>5.0.51a </td>
      </tr>
      <tr>
        <td>Client API header version </td>
        <td>5.0.51a </td>
      </tr>
      <tr>
        <td>MYSQLI_SOCKET </td>
        <td>/tmp/mysql.sock </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>mysqli.default_host</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysqli.default_port</td>
        <td>3306</td>
        <td>3306</td>
      </tr>
      <tr>
        <td>mysqli.default_pw</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysqli.default_socket</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysqli.default_user</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>mysqli.max_links</td>
        <td>Unlimited</td>
        <td>Unlimited</td>
      </tr>
      <tr>
        <td>mysqli.reconnect</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_odbc" id="module_odbc">odbc</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>ODBC Support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Active Persistent Links </td>
        <td>0 </td>
      </tr>
      <tr>
        <td>Active Links </td>
        <td>0 </td>
      </tr>
      <tr>
        <td>ODBC library </td>
        <td>Win32 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>odbc.allow_persistent</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>odbc.check_persistent</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>odbc.default_db</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>odbc.default_pw</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>odbc.default_user</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>odbc.defaultbinmode</td>
        <td>return as is</td>
        <td>return as is</td>
      </tr>
      <tr>
        <td>odbc.defaultlrl</td>
        <td>return up to 4096 bytes</td>
        <td>return up to 4096 bytes</td>
      </tr>
      <tr>
        <td>odbc.max_links</td>
        <td>Unlimited</td>
        <td>Unlimited</td>
      </tr>
      <tr>
        <td>odbc.max_persistent</td>
        <td>Unlimited</td>
        <td>Unlimited</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_pcre" id="module_pcre">pcre</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>PCRE (Perl Compatible Regular Expressions) Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>PCRE Library Version </td>
        <td>7.6 2008-01-28 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>pcre.backtrack_limit</td>
        <td>100000</td>
        <td>100000</td>
      </tr>
      <tr>
        <td>pcre.recursion_limit</td>
        <td>100000</td>
        <td>100000</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_PDO" id="module_PDO">PDO</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>PDO support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>PDO drivers </td>
        <td>sqlite, sqlite2 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_pdo_sqlite" id="module_pdo_sqlite">pdo_sqlite</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>PDO Driver for SQLite 3.x</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>PECL Module version </td>
        <td>1.0.1 $Id: pdo_sqlite.c,v 1.10.2.6.2.3 2007/12/31 07:20:10 sebastian   Exp $ </td>
      </tr>
      <tr>
        <td>SQLite Library </td>
        <td>3.3.7undefined </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_Reflection" id="module_Reflection">Reflection</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Reflection</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Version </td>
        <td>$Id: php_reflection.c,v 1.164.2.33.2.50 2008/03/13 15:56:21 iliaa   Exp $ </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_session" id="module_session">session</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Session Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Registered save handlers </td>
        <td>files user sqlite </td>
      </tr>
      <tr>
        <td>Registered serializer handlers </td>
        <td>php php_binary wddx </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>session.auto_start</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>session.bug_compat_42</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>session.bug_compat_warn</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>session.cache_expire</td>
        <td>180</td>
        <td>180</td>
      </tr>
      <tr>
        <td>session.cache_limiter</td>
        <td>nocache</td>
        <td>nocache</td>
      </tr>
      <tr>
        <td>session.cookie_domain</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>session.cookie_httponly</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>session.cookie_lifetime</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>session.cookie_path</td>
        <td>/</td>
        <td>/</td>
      </tr>
      <tr>
        <td>session.cookie_secure</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>session.entropy_file</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>session.entropy_length</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>session.gc_divisor</td>
        <td>100</td>
        <td>100</td>
      </tr>
      <tr>
        <td>session.gc_maxlifetime</td>
        <td>1440</td>
        <td>1440</td>
      </tr>
      <tr>
        <td>session.gc_probability</td>
        <td>1</td>
        <td>1</td>
      </tr>
      <tr>
        <td>session.hash_bits_per_character</td>
        <td>4</td>
        <td>4</td>
      </tr>
      <tr>
        <td>session.hash_function</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>session.name</td>
        <td>PHPSESSID</td>
        <td>PHPSESSID</td>
      </tr>
      <tr>
        <td>session.referer_check</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>session.save_handler</td>
        <td>files</td>
        <td>files</td>
      </tr>
      <tr>
        <td>session.save_path</td>
        <td>C:/DOCUME~1/Cesar/CONFIG~1/Temp</td>
        <td>C:/DOCUME~1/Cesar/CONFIG~1/Temp</td>
      </tr>
      <tr>
        <td>session.serialize_handler</td>
        <td>php</td>
        <td>php</td>
      </tr>
      <tr>
        <td>session.use_cookies</td>
        <td>On</td>
        <td>On</td>
      </tr>
      <tr>
        <td>session.use_only_cookies</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>session.use_trans_sid</td>
        <td>0</td>
        <td>0</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_SimpleXML" id="module_SimpleXML">SimpleXML</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Simplexml support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Revision </td>
        <td>$Revision: 1.151.2.22.2.39 $ </td>
      </tr>
      <tr>
        <td>Schema support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_sockets" id="module_sockets">sockets</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Sockets Support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_SPL" id="module_SPL">SPL</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>SPL support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>Interfaces </td>
        <td>Countable, OuterIterator, RecursiveIterator, SeekableIterator,   SplObserver, SplSubject </td>
      </tr>
      <tr>
        <td>Classes </td>
        <td>AppendIterator, ArrayIterator, ArrayObject,   BadFunctionCallException, BadMethodCallException, CachingIterator,   DirectoryIterator, DomainException, EmptyIterator, FilterIterator,   InfiniteIterator, InvalidArgumentException, IteratorIterator, LengthException,   LimitIterator, LogicException, NoRewindIterator, OutOfBoundsException,   OutOfRangeException, OverflowException, ParentIterator, RangeException,   RecursiveArrayIterator, RecursiveCachingIterator, RecursiveDirectoryIterator,   RecursiveFilterIterator, RecursiveIteratorIterator, RecursiveRegexIterator,   RegexIterator, RuntimeException, SimpleXMLIterator, SplFileInfo, SplFileObject,   SplObjectStorage, SplTempFileObject, UnderflowException,   UnexpectedValueException </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_SQLite" id="module_SQLite">SQLite</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>SQLite support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>PECL Module version </td>
        <td>2.0-dev $Id: sqlite.c,v 1.166.2.13.2.10 2007/12/31 07:20:11   sebastian Exp $ </td>
      </tr>
      <tr>
        <td>SQLite Library </td>
        <td>2.8.17 </td>
      </tr>
      <tr>
        <td>SQLite Encoding </td>
        <td>iso8859 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>sqlite.assoc_case</td>
        <td>0</td>
        <td>0</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_standard" id="module_standard">standard</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Regex Library </td>
        <td>Bundled library enabled </td>
      </tr>
      <tr>
        <td>Dynamic Library Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Internal Sendmail Support for Windows </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>assert.active</td>
        <td>1</td>
        <td>1</td>
      </tr>
      <tr>
        <td>assert.bail</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>assert.callback</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>assert.quiet_eval</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>assert.warning</td>
        <td>1</td>
        <td>1</td>
      </tr>
      <tr>
        <td>auto_detect_line_endings</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>default_socket_timeout</td>
        <td>60</td>
        <td>60</td>
      </tr>
      <tr>
        <td>safe_mode_allowed_env_vars</td>
        <td>PHP_</td>
        <td>PHP_</td>
      </tr>
      <tr>
        <td>safe_mode_protected_env_vars</td>
        <td>LD_LIBRARY_PATH</td>
        <td>LD_LIBRARY_PATH</td>
      </tr>
      <tr>
        <td>url_rewriter.tags</td>
        <td>a=href,area=href,frame=src,input=src,form=,fieldset=</td>
        <td>a=href,area=href,frame=src,input=src,form=,fieldset=</td>
      </tr>
      <tr>
        <td>user_agent</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_tokenizer" id="module_tokenizer">tokenizer</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>Tokenizer Support </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_wddx" id="module_wddx">wddx</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>WDDX Support</th>
        <th>enabled</th>
      </tr>
      <tr>
        <td>WDDX Session Serializer </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_xml" id="module_xml">xml</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>XML Support </td>
        <td>active </td>
      </tr>
      <tr>
        <td>XML Namespace Support </td>
        <td>active </td>
      </tr>
      <tr>
        <td>libxml2 Version </td>
        <td>2.6.32 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_xmlreader" id="module_xmlreader">xmlreader</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>XMLReader </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_xmlwriter" id="module_xmlwriter">xmlwriter</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>XMLWriter </td>
        <td>enabled </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2><a name="module_zlib" id="module_zlib">zlib</a></h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td>ZLib Support </td>
        <td>enabled </td>
      </tr>
      <tr>
        <td>Stream Wrapper support </td>
        <td>compress.zlib:// </td>
      </tr>
      <tr>
        <td>Stream Filter support </td>
        <td>zlib.inflate, zlib.deflate </td>
      </tr>
      <tr>
        <td>Compiled Version </td>
        <td>1.2.3 </td>
      </tr>
      <tr>
        <td>Linked Version </td>
        <td>1.2.3 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Directive</th>
        <th>Local Value</th>
        <th>Master Value</th>
      </tr>
      <tr>
        <td>zlib.output_compression</td>
        <td>Off</td>
        <td>Off</td>
      </tr>
      <tr>
        <td>zlib.output_compression_level</td>
        <td>-1</td>
        <td>-1</td>
      </tr>
      <tr>
        <td>zlib.output_handler</td>
        <td><em>no value</em></td>
        <td><em>no value</em></td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>Additional Modules</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Module Name</th>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>Environment</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Variable</th>
        <th>Value</th>
      </tr>
      <tr>
        <td>ALLUSERSPROFILE </td>
        <td>C:\Documents and Settings\All Users </td>
      </tr>
      <tr>
        <td>CommonProgramFiles </td>
        <td>C:\Archivos de programa\Archivos comunes </td>
      </tr>
      <tr>
        <td>COMPUTERNAME </td>
        <td>Q-9D1936F3F3734 </td>
      </tr>
      <tr>
        <td>ComSpec </td>
        <td>C:\WINDOWS\system32\cmd.exe </td>
      </tr>
      <tr>
        <td>FP_NO_HOST_CHECK </td>
        <td>NO </td>
      </tr>
      <tr>
        <td>NUMBER_OF_PROCESSORS </td>
        <td>1 </td>
      </tr>
      <tr>
        <td>OS </td>
        <td>Windows_NT </td>
      </tr>
      <tr>
        <td>Path </td>
        <td>C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem </td>
      </tr>
      <tr>
        <td>PATHEXT </td>
        <td>.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH </td>
      </tr>
      <tr>
        <td>PROCESSOR_ARCHITECTURE </td>
        <td>x86 </td>
      </tr>
      <tr>
        <td>PROCESSOR_IDENTIFIER </td>
        <td>x86 Family 6 Model 42 Stepping 7, GenuineIntel </td>
      </tr>
      <tr>
        <td>PROCESSOR_LEVEL </td>
        <td>6 </td>
      </tr>
      <tr>
        <td>PROCESSOR_REVISION </td>
        <td>2a07 </td>
      </tr>
      <tr>
        <td>ProgramFiles </td>
        <td>C:\Archivos de programa </td>
      </tr>
      <tr>
        <td>SystemDrive </td>
        <td>C: </td>
      </tr>
      <tr>
        <td>SystemRoot </td>
        <td>C:\WINDOWS </td>
      </tr>
      <tr>
        <td>TEMP </td>
        <td>C:\WINDOWS\TEMP </td>
      </tr>
      <tr>
        <td>TMP </td>
        <td>C:\WINDOWS\TEMP </td>
      </tr>
      <tr>
        <td>USERPROFILE </td>
        <td>C:\Documents and Settings\LocalService </td>
      </tr>
      <tr>
        <td>windir </td>
        <td>C:\WINDOWS </td>
      </tr>
      <tr>
        <td>AP_PARENT_PID </td>
        <td>1068 </td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>PHP Variables</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <th>Variable</th>
        <th>Value</th>
      </tr>
      <tr>
        <td>PHP_SELF </td>
        <td>/phpinfo.php </td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_ACCEPT&quot;]</td>
        <td>*/*</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_REFERER&quot;]</td>
        <td>http://localhost/</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_ACCEPT_LANGUAGE&quot;]</td>
        <td>es</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_USER_AGENT&quot;]</td>
        <td>Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1;   Trident/4.0)</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_ACCEPT_ENCODING&quot;]</td>
        <td>gzip, deflate</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_HOST&quot;]</td>
        <td>localhost</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;HTTP_CONNECTION&quot;]</td>
        <td>Keep-Alive</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;PATH&quot;]</td>
        <td>C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SystemRoot&quot;]</td>
        <td>C:\WINDOWS</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;COMSPEC&quot;]</td>
        <td>C:\WINDOWS\system32\cmd.exe</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;PATHEXT&quot;]</td>
        <td>.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;WINDIR&quot;]</td>
        <td>C:\WINDOWS</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_SIGNATURE&quot;]</td>
        <td>&lt;address&gt;Apache/2.2.8 (Win32) PHP/5.2.6 Server at localhost   Port 80&lt;/address&gt; </td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_SOFTWARE&quot;]</td>
        <td>Apache/2.2.8 (Win32) PHP/5.2.6</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_NAME&quot;]</td>
        <td>localhost</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_ADDR&quot;]</td>
        <td>127.0.0.1</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_PORT&quot;]</td>
        <td>80</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;REMOTE_ADDR&quot;]</td>
        <td>127.0.0.1</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;DOCUMENT_ROOT&quot;]</td>
        <td>C:/AppServ/www</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_ADMIN&quot;]</td>
        <td>root@localhost</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SCRIPT_FILENAME&quot;]</td>
        <td>C:/AppServ/www/phpinfo.php</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;REMOTE_PORT&quot;]</td>
        <td>1120</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;GATEWAY_INTERFACE&quot;]</td>
        <td>CGI/1.1</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SERVER_PROTOCOL&quot;]</td>
        <td>HTTP/1.1</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;REQUEST_METHOD&quot;]</td>
        <td>GET</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;QUERY_STRING&quot;]</td>
        <td><em>no value</em></td>
      </tr>
      <tr>
        <td>_SERVER[&quot;REQUEST_URI&quot;]</td>
        <td>/phpinfo.php</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;SCRIPT_NAME&quot;]</td>
        <td>/phpinfo.php</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;PHP_SELF&quot;]</td>
        <td>/phpinfo.php</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;REQUEST_TIME&quot;]</td>
        <td>1338612114</td>
      </tr>
      <tr>
        <td>_SERVER[&quot;argv&quot;]</td>
        <td><pre>Array  (  )  </pre></td>
      </tr>
      <tr>
        <td>_SERVER[&quot;argc&quot;]</td>
        <td>0</td>
      </tr>
      <tr>
        <td>_ENV[&quot;ALLUSERSPROFILE&quot;]</td>
        <td>C:\Documents and Settings\All Users</td>
      </tr>
      <tr>
        <td>_ENV[&quot;CommonProgramFiles&quot;]</td>
        <td>C:\Archivos de programa\Archivos comunes</td>
      </tr>
      <tr>
        <td>_ENV[&quot;COMPUTERNAME&quot;]</td>
        <td>Q-9D1936F3F3734</td>
      </tr>
      <tr>
        <td>_ENV[&quot;ComSpec&quot;]</td>
        <td>C:\WINDOWS\system32\cmd.exe</td>
      </tr>
      <tr>
        <td>_ENV[&quot;FP_NO_HOST_CHECK&quot;]</td>
        <td>NO</td>
      </tr>
      <tr>
        <td>_ENV[&quot;NUMBER_OF_PROCESSORS&quot;]</td>
        <td>1</td>
      </tr>
      <tr>
        <td>_ENV[&quot;OS&quot;]</td>
        <td>Windows_NT</td>
      </tr>
      <tr>
        <td>_ENV[&quot;Path&quot;]</td>
        <td>C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem</td>
      </tr>
      <tr>
        <td>_ENV[&quot;PATHEXT&quot;]</td>
        <td>.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH</td>
      </tr>
      <tr>
        <td>_ENV[&quot;PROCESSOR_ARCHITECTURE&quot;]</td>
        <td>x86</td>
      </tr>
      <tr>
        <td>_ENV[&quot;PROCESSOR_IDENTIFIER&quot;]</td>
        <td>x86 Family 6 Model 42 Stepping 7, GenuineIntel</td>
      </tr>
      <tr>
        <td>_ENV[&quot;PROCESSOR_LEVEL&quot;]</td>
        <td>6</td>
      </tr>
      <tr>
        <td>_ENV[&quot;PROCESSOR_REVISION&quot;]</td>
        <td>2a07</td>
      </tr>
      <tr>
        <td>_ENV[&quot;ProgramFiles&quot;]</td>
        <td>C:\Archivos de programa</td>
      </tr>
      <tr>
        <td>_ENV[&quot;SystemDrive&quot;]</td>
        <td>C:</td>
      </tr>
      <tr>
        <td>_ENV[&quot;SystemRoot&quot;]</td>
        <td>C:\WINDOWS</td>
      </tr>
      <tr>
        <td>_ENV[&quot;TEMP&quot;]</td>
        <td>C:\WINDOWS\TEMP</td>
      </tr>
      <tr>
        <td>_ENV[&quot;TMP&quot;]</td>
        <td>C:\WINDOWS\TEMP</td>
      </tr>
      <tr>
        <td>_ENV[&quot;USERPROFILE&quot;]</td>
        <td>C:\Documents and Settings\LocalService</td>
      </tr>
      <tr>
        <td>_ENV[&quot;windir&quot;]</td>
        <td>C:\WINDOWS</td>
      </tr>
      <tr>
        <td>_ENV[&quot;AP_PARENT_PID&quot;]</td>
        <td>1068</td>
      </tr>
    </tbody>
  </table>
  <br />
  <h2>PHP License</h2>
  <table border="0" cellpadding="3" width="600">
    <tbody>
      <tr>
        <td><p>This program is free software; you can redistribute it and/or modify it under   the terms of the PHP License as published by the PHP Group and included in the   distribution in the file: LICENSE </p>
          <p>This program is distributed in the hope that it will be useful, but WITHOUT   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS   FOR A PARTICULAR PURPOSE. </p>
          <p>If you did not receive a copy of the PHP license, or have any questions about   PHP licensing, please contact license@php.net. </p></td>
      </tr>
    </tbody>
  </table>
  <br />
</div>
</body>
</html>