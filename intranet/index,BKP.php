<?
require('auth.php');
include('modulosadmin/claseAutenticacionUsuarioExterno.inc.php');

$autenticacion=new AutenticacionUsuarioExterno;

if ($autenticacion->checkIP()){
	session_name("cod_val");
	session_start();
	$_POST['code']=$_SESSION["code_sha"];
	session_destroy();
	session_unset();
	unset($_SESSION);
}

begin_session();

if(status_secure()){
	header("Location: $INDEX_PAGE");
	exit;
}else{
	if($_POST){
		$username = $_POST['uname'];
		$userpass = $_POST['upass'];
	
		if ($autenticacion->checkIP()){			
			if($autenticacion->validaIntento()){
				if(autentifica()){
					// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
					$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
					header("Location: $destination");
					exit;
				}
			}
		}else{
			if(autentifica()){
				// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
				$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
				header("Location: $destination");
				exit;
			}
		}	
	}
}
session_name("cod_val");
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - Ministerio de la Producci�n</title>
<link href="/estilos/3/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/REG.css" rel="stylesheet" type="text/css" />
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/login_jquery.js"></script>
<script language="JavaScript" type="text/javascript">
<!--

function setFocus()
{
    document.frmLogin.uname.focus();
}

function submit_login()
{
    if (document.frmLogin.uname.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.uname.focus();
        return false;
    } else if (document.frmLogin.upass.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.upass.focus();
        return false;
    } else if (document.frmLogin.codigo.value == "") {
        alert('Ingrese su C�digo de Acceso');
        document.frmLogin.codigo.focus();
        return false;
	} else if (document.frmLogin.answer.value == "") {
        alert('Ingrese su Datos de Autenticaci�n');
        document.frmLogin.answer.focus();
        return false;
	} else if (document.frmLogin.cod_val.value == "") {
        alert('Ingrese el C�digo Autenticaci�n de la Imagen');
        document.frmLogin.cod_val.focus();
        return false;
    } else {
        return true;
    }
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 104px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #007d94;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body onLoad="document.forms.frmLogin.uname.focus()" class="WEB_MAQUE_body">
<div id="Layer1" style="position:absolute; left:0px; top:515px; width:100%; height:44px; z-index:1">
  <div align="center"><span class="WEB_CONTE_creditosLogin GEN_horizontalCenter"><span class="WEB_textoCreditosLogin">Calle Uno Oeste N&deg; 060 - Urb. C&oacute;rpac, San Isidro - Lima - Per&uacute;<br />
  RUC: 20504794637 - Central Telef&oacute;nica: 616-2222<br />
  <a href="http://www.CONVENIO_SITRADOC.gob.pe/portal/portal/apsportalCONVENIO_SITRADOC/internaCONVENIO_SITRADOC?ARE=1&JER=686" target="_blank">Acerca de la Intranet</a> - <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">intranet@CONVENIO_SITRADOC.gob.pe</a>  </span></span></div>
</div>
<div align="center">
<table width="600" height="387" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td width="362"><table width="362"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="362" valign="top"><table id="Table_01" width="362" height="387" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><img src="https:///images/0/0/intro_intranet_22.gif" alt="Cont&aacute;ctate con nosotros aqu&iacute;" width="362" height="387" border="0" usemap="#Map" href="mailto:intranet@CONVENIO_SITRADOC.gob.pe" /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td height="387" valign="top">	 
<div class="WEB_MAQUE_Alineacion3">
	<div class="WEB_MAQUE_Fondo5"><img src="https:///images/0/0/imagen_titulo_home.gif" alt="" width="387" height="22" /></div>
<div class="WEB_MAQUE_Fondo6">
    	<div class="WEB_MAQUE_Fondo7">
    		<div class="WEB_CONTE_bloqueModulos1">
                <div class="WEB_CONTE_bloqueSombra" id="ID_login">
            	
                    <div class="WEB_CONTE_grupoSombraArriba">
                      <div class="WEB_CONTE_sombra1"></div>
                      	<div class="WEB_CONTE_sombraHorizontalArriba"></div>
                        <div class="WEB_CONTE_sombra2"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraMedio">
                      <div class="WEB_CONTE_sombraVerticalIzquierdo"></div>
                        <div class="WEB_CONTE_bloqueEspaciado">
                            <div class="WEB_CONTE_bloqueFondo">
                                <div class="WEB_CONTE_bloqueEspaciadoCabecera">
                                    <div class="WEB_CONTE_bloqueCabecera">
                                        <div class="WEB_CONTE_bloqueCabeceraParte1 GEN_floatLeft"></div>
                                                <div class="WEB_CONTE_bloqueCabeceraParte2 GEN_floatLeft"><img src="https:///images/0/0/titulo_login.jpg" alt="" width="117" height="13" class="WEB_ICONO_login" /></div>
                                      <div class="WEB_CONTE_bloqueCabeceraParte3 GEN_floatRight"></div>
                                            <div class="GEN_clearBoth"></div>
                                    </div>
                              	</div>
                                <div class="WEB_CONTE_sombraContenido">
                                	<div class="WEB_CONTE_login">
                                    	<div class="REG_CONTE_moduloLogin">
                                        	<form <?php echo $_SERVER['PHP_SELF']; ?>  method="post" name="frmLogin" class="REG_CONTE_form1">
                                        		<div class="REG_CONTE_bloqueLogin1">
                                            		<div class="REG_CONTE_loginImagen GEN_floatLeft"><img src="https:///images/0/0/imagen_login.jpg" alt="" width="96" height="98" /></div>
                                                    <div class="REG_CONTE_loginFormulario GEN_floatLeft">
                                                    	<fieldset class="REG_CONTE_fieldset2">
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="email" class="REG_CONTE_label2">Usuario :</label>
																<input id="uname" type="text" name="uname" value="<?php echo $_POST['uname'];?>"  class="REG_CONTE_input3">
																<div align="left" class="Estilo1">&gt;&gt; Direcci&oacute;n de correo sin "@CONVENIO_SITRADOC.gob.pe".</div>
                                                                <br />
                                                            </div>
                                                        
                                                        	<!-- <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Tiene Login?	 :</label>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">No</span>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">Si</span>
                                                                <br />
                                                            </div>
                                                        
                                                                <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Login :</label>
                                                                <input id="email_amigo" name="email_amigo" class="REG_CONTE_input3">
                                                                <br />
                                                            </div>-->
                                                        
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Contrase&ntilde;a :</label>
                                                                <input name="upass" type="password" maxlength="30" id="upass" class="REG_CONTE_input3">
																<div align="left" class="Estilo1">&gt;&gt; La misma de su correo electr&oacute;nico. </div>
                                                                
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                <div class="GEN_clearLeft"></div>
                                            	</div>
                                            	<div class="REG_CONTE_bloqueLogin2">
                                                	<div class="REG_CONTE_mensajeLogin GEN_floatLeft GEN_horizontalCenter">
                                                	  <p class="REG_texto1">
													  <!-- Texto Error --> &nbsp;
													  </p>
                                                	</div>
                                                    <div class="REG_CONTE_digitosLogin GEN_floatLeft">
                                                   	  <div class="REG_CONTE_bloqueDigito">
                                                            <br class="GEN_clearLeft" />
                                                      </div>
                                                        <div class="REG_CONTE_boton1">
                                                        	  <input type="submit" name="button" id="button" value="Ingresar" class="REG_boton1" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                        <div class="WEB_CONTE_sombraVerticalDerecho"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraAbajo">
                        <div class="WEB_CONTE_sombra3"></div>
                        <div class="WEB_CONTE_sombraHorizontalAbajo"></div>
                        <div class="WEB_CONTE_sombra4"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
              </div>
          	</div>
        </div>
    </div>
</div>
<div class="WEB_CONTE_creditosLogin GEN_horizontalCenter"></div>
	</td>
  </tr>
</table>
</div>
<map name="Map" id="Map">
  <area shape="rect" coords="275,54,356,77" href="http://www.CONVENIO_SITRADOC.gob.pe/" target="_blank" alt="Ir al Portal" />
  <area shape="rect" coords="271,344,350,364" href="mailto:intranet@CONVENIO_SITRADOC.gob.pe" target="_blank" alt="Cont&aacute;ctate con nosotros aqu&iacute;" />
</map>
</body>
</html>
<?
session_destroy(); 
?>