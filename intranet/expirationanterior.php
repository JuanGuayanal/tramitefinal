<?
require('auth.php');
begin_session();
if(status_secure()){
	header("Location: $INDEX_PAGE");
	exit;
}else{
	if($_POST){
		$username = $_POST['uname'];
		$userpass = $_POST['upass'];
		if(autentifica()){
			// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
			$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
			header("Location: $destination");
			exit;
		}
	}
?>
<html>
<head>
<title>Intranet SITRADOC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<script language="JavaScript" type="text/javascript">
<!--

function setFocus()
{
    document.frmLogin.uname.focus();
}

function submit_login()
{
    if (document.frmLogin.uname.value == "") {
        alert('Ingrese su nombre de usuario y contraseņa');
        document.frmLogin.uname.focus();
        return false;
    } else if (document.frmLogin.upass.value == "") {
        alert('Ingrese su nombre de usuario y contraseņa');
        document.frmLogin.upass.focus();
        return false;
    } else {
        return true;
    }
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="setFocus()">
<table width="779" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="779"><img src="/img/800x600/cabecera_ext.jpg" width="779" height="86"></td>
  </tr>
  <tr>
    <td valign="top"> 
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="280" align="right" valign="middle" class="bdr-up" height="360"><img src="img/800x600/img_bienvenido_new2.jpg"> 
          </td>
          <td width="250" align="center" valign="top"> 
            <table width="450" border="0" cellspacing="0" cellpadding="4">
                <tr>
                  <td width="20" class="td-tit-login">&nbsp;</td>
                  <td class="td-tit-login"><b>Mensaje:</b></td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td class="texto"><b class="sub-item">El tiempo de inactividad de la sesi&oacute;n de usuario ha sobrepasado el limite permitido (30 minutos). Favor autenticarse para ingresar nuevamente a la Intranet.</b> </td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="20">&nbsp;</td>
                  <td align="center"><form name="form1" method="post" action="index.php">
                      <input type="submit" name="Submit" value="Reingresar" class="but-login">
                  </form></td>
                </tr>
              </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<?
}
?>
