/**
* funcion para saber cuantos dias tiene cada mes
*/
function cuantosDias(mes, anyo){
	var cuantosDias = 31;
	if (mes == "Abril" || mes == "Junio" || mes == "Septiembre" || mes == "Noviembre")
  cuantosDias = 30;
	if (mes == "Febrero" && (anyo/4) != Math.floor(anyo/4))
  cuantosDias = 28;
	if (mes == "Febrero" && (anyo/4) == Math.floor(anyo/4))
  cuantosDias = 29;
	return cuantosDias;
}

/**
* una vez que sabemos cuantos dias tiene cada mes
* asignamos dinamicamente este numero al combo de los dias dependiendo 
* del mes que aparezca en el combo de los meses
*/
function asignaDias(comboDias, comboMeses, Anyo){

	Month = comboMeses[comboMeses.selectedIndex].text;
	Year = Anyo;

	diasEnMes = cuantosDias(Month, Year);
	diasAhora = comboDias.length;

	if (diasAhora > diasEnMes){
		for (i=0; i<(diasAhora-diasEnMes); i++){
			comboDias.options[comboDias.options.length - 1] = null
		}
	}
	if (diasEnMes > diasAhora){
		for (i=0; i<(diasEnMes-diasAhora); i++){
			sumaOpcion = new Option(comboDias.options.length + 1);
			comboDias.options[comboDias.options.length]=sumaOpcion;
		}
	}
	if (comboDias.selectedIndex < 0) 
	  comboDias.selectedIndex = 0;
}

function stat_encuesta(inForm){
	var checkedButton = 0;
	for (var i = 0; i < inForm.idoption.length; i++){
		if (inForm.idoption[i].checked==true) {
			checkedButton=inForm.idoption[i].value;
		}
	}
	if(checkedButton<=0){
		alert("Ud. debe de seleccionar al menos una opci�n de la encuesta");
		return false;
	}else{
		return true;
	}
}

<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;
  if(!d)
    d=document;
  if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document;
	n=n.substring(0,p);
  }
  if(!(x=d[n])&&d.all)
	x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++)
    x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++)
    x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById)
    x=document.getElementById(n);
  return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) {
    test=args[i+2];
	if (test.indexOf('Date')!=-1 || test.indexOf('Hour')!=-1 || test.indexOf('Sel')!=-1 || test.indexOf('date')!=-1 || test.indexOf('hour')!=-1) {
	  var cbxs = new Array;
	  var vals = new Array;
	  cbxs = args[i].split(",");
	  for(j=0;j<cbxs.length;j++)
		vals[j]=MM_findObj(cbxs[j]);
	  for(j=0;j<vals.length;j++){
	  	if((test.indexOf('datetime')!=-1 && j==10) || (test.indexOf('Sel')!=-1 && j==1))
		  vals[j] = vals[j].checked;
		else
	      vals[j] = (vals[j].length > 0 && vals[j].selectedIndex!=-1) ? vals[j][vals[j].selectedIndex].value : false;
	  }
	  val = true
	}else
	  val=MM_findObj(args[i]);

    if (val) {
	  nm=args[i+1];
	  if (test.indexOf('Sel')!=-1){
	   	if(vals.length>1){
		  if(!vals[1]){
			if (vals[0] == 'NULL' || vals[0] == 'none' || !vals[0])
			  errors+='- '+nm+' debe contener un valor.\n';
		  }
		}else
		  if (vals[0] == 'NULL' || vals[0] == 'none' || !vals[0])
			errors+='- '+nm+' debe contener un valor.\n';
	  }else if (test.indexOf('Rad')!=-1){
	    for(var j = 0; j < val.length; j++)
		  if(val[j].checked){
		    var radCheck = true
			break
		  }
	  	if(!radCheck)
		  errors+='- '+nm+' debe selecionar un valor.\n';
	  }else if (test.indexOf('Date')!=-1){
	    if ((vals[0] == 'NULL' || vals[0] == 'none') || (vals[1] == 'NULL' || vals[1] == 'none') || (vals[2] == 'NULL' || vals[2] == 'none') )
		  errors+='- '+nm+' debe contener una fecha correcta.\n';
	  }else if (test.indexOf('dateMen')!=-1){
	    var fecIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]));
	    var fecFin = new Date(Number(vals[5]),Number(vals[4])-1,Number(vals[3]));
	    if (fecIni >= fecFin)
		  errors+='- '+nm+'\n';
	  }else if (test.indexOf('Hour')!=-1) {
	    if ((vals[0] == 'NULL' || vals[0] == 'none') || (vals[1] == 'NULL' || vals[1] == 'none'))
		  errors+='- '+nm+' deAbe contener una Hora correcta.\n';
	  }else if (test.indexOf('hourMen')!=-1){
	    var hourIni = new Date(1990,0,1,Number(vals[0]),Number(vals[1]),0,0);
	    var hourFin = new Date(1990,0,1,Number(vals[2]),Number(vals[3]),0,0);
	    if (hourIni > hourFin)
		  errors+='- '+nm+'\n';
	  }else if (test.indexOf('datetimeMen')!=-1){
	  	// Crea los Objetos de tipo Date
	  	if(vals.length>10){
			if(vals[10]){
				var datetimeIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]));
			    var datetimeFin = new Date(Number(vals[7]),Number(vals[6])-1,Number(vals[5]));
			}else{
			    var datetimeIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]),Number(vals[3]),Number(vals[4]),0,0);
		    	var datetimeFin = new Date(Number(vals[7]),Number(vals[6])-1,Number(vals[5]),Number(vals[8]),Number(vals[9]),0,0);
			}
		}else{
		    var datetimeIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]),Number(vals[3]),Number(vals[4]),0,0);
		    var datetimeFin = new Date(Number(vals[7]),Number(vals[6])-1,Number(vals[5]),Number(vals[8]),Number(vals[9]),0,0);
		}
		// Clasifica los mensajes de Error
	  	if(vals.length>10){
		  if(vals[10]){
		    if (datetimeIni > datetimeFin)
			  errors+='- '+nm+'\n'
		  }else{
		    if (datetimeIni >= datetimeFin)
			  errors+='- '+nm+'\n'
		  }
		}else{
		  if (datetimeIni >= datetimeFin)
		    errors+='- '+nm+'\n';
		}
	  }else if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) {
		  p=val.indexOf('@');
          if (p<1 || p==(val.length-1))
		    errors+='- '+nm+' debe contener una direccion e-mail.\n';
        } else if (test!='R') {
          if (isNaN(val))
		    errors+='- '+nm+' debe contener un n�mero.\n';
          if (test.indexOf('inRange') != -1) {
		    p=test.indexOf(':');
            min=test.substring(8,p);
			max=test.substring(p+1);
            if (val<min || max<val)
			  errors+='- '+nm+' debe contener un n�mero entre '+min+' y '+max+'.\n';
          }
		}
	  } else if (test.charAt(0) == 'R')
	    errors += '- '+nm+' es un dato obligatorio.\n';
	}
  }
  if (errors)
    alert('Los siguientes errores han ocurrido:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function MM_displayStatusMsg(msgStr) { //v1.0
  status=msgStr;
  document.MM_returnValue = true;
}
//-->