//ESTE FICHERO ES UN EJEMPLO PROPORCIONADO POR EL CODIGO
//Autor de este ejemplo: Ivan Nieto

//Modifica este ejemplo para crear tu arbol a medida

// Configuracion
USETEXTLINKS = 1	// A 0 si no queremos que los nombres no sean enlaces
			// A 1 si queremos que si

// Raiz del arbol
foldersTree = gFld("<strong>INTRANET DE SITRADOC</strong>", "")
	// Nivel 1
	NIVEL_1 = insFld(foldersTree, gFld("<small>Institucional</small>", ""))
	      // Nivel 2
		  NIVEL_2 = insFld(NIVEL_1, gFld("<small>Aplicativos</small>","institucional/aplicativos/index.php"))
		  