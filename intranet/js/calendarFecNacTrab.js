<!--

var gdCtrl = new Object();
var gcGray = "#777777";//para las fechas que no pertenecen al men actual
var gcToggle = "#f5f09c";//para colorear las celdas cuando se tiene encima el puntero
var gcBG = "#FFFFFF";//para el fondo de las celdas del calendario
var giYear,giMonth,giDay;
var calName;
var VicPopCal = new Object();

var tbSelMonth, tbSelYear, cellText;

function fPopCalendar(name, popCtrl, dateCtrl, popCal, year, month, day){
	CalendarHide()
	calName = name;
	giYear = year;
	//giYear = 2006;
	giMonth = month;
	giDay = day;
	parent.event.cancelBubble=true;
	VicPopCal = popCal;
	gdCtrl = dateCtrl;
	
	StringHTML = '<table id="popTable" border="0" bgcolor="#DBEAF5">'+
				   '<tr>'+
				     '<td valign="middle" align="center" >'+
					   '<a href="javascript:;" onclick="fPrevMonth()"><img src="/img/800x600/arrow_left.gif" alt="" width="7" height="12" border="0" /></a>'+
					   '&nbsp;<select name="tbSelMonth'+calName+'" class="ipsel2" onChange="fUpdateCal(tbSelYear'+calName+'.value, this.value)">';

	for (i=00; i<12; i++)
		StringHTML +=  '<option value="'+(i+1)+'">'+gMonths[i]+'</option>';
	
	StringHTML +=      '</select>'+
					   '&nbsp;<select name="tbSelYear'+calName+'" class="ipsel2" onChange="fUpdateCal(this.value, tbSelMonth'+calName+'.value)">';
					 
	for(i=1900;i<2008;i++)
		StringHTML +=  '<option value="'+i+'">'+i+'</option>';
	StringHTML +=      '</select>'+
					   '&nbsp;<a href="javascript:;" onclick="fNextMonth()"><img src="/img/800x600/arrow_right.gif" alt="" width="7" height="12" border="0" /></a>'+
				     '</td>'+
			       '</tr>'+
				   '<tr>'+
                     '<td align="center">'+
                       '<table width="100%" border="0" cellpadding="3" cellspacing="0" style="border:1px solid #a0c6e5; background-color:#FFFFFF">';
					 
	StringHTML += fDrawCal(giYear, giMonth, 19, 11);
	
	StringHTML +=      '</table>'+
                     '</td>'+
                   '</tr>'+
				   '<tr>'+
				     '<td align="center" class="lnHoyCal">'+
                       '<a href="javascript:;" onclick="fSetDate(giYear,giMonth,giDay)"><font style="cursor:hand; font:11 Arial"><strong>Hoy es:&nbsp;&nbsp;'+gMonths[giMonth-1]+'&nbsp;'+giDay+',&nbsp;'+giYear+'</strong></font></a>'+
                     '</td>'+
				   '</tr>'+
	             '</table>';
				 
  	VicPopCal.innerHTML = StringHTML;
	
	tbSelMonth = MM_findObj('tbSelMonth'+calName);
	tbSelYear = MM_findObj('tbSelYear'+calName);
	cellText = MM_findObj('cellText'+calName);
	
	fSetYearMon(giYear, giMonth);
	var point = fGetXY(popCtrl);
	with (VicPopCal.style) {
		left = point.x;
		top  = point.y+popCtrl.offsetHeight+1;
		visibility = 'visible';
	}
	VicPopCal.focus();
}

function fSetDate(iYear, iMonth, iDay){
	var anno = giYear;
	var mes  = giMonth;
	var dia  = giDay;

	var dias_actual_aprox=365*eval(anno)+30*eval(mes)+dia;
	var dias_selc_aprox=365*eval(iYear)+30*eval(iMonth)+iDay;

	//Esta condici�n es para que solo se muestre hast la fecha actual mas no una fecha posterior a la de hoy
	//Si comentamos esta parte se puede mostrar una fecha posterior a la de hoy
	//if(dias_actual_aprox>=dias_selc_aprox){
		
	//Ahora si no se quiere que se seleccione un mes posterior
	var meses_actual_aprox=30*eval(mes);
	var meses_selc_aprox=30*eval(iMonth);
	//if(meses_actual_aprox>=meses_selc_aprox){
  		gdCtrl.value = sprintf('%02d',iDay)+"/"+sprintf('%02d',iMonth)+"/"+iYear;
  		VicPopCal.style.visibility = "hidden";
	//}
}

function fSetSelected(aCell){
  var iOffset = 0;
  var iYear = parseInt(tbSelYear.value);
  var iMonth = parseInt(tbSelMonth.value);

  aCell.bgColor = gcBG;
  with (aCell.children["cellText"+calName]){
  	var iDay = parseInt(innerText);
  	if (color==gcGray)
		iOffset = (Victor<10)?-1:1;
	iMonth += iOffset;
	if (iMonth<01) {
		iYear--;
		iMonth = 12;
	}else if (iMonth>12){
		iYear++;
		iMonth = 01;
	}
  }
  fSetDate(iYear, iMonth, iDay);
}

function Point(iX, iY){
	this.x = iX;
	this.y = iY;
}

function fBuildCal(iYear, iMonth) {//
  var aMonth=new Array();//
  for(i=01;i<7;i++)
  	aMonth[i]=new Array(i);

  var dCalDate=new Date(iYear, iMonth-01, 01);
  var iDayOfFirst=dCalDate.getDay();
  var iDaysInMonth=new Date(iYear, iMonth, 0).getDate();
  var iOffsetLast=new Date(iYear, iMonth-01, 0).getDate()-iDayOfFirst+1;
  var iDate = 1;
  var iNext = 1;

  for (d = 0; d < 7; d++)
	aMonth[1][d] = (d<iDayOfFirst)?-(iOffsetLast+d):iDate++;
  for (w = 2; w < 7; w++)
  	for (d = 0; d < 7; d++)
		aMonth[w][d] = (iDate<=iDaysInMonth)?iDate++:-(iNext++);
  return aMonth;
}

function fDrawCal(iYear, iMonth, iCellWidth, iDateTextSize) {
	var WeekDay = new Array("do.","lu.","ma.","mi.","ju.","vi.","s.");
	var styleTD = ' bgcolor="'+gcBG+'" width="'+iCellWidth+'" bordercolor="'+gcBG+'" valign="middle" align="center" style="font: '+iDateTextSize+' Arial;';
	
	StringHTML = '<tr>';
	for(i=0; i<7; i++)
		StringHTML += '<td '+styleTD+'color:#000000" >' + WeekDay[i] + '</td>';
	StringHTML += '</tr>'+
				  '<tr height="1"><td colspan="7" style="background-color:#a0c6e5"></td></tr>';

  	for (w = 1; w < 7; w++) {//se imprime toda la tabla para colocar los dias del mes
		StringHTML += '<tr>';
		for (d = 0; d < 7; d++) {
			StringHTML += '<td id="calCell" '+styleTD+'cursor:hand;" onMouseOver="this.bgColor=gcToggle" onMouseOut="this.bgColor=gcBG" onclick="fSetSelected(this)">';
			StringHTML += '<font id="cellText'+calName+'"> </font>';
			StringHTML += '</td>';
		}
		StringHTML += '</tr>';
	}
	
	return StringHTML;
}

function fUpdateCal(iYear, iMonth) {
  myMonth = fBuildCal(iYear, iMonth);
  var i = 0;
  for (w = 0; w < 6; w++){
	for (d = 0; d < 7; d++){
		// while (cellText[(7*w)+d]) {
			Victor = i++;
			if (myMonth[w+01][d]<0) {
				cellText[(7*w)+d].color = gcGray;
				cellText[(7*w)+d].innerText = -myMonth[w+1][d];
			}else{
				cellText[(7*w)+d].color = ((d==0)||(d==6))?"fb0314":"000066"; //
				cellText[(7*w)+d].innerText = myMonth[w+01][d];
			}
	}
  }
}

function fSetYearMon(iYear, iMon){//sirve para colocar en seleccion a los checkedbox a�o y mes
  tbSelMonth.options[iMon-1].selected = true;//selecciona el mea
  for (i = 0; i < tbSelYear.length; i++)
	if (tbSelYear.options[i].value == iYear)
		tbSelYear.options[i].selected = true;//selecciona el a�o
  fUpdateCal(iYear, iMon);
}

function fPrevMonth(){
  var iMon = tbSelMonth.value;
  var iYear = tbSelYear.value;

  if (--iMon<01) {
	  iMon = 12;
	  iYear--;
  }

  fSetYearMon(iYear, iMon);
}

function fNextMonth(){
  var iMon = tbSelMonth.value;
  var iYear = tbSelYear.value;

  if (++iMon>12) {
	  iMon = 01;
	  iYear++;
  }

  fSetYearMon(iYear, iMon);
}

function fGetXY(aTag){
  var oTmp = aTag;
  var pt = new Point(0,0);
  do {
  	pt.x += oTmp.offsetLeft;
  	pt.y += oTmp.offsetTop;
  	oTmp = oTmp.offsetParent;
  } while(oTmp.tagName!="BODY");
  return pt;
}

////////////////////////////////se cargar junto con la pagina pricipal////////////////////////////////
var gMonths = new Array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Set","Oct","Nov","Dic");

-->
