/*javascript code to make the menu work in non-CSS compliant browsers*/
/**
 * Shows the menu element with id "senderid"
 * @param string senderid
 */
function showMenu(senderid) {
 document.getElementById(senderid).style.display = 'block';
}
/**
 * Hides the menu element with id "senderid"
 * @param string senderid
 */
function hideMenu(senderid) {
 document.getElementById(senderid).style.display = 'none';	
}