<!--
// CONSTANTS
var separator = ",";  // use comma as 000's separator
var decpoint = ".";  // use period as decimal point
var percent = "%";
var currency = "$";  // use dollar sign for currency

function formatNumber(number, format, print) {  // use: formatNumber(number, "format")
	if (print) document.write("formatNumber(" + number + ", \"" + format + "\")<br>");
	
	if (number - 0 != number) return null;  // if number is NaN return null
	var useSeparator = format.indexOf(separator) != -1;  // use separators in number
	var usePercent = format.indexOf(percent) != -1;  // convert output to percentage
	var useCurrency = format.indexOf(currency) != -1;  // use currency format
	var isNegative = (number < 0);
	number = Math.abs (number);
	if (usePercent) number *= 100;
	format = strip(format, separator + percent + currency);  // remove key characters
	number = "" + number;  // convert number input to string
	
	 // split input value into LHS and RHS using decpoint as divider
	var dec = number.indexOf(decpoint) != -1;
	var nleftEnd = (dec) ? number.substring(0, number.indexOf(".")) : number;
	var nrightEnd = (dec) ? number.substring(number.indexOf(".") + 1) : "";
	
	 // split format string into LHS and RHS using decpoint as divider
	dec = format.indexOf(decpoint) != -1;
	var sleftEnd = (dec) ? format.substring(0, format.indexOf(".")) : format;
	var srightEnd = (dec) ? format.substring(format.indexOf(".") + 1) : "";
	
	 // adjust decimal places by cropping or adding zeros to LHS of number
	if (srightEnd.length < nrightEnd.length) {
		var nextChar = nrightEnd.charAt(srightEnd.length) - 0;
		nrightEnd = nrightEnd.substring(0, srightEnd.length);
		if (nextChar >= 5) nrightEnd = "" + ((nrightEnd - 0) + 1);  // round up
	
		// patch provided by Patti Marcoux 1999/08/06
		while (srightEnd.length > nrightEnd.length) {
			nrightEnd = "0" + nrightEnd;
		}
	
		if (srightEnd.length < nrightEnd.length) {
			nrightEnd = nrightEnd.substring(1);
			nleftEnd = (nleftEnd - 0) + 1;
		}
	} else {
		for (var i=nrightEnd.length; srightEnd.length > nrightEnd.length; i++) {
			if (srightEnd.charAt(i) == "0") nrightEnd += "0";  // append zero to RHS of number
			else break;
		}
	}
	
	 // adjust leading zeros
	sleftEnd = strip(sleftEnd, "#");  // remove hashes from LHS of format
	while (sleftEnd.length > nleftEnd.length) {
		nleftEnd = "0" + nleftEnd;  // prepend zero to LHS of number
	}
	
	if (useSeparator) nleftEnd = separate(nleftEnd, separator);  // add separator
	var output = nleftEnd + ((nrightEnd != "") ? "." + nrightEnd : "");  // combine parts
	output = ((useCurrency) ? currency : "") + output + ((usePercent) ? percent : "");
	if (isNegative) {
		// patch suggested by Tom Denn 25/4/2001
		output = (useCurrency) ? "(" + output + ")" : "-" + output;
	}
	return output;
}

function strip(input, chars) {  // strip all characters in 'chars' from input
	var output = "";  // initialise output string
	for (var i=0; i < input.length; i++)
		if (chars.indexOf(input.charAt(i)) == -1)
			output += input.charAt(i);
	return output;
}

function separate(input, separator) {  // format input using 'separator' to mark 000's
	input = "" + input;
	var output = "";  // initialise output string
	for (var i=0; i < input.length; i++) {
		if (i != 0 && (input.length - i) % 3 == 0) output += separator;
		output += input.charAt(i);
	}
	return output;
}

function sprintf()
{
   if (!arguments || arguments.length < 1 || !RegExp)
   {
      return;
   }
   var str = arguments[0];
   var re = /([^%]*)%('.|0|\x20)?(-)?(\d+)?(\.\d+)?(%|b|c|d|u|f|o|s|x|X)(.*)/;
   var a = b = [], numSubstitutions = 0, numMatches = 0;
   while (a = re.exec(str))
   {
      var leftpart = a[1], pPad = a[2], pJustify = a[3], pMinLength = a[4];
      var pPrecision = a[5], pType = a[6], rightPart = a[7];

      numMatches++;
      if (pType == '%')
      {
         subst = '%';
      }
      else
      {
         numSubstitutions++;
         if (numSubstitutions >= arguments.length)
         {
            alert('Error! Not enough function arguments (' + (arguments.length - 1)
               + ', excluding the string)\n'
               + 'for the number of substitution parameters in string ('
               + numSubstitutions + ' so far).');
         }
         var param = arguments[numSubstitutions];
         var pad = '';
                if (pPad && pPad.substr(0,1) == "'") pad = leftpart.substr(1,1);
           else if (pPad) pad = pPad;
         var justifyRight = true;
                if (pJustify && pJustify === "-") justifyRight = false;
         var minLength = -1;
                if (pMinLength) minLength = parseInt(pMinLength);
         var precision = -1;
                if (pPrecision && pType == 'f')
                   precision = parseInt(pPrecision.substring(1));
         var subst = param;
         switch (pType)
         {
         case 'b':
            subst = parseInt(param).toString(2);
            break;
         case 'c':
            subst = String.fromCharCode(parseInt(param));
            break;
         case 'd':
            subst = parseInt(param) ? parseInt(param) : 0;
            break;
         case 'u':
            subst = Math.abs(param);
            break;
         case 'f':
            subst = (precision > -1)
             ? Math.round(parseFloat(param) * Math.pow(10, precision))
              / Math.pow(10, precision)
             : parseFloat(param);
            break;
         case 'o':
            subst = parseInt(param).toString(8);
            break;
         case 's':
            subst = param;
            break;
         case 'x':
            subst = ('' + parseInt(param).toString(16)).toLowerCase();
            break;
         case 'X':
            subst = ('' + parseInt(param).toString(16)).toUpperCase();
            break;
         }
         var padLeft = minLength - subst.toString().length;
         if (padLeft > 0)
         {
            var arrTmp = new Array(padLeft+1);
            var padding = arrTmp.join(pad?pad:" ");
         }
         else
         {
            var padding = "";
         }
      }
      str = leftpart + padding + subst + rightPart;
   }
   return str;
}

function trim(str){
   return str.replace(/^\s*|\s*$/g,"");
}

-->