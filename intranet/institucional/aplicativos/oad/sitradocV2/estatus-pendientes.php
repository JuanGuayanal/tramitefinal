<?php
//include('header.php');
//include('armador.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css"> 
 
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sitra {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #003366;
	background-color: #FEF7C1;
	border: 1px #E9E9E9;
}
 
</style>

<title>Pendientes en SITRADOC</title></head>
<body topmargin="0" leftmargin="0">	
<?php 
	function connect(){
		$hostname_sys = CJDBMSSQL;
		$database_sys = "DB_TRAMITE_DOCUMENTARIO";
		$username_sys = CJDBMSSQLUSER;
		$password_sys = CJDBMSSQLPASS;
		$connect = mssql_connect($hostname_sys, $username_sys, $password_sys);
	
		if(!$connect){ 
			echo "Error en la conexion hacia la base de datos...<br>";
			echo "2: ". $connect ."<br>";
		}else { 
			if(!(mssql_select_db($database_sys,$connect))) { 
			echo "Error al seleccionar la base de datos... <br>"; 
		}else {  
			}                      
		}
	}
connect();
?>

<table width="400" align="center" class="tabla-login">
  <tr align="center"> 
  	<td height="40" colspan="4" class="textoblack" valign="middle"><b>ESTATUS DOCUMENTARIO POR DEPENDENCIA
  	<br>
  	</b></td>
  </tr>

	<?php  
		$sql_quejas="exec DB_TRAMITE_DOCUMENTARIO.dbo.sp_ver_tops_pendientes_sitradoc 1 ";
		$result = mssql_query($sql_quejas);
		//echo $sql_quejas;		
    ?>
  <tr align="center"> 
  	<td height="25" colspan="4" class="sub-item" valign="middle" align="left"><b>CON MAYOR CANTIDAD DE PENDIENTES<br>
  	</b></td>
  </tr>

    <tr style="height: 25px" class="item-sitra">
        <!--<td align="center" class="texto" valign="middle"><b>N�</b></td>-->
        <td align="center" class="texto" valign="middle"><b>DEPENDENCIA</b></td>
        <td align="center" class="texto" valign="middle"><b>TOTAL</b></td>
        <td align="center" class="texto" valign="middle"><b>%</b></td>        
    </tr>
	<?php
		$ncnt=0;
		if (mssql_num_rows($result)){
			while ($row = @mssql_fetch_array($result)) {					
				$ncnt 					=$ncnt +1;				
				$v_dependencia	=$row[0];
				$v_total				=$row[1];
				$v_dporcentaje	=$row[2];
				$v_porcentaje		=$row[3];					
    ?>   

    <tr>
        <!--<td class="texto" align="center"><?=$ncnt?></td>-->
        <td class="texto"><? echo $v_dependencia ?></td>
        <td class="texto" align="center"><? echo number_format($v_total) ?></td>
        <td class="texto" align="center"><? echo number_format(($v_dporcentaje),3) ?><? echo $v_porcentaje ?> </td>
    </tr>
	<?php 
			}
    	}
	?>
    <tr>
        <!--<td></td>-->
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
</table>
<br>
<table width="400" align="center" class="tabla-login">
  <tr align="center">
    <td height="25" colspan="4" class="sub-item" valign="middle" align="left"><b>CON MENOR CANTIDAD DE PENDIENTES</b></td>
  </tr>
  <?php  
		$sql_pendientes="exec DB_TRAMITE_DOCUMENTARIO.dbo.sp_ver_tops_pendientes_sitradoc 2 ";
		$result = mssql_query($sql_pendientes);
		//echo $sql_quejas;		
    ?>
  <tr style="height: 25px" class="item-sitra">
    <!--<td align="center" class="texto" valign="middle"><b>N&deg;</b></td>-->
    <td align="center" class="texto" valign="middle"><b>DEPENDENCIA</b></td>
    <td align="center" class="texto" valign="middle"><b>TOTAL</b></td>
    <td align="center" class="texto" valign="middle"><b>%</b></td>
  </tr>
  <?php
		$ncnt=0;
		if (mssql_num_rows($result)){
			while ($row = @mssql_fetch_array($result)) {					
				$ncnt 					=$ncnt +1;				
				$v_dependencia	=$row[0];
				$v_total				=$row[1];
				$v_dporcentaje	=$row[2];
				$v_porcentaje		=$row[3];	
				
				$V_REP_FECHA	=$row[4];
				$V_REP_HORA		=$row[5];									
    ?>
  <tr>
    <!--<td class="texto" align="center"><?=$ncnt?></td>-->
    <td class="texto"><? echo $v_dependencia ?></td>
    <td class="texto" align="center"><? echo number_format($v_total) ?></td>
    <td class="texto" align="center"><? echo number_format(($v_dporcentaje),3) ?><? echo $v_porcentaje ?> </td>
  </tr>
  <?php 
			}
    	}
	?>
  <tr>
    <!--<td></td>-->
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0"  width="400" align="center" >
<tr>
<td class="textored">&nbsp;<b>Fecha/Hora de Actualizaci&oacute;n: <? echo $V_REP_FECHA.' '.$V_REP_HORA; ?></b>
</td>
</tr>
</table>
</body>
<?
//include('footer.php');
?>