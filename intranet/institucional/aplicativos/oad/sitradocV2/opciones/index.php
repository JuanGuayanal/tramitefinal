<?
include_once('modulosadmin/claseIntranet.inc.php');

$objIntranet = new Intranet();

$menu = ($_POST['menu']) ? $_POST['menu'] : $_GET['menu'];
$subMenu = ($_POST['subMenu']) ? $_POST['subMenu'] : $_GET['subMenu'];
$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

include_once('modulosadmin/claseSitradocOpciones.inc.php');

$accionM=$accion;

$modSitradoc = new SitradocOpciones($menu,$subMenu,$accionM);
if($modSitradoc){
	if ($accion!=$modSitradoc->arr_accion[AGREGA_DOCUMENTO]&&$accion!=$modSitradoc->arr_accion[MODIFICA_REFERENCIA_RESOL]
		&&$accion!=$modSitradoc->arr_accion[ANULA_RESOLUCION]&&$accion!=$modSitradoc->arr_accion[NUEVO_DESTINATARIO]
		&&$accion!=$modSitradoc->arr_accion[DISMINUYE_CONTADOR]
		){
		$objIntranet->Header2('Tr�mite Documentario',false,array('suspEmb'));
		//$objIntranet->Body('tramite_tit.gif',false,false);
		$objIntranet->Body2('tit-sitradoc.gif',false,false);
	}

	switch ($accion){
		case $modSitradoc->arr_accion[FRM_BUSCA_RESOLUCION]:
			$modSitradoc->FormBuscaResolucion($_POST['page'],$_POST['tipResol'],$_POST['nroResol'],$_POST['sumilla'],$_POST['desFechaIni'],$_POST['desFechaFin'],$_POST['dependencia']);
			break;
		case $modSitradoc->arr_accion[BUSCA_RESOLUCION]:
			$modSitradoc->BuscaResolucion($_POST['page'],$_POST['tipResol'],$_POST['nroResol'],$_POST['sumilla'],$_POST['desFechaIni'],$_POST['desFechaFin'],$_POST['dependencia']);
			break;
		case $modSitradoc->arr_accion[FRM_MODIFICA_REFERENCIA_RESOL]:
			$modSitradoc->FormModificaReferenciaResolucion(($_POST['idResolucion']) ? $_POST['idResolucion'] : $_GET['idResolucion'],$_POST['GrupoOpciones1'],$_POST['nroTD'],$_POST['anyo3'],$_POST['tipoDocc'],$_POST['numero2'],$_POST['anyo2'],
	                                 $_POST['siglasDepe2'],$_POST['Busca'],$_POST['observaciones']);
			break;
		case $modSitradoc->arr_accion[MODIFICA_REFERENCIA_RESOL]:
			$modSitradoc->ModificaReferenciaResolucion($_POST['idResolucion'],$_POST['GrupoOpciones1'],$_POST['nroTD'],$_POST['anyo3'],$_POST['tipoDocc'],$_POST['numero2'],$_POST['anyo2'],
	                                 $_POST['siglasDepe2'],$_POST['Busca'],$_POST['observaciones']);
			break;
		case $modSitradoc->arr_accion[FRM_ANULA_RESOLUCION]:
			$modSitradoc->FormAnulaResolucion(($_POST['idResolucion']) ? $_POST['idResolucion'] : $_GET['idResolucion'],$_POST['GrupoOpciones1'],$_POST['observaciones']);
			break;
		case $modSitradoc->arr_accion[ANULA_RESOLUCION]:
			$modSitradoc->AnulaResolucion($_POST['idResolucion'],$_POST['GrupoOpciones1'],$_POST['observaciones']);
			break;
		case $modSitradoc->arr_accion[FRM_BUSCA_CORRESPONDENCIA]:
			$modSitradoc->FormBuscaCorrespondencia();
			break;
		case $modSitradoc->arr_accion[BUSCA_CORRESPONDENCIA]:
			$modSitradoc->BuscaCorrespondencia($_POST['page'],$_POST['noti'],$_POST['TipoCor'],$_POST['resol'],$_POST['detal'],$_POST['destinatario'],
										$_POST['mes_ini'].'/'.$_POST['dia_ini'].'/'.$_POST['anyo_ini'],
										 $_POST['mes_fin'].'/'.$_POST['dia_fin'].'/'.$_POST['anyo_fin']);
			break;
		case $modSitradoc->arr_accion[FRM_NUEVO_DESTINATARIO]:
			$modSitradoc->FormNuevoDestinatario(($_POST['id']) ? $_POST['id'] : $_GET['id'],$_POST['destinatario'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['domicilio']);
			break;
		case $modSitradoc->arr_accion[NUEVO_DESTINATARIO]:
			$modSitradoc->NuevoDestinatario($_POST['id'],$_POST['destinatario'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['domicilio']);
			break;
		case $modSitradoc->arr_accion[FRM_DISMINUYE_CONTADOR]:
			$modSitradoc->FormDisminuyeContador($_POST['dependencia'],$_POST['claseDoc'],$_POST['Busca'],$_POST['exito']);
			break;
		case $modSitradoc->arr_accion[DISMINUYE_CONTADOR]:
			$modSitradoc->DisminuyeContador($_POST['dependencia'],$_POST['claseDoc'],$_POST['Busca'],$_POST['exito'],$_POST['valorDism']);
			break;
		case $modSitradoc->arr_accion[CONSULTA_INTERNOS]:
			$modSitradoc->ConsultaInternos(($_POST['idDocumento']) ? $_POST['idDocumento'] : $_GET['idDocumento'],
											 $_POST['nroTD'],$_POST['anyo3'],$_POST['tipoDocc'],
			                                $_POST['numero2'],$_POST['anyo2'],$_POST['siglasDepe2'],
											($_POST['Busca']) ? $_POST['Busca'] : $_GET['Busca'],$_POST['Buscar'],
											($_POST['exito']) ? $_POST['exito'] : $_GET['exito'],
											$_POST['idClaseDoc'],$_POST['dependencia'],$_POST['asunto'],
											$_POST['observaciones'],$_POST['desFechaIni']
											);
			break;
		/*case $modSitradoc->arr_accion[FRM_BUSCA_DOCUMENTO]:
			$modSitradoc->FormBuscaDocumento($_POST['page'],$_POST['tipDocumento'],$_POST['tipBusqueda'],$_POST['nroTD'],$_POST['asunto'],$_POST['observaciones'],
										 $_POST['fecIniDir'],
										 $_POST['fecFinDir']);
			break;
		case $modSitradoc->arr_accion[BUSCA_DOCUMENTO]:
			$modSitradoc->BuscaDocumento($_POST['page'],$_POST['tipDocumento'],$_POST['tipBusqueda'],$_POST['nroTD'],$_POST['RazonSocial'],$_POST['asunto'],$_POST['RZ'],$_POST['observaciones'],$_POST['procedimiento'],
										 $_POST['mes_ini'].'/'.$_POST['dia_ini'].'/'.$_POST['anyo_ini'],
										 $_POST['mes_fin'].'/'.$_POST['dia_fin'].'/'.$_POST['anyo_fin']);
			break;
		case $modSitradoc->arr_accion[FRM_AGREGA_DOCUMENTO]:
			$modSitradoc->FormAgregaDocumento($_POST['TipoDoc'],$_POST['ruc'],$_POST['RazonSocial'],$_POST['domicilio'],$_POST['asunto'],$_POST['indicativo'],$_POST['folio'],$_POST['Derivar'],$_POST['maxdias'],$_POST['result']);
			break;
		case $modSitradoc->arr_accion[AGREGA_DOCUMENTO]:
			$modSitradoc->AgregaDocumento($_POST['TipoDoc'],$_POST['ruc'],$_POST['RazonSocial'],$_POST['domicilio'],$_POST['asunto'],$_POST['indicativo'],$_POST['folio'],$_POST['Derivar'],$_POST['maxdias'],$_POST['result']);
			break;*/
		/*case $modSitradoc->arr_accion['BUENA_TRANS']:
			$modSitradoc->MuestraStatTrans($accion);
			break;
		case $modSitradoc->arr_accion['MALA_TRANS']:
			$modSitradoc->MuestraStatTrans($accion);
			break;*/
		default:
			$modSitradoc->MuestraIndex();
			break;

	}
	if ($accion!=$modSitradoc->arr_accion[AGREGA_DOCUMENTO]&&$accion!=$modSitradoc->arr_accion[MODIFICA_REFERENCIA_RESOL]
		&&$accion!=$modSitradoc->arr_accion[ANULA_RESOLUCION]&&$accion!=$modSitradoc->arr_accion[NUEVO_DESTINATARIO]
		&&$accion!=$modSitradoc->arr_accion[DISMINUYE_CONTADOR]
		)
		$objIntranet->Footer2(false);
}else{
	$objIntranet->Header2('Tr�mite Documentario',false,array('suspEmb'));
	//$objIntranet->Body('tramite_tit.gif',false,false);
	$objIntranet->Body2('tit-sitradoc.gif',false,false);
	$objIntranet->Footer2(false);	
}

?>