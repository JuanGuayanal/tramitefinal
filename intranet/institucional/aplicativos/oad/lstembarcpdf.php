<?php error_reporting (E_ALL ^ E_NOTICE); ?>
<?php
require('fpdf.php');

class PDF extends FPDF {
	//-- HEADER --//
	function Header(){
		
		$v_NombreEmbarc		= $_REQUEST["nem"];
		$v_Metrica 			= $_REQUEST["mtr"];
		$v_Casco			= $_REQUEST["csc"];
		$v_Regimen			= $_REQUEST["rgm"];
		$v_Anio				= $_REQUEST["anio"];
		
		$v_Fecha 	= date("d/m/Y");
		
		$strTitulo1 	= "INFORMACI�N DE DESCARGA DE EMBARCACIONES, CORRESPONDIENTES AL A�O ". $v_Anio;
		
		$this->SetDrawColor(175,175,175);
		$this->SetFillColor(230,230,0);
		$this->SetLineWidth(0.25);
		//$this->Ln(8);
		//$this->Image('images/logoCONVENIO_SITRADOC.gif',141,25,55);
    	$this->SetFont('Arial','B',13);
		$this->Ln(10);
		//$this->Cell(185,5, $nom_cite_del_trab, 0,0,'L');
		$this->SetFont('Arial','',8);
		$this->Cell(185,5, $strTitulo1, 0,0,'C');
		$this->Ln(3.5);
		
		$this->SetAuthor("MIDIS",1);
		$this->SetTitle("Listado", 1);
		$this->SetDisplayMode(90, 'continuous') ;

	}
	
	
	
	//-- FOOTER --//
	function Footer(){
		$v_NombreEmbarc		= $_REQUEST["nem"];
		$v_Metrica 			= $_REQUEST["mtr"];
		$v_Casco			= $_REQUEST["csc"];
		$v_Regimen			= $_REQUEST["rgm"];
		
		$v_Fecha 	= date("d/m/Y");
	
		$this->SetY(-50);
		$this->SetFont('Arial','',8);	
		$borderF="0";
		
		$this->SetDrawColor(175,175,175);
		$this->SetFillColor(230,230,0);
		$this->SetLineWidth(0.25);		
		$this->SetFont('Arial','',7);
		$this->Ln(7);

	}
	
		
	function PDF($orientation='P',$unit='mm',$format='A4'){
		$this->FPDF($orientation,$unit,$format);
		$this->B=0;
		$this->I=0;
		$this->U=0;
		$this->HREF='';
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',6);


//-- DETAIL --//
$v_NombreEmbarc		= $_REQUEST["nem"];
$v_Matricula		= $_REQUEST["mtr"];
$v_Casco			= $_REQUEST["csc"];
$v_Regimen			= $_REQUEST["rgm"];
$v_Anio				= $_REQUEST["anio"];
	
$v_Fecha 			= date("d/m/Y");

$borderDet="1";
$filHeight="5";
$pdf->Ln(5);
$pdf->SetDrawColor(175,175,175);
$pdf->SetFillColor(230,230,0);
$pdf->SetLineWidth(0.25);
$pdf->Cell(188,6,'ITM      EMBARCACI�N                 MATR�CULA 		  CASCO 		                   REGIMEN                       ARMADOR                                    TOT.CERT.(TM)    TOT.EIP(TM)  TOT.TOLVAS(TM) TOT.DP(TM)',1,0,'L');
$pdf->Ln(5);
$ncnt = 0;

include("connstr.php");
connect();

$sqldet = "SELECT ";
$sqldet = $sqldet ." e.NOMBRE_EMB,";
$sqldet = $sqldet ." e.MATRICULA_EMB,";
$sqldet = $sqldet ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 1, ". $v_Anio .") AS TOTAL_CERTIFICADORA,";
$sqldet = $sqldet ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 2, ". $v_Anio .") AS TOTAL_EIP,";
$sqldet = $sqldet ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 3, ". $v_Anio .") AS TOTAL_TOLVAS,";
$sqldet = $sqldet ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 4, ". $v_Anio .") AS TOTAL_DERECHO_PESCA,";
$sqldet = $sqldet ." C.DESC_CASCO AS CASCO,";
$sqldet = $sqldet ." R.DESC_REGIMEN AS REGIMEN,";
$sqldet = $sqldet ." desarrollo_prueba.f_Nombre_PersonaxEmbarcacion_momento2011(e.ID_EMB, CONVERT(varchar(10), '31/12/2010',103)) AS PERSONA_ARMADOR";
$sqldet = $sqldet ." , e.ID_REGIMEN AS ID_REGIMEN";
$sqldet = $sqldet ." , e.ID_CASCO AS ID_CASCO";
$sqldet = $sqldet ." , dbo.LASTDATAMOD() AS LASTDATAMOD";
$sqldet = $sqldet ." FROM user_dnepp.EMBARCACIONNAC AS e";
$sqldet = $sqldet ." LEFT OUTER JOIN user_dnepp.CASCO AS C ON e.ID_CASCO = C.ID_CASCO ";
$sqldet = $sqldet ." LEFT OUTER JOIN user_dnepp.REGIMEN AS R ON e.ID_REGIMEN = R.ID_REGIMEN";
$sqldet = $sqldet ." WHERE e.NOMBRE_EMB<>'' ";
$sqldet = $sqldet ." AND ( (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 1, ". $v_Anio .")>0)";
$sqldet = $sqldet ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 2, ". $v_Anio .")>0)";
$sqldet = $sqldet ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 3, ". $v_Anio .")>0)";
$sqldet = $sqldet ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 4, ". $v_Anio .")>0) )";
if ($v_Matricula!="") { 
	$sqldet = $sqldet ." AND e.MATRICULA_EMB LIKE '%". $v_Matricula ."%' "; 
}
if ($v_Regimen!="") { 
	$sqldet = $sqldet ." AND e.ID_REGIMEN=". $v_Regimen; 
}
if ($v_Casco!="") { 
	$sqldet = $sqldet ." AND e.ID_CASCO=". $v_Casco; 	
}
$resdet = mssql_query($sqldet);
if (mssql_num_rows($resdet)){ 
	while ($rowdet = @mssql_fetch_array($resdet)) { 
		$ncnt = $ncnt + 1;
	
		$v_NOMBRE_EMB		= substr(trim($rowdet[0]), 0, 17);
		$v_MATRICULA_EMB	= trim($rowdet[1]);
		$v_TOT_DESC_CERT	= number_format( $rowdet[2], 2, '.', ',');
		$v_TOTSISTPESCA_EIP	= number_format( $rowdet[3], 2, '.', ',');
		$v_TOT_DESC_TOLVAS	= number_format( $rowdet[4], 2, '.', ',');
		$v_TOTAL_DER_PESCA	= number_format( $rowdet[5], 2, '.', ',');
		$v_CASCO			= trim($rowdet[6]);
		$v_REGIMEN			= trim($rowdet[7]);
		$v_PERSONA_ARMADOR	= substr(trim($rowdet[8]), 0, 25);
		$v_LASTDATAMOD		= date("d-m-Y h:m:s", strtotime($row[11]) ) ;
		
		$pdf->Ln(1);
		$pdf->SetFont('Arial','',6);
		//$pdf->Cell(0.1);
		$pdf->Cell(8, $filHeight, $ncnt, $borderDet, 0, 'C');
		$pdf->Cell(0.2);
		$pdf->Cell(25, $filHeight, $v_NOMBRE_EMB, $borderDet, 0, 'L');
		$pdf->Cell(0.2);
		$pdf->Cell(15, $filHeight, $v_MATRICULA_EMB, $borderDet, 0, 'L');
		$pdf->Cell(0.2);
		$pdf->Cell(21, $filHeight, $v_CASCO, $borderDet, 0, 'L');
		$pdf->Cell(0.2);
		$pdf->Cell(23, $filHeight, $v_REGIMEN, $borderDet, 0, 'L');		
		$pdf->Cell(0.2);
		$pdf->Cell(34, $filHeight, $v_PERSONA_ARMADOR, $borderDet, 0, 'L');		
		$pdf->Cell(0.2);
		$pdf->Cell(15, $filHeight, $v_TOT_DESC_CERT, $borderDet, 0, 'R');		
		$pdf->Cell(0.2);
		$pdf->Cell(15, $filHeight, $v_TOTSISTPESCA_EIP, $borderDet, 0, 'R');		
		$pdf->Cell(0.2);
		$pdf->Cell(15, $filHeight, $v_TOT_DESC_TOLVAS, $borderDet, 0, 'R');		
		$pdf->Cell(0.2);
		$pdf->Cell(15, $filHeight, $v_TOTAL_DER_PESCA, $borderDet, 0, 'R');										
		$pdf->Ln(4);
		$pdf->Cell(5);
	}
} else {

}

		$pdf->Ln(4);
		$pdf->Cell(100, $filHeight, "* S�lo se listan las 10 primeras coincidencias que tienen datos en Totales...", 0, 0, 'L');
		$pdf->Ln(2);
		$pdf->Cell(100, $filHeight, "* Fuentes Utilizadas: Certificadoras SGS y CERPER, Sistemas de Pesca y Descargas y DDJJ de los Armadores", 0, 0, 'L');
		$pdf->Ln(2);
		$pdf->Cell(100, $filHeight, "* Fecha de Actualizaci�n: ".$v_LASTDATAMOD, 0, 0, 'L');


$pdf->Ln(5);
$pdf->Output();
?>