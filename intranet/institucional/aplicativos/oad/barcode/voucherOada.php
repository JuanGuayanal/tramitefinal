<? 
/*
	AUTHOR: Walter Cattebeke
	DATE: 08-July-2004
	EMAIL: cachiweb@telesurf.com.py
	LICENSE: This code is free. You can use it and/or modify it.
		I only want to be mentioned and notified if you intend to do either.

	Please read the notes.txt !
*/
error_reporting(E_PARSE);
	require("core.php");
?>


<? 
$p_submit= ($_POST['p_submit']) ? $_POST['p_submit'] : $_GET['p_submit'];
$p_text= ($_POST['p_text']) ? $_POST['p_text'] : $_GET['p_text'];
$p_bcType= ($_POST['p_bcType']) ? $_POST['p_bcType'] : $_GET['p_bcType'];
$p_xDim= ($_POST['p_xDim']) ? $_POST['p_xDim'] : $_GET['p_xDim'];

$p_charHeight= ($_POST['p_charHeight']) ? $_POST['p_charHeight'] : $_GET['p_charHeight'];

$p_w2n= ($_POST['p_w2n']) ? $_POST['p_w2n'] : $_GET['p_w2n'];
$p_type= ($_POST['p_type']) ? $_POST['p_type'] : $_GET['p_type'];
$p_label= ($_POST['p_label']) ? $_POST['p_label'] : $_GET['p_label'];
$p_invert= ($_POST['p_invert']) ? $_POST['p_invert'] : $_GET['p_invert'];
$p_checkDigit= ($_POST['p_checkDigit']) ? $_POST['p_checkDigit'] : $_GET['p_checkDigit'];
$p_rotAngle= ($_POST['p_rotAngle']) ? $_POST['p_rotAngle'] : $_GET['p_rotAngle'];

$maximo= ($_POST['maximo']) ? $_POST['maximo'] : $_GET['maximo'];
//echo "x".$p_text."x";

if ($p_submit == "CONVERT") {?>

<?
	
	if (!isset($p_text)){
		$p_textEnc = "1234567890";
	} else {
		$p_textEnc = rawurlencode($p_text);
	}

	if (!isset($p_xDim)){
		$p_xDim = 1;
	}
	if (!isset($p_w2n)){
		$p_w2n = 3;
	}
	if (!isset($p_charHeight)){
		$p_charHeight = 50;
	}
	
	$p_charGap = $p_xDim;


	$msg="";
	if (!safeStr($p_bcType, $p_text)){
		$msg = "Non-Valid characters inside string";
	}

	$dest = "wrapper.php?p_bcType=$p_bcType&p_text=$p_textEnc" . 
		"&p_xDim=$p_xDim&p_w2n=$p_w2n&p_charGap=$p_charGap&p_invert=$p_invert&p_charHeight=$p_charHeight" .
		"&p_type=$p_type&p_label=$p_label&p_rotAngle=$p_rotAngle&p_checkDigit=$p_checkDigit"
?>
<HTML>
<HEAD>
<TITLE>SITRADOC</TITLE>
</HEAD>
<BODY>
<?php
$dependencia=explode(',',$dependencia);
$folio=explode(',',$folio);
for($i=0;$i<count($dependencia);$i++){
	$salto++;
?>
<div align="center">  
  <strong><font size="-2" face="Arial, Helvetica, sans-serif">Programa Nacional Cuna M&aacute;s</font></strong>
</div>
<TABLE border=1 align="center" cellspacing="0" <?php if (($i< count($dependencia)-1) ){ echo ' style="page-break-after:always;"';}?>>
<!--<TR>
	<TD>Text</TD>
	<TD align=center><? echo $p_text; ?></TD>
</TR>-->
<TR>
	<TD colspan=2 align=center >
	<!--<br>-->
	<? if ($msg != ""){ ?>
		<? echo $msg; ?>
	<? } else { ?>
		&nbsp;
<!--		<IMG SRC="<? echo $dest;?>" ALT="<? echo strtoupper($p_text); ?>" height="35">     25/03/2013 -->
        	<IMG SRC="<? echo $dest;?>" ALT="<? echo strtoupper($p_text); ?>" height="65">
		&nbsp;
	<? } ?>
	<!--<br>-->
	</TD>
</TR>
<TR>
	<TD colspan=2 align=center>
		<font size="-2" face="Arial, Helvetica, sans-serif"><strong>REGISTRO N&deg;</strong></font><strong> <font face="Arial, Helvetica, sans-serif" size="-1"><? echo $maximo;?></font></strong>
	</TD>
</TR>
<!--<TR>
	<TD colspan=2 align=center>
		<strong>Contrase&ntilde;a P&aacute;gina Web:</strong> <? echo $clave;?>
	</TD>
</TR>-->
<TR>
	<TD colspan=2 align=left>
		<font size="-3" face="Arial, Helvetica, sans-serif"><strong>REGISTRADOR:</strong> <strong><? echo $usuario;?></strong></font>
	</TD>
</TR>
<TR>
	<TD colspan=2 align=left>
		<font size="-3" face="Arial, Helvetica, sans-serif"><strong>FECHA:</strong></font><strong> <font size="-3" face="Arial, Helvetica, sans-serif"><? echo $fecha;?></font></strong>
	</TD>
</TR>
<!--<TR>
	<TD colspan=2 align=center>
		<em>Oficina de Administraci&oacute;n Documentaria y Archivo</em>
	</TD>
</TR>-->
<TR>
	<TD colspan=2 align=center><font size="-2" face="Arial, Helvetica, sans-serif"><strong><font size="-2" face="Arial, Helvetica, sans-serif"><? echo $dependencia[$i];?></font></strong></font></TD>
</TR>
<!--<TR>
	<TD colspan=2 align=center>
		<strong><font size="-2">oada@CONVENIO_SITRADOC.gob.pe</font></strong>
	</TD>
</TR>-->
<TR>
	<TD colspan=2 align=center><strong><font size="-2" face="Arial, Helvetica, sans-serif">Folios : <strong><font size="-2" face="Arial, Helvetica, sans-serif"><? echo $folio[$i];?></font></strong></font></strong></TD>
</TR>
</TABLE>
<?php 
}//FIN DEL FOREACH DEPE?>
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
//window.onafterprint = function() {window.close()}   // 04/06/2013
window.onafterprint = function() {return false;}
function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>

</body>
</html>



<? } else { ?>

<HTML>
<HEAD>
<TITLE>Barcode Test Page V 1.1 - Cachique</TITLE>
</HEAD>
<BODY>
<h2 align=center>Bar Code Test<br>
<IMG SRC="wrapper.php?p_text=BAR%20CODE%20TEST" ALT="">
</h2>
<FORM METHOD=POST ACTION="index.php">
<TABLE border=1>
<TR>
	<TD>Text</TD>
	<TD><INPUT TYPE="text" NAME="p_text" VALUE="1234567890" maxlength="20" size=25></TD>
</TR>
<TR>
	<TD>Barcode Type</TD>
	<TD>
		<SELECT NAME="p_bcType">
			<OPTION value="1" SELECTED>Code 39</OPTION>
			<OPTION value="2">Interleave 25</OPTION>
			<OPTION value="3">Standard 25</OPTION>
			<OPTION value="4">Code 93</OPTION>
			<OPTION value="5">Royal Mail 4-State</OPTION>
			<OPTION value="6">PostNet</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Bar Width</TD>
	<TD>
		<SELECT NAME="p_xDim">
			<OPTION value="1">Small</OPTION>
			<OPTION value="2" SELECTED>Medium</OPTION>
			<OPTION value="3">Large</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Bar Height</TD>
	<TD>
		<SELECT NAME="p_charHeight">
			<OPTION value="50">Small</OPTION>
			<OPTION value="100" SELECTED>Medium</OPTION>
			<OPTION value="150">Large</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Wide to Narrow</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_w2n" value="2">x2
		<INPUT TYPE="radio" NAME="p_w2n" value="3" CHECKED>x3
	</TD>
</TR>
<TR>
	<TD>Image Type</TD>
	<TD>
		<SELECT NAME="p_type">
			<OPTION value="1" SELECTED>Png</OPTION>
			<OPTION value="2">Jpg</OPTION>
			<? if (function_exists("imagegif")) { ?>
				<OPTION value="3">Gif</OPTION>
			<? } ?>
			<OPTION value="4">Wbmp</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Label?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_label" value="Y" CHECKED>Yes
		<INPUT TYPE="radio" NAME="p_label" value="N">No
	</TD>
</TR>
<TR>
	<TD>Inverted?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_invert" value="Y">Yes
		<INPUT TYPE="radio" NAME="p_invert" value="N" CHECKED>No
	</TD>
</TR>
<TR>
	<TD>Check Digit?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_checkDigit" value="Y">Yes
		<INPUT TYPE="radio" NAME="p_checkDigit" value="N" CHECKED>No
	</TD>
</TR>
<TR>
	<TD>Rotate?</TD>
	<TD><SELECT NAME="p_rotAngle">
			<OPTION value="0" SELECTED>No</OPTION>
			<OPTION value="1">90 Deg.</OPTION>
			<OPTION value="2">180 Deg.</OPTION>
			<OPTION value="3">270 Deg.</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD colspan=2 align=center>
		<INPUT TYPE="submit" NAME="p_submit" value="CONVERT" >
		<INPUT TYPE="reset" NAME="p_reset" value="RESET">
	</TD>
</TR>
</TABLE>
</FORM>
<h5>Read The <a href="notes.txt">Notes</a> file!!!</h5>

</BODY>
</HTML>

<? } ?>
