<? 
/*
	AUTHOR: Walter Cattebeke
	DATE: 08-July-2004
	EMAIL: cachiweb@telesurf.com.py
	LICENSE: This code is free. You can use it and/or modify it.
		I only want to be mentioned and notified if you intend to do either.

	Please read the notes.txt !
*/
error_reporting(E_PARSE);
	require("core.php");
?>


<? 
$p_submit= ($_POST['p_submit']) ? $_POST['p_submit'] : $_GET['p_submit'];
$p_text= ($_POST['p_text']) ? $_POST['p_text'] : $_GET['p_text'];
$p_bcType= ($_POST['p_bcType']) ? $_POST['p_bcType'] : $_GET['p_bcType'];
$p_xDim= ($_POST['p_xDim']) ? $_POST['p_xDim'] : $_GET['p_xDim'];

$p_charHeight= ($_POST['p_charHeight']) ? $_POST['p_charHeight'] : $_GET['p_charHeight'];

$p_w2n= ($_POST['p_w2n']) ? $_POST['p_w2n'] : $_GET['p_w2n'];
$p_type= ($_POST['p_type']) ? $_POST['p_type'] : $_GET['p_type'];
$p_label= ($_POST['p_label']) ? $_POST['p_label'] : $_GET['p_label'];
$p_invert= ($_POST['p_invert']) ? $_POST['p_invert'] : $_GET['p_invert'];
$p_checkDigit= ($_POST['p_checkDigit']) ? $_POST['p_checkDigit'] : $_GET['p_checkDigit'];
$p_rotAngle= ($_POST['p_rotAngle']) ? $_POST['p_rotAngle'] : $_GET['p_rotAngle'];

$notificacion= ($_POST['notificacion']) ? $_POST['notificacion'] : $_GET['notificacion'];
$acto= ($_POST['acto']) ? $_POST['acto'] : $_GET['acto'];
$destinatario= ($_POST['destinatario']) ? $_POST['destinatario'] : $_GET['destinatario'];
$domicilio= ($_POST['domicilio']) ? $_POST['domicilio'] : $_GET['domicilio'];
$distrito= ($_POST['distrito']) ? $_POST['distrito'] : $_GET['distrito'];

//echo "x".$p_text."x";

	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}

				$sql_st="select d.hecho,
						embarcacion=(select case nombrE_emb when 'PRISCILA' THEN 'S/D' ELSE 
NOMBRE_EMB END  from db_dnepp.user_dnepp.embarcacionnac where id_emb=d.id_embarcacion),d.codigo_procedencia,DB_SANCION.DBO.F_DENUNCIA_INFRA(d.id_denuncia)
						from db_sancion.dbo.denuncia d,dbo.correspondencia noti
						where noti.id=d.codigo_documento_notificacion and
							noti.id=$p_text";
							
			if(!mssql_query($sql_st,$con)){
					echo "error en la consulta";
				}else{
					$resultadoDepe=mssql_query($sql_st,$con);
					$num_camposDepe=mssql_num_fields($resultadoDepe);
					$num_registrosDepe=mssql_num_rows($resultadoDepe);
					$j=$num_registrosDepe-1;
					$jj=-1;
						while($datosDepe=mssql_fetch_array($resultadoDepe)){
								$jj++;
								for($i=0;$i<$num_camposDepe;$i++){
									$arregloDepe[$jj][$i]=$datosDepe[$i];
								}//arreglo final que se entrega pero este caso es para 
						}
				}			
			
			for($b=0;$b<$num_registrosDepe;$b++){
					$hechos = $arregloDepe[$b][0];
					$embarcacion = $arregloDepe[$b][1];
					$codigoProcedencia = $arregloDepe[$b][2];
					$fechaInfraccion = $arregloDepe[$b][3];
			}
			
			$sql5= "select i.descripcion + ' ' + i.tipificacion

            from db_sancion.dbo.denuncia  d,db_sancion.dbo.denuncia_infraccion di,db_sancion.dbo.infraccion i,db_tramite_documentario.dbo.correspondencia co

            where  di.id_denuncia=d.id_denuncia and d.codigo_documento_notificacion=co.id and 

            di.id_infraccion=i.id and co.id=$p_text";
			
			if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado=mssql_query($sql5,$con);
					$num_campos=mssql_num_fields($resultado);
					$num_registros=mssql_num_rows($resultado);
					$j=$num_registros-1;
					$jj=-1;
						while($datos=mssql_fetch_array($resultado)){
								$jj++;
								for($i=0;$i<$num_campos;$i++){
									$arreglo[$jj][$i]=$datos[$i];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
			$infraccion="";	
			for($c=0;$c<$num_registros;$c++){
					$infraccion .= $arreglo[$c][0];
			}							
			   
			$sql7= "select substring(dd.indicativo_oficio,1,28),convert(varchar(10),dd.auditmod,103) 
						from db_sancion.dbo.denuncia d,db_tramite_documentario.dbo.documento dd,
							 db_tramite_documentario.dbo.correspondencia noti
						where dd.id_documento=d.codigo_doc_procedencia 
						AND d.codigo_documento_notificacion=noti.id and noti.id=$p_text";
						
			if(!mssql_query($sql7,$con)){
					echo "error en la consulta";
				}else{
					$resultado2=mssql_query($sql7,$con);
					$num_campos2=mssql_num_fields($resultado2);
					$num_registros2=mssql_num_rows($resultado2);
					$j=$num_registros2-1;
					$jj=-1;
						while($datos2=mssql_fetch_array($resultado2)){
								$jj++;
								for($i=0;$i<$num_campos2;$i++){
									$arreglo2[$jj][$i]=$datos2[$i];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
			for($d=0;$d<$num_registros2;$d++){
					$infSISESAT= $arreglo2[$d][0];
					$fecSISESAT= $arreglo2[$d][1];
				if($codigoProcedencia==4){
					$infSISESAT=$infSISESAT."-Dsvs-Sisesat";
				}					
			}							
							   
						

if ($p_submit == "CONVERT") {?>

<?
	
	if (!isset($p_text)){
		$p_textEnc = "1234567890";
	} else {
		$p_textEnc = rawurlencode($p_text);
	}

	if (!isset($p_xDim)){
		$p_xDim = 1;
	}
	if (!isset($p_w2n)){
		$p_w2n = 3;
	}
	if (!isset($p_charHeight)){
		$p_charHeight = 50;
	}
	
	$p_charGap = $p_xDim;


	$msg="";
	if (!safeStr($p_bcType, $p_text)){
		$msg = "Non-Valid characters inside string";
	}

	$dest = "wrapper.php?p_bcType=$p_bcType&p_text=$p_textEnc" . 
		"&p_xDim=$p_xDim&p_w2n=$p_w2n&p_charGap=$p_charGap&p_invert=$p_invert&p_charHeight=$p_charHeight" .
		"&p_type=$p_type&p_label=$p_label&p_rotAngle=$p_rotAngle&p_checkDigit=$p_checkDigit"
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sitra {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #003366;
	background-color: #FEF7C1;
	border: 1px #E9E9E9;
}
.Estilo1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; font-weight: bold; }
-->
</style>

</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="4" class="texto"><div align="center"><strong>C&eacute;dula de Notificaci&oacute;n N&deg;<? echo $notificacion;?>-Dsvs</strong></div></td>
			  </tr>
			  <tr>
				<td colspan="4" class="texto" align="center"><? if ($msg != ""){ ?>
					<? echo $msg; ?>
					<? } else { ?>
						&nbsp;
						<IMG SRC="<? echo $dest;?>" ALT="<? echo strtoupper($p_text); ?>" height="35">
						&nbsp;
					<? } ?></td>
			  </tr>			  
			  <tr>
				<td colspan="4" class="texto"><div align="right"><strong>EXPEDIENTE N&deg;: <? echo $acto;?></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td width="60%">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="20%" class="texto"><strong>Destinatario</strong></td>
				<td colspan="3" class="texto">: <? echo $destinatario;?></td>
			  </tr>
			  <tr>
				<td class="texto"><strong>Domicilio</strong></td>
				<td colspan="3" class="texto">: <? echo $domicilio;?> , <? echo $distrito;?> <? echo $provincia;?> <? echo $departamento;?></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td class="texto"><strong>Entidad</strong></td>
				<td colspan="3" class="texto">:Direcci&oacute;n General de Seguimiento, Control y Vigilancia. MINISTERIO DE LA PRODUCCION</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td class="texto"><strong>Norma que atribuye competencia</strong></td>
				<td colspan="3" class="texto">:Articulo 81&deg; de la Ley General de Pesca  y el Decreto Supremo 016-2007-Produce Art&iacute;culo 26 literal C)</td>
			  </tr>
			  <tr>
				<td class="texto"><strong>Domicilio Entidad</strong> </td>
				<td colspan="2" class="texto">:Calle Uno Oeste # 60 Piso 5 Urbanizaci&oacute;n Corpac San Isidro</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td class="texto"><strong>Materia</strong></td>
				<td colspan="3" class="texto">:Procedimiento Administrativo Sancionador</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td class="texto"><strong>Fecha de infracci&oacute;n</strong> </td>
				<td colspan="2" class="texto">:<?php if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13){ echo "Las indicadas en los Hechos constatados";}else{ echo $fechaInfraccion;} ?></td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td class="texto"><strong>Documentos adjuntos</strong></td>
				<td colspan="3" class="texto">:<?php if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13){ echo "Acta de Inspecci�n";}elseif($codigoProcedencia==4){ echo "Reporte SISESAT N�".$infSISESAT;}else{echo $infSISESAT;}?></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td class="texto">&nbsp;</td>
				<td class="texto">&nbsp;</td>
				<td class="texto">&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
			    <td colspan="4"><div align="center"><strong>DOCUMENTO A NOTIFICAR </strong></div></td>
		      </tr>
			  -->
			  <tr>
			    <td colspan="2" class="texto"><strong>Presuntas infracciones</strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td class="texto">Base Legal:</td>
			    <td class="texto"><? echo $infraccion;?></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" class="texto"><strong>Hechos constatados</strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td class="texto">Los siguientes son los hechos constatados:<br><? echo $hechos;?></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3" class="texto">Plazo: Cinco(05) dias h&aacute;biles, a partir del dia siguiente de recibida la presente notificaci&oacute;n</td>
		      </tr>
			  <tr>
			    <td colspan="2"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" class="texto"><div align="right"><strong>__________________________________________</strong></div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" class="texto"><div align="right"><strong>Director de Seguimiento, Vigilancia y Sanciones&nbsp;&nbsp;&nbsp;&nbsp;</strong></div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" class="texto"><div align="right">Ing. ALEJANDRO RAM&Iacute;REZ SALDA&Ntilde;A &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <!--<tr>
			    <td colspan="2" class="texto"></td>
			    <td class="texto">&nbsp;</td>
		      </tr>-->			  		  			  			  			  
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="3"><!--<div align="center"><strong>Cargo de Entrega</strong></div>--></td>
			  </tr>
			  <tr>
			    <td class="texto">Recibido por _______________________________________</td>
			    <td>&nbsp;</td>
			    <td></td>
		      </tr>
			  <tr>
			    <td class="texto">DNI/Otro _________________________________________</td>
			    <td>&nbsp;</td>
		        <td><span class="texto"><strong>MOTIVO DE LA DEVOLUCI&Oacute;N</strong></span></td>
			  </tr>
			  <tr>
			    <td class="texto">Relaci&oacute;n con el destinatario ___________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">&nbsp;</td>
		      </tr>
			  <tr>
			    <td class="texto">_________________________________________________</td>
			    <td>&nbsp;</td>
			    <td><span class="texto">___Se neg&oacute; a recibir</span></td>
	          </tr>
			  <tr>
			    <td class="texto">Fecha ___________________________________________</td>
			    <td>&nbsp;</td>
			    <td><span class="texto">___No firm&oacute;</span></td>
	          </tr>
			  <tr>
			    <td class="texto">Hora ____________________________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">___Domicilio cerrado</td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td class="texto">___Se mud&oacute;</td>
	          </tr>
			  <tr>
			    <td class="texto">________________________________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">___Domicilio errado (inexistente)</td>
	          </tr>
			  <tr>
			    <td class="texto">Firma y/o sello</td>
			    <td>&nbsp;</td>
			    <td class="texto">___Se encuentra en el extranjero</td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td class="texto">___Persona desaparecida</td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td class="texto">Otros _____________________________________</td>
	          </tr>
			  <tr>
			    <td class="texto">Notificador _______________________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">__________________________________________</td>
		      </tr>
			  <tr>
			    <td class="texto">N&deg; de DNI del notificador ___________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td class="texto">&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td class="texto">_________________________________________________</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td class="texto">Firma del Notificador</td>
			    <td>&nbsp;</td>
			    <td><span class="texto"><strong>CARACTERISTICAS DEL DOMICILIO</strong></span></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td class="texto">&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><span class="texto">Nro. de medidor agua/luz ______________</span></td>
		      </tr>
			  <tr>
			    <td><span class="texto">Observaciones ____________________________________</span></td>
			    <td>&nbsp;</td>
			    <td class="texto">Material y color de la fachada ___________</td>
		      </tr>
			  <tr>
			    <td class="texto">________________________________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">Material y color de la puerta ____________</td>
		      </tr>
			  <tr>
			    <td class="texto">________________________________________________</td>
			    <td>&nbsp;</td>
			    <td class="texto">&nbsp;</td>
		      </tr>
			</table>
		</td>
		</tr>		
		
		</table>
	</td>
  </tr>
  <!--<tr>
    <td>&nbsp;</td>
  </tr>-->
  <!--
  <tr>
    <td>(*) Nota: En este espacio se consignar&aacute;n los documentos a notificar </td>
  </tr>
  <tr>
  	<td>1) Notificaci&oacute;n de Cargos: Requisitos establecidos en art&iacute;culo 17&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC) . A s&iacute; mismo, se consignar&aacute;n los documentos anexados. </td>
  </tr>
  <tr>
  	<td>2) Notificaci&oacute;n de Resoluciones: Requisitos establecidos en el art&iacute;culo 19&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC). </td>
  </tr>
  <tr>
  	<td>3) Comunicaciones con el administrado: Requisitos establecidos en los numerales 132.4 y 169.1 de los art&iacute;culos 132&deg; y 169&deg;, respectivamente, de la Ley N&deg; 27444, Ley del Procedimiento Administrativo General. </td>
  </tr>
  -->
  <tr>
  	<td class="texto">Ver al dorso regimen de beneficios de pago de multas e indicaciones.</td>
  </tr>     
</table>


<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>

</body>
</html>



<? } else { ?>

<HTML>
<HEAD>
<TITLE>Barcode Test Page V 1.1 - Cachique</TITLE>
</HEAD>
<BODY>
<h2 align=center>Bar Code Test<br>
<IMG SRC="wrapper.php?p_text=BAR%20CODE%20TEST" ALT="">
</h2>
<FORM METHOD=POST ACTION="index.php">
<TABLE border=1>
<TR>
	<TD>Text</TD>
	<TD><INPUT TYPE="text" NAME="p_text" VALUE="1234567890" maxlength="20" size=25></TD>
</TR>
<TR>
	<TD>Barcode Type</TD>
	<TD>
		<SELECT NAME="p_bcType">
			<OPTION value="1" SELECTED>Code 39</OPTION>
			<OPTION value="2">Interleave 25</OPTION>
			<OPTION value="3">Standard 25</OPTION>
			<OPTION value="4">Code 93</OPTION>
			<OPTION value="5">Royal Mail 4-State</OPTION>
			<OPTION value="6">PostNet</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Bar Width</TD>
	<TD>
		<SELECT NAME="p_xDim">
			<OPTION value="1">Small</OPTION>
			<OPTION value="2" SELECTED>Medium</OPTION>
			<OPTION value="3">Large</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Bar Height</TD>
	<TD>
		<SELECT NAME="p_charHeight">
			<OPTION value="50">Small</OPTION>
			<OPTION value="100" SELECTED>Medium</OPTION>
			<OPTION value="150">Large</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Wide to Narrow</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_w2n" value="2">x2
		<INPUT TYPE="radio" NAME="p_w2n" value="3" CHECKED>x3
	</TD>
</TR>
<TR>
	<TD>Image Type</TD>
	<TD>
		<SELECT NAME="p_type">
			<OPTION value="1" SELECTED>Png</OPTION>
			<OPTION value="2">Jpg</OPTION>
			<? if (function_exists("imagegif")) { ?>
				<OPTION value="3">Gif</OPTION>
			<? } ?>
			<OPTION value="4">Wbmp</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD>Label?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_label" value="Y" CHECKED>Yes
		<INPUT TYPE="radio" NAME="p_label" value="N">No
	</TD>
</TR>
<TR>
	<TD>Inverted?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_invert" value="Y">Yes
		<INPUT TYPE="radio" NAME="p_invert" value="N" CHECKED>No
	</TD>
</TR>
<TR>
	<TD>Check Digit?</TD>
	<TD>
		<INPUT TYPE="radio" NAME="p_checkDigit" value="Y">Yes
		<INPUT TYPE="radio" NAME="p_checkDigit" value="N" CHECKED>No
	</TD>
</TR>
<TR>
	<TD>Rotate?</TD>
	<TD><SELECT NAME="p_rotAngle">
			<OPTION value="0" SELECTED>No</OPTION>
			<OPTION value="1">90 Deg.</OPTION>
			<OPTION value="2">180 Deg.</OPTION>
			<OPTION value="3">270 Deg.</OPTION>
		</SELECT>
	</TD>
</TR>
<TR>
	<TD colspan=2 align=center>
		<INPUT TYPE="submit" NAME="p_submit" value="CONVERT" >
		<INPUT TYPE="reset" NAME="p_reset" value="RESET">
	</TD>
</TR>
</TABLE>
</FORM>
<h5>Read The <a href="notes.txt">Notes</a> file!!!</h5>
</BODY>
</HTML>

<? } ?>

