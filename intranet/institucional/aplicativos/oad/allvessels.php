<?php

$csv_end ="
";
$csv_sep = ","; 
$Namedate = date("d-m-Y") ;
$csv_file = "/var/www/portal/descarga/".str_replace("-", "", $Namedate). "_allvessels.csv"; 
//$csv_file = "/var/www/portal/descarga/allvessels.csv"; 
$csv=""; 


$sql="SELECT 
     NOMBRE_EMB AS [EMBARCACION],
      MATRICULA_EMB AS [MATRICULA],
      c.DESC_CASCO  as [CASCO],
      R.DESC_REGIMEN AS [REGIMEN],
      TP.DESC_TPRES AS [TIPO PRESEVACION],
      E.ESLORA_EMB  AS [ESLORA],
      E.MANGA_EMB as [MANGA],
      E.PUNTAL_EMB AS [PUNTAL] ,
      CONVERT (DECIMAL (15,3),e.capbod_emb) as [CAPBOD_M3],
      CONVERT (DECIMAL (15,3),e.capbod1_emb) as [CAPBOD_TM],
      Upper(res.numero_res)AS [RESOLUCION],  
      EP.DESC_ESTPER AS [PERMISO_PESCA],
      EZ.DESC_ESTZARP AS [PERMISO_ZARPE],
      e.CODPAG_EMB as [CODIGO PAGO],
      E.TRANSMISOR2_EMB AS [TRANSMISOR],
     
	 replace(
	  DB_DNEPP.USER_DNEPP.f_Nombre_PersonaxEmbarcacion_Vigente(E.ID_EMB) ,',','-') AS [ARMADOR],	   
     
	  DB_GENERAL.DBO.TRAE_PRECINTO_CERPER(e.id_emb) AS [PRECINTO],
      DB_DNEPP.dbo.t_embarcacion_aparejo(E.ID_EMB) AS [APAREJO],
      E.ESPCHI AS [ESPECIE CHI],
      E.ESPCHD AS [ESPECIE CHD],
      DB_DNEPP.DBO.TRAE_PMCE(E.ID_EMB, 5, 1) AS [PMCE NORTE-CENTRO],
      DB_DNEPP.DBO.TRAE_PMCE(E.ID_EMB, 5, 2) AS [PMCE SUR],
      CASE  isnull(DB_GENERAL.desarrollo_prueba.TRAE_EST_EMB_LIMITEESTADO_ACTUAL(E.ID_EMB,'110'),0) 
            WHEN 1 THEN 'NOMINADA'
            WHEN 0 THEN 'NO NOMINADA'
            ELSE 'PARQUEADA' END 
      as [NOMINADA NORTE-CENTRO], /*nomi nc*/  
      CASE isnull(DB_GENERAL.desarrollo_prueba.TRAE_EST_EMB_LIMITEESTADO_ACTUAL(E.ID_EMB,'001'),0) 
            WHEN 1 THEN 'NOMINADA'
            WHEN 0 THEN 'NO NOMINADA'
            ELSE 'PARQUEADA' END     
      AS [NOMINADA SUR], /*nomi sr*/        
      CASE isnull(DB_GENERAL.desarrollo_prueba.TRAE_EST_EMB_ACTIVACION_ACTUAL(E.ID_EMB,'110'),0) 
            WHEN 1 THEN 'ACTIVADO'
            WHEN 0 THEN 'DESACTIVADO'
            ELSE 'SUSPENSION TEMPORAL' END           
      AS [CONVENIO NORTE-CENTRO],    /*convenio nc*/    
      CASE  isnull(DB_GENERAL.desarrollo_prueba.TRAE_EST_EMB_ACTIVACION_ACTUAL(E.ID_EMB,'001'),0) 
            WHEN 1 THEN 'ACTIVADO'
            WHEN 0 THEN 'DESACTIVADO'
            ELSE 'SUSPENSION TEMPORAL' END                 
      AS [CONVENIO SUR],    /*convenio nc*/           isnull(DB_GENERAL.desarrollo_prueba.trae_nomb_grupo_de_emb_actual(DB_GENERAL.desarrollo_prueba.trae_cod_grupo_de_emb_actual(E.ID_EMB,'110'),'110'),'NO AGRUPADA') AS [GRUPO NORTE-CENTRO],
      isnull(DB_GENERAL.desarrollo_prueba.trae_nomb_grupo_de_emb_actual(DB_GENERAL.desarrollo_prueba.trae_cod_grupo_de_emb_actual(E.ID_EMB,'001') ,'001'),'NO AGRUPADA') AS [GRUPO SUR]
from 
      DB_DNEPP.user_dnepp.EMBARCACIONNAC e 
      left join DB_DNEPP.user_dnepp.CASCO  c on e.ID_CASCO=c.ID_CASCO 
      LEFT JOIN DB_DNEPP.user_dnepp.REGIMEN R ON E.ID_REGIMEN =R.ID_REGIMEN 
      LEFT JOIN DB_DNEPP.user_dnepp.TIPOPRESERVACION TP ON E.ID_TPRES =TP.ID_TPRES 
      LEFT JOIN DB_DNEPP.user_dnepp.ESTADOPERMISO EP ON E.ID_ESTPER =EP.ID_ESTPER 
      LEFT JOIN DB_DNEPP.user_dnepp.ESTADOZARPE EZ ON E.ESTADOZARPE_EMB =EZ.ESTADOZARPE_EMB
      LEFT JOIN DB_DNEPP.user_dnepp.resxemb re ON e.id_emb=re.id_emb AND re.estado_resxemb=1
      LEFT JOIN DB_DNEPP.user_dnepp.resolucion res ON re.id_res=res.id_res";


//echo $sql;

$server = '10.10.10.2';

// Connect to MSSQL
$link = mssql_connect($server, CJDBMSSQLUSER, CJDBMSSQLPASS);

if (!$link) {
    die('Something went wrong while connecting to MSSQL');
}else{
	echo "\n conectado";
}

if(mssql_select_db("DB_DNEPP",$link)) echo 'Selected DB: Northwind<BR>';
$result = mssql_query($sql, $link);

$csv.="EMBARCACION,MATRICULA,CASCO,REGIMEN,TIPO PRESEVACION,ESLORA,MANGA,PUNTAL,CAPBOD_M3,CAPBOD_TM,RESOLUCION,PERMISO_PESCA,PERMISO_ZARPE,CODIGO PAGO,TRANSMISOR,ARMADOR,PRECINTO,APAREJO,ESPECIE CHI,ESPECIE CHD,PMCE NORTE-CENTRO,PMCE SUR,NOMINADA NORTE-CENTRO,NOMINADA SUR,CONVENIO NORTE-CENTRO,CONVENIO SUR,GRUPO NORTE-CENTRO,GRUPO SUR
";

while ($row = mssql_fetch_assoc($result)){
	$csv.=$row['EMBARCACION'].$csv_sep.$row['MATRICULA'].$csv_sep.$row['CASCO'].$csv_sep.$row['REGIMEN'].$csv_sep.$row['TIPO PRESEVACION'].$csv_sep.$row['ESLORA'].$csv_sep.$row['MANGA'].$csv_sep.$row['PUNTAL'].$csv_sep.$row['CAPBOD_M3'].$csv_sep.$row['CAPBOD_TM'].$csv_sep.$row['RESOLUCION'].$csv_sep.$row['PERMISO_PESCA'].$csv_sep.$row['PERMISO_ZARPE'].$csv_sep.$row['CODIGO PAGO'].$csv_sep.$row['TRANSMISOR'].$csv_sep.$row['ARMADOR'].$csv_sep.$row['PRECINTO'].$csv_sep.$row['APAREJO'].$csv_sep.$row['ESPECIE CHI'].$csv_sep.$row['ESPECIE CHD'].$csv_sep.$row['PMCE NORTE-CENTRO'].$csv_sep.$row['PMCE SUR'].$csv_sep.$row['NOMINADA NORTE-CENTRO'].$csv_sep.$row['NOMINADA SUR'].$csv_sep.$row['CONVENIO NORTE-CENTRO'].$csv_sep.$row['CONVENIO SUR'].$csv_sep.$row['GRUPO NORTE-CENTRO'].$csv_sep.$row['GRUPO SUR'].$csv_end;
}


//Generamos el csv de todos los datos 
if (!$handle = fopen($csv_file, "w")) { 
	echo "Cannot open file"; 
exit; 
}
if (fwrite($handle, utf8_decode($csv)) === FALSE) { 
	echo "Cannot write to file"; 
exit; 
}
fclose($handle);

mssql_free_result($result);


?>
