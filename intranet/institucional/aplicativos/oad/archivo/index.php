<?
include_once('modulosadmin/claseIntranet.inc.php');

$objIntranet = new Intranet();

$menu = ($_POST['menu']) ? $_POST['menu'] : $_GET['menu'];
$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

include_once('modulosadmin/claseArchivoGeneral.inc.php');
$modAT = new ArchivoGeneral($menu);

if($modAT){
	if ($accion!=$modAT->arr_accion[ARCHIVAR] && $accion!=$modAT->arr_accion[MUESTRA_DETALLE_FLUJODOCDIR]
		&& $accion!=$modAT->arr_accion[IMPRIME_DETALLE_DIR]&& $accion!=$modAT->arr_accion[FRM_ACEPTA_DOC]
		&& $accion!=$modAT->arr_accion[GENERAREPORTE]){
		$objIntranet->Header('Archivo General',false,array('suspEmb'),false,true);
		$objIntranet->Body('tramite_tit.gif',false,false);
	}
	switch ($accion){
		case $modAT->arr_accion[FRM_BUSCA_ARCHIVO] :
			$modAT->FormBuscaArchivo();
			break;
		case $modAT->arr_accion[BUSCA_ARCHIVO] :
			$modAT->BuscaArchivo($_POST['page'],$_POST['stt'],$_POST['nopc']);
			break;
		case $modAT->arr_accion[FRM_BUSCA_ARCHIVO_DEPE] :
			$modAT->FormBuscaArchivoDependencia();
			break;
		case $modAT->arr_accion[BUSCA_ARCHIVO_DEPE] :
			$modAT->BuscaArchivoDependencia($_POST['page'],$_POST['stt'],$_POST['nopc']);
			break;
		case $modAT->arr_accion[FRM_ARCHIVAR] :
			$modAT->FormArchiva(($_POST['nivelI']) ? $_POST['nivelI'] : $_GET['nivelI'],
								($_POST['nivelII']) ? $_POST['nivelII'] : $_GET['nivelII'],
								($_POST['nivelIII']) ? $_POST['nivelIII'] : $_GET['nivelIII']);
			break;
		case $modAT->arr_accion[ARCHIVAR] :
			$modAT->Archiva($_POST['nivelI'],$_POST['nivelII'],$_POST['nivelIII']);
			break;
		case $modAT->arr_accion[MUESTRA_DETALLE_FLUJODOCDIR]:
			$modAT->DetallesFlujoDirector(($_POST['id']) ? $_POST['id'] : $_GET['id'],
			                                 ($_POST['print']) ? $_POST['print'] : $_GET['print']);
			break;
		case $modAT->arr_accion[IMPRIME_DETALLE_DIR]:
			$modAT->ImprimeFlujoDocDir(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modAT->arr_accion[FRM_ACEPTA_DOC]:
			$modAT->AceptaDocumento();
			break;
		case $modAT->arr_accion[FRM_GENERAREPORTE]:
			$modAT->FormGeneraReporte();
			break;
		case $modAT->arr_accion[GENERAREPORTE]:
			$modAT->GeneraReporte($_POST['GrupoOpciones1'],$_POST['mes'],$_POST['anyo']);
			break;
		case $modAT->arr_accion[LISTADOTOTAL]:
			$modAT->ListadoTotal($_POST['a']);
			break;
		case $modAT->arr_accion[BUENA_TRANS]:
			$modAT->MuestraStatTrans($accion);
			break;
		case $modAT->arr_accion[MALA_TRANS]:
			$modAT->MuestraStatTrans($accion);
			break;
		default:
			$modAT->MuestraIndex();
			break;
	}
	if ($accion!=$modAT->arr_accion[ARCHIVAR] && $accion!=$modAT->arr_accion[MUESTRA_DETALLE_FLUJODOCDIR]
		&& $accion!=$modAT->arr_accion[IMPRIME_DETALLE_DIR]&& $accion!=$modAT->arr_accion[FRM_ACEPTA_DOC]
		&& $accion!=$modAT->arr_accion[GENERAREPORTE])
		$objIntranet->Footer(false);
}else{
	$objIntranet->Header('Archivo General',false,array('suspEmb'));
	$objIntranet->Body('tramite_tit.gif',false,false);
	$objIntranet->Footer(false);	
}



?>
