<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=lstembarcaciones.xls");
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">";

include("connstr.php");
connect();
?>
<html>
	<head>
		<title>Resultados</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!-- <link href="/styles/intranet.css" rel="stylesheet" type="text/css"> -->
	</head>
<?php 
$v_NombreEmbarc	= $_REQUEST["nem"];
$v_Matricula	= $_REQUEST["mtr"];
$v_Casco		= $_REQUEST["csc"];
$v_Regimen		= $_REQUEST["rgm"];
$v_Anio			= $_REQUEST["anio"];

?>
<body bgcolor="#FFFFFF">
	<table width="850" height="270" bgcolor="#FFFFFF" border="0" bordercolor="#FF0000" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td valign="top" align="top">				<br>
				<table width="95%" bgcolor="#FFFFFF" border="0" bordercolor="#0000FF" cellspacing="0" cellpadding="1" align="center" class="tabla-login">
					<tr bgcolor="#FFFFFF" class="textoblack">
						<th colspan="9" align="center" class="texto">INFORMACI&Oacute;N DE DESCARGA DE EMBARCACIONES, CORRESPONDIENTES AL A&Ntilde;O <?=$v_Anio?></strong></th>
					</tr>
					<tr><td colspan="9" height="1" bgcolor="#000000"> </td> </tr>
					
					<tr bgcolor="#024467" style="font-size:10px;color:#CCCCCC;font-family:Arial, Helvetica, sans-serif;">
						<td width="100" valign="middle" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')"><strong>ITM</strong></td>
						<td width="100" valign="middle" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')"><strong>NOMBRE DE EMBARCACI&Oacute;N</strong></td>
						<td width="100" valign="middle" align="left" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">MATR&Iacute;CULA</strong></td>
						<td width="100" valign="middle" align="left" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">CASCO</strong></td>
						<td width="130" valign="middle" align="left" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">REGIMEN</strong></td>
						<td width="175" valign="middle" align="left" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')"><strong>ARMADOR</strong></td>
						<td width="90" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">TOTAL<br>CERTIFIC. <br> (TM)</strong></td>
						<td width="90"  align="center"  nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">TOTAL<br>EIP <br> (TM)</strong></td>
						<td width="90"  align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">TOTAL<br>TOLVAS<br> (TM)</strong></td>
						<td width="90"  align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#1c1c1c', startColorstr='#353535', gradientType='0')">TOTAL<br>DER.PESCA<br> (TM)</strong></td>
					</tr>
					<?php
					$ncnt = 0; 
					
					$strSQL = "SELECT ";
					$strSQL = $strSQL ." e.NOMBRE_EMB,";
					$strSQL = $strSQL ." e.MATRICULA_EMB,";
					$strSQL = $strSQL ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 1, ". $v_Anio .") AS TOTAL_CERTIFICADORA,";
					$strSQL = $strSQL ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 2, ". $v_Anio .") AS TOTAL_EIP,";
					$strSQL = $strSQL ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 3, ". $v_Anio .") AS TOTAL_TOLVAS,";
					$strSQL = $strSQL ." DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 4, ". $v_Anio .") AS TOTAL_DERECHO_PESCA,";
					$strSQL = $strSQL ." C.DESC_CASCO AS CASCO,";
					$strSQL = $strSQL ." R.DESC_REGIMEN AS REGIMEN,";
					$strSQL = $strSQL ." desarrollo_prueba.f_Nombre_PersonaxEmbarcacion_momento2011(e.ID_EMB, CONVERT(varchar(10), '31/12/2010',103)) AS PERSONA_ARMADOR";
					$strSQL = $strSQL ." , e.ID_REGIMEN AS ID_REGIMEN";
					$strSQL = $strSQL ." , e.ID_CASCO AS ID_CASCO";
					$strSQL = $strSQL ." , dbo.LASTDATAMOD() AS LASTDATAMOD";
					$strSQL = $strSQL ." FROM user_dnepp.EMBARCACIONNAC AS e";
					$strSQL = $strSQL ." LEFT OUTER JOIN user_dnepp.CASCO AS C ON e.ID_CASCO = C.ID_CASCO ";
					$strSQL = $strSQL ." LEFT OUTER JOIN user_dnepp.REGIMEN AS R ON e.ID_REGIMEN = R.ID_REGIMEN";
					$strSQL = $strSQL ." WHERE e.NOMBRE_EMB<>'' ";
					$strSQL = $strSQL ." AND ( (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 1, ". $v_Anio .")>0)";
					$strSQL = $strSQL ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 2, ". $v_Anio .")>0)";
					$strSQL = $strSQL ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 3, ". $v_Anio .")>0)";
					$strSQL = $strSQL ." AND (DB_GENERAL.dbo.trae_datos_descarga(e.ID_EMB, 4, ". $v_Anio .")>0) )";
					if ($v_Matricula!="") { 
						$strSQL = $strSQL ." AND e.MATRICULA_EMB LIKE '%". $v_Matricula ."%' "; 
					}
					if ($v_Regimen!="") { 
						$strSQL = $strSQL ." AND e.ID_REGIMEN=". $v_Regimen; 
					}
					if ($v_Casco!="") { 
						$strSQL = $strSQL ." AND e.ID_CASCO=". $v_Casco; 	
					}					
					$result = mssql_query($strSQL);
					if (mssql_num_rows($result)){ 
						while ($row = @mssql_fetch_array($result)) { 
							$ncnt 		= $ncnt + 1;
							
							$v_NOMBRE_EMB		= substr(trim($row[0]), 0, 17);
							$v_MATRICULA_EMB	= trim($row[1]);
							$v_TOT_DESC_CERT	= number_format( $row[2], 2, '.', ',');
							$v_TOTSISTPESCA_EIP	= number_format( $row[3], 2, '.', ',');
							$v_TOT_DESC_TOLVAS	= number_format( $row[4], 2, '.', ',');
							$v_TOTAL_DER_PESCA	= number_format( $row[5], 2, '.', ',');
							$v_CASCO			= trim($row[6]);
							$v_REGIMEN			= trim($row[7]);
							$v_PERSONA_ARMADOR	= substr(trim($row[8]), 0, 25);
							$v_LASTDATAMOD		= date("d-m-Y h:m:s", strtotime($row[9]) ) ;
						?>
							<tr  bgcolor="#F9EFD6" style="font-size:10px;color:#000000;font-family:Arial, Helvetica, sans-serif;">
								<td align="left" class="texto-sitradoc" width="120" valign="middle"> &nbsp; <?=$ncnt?> &nbsp;</td>
								<td align="left" class="texto-sitradoc" width="120" valign="middle"> &nbsp; <?=$v_NOMBRE_EMB?> &nbsp;</td>
								<td align="left" class="texto-sitradoc" valign="middle"> &nbsp; <?=$v_MATRICULA_EMB?> &nbsp;</td>
								<td align="left" class="texto-sitradoc" valign="middle"> &nbsp; <?=$v_CASCO?> &nbsp;</td>
								<td align="left" class="texto-sitradoc" valign="middle"> &nbsp;<?=$v_REGIMEN?> &nbsp;</td>
								<td align="left" class="texto-sitradoc" valign="middle"> <?=$v_PERSONA_ARMADOR?> &nbsp; </td>								
								<td align="right" class="texto-sitradoc" valign="middle">  &nbsp; <?=$v_TOT_DESC_CERT?> &nbsp;</td>
								<td align="right" class="texto-sitradoc" valign="middle"> &nbsp;<?=$v_TOTSISTPESCA_EIP?> &nbsp;</td>
								<td align="right" class="texto-sitradoc" valign="middle"> &nbsp; <?=$v_TOT_DESC_TOLVAS?> &nbsp;</td>
								<td align="right" class="texto-sitradoc" valign="middle"> &nbsp; <?=$v_TOTAL_DER_PESCA?> &nbsp; </td>
							</tr>
							<tr> <td colspan="9" height="1" bgcolor="#CCCCCC"> </td> </tr>
						<?php
						}
					}
					?>
					<tr><td colspan="9" height="10"> </td> </tr>
					<tr> <td colspan="9" height="1" bgcolor="#CCCCCC"> </td> </tr>										
					<tr> <td colspan="9" height="4"> </td> </tr>					
					<tr>
						<td colspan="9" align="left" class="texto">
							<font color="#333333" size=1>
								* S�lo se listan las 10 primeras coincidencias que tienen datos en Totales...
								
								<br>
								* Fuentes Utilizadas: 
								&nbsp;&nbsp; Certificadoras SGS y CERPER, Sistemas de Pesca y Descargas y DDJJ de los Armadores
								
								<br>
								* Fecha de Actualizaci�n: 
								&nbsp;&nbsp; <?=$v_LASTDATAMOD?>
								
								
								</font></td>
					</tr>
					<tr> <td colspan="9" height="2"> </td> </tr>					
					<tr> <td colspan="9" height="1" bgcolor="#CCCCCC"> </td> </tr>																						
						</table>
			</td>			
		</tr>
					<tr><td colspan="9" height="5"> </td> </tr>
				</table>
				
</td>
		</tr>
	</table>
		 
</body>
</html>
<script language="javascript" type="text/javascript">window.close();</script>
