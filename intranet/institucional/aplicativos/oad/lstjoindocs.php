<?
function connect(){
	$hostname_sys = CJDBMSSQL;
	$database_sys = "DB_TRAMITE_DOCUMENTARIO";
	$username_sys = CJDBMSSQLUSER;
	$password_sys = CJDBMSSQLPASS;
	$connect = mssql_connect($hostname_sys, $username_sys, $password_sys);
	if(!$connect){ 
		echo "Error en la conexion hacia la base de datos...<br>". $connect ;
	} else { 
		if(!(mssql_select_db($database_sys,$connect))) { 
			echo "Error al seleccionar la base de datos... <br>"; 
		} else {  
			
		}	
	}
}
?>
<?php include('header.php');?>
<?php
connect();
?>
<html>
	<head>
		<title>Resultados</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<!-- <script src="jquery.js"></script> -->
		
		<script>
			if(history.length>0)history.go(+1);window.history.forward(1);
			
			function fn_putdad($xdata1, $xdata2, $xdata3, $xdata4, $xdata5) {
				window.document.frm.txtID_DOC.value 		= "";
				window.document.frm.txtID_MOVDOC.value		= "";
				window.document.frm.txtID_OFICIO.value 		= "";
				window.document.frm.txtNUM_TRAM_DOC.value 	= "";
				window.document.frm.txtAUDIT_MOD.value 		= "";
			
				window.document.frm.txtID_DOC.value 		= $xdata1;
				window.document.frm.txtID_MOVDOC.value		= $xdata2;
				window.document.frm.txtID_OFICIO.value 		= $xdata3;
				window.document.frm.txtNUM_TRAM_DOC.value 	= $xdata4;
				window.document.frm.txtAUDIT_MOD.value 		= $xdata5;
			}
			
			function fn_putson($ydata1, $ydata2, $ydata3, $ydata4, $ydata5) {
				window.document.frm.txtID_DOC2.value 		= "";
				window.document.frm.txtID_MOVDOC2.value		= "";
				window.document.frm.txtID_OFICIO2.value 	= "";
				window.document.frm.txtNUM_TRAM_DOC2.value 	= "";
				window.document.frm.txtAUDIT_MOD2.value 	= "";
			
				window.document.frm.txtID_DOC2.value 		= $ydata1;
				window.document.frm.txtID_MOVDOC2.value		= $ydata2;
				window.document.frm.txtID_OFICIO2.value 	= $ydata3;
				window.document.frm.txtNUM_TRAM_DOC2.value 	= $ydata4;
				window.document.frm.txtAUDIT_MOD2.value 	= $ydata5;
			}						
			
			function fn_join2to1(){
				//DOC UNO(1)
				var $xID_DOC 		= window.document.frm.txtID_DOC.value;
				var $xID_MOVDOC		= window.document.frm.txtID_MOVDOC.value;
				var $xID_OFICIO 	= window.document.frm.txtID_OFICIO.value;
				var $xNUMTRAM_DOC 	= window.document.frm.txtNUM_TRAM_DOC.value;
				var $xAUDIT_MOD 	= window.document.frm.txtAUDIT_MOD.value;
				
				//DOC DOS(2)
				var $yID_DOC2 		= window.document.frm.txtID_DOC2.value;
				var $yID_MOVDOC2 	= window.document.frm.txtID_MOVDOC2.value;
				var $yID_OFICIO2 	= window.document.frm.txtID_OFICIO2.value;
				var $yNUMTRAM_DOC2 	= window.document.frm.txtNUM_TRAM_DOC2.value;
				var $xAUDIT_MOD2 	= window.document.frm.txtAUDIT_MOD2.value;
				
				//DEL RESULTADO DE LA BUSQUEDA
				var $xTipoDocDad_O 	= window.document.frm.cmbTipoDocDad_O.value;
				var $xAnioDad_O 	= window.document.frm.cmbAnioDad_O.value;
				var $xNumTramDad_O 	= window.document.frm.txtNumTramDad_O.value;
				var $xCoddepDad_O 	= window.document.frm.cmbCoddepDad_O.value;
				
				var $xTipoDocSon_S 	= window.document.frm.cmbTipoDocSon_S.value;
				var $xAnioSon_S		= window.document.frm.cmbAnioson_S.value;
				var $xNumTramSon_S 	= window.document.frm.txtNumTramSon_S.value;
				var $xCoddepSon_S 	= window.document.frm.cmbCoddepSon_S.value;				
				
				if($yID_DOC2!="" && $yID_MOVDOC2!=""){
					if ($xAUDIT_MOD2>$xAUDIT_MOD) {
						
						param = "mode=sv";
						param = param + "&id_doc1="		+ $xID_DOC;
						param = param + "&id_movdoc1="	+ $xID_MOVDOC;
						param = param + "&id_oficio1="	+ $xID_OFICIO;
						param = param + "&id_doc2="		+ $yID_DOC2;
						param = param + "&id_movdoc2="	+ $yID_MOVDOC2;
						param = param + "&id_oficio2="	+ $yID_OFICIO2;
						param = param + "&cmbTipoDocDad_O="	+ $xTipoDocDad_O;
						param = param + "&cmbAnioDad_O="+ $xAnioDad_O;
						param = param + "&txtNumTramDad_O="+ $xNumTramDad_O;
						param = param + "&cmbCoddepDad_O="+ $xCoddepDad_O;
						param = param + "&cmbTipoDocSon_S="	+ $xTipoDocSon_S;
						param = param + "&cmbAnioSon_S="+ $xAnioSon_S;
						param = param + "&txtNumTramSon_S="+ $xNumTramSon_S;
						param = param + "&cmbCoddepSon_S="+ $xCoddepSon_S;
						
						window.location.href='datjoindocs.php?'+param;
						
				}else{
					alert("El DOC PADRE(1) DEBE SER MAS ANTIGUO QUE EL DOC HIJO(2), VERIFIQUE...");
					//DOC UNO(1)
					window.document.frm.txtID_DOC.value			= "";
					window.document.frm.txtID_MOVDOC.value		= "";
					window.document.frm.txtID_OFICIO.value		= "";
					window.document.frm.txtNUM_TRAM_DOC.value	= "";
					window.document.frm.txtAUDIT_MOD.value		= "";
					//DOC DOS(2)
					window.document.frm.txtID_DOC2.value		= "";
					window.document.frm.txtID_MOVDOC2.value		= "";
					window.document.frm.txtID_OFICIO2.value		= "";
					window.document.frm.txtNUM_TRAM_DOC2.value	= "";
					window.document.frm.txtAUDIT_MOD2.value		= "";
				}
			}else{
				alert('DEBE ELEGIR UN MOVIMIENTO DOCUMENTARIO... 2');
			}	
		}
			
			<!-- 
			var letters=' ABC�DEFGHIJKLMN�OPQRSTUVWXYZabc�defghijklmn�opqrstuvwxyz������������������������' 
			var numbers='1234567890' 
			var signs=',.:;@-\'' 
			var mathsigns='+-=()*/' 
			var custom='<>#$%&?�' 
			function alpha(e,allow) { 
				var k; 
				k=document.all?parseInt(e.keyCode): parseInt(e.which); 
				return (allow.indexOf(String.fromCharCode(k))!=-1); 
				
			}
			// --> 								
		</script>
				
</head>
<div id="tooltip" style="position:absolute;visibility:hidden;"></div>
<body>
<form name="frm" id="frm" method="post"  action="lstjoindocs.php">
	<table width="100%" height="270" bgcolor="#FFFFFF" border="0" bordercolor="#FF0000" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td valign="top" align="top">
				<table width="850" bgcolor="#FFFFFF" border="0" bordercolor="#0000FF" cellspacing="0" cellpadding="0" align="center">
				
					<tr><td colspan="6" height="5"> </td> </tr>
					<tr bgcolor="#FFFFFF" class="textoblack">
						<td colspan="6" align="left" class="textoblack">
							<strong>
							<a href="https:///intranet.php">Ir al Inicio</a>
							<strong>
						</td>
					</tr>
					<tr><td colspan="6" height="7"> </td> </tr>
					<tr><td colspan="6" height="1" bgcolor="#000000"> </td> </tr>
					<tr><td colspan="6" height="3"> </td> </tr>
					<tr bgcolor="#FFFFFF" class="textoblack">
						<th colspan="6" align="left" class="texto2">
						<strong><font color="#BB0000">UNI&Oacute;N DE DOCUMENTOS INTERNOS</font></strong></th>
					</tr>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr><td colspan="6" height="1" bgcolor="#000000"> </td> </tr>
					
					
					<tr bgcolor="#DAE1E7"><td colspan="6" height="5"> </td> </tr>
					<tr bgcolor="#DAE1E7">
						<td colspan="6" align="left" class="texto2">
						<?php
							$v_CoddepDad	= $_REQUEST["cmbCoddepDad"];
							$v_TipoDocDad	= $_REQUEST["cmbTipoDocDad"];
							$v_AnioDad		= $_REQUEST["cmbAnioDad"];
							$v_Pick_NumTramD= $_REQUEST["txtNumTramDad"];
							$v_NumTramDad	= substr($_REQUEST["txtNumTramDad"]+100000, 1, 5) ."-". $v_AnioDad ."-MIDIS/"; 
						?>
						
							<input type="hidden" name="cmbTipoDocDad_O" id="cmbTipoDocDad_O" value="<?=$_REQUEST["cmbTipoDocDad"]?>">
							<input type="hidden" name="cmbAnioDad_O" id="cmbAnioDad_O" value="<?=$_REQUEST["cmbAnioDad"]?>">
							<input type="hidden" name="txtNumTramDad_O" id="txtNumTramDad_O" value="<?=$_REQUEST["txtNumTramDad"]?>">
							<input type="hidden" name="cmbCoddepDad_O" id="cmbCoddepDad_O" value="<?=$_REQUEST["cmbCoddepDad"]?>">
						
						
							&nbsp;&nbsp;
							<strong><font color="#000000">DOC. PADRE (1):</font></strong>
							&nbsp;&nbsp;
							<select name="cmbTipoDocDad" id="cmbTipoDocDad" class="texto2" style="width:auto;font-family:Arial, Helvetica, sans-serif;font-size:9px;">
								<option value="">TIPO DOC.</option>
								<?php
								$strSQLTDDad = "SELECT ID_CLASE_DOCUMENTO_INTERNO, UPPER(RTRIM(LTRIM(DESCRIPCION))), CATEGORIA";
								$strSQLTDDad = $strSQLTDDad ." FROM dbo.CLASE_DOCUMENTO_INTERNO";
								$strSQLTDDad = $strSQLTDDad ." WHERE PROCEDENCIA='I' AND CATEGORIA='D'";
								$strSQLTDDad = $strSQLTDDad ." ORDER BY 2";
								$resultTDDad = mssql_query($strSQLTDDad);
								if (mssql_num_rows($resultTDDad)){ 
									while ($rowTDDad = @mssql_fetch_array($resultTDDad)) { 
										$v_id_clase_doc_intDad	= $rowTDDad[0];
										$v_descripcionDad		= $rowTDDad[1];
										$v_categoriaDad			= $rowTDDad[2];
										?> <option value="<?=$v_id_clase_doc_intDad?>" <?php if ($v_TipoDocDad==$v_id_clase_doc_intDad) { ?> selected <?php } ?>><?=substr($v_descripcionDad, 0, 20) ." (". $v_categoriaDad .")" ?></option> <?php
									}
								}
								?>								
							</select>							
							
							&nbsp;&nbsp;
							N� TRAM.DOC.:
							<input type="text" name="txtNumTramDad" id="txtNumTramDad" value="<?=$v_Pick_NumTramD?>" size="12" maxlength="8" class="texto2"
								onKeyPress="return alpha(event,numbers)">
							-
							<select name="cmbAnioDad" id="cmbAnioDad" class="texto2" style="width:auto;">
								<option value="">A�O</option>
								<?php 
								$yearD=date("Y");
								for ($i=2008;$i<=$yearD;$i++) {
									?><option value="<?=$i?>" <?php if ($v_AnioDad==$i) { ?> selected <?php } ?>><?=$i?></option><?php
								}
								?>
							</select>
							
							&nbsp;&nbsp;
							<select name="cmbCoddepDad" id="cmbCoddepDad" class="texto2" style="width:auto;font-family:Arial, Helvetica, sans-serif;font-size:9px;">
								<option value="">-- DEPENDENCIA --</option>
								<?php
								$strSQLDepDad = "SELECT CODIGO_DEPENDENCIA, UPPER(LTRIM(RTRIM(SIGLAS))) AS SIGLAS";
								$strSQLDepDad = $strSQLDepDad ." FROM DB_GENERAL.dbo.H_DEPENDENCIA";
								$strSQLDepDad = $strSQLDepDad ." WHERE CONDICION='ACTIVO'";
								$strSQLDepDad = $strSQLDepDad ." ORDER BY 2";
								$resultDepDad = mssql_query($strSQLDepDad);
								if (mssql_num_rows($resultDepDad)){ 
									while ($rowDepDad = @mssql_fetch_array($resultDepDad)) { 
										$v_cod_depenDad	= $rowDepDad[0];
										$v_aliasDad		= $rowDepDad[1];
										?> <option value="<?=$v_cod_depenDad?>" <?php if ($v_CoddepDad==$v_cod_depenDad) { ?> selected <?php } ?>><?=substr($v_aliasDad, 0, 17) ." (". $v_cod_depenDad .")" ?></option> <?php
									}
								}
								?>
							</select>
							
							
					  </td>
					</tr>
					<tr bgcolor="#DAE1E7"><td colspan="6" height="3"> </td> </tr>
					<tr> <td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>
										<tr bgcolor="#F9F1DE"><td colspan="6" height="3"> </td> </tr>
					<tr bgcolor="#F9F1DE">
						<td colspan="6" align="left" class="texto2">
						<?php
							$v_CoddepSon	= $_REQUEST["cmbCoddepSon"];
							$v_TipoDocSon	= $_REQUEST["cmbTipoDocSon"];
							$v_AnioSon		= $_REQUEST["cmbAnioSon"];
							$v_Pick_NumTramS= $_REQUEST["txtNumTramSon"];
							$v_NumTramSon	= substr($_REQUEST["txtNumTramSon"]+100000, 1, 5) ."-". $_REQUEST["cmbAnioSon"] ."-MIDIS/"; 
						?>
						
							<input type="hidden" name="cmbTipoDocSon_S" id="cmbTipoDocSon_S" value="<?=$_REQUEST["cmbTipoDocSon"]?>">
							<input type="hidden" name="cmbAnioSon_S" id="cmbAnioson_S" value="<?=$_REQUEST["cmbAnioSon"]?>">
							<input type="hidden" name="txtNumTramSon_S" id="txtNumTramSon_S" value="<?=$_REQUEST["txtNumTramSon"]?>">
							<input type="hidden" name="cmbCoddepSon_S" id="cmbCoddepSon_S" value="<?=$_REQUEST["cmbCoddepSon"]?>">
							
													
													
							&nbsp;&nbsp;
							<strong><font color="#000000">DOC. HIJO... (2):</font></strong>
							&nbsp;&nbsp;
							<select name="cmbTipoDocSon" id="cmbTipoDocSon" class="texto2" style="width:auto;font-family:Arial, Helvetica, sans-serif;font-size:9px;">
								<option value="">TIPO DOC.</option>
								<?php
								$strSQLTDSon = "SELECT ID_CLASE_DOCUMENTO_INTERNO, UPPER(RTRIM(LTRIM(DESCRIPCION))), CATEGORIA";
								$strSQLTDSon = $strSQLTDSon ." FROM dbo.CLASE_DOCUMENTO_INTERNO";
								$strSQLTDSon = $strSQLTDSon ." WHERE PROCEDENCIA='I' AND CATEGORIA='D'";
								$strSQLTDSon = $strSQLTDSon ." ORDER BY 2";
								$resultTDSon = mssql_query($strSQLTDSon);
								if (mssql_num_rows($resultTDSon)){ 
									while ($rowTDSon = @mssql_fetch_array($resultTDSon)) { 
										$v_id_clase_doc_intSon	= $rowTDSon[0];
										$v_descripcionSon		= $rowTDSon[1];
										$v_categoriaSon			= $rowTDSon[2];
										?> <option value="<?=$v_id_clase_doc_intSon?>" <?php if ($v_TipoDocSon==$v_id_clase_doc_intSon) { ?> selected <?php } ?> ><?=substr($v_descripcionSon, 0, 20) ." (". $v_categoriaSon .")" ?></option> <?php
									}
								}
								?>								
							</select>							
							
							&nbsp;&nbsp;
							N� TRAM.DOC.:
							<input type="text" name="txtNumTramSon" id="txtNumTramSon" value="<?=$v_Pick_NumTramS?>" size="12" maxlength="8" class="texto2"
								onKeyPress="return alpha(event,numbers)">
							-
							<select name="cmbAnioSon" id="cmbAnioSon" class="texto2" style="width:auto;">
								<option value="">A�O</option>
								<?php
								$yearS=date("Y");
								for ($j=2008;$j<=$yearS;$j++) {
									?><option value="<?=$j?>" <?php if ($v_AnioSon==$j) { ?> selected <?php } ?>><?=$j?></option><?php
								}
								?>
							</select>
							
							&nbsp;&nbsp;
							<select name="cmbCoddepSon" id="cmbCoddepSon" class="texto2" style="width:auto;font-family:Arial, Helvetica, sans-serif;font-size:9px;">
								<option value="">-- DEPENDENCIA --</option>
								<?php
								$strSQLDepSon = "SELECT CODIGO_DEPENDENCIA, UPPER(LTRIM(RTRIM(SIGLAS))) AS SIGLAS";
								$strSQLDepSon = $strSQLDepSon ." FROM DB_GENERAL.dbo.H_DEPENDENCIA";
								$strSQLDepSon = $strSQLDepSon ." WHERE CONDICION='ACTIVO'";
								$strSQLDepSon = $strSQLDepSon ." ORDER BY 2";
								$resultDepSon = mssql_query($strSQLDepSon);
								if (mssql_num_rows($resultDepSon)){ 
									while ($rowDepSon = @mssql_fetch_array($resultDepSon)) { 
										$v_cod_depenSon	= $rowDepSon[0];
										$v_aliasSon		= $rowDepSon[1];
										?> <option value="<?=$v_cod_depenSon?>" <?php if ($v_CoddepSon==$v_cod_depenSon) { ?> selected <?php } ?>><?=substr($v_aliasSon, 0, 17) ." (". $v_cod_depenSon .")" ?></option> <?php
									}
								}
								?>
							</select>
							
							
					  </td>
					</tr>
					<tr bgcolor="#F9F1DE"><td colspan="6" height="5"> </td> </tr>
					<tr> <td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>					
					<tr><td colspan="6" height="5"> </td> </tr>
					<tr bgcolor="#FFFFFF">
						<td colspan="6" align="right" class="texto2">
							<input type="submit" name="btnBuscar" id="btnBuscar" value="BUSCAR" class="texto2">
						</td>
					</tr>
					<?php 
					$ncnt_SD = 0; 
					
					$strSQL_SD = "EXEC dbo.usp_ListaDocsINT '". $v_TipoDocDad ."', '". $v_CoddepDad ."', '". $v_NumTramDad ."'";
					$result_SD = mssql_query($strSQL_SD);
					if (mssql_num_rows($result_SD)){ 
					?>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr bgcolor="#FFFFFF">
						<td colspan="6" align="left" class="texto2">
							<strong><font color="#223399">RESULTADOS DOC. PADRE (1):</font></strong>
						</td>
					</tr>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr> <td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>											
					<tr bgcolor="#DAE1E7">
						<td align="center" class="texto2" colspan="6">
							<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr bgcolor="#024467">
								<td align="center" class="texto"><strong><font color="#FFFFFF">CLASE DOC.</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">INDICATIVO_OFICIO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">ORIGEN</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">DESTINO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">DERIVADO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">RECEPCIONADO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">ENDEFI</font></strong></td>
							</tr>
					
					<?php
						while ($row_SD = @mssql_fetch_array($result_SD)) { 
							$ncnt_SD 		= $ncnt_SD + 1;

							$v_ID_DOCUMENTO				= $row_SD[0];
							$v_ID_OFICIO				= $row_SD[1];							
							$v_ID_MOVIMIENTO_DOCUMENTO	= $row_SD[2];
							$v_NUM_TRAM_DOC_DESC		= $row_SD[3];
							$v_INDICATIVO_OFICIO		= $row_SD[4];
							$v_ASUNTO					= trim($row_SD[5]);
							$v_OBSERVACIONES			= trim($row_SD[6]);
							$v_ID_DEPENDENCIA_ORIGEN	= trim($row_SD[7]);
							$v_SIGLAS_DEPEN_ORIGEN		= trim($row_SD[8]);
							$v_ID_DEPENDENCIA_DESTINO	= $row_SD[9];
							$v_SIGLAS_DEPEN_DESTINO		= trim($row_SD[10]);
							$v_AUDIT_MOD				= $row_SD[11];
							$v_AUDIT_REC				= trim($row_SD[12]);
							$v_ENV_DERIV_FINAL			= $row_SD[13]
						?>
							<?php //if($ncnt_SD==1) { ?> 
							<!-- <tr><td colspan="7" align="left"><font color="#000000">ID_DOCUMENTO: <?=$v_ID_DOCUMENTO?> &nbsp;&nbsp; ID_OFICIO: <?=$v_ID_OFICIO?> </font></td></tr> -->
							<?php //}?>
							<tr 
								onClick="fn_putdad('<?=$v_ID_DOCUMENTO?>', '<?=$v_ID_MOVIMIENTO_DOCUMENTO?>', '<?=$v_ID_OFICIO?>', '<?=$v_INDICATIVO_OFICIO?>', '<?=$v_AUDIT_MOD?>');"
								OnMouseOver="this.style.background='#ACBCCA';this.style.cursor='hand';" 
								onMouseOut="this.style.background='#DAE1E7';"							
								bgcolor="#DAE1E7">
								<td align="left" class="texto" valign="top">
									<font color="#223366"><?=$v_NUM_TRAM_DOC_DESC?></font></td>
								<td align="left" class="texto">
									<font color="#223366"> <?=ereg_replace($v_NumTramDad.$v_SIGLAS_DEPEN_ORIGEN, "<font color=0003FF>". $v_NumTramDad.$v_SIGLAS_DEPEN_ORIGEN ."</font>", $v_INDICATIVO_OFICIO)?> </font>
								</td>
								<td align="right" class="texto"><font color="#223366"><?=$v_SIGLAS_DEPEN_ORIGEN?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_SIGLAS_DEPEN_DESTINO?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_AUDIT_MOD?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_AUDIT_REC?></font></td>
								<td align="center" class="texto"><font color="#223366"><?=$v_ENV_DERIV_FINAL?></font></td>
							</tr>
							<tr> <td colspan="7" height="1" bgcolor="#CCCCCC"> </td> </tr>
						<?php
						}
					}
					?>
						</table>
					</td>
					</tr>
					<?php if ($ncnt_SD>0) {?>
					<tr bgcolor="#CCCCCC">
						<td align="left" class="texto" valign="top">
							<font color="#024467" style="font-size:9px;">
							<strong>
							ID_MOVDOC: <input type="text" name="txtID_MOVDOC" id="txtID_MOVDOC" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							ID_DOC: <input type="text" name="txtID_DOC" id="txtID_DOC" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							ID_OFICIO: <input type="text" name="txtID_OFICIO" id="txtID_OFICIO" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							NUMTRAM_DOC: <input type="text" name="txtNUM_TRAM_DOC" id="txtNUM_TRAM_DOC" value"" size="50" class="texto" style="font-weight:bold;font-size:9px;">
							AUDIT_MOD: <input type="text" name="txtAUDIT_MOD" id="txtAUDIT_MOD" value"" size="18" class="texto" style="font-weight:bold;font-size:9px;">
							</strong>
							</font>
						</td>
					</tr>
					<tr><td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>
					<?php }?>
					
					<tr><td colspan="6" height="5"> </td> </tr>
					
					<?php 
					$ncnt_SS = 0; 
					
					$strSQL_SS = "EXEC dbo.usp_ListaDocsINT '". $v_TipoDocSon ."', '". $v_CoddepSon ."', '". $v_NumTramSon ."'";
					$result_SS = mssql_query($strSQL_SS);
					if (mssql_num_rows($result_SS)){ 
					?>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr bgcolor="#FFFFFF">
						<td colspan="6" align="left" class="texto2">
							<strong><font color="#BB0000">RESULTADOS DOC. HIJO...(2):</font></strong>
						</td>
					</tr>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr> <td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>											
					<tr bgcolor="#DAE1E7">
						<td align="center" class="texto2" colspan="6">
							<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr bgcolor="#804040">
								<td align="center" class="texto"><strong><font color="#FFFFFF">CLASE DOC.</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">INDICATIVO_OFICIO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">ORIGEN</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">DESTINO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">DERIVADO</strong></font></td>
								<td align="center" class="texto"><strong><font color="#FFFFFF">RECEPCIONADO</strong></font></td>
								<td align="center" class="texto"><strong><strong><font color="#FFFFFF">ENDEFI</font></strong></strong></td>
							</tr>
					
					<?php
						while ($row_SS = @mssql_fetch_array($result_SS)) { 
							$ncnt_SS 		= $ncnt_SS + 1;
							
							$v_ID_DOCUMENTO2			= $row_SS[0];
							$v_ID_OFICIO2				= $row_SS[1];							
							$v_ID_MOVIMIENTO_DOCUMENTO2	= $row_SS[2];
							$v_NUM_TRAM_DOC_DESC2		= $row_SS[3];
							$v_INDICATIVO_OFICIO2		= $row_SS[4];
							$v_ASUNTO2					= trim($row_SS[5]);
							$v_OBSERVACIONES2			= trim($row_SS[6]);
							$v_ID_DEPENDENCIA_ORIGEN2	= trim($row_SS[7]);
							$v_SIGLAS_DEPEN_ORIGEN2		= trim($row_SS[8]);
							$v_ID_DEPENDENCIA_DESTINO2	= $row_SS[9];
							$v_SIGLAS_DEPEN_DESTINO2	= trim($row_SS[10]);
							$v_AUDIT_MOD2				= $row_SS[11];
							$v_AUDIT_REC2				= trim($row_SS[12]);
							$v_ENV_DERIV_FINAL			= $row_SS[13];
						?>
						
							<?php //if($ncnt_SS==1) { ?> 
							<!-- <tr><td colspan="7" align="left"><font color="#000000">ID_DOCUMENTO: <?=$v_ID_DOCUMENTO2?> &nbsp;&nbsp; ID_OFICIO: <?=$v_ID_OFICIO2?> </font> </td></tr> -->
							<?php //}?>						
							<tr 
								onClick="fn_putson('<?=$v_ID_DOCUMENTO2?>', '<?=$v_ID_MOVIMIENTO_DOCUMENTO2?>', '<?=$v_ID_OFICIO2?>', '<?=$v_INDICATIVO_OFICIO2?>', '<?=$v_AUDIT_MOD2?>');"
								OnMouseOver="this.style.background='#E6CE97';this.style.cursor='hand';"
								onMouseOut="this.style.background='#F9F1DE';"							
								bgcolor="#F9F1DE">
								<td align="left" class="texto"><font color="#223366"><?=$v_NUM_TRAM_DOC_DESC2?></font></td>
								<td align="left" class="texto">
									<font color="#223366"> <?=ereg_replace($v_NumTramSon.$v_SIGLAS_DEPEN_ORIGEN2, "<font color=red>". $v_NumTramSon.$v_SIGLAS_DEPEN_ORIGEN2 ."</font>", $v_INDICATIVO_OFICIO2)?> </font>
								</td>
								<td align="right" class="texto"><font color="#223366"><?=$v_SIGLAS_DEPEN_ORIGEN2?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_SIGLAS_DEPEN_DESTINO2?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_AUDIT_MOD2?></font></td>
								<td align="right" class="texto"><font color="#223366"><?=$v_AUDIT_REC2?></font></td>
								<td align="center" class="texto"><font color="#223366"><?=$v_ENV_DERIV_FINAL ?></font></td>
							</tr>
							<tr> <td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>
						<?php
						}
					}
					?>		
						</table>
					</td>			
					</tr>					

					<?php if ($ncnt_SS>0) {?>
					<tr bgcolor="#CCCCCC">
						<td align="left" class="texto" valign="top">
							<font color="#024467"  style="font-size:9px;">
							<strong>						
							ID_MOVDOC: <input type="text" name="txtID_MOVDOC2" id="txtID_MOVDOC2" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							ID_DOC: <input type="text" name="txtID_DOC2" id="txtID_DOC2" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							ID_OFICIO: <input type="text" name="txtID_OFICIO2" id="txtID_OFICIO2" value"" size="8" class="texto" style="font-weight:bold;font-size:9px;">
							NUMTRAM_DOC: <input type="text" name="txtNUM_TRAM_DOC2" id="txtNUM_TRAM_DOC2" value"" size="50" class="texto" style="font-weight:bold;font-size:9px;">
							AUDIT_MOD: <input type="text" name="txtAUDIT_MOD2" id="txtAUDIT_MOD2" value"" size="18" class="texto" style="font-weight:bold;font-size:9px;">
							</font>
							</strong>							
						</td>
					</tr>
					<tr><td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>
					<tr><td colspan="6" height="5"> </td> </tr>

					<tr bgcolor="#FFFFFF" class="textoblack">
						<td colspan="6" align="right" class="texto2">
							<input type="button" name="btnMergeOneToTwo" id="btnMergeOneToTwo" value="UNIR DOC HIJO (2) AL DOC PADRE (1)" class="texto" 
								onClick="fn_join2to1();" 
								style="font-weight:bold;font-size:9px;">
						</td>
					</tr>
					<tr><td colspan="6" height="2"> </td> </tr>
					<tr><td colspan="6" height="1" bgcolor="#CCCCCC"> </td> </tr>
					<?php }?>
					<tr><td colspan="6" height="5"> </td> </tr>
				</table>
				
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
<?php include('footer2.php'); ?>