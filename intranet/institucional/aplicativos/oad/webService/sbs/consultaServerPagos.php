<?php
require_once('nusoap/lib/nusoap.php');

//creamos el objeto de tipo soap_server
$server = new soap_server;

//registramos la funci�n que vamos a implementar
$server->register('hello');
$server->register('consultaPagosDerechosPesca');

//implementamos la funci�n
function hello ($name){
return "Hola, $name.";
}

function consultaPagosDerechosPesca($trama){
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))){
		$respuestaTrama= "No existe conexi�n a la base de datos";
	}else{ 
		$respuestaTrama= "Existe conexi�n a la BD";
	}
	if(!(mssql_select_db("DB_DNEPP",$con))){
		$respuestaTrama= "Error al elegir la base de datos";
	}
	
	//Se obtienen las variables:
	$messageTypeIdentification=substr($trama,0,4);
	$primaryBitMap=substr($trama,4,16);
	$secondaryBitMap=substr($trama,20,16);
	$numeroTarjeta=substr($trama,36,18);
	$codigoProceso=substr($trama,54,6);
	$monto=substr($trama,60,12);
	$fechaHoraTransaccion=substr($trama,72,10);
	$trace=substr($trama,82,6);
	$fechaCaptura=substr($trama,88,4);
	$modoIngresoDatos=substr($trama,92,3);
	$canal=substr($trama,95,2);
	$binAdquiriente=substr($trama,97,8);
	$fwdInstitutionCode=substr($trama,105,8);
	$retrievalReferenceNumber=substr($trama,113,12);
	$terminalId=substr($trama,125,8);
	$comercio=substr($trama,133,15);
	$cardAcceptorLocation=substr($trama,148,40);
	$transactionCurrencyCode=substr($trama,188,3);
	$datosReservados=substr($trama,191,5);
	
	$longitudTrama=substr($trama,196,3);//se ha cambiadooooooooooooooooooooo
	$codigoFormato=substr($trama,199,2);
	$binProcesador=substr($trama,201,11);
	$codigoAcreedor=substr($trama,212,11);//supongo que es el ruc o de repente el idemb
	$codigoProductoServicio=substr($trama,223,8);///supongo que aqu� debe estar el idemb y/o codigPago a�o y mes del ejercicio a pagar
	$codigoPlazaRecaudador=substr($trama,231,4);
	$codigoAgenciaRecaudador=substr($trama,235,4);
	$tipoDatoPago=substr($trama,239,2);//se ha cambiadoooooooooooooooooooooooo
	$datoPago=substr($trama,241,21);//se ha cambiadooooooooooooooo
	$codigoCiudad=substr($trama,262,3);
	$numeroProdServPagado=substr($trama,265,2);
	$numeroTotalDocPagado=substr($trama,267,3);
	$filler=substr($trama,270,10);
	$medioPago=substr($trama,280,2);
	$importePagadoEfectivo=substr($trama,282,11);
	$importePagoCCuenta=substr($trama,293,11);
	$nroCheque1=substr($trama,304,15);
	$bancoGirador1=substr($trama,319,3);
	$importeCheque1=substr($trama,322,11);
	$plazaCheque1=substr($trama,333,1);	
	$nroCheque2=substr($trama,334,15);
	$bancoGirador2=substr($trama,349,3);
	$importeCheque2=substr($trama,352,11);
	$plazaCheque2=substr($trama,363,1);	
	$nroCheque3=substr($trama,364,15);
	$bancoGirador3=substr($trama,379,3);
	$importeCheque3=substr($trama,382,11);
	$plazaCheque3=substr($trama,393,1);
	$monedaPago=substr($trama,394,3);
	$tipoCambioAplicado=substr($trama,397,11);
	$pagoTotalRealizado=substr($trama,408,11);
	$filler_=substr($trama,419,10);
	$codigoServicioPagado=substr($trama,429,3);
	$estadoDeudor=substr($trama,432,2);
	$importeTotalxProductoxServicio=substr($trama,434,11);
	$nroCuentaAbono=substr($trama,445,19);
	$nroReferenciaAbono=substr($trama,464,12);
	$nroDocumentosPagados=substr($trama,476,2);
	$filler__=substr($trama,478,10);
	$tipoDocumentoPago=substr($trama,488,3);
	$nroDocumentoPago=substr($trama,491,16);
	$periodoCotizacion=substr($trama,507,6);
	$tipoDocIdDeudor=substr($trama,513,2);
	$nroDocIdDeudor=substr($trama,515,15);
	$importeOriginalDeuda=substr($trama,530,11);
	$importePagadoDoc=substr($trama,541,11);
	$codigoConcepto1=substr($trama,552,2);
	$importeConcepto1=substr($trama,554,11);
	$codigoConcepto2=substr($trama,565,2);
	$importeConcepto2=substr($trama,567,11);
	$codigoConcepto3=substr($trama,578,2);
	$importeConcepto3=substr($trama,580,11);
	$codigoConcepto4=substr($trama,591,2);
	$importeConcepto4=substr($trama,593,11);
	$codigoConcepto5=substr($trama,604,2);
	$importeConcepto5=substr($trama,606,11);
	$referenciaDeuda=substr($trama,617,16);
	$filler___=substr($trama,633,34);
	
	//Se va a generar la trama de respuesta:
	$respuestaTrama="0210";
	$respuestaTrama.="F22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
	$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
	$respuestaTrama.=$codigoProceso;
	$respuestaTrama.=$monto;
	$respuestaTrama.=$fechaHoraTransaccion;
	$respuestaTrama.=$trace;
	$respuestaTrama.=$fechaCaptura;
	$respuestaTrama.=$binAdquiriente;//En este campo menciona IDENTIFICACION DE EMPRESA, ES EL BIN ADQUIRIENTE?? O ES EL RUC ED LA EMPRESA????
	$respuestaTrama.=$retrievalReferenceNumber;
	$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
	$respuestaTrama.="  ";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
	$respuestaTrama.=$terminalId;
	$respuestaTrama.=$transactionCurrencyCode;
	$respuestaTrama.=$datosReservados;
	
	$respuestaTrama.="557";
	$respuestaTrama.="01";
	$respuestaTrama.="           ";//Bin propcesador???? varchar(11) Centro de Procesamiento de Cobranzas
	$respuestaTrama.="           ";//Bin acreedor???? varchar(11) ??? es el c�digo de acreedor???
	$respuestaTrama.="        ";//codigo producto/servicio???? varchar(8)
	$respuestaTrama.="    ";//C�digo plaza recaudador varchar(4)
	$respuestaTrama.="    ";//C�digo agencia recaudador varchar(4)
	$respuestaTrama.="  ";//tipo dato de pago 01 por servicio 02 por recibo????
	$respuestaTrama.="                     ";//dato de pago, cliente recibo varchar(21)
	$respuestaTrama.="   ";//codigo de ciudad varchar(3)
	$respuestaTrama.="            ";//N�mero operaci�n cobranza varchar(12)
	$respuestaTrama.="            ";//N�mero operaci�n acreedor varchar(12)
	$respuestaTrama.="01";//N�mero prod/serv pagado varchar(2)
	$respuestaTrama.="001";//N�mero total doc pagado varchar(3)
	$respuestaTrama.="          ";//filler varchar(10)
	$respuestaTrama.=" ";//origen de respuesta varchar(1)
	$respuestaTrama.="   ";//codigo de respuesta extendido varchar(3)
	$respuestaTrama.="                              ";//descripci�n de respuesta aplicativo varchar(30)
	$respuestaTrama.="                    ";//nombre del deudor - nombre del cliente varchar(20)
	
	$respuestaTrama.="               ";//ruc del deudor varchar(15)
	$respuestaTrama.="               ";//ruc del acreedor varchar(15)
	$respuestaTrama.="      ";//codigo zona del deudor varchar(6)
	$respuestaTrama.="                    ";//filler varchar(20)
	
	$respuestaTrama.="   ";//codigo del prod/serv varchar(3)
	$respuestaTrama.="               ";//descripcion del prod/serv varchar(15)
	$respuestaTrama.="           ";//importe total por prod/serv varchar(11)
	$respuestaTrama.="                                        ";//mensaje1 para impresi�n varchar(40)
	$respuestaTrama.="                                        ";//mensaje2 para impresi�n varchar(40)
	$respuestaTrama.="  ";//n�mero de documentos varchar(2) se indicaar� 1??? o 01 o " 1" o "1 "
	$respuestaTrama.="                    ";//filler varchar(20)
	
	$respuestaTrama.="   ";//tipo de servicio varchar(3)
	$respuestaTrama.="               ";//descripcion del documento varchar(15)
	$respuestaTrama.="                ";//n�mero del documento varchar(16)�Se indica Documento cancelado?
	
	//Faltan m�s datos, pero hasta que no se tenga claro de los datos a mostrar quedar�a pendiente
	
	
	//$respuestaTrama="x".$codigoAcreedor."x";
	
	return $respuestaTrama;
}

//llamamos al m�todo service de la clase nusoap
$server->service($HTTP_RAW_POST_DATA);
?>