<?php
require_once('nusoap/lib/nusoap.php');

//creamos el objeto de tipo soap_server
$server = new soap_server;

//registramos la funci�n que vamos a implementar
$server->register('hello');

//implementamos la funci�n
function hello ($name){
return "Hola, $name.";
}

//llamamos al m�todo service de la clase nusoap
$server->service($HTTP_RAW_POST_DATA);
?>
