<?php
/**
 * Codigo para consumir un servicio web (Web Service) por medio de NuSoap
 * La distribucion del codigo es totalmente gratuita y no tiene ningun tipo de restriccion. 
 * Se agradece que mantengan la fuente del mismo.
 */
 


// Inclusion de la libreria nusoap (la que contendra toda la conexión con el servidor //
require('nusoap/lib/nusoap.php');

$oSoapClient = new soapclient('http://172.16.198.248:6060/Mypeperu_innovando/MYPE/1_prueba_RCP/sunat/2_ws.php?wsdl', true);


if ($sError = $oSoapClient->getError()) {
	echo "No se pudo realizar la operación [" . $sError . "]";
	die();
}

//$aParametros = array("idProducto" => $idProducto);
//$respuesta = $oSoapClient->call("obtenerProducto", $aParametros);


// Existe alguna falla en el servicio?
if ($oSoapClient->fault) { // Si
	echo 'No se pudo completar la operaci&oacute;n';
	die();
} else { // No
	$sError = $oSoapClient->getError();
	// Hay algun error ?
	if ($sError) { // Si
		echo 'Error:' . $sError;
		die();
	} 
}
?>
<html>
<body> 
<form action="prueba.php" method="post" name="frmProducto" id="frmProducto"> 
  <table width="400" border="0" cellspacing="0" cellpadding="0"> 
    <tr> 
      <td colspan="2"><div align="center">Digite un producto </div></td> 
    </tr> 
    <tr> 
      <td width="61">&nbsp;</td> 
      <td width="339">&nbsp;</td> 
    </tr> 
    <tr> 
      <td>Producto:</td> 
      <td><input type="text" name="idProducto">
	  </td> 
    </tr> 
    <tr> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
    </tr> 
    <tr> 
      <td>&nbsp;</td> 
      <td><input type="submit" name="Submit" value="Obtener producto"></td> 
    </tr> 
  </table> 
</form> 

</body>
</html>