<?
include_once('modulosadmin/claseIntranet.inc.php');

$objIntranet = new Intranet();

$menu = ($_POST['menu']) ? $_POST['menu'] : $_GET['menu'];
//$subMenu = ($_POST['subMenu']) ? $_POST['subMenu'] : $_GET['subMenu'];
$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

include_once('modulosadmin/claseDirectorioTramite.inc.php');

$modTram2 = new DirectorioTramite($menu,$subMenu);

if($modTram2){
	if ($accion!=$modTram2->arr_accion[AGREGA_PERSONA]&&$accion!=$modTram2->arr_accion[MODIFICA_PERSONA]&&
		$accion!=$modTram2->arr_accion[BUSCA_PERSONA_EXTENDIDO]&&$accion!=$modTram2->arr_accion[AGREGA_CONTACTO]&&
		$accion!=$modTram2->arr_accion[IMPRIMIR_CLAVE]&&$accion!=$modTram2->arr_accion[MUESTRA_DETALLE_PERSONA]&&
		$accion!=$modTram2->arr_accion[IMPRIME_DETALLE]){
		if(!isset($_GET['popup'])){
			$objIntranet->Header('Directorio Institucional',false,array('suspEmb'));
			$objIntranet->Body('directorio_tramite.gif',false,false);
		}
	}
	
	switch ($accion){
			
		case $modTram2->arr_accion[FRM_BUSCA_PERSONA]:
			$modTram2->FormBuscaPersona();
			break;
		case $modTram2->arr_accion[BUSCA_PERSONA]:
			$modTram2->BuscaDirectorio($_POST['page'],$_POST['desNombre'],$_POST['sector'],$_POST['codDepa'],$_POST['tipPersona']);
			break;
		case $modTram2->arr_accion[BUSCA_PERSONA_EXTENDIDO]:
			$modTram2->VistaExtendidaDocPersona($_POST['desNombre'],$_POST['sector'],$_POST['codDepa'],$_POST['tipPersona'],$_POST['ruc']);
			break;
		case $modTram2->arr_accion[FRM_AGREGA_PERSONA]:
			$modTram2->FormAgregaPersona($_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones']);
			break;
		case $modTram2->arr_accion[FRM_AGREGA_PERSONA_WEBSERVICE]:
			$modTram2->FormAgregaPersonaWebService($_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones']);
			break;
		case $modTram2->arr_accion[AGREGA_PERSONA]:
			$modTram2->AgregaPersona($_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones']);
			break;
		case $modTram2->arr_accion[FRM_MODIFICA_PERSONA]:
			$modTram2->FormModificaPersona(($_POST['id']) ? $_POST['id'] : $_GET['id'],$_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones'],$_POST['reLoad']);
			break;
		case $modTram2->arr_accion[MODIFICA_PERSONA]:
			$modTram2->ModificaPersona($_POST['id'],$_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones']);
			break;
		case $modTram2->arr_accion[FRM_AGREGA_CONTACTO]:
			$modTram2->FormAgregaContacto(($_POST['id']) ? $_POST['id'] : $_GET['id'],$_POST['nombres'],$_POST['apellidos'],$_POST['direccion'],$_POST['telefono'],$_POST['fax'],$_POST['mail'],$_POST['radio']);
			break;
		case $modTram2->arr_accion[AGREGA_CONTACTO]:
			$modTram2->AgregaContacto($_POST['id'],$_POST['nombres'],$_POST['apellidos'],$_POST['direccion'],$_POST['telefono'],$_POST['fax'],$_POST['mail'],$_POST['radio']);
			break;
		case $modTram2->arr_accion[IMPRIMIR_CLAVE]:
			$modTram2->ImprimeClave(($_POST['id']) ? $_POST['id'] : $_GET['id'],($_POST['clave']) ? $_POST['clave'] : $_GET['clave']);
			break;
		case $modTram2->arr_accion[MUESTRA_DETALLE_PERSONA]:
			$modTram2->DetallePersona(($_POST['id']) ? $_POST['id'] : $_GET['id'],($_POST['print']) ? $_POST['print'] : $_GET['print']);
			break;
		case $modTram2->arr_accion[IMPRIME_DETALLE]:
			$modTram2->ImprimeDetalle(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modTram2->arr_accion['BUENA_TRANS']:
			$modTram2->MuestraStatTrans($accion);
			break;
		case $modTram2->arr_accion['MALA_TRANS']:
			$modTram2->MuestraStatTrans($accion);
			break;
		case $modTram2->arr_accion[FRM_AGREGA_PERSONA2]:
			$modTram2->FormAgregaPersona2($_POST['sector'],$_POST['tipPersona'],$_POST['codDepa'],$_POST['codProv'],$_POST['codDist'],$_POST['tipIdent'],
			                            $_POST['razonsocial'],$_POST['nombres'],$_POST['apellidos'],$_POST['ruc'],$_POST['direccion'],$_POST['telefono'],
										$_POST['fax'],$_POST['mail'],$_POST['repLegal'],$_POST['nroRepLegal'],$_POST['tipIdentRepLegal'],$_POST['observaciones']);
			break;
		default:
			$modTram2->MuestraIndex();
			break;

	}
	if ($accion!=$modTram2->arr_accion[AGREGA_PERSONA]&&$accion!=$modTram2->arr_accion[MODIFICA_PERSONA]&&
		$accion!=$modTram2->arr_accion[BUSCA_PERSONA_EXTENDIDO]&&$accion!=$modTram2->arr_accion[AGREGA_CONTACTO]&&
		$accion!=$modTram2->arr_accion[IMPRIMIR_CLAVE]&&$accion!=$modTram2->arr_accion[MUESTRA_DETALLE_PERSONA]&&
		$accion!=$modTram2->arr_accion[IMPRIME_DETALLE])
		$objIntranet->Footer();
}else{
	$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
	$objIntranet->Body('tramite_tit.gif',false,false);
	$objIntranet->Footer();	
}
?>
