//calcular la edad de una persona 
//recibe la fecha como un string en formato espa�ol 
//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual 
function calcular_edad(fecha){ 

    //calculo la fecha de hoy 
    hoy=new Date() 
    //alert(hoy) 

    //calculo la fecha que recibo 
    //La descompongo en un array 
    var array_fecha = fecha.split("/") 
    //si el array no tiene tres partes, la fecha es incorrecta 
    if (array_fecha.length!=3) 
       return false 

    //compruebo que los ano, mes, dia son correctos 
    var ano 
    ano = parseInt(array_fecha[2]); 
    if (isNaN(ano)) 
       return false 

    var mes 
    mes = parseInt(array_fecha[1]); 
    if (isNaN(mes)) 
       return false 

    var dia 
    dia = parseInt(array_fecha[0]); 
    if (isNaN(dia)) 
       return false 


    //si el a�o de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4 
    if (ano<=99) 
       ano +=1900 

    //resto los a�os de las dos fechas 
    edad=hoy.getYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o 

    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido 
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
       return edad 
    if (hoy.getMonth() + 1 - mes > 0) 
       return edad+1 

    //entonces es que eran iguales. miro los dias 
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido 
    if (hoy.getUTCDate() - dia >= 0) 
       return edad + 1 

    return edad 
} 


function calcula_edad(valor, obj)
{
var edad="";

if(valor==""){
	obj.value=edad;
}else{
	var resp="";
	resp=calcular_edad(valor);
	if(resp=='0' || resp==false){ edad="0"; }else{ edad=resp; }
	obj.value=edad;
	}
}


function calcular_prima(valor, obj)
{
	var valor_mes=valor.substring(0,2)	
	valor_mes=Number(valor_mes)*5;
	if(valor_mes==0){ obj.value=''; }else{ obj.value=valor_mes;}
	
}




function calcular_finicio(fecha, valor, obj)
{
	
	var valor_mes=valor.substring(0,2)	
	valor_mes=Number(valor_mes);

	if(valor_mes==0){ obj.value=''; }else{ 

	if(fecha!="")
	{
	
	var valor_mensual=valor_mes;
	
	var sdfsfs=recalcF1(fecha,valor_mensual);
	obj.value='01'+sdfsfs.substring(2,10);
	}
		
	}
	
}



function calcular_fvigencia(fecha, valor, obj)
{
	
	var valor_mes=valor.substring(0,2)	
	valor_mes=Number(valor_mes);

	if(valor_mes==0){ obj.value=''; }else{ 

	if(fecha!="")
	{
	
	var valor_mensual=valor_mes*30
	
	var sdfsfs=recalcF1(fecha,valor_mensual);
	
	ultimodia(sdfsfs, obj);
	//obj.value=sdfsfs;
	
		verificar_estado(formregistro.xfechavencimiento.value,formregistro.fechaactual.value);
	}
		
	}
	
}




function calcular_fvigencia_ca(fecha, valor, obj)
{
	
	var valor_mes=valor.substring(0,2)	
	valor_mes=Number(valor_mes);
 	var fechaa=fecha;
 	var fechab=fecha;
 	var fechac=fecha;
	var valor_dia_actual=fechaa.substring(0,2);	
	var valor_mes_actual=fechab.substring(3,5);
	var valor_ano_actual=fechac.substring(6,10);	
	var valor_ano_sigui=Number(valor_ano_actual)+1;
	
	var fecha_selecdf=valor_dia_actual+'/'+valor_mes_actual+'/'+valor_ano_sigui;
	obj.value=fecha_selecdf;
//	alert(fecha_selecdf);
	//ultimodia(fecha_selecdf, obj);
}



function sumar_dias_a_fecha(fecha, diasSumar){ 
/*
alert(fecha+'---------'+diasSumar);
var fecha3= new Date();
fecha3=fecha;
var milisecSumar= parseInt(diasSumar*24*60*60*1000); 
var fecha2=fecha3.setTime(fecha3.getTime()+milisecSumar); 

alert(fecha2);

return fecha; **/
}


  var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31); 

  function finMes(nMes, nAno){ 
   return aFinMes[nMes - 1] + (((nMes == 2) && (nAno % 4) == 0)? 1: 0); 
  } 

   function padNmb(nStr, nLen, sChr){ 
    var sRes = String(nStr); 
    for (var i = 0; i < nLen - String(nStr).length; i++) 
     sRes = sChr + sRes; 
    return sRes; 
   } 

   function makeDateFormat(nDay, nMonth, nYear){ 
    var sRes; 
    sRes = padNmb(nDay, 2, "0") + "/" + padNmb(nMonth, 2, "0") + "/" + padNmb(nYear, 4, "0"); 
    return sRes; 
   } 
    
  function incDate(sFec0){ 
   var nDia = parseInt(sFec0.substr(0, 2), 10); 
   var nMes = parseInt(sFec0.substr(3, 2), 10); 
   var nAno = parseInt(sFec0.substr(6, 4), 10); 
   nDia += 1; 
   if (nDia > finMes(nMes, nAno)){ 
    nDia = 1; 
    nMes += 1; 
    if (nMes == 13){ 
     nMes = 1; 
     nAno += 1; 
    } 
   } 
   return makeDateFormat(nDia, nMes, nAno); 
  } 

  function decDate(sFec0){ 
   var nDia = Number(sFec0.substr(0, 2)); 
   var nMes = Number(sFec0.substr(3, 2)); 
   var nAno = Number(sFec0.substr(6, 4)); 
   nDia -= 1; 
   if (nDia == 0){ 
    nMes -= 1; 
    if (nMes == 0){ 
     nMes = 12; 
     nAno -= 1; 
    } 
    nDia = finMes(nMes, nAno); 
   } 
   return makeDateFormat(nDia, nMes, nAno); 
  } 

  function addToDate(sFec0, sInc){ 
   var nInc = Math.abs(parseInt(sInc)); 
   var sRes = sFec0; 
   if (parseInt(sInc) >= 0) 
    for (var i = 0; i < nInc; i++) sRes = incDate(sRes); 
   else 
    for (var i = 0; i < nInc; i++) sRes = decDate(sRes); 
   return sRes; 
  } 

  function recalcF1(fecha,valor_mensual){ 
    var valor_md = addToDate(fecha, valor_mensual); 
	return valor_md;
   } 
   
   
   
   function refresca_page()
   {
	formregistro.submit();
   }
   
   
   
      function EvaluateText(cadena, obj, obj_focus){ 
    opc = false;  
    if (cadena == "%d") 
     if (event.keyCode > 47 && event.keyCode < 58) 
      opc = true; 
    if (cadena == "%f"){  
     if (event.keyCode > 47 && event.keyCode < 58) 
      opc = true; 
     if (obj.value.search("[.*]") == -1 && obj.value.length != 0) 
      if (event.keyCode == 46) 
       opc = true; 
    } 
	if(event.keyCode == 13 ){ obj_focus.focus(); }
	
    if(opc == false) 
     event.returnValue = false;  
   } 
   
   
   
   function NumberFormat(num, numDec, decSep, thousandSep){ 
    var arg; 
    var Dec; 
    Dec = Math.pow(10, numDec);  
    if (typeof(num) == 'undefined') return;  
    if (typeof(decSep) == 'undefined') decSep = ''; 
    if (typeof(thousandSep) == 'undefined') thousandSep = '.'; 
    if (thousandSep == '.') 
     arg=/./g; 
    else 
     if (thousandSep == ',') arg=/,/g; 
    if (typeof(arg) != 'undefined') num = num.toString().replace(arg,''); 
    num = num.toString().replace(/,/g, '.');  
    if (isNaN(num)) num = "0"; 
    sign = (num == (num = Math.abs(num))); 
    num = Math.floor(num * Dec + 0.50000000001); 
    cents = num % Dec; 
    num = Math.floor(num/Dec).toString();  
    if (cents < (Dec / 10)) cents = "0" + cents;  
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) 
     num = num.substring(0, num.length - (4 * i + 3)) + thousandSep + num.substring(num.length - (4 * i + 3)); 

		
    if (Dec == 1) 
     return (((sign)? '': '-') + num); 
    else 
     return (((sign)? '': '-') + num + decSep + cents); 
	 
	  
   }  
   
   /*
   function valida_letras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    patron =/[A-Za-z]/;
    te = String.fromCharCode(tecla);
	alert(patron.test(te))
    return patron.test(te);
	}*/
	
function valida_letras()
{ 
var key = event.keyCode; 
if( 
   (key > 64 && key < 91) || 
   (key > 96 && key < 123) ||  
   (key==165) || (key==164) || 
   (key==32) || (key==209) || (key==241)
   ){    
event.keyCode=event.keyCode;
/**/  }else{    event.keyCode=0;	 }
} 
	
function valida_letrasynumeros()
{ 
var key = event.keyCode; 

if(
   (key > 47 && key < 58)|| (key==47) || (key==45) || 
   (key > 64 && key < 91) || 
   (key > 96 && key < 123) ||  
   (key==165) || (key==164) || 
   (key==32) || (key==209) || (key==241)
   ){    
event.keyCode=event.keyCode;
/**/  }else{    event.keyCode=0;	 }
} 	
	
	




   function dias(mes, anno) {
      mes = parseInt(mes);
	  anno = parseInt(anno);
      switch (mes) {
	    case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 : return 31;
		case 2 : return (anno % 4 == 0) ? 29 : 28;
	  }
	  return 30;
   }
   
   function ultimodia(elemento, obj) {
      var arreglo = elemento.split("/");
	  var dia = arreglo[0];
	  var mes = arreglo[1];
	  var anno = arreglo[2];
	  
      dia = dias(mes, anno);
	  
	  obj.value = (dia+"/"+mes+"/"+anno);
   }




function  verificar_estado(fecha1, fecha2)
{
		var difdias= DiferenciaFechas(fecha1,fecha2)
		
		if(difdias<0)
		{
		window.document.images["i_estado"].src = "./images/rojo.jpg";
		}else{
		window.document.images["i_estado"].src = "./images/verde.jpg";
		}

}
function DiferenciaFechas (fecha1, fecha2) {   
  
   /* Obtiene los datos del formulario   */
   CadenaFecha1 = fecha1;
   CadenaFecha2 = fecha2;
      
   /*Obtiene dia, mes y a�o   */
   var fecha1 = new fecha_valida( CadenaFecha1 )      
   var fecha2 = new fecha_valida( CadenaFecha2 )   
      
   /*Obtiene objetos Date   */
   var miFecha1 = new Date( fecha1.anio, fecha1.mes, fecha1.dia )   
   var miFecha2 = new Date( fecha2.anio, fecha2.mes, fecha2.dia )   
  
   /* Resta fechas y redondea   */
   var diferencia = miFecha1.getTime() - miFecha2.getTime()   
   var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24))   
   var segundos = Math.floor(diferencia / 1000)   
   
/*   obj.value=dias;*/
   /*alert ('La diferencia es de ' + dias + ' dias,\no ' + segundos + ' segundos.')   */
      
   return dias; 
}   
  
function fecha_valida( cadena ) {   
  
   //Separador para la introduccion de las fechas   
   var separador = "/"  
  
   //Separa por dia, mes y a�o   
   if ( cadena.indexOf( separador ) != -1 ) {   
        var posi1 = 0   
        var posi2 = cadena.indexOf( separador, posi1 + 1 )   
        var posi3 = cadena.indexOf( separador, posi2 + 1 )   
        this.dia = cadena.substring( posi1, posi2 )   
        this.mes = cadena.substring( posi2 + 1, posi3 )   
        this.anio = cadena.substring( posi3 + 1, cadena.length )   
   } else {   
        this.dia = 0   
        this.mes = 0   
        this.anio = 0      
   }   
}   




function limpia_campo_numerico(obj)
{
	var numero_digitado=obj.value;
	
	for (f=1;f<7;f++){ numero_digitado=numero_digitado.replace(" ", ""); }
	
	if(Number(numero_digitado)==0)
	{
		//alert(1);
		obj.value='';
	}else{
		obj.value=NumberFormat(numero_digitado, '2', '.', '');
		}
}


function limpia_campo_numerico_valor(valorx)
{
	var numero_digitado=valorx;
	
	for (f=1;f<7;f++){ numero_digitado=numero_digitado.replace(" ", ""); }
	
	if(Number(numero_digitado)==0)
	{
		//alert(1);
		return '0';
	}else{
		return NumberFormat(numero_digitado, '2', '.', '');
		}
}


function limpia_campo_numerico_tc(obj)
{
	var numero_digitado=obj.value;
	
	for (f=1;f<7;f++){ numero_digitado=numero_digitado.replace(" ", ""); }
	
	if(Number(numero_digitado)==0)
	{
		//alert(1);
		obj.value='';
	}else{
		obj.value=NumberFormat(numero_digitado, '3', '.', '');
		}
}


function calculo_sope(obj1,obj2,obj3)
{
	var numero_digitado1=obj1.value;
	for (f=1;f<7;f++){ numero_digitado1=numero_digitado1.replace(" ", ""); }
	var numero_digitado2=obj2.value;
	for (f=1;f<7;f++){ numero_digitado2=numero_digitado2.replace(" ", ""); }
	
	var saldo_opeta=0;
	saldo_opeta=Number(numero_digitado1)-Number(numero_digitado2);
	obj3.value=NumberFormat(saldo_opeta, '2', '.', ' ');
	if(saldo_opeta<0){ obj3.style.color = "red"; }
	if(saldo_opeta>=0){ obj3.style.color = "black";  }
}