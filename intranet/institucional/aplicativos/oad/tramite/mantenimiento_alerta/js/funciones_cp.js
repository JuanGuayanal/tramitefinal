function onlynumbers(obj){ obj.value=obj.value.replace(/[^0-9]{1,}/,""); }

function fs_SoloNum(e) { 
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/[0-9]|\./; 
	te = String.fromCharCode(tecla); 
	return patron.test(te);  
}

function fs_SoloLetras(e) { 
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/[A-Za-z]|\s|\�|\�|\.|\,/; 
	te = String.fromCharCode(tecla); 
	return patron.test(te);  
}

// TRIM FUNCTION	
function ltrim(s) { 
	return s.replace(/^\s+/, ""); 
}
function rtrim(s) { 
	return s.replace(/\s+$/, ""); 
}
function trim(s) { 
	return rtrim(ltrim(s)); 
}	
	
// TAB FUNCTION WHEN ENTER
function tab(e){	
	tecla = (document.all) ? e.keyCode : e.which; 		
	if (tecla == 13){
		window.event.keyCode = 9;
	}	
}   
   
// MAYUSCULAS
function upperCase(x){
	var y=document.getElementById(x).value;
	document.getElementById(x).value=y.toUpperCase();
}