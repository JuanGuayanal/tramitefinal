<?php
if (strtoupper(substr(PHP_OS, 0, 3)) === 'LIN') {
    require $_SERVER['DOCUMENT_ROOT'].'/sitradocV3/modelo/sql_mod_utilitarios.php';
} else {
    require $_SERVER['DOCUMENT_ROOT'].'\sitradocV3\modelo\sql_mod_utilitarios.php';
}

class Utilitarios{
	public $sql;
	public function __construct(){
		$this->sql=new Sql_mod_utilitarios();
	}
	public function destinoDocumento($dep=0,$clase=0,$doc='',$anio=''){
		$lista=$this->sql->destinoDocumento($dep,$clase,$doc,$anio);	
		$destino=array();
		foreach($lista['lista'] as $row){
			$destino[]=array('DEPENDENCIA'=>utf8_encode($row['DEPENDENCIA']));
		}
		return json_encode($destino);
	}
}
?>