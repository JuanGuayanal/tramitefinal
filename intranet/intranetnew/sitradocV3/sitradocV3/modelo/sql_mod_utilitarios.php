<?php
require 'datosGlobales.php';
class Sql_mod_utilitarios{
	protected $server=CJDBMSSQL;
	protected $user=CJDBMSSQLUSER;
	protected $pass=CJDBMSSQLPASS;
	protected $db='DB_TRAMITE_DOCUMENTARIO';
	protected $link;
	protected $consulta;
	
	public function __construct(){
		$this->conectar();
	}
	protected function conectar(){
		$this->link = mssql_connect($this->server,$this->user,$this->pass);
		mssql_select_db($this->db, $this->link);
	}
	public function ejecutar($consulta,$crud=NULL){
		$this->consulta=mssql_query($consulta);
/*		switch (strtolower($crud)){
			case 'i':
				echo "ESTA CONSULTA PARA HACER UN INSERT";
			break;
			case 'u':
				echo "ESTA CONSULTA PARA HACER UN UPDATE";
			break;
			case 'd':
				echo "ESTA CONSULTA PARA HACER UN DELETE";
			break;
			default:*/
				return $this->listado();
/*			break;
		}*/
	}
	public function listado(){
		$resultado=array();
		$resultado['filas_afectadas']=mssql_rows_affected($this->link);
		while ($row = mssql_fetch_assoc ($this->consulta)) {
			$resultado['lista'][]=$row;
		}
		return $resultado;
	}
//------------------------SENTENCIAS SQL---------------------------
	public function destinoDocumento($dep,$clase,$doc,$anio){	//SE APLICA PARA OBTENER DESTINOS EN MOVIMIENTOS DE UN DOCUMENTO
		$sql=sprintf("EXECUTE sp_get_documento 'BUSCAR_DESTINOS',%d,0,%d,'%s','%s',0,'',1,10 ",$dep,$clase,$doc,$anio);
		return $this->ejecutar($sql);
	}
//---------------------------------------------------------------------
}


?>