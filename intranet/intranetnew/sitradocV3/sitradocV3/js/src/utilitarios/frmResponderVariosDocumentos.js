$(document).ready(function() {
	
	//AGREGANDO ELEMENTOS
	var buscador_documentos='<div id="std_mis_documentos"><div class="std_titulo_popup">Busca documentos</div><div><div class="left"><select id="std_cbo_doc"><option value="1">Externo-Expediente</option><option value="2">Interno Pendiente</option><option value="3">Derivados</option></select><br><span class="solo_interno"><select id="i_clase_doc"><option value="0">TODOS</option></select></span><input type="text" id="b_documento_filtro_text">-<select id="i_anio"></select><span class="solo_interno">-MIDIS/<select id="i_dependencia"><option value="0">TODOS</option></select></span><button id="b_btn_documento_buscar">Buscar</button></div><div class="right"><button id="b_documento_agregar" title="Solo se agregaran los registros marcados con check">Agregar a documentos</button></div><div class="limpiar"></div></div><br><br><div id="std_div_table_"><table id="b_documentos"><thead><tr><th width="1"></th><th width="50">DOCUMENTO</th><th width="150">INDICATIVO</th><th width="480">ASUNTO</th><th>FECHA</th><th><input type="checkbox" id="b_documento_seleccionar_todos"></th></tr></thead></table></div></div>';
	$("body").prepend('<div id="std_modal"></div><div id="std_popup"><div class="std_cerrar_popup">x</div>'+buscador_documentos+'</div>');	
	combos_bdocs();	
	function combos_bdocs(){
		//LLENAR COMBO CLASE DOCUMENTO POR DEPENDENCIA DEL USUARIO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_usuario_clase_documentos',
				type:'POST',
				data:{coddep:$("#i_coddep").val()},
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#idClaseDoc").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO DEPENDENCIA
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_dependencias',
				type:'POST',
				data:{midep:$("#i_coddep").val()},
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#idDependencias").append('<option value="'+option.ID+'">'+option.DEPENDENCIA+'</option>');
					}   
				}
			});
		//LLEBADO DE COMBOS PRARA BUSCADOR
		//
		//LLENA COMBO CLASE DE DOCUMENTO
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_clase_documentos',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_clase_doc").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO AÑOS
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_anios',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_anio").append('<option value="'+option.DESCRIPCION+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
		//LLENA COMBO DEPENDENCIA
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_dependencias',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_dependencia").append('<option value="'+option.ID+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});
	}
	//EVENTOS POPUP
	$("body").delegate("#std_modal, .std_cerrar_popup", "click", function() {
	  $("#std_modal, #std_popup").hide();
	});
	//ABRE POPUP
	$("#std_btn_buscar_documento").click(function(e){
		e.preventDefault();
		$("#b_documento_seleccionar_todos").removeAttr('checked');
		oTabled.fnAdjustColumnSizing();
		$("#std_modal, #std_popup, #std_mis_documentos").fadeIn();
	})
	//OBJETO DATATABLE DOCUMENTOS PARA DERIVAR
  	var oTabled= $('#b_documentos').dataTable( {
		"sPaginationType": "full_numbers",
		"bAutoWidth": false,
		"sScrollY": "320px",
        "bProcessing": true,
        "bServerSide": true,
		"sAjaxSource": "/sitradocV3/index.php?accion=correspondencia_multiple_busca_documento",
		"sServerMethod": "POST",
		"bSort": false,
		"bRetrieve": true,
		"fnServerParams": function ( aoData ) {
      	aoData.push( 
				{ "name": "coddep",	 	"value":$("#i_coddep").val()						},
				{ "name": "tipo", 		"value":$("#std_cbo_doc").val() },
				{ "name": "clasedoc",	"value":$("#i_clase_doc").val() 					},
				{ "name": "numdoc", 	"value":$("#b_documento_filtro_text").val() 		},
				{ "name": "anio", 		"value":$("#i_anio").val() 							},
				{ "name": "siglas", 	"value":$("#i_dependencia").val() 					}
				);
    	},
		"fnDrawCallback": function( oSettings ) {
			$("#b_documentos_filter").remove();
			$("#b_documentos_paginate span a").click(function(){
			$("#b_documento_seleccionar_todos").removeAttr("checked")
			})
		},
		"oLanguage": {
                "sUrl": "/sitradocV3/js/plugins/DataTables-1.9.4/idioma/spanish.txt"
            }
    } );
	//CUANDO EL FORMULARIO DE BUSQUEDA DE DOCUMENTOS ESTE LISTO
	$(".solo_interno").hide();
	$("#std_cbo_doc").change(function(){
		$("#b_documento_filtro_text").val('');
		if($(this).val()=="1"){
			$(".solo_interno").hide();
		}
		else{
			$(".solo_interno").show();
		}
		oTabled.fnDraw();
		$("#b_documento_seleccionar_todos").removeAttr('checked');
	})
	//PARA QUE EL BUSCADOR DE DOCUMENTOS FUNCIONE CON EL BOTON BUSCAR
	$("body").delegate("#b_btn_documento_buscar","click",function(e){//
		e.preventDefault();
		oTabled.fnDraw();
		$("#b_documento_seleccionar_todos").removeAttr('checked');
	})
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON ID "b_documento_seleccionar_todos"
	$('body').delegate('#b_documento_seleccionar_todos',"click",function(e){
		check_padre($('#b_documento_seleccionar_todos'),$(".b_documento_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "b_documento_check"
	$("body").delegate(".b_documento_check","click",function(){
		check_hijo($('#b_documento_seleccionar_todos'),$(".b_documento_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_documento_seleccionar_todos"
	$('body').delegate('#t_documento_seleccionar_todos',"click",function(e){
		check_padre($('#t_documento_seleccionar_todos'),$(".t_documenento_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_documento_check"
	$("body").delegate(".t_documenento_check","click",function(){
		check_hijo($('#t_documento_seleccionar_todos'),$(".t_documenento_check"))
	});
	//HACE REFERENCIA A LA FUNCION "check_padre", AL MOMENTO DE HACERLE CCK AL CHECKBOX CON ID "t_documento_seleccionar_todos"
	$('body').delegate('#t_destinatario_seleccionar_todos',"click",function(e){
		check_padre($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	})
	//HACE REFERENCIA A LA FUNCION "check_hijo", AL MOMENTO DE HACERLE CLICK AL CHECKBOX CON CLASE "t_documento_check"
	$("body").delegate(".t_destinatario_check","click",function(){
		check_hijo($('#t_destinatario_seleccionar_todos'),$(".t_destinatario_check"))
	});
	//ACCION QUE SE DESENCADENA AL MOMENTO DE DARLE CLICK EN EL BOTON AGREGAR A DOCUMENTOS
	$("#b_documento_agregar").click(function(e){
		e.preventDefault();
		var contador=0;
		var contador_nocheck=0;
		$("#b_documentos tbody tr").each(function(){
			$(this).children("td:nth-child(6)").each(function(){
				if($(this).children('input').is(':checked')){
					contador=agregar_docs($(this).parent().children("td:nth-child(1)").children("input").val(),$(this).parent().children("td:nth-child(2)").text(),$(this).parent().children("td:nth-child(3)").html(),$(this).parent().children("td:nth-child(4)").text(),$(this).parent().children("td:nth-child(5)").text(),$(this).parent().children("td:nth-child(1)").children("input").attr('situacion'))+contador;
					contador_nocheck++;
				}
			});
		})
		$("#t_detalle_documento").html('Cantidad de documentos: <strong>'+$("#t_documentos tbody tr").size()+'</strong>');
		if(contador_nocheck==0){
			alert('No ha seleccionado ningun documento.');
		}
		else{
			alert('Se agregaranon '+contador+' documentos a su lista para vincular.');
		}
		
	});
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR documento
	$("#t_eliminar_documento").click(function(){
		eliminar_doc();
	});
	//HACE REFERENCIA AL EVENTO ELIMINAR EN EL MOMENTO QUE SE HACE CLICK NE EL BOTON ELIMINAR destinatario
	$("#t_eliminar_destinatraio").click(function(){
		eliminar_destinatario();
	});
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_documentos) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar_docs(codigo,tipodoc,asunto,indicativo,fecha,situacion){
		var contador=0;
		var existe=0;
		$("#t_documentos tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_documentos tbody").prepend('<tr codigo="'+codigo+'" title="'+codigo+'" situacion="'+situacion+'"><td class="border-left"><input type="hidden" value="'+codigo+'" name="documentov[]">'+tipodoc+'</td><td class="border-left">'+asunto+'</td><td class="border-left">'+indicativo+'</td><td class="border-left">'+fecha+'</td><td class="border-left"><input type="checkbox" class="t_documenento_check"><input type="hidden" name="estado[]" value="'+situacion+'"></td></tr>');
			contador++;
		} 
		return contador;
	}
	//ACCION QUE SE DESENCADENA AL MOMENTO DE DARLE CLICK EN EL BOTON AGREGAR A DESTINATARIOS
	$("#b_agregar_destinatario").click(function(e){
		e.preventDefault();
		if($("#idDependencias").val()==0){alert('No ha seleccionado destino.');}
		else{
		contador=agregar_destinatario($("#idDependencias").val(),$("#idDependencias option:selected").text(),'chau')+contador;	
		}
	});
	
	//CLICK EN EL BOTON RESPODER
	function set_accion(){
		var contador_sit=0;
		maximo_doc=$("#t_documentos tbody tr").size();
		$("#t_documentos tbody tr").each(function(){
			contador_sit=contador_sit+parseInt($(this).attr('situacion'),10);
		})
		if(contador_sit==maximo_doc){return 1;}
		if((contador_sit==0)&& (contador_sit<maximo_doc)){return 0;}
		if((contador_sit>0) && (contador_sit<maximo_doc)){return 2;}
	}
	//$("#frm_responder").submit(function(e){
	$("#responder").click(function(e){
		//e.preventDefault();
		$("#asunto").val($.trim($("#asunto").val()));
		$("#observaciones").val($.trim($("#observaciones").val()));
		if($("#body_t_documentos tr").size()>0){
			if($("#idClaseDoc").val()!=0){
				if($("#asunto").val()!=0){
						if($("#body_t_destinatarios tr").size()>0){
							/*if(set_accion()==0){$('#accion').val('DerivaMultDocMultDest');return true;}
							if(set_accion()==1){$('#accion').val('ReiteraMultDocMultDest');return true;}
							if(set_accion()==2){alert('No puede seleccionar documentos de su bandeja y documentos enviados a la vez');return false;}	*/
							return true;
						}else{alert('No ha seleccionado destinatario(s).');return false;}
				}
				else{alert('No ha escrito asunto.');return false;}
			}else{alert('No ha seleccionado clase de documento.');return false;}
		}else{;alert('No ha seleccionado documento.');return false}
	})
	//CHANGE idClaseDoc
	$("#idClaseDoc").change(function(){
		if($(this).val()==32){
			$("#id_tipo_tratamiento").css('display','table-row');
		}else{
			$("#id_tipo_tratamiento").css('display','none');
		}
	})
	//ESTA FUNCION AGREGA REGISTROS A UNA TABLA (t_destinatarios) PARA LUEGO SER PROCESADAY POSTERIORMENTE GUARDADA EN UNA DB
	function agregar_destinatario(codigo,dependencia){
		var contador=0;
		var existe=0;
		$("#t_destinatarios tbody tr").each(function(){	
			if($(this).attr("codigo")==codigo){
			existe++;
			}
		})
		if(existe==0){
			$("#t_destinatarios tbody").prepend('<tr codigo="'+codigo+'"><td class="border-left"><input type="hidden" value="'+codigo+'" name="destinatariov[]">'+dependencia+'</td><td class="border-left"><input type="checkbox" class="t_destinatario_check"></td></tr>');
			contador++;
		} 
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
		return contador;
	}
	//ELIMINARA LOS REGISTROS DE LA LISTA DOCUMENTOS DONDE SE ENCUENTRAN LOS DOCS SELECCIONADOS CHEKEADOS
	function eliminar_doc(){
		$("#t_documentos tbody tr").each(function(){	
			if($(this).children("td:nth-child(5)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_documento_seleccionar_todos,.t_documento_check").removeAttr('checked');
		$("#t_detalle_documento").html('Cantidad de documentos a vincular: <strong>'+$("#t_documentos tbody tr").size()+'</strong>');
	}
	//ELIMINARA LOS REGISTROS DE LA LISTA DONDE SE ENCUENTRAN LOS DESTINATARIOS SELECCIONADOS CHEKEADOS
	function eliminar_destinatario(){
		$("#t_destinatarios tbody tr").each(function(){	
			
			if($(this).children("td:nth-child(2)").children('input').is(':checked')){
				$(this).remove();
			}
		})
		$("#t_destinatario_seleccionar_todos,.t_destinatario_check").removeAttr('checked');
		$("#t_detalle_destinatraio").html('Cantidad de destinatarios: <strong>'+$("#t_destinatarios tbody tr").size()+'</strong>');
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR LOS CHECKBOX(clase_check_hijo) QUE DEPENDAN DE UN CHECKBOX PRINCIPAL(id_check_padre)
	function check_padre(id_check_padre,clase_check_hijo){
		if(id_check_padre.is(':checked')){
			clase_check_hijo.attr('checked','checked');
		}
		else{
			clase_check_hijo.removeAttr('checked');
		}
	}
	//ESTA FUNCION TE PERMITE MARCAR-DESMARCAR UN CHECKBOX PRINCIPAL(id_check_padre), PARTIENDO DESDE SU HIJO (clase_check_hijo)
	function check_hijo(id_check_padre,clase_check_hijo){
		var total=clase_check_hijo.size()
		var count = 0; 
		clase_check_hijo.each(function(){
			if($(this).is(':checked')){
			count++;
			}
		})
		if(count==total){
			id_check_padre.attr('checked','checked');
		}
		else{
			id_check_padre.removeAttr('checked');
		}
	}	
});