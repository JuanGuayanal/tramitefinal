<?php
require $_SERVER['DOCUMENT_ROOT'].'\sitradocV3\modelo\sql_mod_correspondencia.php';

class Correspondencia{
	public $sql;
	public function __construct(){
		$this->sql=new Sql_mod_correspondencia();
	}
	public function busca_destinatario(){
		$total_personas=$this->sql->total_personal();
		$output = array(
						"iTotalRecords" => $total_personas['lista'][0]['TOTAL'],
						"iTotalDisplayRecords" => 0,
						"aaData" => array()
						);
		if($_POST['filtro']){//SI SE HA ENVIADO LA VARIABLE "FILTRO" POR METODO POST
			if($_POST['filtro']==''){
				$output['aaData']= array();
			}
			else{
				$lista=$this->sql->lista_personal(utf8_decode(limpiar($_POST['filtro'])));
				$row=array();
				if(is_array($lista['lista'])){//SI SE OBTUVO UN JUEGO DE REGISTROS
					foreach($lista['lista'] as $fila){
						$row[]=array('<input type="hidden" value="'.$fila['ID'].'">',utf8_encode($fila['DESTINATARIO']),'<input type="hidden" value="'.$fila['CODIGO_DEPARTAMENTO'].'">',utf8_encode($fila['DEPARTAMENTO']),'<input type="hidden" value="'.$fila['CODIGO_PROVINCIA'].'">',utf8_encode($fila['PROVINCIA']),'<input type="hidden" value="'.$fila['CODIGO_DISTRITO'].'">',utf8_encode($fila['DISTRITO']),utf8_encode($fila['DIRECCION']),'<input type="checkbox" name="b_destinatario_check" class="b_destinatario_check">');
					}
					$output['iTotalDisplayRecords']=$lista['filas_afectadas'];
				}
				$output['aaData']=$row;
			}
		}
		return json_encode( $output );	
	}
	public function formas_de_envio(){
		$formas_de_envio=$this->sql->formas_de_envio();
		return json_encode($formas_de_envio['lista']);
	}
	public function ubigeo($ubigeo,$cod_depa,$cod_prov){
		$ubigeo=$this->sql->ubigeo($ubigeo,$cod_depa,$cod_prov);
		return json_encode($ubigeo['lista']);
	}

	public function combo_clase_documentos($accion,$coddep=0,$tipo=0,$idclasedoc=0,$numdoc='',$anio='',$iddeporigen=0){
		//SI CODDEP=0 LLENA EL COMBO CON TODOS LOS REGISTROS, DE LO CONTRARIO LOS LLENAS SOLO CON LOS QUE EL USUARIO TIENE PERMISO
		$combo=$this->sql->combo_clase_documentos($coddep,$tipo,$idclasedoc,$numdoc,$anio,$iddeporigen);
		return json_encode($combo['lista']);
	}
	public function combo_anios(){
		$combo=$this->sql->combo_anios();
		return json_encode($combo['lista']);
	}
	public function combo_dependencias($miDependencia){

		$combo=$this->sql->combo_dependencias($miDependencia);
		for($i=0;$i<count($combo['lista']);$i++){
			$combo['lista'][$i]['DEPENDENCIA']=utf8_encode($combo['lista'][$i]['DEPENDENCIA']);
		}
		return json_encode($combo['lista']);
	}
	public function multiple_busca_documento($coddep=0,$tipo=0,$idclasedoc=0,$numdoc='',$anio='',$iddeporigen=0,$asunto='',$start=0,$len=10){
		//$siglas=($siglas=='0' || $siglas==0)? '%%':$siglas;
		$start=$start+1;
		$total_personas=0;
		$output = array(
						"iTotalRecords" => $total_personas,
						"iTotalDisplayRecords" => 0,
						"aaData" => array()
						);
		if(trim($numdoc)!=''){
				$lista=$this->sql->multiple_busca_documento($coddep,$tipo,$idclasedoc,utf8_decode(limpiar($numdoc)),$anio,$iddeporigen,$asunto,$start,$len);
				$row=array();
				if(is_array($lista)){//SI SE OBTUVO UN JUEGO DE REGISTROS
					if(isset($lista['lista'])){
						foreach($lista['lista'] as $fila){
							$row[]=array('<input type="hidden" value="'.$fila['ID_DOCUMENTO'].'">',utf8_encode($fila['DESCRIPCION']),$fila['INDICATIVO_OFICIO'].'<br><span class="rojo">'.$fila['CLASEDOC_PADRE'].' '.$fila['PADRE'].'</span>',utf8_encode($fila['ASUNTO']),$fila['FECHA'],'<input type="checkbox" name="b_documento_check" class="b_documento_check">');
						}
						$output['iTotalRecords']=$lista['lista'][0]['TOTAL'];
						$output['iTotalDisplayRecords']=$lista['lista'][0]['TOTAL_FILTRADOS'];
						$output['aaData']=$row;
					}
				}	
		}
		return json_encode( $output );
	} 
	public function lista_personal_domicilios($id){
		$output = array(
						"iTotalRecords" =>0,
						"iTotalDisplayRecords" => 0,
						"aaData" => array()
						);
		$lista=$this->sql->lista_personal_domicilios($id);
		$row=array();
		if(is_array($lista['lista'])){//SI SE OBTUVO UN JUEGO DE REGISTROS
			foreach($lista['lista'] as $fila){
				if($_POST['domicilio']==$fila['PRINCIPAL']){$marcado=' checked="checked" ';}else{$marcado=''; }
				$row[]=array(utf8_encode($fila['DESTINATARIO']),'<input type="hidden" value="'.$fila['CODIGO_DEPARTAMENTO'].'">'.utf8_encode($fila['DEPARTAMENTO']),'<input type="hidden" value="'.$fila['CODIGO_PROVINCIA'].'">'.utf8_encode($fila['PROVINCIA']),'<input type="hidden" value="'.$fila['CODIGO_DISTRITO'].'">'.utf8_encode($fila['DISTRITO']),utf8_encode($fila['DOMICILIO']),'<input type="radio" name="b_domicilio_radio" '.$marcado.' value="'.$fila['PRINCIPAL'].'">');
			}
			$output['iTotalRecords']=$lista['filas_afectadas'];
			$output['iTotalDisplayRecords']=$lista['filas_afectadas'];
		}
		$output['aaData']=$row;
		return json_encode( $output );	
	}
}
?>