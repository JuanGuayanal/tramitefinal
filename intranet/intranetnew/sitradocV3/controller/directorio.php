<?php
require $_SERVER['DOCUMENT_ROOT'].'\sitradocV3\modelo\sql_mod_directorio.php';

class Directorio{
	public $sql;
	public function __construct(){
		$this->sql=new Sql_mod_directorio();
	}
	public function listadoPersonalXML(){
		$formas_de_envio=$this->sql->listadoPersonalXML();
		$contador=0;
		$nodos='';
		foreach($formas_de_envio['lista'] as $row){
			$contador++;
			$nodos.='<personal>';
				$nodos.='<id>'.$row['ID'].'</id>';
				$nodos.='<nombre>'.sanear_string(utf8_encode($row['NOMBRE'])).'</nombre>';
				$nodos.='<documento>'.utf8_encode($row['NRO_DOCUMENTO']).'</documento>';
				$nodos.='<direccion>'.utf8_encode($row['DIRECCION']).'</direccion>';
			$nodos.='</personal>';
		}
		$xml='<?xml version="1.0" encoding="utf-8"?>';
		$xml.='<directorio>';
		$xml.='<totalResultsCount>'.$contador.'</totalResultsCount>';
		$xml.=$nodos;
		$xml.='</directorio>';
		header ("Content-Type:text/xml");
		echo $xml;
	}
}
?>