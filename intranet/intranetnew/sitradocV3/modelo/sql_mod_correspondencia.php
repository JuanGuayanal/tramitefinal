<?php
require 'datosGlobales.php';
class Sql_mod_correspondencia{
	protected $server=CJDBMSSQL;
	protected $user=CJDBMSSQLUSER;
	protected $pass=CJDBMSSQLPASS;
	protected $db='DB_TRAMITE_DOCUMENTARIO';
	protected $link;
	protected $consulta;
	
	public function __construct(){
		$this->conectar();
	}
	protected function conectar(){
		$this->link = mssql_connect($this->server,$this->user,$this->pass);
		mssql_select_db($this->db, $this->link);
	}
	public function ejecutar($consulta,$crud=NULL){
		$this->consulta=mssql_query($consulta);
/*		switch (strtolower($crud)){
			case 'i':
				echo "ESTA CONSULTA PARA HACER UN INSERT";
			break;
			case 'u':
				echo "ESTA CONSULTA PARA HACER UN UPDATE";
			break;
			case 'd':
				echo "ESTA CONSULTA PARA HACER UN DELETE";
			break;
			default:*/
				return $this->listado();
/*			break;
		}*/
	}
	public function listado(){
		$resultado=array();
		$resultado['filas_afectadas']=mssql_rows_affected($this->link);
		while ($row = mssql_fetch_assoc ($this->consulta)) {
			$resultado['lista'][]=$row;
		}
		return $resultado;
	}
//------------------------SENTENCIAS SQL---------------------------
	public function lista_personal($cadena){	//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'LISTA_PERSONAL','$cadena'";
		return $this->ejecutar($sql);
	}
	public function lista_personal_domicilios($id){	//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'DOMICILIOS','',$id";
		return $this->ejecutar($sql);
	}
	public function total_personal(){			//SE APLICA PARA EL POPUP BUSQUEDA DE DESTINATARIOS
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_persona 'TOTAL_PERSONAL'";
		return $this->ejecutar($sql);
	}
	public function ubigeo($ubigeo,$cod_depa,$cod_prov){			//SE APLICA PARA EL COMBO FROMAS DE ENVIO DE CORRESPONDENCIA
		$sql="EXECUTE DB_GENERAL.dbo.sp_get_ubigeo '$ubigeo','$cod_depa','$cod_prov'";
		return $this->ejecutar($sql);
	}
	public function formas_de_envio(){			//SE APLICA PARA EL COMBO FROMAS DE ENVIO DE CORRESPONDENCIA
		$sql="EXECUTE SP_CO_FORMA_ENVIO";
		return $this->ejecutar($sql);
	}
	public function combo_clase_documentos($coddep,$tipo,$idclasedoc,$numdoc,$anio,$iddeporigen){			//SE APLICA PARA EL COMBO CLASE DE DOCUMENTO
		$sql="EXECUTE sp_get_documento 'LISTA_CLASE_DOCS',$coddep,$tipo,$idclasedoc,'$numdoc','$anio',$iddeporigen";
		return $this->ejecutar($sql);
	}
	public function combo_anios(){						//SE APLICA PARA EL COMBO AÑOS
		$sql="EXECUTE sp_get_anio";
		return $this->ejecutar($sql);
	}
	public function combo_dependencias($id=0){			//SE APLICA PARA EL COMBO DEPENDENCIAS
		$sql="EXECUTE sp_get_dependencias $id";
		return $this->ejecutar($sql);
	}
	public function multiple_busca_documento($coddep,$tipo,$idclasedoc,$numdoc,$anio,$iddeporigen,$asunto,$start,$len){
		$sql="EXECUTE sp_get_documento 'BUSCAR',$coddep,$tipo,$idclasedoc,'$numdoc','$anio',$iddeporigen,'$asunto',$start,$len";
		return $this->ejecutar($sql);
	}
//---------------------------------------------------------------------
}
?>