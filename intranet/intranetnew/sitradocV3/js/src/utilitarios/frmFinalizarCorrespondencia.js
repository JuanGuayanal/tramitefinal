$(document).ready(function() {
		//LLENAR COMBO CLASE DOCUMENTO POR DEPENDENCIA DEL USUARIO
		$("#finalizar").click(function(){
		var mensaje='Se han detectado los siguientes errores:\n';
		if( $("#claseDoc").val()=='none'){mensaje=mensaje+'Debe seleccionar una clase de documento.\n';}
		if( $.trim($("#numDoc").val())==''){mensaje=mensaje+'El Nro Indicativo del documento es obligatorio.\n';}
		if( $.trim($("#obs").val())==''){mensaje=mensaje+'Es obligatorio escribir el motivo de finalizacion.';}
			if( ($.trim($("#numDoc").val())=='') || ($.trim($("#obs").val())=='') || ($("#claseDoc").val()=='none')){
				alert(mensaje);
			}
			else{
				$.ajax({
						async:false,
						url: '/institucional/aplicativos/oad/sitradocV2/index.php?accion=FinalizarCorrespondencia',
						type:'POST',
						data:{obs:$("#obs").val(),
							claseDoc:$("#claseDoc").val(),
							numDoc:$("#numDoc").val(),
							anio:$("#i_anio").val(),
							dependencia:$("#dependencia").val()
							},
						success: function(data){
							alert(data)
						}
				});
			}
		})
		//LLENA COMBO AÑOS
		$.ajax({
				async:false,
				dataType: "json",
				url: '/sitradocV3/index.php?accion=correspondencia_combo_anios',
				success: function(data){
					for(var i = 0; i < data.length; i++) {
						var option = data[i.toString()];
						$("#i_anio").append('<option value="'+option.DESCRIPCION+'">'+option.DESCRIPCION+'</option>');
					}   
				}
			});


});