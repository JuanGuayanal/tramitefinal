<?
require('auth.php');
include('modulosadmin/claseAutenticacionUsuarioExterno.inc.php');

$autenticacion=new AutenticacionUsuarioExterno;

if ($autenticacion->checkIP()){
	session_name("cod_val");
	session_start();
	$_POST['code']=$_SESSION["code_sha"];
	session_destroy();
	session_unset();
	unset($_SESSION);
}

begin_session();

if(status_secure()){
	header("Location: $INDEX_PAGE");
	exit;
}else{
	if($_POST){
		$username = $_POST['uname'];
		$userpass = $_POST['upass'];
	
		if ($autenticacion->checkIP()){			
			if($autenticacion->validaIntento()){
				if(autentifica()){
					// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
					$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
					header("Location: $destination");
					exit;
				}
			}
		}else{
			if(autentifica()){
				// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
				$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
				header("Location: $destination");
				exit;
			}
		}	
	}
}
session_name("cod_val");
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - Produce</title>
<link href="/estilos/3/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/REG.css" rel="stylesheet" type="text/css" />
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->

<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/login_jquery.js"></script>

<script language="JavaScript" type="text/javascript">
<!--

function setFocus()
{
    document.frmLogin.uname.focus();
}

function submit_login()
{
    if (document.frmLogin.uname.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.uname.focus();
        return false;
    } else if (document.frmLogin.upass.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.upass.focus();
        return false;
    } else if (document.frmLogin.codigo.value == "") {
        alert('Ingrese su C�digo de Acceso');
        document.frmLogin.codigo.focus();
        return false;
	} else if (document.frmLogin.answer.value == "") {
        alert('Ingrese su Datos de Autenticaci�n');
        document.frmLogin.answer.focus();
        return false;
	} else if (document.frmLogin.cod_val.value == "") {
        alert('Ingrese el C�digo Autenticaci�n de la Imagen');
        document.frmLogin.cod_val.focus();
        return false;
    } else {
        return true;
    }
}
//-->
</script>

</head>

<body class="WEB_MAQUE_body">
	  <p>&nbsp;</p>
	  <p>&nbsp;</p>
	  <p>&nbsp;</p>
<div class="WEB_MAQUE_Alineacion3">
	<div class="WEB_MAQUE_Fondo5"><img src="images/0/0/imagen_titulo_home.gif" alt="" width="387" height="22" /></div>
<div class="WEB_MAQUE_Fondo6">
    	<div class="WEB_MAQUE_Fondo7">
    		<div class="WEB_CONTE_bloqueModulos1">
                <div class="WEB_CONTE_bloqueSombra" id="ID_login">
            	
                    <div class="WEB_CONTE_grupoSombraArriba">
                      <div class="WEB_CONTE_sombra1"></div>
                      	<div class="WEB_CONTE_sombraHorizontalArriba"></div>
                        <div class="WEB_CONTE_sombra2"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraMedio">
                      <div class="WEB_CONTE_sombraVerticalIzquierdo"></div>
                        <div class="WEB_CONTE_bloqueEspaciado">
                            <div class="WEB_CONTE_bloqueFondo">
                                <div class="WEB_CONTE_bloqueEspaciadoCabecera">
                                    <div class="WEB_CONTE_bloqueCabecera">
                                        <div class="WEB_CONTE_bloqueCabeceraParte1 GEN_floatLeft"></div>
                                                <div class="WEB_CONTE_bloqueCabeceraParte2 GEN_floatLeft"><img src="images/0/0/titulo_login.jpg" alt="" width="103" height="13" class="WEB_ICONO_login" /></div>
                                      <div class="WEB_CONTE_bloqueCabeceraParte3 GEN_floatRight"></div>
                                            <div class="GEN_clearBoth"></div>
                                    </div>
                              	</div>
                                <div class="WEB_CONTE_sombraContenido">
                                	<div class="WEB_CONTE_login">
                                    	<div class="REG_CONTE_moduloLogin">
                                        	<form <?php echo $_SERVER['PHP_SELF']; ?>  method="post" name="frmLogin" class="REG_CONTE_form1">
                                        		<div class="REG_CONTE_bloqueLogin1">
                                            		<div class="REG_CONTE_loginImagen GEN_floatLeft"><img src="images/0/0/imagen_login.jpg" alt="" width="96" height="98" /></div>
                                                    <div class="REG_CONTE_loginFormulario GEN_floatLeft">
                                                    	<fieldset class="REG_CONTE_fieldset2">
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="email" class="REG_CONTE_label2">Usuario :</label>
                                                                <input id="uname" type="text" name="uname" value="<?php echo $_POST['uname'];?>"  class="REG_CONTE_input3">
                                                                <br />
                                                            </div>
                                                        
                                                        	<!-- <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Tiene Login?	 :</label>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">No</span>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">Si</span>
                                                                <br />
                                                            </div>
                                                        
                                                                <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Login :</label>
                                                                <input id="email_amigo" name="email_amigo" class="REG_CONTE_input3">
                                                                <br />
                                                            </div>-->
                                                        
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Contrase&ntilde;a :</label>
                                                                <input name="upass" type="password" maxlength="30" id="upass" class="REG_CONTE_input3">
                                                                <br />
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                <div class="GEN_clearLeft"></div>
                                            	</div>
                                            	<div class="REG_CONTE_bloqueLogin2">
                                                	<div class="REG_CONTE_mensajeLogin GEN_floatLeft GEN_horizontalCenter">
                                                	  <p class="REG_texto1">
													  <!-- Texto Error --> &nbsp;
													  </p>
                                                	</div>
                                                    <div class="REG_CONTE_digitosLogin GEN_floatLeft">
                                                   	  <div class="REG_CONTE_bloqueDigito">
                                                            <br class="GEN_clearLeft" />
                                                      </div>
                                                        <div class="REG_CONTE_boton1">
                                                        	  <input type="submit" name="button" id="button" value="Ingresar" class="REG_boton1" />
                                                        </div>
                                                    </div>
                                                    <div class="GEN_clearLeft"></div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                        <div class="WEB_CONTE_sombraVerticalDerecho"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraAbajo">
                        <div class="WEB_CONTE_sombra3"></div>
                        <div class="WEB_CONTE_sombraHorizontalAbajo"></div>
                        <div class="WEB_CONTE_sombra4"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                  </div>
          	</div>
        </div>
    </div>
</div>
<div class="WEB_CONTE_creditosLogin GEN_horizontalCenter"><span class="WEB_textoCreditosLogin">Calle Uno Oeste N� 060 - Urb. C�rpac, San Isidro - Lima - Per� - RUC: 20504794637
Central Telef�nica: 616-2222 - Email: <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">intranet@CONVENIO_SITRADOC.gob.pe</a></span></div>
</body>
</html>
<?
session_destroy(); 
?>