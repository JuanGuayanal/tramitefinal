<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registro de Visitas en el Ministerio de la Producci&oacute;n</title>
</head>

<body>
<table width="98%" border="0" align="center">
  <!--
  <tr>
      <td colspan="3" class="tit-documento-intranet"><b>&nbsp;N&uacute;mero de visitas a partir del 01/01/2011: {$totalVisitas}</b></td>
    </tr>
    -->
  <tr>
      <td>&nbsp;</td>
      <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td width="100"><strong><a href="{$frmUrl}?accion={$accion.IMPRIME_VISITA}&tipo={$tipo}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}&print=&nombreVisitante={$nombreVisitante}&dniVisitante={$dniVisitante}&funcionario={$funcionario}&horInicio={$horInicio}&horFin={$horFin}&tipo2={$tipo2}&tipo3={$tipo3}&mode=btn" target="_blank"><img src="/img/800x600/ico_print-small.gif" width="100" height="17" hspace="2" vspace="2" border="0" align="absmiddle" alt="Imprimir Vista"><!--Imprimir--></a></strong> </td>
    <td width="30%"> 
    <!--<a href="javascript: window.open('masvisitados.php','newWindow','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=650,height=550'); void('');"  ><img src="/img/800x600/ico-mas-visitados.gif" width="120" height="17" hspace="2" vspace="2" border="0" align="absmiddle" alt="Reporte de los Funcionarios m&aacute;s visitados"></a>-->
    
    <!--
    <a href="masvisitados.php" target="_blank"><img src="/img/800x600/ico-mas-visitados.gif" width="120" height="17" hspace="2" vspace="2" border="0" align="absmiddle" alt="Imprimir Vista"></b></a>
    -->    </td>
    <td width="90%">
    
    <!--<a href="javascript: window.open('masvisitados-direccion.php','newWindow','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=700'); void('');"  ><img src="/img/800x600/ico-mas-visitados-direccion.gif" width="200" height="17" hspace="2" vspace="2" border="0" align="absmiddle" alt="Reporte de los m&aacute;s visitados por Direcci&oacute;n" /></a>-->
            <!--
    <a href="masvisitados.php" target="_blank"><img src="/img/800x600/ico-mas-visitados.gif" width="120" height="17" hspace="2" vspace="2" border="0" align="absmiddle" alt="Imprimir Vista"></b></a>
    -->    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="textoblack">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="textoblack"><table width="100%" border="0">
      <tr>
        <td><img src="/img/800x600/ico.entrada_SITRADOC.jpg" width="15" height="15" /></td>
        <td class="textograyvisitas" align="left">Ingreso al SITRADOC</td>
        <td><img src="/img/800x600/ico.entrada.jpg" width="15" height="15" /></td>
        <td class="textograyvisitas" align="left">Ingreso a la Dependencia</td>
        <td><img src="/img/800x600/ico.salida.jpg" width="15" height="15" /></td>
        <td class="textograyvisitas" align="left">Salida de la Dependencia</td>
        <td><img src="/img/800x600/ico.salida_SITRADOC.jpg" width="15" height="15" /></td>
        <td class="textograyvisitas" align="left">Salida del SITRADOC</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="textoblack">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="textoblack"><B>CONTROL DE VISITAS EN EL MINISTERIO DE LA PRODUCCI&Oacute;N  </B></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>&nbsp;</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>N&deg;</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>VISITANTE</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DOCUMENTO<br />
    (DNI / Carnet <br />
    Extranjer&iacute;a/Otros)</strong></td>
    <!--
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>ASUNTO</strong></td>
    -->
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>PERSONA<br />
    VISITADA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DEPENDENCIA</strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>FECHA DE <br />
      VISITA<br />
    </strong></td>
    <!--
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA<br />
      PROGR.</strong></td>
      -->
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA<br />
      ENTRADA
    </strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA<br />
      SALIDA
    </strong></td>
  </tr>
  {section name=i loop=$visi}
  <tr bgcolor="#FFFFF2">
    <td valign="top" class="textograyintranet" style="border-bottom: 1px solid #EBEBEB">    
    
    {if ($tipo=='P' && $codigoTrabajador==239)}<a href="{$frmUrl}?accion={$accion.REGISTRA_ENTRADA_SITRADOC}&id={$visi[i].id}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}">
    <img src="/img/800x600/ico.entrada_SITRADOC.jpg" alt="[ REGISTRAR ENTRADA SITRADOC ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>
	{/if}
    
    {if ($tipo=='Y')}{ if $visi[i].horaIngreso==""}<a href="{$frmUrl}?accion={$accion.REGISTRA_ENTRADA}&id={$visi[i].id}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}">
    <img src="/img/800x600/ico.entrada.jpg" alt="[ REGISTRAR ENTRADA A LA DEPENDENCIA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
    
    {if ($visi[i].horaIngreso!="" && $visi[i].horaSalida=="")}<a href="{$frmUrl}?accion={$accion.REGISTRA_SALIDA}&id={$visi[i].id}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}">
    <img src="/img/800x600/ico.salida.jpg" alt="[ REGISTRAR SALIDA DE LA DEPENDENCIA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
    
    {if ($visi[i].horaIngreso!="" && $visi[i].horaSalida!="" && $codigoTrabajador==239)}<a href="{$frmUrl}?accion={$accion.REGISTRA_SALIDA_SITRADOC}&id={$visi[i].id}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}">
    <img src="/img/800x600/ico.salida_SITRADOC.jpg" alt="[ REGISTRAR SALIDA DEL SITRADOC]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
    
    {/if}</td>
    
	<td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$smarty.section.i.iteration}</td>
	<td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].nomb}<!--{$visi[i].codvisita}-->
   </td>
    <td nowrap="nowrap" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center"><!--{$visi[i].docu}: -->{$visi[i].ndoc}</td>
    <!--
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].asun}</td>
    -->
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].func} <!--{$visi[i].codfunci}--> </td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].siglas}<!--{$visi[i].coddepen}--></td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].fecE}<br /></td>
    <!--
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">    {$visi[i].horaProg}</td>
    -->
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].horaE}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].horaS}</td>
  </tr>
  {sectionelse}
  <tr>
    <td colspan="10" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="10" class="textograyintranet">No hay visitantes.</td>
  </tr>
  {/section}
</table>
</body>
</html>
