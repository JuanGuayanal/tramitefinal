<?php
include_once('modulosadmin/claseIntranet.inc.php');

$objIntranet = new Intranet();

$destination = $_SERVER['DOCUMENT_ROOT'] . substr($_SERVER['PHP_SELF'],11);

// print($_SERVER['DOCUMENT_ROOT'] . $destination);

// open the file in a binary mode
$fp = fopen($destination, 'rb');

//Begin writing headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public"); 
header("Content-Description: File Transfer");

// header('Content-Type: application/zip');
header('Content-Disposition: attachment; filename="' . basename($destination) . '";');
header("Content-Transfer-Encoding: binary");
header('Content-Length: ' . filesize($destination));

// dump the picture and stop the script
fpassthru($fp);
fclose($fp);

exit;

?>