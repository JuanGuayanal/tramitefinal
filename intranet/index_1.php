<?
require_once('datosGlobales.php');
require('auth.php');
include('modulosadmin/claseAutenticacionUsuarioExterno.inc.php');

$autenticacion=new AutenticacionUsuarioExterno;

if ($autenticacion->checkIP()){
	session_name("cod_val");
	session_start();
	$_POST['code']=$_SESSION["code_sha"];
	session_destroy();
	session_unset();
	unset($_SESSION);
}

begin_session();

if(status_secure()){
	header("Location: $INDEX_PAGE");
	exit;
}else{
	if($_POST){
		$username = $_POST['uname'];
		$userpass = $_POST['upass'];
	
		if ($autenticacion->checkIP()){			
			if($autenticacion->validaIntento()){
				if(autentifica()){
					// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
					$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
					header("Location: $destination");
					exit;
				}
			}
		}else{
			if(autentifica()){
				// $destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . session_id() . "/" . $INDEX_PAGE;
				$destination = "https://" . $_SERVER['HTTP_HOST'] . "/" . $INDEX_PAGE;
				header("Location: $destination");
				exit;
			}
		}	
	}
}
session_name("cod_val");
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - Ministerio de la Producci�n</title>
<link href="/estilos/3/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link href="/estilos/3/REG.css" rel="stylesheet" type="text/css" />
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/login_jquery.js"></script>
<script language="JavaScript" type="text/javascript">
<!--

function setFocus()
{
    document.frmLogin.uname.focus();
}

function submit_login()
{
    if (document.frmLogin.uname.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.uname.focus();
        return false;
    } else if (document.frmLogin.upass.value == "") {
        alert('Ingrese su nombre de usuario y contrase�a');
        document.frmLogin.upass.focus();
        return false;
    } else if (document.frmLogin.codigo.value == "") {
        alert('Ingrese su C�digo de Acceso');
        document.frmLogin.codigo.focus();
        return false;
	} else if (document.frmLogin.answer.value == "") {
        alert('Ingrese su Datos de Autenticaci�n');
        document.frmLogin.answer.focus();
        return false;
	} else if (document.frmLogin.cod_val.value == "") {
        alert('Ingrese el C�digo Autenticaci�n de la Imagen');
        document.frmLogin.cod_val.focus();
        return false;
    } else {
        return true;
    }
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 104px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #007d94;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_dragLayer(objName,x,hL,hT,hW,hH,toFront,dropBack,cU,cD,cL,cR,targL,targT,tol,dropJS,et,dragJS) { //v4.01
  //Copyright 1998 Macromedia, Inc. All rights reserved.
  var i,j,aLayer,retVal,curDrag=null,curLeft,curTop,IE=document.all,NS4=document.layers;
  var NS6=(!IE&&document.getElementById), NS=(NS4||NS6); if (!IE && !NS) return false;
  retVal = true; if(IE && event) event.returnValue = true;
  if (MM_dragLayer.arguments.length > 1) {
    curDrag = MM_findObj(objName); if (!curDrag) return false;
    if (!document.allLayers) { document.allLayers = new Array();
      with (document) if (NS4) { for (i=0; i<layers.length; i++) allLayers[i]=layers[i];
        for (i=0; i<allLayers.length; i++) if (allLayers[i].document && allLayers[i].document.layers)
          with (allLayers[i].document) for (j=0; j<layers.length; j++) allLayers[allLayers.length]=layers[j];
      } else {
        if (NS6) { var spns = getElementsByTagName("span"); var all = getElementsByTagName("div"); 
          for (i=0;i<spns.length;i++) if (spns[i].style&&spns[i].style.position) allLayers[allLayers.length]=spns[i];}
        for (i=0;i<all.length;i++) if (all[i].style&&all[i].style.position) allLayers[allLayers.length]=all[i]; 
    } }
    curDrag.MM_dragOk=true; curDrag.MM_targL=targL; curDrag.MM_targT=targT;
    curDrag.MM_tol=Math.pow(tol,2); curDrag.MM_hLeft=hL; curDrag.MM_hTop=hT;
    curDrag.MM_hWidth=hW; curDrag.MM_hHeight=hH; curDrag.MM_toFront=toFront;
    curDrag.MM_dropBack=dropBack; curDrag.MM_dropJS=dropJS;
    curDrag.MM_everyTime=et; curDrag.MM_dragJS=dragJS;
    curDrag.MM_oldZ = (NS4)?curDrag.zIndex:curDrag.style.zIndex;
    curLeft= (NS4)?curDrag.left:(NS6)?parseInt(curDrag.style.left):curDrag.style.pixelLeft; 
    if (String(curLeft)=="NaN") curLeft=0; curDrag.MM_startL = curLeft;
    curTop = (NS4)?curDrag.top:(NS6)?parseInt(curDrag.style.top):curDrag.style.pixelTop; 
    if (String(curTop)=="NaN") curTop=0; curDrag.MM_startT = curTop;
    curDrag.MM_bL=(cL<0)?null:curLeft-cL; curDrag.MM_bT=(cU<0)?null:curTop-cU;
    curDrag.MM_bR=(cR<0)?null:curLeft+cR; curDrag.MM_bB=(cD<0)?null:curTop+cD;
    curDrag.MM_LEFTRIGHT=0; curDrag.MM_UPDOWN=0; curDrag.MM_SNAPPED=false; //use in your JS!
    document.onmousedown = MM_dragLayer; document.onmouseup = MM_dragLayer;
    if (NS) document.captureEvents(Event.MOUSEDOWN|Event.MOUSEUP);
  } else {
    var theEvent = ((NS)?objName.type:event.type);
    if (theEvent == 'mousedown') {
      var mouseX = (NS)?objName.pageX : event.clientX + document.body.scrollLeft;
      var mouseY = (NS)?objName.pageY : event.clientY + document.body.scrollTop;
      var maxDragZ=null; document.MM_maxZ = 0;
      for (i=0; i<document.allLayers.length; i++) { aLayer = document.allLayers[i];
        var aLayerZ = (NS4)?aLayer.zIndex:parseInt(aLayer.style.zIndex);
        if (aLayerZ > document.MM_maxZ) document.MM_maxZ = aLayerZ;
        var isVisible = (((NS4)?aLayer.visibility:aLayer.style.visibility).indexOf('hid') == -1);
        if (aLayer.MM_dragOk != null && isVisible) with (aLayer) {
          var parentL=0; var parentT=0;
          if (NS6) { parentLayer = aLayer.parentNode;
            while (parentLayer != null && parentLayer.style.position) {             
              parentL += parseInt(parentLayer.offsetLeft); parentT += parseInt(parentLayer.offsetTop);
              parentLayer = parentLayer.parentNode;
          } } else if (IE) { parentLayer = aLayer.parentElement;       
            while (parentLayer != null && parentLayer.style.position) {
              parentL += parentLayer.offsetLeft; parentT += parentLayer.offsetTop;
              parentLayer = parentLayer.parentElement; } }
          var tmpX=mouseX-(((NS4)?pageX:((NS6)?parseInt(style.left):style.pixelLeft)+parentL)+MM_hLeft);
          var tmpY=mouseY-(((NS4)?pageY:((NS6)?parseInt(style.top):style.pixelTop) +parentT)+MM_hTop);
          if (String(tmpX)=="NaN") tmpX=0; if (String(tmpY)=="NaN") tmpY=0;
          var tmpW = MM_hWidth;  if (tmpW <= 0) tmpW += ((NS4)?clip.width :offsetWidth);
          var tmpH = MM_hHeight; if (tmpH <= 0) tmpH += ((NS4)?clip.height:offsetHeight);
          if ((0 <= tmpX && tmpX < tmpW && 0 <= tmpY && tmpY < tmpH) && (maxDragZ == null
              || maxDragZ <= aLayerZ)) { curDrag = aLayer; maxDragZ = aLayerZ; } } }
      if (curDrag) {
        document.onmousemove = MM_dragLayer; if (NS4) document.captureEvents(Event.MOUSEMOVE);
        curLeft = (NS4)?curDrag.left:(NS6)?parseInt(curDrag.style.left):curDrag.style.pixelLeft;
        curTop = (NS4)?curDrag.top:(NS6)?parseInt(curDrag.style.top):curDrag.style.pixelTop;
        if (String(curLeft)=="NaN") curLeft=0; if (String(curTop)=="NaN") curTop=0;
        MM_oldX = mouseX - curLeft; MM_oldY = mouseY - curTop;
        document.MM_curDrag = curDrag;  curDrag.MM_SNAPPED=false;
        if(curDrag.MM_toFront) {
          eval('curDrag.'+((NS4)?'':'style.')+'zIndex=document.MM_maxZ+1');
          if (!curDrag.MM_dropBack) document.MM_maxZ++; }
        retVal = false; if(!NS4&&!NS6) event.returnValue = false;
    } } else if (theEvent == 'mousemove') {
      if (document.MM_curDrag) with (document.MM_curDrag) {
        var mouseX = (NS)?objName.pageX : event.clientX + document.body.scrollLeft;
        var mouseY = (NS)?objName.pageY : event.clientY + document.body.scrollTop;
        newLeft = mouseX-MM_oldX; newTop  = mouseY-MM_oldY;
        if (MM_bL!=null) newLeft = Math.max(newLeft,MM_bL);
        if (MM_bR!=null) newLeft = Math.min(newLeft,MM_bR);
        if (MM_bT!=null) newTop  = Math.max(newTop ,MM_bT);
        if (MM_bB!=null) newTop  = Math.min(newTop ,MM_bB);
        MM_LEFTRIGHT = newLeft-MM_startL; MM_UPDOWN = newTop-MM_startT;
        if (NS4) {left = newLeft; top = newTop;}
        else if (NS6){style.left = newLeft; style.top = newTop;}
        else {style.pixelLeft = newLeft; style.pixelTop = newTop;}
        if (MM_dragJS) eval(MM_dragJS);
        retVal = false; if(!NS) event.returnValue = false;
    } } else if (theEvent == 'mouseup') {
      document.onmousemove = null;
      if (NS) document.releaseEvents(Event.MOUSEMOVE);
      if (NS) document.captureEvents(Event.MOUSEDOWN); //for mac NS
      if (document.MM_curDrag) with (document.MM_curDrag) {
        if (typeof MM_targL =='number' && typeof MM_targT == 'number' &&
            (Math.pow(MM_targL-((NS4)?left:(NS6)?parseInt(style.left):style.pixelLeft),2)+
             Math.pow(MM_targT-((NS4)?top:(NS6)?parseInt(style.top):style.pixelTop),2))<=MM_tol) {
          if (NS4) {left = MM_targL; top = MM_targT;}
          else if (NS6) {style.left = MM_targL; style.top = MM_targT;}
          else {style.pixelLeft = MM_targL; style.pixelTop = MM_targT;}
          MM_SNAPPED = true; MM_LEFTRIGHT = MM_startL-MM_targL; MM_UPDOWN = MM_startT-MM_targT; }
        if (MM_everyTime || MM_SNAPPED) eval(MM_dropJS);
        if(MM_dropBack) {if (NS4) zIndex = MM_oldZ; else style.zIndex = MM_oldZ;}
        retVal = false; if(!NS) event.returnValue = false; }
      document.MM_curDrag = null;
    }
    if (NS) document.routeEvent(objName);
  } return retVal;
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
</head>

<body onLoad="document.forms.frmLogin.uname.focus()" class="WEB_MAQUE_body">
<div id="Layer1" style="position:absolute; left:0px; top:524px; width:100%; height:44px; z-index:1">
  <div align="center">
    <table width="800" height="50"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="500" valign="top"><div align="right"><span class="WEB_textoCreditosLogin">Calle Uno Oeste N&deg; 060 - Urb. C&oacute;rpac, San Isidro - Lima - Per&uacute;<br />
        RUC: 20504794637 - Central Telef&oacute;nica: 616-2222<br />
        Horario de atenci&oacute;n en Mesa de Partes: Lunes a viernes de 08:30 a 16:30 hrs,<br>s&aacute;bados de 09:00 a 12:30 hrs.  <!--<a onclick="window.open(this.href,&#39;&#39;,&#39;resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=400,height=318,left=200,top=200,status&#39;); return false" href="http://www.CONVENIO_SITRADOC.gob.pe/CONVENIO_SITRADOC/aviso-oada/aviso-oada.htm">(Ver nota)</a>--><br />
        <a href="http://www.CONVENIO_SITRADOC.gob.pe/portal/portal/apsportalCONVENIO_SITRADOC/internaCONVENIO_SITRADOC?ARE=1&amp;JER=686" target="_blank">Acerca de la Intranet</a> - <a href="http://www.CONVENIO_SITRADOC.gob.pe/portal/portal/apsportalCONVENIO_SITRADOC/internaCONVENIO_SITRADOC?ARE=1&amp;JER=406" target="_blank">Directorio</a> - <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">intranet@CONVENIO_SITRADOC.gob.pe</a></span></div></td>
        <td width="10" valign="bottom"><div align="center"><img src="/images/0/0/separa.gif" width="1" height="50" align="bottom" /></div></td>
        <td width="245" valign="bottom"><img src="/images/0/0/logo_CONVENIO_SITRADOC_web_pie.gif" width="235" height="50" align="bottom" /></td>
      </tr>
    </table>
</div>
</div>

<!--// Layer emergente-->
<div id="Layer2" style="position:absolute; left:17px; top:23px; width:308; height:370; z-index:2"><img src="popud-simulacro-sismo.jpg" width="308" height="370" border="0" usemap="#Map2" />
  <map name="Map2" id="Map2">
    <area shape="rect" coords="239,1,306,34" href="#" onclick="MM_showHideLayers('Layer2','','hide')" alt="Cerrar" />
  </map>
</div>
<div align="center">
<table width="600" height="387" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td width="362"><table width="362"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="362" valign="top"><table id="Table_01" width="362" height="387" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td height="387" valign="top">	 
<div class="WEB_MAQUE_Alineacion3">
	<div class="WEB_MAQUE_Fondo5"><img src="/images/0/0/imagen_titulo_home.gif" alt="" width="387" height="35" /></div>
<div class="WEB_MAQUE_Fondo6">
    	<div class="WEB_MAQUE_Fondo7">
    		<div class="WEB_CONTE_bloqueModulos1">
                <div class="WEB_CONTE_bloqueSombra" id="ID_login">
            	
                    <div class="WEB_CONTE_grupoSombraArriba">
                      <div class="WEB_CONTE_sombra1"></div>
                      	<div class="WEB_CONTE_sombraHorizontalArriba"></div>
                        <div class="WEB_CONTE_sombra2"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraMedio">
                      <div class="WEB_CONTE_sombraVerticalIzquierdo"></div>
                        <div class="WEB_CONTE_bloqueEspaciado">
                            <div class="WEB_CONTE_bloqueFondo">
                                <div class="WEB_CONTE_bloqueEspaciadoCabecera">
                                    <div class="WEB_CONTE_bloqueCabecera">
                                        <div class="WEB_CONTE_bloqueCabeceraParte1 GEN_floatLeft"></div>
                                                <div class="WEB_CONTE_bloqueCabeceraParte2 GEN_floatLeft"><img src="/images/0/0/titulo_login.jpg" alt="" width="117" height="13" class="WEB_ICONO_login" /></div>
                                      <div class="WEB_CONTE_bloqueCabeceraParte3 GEN_floatRight"></div>
                                            <div class="GEN_clearBoth"></div>
                                    </div>
                              	</div>
                                <div class="WEB_CONTE_sombraContenido">
                                	<div class="WEB_CONTE_login">
                                    	<div class="REG_CONTE_moduloLogin">
                                        	<form <?php echo $_SERVER['PHP_SELF']; ?>  method="post" name="frmLogin" class="REG_CONTE_form1">
                                        		<div class="REG_CONTE_bloqueLogin1">
                                            		<div class="REG_CONTE_loginImagen GEN_floatLeft"><img src="/images/0/0/imagen_login.jpg" alt="" width="96" height="98" /></div>
                                                    <div class="REG_CONTE_loginFormulario GEN_floatLeft">
                                                    	<fieldset class="REG_CONTE_fieldset2">
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="email" class="REG_CONTE_label2">Usuario :</label>
																<input id="uname" type="text" name="uname" value="<?php echo $_POST['uname'];?>"  class="REG_CONTE_input3">
																<div align="left" class="Estilo1"></div>
                                                                <br />
                                                            </div>
                                                        
                                                        	<!-- <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Tiene Login?	 :</label>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">No</span>
                                                                <input type="radio" name="radio" id="radio" value="radio" class="REG_CONTE_input4" /><span class="REG_texto1">Si</span>
                                                                <br />
                                                            </div>
                                                        
                                                                <div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Login :</label>
                                                                <input id="email_amigo" name="email_amigo" class="REG_CONTE_input3">
                                                                <br />
                                                            </div>-->
                                                        
                                                        	<div class="REG_CONTE_fila1">  
                                                                <label for="nombre_amigo" class="REG_CONTE_label2">Contrase&ntilde;a :</label>
                                                                <input name="upass" type="password" maxlength="30" id="upass" class="REG_CONTE_input3">
																<div align="left" class="Estilo1"></div>
                                                                
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                <div class="GEN_clearLeft"></div>
                                            	</div>
                                            	<div class="REG_CONTE_bloqueLogin2">
                                                	<div class="REG_CONTE_mensajeLogin GEN_floatLeft GEN_horizontalCenter">
                                                	  <p class="REG_texto1">
													  <!-- Texto Error --> &nbsp;
													  </p>
                                                	</div>
                                                    <div class="REG_CONTE_digitosLogin GEN_floatLeft">
                                                   	  <div class="REG_CONTE_bloqueDigito">
                                                            <br class="GEN_clearLeft" />
                                                      </div>
                                                        <div class="REG_CONTE_boton1">
                                                        	  <input type="submit" name="button" id="button" value="Ingresar" class="REG_boton1" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                        <div class="WEB_CONTE_sombraVerticalDerecho"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_grupoSombraAbajo">
                        <div class="WEB_CONTE_sombra3"></div>
                        <div class="WEB_CONTE_sombraHorizontalAbajo"></div>
                        <div class="WEB_CONTE_sombra4"></div>
                        <div class="GEN_clearLeft"></div>
                    </div>
              </div>
          	</div>
        </div>
    </div>
</div>
<div class="WEB_CONTE_creditosLogin GEN_horizontalCenter"></div>
	</td>
  </tr>
</table>
</div>
</body>
</html>
<?
session_destroy(); 
?>
