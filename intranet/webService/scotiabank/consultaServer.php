<?php
require_once('nusoap/lib/nusoap.php');

//creamos el objeto de tipo soap_server
$server = new soap_server;
$server->configureWSDL('ejecutarTransaccionScotiabank', 'urn:ejecutarTransaccionScotiabank');  

/*$server->wsdl->addComplexType('tramaDerecho','complexType','struct','all','',
               array(
                        'trama' => array('name' => 'trama', 'type' => 'xsd:string')//,
                        
                        ));*/
						
//registramos la funci�n que vamos a implementar
//$server->register('hello');
//$server->register('consultaDeudaDerechosPesca');
$server->register('ejecutarTransaccionScotiabank',
                  array('trama' => 'xsd:string'),
				  array('tramaDerecho' => 'xsd:string')
                 /* array('return'=>'tns:tramaDerecho'),
                  'urn:consultaDeudaDerechosPesca',
                  'urn:consultaDeudaDerechosPesca#tramaDerecho',
                  'rpc',
                  'encoded',
                  'Este m�todo devuelve la trama.'*/);  

//implementamos la funci�n
/*function hello ($name){
return "Hola, $name.";
}*/

function ejecutarTransaccionScotiabank($trama){
	
	//if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))){
	//if(!($con=mssql_connect("SRVSQLP2",CJDBMSSQLUSER,CJDBMSSQLPASS))){
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))){
		$respuestaTrama= "No existe conexi�n a la base de datos";
	}else{ 
		$respuestaTrama= "Existe conexi�n a la BD";
	}
	if(!(mssql_select_db("DB_DNEPP",$con))){
		$respuestaTrama= "Error al elegir la base de datos";
	}
	
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		//$nombre=$_SERVER['REMOTE_HOST'];
		//$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		//$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);
	
	//Se obtienen las variables:
	$messageTypeIdentification=substr($trama,0,4);
	$primaryBitMap=substr($trama,4,16);
	$secondaryBitMap=substr($trama,20,16);
	$numeroTarjeta=substr($trama,36,18);
	$codigoProceso=substr($trama,54,6);
	$monto=substr($trama,60,12);
	$fechaHoraTransaccion=substr($trama,72,10);
	$trace=substr($trama,82,6);
	$fechaCaptura=substr($trama,88,4);
	$modoIngresoDatos=substr($trama,92,3);
	$canal=substr($trama,95,2);
	$binAdquiriente=substr($trama,97,8);
	$fwdInstitutionCode=substr($trama,105,8);
	$retrievalReferenceNumber=substr($trama,113,12);
	$terminalId=substr($trama,125,8);
	$comercio=substr($trama,133,15);
	$cardAcceptorLocation=substr($trama,148,40);
	$transactionCurrencyCode=substr($trama,188,3);
	$datosReservados=substr($trama,191,5);
	
	//Apartir de au� se va a identificar si est� consultando una deuda o un pago o una anulaci�n de pagos o extorno
	
	if($codigoProceso=="355000" && $datosReservados=="00230"){
	//Consulta de deudas
				
				$longitudRequerimiento=substr($trama,196,3);
				$codigoFormato=substr($trama,199,2);
				$binProcesador=substr($trama,201,11);
				$codigoAcreedor=substr($trama,212,11);//supongo que es el ruc o de repente el idemb
				$codigoProductoServicio=substr($trama,223,8);///supongo que aqu� debe estar el idemb y/o codigPago a�o y mes del ejercicio a pagar
				$codigoPlazaRecaudador=substr($trama,231,4);
				$codigoAgenciaRecaudador=substr($trama,235,4);
				$tipoDatoConsulta=substr($trama,239,2);
				$datoConsulta=substr($trama,241,21);
				$codigoCiudad=substr($trama,262,3);
				$codigoServicio=substr($trama,265,3);
				$numeroDocumento=substr($trama,268,16);
				$numeroOperacion=substr($trama,284,12);
				$filler=substr($trama,296,20);
				$tamanoMaxBloque=substr($trama,316,5);
				$posicionUltDoc=substr($trama,321,3);
				$punteroBD=substr($trama,324,10);
				
				//First of all se guarda la trama enviada por el banco
					$sql_stConsulta = "insert into TRAMAS_RECIBIDAS_BANCO(ID_TIPO_TRAMA,TRAMA,AUDITMOD,HOST)
								VALUES (1,'$trama',getDate(),'$nombre_host')
								";
											
								if(!mssql_query($sql_stConsulta,$con)){
									echo "error en la consulta";
								}
				
				$respuestaTrama=$codigoAgenciaRecaudador;
				
				//Se va a generar la trama de respuesta:
				
				//Se consulta si es que el RUC posee embarcaciones con deuda, sino posee, se env�an los mensajes de error
				//Nombre del cliente
				$sql_st = "SELECT CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
										WHEN ID_TIPO_PERSONA=2 THEN RAZON_SOCIAL END,
								  ID
							FROM DB_GENERAL.DBO.PERSONA 
							/*WHERE NRO_DOCUMENTO='$codigoAcreedor'*/
							WHERE NRO_DOCUMENTO=RTRIM('$datoConsulta')
							";
										
							if(!mssql_query($sql_st,$con)){
								echo "error en la consulta";
							}else{
								$resultado=mssql_query($sql_st,$con);
								$num_campos=mssql_num_fields($resultado);
								$num_registros=mssql_num_rows($resultado);
								$j=$num_registros-1;
								$jj=-1;
									while($datos=mssql_fetch_array($resultado)){
										$nombrePersona=$datos[0];
										$RazonSocialPersona=$datos[1];
									}
							}
							
				if(!$nombrePersona||$nombrePersona==""){
					$error1=1;//El Ruc no es un cliente de Produce
				}
				
				//Consulta si tiene deudas activas o no
				
				if($punteroBD>0){
				$sql_st = "SELECT count(*)
							FROM CONSULTA_IMPORTES_BANCO
							/*WHERE RUC='$codigoAcreedor'*/
							WHERE RUC=RTRIM('$datoConsulta')
							AND ESTADO=0
							AND ID_CONSULTA_IMPORTE_BANCO>$punteroBD
							AND MONTO<>'0.00'
							";
				}else{
				$sql_st = "SELECT count(*)
							FROM CONSULTA_IMPORTES_BANCO
							/*WHERE RUC='$codigoAcreedor'*/
							WHERE RUC=RTRIM('$datoConsulta')
							AND ESTADO=0
							AND MONTO<>'0.00'
							";
				}						
							if(!mssql_query($sql_st,$con)){
								echo "error en la consulta";
							}else{
								$resultado=mssql_query($sql_st,$con);
								$num_campos=mssql_num_fields($resultado);
								$num_registros=mssql_num_rows($resultado);
								$j=$num_registros-1;
								$jj=-1;
									while($datos=mssql_fetch_array($resultado)){
										$contDeudas=$datos[0];
									}
							}
				if($contDeudas==0){
					$error2=1;//El Ruc no tiene deudas en Produce
				}
				
				
				$respuestaTrama="0210";
				$respuestaTrama.="B22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
				$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
				$respuestaTrama.=$codigoProceso;
				$respuestaTrama.=$monto;
				$respuestaTrama.=$fechaHoraTransaccion;
				$respuestaTrama.=$trace;
				$respuestaTrama.=$fechaCaptura;
				$respuestaTrama.=$binAdquiriente;
				$respuestaTrama.=$retrievalReferenceNumber;
				$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
				//De acuerdo al correo del 16022010, indica lo sgte:
				/*Indicar 00 para respuesta Ok, Indicar 21 para un error aplicativo Ejem: Cliente sin deuda, Cliente no existe, etc, o Indicar otro tipo de error seg�n lista.*/
				if($error1==1||$error2==1)
					$respuestaTrama.="21";
				else
					$respuestaTrama.="00";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
				
				$respuestaTrama.=$terminalId;
				$respuestaTrama.=$transactionCurrencyCode;
				$respuestaTrama.=$datosReservados;
					
				$respuestaTrama.="860";
				$respuestaTrama.="02";
				$respuestaTrama.="           ";//Bin propcesador???? varchar(11)
				$respuestaTrama.="           ";//Bin acreedor???? varchar(11) ??? es el c�digo de acreedor???
				//De acuerdo al correo del 16022010, indica lo sgte:
				/*Devolver el mismo del mensaje 200 "REC"*/
				$respuestaTrama.=$codigoProductoServicio;//codigo producto/servicio???? varchar(8)
				
				$respuestaTrama.=$codigoAgenciaRecaudador;//agencia varchar(4)
				$respuestaTrama.=$tipoDatoConsulta;//tipo identificacion 01 por servicio 02 por recibo????
				$respuestaTrama.=$datoConsulta;//numero de identificacion varchar(21)
				
			
					if($RazonSocialPersona==14420)
						$RazonSocialPersona=11060;
					elseif($RazonSocialPersona==21315)
						$RazonSocialPersona=11076;
					elseif($RazonSocialPersona==14740)
						$RazonSocialPersona=15471;
					elseif($RazonSocialPersona==14399)
						$RazonSocialPersona=11058;
					elseif($RazonSocialPersona==14439)
						$RazonSocialPersona=15247;
					elseif($RazonSocialPersona==14445)
						$RazonSocialPersona=29565;
					elseif($RazonSocialPersona==14449)
						$RazonSocialPersona=14882;
					elseif($RazonSocialPersona==14534)
						$RazonSocialPersona=14533;
					elseif($RazonSocialPersona==14693)
						$RazonSocialPersona=15453;	
					elseif($RazonSocialPersona==14363)
						$RazonSocialPersona=15193;
					elseif($RazonSocialPersona==12159)
						$RazonSocialPersona=14855;
					elseif($RazonSocialPersona==14605)
						$RazonSocialPersona=14954;
					elseif($RazonSocialPersona==14729)
						$RazonSocialPersona=11077;
					elseif($RazonSocialPersona==14939||$RazonSocialPersona==9436)
						$RazonSocialPersona=14567;
					elseif($RazonSocialPersona==11471)
						$RazonSocialPersona=14797;
					elseif($RazonSocialPersona==14840)
						$RazonSocialPersona=14308;
					elseif($RazonSocialPersona==14244||$RazonSocialPersona==14245)
						$RazonSocialPersona=15121;
					elseif($RazonSocialPersona==14922)
						$RazonSocialPersona=14472;
					elseif($RazonSocialPersona==14438)
						$RazonSocialPersona=15246;
					elseif($RazonSocialPersona==14864)
						$RazonSocialPersona=24472;
					elseif($RazonSocialPersona==14436)
						$RazonSocialPersona=15244;
					elseif($RazonSocialPersona==14656||$RazonSocialPersona==14655)
						$RazonSocialPersona=15409;
					elseif($RazonSocialPersona==14660)
						$RazonSocialPersona=15410;
					elseif($RazonSocialPersona==30810||$RazonSocialPersona==14576)
						$RazonSocialPersona=15334;
					elseif($RazonSocialPersona==11608)
						$RazonSocialPersona=24443;
					elseif($RazonSocialPersona==15396)
						$RazonSocialPersona=14302;
					elseif($RazonSocialPersona==14625)
						$RazonSocialPersona=14964;
					elseif($RazonSocialPersona==14701)
						$RazonSocialPersona=15001;
					elseif($RazonSocialPersona==14377)
						$RazonSocialPersona=15204;
					elseif($RazonSocialPersona==15421||$RazonSocialPersona==14679)
						$RazonSocialPersona=9448;
					elseif($RazonSocialPersona==15425)
						$RazonSocialPersona=14177;
					elseif($RazonSocialPersona==14275)
						$RazonSocialPersona=14825;
					elseif($RazonSocialPersona==15318)
						$RazonSocialPersona=14538;
					elseif($RazonSocialPersona==14351)
						$RazonSocialPersona=15186;
					elseif($RazonSocialPersona==14212||$RazonSocialPersona==14211)
						$RazonSocialPersona=15102;
					elseif($RazonSocialPersona==14237)
						$RazonSocialPersona=11312;
					elseif($RazonSocialPersona==14234)
						$RazonSocialPersona=14233;
					elseif($RazonSocialPersona==14501||$RazonSocialPersona==14500)
						$RazonSocialPersona=15292;
					elseif($RazonSocialPersona==13979)
						$RazonSocialPersona=15446;
					elseif($RazonSocialPersona==14698||$RazonSocialPersona==14697)
						$RazonSocialPersona=14999;
					elseif($RazonSocialPersona==28999)
						$RazonSocialPersona=15348;
					elseif($RazonSocialPersona==37454||$RazonSocialPersona==14296)
						$RazonSocialPersona=31361;
					elseif($RazonSocialPersona==14780)
						$RazonSocialPersona=15499;
					elseif($RazonSocialPersona==11471)
						$RazonSocialPersona=14797;
					elseif($RazonSocialPersona==18145)
						$RazonSocialPersona=14906;
					elseif($RazonSocialPersona==30802)
						$RazonSocialPersona=15285;
					elseif($RazonSocialPersona==14778)
						$RazonSocialPersona=12228;
					elseif($RazonSocialPersona==18874)
						$RazonSocialPersona=14709;
					elseif($RazonSocialPersona==14525)
						$RazonSocialPersona=15305;
					elseif($RazonSocialPersona==14298)
						$RazonSocialPersona=13337;
					elseif($RazonSocialPersona==14223)
						$RazonSocialPersona=15109;
					elseif($RazonSocialPersona==14392)
						$RazonSocialPersona=14867;
					elseif($RazonSocialPersona==14339||$RazonSocialPersona==9585)
						$RazonSocialPersona=43020;
					elseif($RazonSocialPersona==22202)
						$RazonSocialPersona=15261;
					elseif($RazonSocialPersona==14456)
						$RazonSocialPersona=15257;
					elseif($RazonSocialPersona==14721||$RazonSocialPersona==14592)
						$RazonSocialPersona=15350;
					elseif($RazonSocialPersona==14516)
						$RazonSocialPersona=15588;
					elseif($RazonSocialPersona==13979)
						$RazonSocialPersona=15446;
					elseif($RazonSocialPersona==14492)
						$RazonSocialPersona=15278;
					elseif($RazonSocialPersona==14865||$RazonSocialPersona==14565||$RazonSocialPersona==2156171)
						$RazonSocialPersona=14564;
					elseif($RazonSocialPersona==14569||$RazonSocialPersona==14568)
						$RazonSocialPersona=15331;
					elseif($RazonSocialPersona==14478)
						$RazonSocialPersona=15267;
					elseif($RazonSocialPersona==21700||$RazonSocialPersona==15239)
						$RazonSocialPersona=14476;
					elseif($RazonSocialPersona==15516||$RazonSocialPersona==14717)
						$RazonSocialPersona=15456;
					elseif($RazonSocialPersona==14251)
						$RazonSocialPersona=15125;
					elseif($RazonSocialPersona==16162)
						$RazonSocialPersona=43011;
					elseif($RazonSocialPersona==14462)
						$RazonSocialPersona=15289;
					elseif($RazonSocialPersona==14506)
						$RazonSocialPersona=15295;
					elseif($RazonSocialPersona==14650||$RazonSocialPersona==14649)
						$RazonSocialPersona=14982;
					elseif($RazonSocialPersona==36426)
						$RazonSocialPersona=14519;
					elseif($RazonSocialPersona==14517)
						$RazonSocialPersona=15302;
					elseif($RazonSocialPersona==14265||$RazonSocialPersona==14264)
						$RazonSocialPersona=14823;
					elseif($RazonSocialPersona==30788)
						$RazonSocialPersona=14499;
					elseif($RazonSocialPersona==36645||$RazonSocialPersona==10889)
						$RazonSocialPersona=14257;
					elseif($RazonSocialPersona==11438)
						$RazonSocialPersona=36646;
					elseif($RazonSocialPersona==14600||$RazonSocialPersona==14195)
						$RazonSocialPersona=15359;
					elseif($RazonSocialPersona==14599)
						$RazonSocialPersona=15357;
					elseif($RazonSocialPersona==14320||$RazonSocialPersona==14319)
						$RazonSocialPersona=15161;
					elseif($RazonSocialPersona==14461)
						$RazonSocialPersona=15262;
					elseif($RazonSocialPersona==14514)
						$RazonSocialPersona=15299;
					elseif($RazonSocialPersona==14513)
						$RazonSocialPersona=15301;
					elseif($RazonSocialPersona==16790)
						$RazonSocialPersona=15140;
					elseif($RazonSocialPersona==14286)
						$RazonSocialPersona=24125;
					elseif($RazonSocialPersona==12009)
						$RazonSocialPersona=15505;
					elseif($RazonSocialPersona==14587)
						$RazonSocialPersona=15358;
					elseif($RazonSocialPersona==14508||$RazonSocialPersona==14507)
						$RazonSocialPersona=15297;
					elseif($RazonSocialPersona==29910)
						$RazonSocialPersona=14824;
					elseif($RazonSocialPersona==14270)
						$RazonSocialPersona=15132;
					elseif($RazonSocialPersona==14274)
						$RazonSocialPersona=15133;
					elseif($RazonSocialPersona==15364||$RazonSocialPersona==14603)
						$RazonSocialPersona=14602;
					elseif($RazonSocialPersona==29050)
						$RazonSocialPersona=14953;
					elseif($RazonSocialPersona==14460)
						$RazonSocialPersona=15260;
					elseif($RazonSocialPersona==14204)
						$RazonSocialPersona=15279;
					elseif($RazonSocialPersona==15167)
						$RazonSocialPersona=37351;
					elseif($RazonSocialPersona==15137)
						$RazonSocialPersona=14829;
					elseif($RazonSocialPersona==22124)
						$RazonSocialPersona=11318;
					elseif($RazonSocialPersona==28984||$RazonSocialPersona==15108)
						$RazonSocialPersona=39660;
					elseif($RazonSocialPersona==15145||$RazonSocialPersona==14290||$RazonSocialPersona==11251)
						$RazonSocialPersona=25806;
					elseif($RazonSocialPersona==15423||$RazonSocialPersona==14681)
						$RazonSocialPersona=11073;
					elseif($RazonSocialPersona==30789)
						$RazonSocialPersona=15503;
					elseif($RazonSocialPersona==14707)
						$RazonSocialPersona=15444;
					elseif($RazonSocialPersona==14591)
						$RazonSocialPersona=15349;
					elseif($RazonSocialPersona==2156171||$RazonSocialPersona==14565)
						$RazonSocialPersona=14865;
					elseif($RazonSocialPersona==15610||$RazonSocialPersona==15344||$RazonSocialPersona==14316)
						$RazonSocialPersona=16256;
					elseif($RazonSocialPersona==14217)
						$RazonSocialPersona=14801;
					elseif($RazonSocialPersona==17698)
						$RazonSocialPersona=10607;
					elseif($RazonSocialPersona==14915)
						$RazonSocialPersona=37470;
					elseif($RazonSocialPersona==9960)
						$RazonSocialPersona=15405;
					elseif($RazonSocialPersona==28714)
						$RazonSocialPersona=15611;
					elseif($RazonSocialPersona==14740)
						$RazonSocialPersona=15471;
					elseif($RazonSocialPersona==28998||$RazonSocialPersona==14920)
						$RazonSocialPersona=36650;
					elseif($RazonSocialPersona==14216||$RazonSocialPersona==15105)
						$RazonSocialPersona=31213;
					elseif($RazonSocialPersona==14396)
						$RazonSocialPersona=11057;
					elseif($RazonSocialPersona==14457)
						$RazonSocialPersona=15258;
					elseif($RazonSocialPersona==15480)
						$RazonSocialPersona=14754;
					elseif($RazonSocialPersona==14252)
						$RazonSocialPersona=14253;
					elseif($RazonSocialPersona==14337||$RazonSocialPersona==9694)
						$RazonSocialPersona=14847;
					elseif($RazonSocialPersona==14496)
						$RazonSocialPersona=14888;
					elseif($RazonSocialPersona==9537)
						$RazonSocialPersona=15103;
					elseif($RazonSocialPersona==37064)
						$RazonSocialPersona=42920;
					elseif($RazonSocialPersona==14345)
						$RazonSocialPersona=15180;
					elseif($RazonSocialPersona==14430)
						$RazonSocialPersona=41015;
					elseif($RazonSocialPersona==15104||$RazonSocialPersona==14215)
						$RazonSocialPersona=29568;
					elseif($RazonSocialPersona==14554)
						$RazonSocialPersona=14934;
					elseif($RazonSocialPersona==14227)
						$RazonSocialPersona=15111;
					elseif($RazonSocialPersona==26283)
						$RazonSocialPersona=15475;
					elseif($RazonSocialPersona==18367)
						$RazonSocialPersona=15009;
					elseif($RazonSocialPersona==11297)
						$RazonSocialPersona=15394;
					elseif($RazonSocialPersona==14632)
						$RazonSocialPersona=15395;
					elseif($RazonSocialPersona==22149||$RazonSocialPersona==14518)
						$RazonSocialPersona=34324;
					elseif($RazonSocialPersona==14814||$RazonSocialPersona==14248)
						$RazonSocialPersona=14249;
					elseif($RazonSocialPersona==25258||$RazonSocialPersona==15310)
						$RazonSocialPersona=14477;
					elseif($RazonSocialPersona==14394)
						$RazonSocialPersona=14869;
					elseif($RazonSocialPersona==22359||$RazonSocialPersona==10566)
						$RazonSocialPersona=14205;
					elseif($RazonSocialPersona==14261)
						$RazonSocialPersona=15130;
					elseif($RazonSocialPersona==14479)
						$RazonSocialPersona=14896;
					elseif($RazonSocialPersona==29472)
						$RazonSocialPersona=14837;
					elseif($RazonSocialPersona==14238)
						$RazonSocialPersona=15117;
					elseif($RazonSocialPersona==2158409)
						$RazonSocialPersona=2159274;
					elseif($RazonSocialPersona==14619)
						$RazonSocialPersona=15385;
					elseif($RazonSocialPersona==15374)
						$RazonSocialPersona=14610;
					elseif($RazonSocialPersona==14484||$RazonSocialPersona==14483)
						$RazonSocialPersona=15273;
					elseif($RazonSocialPersona==14524||$RazonSocialPersona==14713||$RazonSocialPersona==15007||$RazonSocialPersona==42981)
						$RazonSocialPersona=10508;
					elseif($RazonSocialPersona==14403)
						$RazonSocialPersona=15226;
					elseif($RazonSocialPersona==29000)
						$RazonSocialPersona=14919;
					elseif($RazonSocialPersona==15583)
						$RazonSocialPersona=15284;
					elseif($RazonSocialPersona==32208||$RazonSocialPersona==15689)
						$RazonSocialPersona=15043;
					elseif($RazonSocialPersona==10378)
						$RazonSocialPersona=14011;
					elseif($RazonSocialPersona==14258||$RazonSocialPersona==15341)
						$RazonSocialPersona=43003;
					elseif($RazonSocialPersona==36670||$RazonSocialPersona==9883)
						$RazonSocialPersona=36671;
					elseif($RazonSocialPersona==14397)
						$RazonSocialPersona=15221;
					elseif($RazonSocialPersona==14960||$RazonSocialPersona==14620)
						$RazonSocialPersona=14617;
					elseif($RazonSocialPersona==14872)
						$RazonSocialPersona=2164810;
					elseif($RazonSocialPersona==2163740)
						$RazonSocialPersona=41884;
					elseif($RazonSocialPersona==19994)
						$RazonSocialPersona=11121;
					elseif($RazonSocialPersona==43022||$RazonSocialPersona==13528)
						$RazonSocialPersona=14880;
					elseif($RazonSocialPersona==15438)
						$RazonSocialPersona=18621;
					elseif($RazonSocialPersona==14468)
						$RazonSocialPersona=15187;
					elseif($RazonSocialPersona==21921)
						$RazonSocialPersona=2158099;
					elseif($RazonSocialPersona==14662)
						$RazonSocialPersona=2161821;
							
				$longitudNombreAcreedor=strlen($nombrePersona);
				if($longitudNombreAcreedor>20)
					$respuestaTrama.=substr($nombrePersona,0,20);
				elseif($longitudNombreAcreedor==20)
					$respuestaTrama.=$nombrePersona;
				elseif($longitudNombreAcreedor<20){
					$faltaLong=20-$longitudNombreAcreedor;
					$respuestaTrama.=$nombrePersona.str_repeat(" ",$faltaLong);
				}
				
				
				if($punteroBD>0){
				$sql_st2 = "SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),RUC,
								   ID_CONSULTA_IMPORTE_BANCO,DETALLE
							FROM CONSULTA_IMPORTES_BANCO
							WHERE 
							RUC=RTRIM('$datoConsulta')
							AND ESTADO=0
							AND ID_CONSULTA_IMPORTE_BANCO>$punteroBD
							AND MONTO<>'0.00'
							ORDER BY ID_CONSULTA_IMPORTE_BANCO
							";
				}else{
				$sql_st2 = "SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),RUC,
								   ID_CONSULTA_IMPORTE_BANCO,DETALLE
							FROM CONSULTA_IMPORTES_BANCO
							WHERE 
							RUC=RTRIM('$datoConsulta')
							AND ESTADO=0
							AND MONTO<>'0.00'
							ORDER BY ID_CONSULTA_IMPORTE_BANCO
							";
				}
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}
				
				/*Si hay m�s de 10 registros se debe consultar el �ltimo registro para la paginaci�n*/
				if($num_registros2>10){
					//SE consulta el �ltimo registro
					$punteroRespuestaBD=$arreglo2[9][7];//Se consulta el ID_CONSULTA_IMPORTE_BANCO
				}
				/**/
				
				
				$respuestaTrama.="01";//N�mero de Servicios devueltos //Es 1 ya que solo es el servicio del Derecho de Pesca
				$respuestaTrama.="            ";//N�mero de operaci�n de cobranza
				if($num_registros2>10){
					$respuestaTrama.="1";
				}else{
					$respuestaTrama.="0";//??????????? Indicador si hay m�s documentos: "0" No hay documentos, "1" Si hay documentos
				}
				$respuestaTrama.="1000 ";//Tama�o m�ximo de Bloque  varchar(5)
				$respuestaTrama.="   ";//Posici�n del �ltimo documento
				
				if($num_registros2>10){
					
					$longitudPunteroRespuestaBD=strlen($punteroRespuestaBD);
					if($longitudPunteroRespuestaBD==10){
						$respuestaTrama.=$punteroRespuestaBD;
					}elseif($longitudPunteroRespuestaBD<10){
						
						$faltaLongPunteroBD=10-$longitudPunteroRespuestaBD;
						//echo "x".$longitudPunteroRespuestaBD."x";
						$respuestaTrama.=$punteroRespuestaBD.str_repeat(" ",$faltaLongPunteroBD);
					}
					
				}else{
				$respuestaTrama.="          ";//Puntero de la base de datos
				}
				$respuestaTrama.=" ";//Origen de Respuesta
				
				if($error1==1){
				$respuestaTrama.="001";
				}elseif($error2==1){
				$respuestaTrama.="002";
				}else{
				/*Indicar 000 si la respuesta aplicativa es correcta, de lo contrario indicar seg�n los valores que ustedes vayan a definir 001-Cliente sin deuda 002 cliente no existe 003 , etc.*/
				$respuestaTrama.="000";//Si no hay error, se pinta como 00, o se deja vac�p
				}
				//$respuestaTrama.="   ";//C�digo de respuesta, c�digo de errores de aplicativo
				$respuestaTrama.="          ";//Filler
				
				//Detalle de respuesta
				$respuestaTrama.="001";//codigo servicio varchar(3)//001:Derecho de Pesca
				$respuestaTrama.="604";//Moneda Soles
				$respuestaTrama.="V ";//v: vigente, "m moroso" "s con serv. susp"
				$respuestaTrama.="                                        ";//Mensaje 1 al Deudor varchar(40)
				$respuestaTrama.="                                        ";//Mensaje 2 al Deudor varchar(40)
				$respuestaTrama.="1";// indidcador de cronolog�a: Cualquier fecha "1" Pagos cronol�gicos "0"
				$respuestaTrama.="1";//Indicado de pPagos Vencidos: "0": Todos "1" Vigentes "2" Vencidos
				$respuestaTrama.="0";//Restricci�n de pago, 0: Permite, 1: no permite
				
				//Cantidad de documentos por servicio
				//Se debe consultar cuantas deudas posee el ruc
				
				/*$sql_st2 = "SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),RUC
							FROM CONSULTA_IMPORTES_BANCO
							WHERE 
							RUC=RTRIM('$datoConsulta')
							AND ESTADO=0";
										
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}*/
				if($num_registros2==0 || !$num_registros2)
					$respuestaTrama.="00";
				elseif($num_registros2<10)
					$respuestaTrama.="0".$num_registros2;
				elseif($num_registros2<100){
					$num_registros2=10;
					$respuestaTrama.=$num_registros2;
				}
				
				$respuestaTrama.="     ";//Filler varchar(5)
				
				//Detalle de Servicio
				
				
				for($d=0;$d<$num_registros2;$d++){
					//if()
					$tipoServicio="001";//Derecho de Pesca//Tipo de Servicio
					$respuestaTrama.=$tipoServicio;
					$mesPagar=($arreglo2[$d][4]<10) ? "0".$arreglo2[$d][4] : $arreglo2[$d][4];
					
					//Commit
					if($mesPagar=="00"){
						$nroDocumentoServicio=$arreglo2[$d][8];
					}else{
					$nroDocumentoServicio=$arreglo2[$d][3]."-".$mesPagar."-".$arreglo2[$d][2];//Nro documento
					}
					
					$longitudNroDocumento=strlen($nroDocumentoServicio);
					if($longitudNroDocumento==16)
						$respuestaTrama.=$nroDocumentoServicio;
					elseif($longitudNroDocumento<16){
						$faltaLong2=16-$longitudNroDocumento;
						$respuestaTrama.=$nroDocumentoServicio.str_repeat(" ",$faltaLong2);
					}elseif($longitudNroDocumento>16){
						$respuestaTrama.=substr($nroDocumentoServicio,0,16);
					}
					
					$referenciaDocumento=$arreglo2[$d][6];//Referencia del documento
					$longitudReferencia=strlen($referenciaDocumento);
					if($longitudReferencia==16)
						$respuestaTrama.=$referenciaDocumento;
					elseif($longitudReferencia<16){
						$faltaLong3=16-$longitudReferencia;
						$respuestaTrama.=$referenciaDocumento.str_repeat(" ",$faltaLong3);
					}elseif($longitudReferencia>16){
						$respuestaTrama.=substr($referenciaDocumento,0,16);
					}
					
					$respuestaTrama.="20151231";//Fecha de Vencimiento
					
					$importe=$arreglo2[$d][5];//Referencia del documento
					$longitudImporte=strlen($importe);
					if($longitudImporte==11)
						$importePagarFormateado=$importe;
					elseif($longitudImporte<11){
						$faltaLong4=11-$longitudImporte;
						//$importePagarFormateado=$importe.str_repeat(" ",$faltaLong4);
						$importePagarFormateado=str_repeat("0",$faltaLong4).$importe;
					}
					$respuestaTrama.=$importePagarFormateado;//Importe M�nimo a pagar del documento
					$respuestaTrama.=$importePagarFormateado;//Importe del documento
				}
	
	}//fin del consulta de deudas
	
	if($codigoProceso=="945000" && $datosReservados=="00220" && $secondaryBitMap=="0000000000000018"){
	//Pago de documento
					$longitudTrama=substr($trama,196,3);
					$codigoFormato=substr($trama,199,2);
					$binProcesador=substr($trama,201,11);
					$codigoAcreedor=substr($trama,212,11);
					$codigoProductoServicio=substr($trama,223,8);
					$codigoPlazaRecaudador=substr($trama,231,4);
					$codigoAgenciaRecaudador=substr($trama,235,4);
					$tipoDatoPago=substr($trama,239,2);//se ha cambiadoooooooooooooooooooooooo
					$datoPago=substr($trama,241,21);//se ha cambiadooooooooooooooo
					$codigoCiudad=substr($trama,262,3);
					$numeroProdServPagado=substr($trama,265,2);
					$numeroTotalDocPagado=substr($trama,267,3);
					$filler=substr($trama,270,10);
					$medioPago=substr($trama,280,2);//"00"-Efect "01"-Chq "02"-TC "03"-T.Deb "04"-Tar Virtual "05"-Cargo.Cta "06" Ef+C.Cta "07" Ef+Chq "10" Otro
					$importePagadoEfectivo=substr($trama,282,11);
					if($medioPago=="00"){
						//Se ha realizado un pago en efectivo
						//Se debe registrar en la tabla importe_pagado
					}
					//De igual manera se debe registrar los pagos sin son por cheque, tarjeta Debito, Credito, etc
					
					$importePagoCCuenta=substr($trama,293,11);
					$nroCheque1=substr($trama,304,15);
					$bancoGirador1=substr($trama,319,3);
					$importeCheque1=substr($trama,322,11);
					$plazaCheque1=substr($trama,333,1);
					$nroCheque2=substr($trama,334,15);
					$bancoGirador2=substr($trama,349,3);
					$importeCheque2=substr($trama,352,11);
					$plazaCheque2=substr($trama,363,1);	
					$nroCheque3=substr($trama,364,15);
					$bancoGirador3=substr($trama,379,3);
					$importeCheque3=substr($trama,382,11);
					$plazaCheque3=substr($trama,393,1);
					$monedaPago=substr($trama,394,3);
					$tipoCambioAplicado=substr($trama,397,11);
					$pagoTotalRealizado=substr($trama,408,11);//////
					//Representa el pago total realizado.
					
					$filler_=substr($trama,419,10);
					$codigoServicioPagado=substr($trama,429,3);
					$estadoDeudor=substr($trama,432,2);
					$importeTotalxProductoxServicio=substr($trama,434,11);//Igual al total pagado
					$nroCuentaAbono=substr($trama,445,19);
					$nroReferenciaAbono=substr($trama,464,12);
					$nroDocumentosPagados=substr($trama,476,2);
					$filler__=substr($trama,478,10);
					$tipoDocumentoPago=substr($trama,488,3);
					$nroDocumentoPago=substr($trama,491,16);//Se debe identificar el idEmbarcacion
					
					$periodoCotizacion=substr($trama,507,6);
					$tipoDocIdDeudor=substr($trama,513,2);
					$nroDocIdDeudor=substr($trama,515,15);
					$importeOriginalDeuda=substr($trama,530,11);///
					$importePagadoDoc=substr($trama,541,11);///
					$codigoConcepto1=substr($trama,552,2);
					$importeConcepto1=substr($trama,554,11);
					$codigoConcepto2=substr($trama,565,2);
					$importeConcepto2=substr($trama,567,11);
					$codigoConcepto3=substr($trama,578,2);
					$importeConcepto3=substr($trama,580,11);
					$codigoConcepto4=substr($trama,591,2);
					$importeConcepto4=substr($trama,593,11);
					$codigoConcepto5=substr($trama,604,2);
					$importeConcepto5=substr($trama,606,11);
					$referenciaDeuda=substr($trama,617,16);
					$filler___=substr($trama,633,34);
					
					
					//First of all se guarda la trama enviada por el banco
					$sql_stConsulta = "insert into TRAMAS_RECIBIDAS_BANCO(ID_TIPO_TRAMA,TRAMA,AUDITMOD,HOST)
								VALUES (2,'$trama',getDate(),'$nombre_host')
								";
											
								if(!mssql_query($sql_stConsulta,$con)){
									echo "error en la consulta";
								}
					
					//Se consulta si es que el RUC posee embarcaciones con deuda, sino posee, se env�an los mensajes de error
					//Nombre del cliente
					$sql_st = "SELECT CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
											WHEN ID_TIPO_PERSONA=2 THEN RAZON_SOCIAL END,
									  ID
								FROM DB_GENERAL.DBO.PERSONA 
								WHERE NRO_DOCUMENTO=RTRIM('$datoPago')
								";
											
								if(!mssql_query($sql_st,$con)){
									echo "error en la consulta";
								}else{
									$resultado=mssql_query($sql_st,$con);
									$num_campos=mssql_num_fields($resultado);
									$num_registros=mssql_num_rows($resultado);
									$j=$num_registros-1;
									$jj=-1;
										while($datos=mssql_fetch_array($resultado)){
											$nombrePersona=$datos[0];
											$RazonSocialPersona=$datos[1];
										}
								}
								
					if(!$nombrePersona||$nombrePersona==""){
						$error1=1;//El Ruc no es un cliente de Produce
					}
					
					
					//Con el $nroDocumentoPago (Numero de factura) y $datoPago(Ruc de la empresa) se va a identificar el pago
					
					//Commit
					$sql_st2="SELECT *
							FROM CONSULTA_IMPORTES_BANCO
							WHERE convert(varchar,ANYO)+'-'+case when MES<10 THEN '0'+CONVERT(VARCHAR,MES) else CONVERT(VARCHAR,MES) end+'-'+CONVERT(VARCHAR,ID_EMB)=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=0
							AND MONTO<>'0.00'
							
							/*Commit*/
							AND ANYO<>'0'
							UNION
							
							SELECT *
							FROM CONSULTA_IMPORTES_BANCO
							WHERE DETALLE=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=0
							AND MONTO<>'0.00'
							AND ANYO=0 AND MES=0
								";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}					
					
					if($num_registros2==0){
						//El Ruc no tiene deudas en Produce
						$error2=1;
					}
					
					if($num_registros2==1){
						//Hay resultados
						$idConsultaImporteBanco=$arreglo2[0][9];
						$rucEmpresaEmpresa=$arreglo2[0][0];
						$idEmbParaInsertar=$arreglo2[0][2];
						$anyoParaInsertar=$arreglo2[0][3];
						$mesParaInsertar=$arreglo2[0][4];
						
						//Commit
						if($anyoParaInsertar!="0"){
						//Primeramente se registra el pago en la tabla Importe_pagado
						//Se actualiza el importe pagado, ya que no puede haber pagos parciales, es un pago exacto.
						
						$sql_st2="UPDATE CONSULTA_IMPORTES_BANCO
							SET ESTADO=1 WHERE ID_CONSULTA_IMPORTE_BANCO=$idConsultaImporteBanco";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la ACTUALIZACI�N";
							}
						
						$importeParaInsertar=substr($importeTotalxProductoxServicio,0,9).".".substr($importeTotalxProductoxServicio,9,2);
						
						$sql_SP = sprintf("EXECUTE sp_ins_IMPORTE_PAGADO %d,%d,%d,%d,'%s','%s',%2f,%d,'%s'",
										  1,//Consideramos que todos son importes regulares
										  $anyoParaInsertar,
										  $mesParaInsertar,
										  "2",//000-0093203	Scotiabank - (S/.)
										  $nroDocumentoPago,//Se asumir� que es el recibo
										  date('d/m/Y'),//Fecha de pago: se paga en el d�a
										  $importeParaInsertar,
										  $idEmbParaInsertar,
										  'BANCO'
										  );
						if(!mssql_query($sql_SP,$con)){
								echo "error en la INSERCI�N EN LA TABLA IMPORTE_PAGADO";
							}
						}else{
							//Ejecuci�n del Ws para consulta a Commit
								require_once('nusoap/lib/nusoap.php');
								$wsdl='http://bridgesunatservice.CONVENIO_SITRADOC.gob.pe/ServicioTransaccionesTupa/PagoTupaService.asmx?WSDL';
			
								$aParametros = array("ploter" => $trama);
								$client = new soapclient($wsdl, true);
				
								$client->setCredentials("produccion\auraadmin","auraadmin","basic");
				
								$result = $client->call("SetTransactionScotiabank",$aParametros);
								$respuestaTrama=$result["SetTransactionScotiabankResult"];
								//var_dump($result);
								//echo "<br/>XXResultadoCodigo:".$result["SetTransactionScotiabankResult"]."XX";
							//Fin de ejecucu�n del Ws
							
						}//fin del if($anyoParaInsertar!="0")
					}
					
					//Commit
					if($anyoParaInsertar!="0"){
					//Se va a generar la trama de respuesta:
					$respuestaTrama="0210";
					$respuestaTrama.="F22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.=$codigoProceso;
					$respuestaTrama.=$monto;
					$respuestaTrama.=$fechaHoraTransaccion;
					$respuestaTrama.=$trace;
					$respuestaTrama.=$fechaCaptura;
					$respuestaTrama.=$binAdquiriente;//En este campo menciona IDENTIFICACION DE EMPRESA, ES EL BIN ADQUIRIENTE?? O ES EL RUC ED LA EMPRESA????
					$respuestaTrama.=$retrievalReferenceNumber;
					$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
					if($error1==1||$error2==1){
						$respuestaTrama.="21";
					}else{
						$respuestaTrama.="00";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
					}
					$respuestaTrama.=$terminalId;
					$respuestaTrama.=$transactionCurrencyCode;
					$respuestaTrama.=$datosReservados;
					
					$respuestaTrama.="557";
					$respuestaTrama.="01";
					$respuestaTrama.=$binProcesador;//"           ";//Bin propcesador???? varchar(11) Centro de Procesamiento de Cobranzas
					$respuestaTrama.=$codigoAcreedor;//"           ";//Bin acreedor???? varchar(11) ??? es el c�digo de acreedor???
					$respuestaTrama.="        ";//codigo producto/servicio???? varchar(8)
					$respuestaTrama.="0000";//C�digo plaza recaudador varchar(4)
					$respuestaTrama.=$codigoAgenciaRecaudador;//"    ";//C�digo agencia recaudador varchar(4)
					$respuestaTrama.=$tipoDatoPago;//"  ";//tipo dato de pago 01 por servicio 02 por recibo????
					$respuestaTrama.=$datoPago;//"                     ";//dato de pago, cliente recibo varchar(21)
					$respuestaTrama.=$codigoCiudad;//"   ";//codigo de ciudad varchar(3)
					$respuestaTrama.="            ";//N�mero operaci�n cobranza varchar(12)
					$respuestaTrama.="            ";//N�mero operaci�n acreedor varchar(12)
					$respuestaTrama.="01";//N�mero prod/serv pagado varchar(2)
					$respuestaTrama.="001";//N�mero total doc pagado varchar(3)
					$respuestaTrama.="          ";//filler varchar(10)
					$respuestaTrama.=" ";//origen de respuesta varchar(1) ??? //"0" Proc OLC "1" Serv.OLC "2" Serv Remoto "3" Sist.CxCob. "4" Autoriz. Financiero
					//$respuestaTrama.="   ";//codigo de respuesta extendido varchar(3)
					if($error1==1){
						$respuestaTrama.="001";//codigo de respuesta extendido varchar(3)
					}elseif($error2==1){
						$respuestaTrama.="002";//codigo de respuesta extendido varchar(3)
					}else{
						$respuestaTrama.="000";//Si no hay error, //codigo de respuesta extendido varchar(3)
					}
					
					
					$respuestaTrama.="                              ";//descripci�n de respuesta aplicativo varchar(30)
					
					///////////////////////////////////////////////////////////
					$longitudNombreAcreedor=strlen($nombrePersona);
					if($longitudNombreAcreedor>20)
						$respuestaTrama.=substr($nombrePersona,0,20);
					elseif($longitudNombreAcreedor==20)
						$respuestaTrama.=$nombrePersona;
					elseif($longitudNombreAcreedor<20){
						$faltaLong=20-$longitudNombreAcreedor;
						$respuestaTrama.=$nombrePersona.str_repeat(" ",$faltaLong);//nombre del deudor - nombre del cliente varchar(20)
					}
					
					
					$respuestaTrama.="               ";//ruc del deudor varchar(15)//Usado en caso se Facture
					//$respuestaTrama.=$rucEmpresaEmpresa."    ";//varchar(15)
					$respuestaTrama.="               ";//ruc del acreedor varchar(15)//Usado en caso se Facture
					//$respuestaTrama.="20504794637    ";//ruc del acreedor varchar(15)
					$respuestaTrama.="      ";//codigo zona del deudor varchar(6)//Usado en caso se Facture
					$respuestaTrama.="                    ";//filler varchar(20)
					
					$respuestaTrama.="   ";//codigo del prod/serv varchar(3)
					$respuestaTrama.="               ";//descripcion del prod/serv varchar(15)
					$respuestaTrama.="           ";//importe total por prod/serv varchar(11)
					$respuestaTrama.="                                        ";//mensaje1 para impresi�n varchar(40)
					$respuestaTrama.="                                        ";//mensaje2 para impresi�n varchar(40)
					$respuestaTrama.="01";//n�mero de documentos varchar(2) se indicaar� 1??? o 01 o " 1" o "1 "
					$respuestaTrama.="                    ";//filler varchar(20)
					
					$respuestaTrama.="   ";//tipo de servicio varchar(3)
					$respuestaTrama.="               ";//descripcion del documento varchar(15)
					//$respuestaTrama.="                ";//n�mero del documento varchar(16)�Se indica Documento cancelado?
					$respuestaTrama.=$nroDocumentoPago;
					
					$respuestaTrama.="      ";//Periodo de cotizacion Uso futuro
					$respuestaTrama.="  ";//Tipo DocIndentidad Uso futuro
					$respuestaTrama.="               ";//Nro documento Identidad Uso futuro
					$respuestaTrama.="        ";//Fecha de Emision del documento varchar(8)
					//$respuestaTrama.="        ";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.="20151231";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.="           ";//Importe pagado varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto1 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto1 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto2 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto2 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto3 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto3 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto4 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto4 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto5 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto5 varchar(11)
					
					$respuestaTrama.=" ";//Flag si se factura o No. varchar(1)
					$respuestaTrama.="           ";//Numero de factura varchar(11)
					
					$respuestaTrama.=$referenciaDeuda;//El mismo que el del requerimiento
					
					}//fin del if($anyoParaInsertar!="0")
					
	}
	
	if($codigoProceso=="965000" /*&& $secondaryBitMap=="0000004000000018"*/ && $messageTypeIdentification=="0200"){
	//Anulaci�n de pagos
			unset($datosReservados);//Se mata la variable ya que para la anulaci�n tomar� otra posici�n
			//DE(90) - ORIGINAL DATA ELEMENTS
			$messageTypeIdentification2=substr($trama,191,4);
			$trace2=substr($trama,195,6);
			$fechaHoraTransaccion2=substr($trama,201,10);
			$binAdquiriente2=substr($trama,211,11);
			$fwdInstitutionCode2=substr($trama,222,11);
			
			$datosReservados=substr($trama,233,5);
			
			$longitudDato=substr($trama,238,3);
			$codigoFormato=substr($trama,241,2);
			$binProcesador=substr($trama,243,11);
			$codigoAcreedor=substr($trama,254,11);
			$codigoProductoServicio=substr($trama,265,8);
			$codigoPlazaRecaudador=substr($trama,273,4);
			$codigoAgenciaRecaudador=substr($trama,277,4);
			
			$tipoDatoPago=substr($trama,281,2);
			$datoPago=substr($trama,283,21);
			$codigoCiudad=substr($trama,304,3);
			$filler=substr($trama,307,12);
			
			//Recibo a anular
			$tipoServicio=substr($trama,319,3);
			$nroDocumentoPago=substr($trama,322,16);
			$disponible=substr($trama,338,31);
			
			$nroTransCobOri=substr($trama,369,12);
			$nroOpeOrigAcree=substr($trama,381,12);
			
			//First of all se guarda la trama enviada por el banco
					$sql_stConsulta = "insert into TRAMAS_RECIBIDAS_BANCO(ID_TIPO_TRAMA,TRAMA,AUDITMOD,HOST)
								VALUES (3,'$trama',getDate(),'$nombre_host')
								";
											
								if(!mssql_query($sql_stConsulta,$con)){
									echo "error en la consulta";
								}
								
					//Se consulta si es que el RUC posee embarcaciones con deuda, sino posee, se env�an los mensajes de error
					//Nombre del cliente
					$sql_st = "SELECT CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
											WHEN ID_TIPO_PERSONA=2 THEN RAZON_SOCIAL END,
									  ID
								FROM DB_GENERAL.DBO.PERSONA 
								WHERE NRO_DOCUMENTO=RTRIM('$datoPago')
								";
											
								if(!mssql_query($sql_st,$con)){
									echo "error en la consulta";
								}else{
									$resultado=mssql_query($sql_st,$con);
									$num_campos=mssql_num_fields($resultado);
									$num_registros=mssql_num_rows($resultado);
									$j=$num_registros-1;
									$jj=-1;
										while($datos=mssql_fetch_array($resultado)){
											$nombrePersona=$datos[0];
											$RazonSocialPersona=$datos[1];
										}
								}
								
					if(!$nombrePersona||$nombrePersona==""){
						$error1=1;//El Ruc no es un cliente de Produce
					}
					
					/*
					//Con el $nroDocumentoPago (Numero de factura) y $datoPago(Ruc de la empresa) se va a identificar el pago
					$sql_st2="SELECT *
							FROM CONSULTA_IMPORTES_BANCO
							WHERE convert(varchar,ANYO)+'-'+case when MES<10 THEN '0'+CONVERT(VARCHAR,MES) else CONVERT(VARCHAR,MES) end+'-'+CONVERT(VARCHAR,ID_EMB)=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=0	";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}					
					
					if($num_registros2==0){
						//El Ruc no tiene deudas en Produce
						$error2=1;
					}
					*/
					
					//Con el $nroDocumentoPago (Numero de factura) y $datoPago(Ruc de la empresa) se va a identificar el pago
					$sql_st2="SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),DETALLE,ESTADO,FECHA_TRANSACCION,ID_CONSULTA_IMPORTE_BANCO
							FROM CONSULTA_IMPORTES_BANCO
							WHERE convert(varchar,ANYO)+'-'+case when MES<10 THEN '0'+CONVERT(VARCHAR,MES) else CONVERT(VARCHAR,MES) end+'-'+CONVERT(VARCHAR,ID_EMB)=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=1 
							AND MONTO<>'0.00'
							ORDER BY 10 DESc	";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}					
					
					if($num_registros2==0){
						//El Ruc no tiene deudas en Produce
						$error2=1;
					}
					
					//$respuestaTrama=$sql_st2."-".$arreglo2[0][9];
					
					if($num_registros2>=1){
						//Hay resultados
						$idConsultaImporteBanco=$arreglo2[0][9];
						$rucEmpresaEmpresa=$arreglo2[0][0];
						//Primeramente se registra el pago en la tabla Importe_pagado
						//Se actualiza el importe pagado, vuelve a ACTIVARSE LA DEUDA
						
						$sql_st2="UPDATE CONSULTA_IMPORTES_BANCO
							SET ESTADO=0 WHERE ID_CONSULTA_IMPORTE_BANCO=$idConsultaImporteBanco";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la ACTIVACI�N DE LA DEUDA";
							}
							
						//Se anula el registro de pago de la tama importe_pagado
						
						//ESto ser�a 100% correcto si no hubiese otro pago el mismo d�a para esta embarcaci�n con este mes y en este ejercicio
						$sql_SP = "DELETE FROM IMPORTE_PAGADO WHERE CONVERT(VARCHAR,AUDIT_CREACION,103)='".date('d/m/Y')."' AND RTRIM(NUMREC_IP)=RTRIM('$nroDocumentoPago')";
						if(!mssql_query($sql_SP,$con)){
								echo "error en la ELIMINACI�N EN LA TABLA IMPORTE_PAGADO";
							}
					}
					
					//Se va a generar la trama de respuesta:
					$respuestaTrama="0210";
					$respuestaTrama.="F22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.=$codigoProceso;
					$respuestaTrama.=$monto;
					$respuestaTrama.=$fechaHoraTransaccion;
					$respuestaTrama.=$trace;
					$respuestaTrama.=$fechaCaptura;
					$respuestaTrama.=$binAdquiriente;//En este campo menciona IDENTIFICACION DE EMPRESA, ES EL BIN ADQUIRIENTE?? O ES EL RUC ED LA EMPRESA????
					$respuestaTrama.=$retrievalReferenceNumber;
					$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
					if($error1==1||$error2==1){
						$respuestaTrama.="21";
					}else{
						$respuestaTrama.="00";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
					}
					$respuestaTrama.=$terminalId;
					$respuestaTrama.=$transactionCurrencyCode;
					$respuestaTrama.=$datosReservados;
					
					$respuestaTrama.="206";
					$respuestaTrama.="01";//Identifica Version de Trama a usar - Valor : "01"
					$respuestaTrama.=$binProcesador;
					$respuestaTrama.=$codigoAcreedor;
					$respuestaTrama.=$codigoProductoServicio;
					$respuestaTrama.=$codigoPlazaRecaudador;
					$respuestaTrama.=$codigoAgenciaRecaudador;
					$respuestaTrama.=$tipoDatoPago;
					$respuestaTrama.=$datoPago;
					
					$respuestaTrama.=$codigoCiudad;
					///////////////////////////////////////////////////////////
					$longitudNombreAcreedor=strlen($nombrePersona);
					if($longitudNombreAcreedor>20)
						$respuestaTrama.=substr($nombrePersona,0,20);
					elseif($longitudNombreAcreedor==20)
						$respuestaTrama.=$nombrePersona;
					elseif($longitudNombreAcreedor<20){
						$faltaLong=20-$longitudNombreAcreedor;
						$respuestaTrama.=$nombrePersona.str_repeat(" ",$faltaLong);//nombre del deudor - nombre del cliente varchar(20)
					}
					
					$respuestaTrama.="               ";// - RUC DEL DEUDOR: EN EL CASO DE PAGOS SE CONSIDERABA EN CASO FACTURE
					$respuestaTrama.="               ";// - RUC DEL ACREEDOR: EN EL CASO DE PAGOS SE CONSIDERABA EN CASO FACTURE
					
					if(strlen($nroTransCobOri)==12){
						$respuestaTrama.=$nroTransCobOri;
					}else{
						$faltaLong_X=12-strlen($nroTransCobOri);
						$respuestaTrama.=$nroTransCobOri.str_repeat(" ",$faltaLong_X);
					}
					if(strlen($nroOpeOrigAcree)==12){
						$respuestaTrama.=$nroOpeOrigAcree;
					}else{
						$faltaLong_Y=12-strlen($nroOpeOrigAcree);
						$respuestaTrama.=$nroOpeOrigAcree.str_repeat(" ",$faltaLong_Y);
					}
					$respuestaTrama.="                              ";//- FILLER VARCHAR(30)
					
					$respuestaTrama.=" ";//  - ORIGEN DE RESPUESTA : Identifica quien origino la respuesta  en caso exista error// "0"ProcOLC "1"ServOLC "2"Serv.Remoto "3"Sist.CxC "4" Au
					
					if($error1==1){
						$respuestaTrama.="001";//codigo de respuesta extendido varchar(3)
					}elseif($error2==1){
						$respuestaTrama.="002";//codigo de respuesta extendido varchar(3)
					}else{
						$respuestaTrama.="000";//Si no hay error, //codigo de respuesta extendido varchar(3)
					}
					
					$respuestaTrama.="                              ";//descripci�n de respuesta aplicativo varchar(30)
					
					$respuestaTrama.="001";// CODIGO DE PRODUCTO/SERVICIO
					$respuestaTrama.="               ";//DESCRIPC DEL PROD/SERVICIO
					
					$importe=$arreglo2[0][5];//iMPORTE A ANULAR
					$longitudImporte=strlen($importe);
					if($longitudImporte==11)
						$importePagarFormateado=$importe;
					elseif($longitudImporte<11){
						$faltaLong4=11-$longitudImporte;
						//$importePagarFormateado=$importe.str_repeat(" ",$faltaLong4);
						$importePagarFormateado=str_repeat("0",$faltaLong4).$importe;
					}
					$respuestaTrama.=$importePagarFormateado;//Importe a extornar
					
					$respuestaTrama.="                                        ";//Mensaje 1// VARCHAR(40)
					$respuestaTrama.="                                        ";//Mensaje 2// VARCHAR(40)
					
					$respuestaTrama.="01";//Se indicar� 1
					$respuestaTrama.="                    ";//filler//varchar(20)
					$respuestaTrama.="001";//Codigo de Servicio
					$respuestaTrama.="               ";//referencia del documento// varchar(15)
					
					
					$longitudNroDocPago=strlen($nroDocumentoPago);
					if($longitudNroDocPago==16){
						$respuestaTrama.=$nroDocumentoPago;
					}else{
						$faltaLongZ=16-$longitudNroDocPago;
						$respuestaTrama.=$nroDocumentoPago.str_repeat(" ",$faltaLongZ);
					}
					
					$respuestaTrama.="      ";//Periodo de cotizacion Uso futuro
					$respuestaTrama.="  ";//Tipo DocIndentidad Uso futuro
					$respuestaTrama.="               ";//Nro documento Identidad Uso futuro
					$respuestaTrama.="        ";//Fecha de Emision del documento varchar(8)
					$respuestaTrama.="20151231";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.=$importePagarFormateado;//IMPORTE ANULADO DEL DCTO.//Valor de la deuda
					
					$respuestaTrama.="  ";//Codigo de concepto1 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto1 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto2 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto2 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto3 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto3 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto4 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto4 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto5 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto5 varchar(11)
					
					$respuestaTrama.=" ";//"0" Emite Coprobante  "1" No Emite comprobante
					$respuestaTrama.="           ";//Numero de factura varchar(11)
					
					$respuestaTrama.="                ";//REferencia de la deuda// varchar(16)
					$respuestaTrama.="                                  "; //varchat(34)//filler
					
	}				
	
	
	/*
	$terminalId=substr($trama,125,8);
	$comercio=substr($trama,133,15);
	$cardAcceptorLocation=substr($trama,148,40);
	$transactionCurrencyCode=substr($trama,188,3);
	$datosReservados=substr($trama,191,5);
	*/
	if($codigoProceso=="945000" && $secondaryBitMap=="0000004000000018"){
	//Extorno de pago
			$responseCode=substr($trama,125,2);
			$terminalId=substr($trama,127,8);
			$comercio=substr($trama,135,15);
			$cardAcceptorLocation=substr($trama,150,40);
			$transactionCurrencyCode=substr($trama,190,3);
			
			unset($datosReservados);//Se mata la variable ya que para el extorno tomar� otra posici�n
			//DE(90) - ORIGINAL DATA ELEMENTS
			$messageTypeIdentification2=substr($trama,193,4);
			$trace2=substr($trama,197,6);
			$fechaHoraTransaccion2=substr($trama,203,10);
			$binAdquiriente2=substr($trama,213,11);
			$fwdInstitutionCode2=substr($trama,224,11);
			
			$datosReservados=substr($trama,235,5);
			
					$longitudTrama=substr($trama,240,3);
					$codigoFormato=substr($trama,243,2);
					$binProcesador=substr($trama,245,11);
					$codigoAcreedor=substr($trama,256,11);
					$codigoProductoServicio=substr($trama,267,8);
					$codigoPlazaRecaudador=substr($trama,275,4);
					$codigoAgenciaRecaudador=substr($trama,279,4);
					$tipoDatoPago=substr($trama,283,2);//
					$datoPago=substr($trama,285,21);//
					$codigoCiudad=substr($trama,306,3);
					$numeroProdServPagado=substr($trama,309,2);
					$numeroTotalDocPagado=substr($trama,311,3);
					$filler=substr($trama,314,10);
					$medioPago=substr($trama,324,2);//"00"-Efect "01"-Chq "02"-TC "03"-T.Deb "04"-Tar Virtual "05"-Cargo.Cta "06" Ef+C.Cta "07" Ef+Chq "10" Otro
					$importePagadoEfectivo=substr($trama,326,11);
					if($medioPago=="00"){
						//Se ha realizado un pago en efectivo
						//Se debe registrar en la tabla importe_pagado
					}
					//De igual manera se debe registrar los pagos sin son por cheque, tarjeta Debito, Credito, etc
					
					$importePagoCCuenta=substr($trama,337,11);
					$nroCheque1=substr($trama,348,15);
					$bancoGirador1=substr($trama,363,3);
					$importeCheque1=substr($trama,366,11);
					$plazaCheque1=substr($trama,377,1);
					$nroCheque2=substr($trama,378,15);
					$bancoGirador2=substr($trama,393,3);
					$importeCheque2=substr($trama,396,11);
					$plazaCheque2=substr($trama,407,1);	
					$nroCheque3=substr($trama,408,15);
					$bancoGirador3=substr($trama,423,3);
					$importeCheque3=substr($trama,426,11);
					$plazaCheque3=substr($trama,437,1);
					$monedaPago=substr($trama,438,3);
					$tipoCambioAplicado=substr($trama,441,11);
					$pagoTotalRealizado=substr($trama,452,11);//////
					//Representa el pago total realizado.
					
					$filler_=substr($trama,463,10);
					$codigoServicioPagado=substr($trama,473,3);
					$estadoDeudor=substr($trama,476,2);
					$importeTotalxProductoxServicio=substr($trama,478,11);//Igual al total pagado
					$nroCuentaAbono=substr($trama,489,19);
					$nroReferenciaAbono=substr($trama,508,12);
					$nroDocumentosPagados=substr($trama,520,2);///////////
					$filler__=substr($trama,522,10);
					$tipoDocumentoPago=substr($trama,532,3);
					$nroDocumentoPago=substr($trama,535,16);//Se debe identificar el idEmbarcacion
					
					$periodoCotizacion=substr($trama,551,6);
					$tipoDocIdDeudor=substr($trama,557,2);
					$nroDocIdDeudor=substr($trama,559,15);
					$importeOriginalDeuda=substr($trama,574,11);///
					$importePagadoDoc=substr($trama,585,11);///
					$codigoConcepto1=substr($trama,596,2);
					$importeConcepto1=substr($trama,598,11);
					$codigoConcepto2=substr($trama,609,2);
					$importeConcepto2=substr($trama,611,11);
					$codigoConcepto3=substr($trama,622,2);
					$importeConcepto3=substr($trama,624,11);
					$codigoConcepto4=substr($trama,635,2);
					$importeConcepto4=substr($trama,637,11);
					$codigoConcepto5=substr($trama,648,2);
					$importeConcepto5=substr($trama,650,11);
					$referenciaDeuda=substr($trama,661,16);
					$filler___=substr($trama,677,34);
					
					
					
					//First of all se guarda la trama enviada por el banco
					$sql_stConsulta = "insert into TRAMAS_RECIBIDAS_BANCO(ID_TIPO_TRAMA,TRAMA,AUDITMOD,HOST)
								VALUES (4,'$trama',getDate(),'$nombre_host')
								";
											
								if(!mssql_query($sql_stConsulta,$con)){
									echo "error en la consulta";
								}
					
					//Se consulta si es que el RUC posee embarcaciones con deuda, sino posee, se env�an los mensajes de error
					//Nombre del cliente
					$sql_st = "SELECT CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
											WHEN ID_TIPO_PERSONA=2 THEN RAZON_SOCIAL END,
									  ID
								FROM DB_GENERAL.DBO.PERSONA 
								WHERE NRO_DOCUMENTO=RTRIM('$datoPago')
								";
											
								if(!mssql_query($sql_st,$con)){
									echo "error en la consulta";
								}else{
									$resultado=mssql_query($sql_st,$con);
									$num_campos=mssql_num_fields($resultado);
									$num_registros=mssql_num_rows($resultado);
									$j=$num_registros-1;
									$jj=-1;
										while($datos=mssql_fetch_array($resultado)){
											$nombrePersona=$datos[0];
											$RazonSocialPersona=$datos[1];
										}
								}
								
					if(!$nombrePersona||$nombrePersona==""){
						$error1=1;//El Ruc no es un cliente de Produce
					}
					
					
					//Con el $nroDocumentoPago (Numero de factura) y $datoPago(Ruc de la empresa) se va a identificar el pago
					$sql_st2="SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),DETALLE,ESTADO,FECHA_TRANSACCION,ID_CONSULTA_IMPORTE_BANCO
							FROM CONSULTA_IMPORTES_BANCO
							WHERE convert(varchar,ANYO)+'-'+case when MES<10 THEN '0'+CONVERT(VARCHAR,MES) else CONVERT(VARCHAR,MES) end+'-'+CONVERT(VARCHAR,ID_EMB)=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=1 
							AND MONTO<>'0.00'
							ORDER BY 10 DESc	";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}					
					
					if($num_registros2==0){
						//El Ruc no tiene deudas en Produce
						$error2=1;
					}
					
					//$respuestaTrama=$sql_st2."-".$arreglo2[0][9];
					
					if($num_registros2>=1){
						//Hay resultados
						$idConsultaImporteBanco=$arreglo2[0][9];
						$rucEmpresaEmpresa=$arreglo2[0][0];
						//Primeramente se registra el pago en la tabla Importe_pagado
						//Se actualiza el importe pagado, vuelve a ACTIVARSE LA DEUDA
						
						$sql_st2="UPDATE CONSULTA_IMPORTES_BANCO
							SET ESTADO=0 WHERE ID_CONSULTA_IMPORTE_BANCO=$idConsultaImporteBanco";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en el EXTORNO DE PAGO";
							}
						//Se deber�a EXTORNAR el pago efectuado
						//ESto ser�a 100% correcto si no hubiese otro pago el mismo d�a para esta embarcaci�n con este mes y en este ejercicio
						$sql_SP = "DELETE FROM IMPORTE_PAGADO WHERE CONVERT(VARCHAR,AUDIT_CREACION,103)='".date('d/m/Y')."' AND RTRIM(NUMREC_IP)=RTRIM('$nroDocumentoPago')";
						if(!mssql_query($sql_SP,$con)){
								echo "error en la ELIMINACI�N EN LA TABLA IMPORTE_PAGADO";
							}
					}
					//$respuestaTrama=$sql_st2;
					
					//Se va a generar la trama de respuesta:
					$respuestaTrama="0410";
					$respuestaTrama.="B22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.=$codigoProceso;
					$respuestaTrama.=$monto;
					$respuestaTrama.=$fechaHoraTransaccion;
					$respuestaTrama.=$trace;
					$respuestaTrama.=$fechaCaptura;
					$respuestaTrama.=$binAdquiriente;//En este campo menciona IDENTIFICACION DE EMPRESA, ES EL BIN ADQUIRIENTE?? O ES EL RUC ED LA EMPRESA????
					$respuestaTrama.=$retrievalReferenceNumber;
					$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
					if($error1==1||$error2==1){
						$respuestaTrama.="21";
					}else{
						$respuestaTrama.="00";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
					}
					$respuestaTrama.=$terminalId;
					$respuestaTrama.=$transactionCurrencyCode;
					$respuestaTrama.=$datosReservados;
					
					$respuestaTrama.="557";
					$respuestaTrama.="01";//Identifica Version de Trama a usar - Valor : "01"
					$respuestaTrama.=$binProcesador;//"           ";//Bin propcesador???? varchar(11) Centro de Procesamiento de Cobranzas
					$respuestaTrama.=$codigoAcreedor;//"           ";//Bin acreedor???? varchar(11) ??? es el c�digo de acreedor???
					$respuestaTrama.="        ";//codigo producto/servicio???? varchar(8)
					$respuestaTrama.="0000";//C�digo plaza recaudador varchar(4)
					$respuestaTrama.=$codigoAgenciaRecaudador;//"    ";//C�digo agencia recaudador varchar(4)
					$respuestaTrama.=$tipoDatoPago;//"  ";//tipo dato de pago 01 por servicio 02 por recibo????
					$respuestaTrama.=$datoPago;//"                     ";//dato de pago, cliente recibo varchar(21)
					$respuestaTrama.=$codigoCiudad;//"   ";//codigo de ciudad varchar(3)
					$respuestaTrama.="            ";//N�mero operaci�n cobranza varchar(12)
					$respuestaTrama.="            ";//N�mero operaci�n acreedor varchar(12)
					$respuestaTrama.="01";//N�mero prod/serv pagado varchar(2)
					$respuestaTrama.="001";//N�mero total doc pagado varchar(3)
					$respuestaTrama.="          ";//filler varchar(10)
					$respuestaTrama.=" ";//origen de respuesta varchar(1) ??? //"0" Proc OLC "1" Serv.OLC "2" Serv Remoto "3" Sist.CxCob. "4" Autoriz. Financiero
					//$respuestaTrama.="   ";//codigo de respuesta extendido varchar(3)
					if($error1==1){
						$respuestaTrama.="001";//codigo de respuesta extendido varchar(3)
					}elseif($error2==1){
						$respuestaTrama.="002";//codigo de respuesta extendido varchar(3)
					}else{
						$respuestaTrama.="000";//Si no hay error, //codigo de respuesta extendido varchar(3)
					}
					
					
					$respuestaTrama.="                              ";//descripci�n de respuesta aplicativo varchar(30)
					
					///////////////////////////////////////////////////////////
					$longitudNombreAcreedor=strlen($nombrePersona);
					if($longitudNombreAcreedor>20)
						$respuestaTrama.=substr($nombrePersona,0,20);
					elseif($longitudNombreAcreedor==20)
						$respuestaTrama.=$nombrePersona;
					elseif($longitudNombreAcreedor<20){
						$faltaLong=20-$longitudNombreAcreedor;
						$respuestaTrama.=$nombrePersona.str_repeat(" ",$faltaLong);//nombre del deudor - nombre del cliente varchar(20)
					}
					
					
					$respuestaTrama.="               ";//ruc del deudor varchar(15)//Usado en caso se Facture
					//$respuestaTrama.=$rucEmpresaEmpresa."    ";//varchar(15)
					$respuestaTrama.="               ";//ruc del acreedor varchar(15)//Usado en caso se Facture
					//$respuestaTrama.="20504794637    ";//ruc del acreedor varchar(15)
					$respuestaTrama.="      ";//codigo zona del deudor varchar(6)//Usado en caso se Facture
					$respuestaTrama.="                    ";//filler varchar(20)
					
					$respuestaTrama.="   ";//codigo del prod/serv varchar(3)
					$respuestaTrama.="               ";//descripcion del prod/serv varchar(15)
					$respuestaTrama.="           ";//importe total por prod/serv varchar(11)
					$respuestaTrama.="                                        ";//mensaje1 para impresi�n varchar(40)
					$respuestaTrama.="                                        ";//mensaje2 para impresi�n varchar(40)
					$respuestaTrama.="01";//n�mero de documentos varchar(2) se indicaar� 1??? o 01 o " 1" o "1 "
					$respuestaTrama.="                    ";//filler varchar(20)
					
					$respuestaTrama.="   ";//tipo de servicio varchar(3)
					$respuestaTrama.="               ";//descripcion del documento varchar(15)
					//$respuestaTrama.="                ";//n�mero del documento varchar(16)�Se indica Documento cancelado?
					$respuestaTrama.=$nroDocumentoPago;
					
					$respuestaTrama.="      ";//Periodo de cotizacion Uso futuro
					$respuestaTrama.="  ";//Tipo DocIndentidad Uso futuro
					$respuestaTrama.="               ";//Nro documento Identidad Uso futuro
					$respuestaTrama.="        ";//Fecha de Emision del documento varchar(8)
					//$respuestaTrama.="        ";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.="20151231";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.="           ";//Importe pagado varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto1 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto1 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto2 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto2 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto3 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto3 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto4 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto4 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto5 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto5 varchar(11)
					
					$respuestaTrama.=" ";//Flag si se factura o No. varchar(1)
					$respuestaTrama.="           ";//Numero de factura varchar(11)
					
					$respuestaTrama.=$referenciaDeuda;//El mismo que el del requerimiento
					
					
					
	}
	
	if($codigoProceso=="965000" /*&& $secondaryBitMap=="0000000000000018"*/ && $messageTypeIdentification=="0400"){
	//Extorno de anulaci�n de pago
			$responseCode=substr($trama,125,2);
			$terminalId=substr($trama,127,8);
			$comercio=substr($trama,135,15);
			$cardAcceptorLocation=substr($trama,150,40);
			$transactionCurrencyCode=substr($trama,190,3);
			
			unset($datosReservados);//Se mata la variable ya que para el extorno tomar� otra posici�n
			//ORIGINAL DATA ELEMENTS
			$messageTypeIdentification2=substr($trama,193,4);
			$trace2=substr($trama,197,6);
			$fechaHoraTransaccion2=substr($trama,203,10);
			$binAdquiriente2=substr($trama,213,11);
			$fwdInstitutionCode2=substr($trama,224,11);
			
			$datosReservados=substr($trama,235,5);
			
					$longitudTrama=substr($trama,240,3);
					$codigoFormato=substr($trama,243,2);
					$binProcesador=substr($trama,245,11);
					$codigoAcreedor=substr($trama,256,11);
					$codigoProductoServicio=substr($trama,267,8);
					$codigoPlazaRecaudador=substr($trama,275,4);
					$codigoAgenciaRecaudador=substr($trama,279,4);
					$tipoDatoPago=substr($trama,283,2);//
					$datoPago=substr($trama,285,21);//
					$codigoCiudad=substr($trama,306,3);
					$filler=substr($trama,309,12);
					$tipoServicio=substr($trama,321,3);
					$nroDocumentoPago=substr($trama,324,16);
					$fillerDisponible=substr($trama,340,31);
					
					$nroTransCobOri=substr($trama,371,12);
					$nroOpeOrigAcree=substr($trama,383,12);
					
					
					//First of all se guarda la trama enviada por el banco
					$sql_stConsulta = "insert into TRAMAS_RECIBIDAS_BANCO(ID_TIPO_TRAMA,TRAMA,AUDITMOD,HOST)
								VALUES (5,'$trama',getDate(),'$nombre_host')
								";
											
								if(!mssql_query($sql_stConsulta,$con)){
									echo "error en la consulta";
								}
					
					//Se consulta si es que el RUC posee embarcaciones con deuda, sino posee, se env�an los mensajes de error
					//Nombre del cliente
					$sql_st = "SELECT CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
											WHEN ID_TIPO_PERSONA=2 THEN RAZON_SOCIAL END,
									  ID
								FROM DB_GENERAL.DBO.PERSONA 
								WHERE NRO_DOCUMENTO=RTRIM('$datoPago')
								";
											
								if(!mssql_query($sql_st,$con)){
									echo "error en la consulta";
								}else{
									$resultado=mssql_query($sql_st,$con);
									$num_campos=mssql_num_fields($resultado);
									$num_registros=mssql_num_rows($resultado);
									$j=$num_registros-1;
									$jj=-1;
										while($datos=mssql_fetch_array($resultado)){
											$nombrePersona=$datos[0];
											$RazonSocialPersona=$datos[1];
										}
								}
								
					if(!$nombrePersona||$nombrePersona==""){
						$error1=1;//El Ruc no es un cliente de Produce
					}
					
					
					//Con el $nroDocumentoPago (Numero de factura) y $datoPago(Ruc de la empresa) se va a identificar el pago
					$sql_st2="SELECT RUC,ID_PERSONA,ID_EMB,ANYO,MES,REPLACE(replace(MONTO,',',''),'.',''),DETALLE,ESTADO,FECHA_TRANSACCION,ID_CONSULTA_IMPORTE_BANCO,replace(MONTO,',','')
							FROM CONSULTA_IMPORTES_BANCO
							WHERE convert(varchar,ANYO)+'-'+case when MES<10 THEN '0'+CONVERT(VARCHAR,MES) else CONVERT(VARCHAR,MES) end+'-'+CONVERT(VARCHAR,ID_EMB)=RTRIM('$nroDocumentoPago')
							AND RUC=RTRIM('$datoPago')
							AND ESTADO=0 
							AND MONTO<>'0.00'
							ORDER BY 10 DESc	";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en la consulta";
							}else{
								$resultado2=mssql_query($sql_st2,$con);
								$num_campos2=mssql_num_fields($resultado2);
								$num_registros2=mssql_num_rows($resultado2);
								$j2=$num_registros2-1;
								$jj2=-1;
									while($datos2=mssql_fetch_array($resultado2)){
											$jj2++;
											for($i2=0;$i2<$num_campos2;$i2++){
												$arreglo2[$jj2][$i2]=$datos2[$i2];
											}//arreglo final que se entrega pero este caso es para 
									}
							}					
					
					if($num_registros2==0){
						//El Ruc no tiene deudas en Produce
						$error2=1;
					}
					
					//$respuestaTrama=$sql_st2."-".$arreglo2[0][9];
					
					if($num_registros2>=1){
						//Hay resultados
						$idConsultaImporteBanco=$arreglo2[0][9];
						$rucEmpresaEmpresa=$arreglo2[0][0];
						
						$idEmbParaInsertar=$arreglo2[0][2];
						$anyoParaInsertar=$arreglo2[0][3];
						$mesParaInsertar=$arreglo2[0][4];
						
						//Primeramente se registra el pago en la tabla Importe_pagado
						//Se actualiza el importe pagado, vuelve a ACTIVARSE LA DEUDA
						
						$sql_st2="UPDATE CONSULTA_IMPORTES_BANCO
							SET ESTADO=1 WHERE ID_CONSULTA_IMPORTE_BANCO=$idConsultaImporteBanco";
					
							if(!mssql_query($sql_st2,$con)){
								echo "error en el EXTERNO DE PAGO";
							}
							
							
							
						//Se deber�a actualizar el pago efectuado
						$importeParaInsertar=$arreglo2[0][10];;
						$sql_SP = sprintf("EXECUTE sp_ins_IMPORTE_PAGADO %d,%d,%d,%d,'%s','%s',%2f,%d,'%s'",
										  1,//Consideramos que todos son importes regulares
										  $anyoParaInsertar,
										  $mesParaInsertar,
										  "2",//000-0093203	Scotiabank - (S/.)
										  $nroDocumentoPago,//Se asumir� que es el recibo
										  date('d/m/Y'),//Fecha de pago: se paga en el d�a
										  $importeParaInsertar,
										  $idEmbParaInsertar,
										  'BANCO_EXTRNO_ANULACION'
										  );
						if(!mssql_query($sql_SP,$con)){
								echo "error en la INSERCI�N EN LA TABLA IMPORTE_PAGADO";
							}
					}
					//$respuestaTrama=$sql_st2;
					
					//Se va a generar la trama de respuesta:
					$respuestaTrama="0410";
					$respuestaTrama.="B22080010E808000";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.="0000000000000018";//esto siempre se va a responder o va a ser igual al que envi� el banco??
					$respuestaTrama.=$codigoProceso;
					$respuestaTrama.=$monto;
					$respuestaTrama.=$fechaHoraTransaccion;
					$respuestaTrama.=$trace;
					$respuestaTrama.=$fechaCaptura;
					$respuestaTrama.=$binAdquiriente;//En este campo menciona IDENTIFICACION DE EMPRESA, ES EL BIN ADQUIRIENTE?? O ES EL RUC ED LA EMPRESA????
					$respuestaTrama.=$retrievalReferenceNumber;
					$respuestaTrama.="      ";//AuthorizationIdResponse: c�digo de autorizaci�n, qu� es esto????? varchar(6)
					if($error1==1||$error2==1){
						$respuestaTrama.="21";
					}else{
						$respuestaTrama.="00";//ResponseCode: C�digo de respuesta de transacci�n, qu� es esto????? varchar(2)
					}
					$respuestaTrama.=$terminalId;
					$respuestaTrama.=$transactionCurrencyCode;
					$respuestaTrama.=$datosReservados;
					
					
					$respuestaTrama.="548";
					$respuestaTrama.="01";//Identifica Version de Trama a usar - Valor : "01"
					$respuestaTrama.=$binProcesador;//"           ";//Bin propcesador???? varchar(11) Centro de Procesamiento de Cobranzas
					$respuestaTrama.=$codigoAcreedor;//"           ";//Bin acreedor???? varchar(11) ??? es el c�digo de acreedor???
					$respuestaTrama.="        ";//codigo producto/servicio???? varchar(8)
					$respuestaTrama.="0000";//C�digo plaza recaudador varchar(4)
					$respuestaTrama.=$codigoAgenciaRecaudador;//"    ";//C�digo agencia recaudador varchar(4)
					$respuestaTrama.=$tipoDatoPago;//"  ";//tipo dato de pago 01 por servicio 02 por recibo????
					$respuestaTrama.=$datoPago;//"                     ";//dato de pago, cliente recibo varchar(21)
					$respuestaTrama.=$codigoCiudad;//"   ";//codigo de ciudad varchar(3)
					
					
					$longitudNombreAcreedor=strlen($nombrePersona);
					if($longitudNombreAcreedor>20)
						$respuestaTrama.=substr($nombrePersona,0,20);
					elseif($longitudNombreAcreedor==20)
						$respuestaTrama.=$nombrePersona;
					elseif($longitudNombreAcreedor<20){
						$faltaLong=20-$longitudNombreAcreedor;
						$respuestaTrama.=$nombrePersona.str_repeat(" ",$faltaLong);//nombre del deudor - nombre del cliente varchar(20)
					}
					
					$respuestaTrama.="               ";// - RUC DEL DEUDOR: EN EL CASO DE PAGOS SE CONSIDERABA EN CASO FACTURE
					$respuestaTrama.="               ";// - RUC DEL ACREEDOR: EN EL CASO DE PAGOS SE CONSIDERABA EN CASO FACTURE
					
					if(strlen($nroTransCobOri)==12){
						$respuestaTrama.=$nroTransCobOri;
					}else{
						$faltaLong_X=12-strlen($nroTransCobOri);
						$respuestaTrama.=$nroTransCobOri.str_repeat(" ",$faltaLong_X);
					}
					if(strlen($nroOpeOrigAcree)==12){
						$respuestaTrama.=$nroOpeOrigAcree;
					}else{
						$faltaLong_Y=12-strlen($nroOpeOrigAcree);
						$respuestaTrama.=$nroOpeOrigAcree.str_repeat(" ",$faltaLong_Y);
					}
					$respuestaTrama.="                              ";//- FILLER VARCHAR(30)
					
					$respuestaTrama.=" ";//  - ORIGEN DE RESPUESTA : Identifica quien origino la respuesta  en caso exista error// "0"ProcOLC "1"ServOLC "2"Serv.Remoto "3"Sist.CxC "4" Au
					
					if($error1==1){
						$respuestaTrama.="001";//codigo de respuesta extendido varchar(3)
					}elseif($error2==1){
						$respuestaTrama.="002";//codigo de respuesta extendido varchar(3)
					}else{
						$respuestaTrama.="000";//Si no hay error, //codigo de respuesta extendido varchar(3)
					}
					
					$respuestaTrama.="                              ";//descripci�n de respuesta aplicativo varchar(30)
					
					$respuestaTrama.="001";// CODIGO DE PRODUCTO/SERVICIO
					$respuestaTrama.="               ";//DESCRIPC DEL PROD/SERVICIO
					
					$importe=$arreglo2[0][5];//iMPORTE A ANULAR
					$longitudImporte=strlen($importe);
					if($longitudImporte==11)
						$importePagarFormateado=$importe;
					elseif($longitudImporte<11){
						$faltaLong4=11-$longitudImporte;
						//$importePagarFormateado=$importe.str_repeat(" ",$faltaLong4);
						$importePagarFormateado=str_repeat("0",$faltaLong4).$importe;
					}
					$respuestaTrama.=$importePagarFormateado;//Importe a extornar
					
					$respuestaTrama.="                                        ";//Mensaje 1// VARCHAR(40)
					$respuestaTrama.="                                        ";//Mensaje 2// VARCHAR(40)
					
					$respuestaTrama.="01";//Se indicar� 1
					$respuestaTrama.="                    ";//filler//varchar(20)
					$respuestaTrama.="001";//Codigo de Servicio
					$respuestaTrama.="               ";//referencia del documento// varchar(15)
					
					
					$longitudNroDocPago=strlen($nroDocumentoPago);
					if($longitudNroDocPago==16){
						$respuestaTrama.=$nroDocumentoPago;
					}else{
						$faltaLongZ=16-$longitudNroDocPago;
						$respuestaTrama.=$nroDocumentoPago.str_repeat(" ",$faltaLongZ);
					}
					
					$respuestaTrama.="      ";//Periodo de cotizacion Uso futuro
					$respuestaTrama.="  ";//Tipo DocIndentidad Uso futuro
					$respuestaTrama.="               ";//Nro documento Identidad Uso futuro
					$respuestaTrama.="        ";//Fecha de Emision del documento varchar(8)
					$respuestaTrama.="20151231";//Fecha de Vcto del documento varchar(8)
					$respuestaTrama.=$importePagarFormateado;//IMPORTE ANULADO DEL DCTO.//Valor de la deuda
					
					$respuestaTrama.="  ";//Codigo de concepto1 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto1 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto2 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto2 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto3 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto3 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto4 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto4 varchar(11)
					$respuestaTrama.="  ";//Codigo de concepto5 varchar(2)
					$respuestaTrama.="           ";//Importe de concepto5 varchar(11)
					
					$respuestaTrama.=" ";//"0" Emite Coprobante  "1" No Emite comprobante
					$respuestaTrama.="           ";//Numero de factura varchar(11)
					
					$respuestaTrama.="                ";//REferencia de la deuda// varchar(16)
					$respuestaTrama.="                                  "; //varchat(34)//filler
					
	}
	
	return $respuestaTrama;
}

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
//llamamos al m�todo service de la clase nusoap
$server->service($HTTP_RAW_POST_DATA);
?>