<?
include_once("auth.php");
if (!headers_sent()) {
	begin_session();
	entry_rigth();
}
include_once('modulos.php');
include_once('modulosadmin/claseModulos.inc.php');
$modulos = new Modulos;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet</title>

<link href="/estilos/4/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/JER/JER.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->


<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/home_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/interna_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/menu_01_APS.js"></script>


</head>
<body>



<div class="WEB_MAQUE_Alineacion1">
<div class="WEB_MAQUE_Fondo1">
    	<div class="WEB_MAQUE_f1_c1">
        	<div class="WEB_CONTE_cabecera">
                	<div class="WEB_CONTE_bloqueSuperior1">
                   	  <div class="WEB_CONTE_bloque1 GEN_floatLeft">
                   		<div class="WEB_CONTE_tituloSuperior"><img src="/images/0/0/imagen_titulo_home.gif" alt="" width="500" height="35" /></div>
    <div class="WEB_CONTE_actualizacion">
                            	<p class="WEB_texto3" align="center"></p>
                        </div>
                      </div>
                        <div class="WEB_CONTE_bloque2 GEN_floatLeft">
                          <div class="WEB_CONTE_opcionesSuperior">
                          <ul id="menup">
                          	<li class="nivel1"><a href="/institucional/aplicativos/oad/sitradoc.php"><img src="/img/icono_inicio.gif" /><br />Inicio</a></li>
                            <li><a href="http://www.cunamas.gob.pe/" target="_blank"><img src="/img/icono_portal_MIDIS.jpg" /><br />Portal</a></li>
                            <li><a href="http://correo.cunamas.gob.pe/" target="_blank"><img src="/img/icono_correo.jpg" /><br />Correo</a></li>
                             <li><a href="#"><img src="/img/ayuda.jpg" /><br />Ayuda</a>
                             	
                             </li>
                            <li><a href="/exit.php"><img src="/img/icono_salir.jpg" /><br />Salir</a></li>
                          </ul>
                            <div class="GEN_clearLeft"></div>
                          </div>
                      </div>
                    <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_bloqueSuperior2">
                    	<div class="WEB_CONTE_usuario GEN_floatLeft">
                        	<div class="WEB_CONTE_usuarioParte1 GEN_floatLeft">
                        	  <p class="WEB_texto5">+ Bienvenido: <span class="WEB_texto6"> <?php echo $_SESSION['cod_usuario'] . '@' . $MAIL_DOMAIN ?>.  Hoy es <?php $fecha=time(); echo date("d/m/Y");?></span> </p>
                       	  </div>
                            <div class="WEB_CONTE_usuarioParte2 GEN_floatLeft"></div>
                            <br class="GEN_clearLeft" />
                        </div>
							<div class="WEB_CONTE_usuarioParte3 GEN_floatRight">
						<? $msggrupo=$modulos->mensajesGrupos(); echo $msggrupo; ?>
<?php /*?>						<a href="<? echo "$abs_path/$SUGERENCIA_PAGE";?>"><img src="/img/lb_suge.gif" alt="Enviar sugerencias" width="76" height="17" name="ico_hea1" vspace="0" border="0"></a><?php */?>&nbsp;&nbsp;&nbsp;
							</div>
                        <div class="GEN_clearLeft"></div>
                    </div>                	
                </div>
    	</div>
    </div>
    <div class="WEB_MAQUE_Fondo2">
    	<div class="WEB_MAQUE_f2_c1">
        	<div class="WEB_CONTE_barra">
            	<div class="WEB_CONTE_bloqueBarraIzquierdo"><!--
            	  <a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=Perfil_FormCambiarContrasena">CAMBIAR CONTRASEŅA</a>
            	--></div>
                <div class="WEB_CONTE_bloqueBarraDerecho">
                  
                </div>
                <div class="GEN_clearBoth"></div>
            </div>
        </div>
    </div>
    <div class="WEB_MAQUE_Fondo3">
    	<div class="WEB_MAQUE_f3_c1">