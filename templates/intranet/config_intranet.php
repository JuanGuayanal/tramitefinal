<?
// Define parametros para la Session de Usuario
$MAX_SESSION_TIME = 14400; /* 4 HORAS */
$SESSION_NAME = 'intProd';

// Define parametros de la Configuracion Horaria
$ZONA_HORARIA = "es_PE";
setlocale (LC_TIME, $ZONA_HORARIA);

// Define parametros de la aplicacion
$INDEX_PAGE = "intranet.php";
$LOGIN_PAGE = "index.php";
$EXIT_PAGE = "exit.php";
$EXPIRATION_PAGE = "expiration.php";
$SUGERENCIA_PAGE = "sugerencias.php";
$GRUPOSMAIL_PAGE = "msggrupos.php";

// Define parametros de Envio de Correo
$MAIL_DOMAIN = "cunamas.gob.pe";
$EMAIL_SUGERENCIA = "intranet@$MAIL_DOMAIN";

// Define parametros del Servidor IMAP
$IMAP_HOST = '192.168.64.17';
$IMAP_PORT = 143;

// Define los Servidores de DBs
$serverdb['postgres'] = CJDBMSSQL;
$serverdb['mssql'] = CJDBMSSQL;
$serverdb['mysql'] = 'localhost';

// Define los motores de Base de Datos manejados por la App
$type_db['postgres'] = 'mssql';
$type_db['mssql'] = 'mssql';
$type_db['mysql'] = 'mysql';

// Define los Usuarios para las DBs
$userdb['postgres'][0] = CJDBMSSQLUSER;
$userdb['mssql'][0] = CJDBMSSQLUSER;
$userdb['mysql'][0] = 'root';

// Define los passwords de los Usuarios de las DBs
$passdb['postgres'][0] = CJDBMSSQLPASS;// PASSWORD 
$passdb['mssql'][0] = CJDBMSSQLPASS;
$passdb['mysql'][0] = '05mar2004ja';

// Define las Bases de Datos q contiene la DB0
$db['postgres'][0] = 'intranet';

// Define las Bases de Datos q contiene la DB1
$db['mssql'][0] = 'DB_GENERAL';
$db['mssql'][1] = 'HELPDESK';
$db['mssql'][2] = 'DNA';

// Define las Bases de Datos q contiene la DB2
$db['mysql'][0] = 'minpesdb';

// Define las constantes para las acciones
// de los Formularios de Administracion

define("ACCION_BUSCAR_FALSE",0);
define("ACCION_BUSCAR_TRUE",1);
define("ACCION_VER_FALSE",2);
define("ACCION_VER_TRUE",3);
define("ACCION_INSERTAR_FALSE",4);
define("ACCION_INSERTAR_TRUE",5);
define("ACCION_MODIFICAR_FALSE",6);
define("ACCION_MODIFICAR_TRUE",7);

// Define los meses del A�o por nombres
$mesesNombres = Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "setiembre", "octubre", "noviembre", "diciembre");

// Define los Target posibles para un link
$targetLink = Array("_self","_blank","_parent","_top","window");
?>
