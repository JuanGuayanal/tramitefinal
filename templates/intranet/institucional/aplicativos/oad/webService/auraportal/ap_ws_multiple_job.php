<?php
require_once('nusoap/lib/nusoap.php');

$connect="";
function connect(){
	$hostname_sys = "172.16.247.22";
	$database_sys = "DB_GENERAL";
	$username_sys = "wgarrath";
	$password_sys = "citeproy";
	
	$connect = mssql_connect($hostname_sys, $username_sys, $password_sys);
	if(!$connect){
		$msg = "Error en la conexion hacia la base de datos...<br>";
	}else{
		if(!(mssql_select_db($database_sys,$connect))){
			$msg = "Error al seleccionar la base de datos... <br>"; 
		}else{  
			$msg = "";
		}
	}
}
connect();


$strSQL = "SELECT TOP 1 O.ID_OPERACION, O.ID_TIPO_OPERACION, O.TRAMA, O.ID_ESTADO";
$strSQL = $strSQL .", E.DESCRIPCION_ESTADO"; 
$strSQL = $strSQL .", T.ID_TIPO_OPERACION, T.DESCRIPCION_TIPO_OPERACION, T.RUTA, T.METODO, T.NOMBRE_RESULTADO";
$strSQL = $strSQL .", T.CAMPOS_METODO ";
$strSQL = $strSQL .", DB_GENERAL.wgarrath.Fn_GET_NumErrsxID_OP(O.ID_OPERACION) AS FREC_ERRs ";
$strSQL = $strSQL ." FROM DB_GENERAL.dbo.OPERACION O, DB_GENERAL.dbo.ESTADO E, DB_GENERAL.dbo.TIPO_OPERACION T";
$strSQL = $strSQL ." WHERE O.ID_ESTADO = E.ID_ESTADO";
$strSQL = $strSQL ." AND O.ID_TIPO_OPERACION = T.ID_TIPO_OPERACION";
$strSQL = $strSQL ." AND E.ID_ESTADO IN(1, 2)";
$strSQL = $strSQL ." ORDER BY O.FECHA_EXITO";
$result_SQL = mssql_query($strSQL);
if (mssql_num_rows($result_SQL)){
	$ncnt=0;
	while ($row = @mssql_fetch_array($result_SQL)){
		$ncnt = $ncnt + 1;
		
		$v_ID_OPERACION					= trim($row[0]);
		$v_ID_TIPO_OPERACION			= trim($row[1]);
		$v_TRAMA						= trim($row[2]);
		$v_ID_ESTADO					= trim($row[3]);
		$v_DESCRIPCION_ESTADO			= trim($row[4]);
		$v_ID_TIPO_OPERACION			= trim($row[5]);
		$v_DESCRIPCION_TIPO_OPERACION	= trim($row[6]);
		$v_RUTA							= trim($row[7]);
		$v_METODO						= trim($row[8]);
		$v_NOMBRE_RESULTADO				= trim($row[9]);
		$v_CAMPOS_METODO				= trim($row[10]);
		$v_FREC_ERRs					= trim($row[11]);
				
		//DATOS DE LA TRAMA
		$v_matTRAMA			= explode(";", $v_TRAMA );
		//WS - CON 1 PARÁMETRO
		if ($v_ID_TIPO_OPERACION=='1'){
			$param4_TRAMA	= $v_TRAMA;
		}
		//WS - CON 4 PARÁMETROS
		if ($v_ID_TIPO_OPERACION!='1'){
			$param1			= $v_matTRAMA[0];
			$param2			= $v_matTRAMA[1];
			$param3			= $v_matTRAMA[2];
			$param4_TRAMA	= $v_matTRAMA[3];
		}

		//DATOS DEL CAMPO
		$v_matCAMPOS_METODO = explode(";", $v_CAMPOS_METODO );
			$fld1_idclasem		= $v_matCAMPOS_METODO[0];
			$fld2_idproceso		= $v_matCAMPOS_METODO[1];
			$fld3_lstreferen	= $v_matCAMPOS_METODO[2];
			$fld4_datos			= $v_matCAMPOS_METODO[3];
		
		//EJECUCIÓN DINAMICA DE WS
		$wsdl = "";
		if ($v_FREC_ERRs>=5) {
			$msg = "No se ejecuta el WS... porque es MAYOR a 5 veces";
		}else{
			$msg = "Ejecuta el WS... porque es MENOR a 5 veces";
			
			$wsdl			= $v_RUTA;
			$Datos			= $param4_TRAMA;
			
			unset($aParametros);
			if ($v_ID_TIPO_OPERACION=='1'){
				$aParametros= array('Datos' => $Datos);
			}else{
				$aParametros= array('_0_1_IdClaseM' => $param1,'_0_2_IdProceso' => $param2,'_Lista_de_Referencias' => $param3, 'Datos' => $param4_TRAMA);
			}
			
			$client[$ncnt]	= new soapclient($wsdl, true);
			$client[$ncnt]->setCredentials('produccion\auraadmin','auraadmin','basic');
			
			unset($result_WS);
			$result_WS		= $client[$ncnt]->call($v_METODO, $aParametros) ;
		}
		
		//VERIFICA CONFORMACIÓN DE DATOS DE LOS WEB SERVICES
		if ($client[$ncnt] -> fault) {
			$msg = "No se pudo completar la operación...."; 
		}else{ 
			$error = $client[$ncnt]->getError(); 
			if ($error) { $msg = "Error:' . $error .";  /*exit;*/ }
		}
				
		$i_Result_Aura		= $result_WS[$v_NOMBRE_RESULTADO];
		$strSQL_i = "";
		
		
		//SI SE EJECUTA CON EXITO
		if ($i_Result_Aura>=1) { 
			$v_flg_resultado 		= "2";
			$msg				 	= $i_Result_Aura ."EXITO";
			$v_ws_success			=  "3";
		}else{ 
			$v_flg_resultado 		= "1"; 
			$msg 					= "<font color='red'>ERROR: $i_Result_Aura </font>"; 
			$v_ws_success			=  "2"; 
		} 


		if ($v_FREC_ERRs>5) {
			//ACTUALIZA TABLA DE OPERACION PARA NO CARGAR REPETIDAMENTE LOS WEBSERVICES
			$strsQL_u2 	= "UPDATE DB_GENERAL.dbo.OPERACION SET  ";
			$strsQL_u2	= $strsQL_u2 ." ID_ESTADO = 4, FECHA_EXITO = GETDATE() ";
			$strsQL_u2	= $strsQL_u2 ." WHERE ID_OPERACION = ". $v_ID_OPERACION;
			mssql_query($strsQL_u2);
		}else{
			//ACUSA EN TABLA DE LOGs
			$strSQL_i	= "INSERT INTO DB_GENERAL.dbo.LOG_OPERACION";
			$strSQL_i 	= $strSQL_i ." (ID_OPERACION, FECHA_LOG_OPERACION, ID_RESPUESTA) ";
			$strSQL_i 	= $strSQL_i ." VALUES ('". $v_ID_OPERACION  ."', GETDATE(), ";
			$strSQL_i 	= $strSQL_i ." '". $v_flg_resultado ."') ";
			mssql_query($strSQL_i);
			
			//if (trim($error)!="") { $v_ws_success = "7"; }
			
			//ACTUALIZA TABLA DE OPERACION PARA NO CARGAR REPETIDAMENTE LOS WEBSERVICES
			$strsQL_u 	= "UPDATE DB_GENERAL.dbo.OPERACION SET  ";
			$strsQL_u	= $strsQL_u ." ID_ESTADO = '". $v_ws_success ."', CODIGO_ERROR = '". $i_Result_Aura ."' ";
			$strsQL_u	= $strsQL_u .", FECHA_EXITO = GETDATE() ";
			$strsQL_u	= $strsQL_u ." WHERE ID_OPERACION = ". $v_ID_OPERACION;
			mssql_query($strsQL_u);
		}
	}
}else{
	$msg = "No se encontraron WS para listar...";
}

unset($connect);

//echo "Error: ". $error ."<br>";
echo "Mensaje: ". $msg;

?>