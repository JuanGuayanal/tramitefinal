<?
include_once('modulosadmin/claseIntranet.inc.php');

$objIntranet = new Intranet();

$menu = ($_POST['menu']) ? $_POST['menu'] : $_GET['menu'];
$subMenu = ($_POST['subMenu']) ? $_POST['subMenu'] : $_GET['subMenu'];
$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

include_once('modulosadmin/claseRegistroDatosTrabajador.inc.php');

$modRT = new RegistroDatosTrabajador($menu,$subMenu);

if($modRT){
	if ($accion!=$modRT->arr_accion[AGREGA_TRABAJADOR]&&$accion!=$modRT->arr_accion[MODIFICA_TRABAJADOR]
		&&$accion!=$modRT->arr_accion[AGREGA_DEPENDENCIA]&&$accion!=$modRT->arr_accion[MODIFICA_DEPENDENCIA]
	    ){
		$objIntranet->Header('Registro Datos del Trabajador',false,array('suspEmb'),false,true);
		$objIntranet->Body('helpdesk_tit.gif');
	}
	switch ($accion){
		case $modRT->arr_accion[FRM_BUSCA_TRABAJADOR] :
			$modRT->FormBuscaTrabajador();
			break;
		case $modRT->arr_accion[BUSCA_TRABAJADOR] :
			$modRT->BuscaTrabajador($_POST['page'],$_POST['nombreTrabajador'],$_POST['status'],$_POST['profesion'],$_POST['dependencia'],$_POST['condicion'],$_POST['cargo']);
			break;
		case $modRT->arr_accion[FRM_AGREGA_TRABAJADOR] :
			$modRT->FormAgregaTrabajador($_POST['dependencia'],$_POST['apellidoTrabajador'],$_POST['nombreTrabajador'],$_POST['condicion'],$_POST['email'],
										 $_POST['profesion'],$_POST['dni'],$_POST['telefono'],$_POST['desFechaIni'],$_POST['publi'],$_POST['subDependencia'],
										 $_POST['dependenciaPortal'],$_POST['cargo'],$_POST['publi2'],$_POST['anexo'],$_POST['publi3']
										);
			break;			
		case $modRT->arr_accion[AGREGA_TRABAJADOR] :
			$modRT->AgregaTrabajador($_POST['dependencia'],$_POST['apellidoTrabajador'],$_POST['nombreTrabajador'],$_POST['condicion'],$_POST['email'],
										 $_POST['profesion'],$_POST['dni'],$_POST['telefono'],$_POST['desFechaIni'],$_POST['publi'],$_POST['subDependencia'],
										 $_POST['dependenciaPortal'],$_POST['cargo'],$_POST['publi2'],$_POST['anexo'],$_POST['publi3']);
			break;
			
		case $modRT->arr_accion[FRM_MODIFICA_TRABAJADOR] :
			$modRT->FormModificaTrabajador(($_POST['id']) ? $_POST['id'] : $_GET['id'],
										$_POST['dependencia'],$_POST['apellidoTrabajador'],$_POST['nombreTrabajador'],
										$_POST['condicion'],$_POST['email'], $_POST['profesion'],$_POST['dni'],$_POST['telefono'],
										$_POST['desFechaIni'],$_POST['status'],$_POST['publi'],$_POST['subDependencia'],
										$_POST['dependenciaPortal'],$_POST['cargo'],$_POST['publi2'],$_POST['anexo'],
										$_POST['publi3']);
			break;
		case $modRT->arr_accion[MODIFICA_TRABAJADOR] :
			$modRT->ModificaTrabajador($_POST['id'],
										$_POST['dependencia'],$_POST['apellidoTrabajador'],$_POST['nombreTrabajador'],
										$_POST['condicion'],$_POST['email'], $_POST['profesion'],$_POST['dni'],
										$_POST['telefono'],$_POST['desFechaIni'],$_POST['status'],$_POST['publi'],
										$_POST['subDependencia'], $_POST['dependenciaPortal'],$_POST['cargo'],
										$_POST['publi2'],$_POST['anexo'],$_POST['publi3']);
			break;
		case $modRT->arr_accion[FRM_BUSCA_DEPENDENCIA] :
			$modRT->FormBuscaDependencia();
			break;
		case $modRT->arr_accion[BUSCA_DEPENDENCIA] :
			$modRT->BuscaDependencia($_POST['page'],$_POST['nombreDependencia'],$_POST['status']);
			break;
		case $modRT->arr_accion[FRM_AGREGA_DEPENDENCIA] :
			$modRT->FormAgregaDependencia($_POST['tipDependencia'],$_POST['nombreDependencia'],$_POST['siglasDependencia'],$_POST['condicion'],$_POST['email'],
										 $_POST['seccion'],$_POST['anexo'],$_POST['piso'],$_POST['directo'],$_POST['categoriaDepe']);
			break;
		case $modRT->arr_accion[AGREGA_DEPENDENCIA] :
			$modRT->AgregaDependencia($_POST['tipDependencia'],$_POST['nombreDependencia'],$_POST['siglasDependencia'],$_POST['condicion'],$_POST['email'],
										 $_POST['seccion'],$_POST['anexo'],$_POST['piso'],$_POST['directo'],$_POST['categoriaDepe']);
			break;
		case $modRT->arr_accion[FRM_MODIFICA_DEPENDENCIA] :
			$modRT->FormModificaDependencia(($_POST['id']) ? $_POST['id'] : $_GET['id'],$_POST['tipDependencia'],$_POST['nombreDependencia'],$_POST['siglasDependencia'],$_POST['condicion'],$_POST['email'],
										 $_POST['seccion'],$_POST['anexo'],$_POST['piso'],$_POST['directo'],$_POST['categoriaDepe'],$_POST['reLoad']);
			break;
		case $modRT->arr_accion[MODIFICA_DEPENDENCIA] :
			$modRT->ModificaDependencia($_POST['id'],$_POST['tipDependencia'],$_POST['nombreDependencia'],$_POST['siglasDependencia'],$_POST['condicion'],$_POST['email'],
										 $_POST['seccion'],$_POST['anexo'],$_POST['piso'],$_POST['directo'],$_POST['categoriaDepe']);
			break;			
		case $modRT->arr_accion[BUENA_TRANS]:
			$modRT->MuestraStatTrans($accion);
			break;
		case $modRT->arr_accion[MALA_TRANS]:
			$modRT->MuestraStatTrans($accion);
			break;
		default:
			$modRT->MuestraIndex();
			break;
	}
	if ($accion!=$modRT->arr_accion[AGREGA_TRABAJADOR]&&$accion!=$modRT->arr_accion[MODIFICA_TRABAJADOR]
		&&$accion!=$modRT->arr_accion[AGREGA_DEPENDENCIA]&&$accion!=$modRT->arr_accion[MODIFICA_DEPENDENCIA]
		)
		$objIntranet->Footer();
}else{
	$objIntranet->Header('Help Desk',false,array('suspEmb'));
	$objIntranet->Body('helpdesk_tit.gif');
	$objIntranet->Footer();	
}



?>
