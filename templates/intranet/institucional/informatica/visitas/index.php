<?php

error_reporting(E_ALL);

include_once('intranet/modulosadmin/claseIntranet.inc.php');
$objIntranet = new Intranet();

$accion = ($_POST['accion']) ? $_POST['accion'] : $_GET['accion'];

include_once('intranet/modulosadmin/claseVisitas.inc.php');

$modVisi = & new Visitas;

if($modVisi){
	if ($accion!=$modVisi->arr_accion['IMPRIME_VISITA']){
		$objIntranet->Header('Visitas Produce',false);
		$objIntranet->Body('visitasCONVENIO_SITRADOC_tit.gif',false,false);
	}
	switch ($accion){
		case $modVisi->arr_accion['FRM_BUSCA_VISITA']:
			$modVisi->FormBuscaVisita();
			break;
		case $modVisi->arr_accion['BUSCA_VISITA']:
			$modVisi->BuscaVisita(($_POST['tipo']) ? $_POST['tipo'] : $_GET['tipo'],
									($_POST['mensaje']) ? $_POST['mensaje'] : $_GET['mensaje'],
									($_POST['tipo2']) ? $_POST['tipo2'] : $_GET['tipo2']
									);
			break;
		case $modVisi->arr_accion['IMPRIME_VISITA']:
			$modVisi->ImprimeVisita(($_POST['tipo']) ? $_POST['tipo'] : $_GET['tipo'],
							($_POST['campo_fecha']) ? $_POST['campo_fecha'] : $_GET['campo_fecha'],
									($_POST['campo_fecha2']) ? $_POST['campo_fecha2'] : $_GET['campo_fecha2'],
									($_POST['nombreVisitante']) ? $_POST['nombreVisitante'] : $_GET['nombreVisitante'],
									($_POST['dniVisitante']) ? $_POST['dniVisitante'] : $_GET['dniVisitante'],
									($_POST['funcionario']) ? $_POST['funcionario'] : $_GET['funcionario'],
									($_POST['horInicio']) ? $_POST['horInicio'] : $_GET['horInicio'],
									($_POST['horFin']) ? $_POST['horFin'] : $_GET['horFin'],
									($_POST['tipo2']) ? $_POST['tipo2'] : $_GET['tipo2'],
									($_POST['print']) ? $_POST['print'] : $_GET['print']
									
									
									);
			break;
		case $modVisi->arr_accion['FRM_AGREGA_VISITA']:
			$modVisi->FormAgregaVisitante($_POST['tipo'],$_POST['dniVisitante'],$_POST['nombresVisitante'],$_POST['apellidosVisitante'],$_POST['tipo2'],
								   			$_POST['tipo3'],$_POST['tipo4'],$_POST['obs'],$_POST['cmbProcedencia']);
			break;
		case $modVisi->arr_accion['AGREGA_VISITA']:
			$modVisi->AgregaVisitante($_POST['tipo'],$_POST['dniVisitante'],$_POST['nombresVisitante'],$_POST['apellidosVisitante'],$_POST['tipo2'],
								   			$_POST['tipo3'],$_POST['tipo4'],$_POST['obs']);
			break;
		case $modVisi->arr_accion['REGISTRA_SALIDA']:
			$modVisi->RegistraSalida(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modVisi->arr_accion['REGISTRA_ENTRADA']:
			$modVisi->RegistraEntrada(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;

		case $modVisi->arr_accion['REGISTRA_SALIDA_CONVENIO_SITRADOC']:
			$modVisi->RegistraSalidaProduce(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modVisi->arr_accion['REGISTRA_ENTRADA_CONVENIO_SITRADOC']:
			$modVisi->RegistraEntradaProduce(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modVisi->arr_accion['CREA_CSV_VISITAS']:
			$modVisi->CreaCsvVisitas(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;
		case $modVisi->arr_accion['CANCELA_VISITA']:
			$modVisi->fncCancelavisitaxId(($_POST['id']) ? $_POST['id'] : $_GET['id']);
			break;			
		default:
			$modVisi->MuestraIndex();
			break;
	}
	if ($accion!=$modVisi->arr_accion['IMPRIME_VISITA'])
		$objIntranet->Footer(false);
}

exit;

?>