<?
include_once("auth.php");
begin_session();
entry_rigth();
include_once('modulos.php');
include_once('modulosadmin/claseModulos.inc.php');
$modulos = new Modulos;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - Produce</title>
<link href="/estilos/4/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/JER/JER.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->


<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/home_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/interna_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/menu_01_APS.js"></script>

</head>

<body>
<div class="WEB_MAQUE_Alineacion1">
	<div class="WEB_MAQUE_Fondo1">
    	<div class="WEB_MAQUE_f1_c1">
        	<div class="WEB_CONTE_cabecera">
                	<div class="WEB_CONTE_bloqueSuperior1">
                   	  <div class="WEB_CONTE_bloque1 GEN_floatLeft">
                   		<div class="WEB_CONTE_tituloSuperior"><img src="/images/0/0/imagen_titulo_home.gif" alt="" width="387" height="35" /></div>
                            <div class="WEB_CONTE_actualizacion">
                            	<p class="WEB_texto3">+   Ultima Actualizaci&oacute;n: <span class="WEB_texto4"><? echo ($index_intranet) ? "&nbsp;" : muestraFechaActual(); ?></span>&nbsp;&nbsp;&nbsp;"A�o del Centenario de Machu Picchu para el mundo"</p>
                        </div>
                      </div>
                        <div class="WEB_CONTE_bloque2 GEN_floatLeft">
                          <div class="WEB_CONTE_opcionesSuperior">
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoInicio"><a href="/intranet.php">Inicio</a></div>
                           	    <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoPortal"><a href="http://www.CONVENIO_SITRADOC.gob.pe" target="_blank"> Portal</a></div>
                                <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoCorreo"><a href="https://correoweb.CONVENIO_SITRADOC.gob.pe" target="_blank">Correo</a></div>
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoSalir"><a href="/exit.php">Salir</a></div>
                            <div class="GEN_clearLeft"></div>
                          </div>
                      </div>
                    <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_bloqueSuperior2">
                    	<div class="WEB_CONTE_usuario GEN_floatLeft">
                        	<div class="WEB_CONTE_usuarioParte1 GEN_floatLeft">
                        	  <p class="WEB_texto5">+   Bienvenido: <span class="WEB_texto6"><?php echo $_SESSION['cod_usuario'] . '@' . $MAIL_DOMAIN ?></span></p>
                        	</div>
                            <div class="WEB_CONTE_usuarioParte2 GEN_floatLeft"></div>
                            <br class="GEN_clearLeft" />
                        </div>
							<div class="WEB_CONTE_usuarioParte3 GEN_floatRight">
						<? $msggrupo=$modulos->mensajesGrupos(); echo $msggrupo; ?>
				<?php /*?>		<a href="<?echo "$abs_path/$SUGERENCIA_PAGE";?>"><img src="/img/lb_suge.gif" alt="Enviar sugerencias" width="76" height="17" name="ico_hea1" vspace="0" border="0"></a>&nbsp;&nbsp;&nbsp;<?php */?>
							</div>
                        <!-- <div class="WEB_CONTE_buscador GEN_floatLeft">
                                <input type="text" name="textfield" id="buscador" />
                                <div class="WEB_CONTE_buscadorBoton"></div>
                        </div> --> 
                        <div class="GEN_clearLeft"></div>
                    </div>                	
                </div>
    	</div>
    </div>
    <div class="WEB_MAQUE_Fondo2">
    	<div class="WEB_MAQUE_f2_c1">
        	<div class="WEB_CONTE_barra">
            	<div class="WEB_CONTE_bloqueBarraIzquierdo">
            	  <p class="WEB_texto3">+   Tipo de Cambio: Compra <span class="WEB_texto4"> S/. <? echo $_SESSION['dolarCompra'] ?></span> Venta <span class="WEB_texto4"> S/. <? echo $_SESSION['dolarVenta'] ?></span></p>
            	</div>
                <div class="WEB_CONTE_bloqueBarraDerecho">
                  <p class="WEB_texto3">Valor FOB Harina de Pescado <span class="WEB_texto4">US$ <? echo $_SESSION['fob']; ?></span>   l   Valor UIT: <span class="WEB_texto4">S/. <? echo $_SESSION['uit']; ?></span>   l   Tasa de Inter&eacute;s Legal Efectiva Diaria: <span class="WEB_texto4"><? echo $_SESSION['tasaDiaria']; ?>%</span></p>
                </div>
                <div class="GEN_clearBoth"></div>
            </div>
        </div>
    </div>
    <div class="WEB_MAQUE_Fondo3">
    	<div class="WEB_MAQUE_f3_c1">
		          	<div class="GEN_clearLeft"></div>
    	</div>
    </div>
</div>
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="10" bgcolor="#5E7E95">&nbsp;</td>
    <td width="8" >&nbsp;</td>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="848" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="8" >&nbsp;</td>
    <td width="10" bgcolor="#5E7E95">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#5E7E95">&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#E5E5E5">&nbsp;</td>
    <td bgcolor="#FFFFFF">