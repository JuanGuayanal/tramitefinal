/*
$(document).ready(function (){
	$("div.WEB_CONTE_comboInferior1").hover(ComboEncima1,ComboFuera1);	
	$("div.WEB_CONTE_botonBannerTitulo1").hover(EnlaceEncima1,EnlaceFuera1);
	$("div.WEB_CONTE_botonBannerTitulo2").hover(EnlaceEncima2,EnlaceFuera2);	
	$("div.WEB_CONTE_botonBannerImagen1").hover(ImagenEncima1,ImagenFuera1);
	$("div.WEB_CONTE_botonBannerImagen2").hover(ImagenEncima2,ImagenFuera2);
	$("div.WEB_CONTE_tab").each(TabAncho);
	num_noticias =$("div.NOT_CONTE_bloqueNoticiaHome").length;
	$("div.WEB_CONTE_tab").hover(TabEncima1,TabFuera1);
	$("div.WEB_CONTE_comboInferior1").bind("click",MenuCombo);
	posicion_inicial=$("div.WEB_CONTE_bloqueContenidoNoticias").height();
	posicion_final=-posicion_inicial;
	IniciarScroll();
	TextoBuscador('#buscador', 'Escribe el texto a buscar');
	
	//Scroll Actividades
	IniciarScrollActividades();	
	
	$("div.WEB_CONTE_bannerMensaje").show("slow",Temporizador1());

	$("div.WEB_CONTE_tabInferior1").hover(TabInferiorEncima,TabInferiorFuera);
	$("div.WEB_CONTE_tabInferior2").hover(TabInferiorEncima,TabInferiorFuera);
	$("div.WEB_CONTE_tabInferior3").hover(TabInferiorEncima,TabInferiorFuera);
	$("div.WEB_CONTE_tabInferior4").hover(TabInferiorEncima,TabInferiorFuera);
	$("div[class^=WEB_CONTE_tabInferior]").each(TabInferiorAncho);

});
*/

/* Dimencionar Bloques */

function DimencionarBloque1(id,ancho,alto){
	
	$("div#"+id).css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraArriba").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraMedio").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraAbajo").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_sombraHorizontalArriba").css("width",ancho-11);
	$("div#"+id).find("div.WEB_CONTE_sombraHorizontalAbajo").css("width",ancho-11);
	$("div#"+id).find("div.WEB_CONTE_bloqueCabecera").css("width",ancho-21);
	$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("width",ancho-21);

	if(alto=="auto"){
		
		$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("height",alto);
		$("div#"+id).css("height",alto);
		
		var anchoContenido=($("div#"+id).find("div.WEB_CONTE_grupoSombraMedio").height());
		
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalIzquierdo").css("height",anchoContenido);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalDerecho").css("height",anchoContenido);
		
	}else{
		$("div#"+id).css("height",alto);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalIzquierdo").css("height",alto-11);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalDerecho").css("height",alto-11);
		$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("height",alto-54);
	}

}

function DimencionarBloque2(id,ancho,alto){
	
	$("div#"+id).css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraArriba").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraMedio").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_grupoSombraAbajo").css("width",ancho);
	$("div#"+id).find("div.WEB_CONTE_sombraHorizontalArriba").css("width",ancho-11);
	$("div#"+id).find("div.WEB_CONTE_sombraHorizontalAbajo").css("width",ancho-11);
	$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("width",ancho-21);

	if(alto=="auto"){
		
		$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("height",alto);
		$("div#"+id).css("height",alto);
		
		var anchoContenido=($("div#"+id).find("div.WEB_CONTE_grupoSombraMedio").height());
		
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalIzquierdo").css("height",anchoContenido);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalDerecho").css("height",anchoContenido);
		
	}else{
		$("div#"+id).css("height",alto);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalIzquierdo").css("height",alto-11);
		$("div#"+id).find("div.WEB_CONTE_sombraVerticalDerecho").css("height",alto-11);
		$("div#"+id).find("div.WEB_CONTE_sombraContenido").css("height",alto-22);
	}

}

function ConfigurarTramites(){
	

	var altoContenido=($("div#ID_tramites").height()-89);
	var anchoContenido=($("div#ID_tramites").width()-21);

	$("div.WEB_CONTE_tramiteLista").css("height",altoContenido);
	$("div.WEB_CONTE_tramiteLista").css("width",anchoContenido);
}
	

function ColorearBolsas(){

	$("div.WEB_CONTE_bolsas div.WEB_CONTE_bloqueBolsas:even").css("color","white");
	$("div.WEB_CONTE_bolsas div.WEB_CONTE_bloqueBolsas:odd").css("background","white");
}



/* Tab Botones Inferiores */

var A_tabContenido=["","fondo_tab_contenido1","fondo_tab_contenido2","fondo_tab_contenido3","fondo_tab_contenido4"];

function TabInferiorContenido(){
	var tab_elegido=this.className.substring(22,21);
	for(var i=1;i<5;i++){
		document.getElementById("Tab_Inferior"+i).className="WEB_CONTE_tabInferior"+i;
	}
	this.className="WEB_CONTE_tabInferior"+tab_elegido+"_on";
	
	fondoContenido = "images/"+A_tabContenido[tab_elegido]+".jpg";
	
	//$("div.WEB_CONTE_tabContenido").css("background-image",fondoContenido);
	document.getElementById("tab_contenido").className="WEB_CONTE_tabContenidoFondo"+tab_elegido;
	//alert(A_tabContenido[tab_elegido]);
}


var timerID="";
var duracion=5000;

function Temporizador1(){
	timerID = setTimeout("OcultarMensaje()", duracion);
}

function Temporizador2(){
	timerID = setTimeout("MostrarMensaje()", duracion);
}

function OcultarMensaje() {
	clearTimeout(timerID);
//	$("div.WEB_CONTE_bannerMensaje").hide("slow",Temporizador2());
	$("div.WEB_CONTE_bannerMensaje").hide("slow","");
}

function MostrarMensaje() {
	clearTimeout(timerID);
	$("div.WEB_CONTE_bannerMensaje").show("slow",Temporizador1());
}

/* ******************************************************************* */
/* ******************************************************************* */
/* ******************************************************************* */
/* ******************************************************************* */

/* Noticias Scroll */


var pausa_inicio=6000; 
var velocidad_scroll=1; 
var pausa_scroll=1; 

var velocidad_temp=velocidad_scroll;
var pausa_velocidad=(pausa_scroll==0)? velocidad_temp : 0;
var altura_actual=0;

function ScrollActivado(){
	if (parseInt(id_scroll.style.top)>(altura_actual*(-1)+8)){
		id_scroll.style.top=parseInt(id_scroll.style.top)-velocidad_temp+"px";
	}else{
		id_scroll.style.top=parseInt(id_contenedor)+8+"px";
	}
}

function IniciarScroll(){

	var altoContenido=($("div#ID_noticias").height()-56);
	var anchoContenido=($("div#ID_noticias").width()-21);
	var altoEnlace=($("div.NOT_CONTE_noticiaEnlaceHome").height()+20);

	$("div.WEB_CONTE_scrollNoticias").css("top",10);
	$("div.WEB_CONTE_scrollNoticias").css("height",altoContenido-altoEnlace);
	$("div.WEB_CONTE_scrollNoticias").css("clip","rect("+0+"px,"+anchoContenido+"px,"+(altoContenido-altoEnlace)+"px,"+0+"px)");

	id_scroll=document.getElementById("ID_scrollNoticias",altoContenido);

	$("div.WEB_CONTE_bloqueContenidoNoticias").css("top",0);
	id_contenedor=$("div.WEB_CONTE_scrollNoticias").height();
	altura_actual=$("div.WEB_CONTE_bloqueContenidoNoticias").height();
	setTimeout("id_interval=setInterval('ScrollActivado()',65)", pausa_inicio);
	$("div.WEB_CONTE_scrollNoticias").bind('mouseover',DetenerScroll).bind('mouseout',AvanzarScroll);
}


function DetenerScroll(){
	velocidad_temp=pausa_velocidad;
}

function AvanzarScroll(){
	velocidad_temp=velocidad_scroll;
}

/* Actividades */

var pausa_inicioact=6000; 
var velocidad_scrollact=1; 
var pausa_scrollact=1; 

var velocidad_tempact=velocidad_scrollact;
var pausa_velocidadact=(pausa_scrollact==0)? velocidad_tempact : 0;
var altura_actualact=0;

function ScrollActividades(){
	if (parseInt(id_scrollact.style.top)>(altura_actualact*(-1)+8)){
		id_scrollact.style.top=parseInt(id_scrollact.style.top)-velocidad_tempact+"px";
	}else{
		id_scrollact.style.top=parseInt(id_contenedoract)+8+"px";
	}
}

function IniciarScrollActividades(){	
	id_scrollact=document.getElementById("WEB_ID_actividades");	
	$("div.WEB_CONTE_bloqueContenidoActividades").css("top",0);
	id_contenedoract=$("div.WEB_CONTE_scrollActividades").height();
	altura_actualact=$("div.WEB_CONTE_bloqueContenidoActividades").height();
	setTimeout("id_interval=setInterval('ScrollActividades()',65)", pausa_inicioact);
	$("div.WEB_CONTE_scrollActividades").bind('mouseover',DetenerActividades).bind('mouseout',AvanzarActividades);	
}

function DetenerActividades(){
	velocidad_tempact=pausa_velocidadact;
}

function AvanzarActividades(){
	velocidad_tempact=velocidad_scrollact;
}

 /* ********************************************************************************************* */
 /* ********************************************************************************************* */
 /* ********************************************************************************************* */

/* Buscador Texto por Default */

function TextoBuscador(id_input, valor) {
    if($.trim($(id_input).val()) == "") {
        $(id_input).val(valor);
    }
  
    $(id_input).focus(function() {
        if($(id_input).val() == valor) {
            $(id_input).val("");
        }
    });
    
    $(id_input).blur(function() {
        if($.trim($(id_input).val()) == "") {
            $(id_input).val(valor);
        }
    });
 }

 /* ********************************************************************************************* */
 /* ********************************************************************************************* */
 /* ********************************************************************************************* */


var A_menuCombo=["",0,0,0];
var A_anchoBotones=[];

function TabAncho(i){
	var x=$(this);
	A_anchoBotones[i]=x.width();
}

function MenuCombo(){
	$("div.WEB_CONTE_menuCombo1").hide();
	var combo_id=$(this).children(".WEB_CONTE_menuCombo1").attr("id");
	var combo_elegido=combo_id.substring(7,6);
	$(this).children("div.WEB_CONTE_menuCombo1").slideDown();
	$(this).children("div.WEB_CONTE_menuCombo1").css("left",A_menuCombo[combo_elegido]);
	$(this).children("div.WEB_CONTE_menuCombo1").hover(MenuComboEncima,MenuComboFuera)
}

function MenuComboEncima(){
	$(this).show();	
}

function MenuComboFuera(){
	$(this).hide();
}

function ComboEncima1(){
	$(this).addClass("WEB_CONTE_comboInferior1_on"); 
}

function ComboFuera1(){
	$(this).removeClass("WEB_CONTE_comboInferior1_on");
	$(this).children("div.WEB_CONTE_menuCombo1").hide();
}


function TabEncima1(){
	$(this).children("div.WEB_CONTE_tabParte1").addClass("WEB_CONTE_tabParte1_on");
	$(this).children("div.WEB_CONTE_tabParte2").addClass("WEB_CONTE_tabParte2_on");
	$(this).children("div.WEB_CONTE_menuTab1").fadeIn(400);
	var tab_id=$(this).find("div[id^=Tab]").attr("id");
	var tab_elegido=tab_id.substring(5,4);
	var tab_ancho=$(this).width();
	var tab_posicion=0;
	for(var i=0;i<tab_elegido;i++){
		tab_posicion+=A_anchoBotones[i];
	}
	$(this).children("div.WEB_CONTE_menuTab1").css("left",-(tab_posicion-tab_ancho));
	$("div.WEB_CONTE_tabActivo").css("left",tab_posicion).show();
}

function TabFuera1(){
	$(this).children("div.WEB_CONTE_tabParte1").removeClass("WEB_CONTE_tabParte1_on");
	$(this).children("div.WEB_CONTE_tabParte2").removeClass("WEB_CONTE_tabParte2_on");
	$(this).children("div.WEB_CONTE_menuTab1").hide();
	$("div.WEB_CONTE_tabActivo").hide();
}

function EnlaceEncima1(){
	$(this).addClass("WEB_CONTE_botonBannerTitulo1_on");
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerImagen1").css({"border": "1px solid #ef8608"});
}

function EnlaceFuera1(){
	$(this).removeClass("WEB_CONTE_botonBannerTitulo1_on");
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerImagen1").css({"border": "1px solid white"});
}


function EnlaceEncima2(){
	$(this).addClass("WEB_CONTE_botonBannerTitulo2_on");
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerImagen2").css({"border": "1px solid #ef8608"});
}

function EnlaceFuera2(){
	$(this).removeClass("WEB_CONTE_botonBannerTitulo2_on");
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerImagen2").css({"border": "1px solid white"});
}

function ImagenEncima1(){
	$(this).css({"border": "1px solid #ef8608"});
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerTitulo1").addClass("WEB_CONTE_botonBannerTitulo1_on");
}

function ImagenFuera1(){
	$(this).css({"border": "1px solid white"});
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerTitulo1").removeClass("WEB_CONTE_botonBannerTitulo1_on");
}


function ImagenEncima2(){
	$(this).css({"border": "1px solid #ef8608"});
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerTitulo2").addClass("WEB_CONTE_botonBannerTitulo2_on");
}

function ImagenFuera2(){
	$(this).css({"border": "1px solid white"});
	$(this).parent().parent().children().children("div.WEB_CONTE_botonBannerTitulo2").removeClass("WEB_CONTE_botonBannerTitulo2_on");
}