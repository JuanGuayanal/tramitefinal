
/* Noticias Scroll */

var pausa_inicio=0; 
var velocidad_scroll=1; 
var pausa_scroll=1; 

var velocidad_temp=velocidad_scroll;
var pausa_velocidad=(pausa_scroll==0)? velocidad_temp : 0;
var altura_actual=0;

function ScrollActivado(){
	if (parseInt(id_scroll.style.top)>(altura_actual*(-1)+8)){
		id_scroll.style.top=parseInt(id_scroll.style.top)-velocidad_temp+"px";
	}else{
		id_scroll.style.top=parseInt(id_contenedor)+8+"px";
	}
}

function IniciarScroll(){
	id_scroll=document.getElementById("WEB_ID_noticias");
	$("div.WEB_CONTE_bloqueContenidoNoticias").css("top",200);
	id_contenedor=$("div.WEB_CONTE_scrollNoticias").height();
	altura_actual=$("div.WEB_CONTE_bloqueContenidoNoticias").height();
	setTimeout("id_interval=setInterval('ScrollActivado()',30)", pausa_inicio);
	$("div.WEB_CONTE_bloqueNoticiaHome").bind('mouseover',DetenerScroll).bind('mouseout',AvanzarScroll);
}


function DetenerScroll(){
	velocidad_temp=pausa_velocidad;
}

function AvanzarScroll(){
	velocidad_temp=velocidad_scroll;
}
