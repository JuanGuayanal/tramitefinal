<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - Produce</title>
<link href="/estilos/4/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/JER/JER.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<!--<link href="estilo_prueba/INI.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/WEB.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/GEN.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/NOT.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="estilo_prueba/ZOP/ZOP.css" rel="stylesheet" type="text/css" /> -->


<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/home_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/interna_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/menu_01_APS.js"></script>

</head>

<body>
<div class="WEB_MAQUE_Alineacion1">
	<div class="WEB_MAQUE_Fondo1">
    	<div class="WEB_MAQUE_f1_c1">
        	<div class="WEB_CONTE_cabecera">
                	<div class="WEB_CONTE_bloqueSuperior1">
                   	  <div class="WEB_CONTE_bloque1 GEN_floatLeft">
                   		<div class="WEB_CONTE_tituloSuperior"><img src="/images/0/0/imagen_titulo_home.gif" alt="" width="387" height="22" /></div>
                            <div class="WEB_CONTE_actualizacion">
                            	<p class="WEB_texto3">+   Ultima Actualizaci&oacute;n: <span class="WEB_texto4">05.ABR.2008</span></p>
                        </div>
                      </div>
                        <div class="WEB_CONTE_bloque2 GEN_floatLeft">
                          <div class="WEB_CONTE_opcionesSuperior">
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoMensajeria"><a href="/intranet.php">Inicio</a></div>
                           	    <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoPortal"><a href="http://www.CONVENIO_SITRADOC.gob.pe" target="_blank">Portal Produce</a></div>
                              <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoEscribenos"><a href="https://correoweb.CONVENIO_SITRADOC.gob.pe" target="_blank">Correo Web </a></div>
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoSalir"><a href="/exit.php">Cerrar Sesi&oacute;n</a></div>
                            <div class="GEN_clearLeft"></div>
                          </div>
                      </div>
                    <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_bloqueSuperior2">
                    	<div class="WEB_CONTE_usuario GEN_floatLeft">
                        	<div class="WEB_CONTE_usuarioParte1 GEN_floatLeft">
                        	  <p class="WEB_texto5">+   Bienvenido: <span class="WEB_texto6">Javier G&oacute;mez Sanchez</span></p>
                        	</div>
                            <div class="WEB_CONTE_usuarioParte2 GEN_floatLeft"></div>
                            <br class="GEN_clearLeft" />
                        </div>
                        <!-- <div class="WEB_CONTE_buscador GEN_floatLeft">
                                <input type="text" name="textfield" id="buscador" />
                                <div class="WEB_CONTE_buscadorBoton"></div>
                        </div> -->
                        <div class="GEN_clearLeft"></div>
                    </div>
                	
                </div>
    	</div>
    </div>
    <div class="WEB_MAQUE_Fondo2">
    	<div class="WEB_MAQUE_f2_c1">
        	<div class="WEB_CONTE_barra">
            	<div class="WEB_CONTE_bloqueBarraIzquierdo">
            	  <p class="WEB_texto3">+   Tipo de Cambio: Compra:  <span class="WEB_texto4">S/.2.645</span>  Venta:  <span class="WEB_texto4">S/.2.725</span></p>
            	</div>
                <div class="WEB_CONTE_bloqueBarraDerecho">
                  <p class="WEB_texto3">Valor FOB Harina de Pescado <span class="WEB_texto4">US$ 15.20</span>   l   Valor UIT: <span class="WEB_texto4">S/. 150.00</span>   l   Tasa de Inter&eacute;s diaria: <span class="WEB_texto4">05.56%</span></p>
                </div>
                <div class="GEN_clearBoth"></div>
            </div>
        </div>
    </div>
    <div class="WEB_MAQUE_Fondo3">
    	<div class="WEB_MAQUE_f3_c1">