<?

require_once('modulosadmin/claseAuth.inc.php');

class JScriptForm extends Auth{

	/* ------------------------------------------------------------- */
	/* Funcion para Insertar Java Script Campo Fecha en Formulario	 */
	/* Trabaja conjuntamente con insertaFechaForm()					 */
	/* ------------------------------------------------------------- */
	
	function insertaFechaScript($interval=1){
?>
	<script language="JavaScript">
	function cuantosDias(mes, anyo){
		var cuantosDias = 31;
		if (mes == "04" || mes == "06" || mes == "09" || mes == "11")
			cuantosDias = 30;
		if (mes == "02" && (anyo/4) != Math.floor(anyo/4))
			cuantosDias = 28;
		if (mes == "02" && (anyo/4) == Math.floor(anyo/4))
			cuantosDias = 29;
		return cuantosDias;
	}
	
	function asignaDias(comboDias, comboMeses, comboAnyos){
	
		Month = comboMeses[comboMeses.selectedIndex].value;
		Year = comboAnyos[comboAnyos.selectedIndex].value;
		
		diasEnMes = cuantosDias(Month, Year);
		diasAhora = comboDias.length-1;
		
		if (diasAhora > diasEnMes){
			for (i=0; i<(diasAhora-diasEnMes); i++){
				comboDias.options[comboDias.options.length - 1] = null
			}
		}
		if (diasEnMes > diasAhora){
			for (i=0; i<(diasEnMes-diasAhora); i++){
				var value = Number(comboDias.options[comboDias.options.length-1].value) + 1;
				sumaOpcion = new Option(value,value);
				comboDias.options[comboDias.options.length]=sumaOpcion;
			}
		}
		if (comboDias.selectedIndex < 0)
			comboDias.selectedIndex = 0;
	}
	</script>
<?
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Insertar Java Script Campo Fecha de Papeleta	 */
	/* Trabaja conjuntamente con insertaFechaPapeletaForm()			 */
	/* ------------------------------------------------------------- */
	
	function insertaFechaPapeletaScript($dias){
?>
	<script language="JavaScript">
	var arrDiasPap = new Array();
	var arrMesesPap = new Array();
<?
		while( list($anyo) = each($dias) ){
			echo "\t arrDiasPap[${anyo}] = new Array();\r\n";
			echo "\t arrMesesPap[${anyo}] = new Array();\r\n";
			echo "\t var contMeses=0;\r\n";
			while( list($mes) = each($dias[$anyo]) ) {
				echo "\t arrMesesPap[${anyo}][contMeses] = '".sprintf("%02d",$mes)."'; contMeses++;\r\n";				
				echo "\t arrDiasPap[${anyo}]['".sprintf("%02d",$mes)."'] = new Array();\r\n";
				for($i=0;$i<count($dias[$anyo][$mes]);$i++)
					echo "\t arrDiasPap[${anyo}]['".sprintf("%02d",$mes)."'][${i}] = '".sprintf('%02d',$dias[$anyo][$mes][$i])."';\r\n";
			}
		}
?>

	function asignaMesesPapeleta(comboDias, comboMeses, comboAnyos){
		var Year = comboAnyos[comboAnyos.selectedIndex].value;
		var numMeses = comboMeses.options.length;
		
		for (i=0; i<numMeses-1; i++)
			comboMeses.options[comboMeses.options.length - 1] = null;

		if(!isNaN(Year))
			for (i=0; i<arrMesesPap[Year].length; i++)
				comboMeses.options[comboMeses.options.length] = new Option(arrMesesPap[Year][i],arrMesesPap[Year][i]);

		if (comboMeses.selectedIndex < 0)
			comboMeses.selectedIndex = 0;
	}
	function asignaDiasPapeleta(comboDias, comboMeses, comboAnyos){
		var Month = comboMeses[comboMeses.selectedIndex].value;
		var Year = comboAnyos[comboAnyos.selectedIndex].value;
		var numDias = comboDias.options.length

		for (i=0; i<numDias-1; i++)
			comboDias.options[comboDias.options.length - 1] = null;

		if(!isNaN(Year) && !isNaN(Month))
			for (i=0; i<arrDiasPap[Year][Month].length; i++)
				comboDias.options[comboDias.options.length] = new Option(arrDiasPap[Year][Month][i],arrDiasPap[Year][Month][i]);

		if (comboDias.selectedIndex < 0)
			comboDias.selectedIndex = 0;
	}
	</script>
<?
	}

	/* --------------------------------------------------------------------- */
	/* Funcion para Insertar Java Script Campo Subdependencia en Formulario	 */
	/* Trabaja conjuntamente con insertaDependenciaFrm()			   		 */
	/* --------------------------------------------------------------------- */
	
	function JSsubDep($frmName,$val=NULL,$valDepNone=NULL,$selSubDep='idSubdependencia',$selDep='idDependencia'){
		$html = new Smarty;
		$html->assign('frmName',$frmName);
		$html->assign('val',$val);
		$html->assign('selSubDep',$selSubDep);
		$html->assign('selDep',$selDep);
		$html->assign('valDepNone',$valDepNone);
		return $html->fetch('jsscript/JSsubDep.tpl.php');
	}
	
	function insertaSubdependenciaScript($select,$selectDep,$valSubDep=NULL){
?>
		<script language="JavaScript">
		function muestraSubdependencias(select){
			var arrIndice
			arrIndice = select.options[select.selectedIndex].value
			var itemsSel = <?=$select?>.options.length
			for(var i=1; i<=itemsSel; i++){
				<?=$select?>.options[itemsSel-i]=null
			}
			if(select.options[select.selectedIndex].value != ''){
				for(var i=0; i<dependenciaVal[arrIndice].length; i++){
					<?=$select?>.options[i]=new Option(dependenciaDes[arrIndice][i], dependenciaVal[arrIndice][i])
				}
			}
		}
		muestraSubdependencias(<?=$selectDep;?>);
<?
		if(!is_null($valSubDep)){
?>
		for(var i=0; i<<?=$select?>.options.length; i++){
			if(<?=$select?>.options[i].value == <?=$valSubDep;?>)
				<?=$select?>.options[i].selected=true
		}
<?
		}
?>	
		</script>
<?
	}
	
	/* -------------------------------------------------------------- */
	/* Funcion para Insertar JavaScript Intercambia Items Select Form */
	/* -------------------------------------------------------------- */
	
	function JSItemsChange(){
		$html = new Smarty;
		return $html->fetch('jsscript/JSItemsChange.tpl.php');
	}
	
	function insertaScriptItemsSelectForm($selSrc,$selDest){
?>
	<script language="JavaScript">
	function insertaItemSelectForm(selSrc,selDest){
		var errorUsr = ''
		var i=0
		start1 :
		for(i; i<selSrc.options.length; i++){
			if(selSrc.options[i].selected && selSrc.options[i].value!=''){
				var numItemSelDest = selDest.options.length
				for(var k=0; k<numItemSelDest; k++){
					if(selSrc.options[i].value == selDest.options[k].value){
						errorUsr += '- '+selSrc.options[i].text+'\r\n'
						continue start1
					}
				}
				selDest.options[selDest.options.length]=new Option(selSrc.options[i].text,selSrc.options[i].value,true,true)
			}
		}
		if(errorUsr!='')
			alert('Los siguientes Usuarios\r\nno se agregaron:\r\n'+errorUsr+'Por ser repetidos')
	}
	
	function eliminaItemSelectForm(selDest){
		optionsIns = new Array();
		var k=0;
		for(var i=0; i<selDest.options.length; i++){
			if(selDest.options[i].selected == false && selDest.options[i].value!=''){
				optionsIns[k] = selDest.options[i];
				k++;
			}
		}
		if(optionsIns.length != selDest.options.length-1){
			var numItemsDest = selDest.options.length
			for(var i=1; i<numItemsDest; i++){
				selDest.options[numItemsDest - i] = null
			}
			for(var i=0; i<optionsIns.length; i++)
				selDest.options[i+1] = optionsIns[i]
		}
	}
	
	function seleccionaItemSelectForm(ini,sel){
		for(var i=0; i<ini-1; i++)
			sel.options[i].selected = false
		for(var i=ini-1; i<sel.options.length; i++)
			sel.options[i].selected = true
	}
	</script>
	<input type="button" name="Button" value="Agregar &gt;&gt;" class="but1" onClick="insertaItemSelectForm(<?=$selSrc;?>,<?=$selDest;?>)">
	<br>
	<br>
	<input type="button" name="Submit2" value="&lt;&lt; Eliminar" class="but1" onClick="eliminaItemSelectForm(<?=$selDest;?>)">
<?
	}
	
	/* -------------------------------------------------------------- */
	/* Funcion para Insertar JavaScript Envia Formulario			  */
	/* -------------------------------------------------------------- */
	
	function insertaScriptSubmitForm($frm,$funciones=NULL,$vars=NULL){
		$html = new Smarty;
		$html->assign('vars',$vars);
		$html->assign('frm',$frm);
		$html->assign('funciones',$funciones);
		return $html->fetch('jsscript/submit.tpl.php');
	}

	function insertaScriptTipoBusquedaOnomastico($frmName){
?>
	<script language="JavaScript">
	function tipbuscaOnomastico() {
		var form1 = document.<?=$frmName?>;
		if(form1.nombre.value == ""){
			if (confirm('La Busqueda se realizar� por la Fecha Ingresada\n�Desea Continuar?'))
				return true;
			else
				return false;
		}else{
			if (confirm('La Busqueda se realizar� por la el Nombre ingresado\nSi desea hacer busquedas "Por Fecha", deje el campo "Por Nombre" en blanco\n�Desea Continuar?'))
				return true;
			else
				return false;
		}
	}
	</script>
	<link href="/estilos/calendar-green.css" rel="stylesheet" type="text/css" />	
<link href="/styles/style_intranet.css" rel="stylesheet" type="text/css" />
	<SCRIPT src="estilos/calendar.js" type=text/javascript></SCRIPT>
	<SCRIPT src="estilos/calendar-es.js" 
	type=text/javascript></SCRIPT>
	<SCRIPT src="estilos/calendar-setup.js" 
	type=text/javascript></SCRIPT>
	<script language="JavaScript">
function cargarR(pForm,a)
{
	//var texto=pForm.tipDocumento.value;
	//var texto2=pForm.tipBusqueda.value;
		//pForm.action="index.php?accion=frmSearchNew&menu=SumarioDir&tipDocumento="+texto+"&tipBusqueda"+texto2;
		pForm.action="index.php?tipoEstado="+a;
	    //pForm.bsubmit.disabled=true;
		pForm.submit();
}
</script>		
<?
	}

}
?>