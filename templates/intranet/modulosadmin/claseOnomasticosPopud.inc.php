<?
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class Onomasticos extends Modulos{

	/* ----------------------------------------------------- */
	/* Constructor											 */
	/* ----------------------------------------------------- */
	/* CLASE ONOMASTICOS									 */
	/* ----------------------------------------------------- */
	
	function Onomasticos(){
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans',
							FRM_BUSCA_ONOMASTICO =>'frmSearchOnomastico',
							BUSCA_ONOMASTICO =>'searchOnomastico'
							);		
		
	}

	/* ----------------------------------------------------- */
	/* Funcion para la Muestra de los Cumplea�os de la		 */
	/* Semana Actual										 */
	/* ----------------------------------------------------- */
	/* CLASE ONOMASTICOS									 */
	/* ----------------------------------------------------- */
	
	function MuestraIndex(){
		//$this->FormBuscaOnomastico();
		$this->BuscaOnomastico();
	}	
	
	function FormBuscaOnomastico($page=NULL,$nombre=NULL,$campo_fecha=NULL,$campo_fecha2=NULL,$tipEstado=NULL,$search=false){
	
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscaOnomastico';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nombre',$nombre);
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);
		$html->assign_by_ref('tipEstado',$tipEstado);
		
		$llaveInicial="{";
		$llaveFinal="}";
		$html->assign_by_ref('llaveInicial',$llaveInicial);
		$html->assign_by_ref('llaveFinal',$llaveFinal);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('oad/headerArm.tpl.php');
		$html->display('onomasticos/searchPopud.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	
	function BuscaOnomastico($page,$nombre,$campo_fecha,$campo_fecha2,$tipEstado){
		if(!$tipEstado){
			$tipEstado=2;
			if(!$campo_fecha || $campo_fecha==""){
				$campo_fecha=date('d/m');
			}	
			if(!$campo_fecha2 || $campo_fecha2==""){
				$campo_fecha2=date('d/m');
			}			
		}
		
		
		
		//echo $tipEstado;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscaPersona';
		$html->assign('frmName','frmSearchExtendedPer');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array(									
									'nombre'=>$nombre,
									'campo_fecha'=>$campo_fecha,
									'campo_fecha2'=>$campo_fecha2,
									'tipEstado'=>$tipEstado
									));
		
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaOnomastico($page,$nombre,$campo_fecha,$campo_fecha2,$tipEstado,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
				
		// Condicionales Segun ingreso del Usuario
		/*$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";*/
		
		/*if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";*/
		$condConsulta[] = "ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
		$condConsulta[] = "b.fecha_nacimiento is not null AND B.FECHA_NACIMIENTO<>'29/02/1900'";	
		$condConsulta[] = " convert(int,
																			substring(B.FECHA_NACIMIENTO,7,4)
																		)>1918 ";	
			if(!empty($nombre)&&!is_null($nombre)&&$tipEstado==1)
		{
			$condConsulta[] = "(LOWER(nombres_trabajador) like '%" . $nombre . "%' or LOWER(apellidos_trabajador) like '%" . $nombre .		 "%')";
		}
		
		if($campo_fecha&&$campo_fecha!=""&&$campo_fecha2&&$campo_fecha2!=""&&$tipEstado==2){
			
				/*	$sql_st = "( 
								(convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha."/1919',103))
								and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha2."/1919',103) )";		
							
					for($anyo=1920;$anyo<date('Y');$anyo++){
							$campo_fecha_=$campo_fecha."/".$anyo;
							$campo_fecha2_=$campo_fecha2."/".$anyo;
							$sql_st .=" or (convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha2_."',103))
										and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha_."',103))";
					}
					$sql_st .=")";
			$condConsulta[] = $sql_st;*/
			
			$sql_st="(";
			
			$sql_st.= "  substring(B.FECHA_NACIMIENTO,1,5)='".$campo_fecha."' ";				
			
			if($campo_fecha2!=$campo_fecha)
			{
				$sql_st.= " or  substring(B.FECHA_NACIMIENTO,1,5)='".$campo_fecha2."' ";				
			}			
			
			$sql_st.=" )";
			$condConsulta[] = $sql_st;
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		//echo "<br>la tabla".$table."<br>el wherte".$where."<br>tipo de cor".$TipoCor."<br>";

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($nombre);
	

			$sql_SP = sprintf("EXECUTE sp_busIdOnomastico %s,%s,%s,0,0",
								($nombre) ? "'".$this->PrepareParamSQL($nombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
	//	echo $sql_SP.'<BR>';
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
				$sql_SP = sprintf("EXECUTE sp_busIdOnomastico %s,%s,%s,%s,%s",
									($nombre) ? "'".$this->PrepareParamSQL($nombre)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$start,
									$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosOnomasticos %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow()){
							$html->append('arrPer', array('id' => $id[0],
														  'nombre' => ucwords($NotiData[1]),
														  'apellido' => $NotiData[2],
														  'dependencia' => ucfirst($NotiData[0]),
														   'email' => $NotiData[3],
														  'fecha' => $NotiData[4],
														  'siglas' => $NotiData[5]
														  ));
							if($_SESSION['cod_usuario']==$NotiData[3]){
								$alerta=1;
								$nombresCompleto=ucwords($NotiData[2]);
							}
						}
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		$html->assign('alerta',$alerta);
		$html->assign('nombresCompleto',$nombresCompleto);
		
		$html->assign_by_ref('userDirectorio',$_SESSION['cod_usuario']);

		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_PERSONA], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscaOnomastico', $this->arr_accion[BUSCA_ONOMASTICO], true));

		// Muestra el Resultado de la Busqueda
		$html->display('onomasticos/searchResultPopud.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		
	}	
	
	function muestraOnomasticoSemanal() {
		$dotw = date('w');
		if($dotw == 0) $dotw = 7;
		$op_f = $dotw-1;
		$op_l = 7-$dotw;
		$fdotw = date ("d/n/Y", mktime (0,0,0,date("m"),date("d")-$op_f,date("Y")));
		$ldotw = date ("d/n/Y", mktime (0,0,0,date("m"),date("d")+$op_l,date("Y")));
	
		// Busqueda de las personas q cumplen a�os en la semana actual
		$this->abreConnDB();
		// $this->conn->debug = true;
		$sql_st="select c.siglas, b.apellidos_trabajador, b.nombres_trabajador, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
				"from dbo.h_trabajador b, dbo.h_dependencia c ".
				"where c.codigo_dependencia=b.coddep and ".
				"CONVERT(datetime,CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(YY,GETDATE())),103) BETWEEN CONVERT(datetime,'$fdotw',103) AND CONVERT(datetime,'$ldotw',103) and ".
				"ESTADO!='INACTIVO' and b.CONDICION!='GENERAL' ".
				"ORDER BY CONVERT(datetime,CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(YY,GETDATE())),103)";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
	?>
	<table width="170" border="0" cellpadding="2" cellspacing="4">
	  <tr>
		<td width="170"><img src="/img/800x600/onomasticos/lb-cumplesemanal.gif" width="170" height="25"></td>
	  </tr>
<?
			$numrows = $rs->RecordCount();
			if($numrows<=0){
?>
	  <tr>
		<td class="contenido-intranet"><b>No hay Onom&aacute;sticos en esta semana.</b></td>
	  </tr>
<?
			}
			while ($row = $rs->FetchRow()) {
?>
	  <tr>
		<td>
		  <table width="170" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td class="item" colspan="2" width="170"> 
				<b><? echo ucwords(strtolower($row[1])) . " " . ucwords(strtolower($row[2])); ?></b>
			  </td>
			</tr>
			<tr> 
			  <td class="sub-item-intranet" width="80">Dependencias</td>
			  <td class="textogray" width="90"> 
				<? echo $row[0];?>
			  </td>
			</tr>
			<tr> 
			  <td class="sub-item-intranet" width="80">Fecha</td>
			  <td class="contenido-intranet" width="90"> 
				<? echo $row[3];?>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
<?
			}
			unset($row);
			$rs->Close();
		}
?>
	</table>
<?
	}
	
	/********************************************************************/
	/* Funcion para la Muestra de los Cumplea�os de la Busqueda Central */
	/* Modulo Onomasticos  										    	*/
	/********************************************************************/
	
	function muestraTablaBusqueda($nombre=NULL, $dia=NULL, $mes=NULL,$campo_fecha=NULL,$campo_fecha2=NULL,$tipoEstado){
		$frmName = "frmOno";
		$this->insertaScriptTipoBusquedaOnomastico($frmName)
		//$tipEstado=($_POST['tipEstado']) ? $_POST['tipEstado'] : $_GET['tipEstado'];
?>
<table width="531"  border="0" align="center" cellspacing="2">
  <tr>
    <td class="contenido-intranet">Permite conocer los onom�sticos de los trabajadores de CONVENIO_SITRADOC, donde puedes buscar por apellidos y nombre o por mes y d�a de nacimiento.</td>
  </tr>
</table>	
<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	<table width="531" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td>	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabla-login" bgcolor="#FDF9EE">
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7" ><label> 
              <input name="tipEstado" type="radio" value="1" <?php if ($tipoEstado==1||!$tipoEstado){ echo "checked"; }?> onClick="cargarR(document.<?=$frmName?>,1);">
              Por apellido y/o nombre </label>
              &nbsp;
			  <label> 
              <input name="tipEstado" type="radio" value="2" <?php if ($tipoEstado==2){ echo "checked";} ?> onClick="cargarR(document.<?=$frmName?>,2);">
              Por fecha </label>
			<input name="tipoEstado" type="hidden" id="tipoEstado" />
			</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7" >&nbsp;</td>
          </tr
		  ><?php if ($tipoEstado==1||!$tipoEstado){?>		  
          <tr>
            <td width="10">&nbsp;</td>
            <td class="contenido-intranet" width="150">Por Nombre y/o Apellido:</td>
            <td colspan="2"><input type="text" name="nombre" class="ip-login contenido" size="23" value="<?echo $nombre?>">
            </td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" value="Buscar" name="Submit" class="submitintranet" onClick="return tipbuscaOnomastico()"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
		  <?php }?>
		  <?php if ($tipoEstado==2){?>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido-intranet" width="150">Por Fecha:</td>
            <td width="45">Entre el</td>
            <td width="45"><input name="campo_fecha"  type="text" class="ip-login contenido" id="campo_fecha"  size="12" tabindex="4" onKeyPress="ninguna_letra();" value="<?php echo date('d/m') ; ?>" readonly=""></td>
            <td align="left">      <img src="estilos/i.p.cal.gif" width="15" height="12" id=lanzador value=...>
      <!-- script que define y configura el calendario-->
      <SCRIPT type=text/javascript>
			    Calendar.setup({
		        inputField     :    "campo_fecha",      // id del campo de texto
        		ifFormat       :    "%d/%m",       // formato de la fecha, cuando se escriba en el campo de texto
		        button         :    "lanzador"   // el id del bot&oacute;n que lanzar&aacute; el calendario
			    });
		</SCRIPT>
      <input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="" />
</td>
            <td width="45">y el</td>
            <td width="45"><input name="campo_fecha2"  type="text" class="ip-login contenido" id="campo_fecha2"  size="12" tabindex="4" onKeyPress="ninguna_letra();" value="<?php echo date('d/m') ; ?>" readonly=""></td>
            <td><img src="estilos/i.p.cal.gif" width="15" height="12" id=lanzador2 value=...>
      <!-- script que define y configura el calendario-->
      <SCRIPT type=text/javascript>
			    Calendar.setup({
		        inputField     :    "campo_fecha2",      // id del campo de texto
        		//ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
				ifFormat       :    "%d/%m",       // formato de la fecha, cuando se escriba en el campo de texto
		        button         :    "lanzador2"   // el id del bot&oacute;n que lanzar&aacute; el calendario
			    });
		</SCRIPT>
      <input name="fecDesembarqueFin" type="hidden" id="fecDesembarqueFin" value="" /></td>
            <td><input type="submit" value="Buscar" name="Submit" class="submitintranet" onClick="return tipbuscaOnomastico()">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
		  <?php }?>
        </table></td>
	  </tr>
	  
	</table>
</form>
	<br>
<?
	}
	
	function muestraResultadosBusqueda($nombre, $dia, $mes,$campo_fecha,$campo_fecha2){
		global $type_db, $serverdb, $userdb, $passdb, $db, $abs_path;
		//echo "c".$campo_fecha."c".$campo_fecha2;
		
		if(!empty($nombre)){
			// Query para la busqueda de Todos los registros coincidentes con CADA UNA de las palabras ingresadas
		/*	$arr_nombre=explode(" ", str_replace("  ", " ", trim(strtolower($nombre))));
			for($i=0;$i<count($arr_nombre);$i++){
				if(!empty($arr_nombre[$i])){
					$sql_st[] =	"select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
									"from dbo.h_trabajador b, dbo.h_dependencia c ".
									"where c.codigo_dependencia=b.coddep and (LOWER(nombres_trabajador) like '%" . $arr_nombre[$i] . "%' ".
									"or LOWER(apellidos_trabajador) like '%" . $arr_nombre[$i] . "%') and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
				}
			}
			if(count($sql_st)>1){
				$sql_st = implode(" UNION ", $sql_st);
				$sql_st .= " order by b.apellidos_trabajador";
			}else{
				$sql_st = $sql_st[0];
				$sql_st .= " order by b.apellidos_trabajador";
			}
		*/
			// Query para la busqueda de Todos los registros coincidentes con TODA LA FRASE de ingresada
			$strNombre = trim(strtolower($nombre));

        	        // Evalua que no contenga sentencias SQL
	                $this->evaluaNoSql($strNombre);

			$sql_st = "select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
						"from dbo.h_trabajador b, dbo.h_dependencia c ".
						"where c.codigo_dependencia=b.coddep and (LOWER(nombres_trabajador) like '%" . $strNombre . "%' ".
						"or LOWER(apellidos_trabajador) like '%" . $strNombre . "%') and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
		//	echo $sql_st;
		}else{
			// Query para la busqueda de los Registros coincidentes con la Fecha de Nacimiento Ingresada
			$sql_st ="select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
						"from dbo.h_trabajador b, dbo.h_dependencia c ".
						//"where c.codigo_dependencia=b.coddep and DATEPART(DD, CONVERT(datetime,fecha_nacimiento,103))=${dia}".
						//" and DATEPART(MM, CONVERT(datetime,fecha_nacimiento,103))=${mes} and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
						"where c.codigo_dependencia=b.coddep 
							AND b.fecha_nacimiento is not null
							AND B.FECHA_NACIMIENTO<>'29/02/1900' and (";
					$sql_st .=" (convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha."/1919',103))
							and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha2."/1919',103) )";		
							
					for($anyo=1920;$anyo<date('Y');$anyo++){
							$campo_fecha_=$campo_fecha."/".$anyo;
							$campo_fecha2_=$campo_fecha2."/".$anyo;
							$sql_st .=" or (convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha2_."',103))
							and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha_."',103) )";
					}		
							$sql_st .=") and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL' 		
								order by DATEPART(MM, CONVERT(datetime,fecha_nacimiento,103)),DATEPART(DD, CONVERT(datetime,fecha_nacimiento,103)) DESC		
						";
		//	echo $sql_st;
		}
		
		// Ejecucion del Query
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$numrows = $rs->RecordCount();
			if($numrows>0){
	?> 
		<table width="531" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td height="1"> 
			  <hr width="100%" size="1" align="center">
			</td>
		  </tr>
		  <tr>
			<td class="contenido" valign="bottom" height="14"><img src="<?=$abs_path;?>/img/800x600/lb-resultados.gif" width="93" height="14" align="absbottom">
			<b><? echo "Se encontraron $numrows coincidencia"; if($numrows>1) echo "s";?></b>
			</td>
		  </tr>
		  <tr>
			<td>
			  <hr width="100%" size="1">
			</td>
		  </tr>
		</table>
		<br>
<?
			}else{
?>
		<table width="531" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td height="1"> 
			  <hr width="100%" size="1" align="center">
			</td>
		  </tr>
		  <tr> 
			<td><img src="<?=$abs_path;?>/img/800x600/lb-sinresultados.gif" width="239" height="14"></td>
		  </tr>
		  <tr> 
			<td> 
			  <hr width="100%" size="1">
			</td>
		  </tr>
		</table>
		<br>
<?
			}
			while ($row = $rs->FetchRow()) {
?>
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
		    <td rowspan="4" valign="top" width="40" align="center"><img src="<?=$abs_path;?>/img/800x600/onomasticos/cumple.jpg" width="27" height="27"></td>
		    <td colspan="2" class="item">&nbsp;</td>
	      </tr>
		  <tr> 
			<td colspan="2" class="item-intranet"><b><?echo $row[1];?> <?echo $row[2];?></b></td>
		  </tr>
		  <tr> 
			<td class="sub-item-intranet" width="80">Dependencias:</td>
			<td class="textogray" width="424">
			  <?echo $row[0];?>
			</td>
		  </tr>
		  <tr> 
			<td class="sub-item-intranet" width="80">Fecha:</td>
			<td class="sub-item-intranet" width="424"><b>
			  <?echo $row[4];?></b>
			</td>
		  </tr>
		  <tr>
		    <td valign="top" align="center">&nbsp;</td>
		    <td class="sub-item">&nbsp;</td>
		    <td class="contenido">&nbsp;</td>
	      </tr>
</table>
		<br>
	<?
			}
			unset($row);
			$rs->Close();
		}
	}

}	
?>
