<?php

include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class ActividadesMinistro extends Modulos{

    function ActividadesMinistro($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][1];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][2];
		$this->userDB = $this->arr_userDB['postgres'][1];
		$this->passDB = $this->arr_passDB['postgres'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_ACTIVIDAD => 'frmSearchActi',
							BUSCA_ACTIVIDAD => 'searchActi',
							FRM_AGREGA_ACTIVIDAD => 'frmAddActi',
							AGREGA_ACTIVIDAD => 'addActi',
							FRM_MODIFICA_ACTIVIDAD => 'frmModifyActi',
							MODIFICA_ACTIVIDAD => 'modifyActi',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		$this->datosUsuarioPortal();
		
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Actividades del Ministro',false,array('noticias'));
			$objIntranet->Body('actiminiportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		/*
		if($this->userIntranet['COD_DEP']!=18){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Actividades del Ministro',false,array('noticias'));
			$objIntranet->Body('actiminiportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		
		// Items del Menu Principal
		if($_SESSION['mod_ind_leer']) $this->menu_items[0] = array ( 'val' => 'frmSearchActi', label => 'BUSCAR' );
		if($_SESSION['mod_ind_insertar']) $this->menu_items[1] = array ( 'val' => 'frmAddActi', label => 'AGREGAR' );
		if($_SESSION['mod_ind_modificar']) $this->menu_items[2] = array ( 'val' => 'frmModifyActi', label => 'MODIFICAR' );

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	
	function ValidaFechaActividad($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('portal/actividades/headerArm.tpl.php');
		$html->display('portal/actividades/showStatTrans.inc.php');
		$html->display('portal/actividades/footerArm.tpl.php');
	}
	
	function FormBuscaActividad($page=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('portal/actividades/headerArm.tpl.php');
		$html->display('portal/actividades/search.tpl.php');
		if(!$search) $html->display('portal/actividades/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaActividad();
	}

	
	function BuscaActividad($page,$desNombre){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaActividad($page,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_st = sprintf("SELECT count(*) ".
						  "FROM public.actividad ".
						  "WHERE (Upper(des_titulo) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
								 "or Upper(des_actividad) like Upper('%%%1\$s%%'))",
						  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL);
		// echo $sql_st;		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar Busqueda
			$start =  ($page == 1) ? 0 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_st = sprintf("SELECT id_actividad, des_titulo, to_char(fec_actividad, 'DD/MM/YYYY HH24:MI'), ".
									 "CASE WHEN ind_activo is true THEN 'Activo' ELSE 'Inactivo' END ".
							  "FROM public.actividad ".
							  "WHERE (Upper(des_titulo) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
									 "or Upper(des_actividad) like Upper('%%%1\$s%%')) ".
							  "ORDER BY fec_creacion desc ".
							  "LIMIT %2\$s OFFSET %3\$s",
							  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL,
							  $this->numMaxResultsSearch,
							  $start);
			//echo $sql_st;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while($row = $rs->FetchRow()){
					$html->append('arrActi', array('id' => $row[0],
												   'desc' => $row[1],
												   'fech' => $row[2],
												   'esta' => $row[3]));
				}
				$rs->Close();
			}
			unset($rs);
		}else
			$start = 0;
					
		// Setea indicadores de derecho
		$html->assign('indMod',$_SESSION['mod_ind_modificar']);

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ACTIVIDAD], true));

		// Muestra el Resultado de la Busqueda
		$html->display('portal/actividades/searchResult.tpl.php');
		$html->display('portal/actividades/footerArm.tpl.php');
	}
	
	function FormManipulaActividad($idActividad=NULL,$desTitulo=NULL,$desResumen=NULL,$desActividad=NULL,$fecActividad=NULL,$desFuente=NULL,
 							     $indActivo=NULL,$indImg=NULL,$desImg=NULL,$indImgExp=NULL,$desImgExp=NULL,$numAnImgExp=NULL,
							     $numAlImgExp=NULL,$reLoad=false,$errors=NULL){
								
		if($idActividad&&!$reLoad){
			$this->abreConnDB();
			
			$sql_st = sprintf("SELECT des_titulo,des_resumen,des_actividad,to_char(fec_actividad,'MM/DD/YYYY HH24:MI'),des_fuente".
			   						 ",CASE WHEN ind_activo is true THEN 'Y' ELSE 'N' END,CASE WHEN ind_imagen is true THEN 'Y' ELSE 'N' END".
									 ",des_imagen,CASE WHEN ind_imagenexpandible is true THEN 'Y' ELSE 'N' END,des_imagenexpandible".
									 ",num_imagenancho,num_imagenalto ".
							  "FROM public.actividad ".
							  "WHERE id_actividad=%d",$this->PrepareParamSQL($idActividad));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($desTitulo,$desResumen,$desActividad,$fecActividad,$desFuente,
						 $indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
						 $numAlImgExp) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
		//Manipulacion de las Fechas
		$fecActividad = ($fecActividad!='// :'&&$fecActividad&&!strstr($fecActividad,'none')) ? $fecActividad : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddActi';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('idActividad',$idActividad);
		$html->assign_by_ref('desTitulo',$desTitulo);
		$html->assign_by_ref('desResumen',$desResumen);
		$html->assign_by_ref('desActividad',$desActividad);
		$html->assign_by_ref('desFuente',$desFuente);
		$html->assign_by_ref('desImg',$desImg);
		$html->assign_by_ref('desImgExp',$desImgExp);
		
		$html->assign_by_ref('numAnImgExp',$numAnImgExp);
		$html->assign_by_ref('numAlImgExp',$numAlImgExp);
		
		$html->assign_by_ref('indActivo',$indActivo);
		$html->assign_by_ref('indImg',$indImg);
		$html->assign_by_ref('indImgExp',$indImgExp);
		
		// Fecha Actividad
		$html->assign_by_ref('selMesActi',$this->ObjFrmMes(1, 12, true, substr($fecActividad,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaActi',$this->ObjFrmDia(1, 31, substr($fecActividad,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoActi',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecActividad,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selHoraActi',$this->ObjFrmHora(0, 23, true, (substr($fecActividad,11,2)) ? substr($fecActividad,11,2) : NULL, true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selMinActi',$this->ObjFrmMinuto(1, true, (substr($fecActividad,14,2)) ? substr($fecActividad,14,2) : NULL, true, array('val'=>'none','label'=>'---')));

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('portal/actividades/headerArm.tpl.php');
		$html->display('portal/actividades/frmHandleActi.tpl.php');
		$html->display('portal/actividades/footerArm.tpl.php');
	
	}

	function ManipulaActividad($idActividad,$desTitulo,$desResumen,$desActividad,$fecActividad,$desFuente,
							 $indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
							 $numAlImgExp){
							
		//Manipulacion de las Fechas
		$fecActividad = ($fecActividad!='// :'&&$fecActividad&&!strstr($fecActividad,'none')) ? $fecActividad : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImg = trim($desImg)!='/mipe/img/prensa/' ? $desImg : NULL; 
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$desImgExp = trim($desImgExp)!='/mipe/img/prensa/' ? $desImgExp : NULL; 
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Comprueba Valores	
		if(!$desTitulo){ $bTit = true; $this->errors .= 'El T�tulo debe ser especificado<br>'; }
		if(!$desResumen){ $bRes = true; $this->errors .= 'El Resumen debe ser especificado<br>'; }
		if(!$desActividad){ $bActi = true; $this->errors .= 'La Actividad debe ser especificada<br>'; }
		if($fecActividad){ $bFActi = ($this->ValidaFechaActividad($fecActividad,'Actividad')); }
		if($indImg=='Y'&&!$desImg){ $bImg = true; $this->errors .= 'La Imagen debe ser especificada<br>'; }
		if($indImg=='Y'&&$indImgExp=='Y'&&!$desImgExp){ $bDImgE = true; $this->errors .= 'La Imagen Expandible debe ser especificada<br>'; }
		if($indImg=='Y'&&$indImgExp=='Y'&&(!$numAnImgExp||!$numAnImgExp)){ $bTImgE = true; $this->errors .= 'El Tama�o de la Imagen Expandible debe ser especificado<br>'; }

		if($bTit||$bRes||$bActi||$bFActi||$bImg||$bDImgE||$bTImgE){
			$objIntranet = new Intranet();
			$objIntranet->Header('Actividades del Ministro',false,array('noticias'));
			$objIntranet->Body('actiminiportal_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormManipulaActividad($idActividad,$desTitulo,$desResumen,$desActividad,$fecActividad,$desFuente,
							$indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
							$numAlImgExp,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			// $this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_FT = sprintf("SELECT %s(%s'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
							  $idActividad ? 'modActiMiniPortal' : 'addActiMiniPortal',
							  $idActividad ? $this->PrepareParamSQL($idActividad).',' : '',
							  $this->PrepareParamSQL($desTitulo),
							  $this->PrepareParamSQL($desResumen),
							  $this->PrepareParamSQL($desActividad),
							  ($fecActividad) ? "'".$this->PrepareParamSQL($fecActividad)."'" : 'NULL',
							  ($desFuente) ? "'".$this->PrepareParamSQL($desFuente)."'" : 'NULL',
							  ($indImg=='Y') ? 'true' : 'false',
							  ($indImg=='Y') ? "'".$this->PrepareParamSQL($desImg)."'" : 'NULL',
							  ($indImgExp=='Y') ? 'true' : 'false',
							  ($indImgExp=='Y') ? "'".$this->PrepareParamSQL($desImgExp)."'" : 'NULL',
							  ($indImgExp=='Y') ? $this->PrepareParamSQL($numAnImgExp) : 'NULL',
							  ($indImgExp=='Y') ? $this->PrepareParamSQL($numAlImgExp) : 'NULL',
							  ($indActivo=='Y') ? 'true' : 'false',
							  $this->userIntranet['CODIGO']);
			// echo $sql_FT;
			$rs = & $this->conn->Execute($sql_FT);
			unset($sql_FT);
			if (!$rs)
				$RETVAL=0;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 0;
				$rs->Close();
			}
			unset($rs);		

			if(!$RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= (!$RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu=";
			$destination .= $idActividad ? $this->menu_items[2]['val'] : $this->menu_items[1]['val'];
			// echo $destination; exit;
			header("Location: $destination");
			exit;
		}
	}
}
?>
