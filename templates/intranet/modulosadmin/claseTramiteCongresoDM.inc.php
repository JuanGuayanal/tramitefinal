<?
include_once('claseTramiteCongreso.inc.php');
//require_once('/portal/modulos/claseModulos.inc.php');

class TramiteCongresoDM extends TramiteCongreso{

	var $statDocumento = array(NODERIVADO  => 'N',
							   DERIVADOVMP => 'D',
							   DERIVADOVMI => 'D',
							   DERIVADOSG => 'D',
							   RESPONDIDO => 'R');
	
	function TramiteCongresoDM($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;

		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][0];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][9];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							MUESTRA_SUMARIO => 'viewSumary',
							FRM_AGREGA_DOCUMENTO =>'frmAddDocumento',
							AGREGA_DOCUMENTO => 'addDocumento',
							FRM_BUSCA_DOCUMENTO => 'frmSearchDoc',
							BUSCA_DOCUMENTO => 'searchDoc',
							FRM_ASIGNA_DOCUMENTO => 'frmAssignDocumento',
							ASIGNA_DOCUMENTO => 'assignDocumento',
							FRM_CERRAR_TRAMITE => 'frmCerrarTramite',
							CERRAR_TRAMITE => 'cerrartramite',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							REPORTE_DOCTER => 'ReporteDocTer',
							GENERAREPORTE => 'GeneraReporte',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'viewSumary', label => 'SUMARIO' ),
		                    1 => array ( 'val' => 'frmAddDocumento', label => 'AGREGAR' ),
							2 => array ( 'val' => 'frmSearchDoc', label => 'BUSCAR' ),
							3 => array ( 'val' => 'frmAssignDocumento', label => 'ASIGNAR'),
							4 => array ( 'val' => 'frmCerrarTramite', label => 'CERRAR TRAMITE'),
							5 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTES')
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		$this->pathTemplate = 'dm/tramite/';		
    }
	
	function CantidadDocumentos($stat,$i){
		$this->abreConnDB();
		//$this->conn->debug = true;
		if ($this->statDocumento[NODERIVADO]==$stat){
			$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento ".
				  "WHERE flag='{$stat}'";
		}
		if (($this->statDocumento[DERIVADOVMP]==$stat)&&($i==2)){
			$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento d, dbo.documento_h_dependencia ddep ".
				  "WHERE d.flag='{$stat}' AND ddep.codigo_dependencia=16 AND ddep.id_documento=d.id_documento";
		}
		if (($this->statDocumento[DERIVADOVMI]==$stat&&($i==3))){
			$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento d, dbo.documento_h_dependencia ddep ".
				  "WHERE d.flag='{$stat}' AND ddep.codigo_dependencia=36 AND ddep.id_documento=d.id_documento";
		}
		if (($this->statDocumento[DERIVADOSG]==$stat&&($i==4))){
			$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento d, dbo.documento_h_dependencia ddep ".
				  "WHERE d.flag='{$stat}' AND ddep.codigo_dependencia=5 AND ddep.id_documento=d.id_documento";
		}
		if ($this->statDocumento[RESPONDIDO]==$stat){
			$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento ".
				  "WHERE flag='{$stat}'";
		}
		/*
		$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.documento ".
				  "WHERE flag='{$stat}'";
		*/
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$row = $rs->FetchRow();
			$rs->Close();
		}
		
		return $row[0];	
	}
	
	function MuestraSumario(){
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('statDocumento',$this->statDocumento);
		
		$html->assign('nNoderivado',$this->CantidadDocumentos($this->statDocumento[NODERIVADO],1));
		$html->assign('nDerivado1',$this->CantidadDocumentos($this->statDocumento[DERIVADOVMP],2));
		$html->assign('nDerivado2',$this->CantidadDocumentos($this->statDocumento[DERIVADOVMI],3));
		$html->assign('nDerivado3',$this->CantidadDocumentos($this->statDocumento[DERIVADOSG],4));
		$html->assign('nRespondido',$this->CantidadDocumentos($this->statDocumento[RESPONDIDO],5));

		$html->display($this->pathTemplate . 'index.tpl.php');
	}
	
	function MuestraIndex(){
		$this->MuestraSumario();
	}

	function FormAgregaDocumento($nroTD=NULL,$idSolicitante=NULL,$fecIngreso=NULL,$nro_doc=NULL,$asunto=NULL,$prioridad=NULL,$indDer=NULL,$codDependencia=NULL,$nota=NULL,$fecAsignacion=NULL,$maxdias=NULL,$result=NULL,$errors=false){
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso) ? $fecIngreso : date('m/d/Y');

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddDocumento';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('nro_doc',$nro_doc);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('indDer',$indDer);

		// Setea Campos de los Datos Ingresados por el Usuario
		//Manipulacion de las Fechas
		$fecAsignacion = ($fecAsignacion!='//'&&$fecAsignacion) ? $fecAsignacion : date('m/d/Y');

		//$html->assign_by_ref('id',$idDocumento);
		$html->assign_by_ref('nota',$nota);
		$html->assign_by_ref('maxdias',$maxdias);
		$html->assign_by_ref('result',$result);
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(dependencia) ".
				  "FROM DB_GENERAL.dbo.h_dependencia ".
				  "WHERE (codigo_dependencia=16 or codigo_dependencia=36 or codigo_dependencia=5) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDependencia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha de Asignacion
		$html->assign_by_ref('selMesAsig',$this->ObjFrmMes(1, 12, true, substr($fecAsignacion,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaAsig',$this->ObjFrmDia(1, 31, substr($fecAsignacion,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoAsig',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAsignacion,6,4), true, array('val'=>'none','label'=>'--------')));



		//$html->assign_by_ref('prioridad',$prioridad);
		
		// Contenido Select Solicitante
		$sql_st = "SELECT id_solicitante, lower(descripcion) ".
				  "FROM dbo.solicitante ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSolicitante',$this->ObjFrmSelect($sql_st, $idSolicitante, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddDocumento.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}

	function AgregaDocumento($nroTD,$idSolicitante,$fecIngreso,$nro_doc,$asunto,$prioridad,$indDer,$codDependencia,$nota,$fecAsignacion,$maxdias,$result){
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		// Comprueba Valores	
		if(!$nroTD){ $bReg = true; $this->errors .= 'El N�mero de Tr�mite Documentario debe ser especificado<br>'; }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaDocumento($fecIngreso,'Ingreso')); }
		if($fecIngreso > date('m/d/Y')){ $bFIng =true; $this->errors .= 'La fecha de Ingreso es mayor al d�a de hoy.
																		 Vuelva a ingresar los Datos';}
		if(!$nro_doc){ $bDoc = true; $this->errors .= 'El N�mero de Documento u Oficio debe ser especificado<br>'; }
		if(!$idSolicitante){ $bSol = true; $this->errors .= 'El Congresista o Comisi�n debe ser especificada<br>'; }
		if(!$asunto){ $bAsunto = true; $this->errors .= 'El Asunto debe ser especificado<br>'; }
		if(!$prioridad){ $bprior = true; $this->errors .= 'La prioridad debe ser especificada<br>'; }

		if($bReg||$bFIng||$bDoc||$bSol||$bAsunto||$bprior){
			$objIntranet = new Intranet();
			$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaDocumento($nroTD,$idSolicitante,$fecIngreso,$nro_doc,$asunto,$prioridad,$indDer,$codDependencia,$nota,$fecAsignacion,$maxdias,$result,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;

			/////////////////////////////////////////////////////////////////////////
			if ($indDer == 1){
				$sql_SP = sprintf("EXECUTE sp_insasigDocumento '%s',%d,%s,'%s',%s,%d,'%s',%s,%s,'%s',%d",
								  $this->PrepareParamSQL($nroTD),
								  $this->PrepareParamSQL($idSolicitante),
								  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : "NULL",
								  ($nro_doc) ? $nro_doc : 'NULL',
								  ($asunto) ? "'".$this->PrepareParamSQL($asunto)."'" : "NULL",
								  $this->PrepareParamSQL($prioridad),
								  $this->PrepareParamSQL($nota),
								  ($fecAsignacion) ? "'".$this->PrepareParamSQL($fecAsignacion)."'" : "NULL",
								  ($result) ? "'".$this->PrepareParamSQL($result)."'" : "NULL",
								  $_SESSION['cod_usuario'],
								  $this->PrepareParamSQL($codDependencia));
			}else{
						$sql_SP = sprintf("EXECUTE sp_insDocumento '%s',%d,%s,'%s',%s,%d,'%s'",
										  $this->PrepareParamSQL($nroTD),
										  $this->PrepareParamSQL($idSolicitante),
										  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : "NULL",
										  ($nro_doc) ? $nro_doc : 'NULL',
										  ($asunto) ? "'".$this->PrepareParamSQL($asunto)."'" : "NULL",
										  $this->PrepareParamSQL($prioridad),
										  $_SESSION['cod_usuario']);
			
			}
				 //echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;

		}
	}
	
	function FormAsignaDocumento($idDocumento,$codDependencia=NULL,$nota=NULL,$fecAsignacion=NULL,$maxdias=NULL,$result=NULL,$errors=false){
		
		if(!$idDocumento){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		
		//Manipulacion de las Fechas
		$fecAsignacion = ($fecAsignacion!='//'&&$fecAsignacion) ? $fecAsignacion : date('m/d/Y');

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAssignDocumento';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$idDocumento);
		$html->assign_by_ref('nota',$nota);
		$html->assign_by_ref('maxdias',$maxdias);
		$html->assign_by_ref('result',$result);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(dependencia) ".
				  "FROM DB_GENERAL.dbo.h_dependencia ".
				  "WHERE (codigo_dependencia=16 or codigo_dependencia=36 or codigo_dependencia=5) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDependencia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha de Asignacion
		$html->assign_by_ref('selMesAsig',$this->ObjFrmMes(1, 12, true, substr($fecAsignacion,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaAsig',$this->ObjFrmDia(1, 31, substr($fecAsignacion,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoAsig',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAsignacion,6,4), true, array('val'=>'none','label'=>'--------')));
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAssignDocumento.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}

	function AsignaDocumento($idDocumento,$codDependencia,$nota,$fecAsignacion,$maxdias,$result){
		//Manipulacion de las Fechas
		$fecAsignacion = ($fecAsignacion!='//'&&$fecAsignacion&&!strstr($fecAsignacion,'none')) ? $fecAsignacion : NULL;

		// Comprueba Valores	
		if($fecAsignacion){ $bFAsig = ($this->ValidaFechaDocumento($fecAsignacion,'Asignacion')); }
		if($fecAsignacion > date('m/d/Y')){ $bFAsig =true; $this->errors .= 'La fecha de Asignaci�n es mayor al d�a de hoy.
																		     Vuelva a ingresar los Datos';}
		if(!$nota){ $bNota = true; $this->errors .= 'El N�mero de Nota debe ser especificado<br>'; }
		if(!$codDependencia){ $bDep = true; $this->errors .= 'La Dependencia debe ser especificada<br>'; }
		if(!$maxdias){ $bmax = true; $this->errors .= 'El m�ximo dias de Respuesta debe ser especificado<br>'; }

		if($bFAsig||$bNota||$bDep||$bmax){
			$objIntranet = new Intranet();
			$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAsignaDocumento($idDocumento,$codDependencia,$nota,$fecAsignacion,$maxdias,$result,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$idDocumento."holas";exit;
			
			$this->conn->BeginTrans();
			
			$sql_SP = sprintf("EXECUTE sp_modDocumento %d,'%s',%s,%s,'%s'",
							  $this->PrepareParamSQL($idDocumento),
							  $this->PrepareParamSQL($nota),
							  ($fecAsignacion) ? "'".$this->PrepareParamSQL($fecAsignacion)."'" : "NULL",
							  ($result) ? "'".$this->PrepareParamSQL($result)."'" : "NULL",
							  $_SESSION['cod_usuario']);
			 //echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow())
					list($RETVAL,$idDocumento) = $row;
				else
					$RETVAL = 1;
				unset($rs);
				
				if(!$RETVAL){
						$sql_SP = sprintf("EXECUTE sp_asigDocumento %d,%d",
							  			  $idDocumento,
										  $this->PrepareParamSQL($codDependencia));
						//echo $sql_SP;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $this->conn->Execute($sql_SP)) ? $row[0] : 1;
							if($RETVAL) break;
						}
						unset($rs);
					}
				
				}
			unset($rs);
			
			if($RETVAL)
				$this->conn->RollBackTrans();
			else
				$this->conn->CommitTrans();	

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			header("Location: $destination");
			exit;
			
		}
	}

	function FormCerrarTramite($idDocumento,$nro_oficio=NULL,$fecOficio=NULL,$errors=false){
	
		if(!$idDocumento){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}

		//Manipulacion de las Fechas
		$fecOficio = ($fecOficio!='//'&&$fecOficio) ? $fecOficio : date('m/d/Y');

		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmCerrarTramite';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nro_oficio',$nro_oficio);
		
		// Fecha de Cierre del Tramite
		$html->assign_by_ref('selMesOficio',$this->ObjFrmMes(1, 12, true, substr($fecOficio,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaOficio',$this->ObjFrmDia(1, 31, substr($fecOficio,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoOficio',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecOficio,6,4), true, array('val'=>'none','label'=>'--------')));
	
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$idDocumento);
	
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmCerrarTramite.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}

	function CerrarTramite($idDocumento,$nro_oficio,$fecOficio){
		//Manipulacion de las Fechas
		$fecOficio = ($fecOficio!='//'&&$fecOficio&&!strstr($fecOficio,'none')) ? $fecOficio : NULL;

		// Comprueba Valores	
		if($fecOficio){ $bFOficio = ($this->ValidaFechaDocumento($fecOficio,'Oficio')); }
		if($fecOficio > date('m/d/Y')){ $bFOficio =true; $this->errors .= 'La fecha de Oficio es mayor al d�a de hoy.
																		     Vuelva a ingresar los Datos';}
		if(!$nro_oficio){ $bNro = true; $this->errors .= 'El N�mero de Oficio debe ser especificado<br>'; }

		if($bFOficio||$bNro){
			$objIntranet = new Intranet();
			$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormCerrarTramite($idDocumento,$nro_oficio,$fecOficio,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			/*Obtenemos el Id del SubDocumento que hemos ingresado*/
			//echo $idDocumento;
			$sql_st = "SELECT sd.id_subdocumento FROM dbo.sub_documento sd, dbo.documento_h_dependencia ddep, dbo.documento d 
			           WHERE d.id_documento=$idDocumento and ddep.id_documento_h_dependencia=sd.id_documento_h_dependencia
					         and ddep.id_documento=d.id_documento";
			//echo $sql_st;exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				exit;
			else{
				$subdocdep2 = $rs->FetchRow();
				$idSubDocumento = $subdocdep2[0];
				$rs->Close();
				}
			//
			unset($rs);
							
			/**/

			$sql_SP = sprintf("EXECUTE sp_modSubDocumento %d,'%s',%s,'%s'",
							  $this->PrepareParamSQL($idSubDocumento),
							  $this->PrepareParamSQL($nro_oficio),
							  ($fecOficio) ? "'".$this->PrepareParamSQL($fecOficio)."'" : "NULL",
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			/*Finalmente actualizamos el flag T del documento*/
					$sql_st = "UPDATE dbo.documento SET flag='T' WHERE id_documento=$idDocumento";
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if (!$rs)
						exit;
					else{
						//$docdep = $rs->FetchRow();
						$rs->Close();
						}
					unset($rs);
			/**/		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[4]['val']}";
			header("Location: $destination");
			exit;

		}
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage --size 'a4' --fontsize 6.0 %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}

	function FormGeneraReporte($tipReporte=NULL,$search2=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'reportes.tpl.php');
		if(!$search2) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
		
	function GeneraReporte($tipReporte){
		switch($tipReporte){
			case 1:
			$this->ReporteDocTer();
			break;
		}
	
	}

	function ReporteDocTer(){
		// Genera HTML
		$html = new Smarty;
		//$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;

		$sql_st = "EXEC sp_reporteDocTer";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('docter',array('tram'=>$row[0],
										  'fIng'=>$row[1],
										  'dOficio'=>$row[2],
										  'asunto'=>$row[3],
										  'sol'=>ucwords($row[4]),
										  'der'=>ucwords($row[5]),
										  'nota'=>$row[6],
										  'fNota'=>$row[7],
										  'doc'=>$row[8],
										  'fDoc'=>$row[9],
										  'oficio'=>$row[10],
										  'fOficio'=>$row[11]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		//$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.SITRADOC.jpg';
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.SITRADOC.jpg';
		
		//$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/dm/tramite/reports';
		$filename = 'DocTer'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dm/tramite/reporteDocTer.tpl.php'),true,$logo);


		$destination = 'https://' . $_SERVER['HTTP_HOST'] . '/institucional/aplicativos/dm/tramite/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

}
?>