<?
require_once('claseModulos.inc.php');

class Calendario extends Modulos{
	// Almacena la Fecha del Dia Visualizado
	var $fecha;
	var $diaFecha;
	var $mesFecha;
	var $anyoFecha;
	
	var $useCal;
	
	var $HoraIni = '8';
	var $HoraFin = '18';
	
	var $HoraIniVistaDiaria = '6';
	var $HoraFinVistaDiaria = '22';

	function Calendario($fecha) {
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		$this->arr_accion = array(
							NUEVA_ACTIVIDAD => 'newActivity',
							ACTIVIDAD_PARTICIPANTES => 'participants',
							AGREGA_ACTIVIDAD => 'insActivity',
							VISTA_DIARIA => 'viewDay',
							VISTA_SEMANAL => 'viewWeek',
							MUESTRA_ACTIVIDAD => 'showActivity'
							);
		
		// Setea Variables de la Fecha
		$this->fecha = $fecha;
		$this->anyoFecha = ($this->fecha) ? substr($this->fecha,6,4) : date('Y');
		$this->mesFecha = ($this->fecha) ? sprintf('%d',substr($this->fecha,0,2)) : date('n');
		$this->diaFecha = ($this->fecha) ? sprintf('%d',substr($this->fecha,3,2)) : date('j');
		
		$this->datosUsuarioPOSTGRESQL();
		if(!$this->userIntranet)
			return ($this=false);
			
		$this->ModuloInfo(28);
		
		$this->userCal = $this->userIntranet['CODIGO'];
	}
	
	function DiaContiguo($pFecha,$ant=false){
		$dia = strftime('%d',strtotime($pFecha));
		$mes = strftime('%m',strtotime($pFecha));
		$anyo = strftime('%Y',strtotime($pFecha));
		
		return ($ant) ? strftime('%m/%d/%Y',mktime(0,0,0,$mes,$dia-1,$anyo)) : strftime('%m/%d/%Y',mktime(0,0,0,$mes,$dia+1,$anyo));
	}
	
	function MiniCalendario($full=false){
		$today = ($this->mesFecha==date('n')&&$this->anyoFecha==date('Y')) ? date('j') : false;
		$monthAnt = strftime('%m/%d/%Y',mktime(0,0,0,$this->mesFecha,0,$this->anyoFecha));
		$monthPos = strftime('%m/%d/%Y',mktime(0,0,0,$this->mesFecha+1,1,$this->anyoFecha));
				
		$tituloCal = ucfirst(strftime('%B %Y',mktime(1,1,1,$this->mesFecha,1,$this->anyoFecha)));
		$numDays = date("t",mktime(1,1,1,$this->mesFecha,1,$this->anyoFecha)); 
		$wdays = array('Do','Lu','Ma','Mi','Ju','Vi','Sa');
		
		// Calcula los dias del mes anterior dentro de la Primera Semanda del Mes
		$calc = date("w",mktime(1,1,1,$this->mesFecha,1,$this->anyoFecha));
		for($i=$calc; $i>0; $i--)
			$daysAnt[] = date("j",mktime(1,1,1,$this->mesFecha,1-$i,$this->anyoFecha));
		
		// Calcula los dias del mes posterior dentro de la Ultima Semanda del Mes
		// $calc = ceil((count($daysAnt)+$numDays)/7)*7 - (count($daysAnt)+$numDays);
		$calc = 42 - (count($daysAnt)+$numDays);
		for($i=1; $i<=$calc; $i++)
			$daysPos[] = date("j",mktime(1,1,1,$this->mesFecha,$numDays+$i,$this->anyoFecha));
		  
		// Muestra el HTML
		$html = new Smarty;
		$html->assign('frmUrl',$this->modInfo['PATH']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref("today",$today);
		$html->assign_by_ref("mesAnt",$monthAnt);
		$html->assign_by_ref("mesPos",$monthPos);
		
		$html->assign_by_ref("tituloCal",$tituloCal);
		$html->assign_by_ref("dayCal",$this->diaFecha);
		$html->assign_by_ref("wdays",$wdays);
		$html->assign("daysPad",count($daysAnt));
		$html->assign_by_ref("daysAnt",$daysAnt);
		$html->assign_by_ref("daysPos",$daysPos);
		$html->assign("numDays",$numDays+1);
		$html->assign_by_ref("month",sprintf('%02d',$this->mesFecha));
		$html->assign_by_ref("year",$this->anyoFecha);
		$html->display('calendario/miniCalendario.tpl.php');
	}
	
	function ValidaCamposActividad($desAsunto,$fecInicio,$fecFin,$indDiaEntero){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if(!$desAsunto)
			$errors .= "- Asunto es un dato Obligatorio.<br>";
		if( !checkdate(sprintf('%d',substr($fecInicio,0,2)),sprintf('%d',substr($fecInicio,3,2)),substr($fecInicio,6,4)) )
			$errors .= "- La Fecha de Inicio no es Valida.<br>";
		if( !checkdate(sprintf('%d',substr($fecFin,0,2)),sprintf('%d',substr($fecFin,3,2)),substr($fecFin,6,4)) )
			$errors .= "- La Fecha de Fin no es Valida.<br>";
		if(!($indDiaEntero) && strtotime($fecInicio)>=strtotime($fecFin) )
			$errors .= "- El Inicio de la Actividad debe ser menor al Fin.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
		
	}
	
	function FrmNuevaActividad($desAsunto=NULL,$desUbicacion=NULL,$desActividad=NULL,$fecInicio=NULL,$fecFin=NULL,$indDiaEntero=NULL,$indGrupal=NULL,$indPrivado=NULL,$errors=NULL){
		//Manipulacion de las Fechas de Ingreso de la Actividad
		$fecInicio = ($fecInicio!='// :'&&$fecInicio) ? $fecInicio : $this->fecha;
		$fecFin = ($fecFin!='// :'&&$fecFin) ? $fecFin : $this->fecha;
		
		// Crea el Objeto HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmActividad';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$this->modInfo['PATH']);
		$html->assign_by_ref('errors',$errors);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('date',$this->fecha);
		
		// Setea Campos de los Datos del solicitante
		$html->assign_by_ref('desAsunto',$desAsunto);
		$html->assign_by_ref('desUbicacion',$desUbicacion);
		$html->assign_by_ref('desActividad',$desActividad);
		
		// Setea Campos Fecha de Inicio
		$html->assign('selMesIni',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true));
		$html->assign('selDiaIni',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true));
		$anyoIni = substr($fecInicio,6,4);
		$html->assign('selAnyoIni',$this->ObjFrmAnyo($anyoIni-3, date('Y')+10, $anyoIni, true));
		$html->assign('selHoraIni',$this->ObjFrmHora(0, 23, true, (substr($fecInicio,11,2)) ? substr($fecInicio,11,2) : $this->HoraIni, true));
		$html->assign('selMinIni',$this->ObjFrmMinuto(30, true, (substr($fecInicio,14,2)) ? substr($fecInicio,11,2) : NULL, true));
		// Setea Campos Fecha de Fin
		$html->assign('selMesFin',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true));
		$html->assign('selDiaFin',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true));
		$anyoFin = substr($fecFin,6,4);
		$html->assign('selAnyoFin',$this->ObjFrmAnyo($anyoFin-3, date('Y')+10, $anyoFin, true));
		$html->assign('selHoraFin',$this->ObjFrmHora(0, 23, true, (substr($fecFin,11,2)) ? substr($fecFin,11,2) : $this->HoraIni, true));
		$html->assign('selMinFin',$this->ObjFrmMinuto(30, true, (substr($fecFin,14,2)) ? substr($fecFin,14,2) : NULL, true));

		$html->assign_by_ref('indDiaEntero',$indDiaEntero);
		$html->assign_by_ref('indGrupal',$indGrupal);
		$html->assign_by_ref('indPrivado',$indPrivado);
		
		$html->display('calendario/frmNuevaActividad.tpl.php');
	}
	
	function FrmActividadParticipantes($desAsunto,$desUbicacion,$desActividad,$fecInicio,$fecFin,$indDiaEntero,$indGrupal,$indPrivado){
		if(!$indGrupal)
			$this->AgregaActividad($desAsunto,$desUbicacion,$desActividad,$fecInicio,$fecFin,$indDiaEntero,$indGrupal,$indPrivado);
		else{
			if($this->ValidaCamposActividad($desAsunto,$fecInicio,$fecFin,$indDiaEntero)){
				$errors = $this->muestraMensajeInfo($this->errors,true);
				$this->FrmNuevaActividad($desAsunto,$desUbicacion,$desActividad,$fecInicio,$fecFin,$indDiaEntero,$indGrupal,$indPrivado,$errors);
			}else{
				$html = new Smarty;
	
				// Setea Caracteristicas en el Formulario
				$frmName = 'frmActividad';
				$html->assign_by_ref('accion',$this->arr_accion);
				$html->assign_by_ref('frmName',$frmName);
				$html->assign('frmUrl',$this->modInfo['PATH']);
				$html->assign_by_ref('date',$this->fecha);
				
				$sql_st1 =  "SELECT id_trabajador, substr(des_apellidos,1,strpos(des_apellidos,' ')-1)||', '||substr(des_nombres,1,strpos(des_nombres,' ')-1) ".
							"FROM trabajador ".
							"WHERE ind_activo IS TRUE and id_subdependencia={$this->userIntranet['COD_DEP']} ".
							"ORDER BY 2 ";
				$sql_st2 =  "SELECT id_trabajador, substr(des_apellidos,1,strpos(des_apellidos,' ')-1)||', '||substr(des_nombres,1,strpos(des_nombres,' ')-1) ".
							"FROM trabajador ".
							"WHERE ind_activo IS TRUE and id_trabajador={$this->userIntranet['CODIGO']} ".
							"ORDER BY 2 ";
				$html->assign('objItemsChange',$this->ObjFrmSelItemChange($sql_st1,$sql_st2,array('usuarios','participantes'),array('Usuarios','Participantes'),10,'ipsel1',$frmName,array('val'=>'','label'=>'-----------------------------')));
	
				// Setea Campos de la Actividad
				$html->assign_by_ref('desAsunto',$desAsunto);
				$html->assign_by_ref('desUbicacion',$desUbicacion);
				$html->assign_by_ref('desActividad',$desActividad);
				$html->assign_by_ref('fecInicio',$fecInicio);
				$html->assign_by_ref('fecFin',$fecFin);
				$html->assign_by_ref('indDiaEntero',$indDiaEntero);
				$html->assign_by_ref('indGrupal',$indGrupal);
				$html->assign_by_ref('indPrivado',$indPrivado);
				// Muestra la Interfaz HTML
				$html->display('calendario/frmActividadParticipantes.tpl.php');
			}
		}
	}
	
	function AgregaActividad($desAsunto,$desUbicacion,$desActividad,$fecInicio,$fecFin,$indDiaEntero,$indGrupal,$indPrivado,$participantes=NULL){
		// Si hay errores vuelve a mostrar el formulario de Ingreso de Actividad
		if($this->ValidaCamposActividad($desAsunto,$fecInicio,$fecFin,$indDiaEntero)){
			$errors = $this->muestraMensajeInfo($this->errors,true);
			$this->FrmNuevaActividad($desAsunto,$desUbicacion,$desActividad,$fecInicio,$fecFin,$indDiaEntero,$indGrupal,$indPrivado,$errors);
		}else{
			// Prepara el Statement
			$idActividad = $this->SequenceNextVal('CalendarioActividad_id_seq');
			if($idActividad){
				$ins_st = sprintf("INSERT into CalendarioActividad(id_actividad,id_trabajador,des_asunto,des_ubicacion,des_actividad,".
											  "fec_inicio,fec_fin,ind_diaentero,ind_grupal,ind_privado,usr_audit_crea,usr_audit_mod) ".
								  "VALUES (%d,%d,'%s',%s,%s,%s,%s,%s,%s,%s,%d,%d)",
								  $idActividad,
								  $this->userIntranet['CODIGO'],
								  $this->PrepareParamSQL($desAsunto),
								  ($desUbicacion) ? "'".$this->PrepareParamSQL($desUbicacion)."'" : 'NULL',
								  ($desActividad) ? "'".$this->PrepareParamSQL($desActividad)."'" : 'NULL',
								  ($indDiaEntero) ? "to_date('$fecInicio','MM/DD/YYYY')" : "to_timestamp('$fecInicio','MM/DD/YYYY HH24:MI')",
								  ($indDiaEntero) ? "to_date('$fecFin','MM/DD/YYYY')" : "to_timestamp('$fecFin','MM/DD/YYYY HH24:MI')",
								  ($indDiaEntero) ? 'true' : 'false',
								  ($indGrupal) ? 'true' : 'false',
								  ($indPrivado) ? 'true' : 'false',
								  $this->userIntranet['CODIGO'],
								  $this->userIntranet['CODIGO']);
				// Abre Conexion y ejecuta el Statement
				$this->abreConnDB();
				if( $this->conn->Execute($ins_st) === false ){
					print $this->conn->ErrorMsg();
				}else{
					unset($ins_st);
					if(is_array($participantes)){
						for($i=0;$i<count($participantes);$i++){
							$ins_st = sprintf("INSERT INTO CalActiParticipante(id_actividad,id_trabajador,usr_audit) ".
											  "VALUES(%d,%d,%d)",
											  $idActividad,
											  $this->PrepareParamSQL($participantes[$i]),
											  $this->userIntranet['CODIGO']);
							if( $this->conn->Execute($ins_st) === false )
								print $this->conn->ErrorMsg();
						}
					}else{
						$ins_st = sprintf("INSERT INTO CalActiParticipante(id_actividad,id_trabajador,usr_audit) ".
										  "VALUES(%d,%d,%d)",
										  $idActividad,
										  $this->userIntranet['CODIGO'],
										  $this->userIntranet['CODIGO']);
						if( $this->conn->Execute($ins_st) === false )
							print $this->conn->ErrorMsg();
					}
					
					$this->VistaDiaria(substr($fecInicio,0,10));
				}
			}
		}
	}

	function ActividadesDiaEntero($fecha){
		// Obtiene las Actividades del Todo el Dia
		$sql_st = "SELECT a.id_actividad, des_asunto, '(Todo el dia)' ".
				  "FROM CalendarioActividad a, CalActiParticipante p ".
				  "WHERE (a.id_actividad=p.id_actividad and p.id_trabajador={$this->userIntranet['CODIGO']}) and ".
				  		 "ind_DiaEntero IS TRUE and to_date('{$fecha}','MM/DD/YYYY') between date(fec_inicio) and date(fec_fin) ".
				  "ORDER BY fec_inicio";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$todoDia[] = array( 'id' => $row[0],
									'asunto' => $row[1],
									'time' => $row[2]
								  );
			$rs->Close();
		}
		unset($rs);
		return (is_array($todoDia)) ? $todoDia : false;
	}
	
	function ActividadesPrevioComienzo($fecha){
		// Obtiene las Actividades que empezaron en dias Anteriores
		$sql_st = "SELECT a.id_actividad, des_asunto, ".
						 "CASE WHEN to_char(fec_inicio,'MM/DD/YYYY')='{$fecha}' ".
						 	  "THEN '( Empieza Hoy a las '||to_char(fec_inicio,'HH:MIam')||', ' ".
							  "ELSE '( Empez� el ' || to_char(fec_inicio,'DD/MM/YYYY')||', ' ".
						 "END ||".
						 "CASE WHEN to_char(fec_fin,'MM/DD/YYYY')='{$fecha}' ".
						 	  "THEN 'Termina Hoy a las '||to_char(fec_fin,'HH:MIam')||' )' ".
							  "ELSE 'Termina el ' || to_char(fec_fin,'DD/MM/YYYY')||' )' ".
						 "END ".
				  "FROM CalendarioActividad a, CalActiParticipante p ".
				  "WHERE (a.id_actividad=p.id_actividad and p.id_trabajador={$this->userIntranet['CODIGO']}) and ".
				  		"ind_DiaEntero IS FALSE and ".
						"( to_date('{$fecha}','MM/DD/YYYY')>date(fec_inicio) and to_date('{$fecha}','MM/DD/YYYY')<=date(fec_fin) ) ".
						"ORDER BY fec_inicio";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$diaAnterior[] = array('id' => $row[0],
									   'asunto' => $row[1],
									   'time' => $row[2]
									  );
			$rs->Close();
		}
		unset($rs);
		return (is_array($diaAnterior)) ? $diaAnterior : false;
	}
	
	function ActividadesDiaParticular($fecha){
		$sql_st = "SELECT a.id_actividad, ".
						 "des_asunto, to_char(fec_inicio,'HH:MIam') || ' - ' || ".
						 "CASE WHEN to_char(fec_fin,'MM/DD/YYYY')='{$fecha}' ".
						      "THEN to_char(fec_fin,'HH:MIam') ".
							  "ELSE to_char(fec_fin,'DD/MM/YYYY') ".
						 "END, ".
						 "to_number(to_char(fec_inicio,'HH24'),'99'), ".
						 "CASE WHEN to_number(to_char(fec_inicio,'MI'),'99')<29 ".
						      "THEN 1 ".
							  "ELSE 2 ".
						 "END ".
				  "FROM CalendarioActividad a, CalActiParticipante p ".
				  "WHERE (a.id_actividad=p.id_actividad and p.id_trabajador={$this->userIntranet['CODIGO']}) and ".
						 "ind_DiaEntero IS FALSE and to_char(fec_inicio,'MM/DD/YYYY')='{$fecha}' and ".
						 "to_char(fec_fin,'MM/DD/YYYY')>='{$fecha}' ".
				  "ORDER BY fec_inicio";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$actividades["{$row[3]}"]["{$row[4]}"][] = array('id' => $row[0],
									   						     'asunto' => $row[1],
									   						     'time' => $row[2]
									  					    );
			$rs->Close();
		}
		unset($rs);
		return (is_array($actividades)) ? $actividades : false;
	}
	
	function VistaDiaria(){
		// Abre la Conexion a la DB
		$this->abreConnDB();
		// Obtiene las Actividades del Todo el Dia
		$todoDia = $this->ActividadesDiaEntero($this->fecha);
		
		// Obtiene las Actividades que empezaron en dias Anteriores
		$diaAnterior = $this->ActividadesPrevioComienzo($this->fecha);
		
		// Obtiene las Actividades correspondientes solo a la Fecha Indicada
		$actividades = $this->ActividadesDiaParticular($this->fecha);
		for($i=$this->HoraIniVistaDiaria;$i<=$this->HoraFinVistaDiaria;$i++){
			if($actividades){
				if (array_key_exists($i, $actividades)) {
					if(!array_key_exists(1, $actividades[$i]))
						$actividades[$i][1] = false;
					if(!array_key_exists(2, $actividades[$i]))
						$actividades[$i][2] = false;
				}else
					$actividades[$i] = array(1 => false, 2 => false);
			}else
				$actividades[$i] = NULL;
		}
		ksort ($actividades);
		reset ($actividades);
		
		// Crea el Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign('appUrl',$_SERVER['PHP_SELF']);
		$html->assign('frmUrl',$this->modInfo['PATH']);
		$html->assign_by_ref('date',$this->fecha);
		
		// Setea la Variable el el Template HTML
		$html->assign('fechaAnt',$this->DiaContiguo($this->fecha,true));
		$html->assign('fechaPos',$this->DiaContiguo($this->fecha));
		$html->assign_by_ref('todoDia',$todoDia);
		$html->assign_by_ref('diaAnterior',$diaAnterior);
		$html->assign_by_ref('actividades',$actividades);
		
		$html->assign_by_ref('horaIni',$this->HoraIni);
		$html->assign_by_ref('horaFin',$this->HoraFin);
		$html->assign_by_ref('horaDiaIni',$this->HoraIniVistaDiaria);
		$html->assign_by_ref('horaDiaFin',$this->HoraIniVistaDiaria);
		$html->assign('fecha',strtoupper(strftime('%A, %d DE %B DE %Y',strtotime($this->fecha))));
		
		$html->display('calendario/vistaDiaria.tpl.php');
	}
	
	
	function ActividadDetalle($id){
		$sql_st = "SELECT id_actividad, UPPER(des_asunto), des_ubicacion, des_actividad, to_char(fec_inicio,'MM/DD/YYYY'), ".
						 "CASE WHEN ind_diaentero IS TRUE THEN '(Todo el d�a)' ELSE to_char(fec_inicio,'HH12:MI am') END, ".
						 "to_char(fec_fin,'MM/DD/YYYY'), to_char(fec_fin,'HH12:MI am'), ind_diaentero, ".
						 "t.id_trabajador, t.des_apellidos || ' ' || t.des_nombres, ".
						 "to_char(a.fec_creacion,'DD/MM/YYYY HH12:MI am'), ind_privado ".
				  "FROM trabajador t, CalendarioActividad a ".
				  "WHERE t.id_trabajador=a.id_trabajador and id_actividad={$id} and id_actividad=(SELECT id_actividad ".
				  																					"FROM CalActiParticipante ".
																									"WHERE id_actividad={$id} and id_trabajador={$this->userCal})";
		//echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if ($row = $rs->FetchRow()){
				$actividad = array(id=>$row[0],
								   asunto=>$row[1],
								   ubicacion=>($row[2]) ? $row[2] : false,
								   'actividad'=>($row[3]) ? nl2br($row[3]) : false,
								   'inicio'=>ucfirst(strftime('%A, %d de %B de %Y ',strtotime($row[4]))).$row[5],
								   'fin'=>($row[8]=='t') ? false : ucfirst(strftime('%A, %d de %B de %Y ',strtotime($row[6]))).$row[7],
								   'idCreador'=>$row[9],
								   'creador'=>$row[10],
								   'fecCreado'=>$row[11],
								   'privado'=>($row[12]=='t') ? true : false
								  );
				unset($row);
				// Ejecuta Statement para localizar los participantes de la Actividad
				$sql_st =  "SELECT substr(des_apellidos,1,strpos(des_apellidos,' ')-1)||', '||substr(des_nombres,1,strpos(des_nombres,' ')-1) ".
							"FROM trabajador t, CalActiParticipante p ".
							"WHERE t.id_trabajador=p.id_trabajador and id_actividad={$id} ".
							"ORDER BY 1"; 
				$rs_part = & $this->conn->Execute($sql_st);
				unset($sql_st);
				if (!$rs_part)
					print $this->conn->ErrorMsg();
				else{
					while($row=$rs_part->FetchRow())
						$participantes[] = $row[0];
					$rs_part->Close();
				}
				$rs->Close();
			}

			$html = new Smarty;
			
			// Setea Caracteristicas en el Formulario
			$frmName = 'frmModActi';
			$html->assign_by_ref('accion',$this->arr_accion);
			$html->assign_by_ref('frmName',$frmName);
			$html->assign('frmUrl',$this->modInfo['PATH']);
			$html->assign_by_ref('date',$this->fecha);
			// Setea Campos a Mostrar
			$html->assign_by_ref('act',$actividad);
			$html->assign_by_ref('parts',$participantes);
			$html->assign('showSubmit',($this->userCal==$actividad['idCreador']) ? true : false);
			$html->display('calendario/frmMuestraActividad.tpl.php');
			
		}
		unset($rs);
	}
	
	function VistaSemanal(){
		// Forma el Array Semanal
		$wDia = date('w',mktime(0,0,0,$this->mesFecha,$this->diaFecha,$this->anyoFecha));
		$wDia = ($wDia==0) ? 7 : $wDia;
		$fDia = date('j',mktime(0,0,0,$this->mesFecha,$this->diaFecha-($wDia-1),$this->anyoFecha));
		$fMes = date('n',mktime(0,0,0,$this->mesFecha,$this->diaFecha-($wDia-1),$this->anyoFecha));
		$fAnyo = date('Y',mktime(0,0,0,$this->mesFecha,$this->diaFecha-($wDia-1),$this->anyoFecha));
		
		// Lista las Actividades de la Semana que duran Solamente 1 dia
		$sql_st = "SELECT a.id_actividad, ".
						 "des_asunto, to_char(fec_inicio,'HH:MIam') || '-' || ".
						 "CASE WHEN date(fec_inicio)=date(fec_fin) ".
							  "THEN to_char(fec_fin,'HH:MIam') ".
							  "ELSE to_char(fec_fin,'DD/MM/YYYY') ".
						 "END, ".
						 "to_number(to_char(fec_inicio,'HH24'),'99'), ".
						 "CASE WHEN to_number(to_char(fec_inicio,'MI'),'99')<29 ".
							  "THEN 1 ".
							  "ELSE 2 ".
						 "END, ".
						 "date_part('dow',fec_inicio) ".
				  "FROM CalendarioActividad a, CalActiParticipante p ".
				  "WHERE (a.id_actividad=p.id_actividad and p.id_trabajador={$this->userIntranet['CODIGO']}) and ".
						 "ind_DiaEntero IS FALSE and date(fec_inicio) between to_date('".date('m/d/Y',mktime(0,0,0,$fMes,$fDia,$fAnyo))."','MM/DD/YYYY') and ".
						 "to_date('".date('m/d/Y',mktime(0,0,0,$fMes,$fDia+6,$fAnyo))."','MM/DD/YYYY') and date(fec_inicio)=date(fec_fin) ".
				  "ORDER BY fec_inicio";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$semana[$row[5]]['actividades'][] = array('id' => $row[0],
																 'asunto' => (strlen($row[1])>20) ? substr($row[1],0,20).'...' : $row[1],
																 'time' => $row[2]
													);
			$rs->Close();
		}
		
		for($i=$fDia,$j=1;$i<($fDia+7);$i++,$j = ($j==6) ? 0 : $j+1){
			$fecha = date('m/d/Y',mktime(0,0,0,$fMes,$i,$fAnyo));
			$semana[$j]['fecha'] = ucfirst(strftime('%A, %d de %B de %Y',mktime(0,0,0,$fMes,$i,$fAnyo)));
			// Obtiene las Actividades del Todo el Dia
			$sql_st = "SELECT a.id_actividad, des_asunto, date_part('dow',to_date('{$fecha}','MM/DD/YYYY')), ".
							 "CASE WHEN ind_diaentero IS FALSE and date(fec_inicio)=to_date('{$fecha}','MM/DD/YYYY') THEN 'a'||date_part('hour',fec_inicio) ".
							 	  "WHEN ind_diaentero IS FALSE and date(fec_fin)=to_date('{$fecha}','MM/DD/YYYY') THEN 'd'||date_part('hour',fec_fin) ".
								  "ELSE NULL ".
							 "END ".
					  "FROM CalendarioActividad a, CalActiParticipante p ".
					  "WHERE (a.id_actividad=p.id_actividad and p.id_trabajador={$this->userIntranet['CODIGO']}) and ".
							 "to_date('{$fecha}','MM/DD/YYYY') between date(fec_inicio) and date(fec_fin) and ".
							 "((ind_diaentero is FALSE and date(fec_inicio)<date(fec_fin)) or  ind_diaentero is TRUE) ".
					  "ORDER BY ind_diaentero DESC, fec_inicio ASC";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				while ($row = $rs->FetchRow())
					$semana[$row[2]]['todoDia'][] = array('id' => $row[0],
														  'asunto' => (strlen($row[1])>20) ? substr($row[1],0,20).'...' : $row[1],
														  'hora' => $row[3]
														);
				$rs->Close();
			}
			unset($rs);
		}

		// Crea el Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign('appUrl',$_SERVER['PHP_SELF']);
		$html->assign('frmUrl',$this->modInfo['PATH']);
		$html->assign_by_ref('date',$this->fecha);
		
		// Setea la Variable el el Template HTML
		$html->assign('diaIni',date('d.m.Y',mktime(0,0,0,$fMes,$fDia,$fAnyo)));
		$html->assign('diaFin',date('d.m.Y',mktime(0,0,0,$fMes,($fDia+6),$fAnyo)));
		$html->assign('fechaAnt',date('m/d/Y',mktime(0,0,0,$fMes,($fDia-1),$fAnyo)));
		$html->assign('fechaPos',date('m/d/Y',mktime(0,0,0,$fMes,($fDia+7),$fAnyo)));
		$html->assign_by_ref('semana',$semana);
		
		$html->display('calendario/vistaSemanal.tpl.php');

	}
}
?>
