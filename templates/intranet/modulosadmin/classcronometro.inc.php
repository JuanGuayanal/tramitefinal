<?php
      Class cronometro {

      var $comienzo;

       

      function getMicrotime() {

      list($milisegundos, $segundos) = explode(" ", microtime());

      return ( (float) $milisegundos + (float) $segundos );

      }


      # constructor cronometro

      function cronometro() {

      $this->comienzo = $this->getMicrotime();

      return true;

      }

       

      # para el cronometro y devuelve el tiempo

      # se puede dar una salida formateada a traves de los parametros.

      # Si $formatear esta a verdadero entonces devolvera cuantos segundos

      # se demoro con $nroDecimales decimales (milisegundos).

      function stop($formatear = false, $nroDecimales = 0) {

      $tiempo = $this->getMicrotime() - $this->comienzo;

      return ( $formatear ) ? number_format( $tiempo, $nroDecimales, ',', '.') : $tiempo;

      }

      }
	  ?>