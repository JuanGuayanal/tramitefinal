<?
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class DatosSGS extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function DatosSGS($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_AGREGA_ATENCION =>'frmAddAtention',
							AGREGA_ATENCION => 'addAtention',
							FRM_BUSCA_DATOS =>'frmSearchDatos',
							BUSCA_DATOS =>'searchDatos',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchDatos', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmAddAtention', label => 'AGREGAR' )
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		$this->pathTemplate = 'oti/sgs/';		
    }
	
	function MuestraStatTrans($accion){

		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_DATOS]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormBuscaDatosSGS();
		//$this->XMLEmbarcacion();
	}
	
	function XMLEmbarcacion(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$a=htmlspecialchars("<")."embarcaciones".htmlspecialchars(">");
				
		$sql_st = "SELECT id_emb, nombre_emb, substring(nombre_emb,1,1), matricula_emb
  FROM db_dnepp.user_dnepp.embarcacionnac
  ORDER BY 2";
	
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						/*while($AtencionData = $rs->FetchRow())
							$a.="<".$AtencionData[2]."><puerto><id>".$AtencionData[0]."</id><nombre>".$AtencionData[1]."</nombre></puerto></A>";
						$rs->Close();*/
						
						$ii=-1;
						while(!$rs->EOF){
							$ii++;
							$codigoPuerto[$ii]=$rs->fields[0];
							$descripcionPuerto[$ii]=$rs->fields[1];
							$firstCharPuerto[$ii]=$rs->fields[2];
							$matricula[$ii]=$rs->fields[3];
							
							if($ii==0){
								//$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[0].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
								$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[0]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[0]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
							}else{
								if($firstCharPuerto[$ii]!=$firstCharPuerto[$ii-1]){
									$a.=htmlspecialchars("</").$firstCharPuerto[$ii-1].htmlspecialchars(">");
									//$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}else{
									//$a.=htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}		
							}
							
							$rs->MoveNext();
						}
						$rs->Close();						
						
					}
		$contador=count($firstCharPuerto);			
		$a.=htmlspecialchars("</").$firstCharPuerto[$contador-1].htmlspecialchars(">");
		//$a.=htmlspecialchars("</")."puertos".htmlspecialchars(">");
		$a.=htmlspecialchars("</")."embarcaciones".htmlspecialchars(">");
		echo "<br>".$a;													
	
	
		/*$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('emb',array('id'=>$row[0],
										  'nomb'=>$row[1],
										  'matr'=>$row[2]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEmbarcaciones.tpl.php');*/
	
	}	
	
	function FormBuscaDatosSGS($page=NULL,$desEmbarcacion=NULL,$idEmbarcacion=NULL,$desEspecie=NULL,$idEspecie=NULL,$desDestino=NULL,$idDestino=NULL,
							   $desPuerto=NULL,$idPuerto=NULL,$desEstablecimiento=NULL,$idEstablecimiento=NULL,$nroTolva=NULL,$nroReporte=NULL,$statusDescarga=NULL,
							   $desFechaIni=NULL,$desFechaFin=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('desEmbarcacion',$desEmbarcacion);
		$html->assign_by_ref('idEmbarcacion',$idEmbarcacion);
		$html->assign_by_ref('desEspecie',$desEspecie);
		$html->assign_by_ref('idEspecie',$idEspecie);
		$html->assign_by_ref('desDestino',$desDestino);
		$html->assign_by_ref('idDestino',$idDestino);
		$html->assign_by_ref('desPuerto',$desPuerto);
		$html->assign_by_ref('idPuerto',$idPuerto);
		$html->assign_by_ref('desEstablecimiento',$desEstablecimiento);
		$html->assign_by_ref('idEstablecimiento',$idEstablecimiento);
		$html->assign_by_ref('nroTolva',$nroTolva);
		$html->assign_by_ref('nroReporte',$nroReporte);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		
		/*$html->assign('datos',compact('idEmbarcacion',
									  'desEmbarcacion',
									  'idEspecie',
									  'desEspecie',
									  'idDestino',
									  'desDestino',
									  'idPuerto',
									  'desPuerto',
									  'idEstablecimiento',
									  'desEstablecimiento'));		*/
		
		// Contenido Select del T�CNICO
		$sql_st = "SELECT id_estado_sgs,descripcion ".
					"FROM DB_GENERAL.DBO.ESTADO_SGS ".
					"order by 2";
		$html->assign_by_ref('selStatusDescarga',$this->ObjFrmSelect($sql_st, $statusDescarga, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT id,razon_social ".
					"FROM DB_GENERAL.DBO.EIPs ".
					"order by 2";
		$html->assign_by_ref('selEIP',$this->ObjFrmSelect($sql_st, $eip, true, true, array('val'=>'none','label'=>'Todas las Eip')));
		unset($sql_st);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT CODIGO,DESCRIPCION ".
					"FROM DB_GENERAL.DBO.ESPECIES ".
					"order by 2";
		$html->assign_by_ref('selEspecieDescargada',$this->ObjFrmSelect($sql_st, $especieDescargada, true, true, array('val'=>'none','label'=>'Todas las especies')));
		unset($sql_st);

		// Contenido Select del Dependencia
		$sql_st = "SELECT CODIGO,DESCRIPCION ".
					"FROM db_generaL.DBO.DESTINO_RRS ".
					"order by 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $destino, true, true, array('val'=>'none','label'=>'Todas los destinos')));
		unset($sql_st);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT CODIGO,DESCRIPCION ".
					"FROM db_generaL.DBO.puertoS ".
					"order by 2";
		$html->assign_by_ref('selPuerto',$this->ObjFrmSelect($sql_st, $puerto, true, true, array('val'=>'none','label'=>'Todas los puertos')));
		unset($sql_st);
		
		//$a=htmlspecialchars("<")."puertos".htmlspecialchars(">");
		$a=htmlspecialchars("<")."establecimientos".htmlspecialchars(">");
		/*$sql_st = "SELECT CODIGO,DESCRIPCION,substring(descripcion,1,1) ".
					"FROM db_generaL.DBO.puertoS ".
					"order by 2";/**/
		/**/$sql_st = "sELECT ID,RAZON_SOCIAL,substring(RAZON_SOCIAL,1,1) 
					FROM DB_GENERAL.DBO.EIPS
					order by 2";					/**/
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						/*while($AtencionData = $rs->FetchRow())
							$a.="<".$AtencionData[2]."><puerto><id>".$AtencionData[0]."</id><nombre>".$AtencionData[1]."</nombre></puerto></A>";
						$rs->Close();*/
						
						$ii=-1;
						while(!$rs->EOF){
							$ii++;
							$codigoPuerto[$ii]=$rs->fields[0];
							$descripcionPuerto[$ii]=$rs->fields[1];
							$firstCharPuerto[$ii]=$rs->fields[2];
							
							if($ii==0){
								//$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[0].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
								$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[0]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
							}else{
								if($firstCharPuerto[$ii]!=$firstCharPuerto[$ii-1]){
									$a.=htmlspecialchars("</").$firstCharPuerto[$ii-1].htmlspecialchars(">");
									//$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
								}else{
									//$a.=htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
								}		
							}
							
							$rs->MoveNext();
						}
						$rs->Close();						
						
					}
		$contador=count($firstCharPuerto);			
		$a.=htmlspecialchars("</").$firstCharPuerto[$contador-1].htmlspecialchars(">");
		//$a.=htmlspecialchars("</")."puertos".htmlspecialchars(">");
		$a.=htmlspecialchars("</")."establecimientos".htmlspecialchars(">");
		//echo "<br>".$a;												

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search2.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function BuscaDatosSGS($page,$desEmbarcacion,$idEmbarcacion,$desEspecie,$idEspecie,$desDestino,$idDestino,
							   $desPuerto,$idPuerto,$desEstablecimiento,$idEstablecimiento,$nroTolva,$nroReporte,$statusDescarga,
							   $desFechaIni,$desFechaFin){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDatosSGS($page,$desEmbarcacion,$idEmbarcacion,$desEspecie,$idEspecie,$desDestino,$idDestino,
							   $desPuerto,$idPuerto,$desEstablecimiento,$idEstablecimiento,$nroTolva,$nroReporte,$statusDescarga,
							   $desFechaIni,$desFechaFin,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		$idEmbarcacion = explode (";", $idEmbarcacion);
		$idEmb=$idEmbarcacion[0];
		//$condConsulta[] = "r.usuario=t.email";
		//echo $idEmb."xx";
		if($idEmb>0){
			$condConsulta[] = "d.cod_emb=$idEmb ";				
		}
		if($idEspecie>0){
			$condConsulta[] = "d.codigo_especie='$idEspecie' ";
		}
		if($idDestino>0){
			$condConsulta[] = "d.codigo_destino='$idDestino' ";
		}
		if($idPuerto>0){
			$condConsulta[] = "d.codigo_puerto=$idPuerto ";
		}
		if($idEstablecimiento>0){
			$condConsulta[] = "d.codigo_eip=$idEstablecimiento ";
		}
		if($statusDescarga>0)
			$condConsulta[] = "d.id_estado_sgs=$statusDescarga";
		if($nroTolva>0)
			$condConsulta[] = "d.codigo_tolva=$nroTolva";
		if($nroReporte && $nroReporte!="")
			$condConsulta[] = "d.numero_reporte='$nroReporte'";
		if($desFechaIni && $desFechaIni!="")
			$condConsulta[] = "convert(datetime,d.fecha,103)>=convert(datetime,'$desFechaIni',103)";
		if($desFechaFin && $desFechaFin!="")
			$condConsulta[] = "convert(datetime,d.fecha,103)<dateadd(dd,1,convert(datetime,'$desFechaFin',103))";
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		$this->conn->debug = true;
		
		$sql_SP = sprintf("EXECUTE sp_busIDDatosSGS %s,%s,0,0",
							//($nopc) ? "'".$this->PrepareParamSQL($nopc)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDatosSGS %s,%s,%s,%s",
								//($nopc) ? "'".$this->PrepareParamSQL($nopc)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosSGS %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($AtencionData = $rs->FetchRow())
							$html->append('arrAte', array('id' => $id[0],
														  'eip' => ucfirst($AtencionData[1]),
														  'fecha' => $AtencionData[2],
														  'tolva' => ucwords($AtencionData[3]),
														  'especie' => strtoupper($AtencionData[4]),
														  'destino' => strtoupper($AtencionData[5]),
														  'cant' => strtoupper($AtencionData[6]),
														  'desc' => ucfirst($AtencionData[7]),
														  'horaini' =>$AtencionData[8],
														  'horafin' =>$AtencionData[9],
														  'puerto' => ucwords($AtencionData[10]),
														  'emb' => ucwords($AtencionData[11]),
														  'nroReporte' => strtoupper($AtencionData[12]),
														  'status' => strtoupper($AtencionData[13]),
														  'matriEmb' => strtoupper($AtencionData[14]),
														  'hora' =>$AtencionData[15],
														  'dia' =>$AtencionData[16],
														  //'horaini' =>substr($AtencionData[17],0,16),
														  //'horafin' =>substr($AtencionData[18],0,16),
														  'pc' =>$AtencionData[19]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DATOS], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function FormAgregaAtencion($nopc=NULL,$tipo1=NULL,$tipo2=NULL,$tipo3=NULL,$aten1=NULL,$aten2=NULL,$aten3=NULL,$stt=NULL,$obs=NULL,$tiempo=NULL,$errors=false){
		global $nombrePC;
		$nombrePC=($_POST['nombrePC']) ? $_POST['nombrePC'] : $_GET['nombrePC'];		
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nopc',$nopc);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('tiempo',$tiempo);
		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('nombrePC',$nombrePC);
		$html->assign_by_ref('nombreFisicoPC',$nombreFisicoPC);
		
		$html->assign_by_ref('nombreRealPC',$nombreRealPC);
		$html->assign_by_ref('error',$error);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo1',$this->ObjFrmSelect($sql_st, $tipo1, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo1) ? $tipo1 : 0);
		$html->assign_by_ref('selAten1',$this->ObjFrmSelect($sql_st, $aten1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo2 de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo2',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select 2del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo2) ? $tipo2 : 0);
		$html->assign_by_ref('selAten2',$this->ObjFrmSelect($sql_st, $aten2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo3',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo3) ? $tipo3 : 0);
		$html->assign_by_ref('selAten3',$this->ObjFrmSelect($sql_st, $aten3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddAtention.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
}
?>
