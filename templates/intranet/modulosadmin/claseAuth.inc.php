<?
require_once('modulosadmin/claseGeneral.inc.php');

class Auth extends General{
	var $sessionName = 'intProd';
	var $maxSessionTime = 14400;
	var $pageSessionExpira = 'expiration.php';
	
	// Define parametros del Servidor IMAP
	var $imapHost = 'correo.midis.gob.pe';
	var $imapPort = 143;

	function BeginSession(){
		if(!isset($_SESSION)){
			session_name($this->sessionName);
			session_start();
			$_SESSION['session_max_time'] = $this->maxSessionTime;
		}
		if(!$this->StatusSecure()){
			session_unset();
			session_destroy();
		}
	}
	
	function DestroySession(){
		session_unset();
		session_destroy();
		$destination = "https://" . $_SERVER['HTTP_HOST'] . "/";
		header("Location: $destination");
		exit;
	}
	
	function StatusSecure(){
		if(array_key_exists('secure_pas',$_SESSION))
			return true;
		else
			return false;	
	}
	
	function EntryRigth(){
		if(!$this->StatusSecure()){
			$destination = "https://" . $_SERVER['HTTP_HOST'] . "/";
			header("Location: $destination");
			exit;
		}
		if($this->TimeSessionLife())
			$this->DestroySessionByTime();
		$this->GetModRights();
	}
	
	function GetModRights(){
		// Obtiene el PATH de la Pagina visitada
		$url = $_SERVER['PHP_SELF'];
		ereg("/([0-9a-z]{32})", $_SERVER['PHP_SELF'], $regs);
		$path = dirname(str_replace($regs[0], "", $url))."/";
		
		// Define los derechos iniciales
		$_SESSION['modulo_id'] = NULL;
		$_SESSION['mod_ind_leer'] = false;
		$_SESSION['mod_ind_insertar'] = false;
		$_SESSION['mod_ind_modificar'] = false;

		// Almacena en Variables Temporales las Variables de la Clase
		$statConn = $this->statConn;
		$typeDB = $this->typeDB;
		$DB = $this->DB;
		$userDB = $this->userDB;
		$passDB = $this->passDB;

		// Setea Parametros para la Conexion PostgreSQL
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][0];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
							
		$this->abreConnDB();
		
		// Obtiene los Grupos a los q pertenece el Usuario
		$sql_st = "SELECT DISTINCT(gu.id_grupo) ".
				  "FROM grupo g, grupo_usuario gu ".
				  "WHERE g.id_grupo=gu.id_grupo and gu.cod_usuario='{$_SESSION['cod_usuario']}' and g.ind_activo IS true";
		$rs = $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow()) {
				$cods_grupos[] = $row[0];
			}
			//$_SESSION['cods_grupos'] = implode(",",$cods_grupos);
			unset($row);
			$rs->Close();
		}
		unset($rs);
			
		// Obtiene el ID del modulo que contiene la pagina visitada
		$sql_st = "SELECT id_modulo ".
				  "FROM modulo ".
				  "WHERE des_path='$path' and ind_activo IS true";
		$rs = $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$_SESSION['modulo_id'] = $row[0];
				// Obtiene los derechos del Usuario o Grupos a los que pertenece
				// sobre el Modulo Visitado
				$sql_ders_st = "SELECT ind_leer, ind_insertar, ind_modificar ".
							   "FROM modulo_derecho ".
							   "WHERE id_modulo={$row[0]} and (cod_usuario='{$_SESSION['cod_usuario']}'";
				$sql_ders_st .= (!is_null($_SESSION['cods_grupos'])) ? " or id_grupo IN ({$_SESSION['cods_grupos']})" : "";
				$sql_ders_st .= ")";
				$rs_ders = $this->conn->Execute($sql_ders_st);
				unset($sql_ders_st);
				if (!$rs_ders){
					print $this->conn->ErrorMsg();
				}else{
					while ($row_ders = $rs_ders->FetchRow()) {
						if($row_ders[0]=='t')
							$_SESSION['mod_ind_leer'] = true;
						if($row_ders[1]=='t')
							$_SESSION['mod_ind_insertar'] = true;
						if($row_ders[2]=='t')
							$_SESSION['mod_ind_modificar'] = true;
					}
					unset($row_ders);
					$rs_ders->Close();
				}
				unset($rs_ders);
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Almacena las Variables Temporales en las Variables de la Clase
		$this->statConn = $statConn;
		$this->typeDB = $typeDB;
		$this->DB = $DB;
		$this->userDB = $userDB;
		$this->passDB = $passDB;
		
		if($this->statConn){
			$this->CierraConnDB();
			$this->abreConnDB();
		}else{
			$this->statConn = true;
			$this->CierraConnDB();
		}
		
	}
	
	// Entrega True si el tiempo ha expirado y False si la session continuara viva
	function TimeSessionLife_09112013(){
		list($usec, $sec) = explode(" ",microtime()); 
		$time_end = (float)$usec + (float)$sec; 
		if( ( $time_end - $_SESSION['time_start'] ) > $this->maxSessionTime ){		
			return true;
		}else{
			$_SESSION['time_start']=$time_end;
			return false;
		}
	}
	
		function TimeSessionLife(){
			return false;
		}
	
	function DestroySessionByTime(){
		session_unset();
		session_destroy();	
		$destination = "https://" . $_SERVER['HTTP_HOST'] . "/".$this->pageSessionExpira;
		header("Location: $destination");
		exit;
	}
	
	function Autentifica(){
		$username = strtolower($username);
		if(empty($username)||empty($userpass))
			return false;
		$construc = '{' . $this->imapHost . ':' . $this->imapPort . '}';
		$conn_imap = @imap_open($construc, $username, $userpass, OP_HALFOPEN);
		if(!$conn_imap){
			return false;
		}else{
			imap_close($conn_imap);
			
			// Crea Session ID Aleatorio e Inicia la Session
			srand((double)microtime()*1000000);
			$session_id = md5(uniqid(rand()));
			session_id($session_id);
			session_start();
			$_SESSION['secure_pas']=1;
			$_SESSION['cod_usuario']="$username";
			// Setea el Tiempo de Inicio de la sesion
			list($usec, $sec) = explode(" ",microtime());
			$_SESSION['time_start'] = (float)$usec + (float)$sec;
			
			// Almacena en Variables Temporales las Variables de la Clase
			$statConn = $this->statConn;
			$typeDB = $this->typeDB;
			$DB = $this->DB;
			$userDB = $this->userDB;
			$passDB = $this->passDB;

			$this->statConn = false;
			$this->serverDB = $this->arr_serverDB['mssql'][2];
			$this->typeDB = $this->arr_typeDB['mssql'];
			$this->DB = $this->arr_DB['mssql'][0];
			$this->userDB = $this->arr_userDB['mssql'][0];
			$this->passDB = $this->arr_passDB['mssql'][0];

			// Abre la Conexion a la DB MSSQL
			$this->abreConnDB();
			$sql_st = "SELECT d.dependencia, idprofesion, t.condicion, nombres_trabajador, ".
					  "apellidos_trabajador, fecha_nacimiento, dni, telefono, publicado, estado ".
					  "FROM dbo.h_trabajador t, dbo.h_dependencia d ".
					  "WHERE t.email='$username' and t.coddep_pub=d.codigo_dependencia";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				if ($row = $rs->FetchRow()) {
					($row[1]==NULL) ? $id_profesion=6 : $id_profesion=$row[1];
					($row[9]=='ACTIVO') ? $ind_activo=1 : $ind_activo=0;

					// Setea Parametros para la Conexion PostgreSQL
					$this->statConn = false;
					$this->typeDB = $this->arr_typeDB['postgres'];
					$this->DB = $this->arr_DB['postgres'][0];
					$this->userDB = $this->arr_userDB['postgres'][0];
					$this->passDB = $this->arr_passDB['postgres'][0];
					
					$this->abreConnDB();

					$sql_st = "SELECT id_condicion ".
							  "FROM trabajador_condicion ".
							  "WHERE des_condicion='" . $row[2] ."'";
					$rs_aux = $this->conn->Execute($sql_st);
					unset($sql_st);
					if (!$rs_aux){
						print $this->conn->ErrorMsg();
					}else{
						if ($row_aux = $rs_aux->FetchRow()) {
							$id_condicion = $row_aux[0];
							unset($row_aux);
						}
						$rs_aux->Close();
					}
					unset($rs_aux);
						
					$sql_st = "SELECT id_subdependencia ".
							  "FROM subdependencia sd, dependencia d ".
							  "WHERE d.des_dependencia='{$row[0]}' and sd.id_dependencia=d.id_dependencia and sd.ord_subdependencia=1";

					$rs_aux = $this->conn->Execute($sql_st);
					unset($sql_st);
					if (!$rs_aux){
						print $this->conn->ErrorMsg();
					}else{
						if ($row_aux = $rs_aux->FetchRow()) {
							$upd_st_usr = "UPDATE trabajador set id_subdependencia={$row_aux[0]}, id_profesion=${id_profesion}, ".
										  "id_condicion=${id_condicion}, des_nombres='{$row[3]}', des_apellidos='{$row[4]}', ".
										  "fec_nacimiento=to_date('{$row[5]}','DD/MM/YYYY'), des_dni='{$row[6]}', ".
										  "des_telefono='{$row[7]}', ind_publicado='{$row[8]}', fec_modificacion=now(), ".
										  "ind_activo='${ind_activo}', des_password='". md5("$username"."$userpass") ."' ".
										  "WHERE cod_trabajador='$username'";

							$ins_st_usr = "INSERT INTO trabajador(cod_trabajador, des_password, id_subdependencia, id_profesion, id_condicion, des_nombres, des_apellidos, ".
										  "fec_nacimiento, des_dni, des_telefono, ind_publicado, ind_activo, usr_audit) ".
										  "values('${username}', '". md5("$username"."$userpass") ."', {$row_aux[0]}, ${id_profesion}, {$id_condicion}, '{$row[3]}', '{$row[4]}', ".
										  "to_date('{$row[5]}','DD/MM/YYYY'), '{$row[6]}', '{$row[7]}', '{$row[8]}', '${ind_activo}', 'djara')";
						}
						$rs_aux->Close();
					}
					unset($rs_aux);
				}
				unset($row);
			}
			$rs->Close();
			
			// Actualizacion de la Tabla Usuario en la DB PostgreSQL
			$sql_st = "SELECT pas_usuario FROM usuario WHERE cod_usuario='$username'";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				if ($row = $rs->FetchRow()) {
					if($row[0] != md5("$username"."$userpass")) {
						$upd_st = "UPDATE usuario SET pas_usuario='" . md5("$username"."$userpass") . "' WHERE cod_usuario='$username'";
						if ($this->conn->Execute($upd_st) === false)
							print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
						unset($upd_st);
					}
				}else{
					$ins_st = "INSERT into usuario(cod_usuario, pas_usuario) VALUES('$username', '" . md5("$username"."$userpass") ."')";
					if ($this->conn->Execute($ins_st) === false)
						print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
					else{
						// Inserta el Usuario al Grupo CONVENIO_SITRADOC
						$ins_st = "INSERT into grupo_usuario(cod_usuario, id_grupo) VALUES('$username',3)";
						if ($this->conn->Execute($ins_st) === false)
							print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
					}
					unset($ins_st);
				}
				$rs->Close();
				unset($rs);
				
				// Inserta o Modifica el usuario en la Tabla trabajadador
				$sql_st = "select cod_trabajador from trabajador where cod_trabajador='$username'";
				$rs = & $this->conn->Execute($sql_st);
				unset($sql_st);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					if ($row = $rs->FetchRow()) {
						// mail('djara@CONVENIO_SITRADOC.gob.pe','Intranet',"${username}\r\n${upd_st_usr}");
						if(isset($upd_st_usr)){
							if ($this->conn->Execute($upd_st_usr) === false) 
								print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
							unset($upd_st_usr);
						}
					}else{
						if(!empty($ins_st_usr)){
							mail('djara@CONVENIO_SITRADOC.gob.pe','Intranet',"${username}\r\n${ins_st_usr}");
							if(isset($ins_st_usr)){
								if ($this->conn->Execute($ins_st_usr) === false) 
									print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
								unset($ins_st_usr);
							}
						}
					}
					$rs->Close();
					unset($rs);
				}
			}
			
			// Almacena las Variables Temporales en las Variables de la Clase
			$this->statConn = $statConn;
			$this->typeDB = $typeDB;
			$this->DB = $DB;
			$this->userDB = $userDB;
			$this->passDB = $passDB;
			
			if($this->statConn){
				$this->CierraConnDB();
				$this->abreConnDB();
			}else{
				$this->statConn = true;
				$this->CierraConnDB();
			}
			
			// Todo el Proceso estuvo correcto, retorna Verdadero
			return true;
		}
	}
}
?>
