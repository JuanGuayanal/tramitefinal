<?
require_once('claseModulos.inc.php');

class Onomasticos extends Modulos{

	/* ----------------------------------------------------- */
	/* Constructor											 */
	/* ----------------------------------------------------- */
	/* CLASE ONOMASTICOS									 */
	/* ----------------------------------------------------- */
	
	function Onomasticos(){
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
	}

	/* ----------------------------------------------------- */
	/* Funcion para la Muestra de los Cumplea�os de la		 */
	/* Semana Actual										 */
	/* ----------------------------------------------------- */
	/* CLASE ONOMASTICOS									 */
	/* ----------------------------------------------------- */
	
	function muestraOnomasticoSemanal() {
		$dotw = date('w');
		if($dotw == 0) $dotw = 7;
		$op_f = $dotw-1;
		$op_l = 7-$dotw;
		$fdotw = date ("d/n/Y", mktime (0,0,0,date("m"),date("d")-$op_f,date("Y")));
		$ldotw = date ("d/n/Y", mktime (0,0,0,date("m"),date("d")+$op_l,date("Y")));
	
		// Busqueda de las personas q cumplen a�os en la semana actual
		$this->abreConnDB();
		// $this->conn->debug = true;
		$sql_st="select c.siglas, b.apellidos_trabajador, b.nombres_trabajador, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
				"from dbo.h_trabajador b, dbo.h_dependencia c ".
				"where c.codigo_dependencia=b.coddep and ".
				"CONVERT(datetime,CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(YY,GETDATE())),103) BETWEEN CONVERT(datetime,'$fdotw',103) AND CONVERT(datetime,'$ldotw',103) and ".
				"ESTADO!='INACTIVO' and b.CONDICION!='GENERAL' ".
				"ORDER BY CONVERT(datetime,CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) + '/' + CONVERT(varchar,DATEPART(YY,GETDATE())),103)";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
	?>
	<table width="170" border="0" cellpadding="2" cellspacing="4">
	  <tr>
		<td width="170"><img src="/img/800x600/onomasticos/lb-cumplesemanal.gif" width="170" height="25"></td>
	  </tr>
<?
			$numrows = $rs->RecordCount();
			if($numrows<=0){
?>
	  <tr>
		<td class="contenido-intranet"><b>No hay Onom&aacute;sticos en esta semana.</b></td>
	  </tr>
<?
			}
			while ($row = $rs->FetchRow()) {
?>
	  <tr>
		<td>
		  <table width="170" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td class="item" colspan="2" width="170"> 
				<b><? echo ucwords(strtolower($row[1])) . " " . ucwords(strtolower($row[2])); ?></b>
			  </td>
			</tr>
			<tr> 
			  <td class="sub-item-intranet" width="80">Dependencias</td>
			  <td class="textogray" width="90"> 
				<? echo $row[0];?>
			  </td>
			</tr>
			<tr> 
			  <td class="sub-item-intranet" width="80">Fecha</td>
			  <td class="contenido-intranet" width="90"> 
				<? echo $row[3];?>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
<?
			}
			unset($row);
			$rs->Close();
		}
?>
	</table>
<?
	}
	
	/********************************************************************/
	/* Funcion para la Muestra de los Cumplea�os de la Busqueda Central */
	/* Modulo Onomasticos  										    	*/
	/********************************************************************/
	
	function muestraTablaBusqueda($nombre=NULL, $dia=NULL, $mes=NULL,$campo_fecha=NULL,$campo_fecha2=NULL,$tipoEstado){
		$frmName = "frmOno";
		$this->insertaScriptTipoBusquedaOnomastico($frmName)
		//$tipEstado=($_POST['tipEstado']) ? $_POST['tipEstado'] : $_GET['tipEstado'];
?>
<table width="531"  border="0" align="center" cellspacing="2">
  <tr>
    <td class="contenido-intranet">Permite conocer los onom�sticos de los trabajadores de CONVENIO_SITRADOC, donde puedes buscar por apellidos y nombre o por mes y d�a de nacimiento.</td>
  </tr>
</table>	
<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	<table width="531" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td>	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabla-login" bgcolor="#FDF9EE">
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7" ><label> 
              <input name="tipEstado" type="radio" value="1" <?php if ($tipoEstado==1||!$tipoEstado){ echo "checked"; }?> onClick="cargarR(document.<?=$frmName?>,1);">
              Por apellido y/o nombre </label>
              &nbsp;
			  <label> 
              <input name="tipEstado" type="radio" value="2" <?php if ($tipoEstado==2){ echo "checked";} ?> onClick="cargarR(document.<?=$frmName?>,2);">
              Por fecha </label>
			<input name="tipoEstado" type="hidden" id="tipoEstado" />
			</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7" >&nbsp;</td>
          </tr
		  ><?php if ($tipoEstado==1||!$tipoEstado){?>		  
          <tr>
            <td width="10">&nbsp;</td>
            <td class="contenido-intranet" width="150">Por Nombre y/o Apellido:</td>
            <td colspan="2"><input type="text" name="nombre" class="ip-login contenido" size="23" value="<?echo $nombre?>">
            </td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" value="Buscar" name="Submit" class="submitintranet" onClick="return tipbuscaOnomastico()"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
		  <?php }?>
		  <?php if ($tipoEstado==2){?>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido-intranet" width="150">Por Fecha:</td>
            <td width="45">Entre el</td>
            <td width="45"><input name="campo_fecha"  type="text" class="ip-login contenido" id="campo_fecha"  size="12" tabindex="4" onKeyPress="ninguna_letra();" value="<?php echo date('d/m') ; ?>" readonly=""></td>
            <td align="left">      <img src="estilos/i.p.cal.gif" width="15" height="12" id=lanzador value=...>
      <!-- script que define y configura el calendario-->
      <SCRIPT type=text/javascript>
			    Calendar.setup({
		        inputField     :    "campo_fecha",      // id del campo de texto
        		ifFormat       :    "%d/%m",       // formato de la fecha, cuando se escriba en el campo de texto
		        button         :    "lanzador"   // el id del bot&oacute;n que lanzar&aacute; el calendario
			    });
		</SCRIPT>
      <input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="" />
</td>
            <td width="45">y el</td>
            <td width="45"><input name="campo_fecha2"  type="text" class="ip-login contenido" id="campo_fecha2"  size="12" tabindex="4" onKeyPress="ninguna_letra();" value="<?php echo date('d/m') ; ?>" readonly=""></td>
            <td><img src="estilos/i.p.cal.gif" width="15" height="12" id=lanzador2 value=...>
      <!-- script que define y configura el calendario-->
      <SCRIPT type=text/javascript>
			    Calendar.setup({
		        inputField     :    "campo_fecha2",      // id del campo de texto
        		//ifFormat       :    "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
				ifFormat       :    "%d/%m",       // formato de la fecha, cuando se escriba en el campo de texto
		        button         :    "lanzador2"   // el id del bot&oacute;n que lanzar&aacute; el calendario
			    });
		</SCRIPT>
      <input name="fecDesembarqueFin" type="hidden" id="fecDesembarqueFin" value="" /></td>
            <td><input type="submit" value="Buscar" name="Submit" class="submitintranet" onClick="return tipbuscaOnomastico()">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="contenido">&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
		  <?php }?>
        </table></td>
	  </tr>
	  
	</table>
</form>
	<br>
<?
	}
	
	function muestraResultadosBusqueda($nombre, $dia, $mes,$campo_fecha,$campo_fecha2){
		global $type_db, $serverdb, $userdb, $passdb, $db, $abs_path;
		//echo "c".$campo_fecha."c".$campo_fecha2;
		
		if(!empty($nombre)){
			// Query para la busqueda de Todos los registros coincidentes con CADA UNA de las palabras ingresadas
		/*	$arr_nombre=explode(" ", str_replace("  ", " ", trim(strtolower($nombre))));
			for($i=0;$i<count($arr_nombre);$i++){
				if(!empty($arr_nombre[$i])){
					$sql_st[] =	"select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
									"from dbo.h_trabajador b, dbo.h_dependencia c ".
									"where c.codigo_dependencia=b.coddep and (LOWER(nombres_trabajador) like '%" . $arr_nombre[$i] . "%' ".
									"or LOWER(apellidos_trabajador) like '%" . $arr_nombre[$i] . "%') and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
				}
			}
			if(count($sql_st)>1){
				$sql_st = implode(" UNION ", $sql_st);
				$sql_st .= " order by b.apellidos_trabajador";
			}else{
				$sql_st = $sql_st[0];
				$sql_st .= " order by b.apellidos_trabajador";
			}
		*/
			// Query para la busqueda de Todos los registros coincidentes con TODA LA FRASE de ingresada
			$strNombre = trim(strtolower($nombre));

        	        // Evalua que no contenga sentencias SQL
	                $this->evaluaNoSql($strNombre);

			$sql_st = "select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
						"from dbo.h_trabajador b, dbo.h_dependencia c ".
						"where c.codigo_dependencia=b.coddep and (LOWER(nombres_trabajador) like '%" . $strNombre . "%' ".
						"or LOWER(apellidos_trabajador) like '%" . $strNombre . "%') and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
		//	echo $sql_st;
		}else{
			// Query para la busqueda de los Registros coincidentes con la Fecha de Nacimiento Ingresada
			$sql_st ="select c.dependencia, b.apellidos_trabajador, b.nombres_trabajador, b.email, CONVERT(varchar,DATEPART(DD,CONVERT(datetime,fecha_nacimiento,103)))+'/'+CONVERT(varchar,DATEPART(MM,CONVERT(datetime,fecha_nacimiento,103))) ".
						"from dbo.h_trabajador b, dbo.h_dependencia c ".
						//"where c.codigo_dependencia=b.coddep and DATEPART(DD, CONVERT(datetime,fecha_nacimiento,103))=${dia}".
						//" and DATEPART(MM, CONVERT(datetime,fecha_nacimiento,103))=${mes} and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL'";
						"where c.codigo_dependencia=b.coddep 
							AND b.fecha_nacimiento is not null
							AND B.FECHA_NACIMIENTO<>'29/02/1900' and (";
					$sql_st .=" (convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha."/1919',103))
							and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha2."/1919',103) )";		
							
					for($anyo=1920;$anyo<date('Y');$anyo++){
							$campo_fecha_=$campo_fecha."/".$anyo;
							$campo_fecha2_=$campo_fecha2."/".$anyo;
							$sql_st .=" or (convert(datetime,b.fecha_nacimiento,103)<dateadd(dd,1,convert(datetime,'".$campo_fecha2_."',103))
							and convert(datetime,b.fecha_nacimiento,103)>=convert(datetime,'".$campo_fecha_."',103) )";
					}		
							$sql_st .=") and ESTADO!='INACTIVO' and b.CONDICION!='GENERAL' 		
								order by DATEPART(MM, CONVERT(datetime,fecha_nacimiento,103)),DATEPART(DD, CONVERT(datetime,fecha_nacimiento,103)) DESC		
						";
		//	echo $sql_st;
		}
		
		// Ejecucion del Query
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$numrows = $rs->RecordCount();
			if($numrows>0){
	?> 
		<table width="531" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td height="1"> 
			  <hr width="100%" size="1" align="center">
			</td>
		  </tr>
		  <tr>
			<td class="contenido" valign="bottom" height="14"><img src="<?=$abs_path;?>/img/800x600/lb-resultados.gif" width="93" height="14" align="absbottom">
			<b><? echo "Se encontraron $numrows coincidencia"; if($numrows>1) echo "s";?></b>
			</td>
		  </tr>
		  <tr>
			<td>
			  <hr width="100%" size="1">
			</td>
		  </tr>
		</table>
		<br>
<?
			}else{
?>
		<table width="531" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td height="1"> 
			  <hr width="100%" size="1" align="center">
			</td>
		  </tr>
		  <tr> 
			<td><img src="<?=$abs_path;?>/img/800x600/lb-sinresultados.gif" width="239" height="14"></td>
		  </tr>
		  <tr> 
			<td> 
			  <hr width="100%" size="1">
			</td>
		  </tr>
		</table>
		<br>
<?
			}
			while ($row = $rs->FetchRow()) {
?>
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
		    <td rowspan="4" valign="top" width="40" align="center"><img src="<?=$abs_path;?>/img/800x600/onomasticos/cumple.jpg" width="27" height="27"></td>
		    <td colspan="2" class="item">&nbsp;</td>
	      </tr>
		  <tr> 
			<td colspan="2" class="item-intranet"><b><?echo $row[1];?> <?echo $row[2];?></b></td>
		  </tr>
		  <tr> 
			<td class="sub-item-intranet" width="80">Dependencias:</td>
			<td class="textogray" width="424">
			  <?echo $row[0];?>
			</td>
		  </tr>
		  <tr> 
			<td class="sub-item-intranet" width="80">Fecha:</td>
			<td class="sub-item-intranet" width="424"><b>
			  <?echo $row[4];?></b>
			</td>
		  </tr>
		  <tr>
		    <td valign="top" align="center">&nbsp;</td>
		    <td class="sub-item">&nbsp;</td>
		    <td class="contenido">&nbsp;</td>
	      </tr>
</table>
		<br>
	<?
			}
			unset($row);
			$rs->Close();
		}
	}

}	
?>
