<?
require_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class SitradocOpciones extends Modulos{

	function SitradocOpciones($menu,$subMenu,$accionM){
	
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];

		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
				
		$this->datosUsuarioMSSQL();
		$this->datosPermisoSITRADOC();
		
		if(!$_SESSION['administracion']){
			$_SESSION['administracion'] = $this->userSITRADOC['ADMINISTRACION'];
			$_SESSION['coordinador'] = $this->userSITRADOC['COORDINADOR'];
			$_SESSION['resolucion'] = $this->userSITRADOC['RESOLUCION'];
			$_SESSION['correspondencia'] = $this->userSITRADOC['CORRESPONDENCIA'];
		}

		/* Si se pierde la conexi�n al servidor no debe realizar
		   ninguna acci�n Permitida, no se instancia la Clase */ 
		/**/
		if($this->userIntranet['COD_DEP']==0){
			$inicioA=0;
			while($this->userIntranet['COD_DEP']==0){
				$this->datosUsuarioMSSQL();
				$inicioA++;
			}
		}
		/**/

		/**/
		if($this->userIntranet['COD_DEP']==0){
			$objIntranet = new Intranet();
			//$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			//$objIntranet->Body('tramite_tit.gif',false,false);
			
			//$this->errors .= 'Se ha perdido la conexi�n a este m�dulo, s�rvase ingresar nuevamente. Gracias';
			$this->errors = '<b>Se ha perdido la conexi�n a este m�dulo, s�rvase actualizar o presione F5 y Reintentar. Gracias</b><br>';
			$this->muestraMensajeInfo($this->errors);
			
			//$objIntranet->Footer();

			return $this = false;
		}
		/**/		
		
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_RESOLUCION => 'frmBuscaResol',
							BUSCA_RESOLUCION => 'buscaResol',
							FRM_MODIFICA_REFERENCIA_RESOL =>'frmModifyReferenciaResol',
							MODIFICA_REFERENCIA_RESOL =>'modifyReferenciaResol',
							FRM_ANULA_RESOLUCION => 'frmAnulaResol',
							ANULA_RESOLUCION => 'anulaResol',
							FRM_BUSCA_CORRESPONDENCIA => 'frmSearchNoti',
							BUSCA_CORRESPONDENCIA => 'searchNoti',
							FRM_NUEVO_DESTINATARIO => 'frmNewDestinatario',
							NUEVO_DESTINATARIO => 'newDestinatario',
							FRM_DISMINUYE_CONTADOR => 'frmDisminuyeContador',
							DISMINUYE_CONTADOR => 'disminuyeContador',
							AGREGA_DOCUMENTO => 'addDocumento',
							FRM_BUSCA_DOCUMENTO =>'frmSearchDoc',
							BUSCA_DOCUMENTO =>'searchDoc',
							FRM_ADJUNTA_DOCUMENTO => 'frmAdjuntaDocumento',
							ADJUNTA_DOCUMENTO => 'adjuntaDocumento',
							FRM_MODIFICA_DOCUMENTO => 'frmModifyDocumento',
							MODIFICA_DOCUMENTO => 'modifyDocumento',
							BUSCA_ADJUNTO=>'buscaAdjunto',
							CONSULTA_INTERNOS => 'consultaInternos'
							);
	}						
							
	function MuestraIndex(){
		$this->FormBuscaResolucion();
	}
	
	function FechaActual(){
		setlocale (LC_TIME, $this->zonaHoraria);
		return ucfirst(strtolower(strftime("%d/%m/%Y")));
	}
	function HoraActual(){
		$this->abreConnDB();
//		$this->conn->debug = true;

		$sql="select convert(varchar,getDate(),108)";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rs->fields[0];
			}
		//setlocale (LC_TIME, $this->zonaHoraria);
		//return ucfirst(strtolower(strftime("%T")));	
		return($hora);
	}

	function FormBuscaResolucion($page=NULL,$tipResol=NULL,$nroResol=NULL,$sumilla=NULL,$desFechaIni=NULL,$desFechaFin=NULL,$dependencia=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscaResol';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nroResol',$nroResol);
		$html->assign_by_ref('sumilla',$sumilla);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		// Contenido Select Congresista
		$sql_st = "SELECT id, SUBSTRING(lower(descrip_COMPLETA),1,20) ".
				  "FROM TIPO_RESOLUCION ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResolucion',$this->ObjFrmSelect($sql_st, $tipResol, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = "SELECT codigo_dependencia, lower(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where condicion='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/resoluciones/search.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	
	function BuscaResolucion($page,$tipResol,$nroResol,$sumilla,$desFechaIni,$desFechaFin,$dependencia){
		//Manipulacion de las Fechas
		$fecIniNot = ($fecIniNot!='//'&&$fecIniNot&&!strstr($fecIniNot,'none')) ? $fecIniNot : NULL;
		$fecFinNot = ($fecFinNot!='//'&&$fecFinNot&&!strstr($fecFinNot,'none')) ? $fecFinNot : NULL;
		//echo $fecIniNot." ".$fecFinNot."esta es una prueba";
		$dia_ini=substr($desFechaIni,0,2);
		$dia_fin=substr($desFechaFin,0,2);
		$mes_ini=substr($desFechaIni,3,2);
		$mes_fin=substr($desFechaFin,3,2);
		$anyo_ini=substr($desFechaIni,6,4);
		$anyo_fin=substr($desFechaFin,6,4);
		
		$desFechaIni2=$mes_ini."/".$dia_ini."/".$anyo_ini;
		$desFechaFin2=$mes_fin."/".$dia_fin."/".$anyo_fin;
		
		if($desFechaIni){$bFIng = $this->ValidaFechaProyecto($desFechaIni2,'Inicio');}
		if($desFechaFin){ $bFSal = ($this->ValidaFechaProyecto($desFechaFin2,'Fin')); }
		if ($desFechaIni&&$desFechaFin){
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $desFechaIni, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $desFechaFin, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";$this->FormBuscaResolucion($page,$tipResol,$nroResol,$sumilla,$fecFirma,$fecPublicacion,$desFechaIni,$fecDesembarqueIni,$desFechaFin,$fecDesembarqueFin,$errors);exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo "error en los meses";$this->FormBuscaResolucion($page,$tipResol,$nroResol,$sumilla,$fecFirma,$fecPublicacion,$desFechaIni,$fecDesembarqueIni,$desFechaFin,$fecDesembarqueFin,$errors);exit;
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo "error en los dias";$this->FormBuscaResolucion($page,$tipResol,$nroResol,$sumilla,$fecFirma,$fecPublicacion,$desFechaIni,$fecDesembarqueIni,$desFechaFin,$fecDesembarqueFin,$errors);exit;}
					}
			}
		}		

		}
		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
			//exit;
			$this->FormBuscaResolucion($page,$tipResol,$nroResol,$sumilla,$desFechaIni,$desFechaFin,$dependencia,$errors);
			
		}else{

		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarResol';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaResolucion($page,$tipResol,$nroResol,$sumilla,$desFechaIni,$desFechaFin,$dependencia,true);
		
		$html->assign('datos',array('page'=>$page,
									'tipResol'=>$tipResol,
									'nroResol'=>$nroResol,
									'sumilla'=>$sumilla,
									'fecFirma'=>$fecFirma,
									'desFechaIni'=>$desFechaIni,
									'desFechaFin'=>$desFechaFin,
									'dependencia'=>$dependencia
									));
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		// Condicionales Segun ingreso del Usuario
		if(!empty($sumilla)&&!is_null($sumilla))
			$condConsulta[] = "Upper(sumilla) LIKE Upper('%{$sumilla}%')";
		if(!empty($tipResol)&&!is_null($tipResol)&&$tipResol!='none')
			$condConsulta[] = "id_tipo_resolucion=$tipResol";
		if(!empty($dependencia)&&!is_null($dependencia)&&$dependencia!='none')
			$condConsulta[] = "coddep=$dependencia";
		if($desFechaIni)
			$condConsulta[] = "convert(varchar,convert(datetime,ffirma,101),103)='$desFechaIni'";
		if($desFechaFin)
			$condConsulta[] = "convert(varchar,convert(datetime,fpublicacion,101),103)='$desFechaFin'";
			
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		// Ejecuta el conteo General
		$this->abreConnDB();
		
		
				$sql_SP = sprintf("EXECUTE sp_busIdResolControl %s,%s,%s,%d,0,0",
									($nroResol) ? "'".$this->PrepareParamSQL($nroResol)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$this->userIntranet['COD_DEP']
									);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
					$sql_SP = sprintf("EXECUTE sp_busIdResolControl %s,%s,%s,%d,%s,%s",
										($nroResol) ? "'".$this->PrepareParamSQL($nroResol)."'" : 'NULL',
										($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
										($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
										$this->userIntranet['COD_DEP'],
										$start,
										$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosResol %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow())
							$html->append('arrResol', array('id' => $id[0],
														  'tipResol' => ucwords($NotiData[1]),
														  'nroResol' => $NotiData[2],
														  'sumilla' => $NotiData[3],
														  'fecFir' => ucfirst($NotiData[4]),
														  'fecPub' => strtoupper($NotiData[5]),
														  'referencia' => $NotiData[6],
														  'fecErratas' => $NotiData[7],
														  'fecRecibido' => $NotiData[32]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('coddep',$this->userIntranet['COD_DEP']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscaResol', $this->arr_accion[BUSCA_RESOLUCION], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/tramite/administracion/resoluciones/searchResult.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		}//fin del if($FInicio and $FeFin)
	}
	
	function FormModificaReferenciaResolucion($idResolucion,$GrupoOpciones1=NULL,$nroTD=NULL,$anyo3=NULL,$tipoDocc=NULL,$numero2=NULL,$anyo2=NULL,
	                                 $siglasDepe2=NULL,$Busca=NULL,$observaciones=NULL){

		global $Buscar,$idDocumento;
		if(empty($idResolucion)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}	
		//echo $idExp;
		$html = new Smarty;		
		
		if($idResolucion){
			$this->abreConnDB();
			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("SELECT r.id,tr.descrip_completa,r.nro_resol,r.sumilla,convert(varchar,convert(datetime,r.ffirma,101),103),
										convert(varchar,convert(datetime,fpublicacion,101),103),case when d.id_tipo_documento in (1,2) then d.num_tram_documentario
																									 when d.id_tipo_documento=4 then d.indicativo_oficio end,
										convert(varchar,convert(datetime,fERRATAS,101),103)
								 from resolucion r,tipo_resolucion tr,documento d
								 where r.id_tipo_resolucion=tr.id and r.id_documento=d.id_documento
								 and r.id=%d",$this->PrepareParamSQL($idResolucion));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				//$idResolucion=$rs->fields[0];
				$nroResol=$rs->fields[2];
				$sumilla=$rs->fields[3];
				$TipoResol=$rs->fields[1];
				$referencia=$rs->fields[6];
				$fecErratas=$rs->fields[7];
				$fecFirma=$rs->fields[4];
				$fecPub=$rs->fields[5];
				
				$html->assign_by_ref('nroResol',$nroResol);
				$html->assign_by_ref('sumilla',$sumilla);
				$html->assign_by_ref('TipoResol',$TipoResol);
				$html->assign_by_ref('referencia',$referencia);
				$html->assign_by_ref('fecErratas',$fecErratas);
				$html->assign_by_ref('fecFirma',$fecFirma);
				$html->assign_by_ref('fecPub',$fecPub);

				
			}
			unset($rs);
		}
		
		
		if($GrupoOpciones1==1){
			
			if($Buscar==1&&$nroTD!=""){
			
				$this->abreConnDB();
				//$this->conn->debug = true;
				
					if($anyo3>0){
						$nroOTD=$nroTD."-".$anyo3;
					}else{
						$nroOTD=$nroTD."-";
					}
					//echo "matrix".$nroOTD;

					$sql="select d.id_documento,d.num_tram_documentario,d.indicativo_oficio,
								 case when d.id_tipo_documento=1 then d.asunto
									  when d.id_tipo_documento=2 then tup.descripcion end,
								 d.observaciones,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
						  from documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
						  where 
							   d.num_tram_documentario like '%{$nroOTD}%' and id_estado_documento in (1,9)	 
						 ";
					 
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDocumento=$rs->fields[0];
					$numTram=$rs->fields[1];
					$ind=$rs->fields[2];
					$asunto2=$rs->fields[3];
					$obs=$rs->fields[4];
					$fecRec=$rs->fields[5];
					if($idDocumento>0){
						$exito=1;
					}else{
						$exito=0;
					}
				}
			}
		}elseif($GrupoOpciones1==2){
			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				//$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/";
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
							and d.id_estado_documento=1
					      and d.coddep=$siglasDepe2 and d.indicativo_oficio like '%{$var}%'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
											  case when d.id_tipo_documento in (1,2) then d.num_tram_documentario end
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 and
										      cdi.id_clase_documento_interno=d.id_clase_documento_interno 
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									$numTram=$rs3->fields[6];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				
			}//fin del if($Buscar)
		}//fin del if($GrupoOpciones1)		
		
		//Manipulacion de las Fechas
		$fecIni = ($fecIni!='//'&&$fecIni) ? $fecIni : NULL;
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : NULL;
		$fecFirma = ($fecFirma!='//'&&$fecFirma) ? $fecFirma : date('m/d/Y');
		$fecPub = ($fecPub!='//'&&$fecPub) ? $fecPub : NULL;
			
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		/*$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyReferenciaResol';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('sumilla',$sumilla);
		$html->assign_by_ref('idExp',$idExp);
		$html->assign_by_ref('TipoResol',$TipoResol);
		$html->assign_by_ref('nroResol',$nroResol);
		//Para la notificaci�n
		$html->assign_by_ref('campo4',$campo4);
		$html->assign_by_ref('destinatario',$destinatario);
		$html->assign_by_ref('DomProc',$DomProc);
		$html->assign_by_ref('vigencia',$vigencia);
		$html->assign_by_ref('radio',$radio);
		$html->assign_by_ref('checkbox',$checkbox);
		$html->assign_by_ref('checkbox2',$checkbox2);
		$html->assign_by_ref('checkbox3',$checkbox3);
		
		$html->assign_by_ref('opcion2',$opcion2);
		$html->assign_by_ref('nroResol',$nroResol);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('idResolucion',$idResolucion);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('GrupoOpciones2',$GrupoOpciones2);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('anyo3',$anyo3);
		
		$html->assign_by_ref('idDocumento',$idDocumento);
		$html->assign_by_ref('claseDoc',$claseDoc);
		$html->assign_by_ref('numTram',$numTram);
		//Para la notificaci�n
		$html->assign_by_ref('ind',$ind);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('exito',$exito);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('fecRec',$fecRec);
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT id_clase_documento_interno, substring(descripcion,1,23) ".
						  "FROM dbo.clase_documento_interno ".
						  "where procedencia='I' and categoria='D' ".
						  "and id_clase_documento_interno not in (55) ".
						  "ORDER BY 2";
		$html->assign_by_ref('selTipoDocc',$this->ObjFrmSelect($sql_st, $tipoDocc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT codigo_dependencia, substring(siglas,1,9) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Seleccione')));
		unset($sql_st);

		// Contenido Select del tipo de documento
		if($this->userIntranet['COD_DEP']==5){
			$sql_st = "SELECT id, Upper(descrip_completa) ".
						  "FROM tipo_resolucion ".
						  "ORDER BY 2";
			$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $TipoResol, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($this->userIntranet['DIRE']==2&& ($this->userIntranet['COD_DEP']==16||$this->userIntranet['COD_DEP']==36)){
			$sql_st = "SELECT id, Upper(descrip_completa) ".
						  "FROM tipo_resolucion ".
						  "WHERE id=3 ".
						  "ORDER BY 2";
			$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $TipoResol, true, true));
		}elseif($this->userIntranet['DIRE']==47){
			$sql_st = "SELECT id, Upper(descrip_completa) ".
						  "FROM tipo_resolucion ".
						  "WHERE id=7 ".
						  "ORDER BY 2";
			$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $TipoResol, true, true));
		}else{
		    if($this->userIntranet['COD_DEP']==23){
				$sql_st = "SELECT id, Upper(descrip_completa) ".
							  "FROM tipo_resolucion ".
							  "WHERE id in (4,16) ".
							  "ORDER BY 2";
			}else{
				$sql_st = "SELECT id, Upper(descrip_completa) ".
							  "FROM tipo_resolucion ".
							  "--WHERE id=4 ".
							  "ORDER BY 2";
			}
			$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $TipoResol, true, true));
		}
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIni',$this->ObjFrmMes(1, 12, true, substr($fecIni,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIni',$this->ObjFrmDia(1, 31, substr($fecIni,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIni',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecIni,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesFin',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaFin',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoFin',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));

		// Fecha Publicaci�n
		$html->assign_by_ref('selMesPub',$this->ObjFrmMes(1, 12, true, substr($fecPub,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaPub',$this->ObjFrmDia(1, 31, substr($fecPub,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoPub',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecPub,6,4), true, array('val'=>'none','label'=>'--------')));

		// Fecha Firma
		$html->assign_by_ref('selMesFirma',$this->ObjFrmMes(1, 12, true, substr($fecFirma,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaFirma',$this->ObjFrmDia(1, 31, substr($fecFirma,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoFirma',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFirma,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/resoluciones/frmModifyReferencia.tpl.php');
		$html->display('oad/footerArm.tpl.php');
	}
	
	function ModificaReferenciaResolucion($idResolucion,$GrupoOpciones1,$nroTD,$anyo3,$tipoDocc,$numero2,$anyo2,
	                                 $siglasDepe2,$Busca,$observaciones){
		global $idDocumento;
		
//		echo $idResolucion."-".$idDocumento;exit;

		if($idDocumento<0||!$idDocumento){ $b1 = true; $this->errors .= 'La referencia debe ser especificada<br>'; }
		if($idResolucion<0||!$idResolucion){ $b2 = true; $this->errors .= 'La resoluci�n debe ser especificada<br>'; }
		if(!$observaciones){ $b3 = true; $this->errors .= 'Las observaciones deben ser especificadas<br>'; }						   

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaReferenciaResolucion($idResolucion,$GrupoOpciones1,$nroTD,$anyo3,$tipoDocc,$numero2,$anyo2,
	                                 $siglasDepe2,$Busca,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE sp_changeReferenciaResolucion %d,%d,'%s','%s'",
								$idResolucion,
								$idDocumento,
								($observaciones) ? $this->PrepareParamSQL($observaciones) : "NULL",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
				/*if($row = $rs->FetchRow())
					list($RETVAL,$indInterno) = $row;
				else
					$RETVAL = 1;*/
				//unset($rs);

			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[6]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			$destination .= "&indInterno={$indInterno}";
			header("Location: $destination");
			exit;
		}
	
	}
	
	function FormAnulaResolucion($idResolucion,$GrupoOpciones1=NULL,$observaciones=NULL){

		global $Buscar,$idDocumento;
		if(empty($idResolucion)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}	
		//echo $idExp;
		$html = new Smarty;		
		
		if($idResolucion){
			$this->abreConnDB();
			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("SELECT r.id,tr.descrip_completa,r.nro_resol,r.sumilla,convert(varchar,convert(datetime,r.ffirma,101),103),
										convert(varchar,convert(datetime,fpublicacion,101),103),case when d.id_tipo_documento in (1,2) then d.num_tram_documentario
																									 when d.id_tipo_documento=4 then d.indicativo_oficio end,
										convert(varchar,convert(datetime,fERRATAS,101),103),r.coddep,r.id_tipo_resolucion
								 from resolucion r,tipo_resolucion tr,documento d
								 where r.id_tipo_resolucion=tr.id and r.id_documento=d.id_documento
								 and r.id=%d",$this->PrepareParamSQL($idResolucion));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				//$idResolucion=$rs->fields[0];
				$nroResol=$rs->fields[2];
				$sumilla=$rs->fields[3];
				$TipoResol=$rs->fields[1];
				$referencia=$rs->fields[6];
				$fecErratas=$rs->fields[7];
				$fecFirma=$rs->fields[4];
				$fecPub=$rs->fields[5];
				$coddepResol=$rs->fields[8];
				$idTipoResol=$rs->fields[9];
				
				$html->assign_by_ref('nroResol',$nroResol);
				$html->assign_by_ref('sumilla',$sumilla);
				$html->assign_by_ref('TipoResol',$TipoResol);
				$html->assign_by_ref('referencia',$referencia);
				$html->assign_by_ref('fecErratas',$fecErratas);
				$html->assign_by_ref('fecFirma',$fecFirma);
				$html->assign_by_ref('fecPub',$fecPub);

				
			}
			unset($rs);
			
			$sql2="select max(contador) from contresol where coddep=$coddepResol and id_tipo_resolucion=$idTipoResol";
			$rs2 = & $this->conn->Execute($sql2);
			unset($sql2);
			if (!$rs2){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$contResol=$rs2->fields[0];
				$html->assign_by_ref('contResol',$contResol);
			}			
		}	
		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAnulaResol';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('sumilla',$sumilla);
		$html->assign_by_ref('idExp',$idExp);
		$html->assign_by_ref('TipoResol',$TipoResol);
		$html->assign_by_ref('nroResol',$nroResol);
		//Para la notificaci�n
		$html->assign_by_ref('campo4',$campo4);
		$html->assign_by_ref('destinatario',$destinatario);
		$html->assign_by_ref('DomProc',$DomProc);
		$html->assign_by_ref('vigencia',$vigencia);
		$html->assign_by_ref('radio',$radio);
		$html->assign_by_ref('checkbox',$checkbox);
		$html->assign_by_ref('checkbox2',$checkbox2);
		$html->assign_by_ref('checkbox3',$checkbox3);
		
		$html->assign_by_ref('opcion2',$opcion2);
		$html->assign_by_ref('nroResol',$nroResol);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('idResolucion',$idResolucion);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('GrupoOpciones2',$GrupoOpciones2);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('anyo3',$anyo3);
		
		$html->assign_by_ref('idDocumento',$idDocumento);
		$html->assign_by_ref('claseDoc',$claseDoc);
		$html->assign_by_ref('numTram',$numTram);
		//Para la notificaci�n
		$html->assign_by_ref('ind',$ind);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('exito',$exito);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/resoluciones/frmAnulaResol.tpl.php');
		$html->display('oad/footerArm.tpl.php');
	}
	
	function AnulaResolucion($idResolucion,$GrupoOpciones1,$observaciones){
		
//		echo $idResolucion."-".$idDocumento;exit;

		if($idResolucion<0||!$idResolucion){ $b2 = true; $this->errors .= 'La resoluci�n debe ser especificada<br>'; }
		if(!$observaciones){ $b3 = true; $this->errors .= 'Las observaciones deben ser especificadas<br>'; }						   

		if($b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAnulaResolucion($idResolucion,$GrupoOpciones1,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE sp_anulaResolucion %d,%d,'%s','%s'",
								$idResolucion,
								$GrupoOpciones1,
								($observaciones) ? $this->PrepareParamSQL($observaciones) : "NULL",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
				/*if($row = $rs->FetchRow())
					list($RETVAL,$indInterno) = $row;
				else
					$RETVAL = 1;*/
				//unset($rs);

			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[6]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			$destination .= "&indInterno={$indInterno}";
			header("Location: $destination");
			exit;
		}
	
	}
	
	function FormBuscaCorrespondencia($page=NULL,$noti=NULL,$TipoCor=NULL,$resol=NULL,$detal=NULL,$destinatario=NULL,$fecIniNot=NULL,$fecFinNot=NULL,$search=false){
		global $dependencia;
		global $anyo3;
		global $status,$tipMensajeria,$FechaIniCrea,$FechaFinCrea,$desFechaIni,$desFechaFin,$FechaIniDili,$FechaFinDili,$FechaIniVeri,$FechaFinVeri,$vistoBueno;
		$this->abreConnDB();
		//$this->conn->debug = false;
		
		//Manipulacion de las Fechas
		$fecIniNot = ($fecIniNot!='//'&&$fecIniNot) ? $fecIniNot : NULL;
		$fecFinNot = ($fecFinNot!='//'&&$fecFinNot) ? $fecFinNot : NULL;

		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('noti',$noti);
		$html->assign_by_ref('resol',$resol);
		$html->assign_by_ref('detal',$detal);
		$html->assign_by_ref('destinatario',$destinatario);
		$html->assign_by_ref('TipoCor',$TipoCor);
		$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('anyo3',$anyo3);
		
		$html->assign_by_ref('status',$status);
		$html->assign_by_ref('tipMensajeria',$tipMensajeria);
		$html->assign_by_ref('FechaIniCrea',$FechaIniCrea);
		$html->assign_by_ref('FechaFinCrea',$FechaFinCrea);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('FechaIniDili',$FechaIniDili);
		$html->assign_by_ref('FechaFinDili',$FechaFinDili);
		$html->assign_by_ref('FechaIniVeri',$FechaIniVeri);
		$html->assign_by_ref('FechaFinVeri',$FechaFinVeri);
		
		$html->assign_by_ref('vistoBueno',$vistoBueno);
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		//Contenido de las Siglas de las Dependencias
		$sql_st = "SELECT codigo_dependencia, Upper(siglas) ".
					  "FROM db_general.dbo.h_dependencia ".
					  "WHERE condicion='ACTIVO' and codigo_dependencia not in (34,49) ".
					  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		

		
		//Contenido del Select Tipo Correspondencia
		$sql_st = "SELECT id, desccripcion ".
					  "FROM tipo_correspondencia ".
					  "ORDER BY 2";
		$html->assign_by_ref('selTipoCorrespondencia',$this->ObjFrmSelect($sql_st, $TipoCor, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniNot,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniNot,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecIniNot,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinNot,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinNot,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFinNot,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		$accionHeader=0;
		$html->assign_by_ref('accionHeader',$accionHeader);
	
		// Muestra el Formulario
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/correspondencia/search.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	function BuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot){
		global $dependencia;
		global $anyo3;
		global $status,$tipMensajeria,$FechaIniCrea,$FechaFinCrea,$desFechaIni,$desFechaFin,$FechaIniDili,$FechaFinDili,$FechaIniVeri,$FechaFinVeri,$vistoBueno;		
		
		//$dependencia = ($_POST['dependencia']) ? $_POST['dependencia'] : $_GET['dependencia'];
		//Manipulacion de las Fechas
		$fecIniNot = ($fecIniNot!='//'&&$fecIniNot&&!strstr($fecIniNot,'none')) ? $fecIniNot : NULL;
		$fecFinNot = ($fecFinNot!='//'&&$fecFinNot&&!strstr($fecFinNot,'none')) ? $fecFinNot : NULL;
		//echo $fecIniNot." ".$fecFinNot."esta es una prueba";
		if($fecIniNot){$bFIng = $this->ValidaFechaProyecto($fecIniNot,'Inicio');}
		if($fecFinNot){ $bFSal = ($this->ValidaFechaProyecto($fecFinNot,'Fin')); }
		if ($fecIniNot&&$fecFinNot){

		$mes_ini=substr($fecIniNot,0,2);
		$mes_fin=substr($fecFinNot,0,2);
		$dia_ini=substr($fecIniNot,3,2);
		$dia_fin=substr($fecFinNot,3,2);
		$anyo_ini=substr($fecIniNot,6,4);
		$anyo_fin=substr($fecFinNot,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecIniNot, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFinNot, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";$this->FormBuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot,$errors);exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "error en los meses";$this->FormBuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot,$errors);exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "error en los dias";$this->FormBuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot,$errors);exit;}
					}
			}
		}		

		}
		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
			//exit;
			$this->FormBuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot,$errors);
			
		}else{

		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaCorrespondencia($page,$noti,$TipoCor,$resol,$detal,$destinatario,$fecIniNot,$fecFinNot,true);
		
		$html->assign('datos',array('page'=>$page,
									'noti'=>$noti,
									'TipoCor'=>$TipoCor,
									'resol'=>$resol,
									'detal'=>$detal,
									'destinatario'=>$destinatario,
									'fecIniNot'=>$fecIniNot,
									'fecFinNot'=>$fecFinNot,
									'dependencia'=>$dependencia
									));
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		// Condicionales Segun ingreso del Usuario
		if(!empty($resol)&&!is_null($resol))
			$condConsulta[] = "Upper(acto) LIKE Upper('%{$resol}%')";
		if(!empty($detal)&&!is_null($detal))
			$condConsulta[] = "Upper(detalles) LIKE Upper('%{$detal}%')";
		if(!empty($destinatario)&&!is_null($destinatario))
			$condConsulta[] = "Upper(destinatario) LIKE Upper('%{$destinatario}%')";
		if(!empty($TipoCor)&&!is_null($TipoCor)&&$TipoCor!='none')
			$condConsulta[] = "id_tipo_correspondencia=$TipoCor";		
		if($fecIniNot&&$fecFinNot){
			$condConsulta[] ="(convert(datetime,audit_mod,103)>convert(datetime,'$fecIniNot',101) and convert(datetime,audit_mod,103)<dateadd(dd,1,convert(datetime,'$fecFinNot',101)))";
		}
		if($dependencia>0){
			$condConsulta[] = "coddep=$dependencia";
		}
		if($anyo3>0){
			$condConsulta[] = "year(audit_mod)=$anyo3";
		}
		if($tipMensajeria>0){
			if($tipMensajeria==1){
				$condConsulta[] = "id_tipo_mensajeria=0 and fecentcourier is not null";
			}elseif($tipMensajeria==2){
				$condConsulta[] = "id_tipo_mensajeria=1 and fecentcourier is not null";
			}	
		}
		if($status>0){
			if($status==1){
				$condConsulta[] = "flag=1";
			}elseif($status==2){
				$condConsulta[] = "flag=2";
			}elseif($status==3){
				$condConsulta[] = "flag=3";
			}elseif($status==4){
				$condConsulta[] = "fecentcourier is null";
			}elseif($status==5){
				$condConsulta[] = "fecentcourier is not null and fecnot is null";
			}
		}
		
		if($desFechaIni&& $desFechaIni!="" && $desFechaFin&& $desFechaFin!=""){
			$condConsulta[] = "convert(datetime,fecentcourier,103)>=convert(datetime,'$desFechaIni',103)";
			$condConsulta[] = "convert(datetime,fecentcourier,103)<dateadd(dd,1,convert(datetime,'$desFechaFin',103))";
		}

		if($FechaIniDili&& $FechaIniDili!="" && $FechaFinDili&& $FechaFinDili!=""){
			$condConsulta[] = "convert(datetime,fecnot,103)>=convert(datetime,'$FechaIniDili',103)";
			$condConsulta[] = "convert(datetime,fecnot,103)<dateadd(dd,1,convert(datetime,'$FechaFinDili',103))";
		}
		
		if($FechaIniCrea&& $FechaIniCrea!="" && $FechaFinCrea&& $FechaFinCrea!=""){
			$condConsulta[] = "convert(datetime,audit_mod,103)>=convert(datetime,'$FechaIniCrea',103)";
			$condConsulta[] = "convert(datetime,audit_mod,103)<dateadd(dd,1,convert(datetime,'$FechaFinCrea',103))";
		}		

		if($FechaIniVeri&& $FechaIniVeri!="" && $FechaFinVeri&& $FechaFinVeri!=""){
			$condConsulta[] = "convert(datetime,audit_cargo_oada,103)>=convert(datetime,'$FechaIniVeri',103)";
			$condConsulta[] = "convert(datetime,audit_cargo_oada,103)<dateadd(dd,1,convert(datetime,'$FechaFinVeri',103))";
		}
		if($vistoBueno==1){
			$condConsulta[] = "audit_cargo_oada is not null";
		}
		if($vistoBueno==2){
			$condConsulta[] = "audit_cargo_oada is null and usuario2='ROBOT2'";
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		if($TipoCor==1){
			$sql_SP = sprintf("EXECUTE sp_busIDCorre %s,%s,%s,0,0",
								($noti) ? "'".$this->PrepareParamSQL($noti)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
		}else{
				$sql_SP = sprintf("EXECUTE sp_busIDCorresp %s,%s,%s,0,0",
									($noti) ? "'".$this->PrepareParamSQL($noti)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
									);
		}
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			if($TipoCor==1){
				$sql_SP = sprintf("EXECUTE sp_busIDCorre %s,%s,%s,%s,%s",
									($noti) ? "'".$this->PrepareParamSQL($noti)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$start,
									$stop);
			}else{
				if(!empty($dependencia)&&!is_null($dependencia)&& $dependencia!='none'){
					if($anyo3>0){
						$sql_SP = sprintf("EXECUTE sp_busIDCorresp %s,%s,%s,%s,%s",
											($noti) ? "'".$this->PrepareParamSQL($noti)."-".$anyo3."-CONVENIO_SITRADOC/"."'" : "'".$anyo3."-CONVENIO_SITRADOC/"."'",
											($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
											($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
											$start,
											$stop);
					}else{
						$sql_SP = sprintf("EXECUTE sp_busIDCorresp %s,%s,%s,%s,%s",
											($noti) ? "'".$this->PrepareParamSQL($noti)."-2006-CONVENIO_SITRADOC/"."'" : "'"."-CONVENIO_SITRADOC/"."'",
											($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
											($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
											$start,
											$stop);
					}
				}else{
					$sql_SP = sprintf("EXECUTE sp_busIDCorresp %s,%s,%s,%s,%s",
										($noti) ? "'".$this->PrepareParamSQL($noti)."'" : 'NULL',
										($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
										($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
										$start,
										$stop);
				}
			}
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosNotificacion %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow())
							$html->append('arrNoti', array('id' => $id[0],
														  'not' => ucwords($NotiData[1]),
														  'funcionario' => $NotiData[2],
														  'acto' => $NotiData[3],
														  'detal' => ucfirst($NotiData[4]),
														  'proc' => strtoupper($NotiData[5]),
														  'destinatario' => $NotiData[6],
														  'domicilio' => $NotiData[7],
														  'fecNoti' => $NotiData[16],
														  'hora' => $NotiData[17],
														  'min' => $NotiData[18],
														  'user' => $NotiData[19],
														  'flag' => $NotiData[20],
														  'dep' => $this->userIntranet['COD_DEP'],
														  'fecEntCourier' => $NotiData[30],
														  'fecRecibido' => $NotiData[32],
														  'nombrePerActo' => $NotiData[28],
														  'dniPerActo' => $NotiData[18],
														  'motivo' => $NotiData[21],
														  'observ' => $NotiData[22],
														  'depa' => $NotiData[24],
														  'prov' => $NotiData[25],
														  'dist' => $NotiData[26],
														  'fecAcepOADA' => $NotiData[39],
														  'userAcepOADA' => $NotiData[40],
														  'claseDoc' => $NotiData[41]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_CORRESPONDENCIA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/tramite/administracion/correspondencia/searchResult.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		}//fin del if($FInicio and $FeFin)
	}
	
	function FormNuevoDestinatario($id,$destinatario=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$domicilio=NULL){

		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}	
		//echo $idExp;
		$html = new Smarty;	
		
		if($id){
			$this->abreConnDB();
			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("SELECT numero from correspondencia where id=%d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				//$idResolucion=$rs->fields[0];
				$nroCorrespondencia=$rs->fields[0];
				
				$html->assign_by_ref('nroCorrespondencia',$nroCorrespondencia);
			}
			unset($rs);
		}			
				
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmNewDestinatario';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);		
		$html->assign_by_ref('destinatario',$destinatario);
		$html->assign_by_ref('domicilio',$domicilio);		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/correspondencia/frmNuevoDestinatario.tpl.php');
		$html->display('oad/footerArm.tpl.php');
	}	
	
	function NuevoDestinatario($id,$destinatario,$codDepa,$codProv,$codDist,$domicilio){

		// Comprueba Valores	
		if($id<1||!$id||$id==0){ $b1 = true; $this->errors .= 'La correspondencia debe ser especificada<br>'; }
		if(!$destinatario){ $b2 = true; $this->errors .= 'El destinatario ser especificado<br>'; }
		if(!$domicilio){ $b3 = true; $this->errors .= 'El domicilio ser especificado<br>'; }
		if(($codDepa<0)){ $b4 = true; $this->errors .= 'El Departamento debe ser especificado<br>'; }
		if(($codProv<0)){ $b5 = true; $this->errors .= 'La Provincia debe ser especificada<br>'; }
		if(($codDist<0)){ $b6 = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }

		if($b1||$b2||$b3||$b4||$b5||$b6){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormNuevoDestinatario($id,$destinatario,$fecEntCourier,$codDepa,$codProv,
								   $codDist,$domicilio,$radio2,
								   $horEntCourier,$reLoad,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE sp_newDestinatario %d,'%s',%s,%s,%s,%s,%s",
								$id,
							  $_SESSION['cod_usuario'],
							  ($destinatario) ? "'".$this->PrepareParamSQL($destinatario)."'" : "NULL",
							  ($codDepa) ? "'".$this->PrepareParamSQL($codDepa)."'" : "NULL",
							  ($codProv) ? "'".$this->PrepareParamSQL($codProv)."'" : "NULL",
							  ($codDist) ? "'".$this->PrepareParamSQL($codDist)."'" : "NULL",
							  ($domicilio) ? "'".$this->PrepareParamSQL($domicilio)."'" : "NULL"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[9]['val']}&subMenu={$this->subMenu_items[3]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormDisminuyeContador($dependencia=NULL,$claseDoc=NULL,$Busca=NULL,$exito=NULL,$errors=false){
			
		$html = new Smarty;		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		/*$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());*/

		if($Busca){
			$this->abreConnDB();
			//$this->conn->debug = true;
			$sql_st = "select case when max(cont.contador)>0 then max(cont.contador)
							else 0 end as CONTADOR,CDI.id_CLASE_documento_INTERNO ,CDI.DESCRIPCION
				from dbo.clase_documento_interno cdi,cuentainterno cont  
				where cdi.categoria='D' 
				and cont.contador>0
				AND cont.id_tipo_documento=cdi.id_clase_documento_interno";
				
			if($dependencia>0)
			    $sql_st.=" AND cont.coddep=$dependencia";
			if($claseDoc>0)
				$sql_st.=" AND cdi.id_clase_documento_interno=$claseDoc";
				
			$sql_st.=" group by CDI.id_CLASE_documento_INTERNO ,CDI.DESCRIPCION
				order by CDI.id_CLASE_documento_INTERNO					
					";
			
			/*$sql_st = "SELECT t.email,case when p.ind_resolucion='TRUE' then ' CHECKED' ELSE '' END,
								      case when p.ind_correspondencia='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_finalizacion='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_archivo='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_reasignacion='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_anulacion='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_correccionDoc='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.administracion='TRUE' then ' CHECKED' ELSE '' END,
									  case when p.ind_docAntiguos='TRUE' then ' CHECKED' ELSE '' END
						from db_general.dbo.h_trabajador t,dbo.permiso p
						where t.codigo_trabajador=p.codigo_trabajador and t.estado='ACTIVO' and t.condicion<>'GENERAL'
			   ";
			 if($dependencia>0){
			    $sql_st.=" and t.coddep=$dependencia";
				if($subDependencia>0)
					$sql_st.= " and t.idsubdependencia=$subDependencia";
				if($trabajador>0)
					$sql_st.= " and t.codigo_trabajador=$trabajador";
				
			}*/
			//$sql_st.= " order by t.email ";
							
	    //echo $sql_st; exit;
		
		//echo "x".$claseDoc."x";
		//if($claseDoc=="none")
			$valorLectura=" readonly";
		/*else
			$valorLectura="";*/	
		
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$html_tr = NULL;
			$imagen = "/img/800x600/ico-grupo.gif";
			while ($row = $rs->FetchRow()) {
				$color_td = (isset($row[8])) ? $row[8] : '#F6F6F6';
				$html_tr .= "
	  <tr>
	  <td class=\"texto\" bgcolor=\"${color_td}\" valign=\"middle\">
	    &nbsp;&nbsp;<img src=\"${imagen}\" border=\"0\" align=\"absmiddle\" hspace=\"1\">&nbsp;&nbsp;&nbsp;{$row[2]}
	  </td>
		<td align=\"center\" bgcolor=\"${color_td}\"> 
		  <input type=\"textfield\" name=\"der[{$row[0]}][0]\" size=\"5\" {$valorLectura} class=\"ip-login contenido\" value=\"{$row[0]}\" onKeyPress=\"solo_num();\">
		</td>
		<td align=\"center\" bgcolor=\"${color_td}\"> 
		  <input type=\"checkbox\" name=\"der[{$row[0]}][1]\" value=\"1\"{$row[2]}>
		  <input type=\"hidden\" name=\"valorDism\" value=\"{$row[0]}\">
		</td>
	  </tr>";
			}
			unset($row);
			$rs->Close(); # optional
		}
				if(is_null($html_tr))
			$perm = array(false);
		else
			$perm = array(true,$html_tr);

		unset($rs);		
		}
		
		$aa=$perm[1];
		$html->assign_by_ref('aa',$aa);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmDisminuyeContador';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);

		
		// Contenido Select del Select Dependencia
		$sql_st = "SELECT codigo_dependencia, substring(siglas,1,15) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		// Contenido Select del Select Dependencia
		if($dependencia>0){
		$sql_st = "SELECT distinct cdi.id_clase_documento_interno,cdi.descripcion ".
					"from dbo.clase_documento_interno cdi,cuentainterno c ".
					"where cdi.id_clase_documento_interno=c.id_tipo_documento ".
					"and c.contador>0 ".
					"and c.coddep=$dependencia ".
					"order by 2";
		}
		$html->assign_by_ref('selClaseDoc',$this->ObjFrmSelect($sql_st, $claseDoc, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
				
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		$html->assign_by_ref('claseDoc',$claseDoc);		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerControl.tpl.php');
		$html->display('oad/tramite/administracion/dismimuyeContadorDoc.tpl.php');
		$html->display('oad/footerArm.tpl.php');
	}	
	
	function DisminuyeContador($dependencia,$claseDoc,$Busca,$exito,$valorDism){
	
		// Comprueba Valores	
		if(!$valorDism){ $b1 = true; $this->errors .= 'El contador a disminuir debe ser especificado<br>'; }
		if(($dependencia<0)){ $b2 = true; $this->errors .= 'La dependencia debe ser especificado<br>'; }
		if(($claseDoc<0)){ $b3 = true; $this->errors .= 'La Clase de documento debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormDisminuyeContador($dependencia,$claseDoc,$Busca,$exito,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE sp_disminuyeContador %d,'%s',%d,%d",
								($dependencia>0) ? $dependencia : "0",
							  $_SESSION['cod_usuario'],
							  ($claseDoc) ? $this->PrepareParamSQL($claseDoc) : "0",
							  ($valorDism) ? $this->PrepareParamSQL($valorDism) : "0"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[9]['val']}&subMenu={$this->subMenu_items[3]['val']}";
			header("Location: $destination");
			exit;
		}
	}	
	
	function ConsultaInternos($idDocumento,$nroTD=NULL,$anyo3=NULL,$tipoDocc=NULL,$numero2=NULL,$anyo2=NULL,
	                               $siglasDepe2=NULL,$Busca,$Buscar,$exito,$idClaseDoc=NULL,$dependencia=NULL,
								   $asunto=NULL,$observaciones=NULL,$desFechaIni=NULL,$errors=false){
		global $idDocPadre,$ids,$ids2,$ids3,$depedepe;
		global $bSubmit,$bSubmit2;
		$html = new Smarty;
		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());
		
			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				//$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/";
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
						  and d.id_estado_documento=1 and d.coddep=$siglasDepe2
					      and d.indicativo_oficio like '%{$var}%'";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 /*and*/
										      
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				$idDocPadre=$idDocumento;	
			}//fin del if($Buscar)
		//$idDocPadre=$idDocumento;
		
		if($idDocPadre>0){
			if($this->userIntranet['COD_DEP']==38||$this->userIntranet['COD_DEP']==13){
			//Para que el usuario de Dgi pueda consultar los documentos del DVMYPE-I
			$sql_st = sprintf("SELECT md.id_movimiento_documento,case when (md.id_oficio is null and d.id_tipo_documento in (1,2)) then d.num_tram_documentario
                                       else Upper(c.descripcion) end,d.indicativo_oficio,d.asunto,d.observaciones,
	   							  Upper(dep1.siglas),Upper(dep2.siglas),
		                          convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  convert(varchar,md.audit_rec,103)+' '+convert(varchar,md.audit_rec,108),
								  md.id_documento,md.id_oficio,md.derivado,md.finalizado,md.id_dependencia_origen,md.id_dependencia_destino 
		FROM movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                 left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                    documento d, dbo.clase_documento_interno c
		WHERE md.id_documento=%d /*and md.id_oficio>0*/ 
						and ((d.id_documento=md.id_oficio and md.id_oficio>0) or (d.id_documento=md.id_documento and md.id_oficio is null)) 
						and d.id_clase_documento_interno=c.id_clase_documento_interno
						and md.id_documento in (
												select id_documento 
												from movimiento_documento 
												where id_documento=$idDocPadre 
													and id_dependencia_origen in (".$this->userIntranet['COD_DEP'].",38,36)
													AND id_dependencia_destino in (8,12,10,45,9)
											)
		order by md.audit_mod  
							",
							$idDocPadre);
			}else{
			$sql_st = sprintf("SELECT md.id_movimiento_documento,case when (md.id_oficio is null and d.id_tipo_documento in (1,2)) then d.num_tram_documentario
                                       else Upper(c.descripcion) end,d.indicativo_oficio,d.asunto,d.observaciones,
	   							  Upper(dep1.siglas),Upper(dep2.siglas),
		                          convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  convert(varchar,md.audit_rec,103)+' '+convert(varchar,md.audit_rec,108),
								  md.id_documento,md.id_oficio,md.derivado,md.finalizado,md.id_dependencia_origen,md.id_dependencia_destino 
		FROM movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                 left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                    documento d, dbo.clase_documento_interno c
		WHERE md.id_documento=%d /*and md.id_oficio>0*/ 
						and ((d.id_documento=md.id_oficio and md.id_oficio>0) or (d.id_documento=md.id_documento and md.id_oficio is null)) 
						and d.id_clase_documento_interno=c.id_clase_documento_interno
						and md.id_documento in (
												select id_documento 
												from movimiento_documento 
												where id_documento=$idDocPadre 
													and id_dependencia_origen=".$this->userIntranet['COD_DEP']."
													AND id_dependencia_destino in (8,12,10,45,9)
											)
		order by md.audit_mod  
							",
							$idDocPadre);
			}
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('idMov' => $row[0],
										   'cla' => $row[1],
										   'ind' => $row[2],
										   'asu' => $row[3],
										   'obs' => $row[4],
										   'depO' => ucfirst($row[5]),
										   'depD' => ucfirst($row[6]),
										   'fecDer' => $row[7],
										   'fecRec' => $row[8],
										   'idDoc' => $row[9],
										   'idOf' => $row[10],
										   'derivado' => $row[11],
										   'finalizado' => $row[12],
										   'idDepeOri' => $row[13],
										   'idDepeDest' => $row[14]									   
										   ));
			$rs->Close();
		}
		unset($rs);		
		}
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'consultaInternos';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$rutaSitradoc='https://'.$_SERVER['HTTP_HOST'].'/institucional/aplicativos/oad/sitradocV2/index.php';
		$html->assign('frmRutaSitradoc',$rutaSitradoc);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('idDocumento',$idDocumento);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('numTram',$numTram);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('tipoDocc',$tipoDocc);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('siglasDepe2',$siglasDepe2);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('exito',$exito);
		$html->assign_by_ref('idClaseDoc',$idClaseDoc);
		$html->assign_by_ref('dependencia',$dependencia);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('idDocPadre',$idDocPadre);
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		$html->assign_by_ref('bSubmit',$bSubmit);
		$html->assign_by_ref('bSubmit2',$bSubmit2);
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT distinct cdi.id_clase_documento_interno, lower(cdi.descripcion) ".
				  "FROM dbo.clase_documento_interno cdi,cuentainterno c ".
				  "WHERE cdi.procedencia='i' AND cdi.categoria='D' ".
				  "and cdi.id_clase_documento_interno=c.id_tipo_documento and c.coddep=".$this->userIntranet['COD_DEP']." ".
				  "and cdi.id_clase_documento_interno not in (46,47) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selClaseDoc',$this->ObjFrmSelect($sql_st, $idClaseDoc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
			$sql_st = "SELECT codigo_dependencia, lower(dependencia) ".
					  "FROM db_general.dbo.h_dependencia ".
					  "where codigo_dependencia<>34 /*and codigo_dependencia<>". $this->userIntranet['COD_DEP'] ."*/ ".
					  "and codigo_dependencia not in (63,64) ".
					  " ".//agregado para los usuarios de OTD QUE tambi�n pueden generar correspondencia
					  "and condicion='ACTIVO' ".
					  "ORDER BY 2";
		
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT distinct cdi.id_clase_documento_interno, lower(cdi.descripcion) ".
					  "FROM dbo.clase_documento_interno cdi,cuentainterno c ".
					  "WHERE cdi.procedencia='i' AND cdi.categoria='D' ".
					  "and cdi.id_clase_documento_interno=c.id_tipo_documento and c.coddep=".$this->userIntranet['COD_DEP']." ".
					  "and cdi.id_clase_documento_interno not in (46,47,59,55,81,82,83,103) ".
					  "ORDER BY 2";
		$html->assign_by_ref('selTipoDocc',$this->ObjFrmSelect($sql_st, $tipoDocc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select DEPENDENCIA
		if($this->userIntranet['COD_DEP']==38||$this->userIntranet['COD_DEP']==13){
		//Para que el usuario de Dgi pueda consultar los documentos del DVMYPE-I
		$sql_st = "SELECT codigo_dependencia, substring(siglas,1,9) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "and CODIGO_DEPENDENCIA in (".$this->userIntranet['COD_DEP'].",38,36) ".
						  " ORDER BY 2";
		}else{
		$sql_st = "SELECT codigo_dependencia, substring(siglas,1,9) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "and CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP'].
						  " ORDER BY 2";
		}
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true));

		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);
		
		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerCoordinador.tpl.php');
		$html->display('oad/tramite/administracion/consultaInternosCoordinador.tpl.php');
		$html->display('oad/footerArm.tpl.php');
	}
	

	
}
?>