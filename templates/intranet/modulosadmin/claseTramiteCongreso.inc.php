<?
require_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class TramiteCongreso extends Modulos{

	// Atributos de la Aplicacion
	var $tipbusqueda;
	var $pathTemplate;

	/*Funciones*/

	function ValidaFechaDocumento($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}

	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function FormBuscaDocumento($page=NULL,$tipBusqueda=NULL,$nroTD=NULL,$congresista=NULL,$asunto=NULL,$prioridad=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto',$asunto);
		
		// Contenido Select Congresista
		$sql_st = "SELECT id_solicitante, lower(descripcion) ".
				  "FROM dbo.solicitante ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCongresista',$this->ObjFrmSelect($sql_st, $congresista, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function BuscaDocumento($page,$tipBusqueda,$nroTD,$congresista,$asunto,$prioridad){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocumento($page,$tipBusqueda,$nroTD,$congresista,$asunto,$prioridad,true);
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		switch($tipBusqueda){
			// No Derivados
			case 1:
				$condConsulta[] = "d.flag='N'";
				break;
			// Derivados VMP
			case 2:
				$condConsulta[] = "d.flag='D' and ddep.codigo_dependencia=16";
				break;
			// Derivados VMI
			case 3:
				$condConsulta[] = "d.flag='D' and ddep.codigo_dependencia=36";
				break;
			// Derivados SG
			case 11:
				$condConsulta[] = "d.flag='D' and ddep.codigo_dependencia=5";
				break;
			// Derivados VMI-VMP
			case 4:
				$condConsulta[] = "d.flag='D' and (ddep.codigo_dependencia=16 or ddep.codigo_dependencia=36)";
				break;
			// Derivados Vencidos
			case 5:
				$condConsulta[] = "d.flag='D' and d.fecha_max_resp < getDate()";
				break;
			// Respondidos
			case 6:
				$condConsulta[] = "d.flag='R'";
				break;
			//Todos los que corresponden al Despacho
			case 7:
				$condConsulta[] = "d.flag!='T'";
				break;
			// Nuevos Derivados
			case 8:
				$condConsulta[] = "d.flag='D' and d.fecha_max_resp > getDate() and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'];
				break;
			// Derivados Vencidos
			case 9:
				$condConsulta[] = "d.flag='D' and d.fecha_max_resp < getDate() and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'];
				break;
			case 10:
				$condConsulta[] = "d.flag='D' and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'];
				break;
		}
		
		// Condicionales Segun ingreso del Usuario
		if(!empty($asunto)&&!is_null($asunto))
			$condConsulta[] = "Upper(d.asunto) LIKE Upper('%{$asunto}%')";
		if(!empty($congresista)&&!is_null($congresista)&&$congresista!='none')
			$condConsulta[] = "d.id_solicitante=$congresista";
		if(!empty($prioridad)&&!is_null($prioridad)&&$prioridad!='none')
			$condConsulta[] = "d.prioridad=$prioridad";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		
		$sql_SP = sprintf("EXECUTE sp_busIDDocumento %s,%s,%s,0,0",
							($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDocumento %s,%s,%s,%s,%s",
								($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del Inventariado
					$sql_SP = sprintf("EXECUTE sp_busDatosDocumento %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($DocumentoData = $rs->FetchRow())
							$html->append('arrDoc', array('id' => $id[0],
														  'sol' => ucwords($DocumentoData[0]),
														  'nroTD' => $DocumentoData[1],
														  'fecing' => $DocumentoData[2],
														  'asunto' => ucwords($DocumentoData[3]),
														  'oficio' => strtoupper($DocumentoData[4]),
														  'asig' => ucwords($DocumentoData[6]),
														  'nota' => ucwords($DocumentoData[5]),
														  'fecasig' => $DocumentoData[7],
														  'max' => $DocumentoData[8],
														  'doc' => $DocumentoData[9],
														  'fecrpta' => $DocumentoData[10],
														  'docoficio' => $DocumentoData[11],
														  'fecoficio' => $DocumentoData[12],
														  'priori' => $DocumentoData[13],
														  'oficina' => ucwords($DocumentoData[14])));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DOCUMENTO], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
 
}
?>