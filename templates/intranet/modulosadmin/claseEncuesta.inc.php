<?php
require_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class Encuesta extends Modulos{
	// Declaracion de Constantes
	var $appUrl = '/encuesta.php';
	
	// Constructor
	function Encuesta(){
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		$this->arr_accion = array(
							EJECUTA_VOTACION => 'newVote',
							MUESTRA_RESULTADOS => 'showResults',
							BUSCA_ENCUESTA => 'searchResult'
							);
	}
	
	function PollMain($idPoll) {
		// Selecci�n de la Encuesta con el $idPoll indicado, asi como de sus respectivas opciones
		$this->abreConnDB();
		$sql_st = "SELECT tit_encuesta FROM encuesta_desc WHERE id_encuesta={$idPoll}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$titPoll = $row[0];
			unset($row);
			$rs->Close();
		}
		
		// Crea el Objeto HTML	
		$html = new Smarty;
		// Declara Variables del Formulario
		$html->assign('frmName','frmEncuesta');
		$html->assign_by_ref('frmUrl',$this->appUrl);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('id',$idPoll);
		$html->assign_by_ref('titulo',$titPoll);
		// Construye el Set de Radio Buttons para las Opciones
		$sql_st = 	"SELECT id_opcion, des_opcion, cnt_opcion ".
					"FROM encuesta_datos ".
					"WHERE id_encuesta={$idPoll} and ind_activo IS true ".
					"ORDER BY id_opcion ASC";
		$html->assign_by_ref('selOption',$this->ObjFrmRadioButton($sql_st, 'idOption', NULL, 1, true));
		unset($sql_st);
		
		$html->display('encuesta/PollMain.tpl.php');
	}

	function BuscaEncuesta() {
		$html = new Smarty;
		
		$this->abreConnDB();
		//$this->conn->debug=true;
		$sql_st = "SELECT * FROM encuesta_desc order by 1 desc ";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('arrEnc',array('a1' => $row[0],
										   'a2' => $row[1],
										   'a3' => $row[2],
										   'a4' => $row[3]
										   ));
			$rs->Close();
		}
		unset($rs);		
		
		// Crea el Objeto HTML	
		// Declara Variables del Formulario
		$html->assign('frmName','frmEncuesta');
		$html->assign_by_ref('frmUrl',$this->appUrl);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		/*$html->assign_by_ref('id',$idPoll);
		$html->assign_by_ref('titulo',$titPoll);*/
		// Construye el Set de Radio Buttons para las Opciones
		/*$sql_st = 	"SELECT id_opcion, des_opcion, cnt_opcion ".
					"FROM encuesta_datos ".
					"WHERE id_encuesta={$idPoll} and ind_activo IS true ".
					"ORDER BY id_opcion ASC";
		$html->assign_by_ref('selOption',$this->ObjFrmRadioButton($sql_st, 'idOption', NULL, 1, true));
		unset($sql_st);*/
		
		$html->display('encuesta/searchResult.tpl.php');
	}

	function PollLatest(){
		$this->abreConnDB();
	
		$sql_st = "SELECT id_encuesta ".
					 "from encuesta_desc ".
					 "WHERE ind_activo IS true ".
					 "ORDER BY id_encuesta DESC";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) 
				$idPoll = $row[0];
			unset($row);
			$rs->Close();
		}
	
		return $idPoll;
	}

	function PollNewest(){
		$this->PollMain($this->pollLatest());
	}

	function PollIntegrity($idPoll,$user){
		$this->abreConnDB();
		// Verifica la Votacion del Usuario 
		$sql_st = "SELECT count(*) ".
				  "FROM encuesta_integridad ".
				  "WHERE cod_usuario='{$user}' and id_encuesta={$idPoll}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$stat = $row[0];
			unset($row);
			$rs->Close();
		}
		return ($stat > 0) ? true : false;
	}

	function PollCollector($idPoll, $idOption){
		$stat_votacion=0;
		$this->abreConnDB();
		
		if(!$this->PollIntegrity($idPoll,$_SESSION['cod_usuario'])){
			$ins_st = "INSERT into encuesta_integridad(cod_usuario, id_encuesta) ".
					  "VALUES('{$_SESSION['cod_usuario']}', {$idPoll})";
			if ($this->conn->Execute($ins_st) === false)
				print 'error actualizando: '.$conn->ErrorMsg().'<BR>';
			else{
				unset($ins_st);
				$upd_st = "UPDATE encuesta_datos ".
						  "SET cnt_opcion=cnt_opcion+1 ".
						  "WHERE id_encuesta={$idPoll} AND id_opcion={$idOption}";
				if ($this->conn->Execute($upd_st) === false)
					print 'error actualizando: '.$conn->ErrorMsg().'<BR>';
				unset($upd_st);
			}	
		}else
			echo '<br><br><p align="center" class="tit-documento-intranet">Ud. ya ha realizado una votacion para esta encuesta.<br>S�lo se permite una por usuario.</p>';
	}

	function PollResults($idPoll){
		$BarScale = 150;
		if(!isset($idPoll)) $idPoll = 1;
		// Selecci�n de la Encuesta con el $idPoll indicado, asi como de sus respectivas opciones
		$this->abreConnDB();
		$sql_st = "SELECT tit_encuesta FROM encuesta_desc WHERE id_encuesta={$idPoll}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$titPoll = $row[0];
			unset($row);
			$rs->Close();
		}
		// Calcula la Suma de las Votaciones para cada opcion de la encuesta
		$sql_st = "SELECT SUM(cnt_opcion) FROM encuesta_datos WHERE id_encuesta={$idPoll}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$sum = $row[0];
			unset($row);
			$rs->Close();
		}
		// Lista las Opciones de Cada encuesta con el numero de sus votaciones
		$sql_st = 	"SELECT des_opcion, cnt_opcion ".
					"FROM encuesta_datos ".
					"WHERE id_encuesta={$idPoll} and ind_activo IS true ".
					"ORDER BY id_opcion ASC";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()) {
				$percent = ($sum) ? (100*$row[1])/$sum : 0;
				$percentInt = (int)$percent * $BarScale / 100;
				$percent2 = (int)$percent;
				if ($percent > 0) {
					$barra = "<img src=\"img/800x600/leftbar.gif\" height=14 width=7>";
					$barra .= "<img src=\"img/800x600/mainbar.gif\" height=14 width=$percentInt Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/rightbar.gif\" height=14 width=7>";
				} else {
					$barra = "<img src=\"img/800x600/leftbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/mainbar.gif\" height=14 width=3 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/rightbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
				}
				$option[] = array('label'=>$row[0],
								  'barra'=>$barra,
								  'sumary'=>sprintf("%.2f %% (%d)", $percent, $row[1])
								  );
			}
			unset($row);
			$rs->Close();
		}
		
		// Crea el Objeto HTML	
		$html = new Smarty;
		// Declara Variables del Formulario
		$html->assign_by_ref('titulo',$titPoll);
		$html->assign_by_ref('option',$option);

		$html->display('encuesta/PollResults.tpl.php');

	}


}

?>