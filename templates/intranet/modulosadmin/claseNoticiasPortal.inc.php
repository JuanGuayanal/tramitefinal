<?php
include_once 'mimemail/htmlMimeMail.php';
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class NoticiasPortal extends Modulos{

    function NoticiasPortal($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][1];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][2];
		$this->userDB = $this->arr_userDB['postgres'][1];
		$this->passDB = $this->arr_passDB['postgres'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_NOTICIA => 'frmSearchNew',
							BUSCA_NOTICIA => 'searchNew',
							FRM_AGREGA_NOTICIA => 'frmAddNew',
							AGREGA_NOTICIA => 'addNew',
							FRM_MODIFICA_NOTICIA => 'frmModifyNew',
							MODIFICA_NOTICIA => 'modifyNew',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		
		$this->datosUsuarioPortal();
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}

		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		/*
		if($this->userIntranet['COD_DEP']!=18){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
				
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		
		// Items del Menu Principal
		if($_SESSION['mod_ind_leer']) $this->menu_items[0] = array ( 'val' => 'frmSearchNew', label => 'BUSCAR' );
		if($_SESSION['mod_ind_insertar']) $this->menu_items[1] = array ( 'val' => 'frmAddNew', label => 'AGREGAR' );
		if($_SESSION['mod_ind_modificar']) $this->menu_items[2] = array ( 'val' => 'frmModifyNew', label => 'MODIFICAR' );

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	
	function ValidaFechaNoticia($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('portal/noticias/headerArm.tpl.php');
		$html->display('portal/noticias/showStatTrans.inc.php');
		$html->display('portal/noticias/footerArm.tpl.php');
	}
	
	function FormBuscaNoticia($page=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('portal/noticias/headerArm.tpl.php');
		$html->display('portal/noticias/search.tpl.php');
		if(!$search) $html->display('portal/noticias/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaNoticia();
	}

	
	function BuscaNoticia($page,$desNombre){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaNoticia($page,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);	

		$sql_st = sprintf("SELECT count(*) ".
						  "FROM public.noticia ".
						  "WHERE (Upper(des_titulo) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
								 "or Upper(des_noticia) like Upper('%%%1\$s%%'))",
						  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL);
		// echo $sql_st;		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar Busqueda
			$start =  ($page == 1) ? 0 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_st = sprintf("SELECT id_noticia, des_titulo, to_char(fec_noticia, 'DD/MM/YYYY HH24:MI'), ".
									 "CASE WHEN ind_activo is true THEN 'Activo' ELSE 'Inactivo' END ".
							  "FROM public.noticia ".
							  "WHERE (Upper(des_titulo) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
									 "or Upper(des_noticia) like Upper('%%%1\$s%%')) ".
							  "ORDER BY fec_creacion desc ".
							  "LIMIT %2\$s OFFSET %3\$s",
							  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL,
							  $this->numMaxResultsSearch,
							  $start);
			//echo $sql_st;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while($row = $rs->FetchRow()){
					$html->append('arrNoti', array('id' => $row[0],
												   'desc' => $row[1],
												   'fech' => $row[2],
												   'esta' => $row[3]));
				}
				$rs->Close();
			}
			unset($rs);
		}else
			$start = 0;
					
		// Setea indicadores de derecho
		$html->assign('indMod',$_SESSION['mod_ind_modificar']);
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_NOTICIA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('portal/noticias/searchResult.tpl.php');
		$html->display('portal/noticias/footerArm.tpl.php');
	}
	
	function FormManipulaNoticia($idNoticia=NULL,$desTitulo=NULL,$desResumen=NULL,$desNoticia=NULL,$fecNoticia=NULL,$desFuente=NULL,
 							     $indActivo=NULL,$indImg=NULL,$desImg=NULL,$indImgExp=NULL,$desImgExp=NULL,$numAnImgExp=NULL,
							     $numAlImgExp=NULL,$reLoad=false,$errors=NULL){
								
		if($idNoticia&&!$reLoad){
			$this->abreConnDB();
			
			$sql_st = sprintf("SELECT des_titulo,des_resumen,des_noticia,to_char(fec_noticia,'MM/DD/YYYY HH24:MI'),des_fuente".
			   						 ",CASE WHEN ind_activo is true THEN 'Y' ELSE 'N' END,CASE WHEN ind_imagen is true THEN 'Y' ELSE 'N' END".
									 ",des_imagen,CASE WHEN ind_imagenexpandible is true THEN 'Y' ELSE 'N' END,des_imagenexpandible".
									 ",num_imagenancho,num_imagenalto ".
							  "FROM public.noticia ".
							  "WHERE id_noticia=%d",$this->PrepareParamSQL($idNoticia));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($desTitulo,$desResumen,$desNoticia,$fecNoticia,$desFuente,
						 $indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
						 $numAlImgExp) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddNew';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('idNoticia',$idNoticia);
		$html->assign_by_ref('desTitulo',$desTitulo);
		$html->assign_by_ref('desResumen',$desResumen);
		$html->assign_by_ref('desNoticia',$desNoticia);
		$html->assign_by_ref('desFuente',$desFuente);
		$html->assign_by_ref('desImg',$desImg);
		$html->assign_by_ref('desImgExp',$desImgExp);
		
		$html->assign_by_ref('numAnImgExp',$numAnImgExp);
		$html->assign_by_ref('numAlImgExp',$numAlImgExp);
		
		$html->assign_by_ref('indActivo',$indActivo);
		$html->assign_by_ref('indImg',$indImg);
		$html->assign_by_ref('indImgExp',$indImgExp);
		
		// Fecha Noticia
		$html->assign_by_ref('selMesNot',$this->ObjFrmMes(1, 12, true, substr($fecNoticia,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaNot',$this->ObjFrmDia(1, 31, substr($fecNoticia,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoNot',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecNoticia,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selHoraNot',$this->ObjFrmHora(0, 23, true, (substr($fecNoticia,11,2)) ? substr($fecNoticia,11,2) : NULL, true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selMinNot',$this->ObjFrmMinuto(1, true, (substr($fecNoticia,14,2)) ? substr($fecNoticia,14,2) : NULL, true, array('val'=>'none','label'=>'---')));

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('portal/noticias/headerArm.tpl.php');
		$html->display('portal/noticias/frmAddNew.tpl.php');
		$html->display('portal/noticias/footerArm.tpl.php');
	
	}

	function ManipulaNoticia($idNoticia,$desTitulo,$desResumen,$desNoticia,$fecNoticia,$desFuente,
							 $indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
							 $numAlImgExp){
							
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImg = trim($desImg)!='/mipe/img/prensa/' ? $desImg : NULL; 
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$desImgExp = trim($desImgExp)!='/mipe/img/prensa/' ? $desImgExp : NULL; 
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Comprueba Valores	
		if(!$desTitulo){ $bTit = true; $this->errors .= 'El T�tulo debe ser especificado<br>'; }
		if(!$desResumen){ $bRes = true; $this->errors .= 'El Resumen debe ser especificado<br>'; }
		if(!$desNoticia){ $bNot = true; $this->errors .= 'La Noticia debe ser especificada<br>'; }
		if($fecNoticia){ $bFNot = ($this->ValidaFechaNoticia($fecNoticia,'Noticia')); }
		if($indImg=='Y'&&!$desImg){ $bImg = true; $this->errors .= 'La Imagen debe ser especificada<br>'; }
		if($indImg=='Y'&&$indImgExp=='Y'&&!$desImgExp){ $bDImgE = true; $this->errors .= 'La Imagen Expandible debe ser especificada<br>'; }
		if($indImg=='Y'&&$indImgExp=='Y'&&(!$numAnImgExp||!$numAnImgExp)){ $bTImgE = true; $this->errors .= 'El Tama�o de la Imagen Expandible debe ser especificado<br>'; }

		if($bTit||$bRes||$bNot||$bFNot||$bImg||$bDImgE||$bTImgE){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormManipulaNoticia($idNoticia,$desTitulo,$desResumen,$desNoticia,$fecNoticia,$desFuente,
							$indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
							$numAlImgExp,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			//$this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_FT = sprintf("SELECT %s(%s'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%d)",
							  $idNoticia ? 'modNoticiaPortal' : 'addNoticiaPortal',
							  $idNoticia ? $this->PrepareParamSQL($idNoticia).',' : '',
							  $this->PrepareParamSQL($desTitulo),
							  $this->PrepareParamSQL($desResumen),
							  $this->PrepareParamSQL($desNoticia),
							  ($fecNoticia) ? "'".$this->PrepareParamSQL($fecNoticia)."'" : 'NULL',
							  ($desFuente) ? "'".$this->PrepareParamSQL($desFuente)."'" : 'NULL',
							  ($indImg=='Y') ? 'true' : 'false',
							  ($indImg=='Y') ? "'".$this->PrepareParamSQL($desImg)."'" : 'NULL',
							  ($indImgExp=='Y') ? 'true' : 'false',
							  ($indImgExp=='Y') ? "'".$this->PrepareParamSQL($desImgExp)."'" : 'NULL',
							  ($indImgExp=='Y') ? $this->PrepareParamSQL($numAnImgExp) : 'NULL',
							  ($indImgExp=='Y') ? $this->PrepareParamSQL($numAlImgExp) : 'NULL',
							  ($indActivo=='Y') ? 'true' : 'false',
							  $this->userIntranet['CODIGO']);
			// echo $sql_FT;
			$rs = & $this->conn->Execute($sql_FT);
			unset($sql_FT);
			if (!$rs)
				$RETVAL=0;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 0;
				$rs->Close();
			}				
			unset($rs);
			
			$mes=substr($fecNoticia,0,2);
			$dia=substr($fecNoticia,3,2);
			$year=substr($fecNoticia,6,4);
			$hora=substr($fecNoticia,11,5);
			
			
			//Envio de correo a lista de inter�s
			$desTitulo = "NOTICIA DEL D�A"; 
			$desMensaje='<html><head></head><body topmargin="0" leftmargin="0">
										<table>
										<tr> 
										<td width="20" align="center" valign="top"></td>
										<td valign="top" align="justify"><font color="#000000" size="2" face="Verdana">'.						
										'<b>'.$desResumen.'</b>'.
										'</font><br></td></tr>
										<tr> 
										<td width="20" align="justify" valign="top"></td>
										<td valign="top" align="justify"><font color="#000000" size="1" face="Verdana"><br>'.						
										'  '.$desNoticia.'<br>'.
										'</font></td></tr>
										<tr> 
										<td width="20" align="justify" valign="top"></td>
										<td valign="top" align="left"><font color="#000000" size="1" face="Verdana"><br>Fecha de la Noticia'.						
										'  '.$dia."/".$mes."/".$year." ".$hora. '<br>'.
										'</font></td></tr></table></body>/</html>';
									
			
			$mail= & new htmlMimeMail();
			
			if(!$idNoticia){
			
				//echo "xxxx".$idNoticia;exit;
					//$eFrom = "noticias_del_dia-owner@CONVENIO_SITRADOC.gob.pe";
					$eFrom = "noticias_del_dia@CONVENIO_SITRADOC.gob.pe";
					$eDest = "noticias_del_dia@CONVENIO_SITRADOC.gob.pe";
						
						$mail->setSubject($desTitulo);
						$mail->setHtml($desMensaje);
						
						$mail->setFrom($eFrom);						
						$mail->setReturnPath($eFrom);
						$result = $mail->send(array($eDest));
				}
					

			if(!$RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= (!$RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu=";
			$destination .= $idNoticia ? $this->menu_items[2]['val'] : $this->menu_items[1]['val'];
			// echo $destination; exit;
			header("Location: $destination");
			exit;
		}
	}
}
?>
