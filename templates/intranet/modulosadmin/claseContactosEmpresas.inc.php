<?php
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class ContactosEmpresas extends Modulos{

	function ContactosEmpresas($menu,$subMenu){
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
			
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][1];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];

		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_CONTACTO' => 'frmSearchContactos',
							'BUSCA_CONTACTO' => 'searchContactos',
							'FRM_AGREGA_CONTACTO' => 'frmAddContactos',
							'AGREGA_CONTACTO' => 'addContactos',
							'FRM_MODIFICA_CONTACTO' => 'frmModifyContactos',
							'MODIFICA_CONTACTO' => 'modifyContactos',
							'FRM_BUSCA_EMPRESA' => 'frmSearchEmpresa',
							'BUSCA_EMPRESA' => 'searchEmpresa',
							'FRM_AGREGA_EMPRESA' => 'frmAddEmpresa',
							'AGREGA_EMPRESA' => 'addEmpresa',
							'FRM_MODIFICA_EMPRESA' => 'frmModifyEmpresa',
							'MODIFICA_EMPRESA' => 'modifyEmpresa',
							'BUENA_TRANS' => 'goodTrans',
							'MALA_TRANS' => 'badTrans'							
							);
							
							
		$this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchContactos', label => "CONTACTOS" ),
							1 => array ( 'val' => 'frmSearchEmpresa', label => 'EMPRESAS' )
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		/*
		// Items del Sub Menu para cada Menu
		if($this->menuPager==$this->menu_items[0]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchContactos', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddContactos', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyContactos', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchEmpresa', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddEmpresa', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyEmpresa', label => 'MODIFICAR' )
										 );
										*/ 
		// Items del Sub Menu para cada Menu
		if($this->menuPager==$this->menu_items[0]['val']){
			/*if($_SESSION['mod_ind_leer'])*/ $this->subMenu_items[] = array ( 'val' => 'frmSearchContactos', label => 'BUSCAR' );
			/*if($_SESSION['mod_ind_insertar'])*/ $this->subMenu_items[] =	array ( 'val' => 'frmAddContactos', label => 'AGREGAR' );
			/*if($_SESSION['mod_ind_modificar'])*/ $this->subMenu_items[] =	array ( 'val' => 'frmModifyContactos', label => 'MODIFICAR' );
		}elseif($this->menuPager==$this->menu_items[1]['val']){
			/*if($_SESSION['mod_ind_leer'])*/ $this->subMenu_items[] = array ( 'val' => 'frmSearchEmpresa', label => 'BUSCAR' );
			/*if($_SESSION['mod_ind_insertar'])*/ $this->subMenu_items[] =	array ( 'val' => 'frmAddEmpresa', label => 'AGREGAR' );
			/*if($_SESSION['mod_ind_modificar'])*/ $this->subMenu_items[] =	array ( 'val' => 'frmModifyEmpresa', label => 'MODIFICAR' );
		}
		
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];
	}

	/* Funcion para validar Email */
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	} 	
	/* Funcion para validar WebSite */
	function ComprobarWeb($web){ 
		$web_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($web) >= 8) && (substr_count($web,".") == 1) && (substr($web,0,1) != ".")){ 
		   if ((!strstr($web,"'")) && (!strstr($web,"\"")) && (!strstr($web,"\\")) && (!strstr($web,"\$")) && (!strstr($web," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($web,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($web, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($web,0,strlen($web) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $web_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($web_correcto) 
		   return true; 
		else 
		   return false; 
	} 	

	
	function MuestraStatTrans($accion){
	
		//echo "justomatrix";
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_CONTACTO]);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_EMPRESA]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/showStatTrans.inc.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaContacto();
	}
	
	function FormBuscaContacto($page=NULL,$desNombre=NULL,$codEmpresa=NULL,$codArea=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		// Contenido Select Area
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.d_area ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArea',$this->ObjFrmSelect($sql_st, $codArea, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, lower(nombre) ".
				  "FROM dbo.d_empresa ".
				  "ORDER BY 2";
		$html->assign('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmpresa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/contactos/search.tpl.php');
		if(!$search) $html->display('oti/directorio/footerArm.tpl.php');
	}
	
		function BuscaContacto($page,$desNombre,$codEmpresa,$codArea){

                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);

		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaContacto($page,$desNombre,$codEmpresa,$codArea,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		if(!empty($codArea)&&!is_null($codArea)&&$codArea!='none')
			$condConsulta[] = "codigo_area={$codArea}";
		if(!empty($codEmpresa)&&!is_null($codEmpresa)&&$codEmpresa!='none')
			$condConsulta[] = "codigo_empresa={$codEmpresa}";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);	

		$sql_SP = sprintf("EXECUTE sp_busIDContacto %s,%s,0,0",
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// echo $sql_SP;		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDContacto %s,%s,%d,%d",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Ejecuta SP para listar Datos de Cada Proyecto
					$sql_SP = sprintf("EXECUTE sp_busDatosContacto %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
							$html->append('arrCont', array('id' => $id[0],
													 	   'desc' => $row[0],
														   'empr' => ucwords($row[1]),
														   'area' => ucwords($row[2]),
														   'celu' => $row[3],
														   'mail' => $row[4],
														   'fijo' => $row[5]));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
					
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_CONTACTO], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oti/directorio/contactos/searchResult.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	}
	
	
	function FormAgregaContacto($desNom=NULL,$desApel=NULL,$codEmpresa=NULL,$codArea=NULL,$celular=NULL,$email=NULL,$anexo=NULL,$errors=false){

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddContactos';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNom',$desNom);
		$html->assign_by_ref('desApel',$desApel);
		$html->assign_by_ref('celular',$celular);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('anexo',$anexo);
		
		// Contenido Select Area
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.d_area ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArea',$this->ObjFrmSelect($sql_st, $codArea, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, lower(nombre) ".
				  "FROM dbo.d_empresa ".
				  "ORDER BY 2";
		$html->assign('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmpresa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/contactos/frmAddContacto.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	
	}

	function AgregaContacto($desNom,$desApel,$codEmpresa,$codArea,$celular,$email,$anexo){
		$tamanoNombre=strlen($desNom);
		$tamanoApellido=strlen($desApel);

		// Comprueba Valores	
		if(!$desNom){ $bNom = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$desApel){ $bApel = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$codArea){ $bArea = true; $this->errors .= 'La �rea debe ser especificada<br>'; }
		if(!$codEmpresa){ $bEmp = true; $this->errors .= 'La Empresa debe ser especificada<br>'; }
		if($celular&&!is_numeric($celular)){ $bcel = true; $this->errors .= 'El Celular debe ser un valor num�rico<br>'; }
		if((!$this->ComprobarEmail($email))&&($email!="")){ $bmail = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }
		if($anexo&&!is_numeric($anexo)){ $banexo = true; $this->errors .= 'El Anexo debe ser un valor num�rico<br>'; }
		if($tamanoNombre<3){ $bTam1 = true; $this->errors .= 'El menos 3 caracteres debe contener el nombre<br>'; }
		if($tamanoApellido<3){ $bTam2 = true; $this->errors .= 'El menos 3 caracteres debe contener el apellido<br>'; }
		
			$this->abreConnDB();
			//$this->conn->debug = true;
				$sql="select count(*) from dbo.d_contacto 
					   where Upper(nombres)='$desNom' and Upper(apellidos)='$desApel'";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe el contacto en el Directorio de CONVENIO_SITRADOC<br>'; }
				}		

		if($bNom||$bApel||$bArea||$bEmp||$bcel||$bmail||$banexo||$bTam1||$bTam2||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaContacto($desNom,$desApel,$codEmpresa,$codArea,$celular,$email,$anexo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;	
			$sql_SP = sprintf("EXECUTE sp_addContacto '%s','%s',%d,%d,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($desNom),
							  $this->PrepareParamSQL($desApel),
							  $this->PrepareParamSQL($codEmpresa),
							  $this->PrepareParamSQL($codArea),
							  ($celular) ? "'".$this->PrepareParamSQL($celular)."'" : 'NULL',
							  ($email) ? "'".$this->PrepareParamSQL($email)."'" : 'NULL',
							  ($anexo) ? "'".$this->PrepareParamSQL($anexo)."'" : 'NULL',
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	
	function FormModificaContacto($idContacto,$desNom=NULL,$desApel=NULL,$codEmpresa=NULL,$codArea=NULL,$celular=NULL,$email=NULL,$anexo=NULL,$errors=false,$reLoad=false){
		if(!$idContacto){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Obtiene Datos del Contacto
		if(!$reLoad){
			$this->abreConnDB();
			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("EXECUTE sp_listContacto %d",$this->PrepareParamSQL($idContacto));
			
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$desNom = $row[0];
					$desApel = $row[1];
					$codEmpresa = $row[2];
					$codArea = $row[3];
					$celular = $row[4];
					$email = $row[5];
					$anexo = $row[6];
					
				}
				$rs->Close();
			}
			unset($rs);
		}
		

		// Genera Objeto HTML
		$html = new Smarty;
		// Genera el Menu Pager de la Cabecera
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyContactos';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		// A�ade JavaScript para el envio del Formulario
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		
		// Setea Valores de los Campos del Formulario
	
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$idContacto);
		$html->assign_by_ref('desNom',$desNom);
		$html->assign_by_ref('desApel',$desApel);
		$html->assign_by_ref('celular',$celular);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('anexo',$anexo);
		
		// Contenido Select Area
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.d_area ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArea',$this->ObjFrmSelect($sql_st, $codArea, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, lower(nombre) ".
				  "FROM dbo.d_empresa ".
				  "ORDER BY 2";
		$html->assign('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmpresa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		
		// Setea el Valor para los codigos de Contactos a Modificar
		$html->assign_by_ref('codigo',$codigo);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/contactos/frmModifyContacto.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	}

	function ModificaContacto($idContacto,$desNom,$desApel,$codEmpresa,$codArea,$celular,$email,$anexo){
		$tamanoNombre=strlen($desNom);
		$tamanoApellido=strlen($desApel);

		// Comprueba Valores	
		if(!$desNom){ $bNom = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$desApel){ $bApel = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if($celular&&!is_numeric($celular)){ $bcel = true; $this->errors .= 'El celular debe ser un valor num�rico<br>'; }
		if(!$codArea){ $bArea = true; $this->errors .= 'El Area debe ser especificada<br>'; }
		if(!$codEmpresa){ $bEmp = true; $this->errors .= 'La Empresa debe ser especificada<br>'; }
		if((!$this->ComprobarEmail($email))&&($email!="")){ $bmail = true; $this->errors .= 'El mail debe ser correcto<br>'; }
		if($anexo&&!is_numeric($anexo)){ $banexo = true; $this->errors .= 'El anexo debe ser un valor num�rico<br>'; }
		if($tamanoNombre<3){ $bTam1 = true; $this->errors .= 'El menos 3 caracteres debe contener el nombre<br>'; }
		if($tamanoApellido<3){ $bTam2 = true; $this->errors .= 'El menos 3 caracteres debe contener el apellido<br>'; }		
		
			$this->abreConnDB();
			//$this->conn->debug = true;
				$sql="select count(*) from dbo.d_contacto 
					   where Upper(nombres)='$desNom' and Upper(apellidos)='$desApel'";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					//if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe el contacto en el Directorio de CONVENIO_SITRADOC<br>'; }
				}		

		if($bNom||$bApel||$bArea||$bEmp||$bcel||$bmail||$banexo||$bTam1||$bTam2||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaContacto($idContacto,$desNom,$desApel,$codEmpresa,$codArea,$celular,$email,$anexo,$errors,true);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE sp_modContacto %d,'%s','%s',%d,%d,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($idContacto),
							  $this->PrepareParamSQL($desNom),
							  $this->PrepareParamSQL($desApel),
							  $this->PrepareParamSQL($codEmpresa),
							  $this->PrepareParamSQL($codArea),
							  ($celular) ? "'".$this->PrepareParamSQL($celular)."'" : 'NULL' ,
							  ($email) ? "'".$this->PrepareParamSQL($email)."'" : 'NULL',
							  ($anexo) ? "'".$this->PrepareParamSQL($anexo)."'" : 'NULL',
							  $_SESSION['cod_usuario']);
			//echo $sql_SP;
			//exit();
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	
	function FormBuscaEmpresa($page=NULL,$desNombre=NULL,$codRubro=NULL,$codDistrito=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		// Contenido Select Rubro
		$sql_st = "SELECT codigo_rubro, lower(descripcion) ".
				  "FROM dbo.rubro ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRubro',$this->ObjFrmSelect($sql_st, $codRubro, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Distrito
		$sql_st = "SELECT codigo, lower(distrito) ".
				  "FROM dbo.d_distrito ".
				  "WHERE flag='1'".
				  "ORDER BY 2";
		$html->assign('selDistrito',$this->ObjFrmSelect($sql_st, $codDistrito, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/empresas/search.tpl.php');
		if(!$search) $html->display('oti/directorio/footerArm.tpl.php');
	}
	
	function BuscaEmpresa($page,$desNombre,$codRubro,$codDistrito){

                // Evalua que no contenga sentencias SQL

                $this->evaluaNoSql($desNombre);

		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaEmpresa($page,$desNombre,$codRubro,$codDistrito,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		if(!empty($codRubro)&&!is_null($codRubro)&&$codRubro!='none')
			$condConsulta[] = "codigo_rubro={$codRubro}";
		if(!empty($codDistrito)&&!is_null($codDistrito)&&$codDistrito!='none')
			$condConsulta[] = "codigo_distrito={$codDistrito}";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		$sql_SP = sprintf("EXECUTE sp_busIDEmpresa %s,%s,0,0",
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// echo $sql_SP;		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDEmpresa %s,%s,%d,%d",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Ejecuta SP para listar Datos de Cada Proyecto
					$sql_SP = sprintf("EXECUTE sp_busDatosEmpresa %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
							$html->append('arrEmp', array('id' => $id[0],
													 	   'desc' => $row[0],
														   'rubro' => ucwords($row[1]),
														   'dis' => ucwords($row[2]),
														   'dir' => ucwords($row[3]),
														   'mail' => $row[4],
														   'web' => $row[5],
														   'observacion' => ucfirst($row[6])));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
					
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_EMPRESA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oti/directorio/empresas/searchResult.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	}
	
	function FormAgregaEmpresa($desNom=NULL,$codRubro=NULL,$direccion=NULL,$codDistrito=NULL,$fax=NULL,$email=NULL,$web=NULL,$observacion=NULL,$telefono=NULL,$errors=false){

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddEmpresa';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNom',$desNom);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('web',$web);
		$html->assign_by_ref('observacion',$observacion);
		//$html->assign_by_ref('telefono',$telefono);
		
		// Contenido Select Rubro
		$sql_st = "SELECT codigo_rubro, lower(descripcion) ".
				  "FROM dbo.rubro ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRubro',$this->ObjFrmSelect($sql_st, $codRubro, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Distrito
		$sql_st = "SELECT codigo, lower(distrito) ".
				  "FROM dbo.d_distrito ".
				  "WHERE flag='1'".
				  "ORDER BY 2";
		$html->assign('selDistrito',$this->ObjFrmSelect($sql_st, $codDistrito, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/empresas/frmAddEmpresa.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	
	}

	function AgregaEmpresa($desNom,$codRubro,$direccion,$codDistrito,$fax,$email,$web,$observacion,$telefono){
		$tamanoDireccion=strlen($direccion);
		$tamanoRazonSocial=strlen($desNom);		
		
		// Comprueba Valores	
		if(!$desNom){ $bNom = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$codRubro){ $bRubro = true; $this->errors .= 'El Rubro debe ser especificado<br>'; }
		if(!$direccion){ $bDir = true; $this->errors .= 'La direcion debe ser especificado<br>'; }
		if(!$codDistrito){ $bDis = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }
		//if(!$fax&&!is_numeric($fax)){ $bfax = true; $this->errors .= 'El fax debe ser un valor num�rico<br>'; }
		if((!$this->ComprobarEmail($email))&&($email!="")){ $bmail = true; $this->errors .= 'El Email debe ser correcto<br>'; }
		if($tamanoRazonSocial<3){ $bTam1 = true; $this->errors .= 'El menos 3 caracteres debe contener la Raz�n Social<br>'; }
		if($tamanoDireccion<5){ $bTam2 = true; $this->errors .= 'El menos 5 caracteres debe contener la Direcci�n<br>'; }
		//if(!$web){ $bweb = true; $this->errors .= 'La Web debe ser indicada<br>'; }
		//if(!$observacion){ $bobserv = true; $this->errors .= 'La observaci�n debe ser especificada';}
		
			$this->abreConnDB();
			//$this->conn->debug = true;
				$sql="select count(*) from dbo.d_empresa
					   where Upper(nombre)='$desNom'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe la empresa en el Directorio de CONVENIO_SITRADOC<br>'; }
				}		

		if($bNom||$bRubro||$bDir||$bDis||$bmail||$bTam1||$bTam2||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaEmpresa($desNom,$codRubro,$direccion,$codDistrito,$fax,$email,$web,$observacion,$telefono,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;
			$this->conn->BeginTrans();

			$sql_SP = sprintf("EXECUTE sp_insEmpresa %d,'%s','%s',%s,%d,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($codRubro),
							  $this->PrepareParamSQL($desNom),
							  $this->PrepareParamSQL($direccion),
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "NULL",
							  $this->PrepareParamSQL($codDistrito),
							  ($email) ? "'".$this->PrepareParamSQL($email)."'" : "NULL",
							  ($web) ? "'".$this->PrepareParamSQL($web)."'" : 'NULL',
							  ($observacion) ? "'".$this->PrepareParamSQL($observacion)."'" : "NULL",
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow())
					list($RETVAL,$idEmpresa) = $row;
				else
					$RETVAL = 1;
				unset($rs);
				
				if(!$RETVAL){
					for($i=0;$i<count($telefono);$i++){
						$sql_SP = sprintf("EXEC sp_insTelefonoEmpresa %d,%s,'%s'",
										  $idEmpresa,
										  "'".$this->PrepareParamSQL($telefono[$i])."'",
										  $_SESSION['cod_usuario']);
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $this->conn->Execute($sql_SP)) ? $row[0] : 1;
							if($RETVAL) break;
						}
						unset($rs);
					}
				}
			}
			unset($rs);
			
			if($RETVAL)
				$this->conn->RollBackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
		}
	}
	
	
	function FormModificaEmpresa($idEmpresa,$desNom=NULL,$codRubro=NULL,$direccion=NULL,$codDistrito=NULL,$fax=NULL,$email=NULL,$web=NULL,$observacion=NULL,$telefono=NULL,$errors=false,$reLoad=false){
		// Almacena en una variable $codEmpresa el primer ID a modificar
		// del Array $id
		//$idEmpresa = $codigo[0];
		// Obtiene Datos de la Empresa
		if(!$idEmpresa){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		
		
		if(!$reLoad){
			$this->abreConnDB();
			// $this->conn->debug = true;
			// Obtiene los Datos de la Empresa
			$sql_SP = sprintf("EXECUTE sp_listDatosEmpresa %d",$this->PrepareParamSQL($idEmpresa));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$codRubro = $row[1];
					$desNom = $row[2];
					$direccion = $row[3];
					$fax = $row[4];
					$codDistrito = $row[5];
					$email = $row[6];
					$web = $row[7];
					$observacion=$row[8];
					
				}
				$rs->Close();
			}
			unset($rs);

			$sql_SP = sprintf("EXECUTE sp_listTelefonoEmpresa %d",$this->PrepareParamSQL($idEmpresa));
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$telefono[] = $row[0];
				$rs->Close();
			}
			unset($rs);
		}
		
		// Genera Objeto HTML
		$html = new Smarty;
		// Genera el Menu Pager de la Cabecera
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyEmpresa';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		// A�ade JavaScript para el envio del Formulario
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		
		// Setea Valores de los Campos del Formulario
	
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$idEmpresa);
		$html->assign_by_ref('desNom',$desNom);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('web',$web);
		$html->assign_by_ref('observacion',$observacion);
		$html->assign_by_ref('telefono',$telefono);
		
		// Contenido Select Rubro
		$sql_st = "SELECT codigo_rubro, lower(descripcion) ".
				  "FROM dbo.rubro ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRubro',$this->ObjFrmSelect($sql_st, $codRubro, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Distrito
		$sql_st = "SELECT codigo, lower(distrito) ".
				  "FROM dbo.d_distrito ".
				  "ORDER BY 2";
		$html->assign('selDistrito',$this->ObjFrmSelect($sql_st, $codDistrito, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		
		// Setea el Valor para los codigos de Contactos a Modificar
		$html->assign_by_ref('codigo',$codigo);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oti/directorio/headerArm.tpl.php');
		$html->display('oti/directorio/empresas/frmModifyEmpresa.tpl.php');
		$html->display('oti/directorio/footerArm.tpl.php');
	}

	function ModificaEmpresa($idEmpresa,$desNom,$codRubro,$direccion,$codDistrito,$fax,$email,$web,$observacion,$telefono){
		$tamanoDireccion=strlen($direccion);
		$tamanoRazonSocial=strlen($desNom);		

		// Comprueba Valores	
		if(!$desNom){ $bNom = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$direccion){ $bDir = true; $this->errors .= 'La Direccion debe ser especificado<br>'; }
		//if($fax&&!is_numeric($fax)){ $bfax = true; $this->errors .= 'El fax debe ser un valor num�rico<br>'; }
		if(!$codRubro){ $bRubro = true; $this->errors .= 'El Rubro debe ser especificada<br>'; }
		if(!$codDistrito){ $bDis = true; $this->errors .= 'El distrito debe ser especificada<br>'; }
		if((!$this->ComprobarEmail($email))&&($email!="")){ $bmail = true; $this->errors .= 'El mail debe ser correcto<br>'; }
		if($tamanoRazonSocial<3){ $bTam1 = true; $this->errors .= 'El menos 3 caracteres debe contener la Raz�n Social<br>'; }
		if($tamanoDireccion<5){ $bTam2 = true; $this->errors .= 'El menos 5 caracteres debe contener la Direcci�n<br>'; }		
		//if(!$web){ $bweb = true; $this->errors .= 'La Web debe ser especificada<br>'; }
		//if(!$observacion){ $bobserv = true; $this->errors .= 'La Observaci�n debe ser especificada';}
		
			$this->abreConnDB();
			//$this->conn->debug = true;
				$sql="select count(*) from dbo.d_empresa
					   where Upper(nombre)='$desNom'";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					//if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe la empresa en el Directorio de CONVENIO_SITRADOC<br>'; }
				}		

		if($bNom||$bDir||$bRubro||$bDis||$bmail||$bTam1||$bTam2||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaEmpresa($idEmpresa,$desNom,$codRubro,$direccion,$codDistrito,$fax,$email,$web,$observacion,$telefono,$errors,true);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE sp_modEmpresa %d,%d,'%s','%s',%s,%d,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($idEmpresa),
							  $this->PrepareParamSQL($codRubro),
							  $this->PrepareParamSQL($desNom),
							  $this->PrepareParamSQL($direccion),
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "NULL",
							  $this->PrepareParamSQL($codDistrito),
							  ($email) ? "'".$this->PrepareParamSQL($email)."'" : "NULL",
							  ($web) ? "'".$this->PrepareParamSQL($web)."'" : 'NULL',
							  ($observacion) ? "'".$this->PrepareParamSQL($observacion)."'" : "NULL",
							  $_SESSION['cod_usuario']);

			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
			unset($rs);
				
			if(!$RETVAL){
				$sql_SP = sprintf("EXECUTE sp_delTelefonoEmpresa %d",
								  $this->PrepareParamSQL($idEmpresa));
	
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				unset($rs);

				for($i=0;$i<count($telefono);$i++){
					$sql_SP = sprintf("EXEC sp_insTelefonoEmpresa %d,%s,'%s'",
									  $idEmpresa,
									  "'".$this->PrepareParamSQL($telefono[$i])."'",
									  $_SESSION['cod_usuario']);
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $this->conn->Execute($sql_SP)) ? $row[0] : 1;
						if($RETVAL) break;
					}
					unset($rs);
				}
			}
						
			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}

}
?>
