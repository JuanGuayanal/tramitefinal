<?php
// putenv("TDSDUMP=/tmp/Inventario.log");
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class EncuestaCapacitacion extends Modulos{

    function EncuestaCapacitacion($menu,$subMenu){
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;

		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][8];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_PC' => 'frmSearchPC',
							'BUSCA_PC' => 'searchPC',
							'MUESTRA_ATENCIONES_PC' => 'showAtenPC',
							'MUESTRA_DETALLE_PC' => 'showDetailPC',
							'FRM_AGREGA_PC' => 'frmAddPC',
							'AGREGA_PC' => 'addPC',							
							'FRM_MODIFICA_PC' => 'frmModifyPC',
							'MODIFICA_PC' => 'modifyPC',
							'FRM_BUSCA_PARTE' => 'frmSearchPart',
							'BUSCA_PARTE' => 'searchPart',
							'BUSCA_PARTE_EXTENDIDA' => 'searchPartExtended',
							'FRM_AGREGA_PARTE' => 'frmAddPart',
							'AGREGA_PARTE' => 'addPart',
							'FRM_MODIFICA_PARTE' => 'frmModifyPart',
							'MODIFICA_PARTE' => 'modifyPart',
							'ELIMINA_PARTE' => 'deletePart',
							'BUENA_TRANS' => 'goodTrans',
							'MALA_TRANS' => 'badTrans'							
							);

		$this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchPC', 'label' => "PC's" ),
							1 => array ( 'val' => 'frmSearchPart', 'label' => 'PARTES Y PIEZAS' )
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];

		// Items del Sub Menu para cada Menu
		if($this->menuPager==$this->menu_items[0]['val']){
			if($_SESSION['mod_ind_leer']) $this->subMenu_items[] = array ( 'val' => 'frmSearchPC', 'label' => 'BUSCAR' );
			if($_SESSION['mod_ind_insertar']||$this->userIntranet['CODIGO']==646) $this->subMenu_items[] =	array ( 'val' => 'frmAddPC', 'label' => 'AGREGAR' );
			if($_SESSION['mod_ind_modificar']||$this->userIntranet['CODIGO']==646) $this->subMenu_items[] =	array ( 'val' => 'frmModifyPC', 'label' => 'MODIFICAR' );
		}elseif($this->menuPager==$this->menu_items[1]['val']){
			if($_SESSION['mod_ind_leer']) $this->subMenu_items[] = array ( 'val' => 'frmSearchPart', 'label' => 'BUSCAR' );
			if($_SESSION['mod_ind_insertar']||$this->userIntranet['CODIGO']==646) $this->subMenu_items[] =	array ( 'val' => 'frmAddPart', 'label' => 'AGREGAR' );
			if($_SESSION['mod_ind_modificar']||$this->userIntranet['CODIGO']==646) $this->subMenu_items[] =	array ( 'val' => 'frmModifyPart', 'label' => 'MODIFICAR' );
		}
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];
    }
	
	function ValidaFechaInventariado($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion['FRM_BUSCA_PC']);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion['FRM_BUSCA_PARTE']);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('capacitacion/headerArm.tpl.php');
		$html->display('capacitacion/showStatTrans.inc.php');
		$html->display('capacitacion/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaInventariado();
	}
	
	function FormBuscaInventariado($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('capacitacion/headerArm.tpl.php');
		$html->display('capacitacion/pcs/search.tpl.php');
		if(!$search) $html->display('capacitacion/footerArm.tpl.php');
	}
	
	function BuscaInventariado($page,$tipBusqueda,$desNombre){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaInventariado($page,$tipBusqueda,$desNombre,true);

		// Ejecuta el conteo General
		
		$this->abreConnDB();
// $this->conn->debug = true;
		$sql_SP = sprintf("EXECUTE sp_busIDInven_intra %d,%s,0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL');
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);


		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDInven_intra %d,%s,%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($idCPU = $rsId->FetchRow()){
					// Obtiene Todos los Datos del Inventariado*/
					$sql_SP = sprintf("EXECUTE sp_busPCCPU_intra %d",$idCPU[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($cpuData = $rsData->FetchRow())
							$html->append('arrInv', array('id' => $idCPU[0],
														  'depe' => ucwords($cpuData[0]),
														  'trab' => ucwords($cpuData[1]),
														  'cInv' => $cpuData[2],
														  'desc' => $cpuData[3],
														  'cPat' =>	$cpuData[4]));
						$rsData->Close();
					}
					unset($rsData);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		if($this->userIntranet['CODIGO']==646)
			$_SESSION['mod_ind_modificar']="T";
		$html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('desSerial',mktime());
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion['BUSCA_PC'], true));

		// Muestra el Resultado de la Busqueda
		$html->display('capacitacion/searchResult.tpl.php');
		$html->display('capacitacion/footerArm.tpl.php');
	}

	
	function ListaAtencionesInventariado($idCPU){
		$this->abreConnDB();
		// $this->conn->debug = true;

		// Obtiene los Datos del Inventariado
		$sql_SP = sprintf("EXECUTE sp_listPCCPU_lite_intra %d, 1",$this->PrepareParamSQL($idCPU));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			if($row = $rs->FetchRow()){
				$desDependencia = $row[0];
				$desTrabajador = $row[1];
				$codInventariado = $row[2];
				$desNombre = $row[3];
			}
			// list(null,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
			//	 $idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran) = $rs->FetchRow();
			$rs->Close();
		}
		unset($rs);

		// Obtiene las Atenciones sobre el inventariado
		$sql_st = sprintf("EXECUTE sp_listAtencionesPC %d",
						  $this->PrepareParamSQL($idCPU));

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			// Crea el Objeto HTML
			$html = & new Smarty;
			$cUsr = $cDep = $loop = 0;
			while ($row = $rs->FetchRow()){
				if($loop==0){
					$userTemp = $row[0];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				if($userTemp!=$row[0]){
					$cUsr++;
					$userTemp = $row[0];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				$ate['users'][$cUsr]['ates'][] = array(	'moti'=>ucfirst($row[1]),
														// 'trab'=>ucwords($row[2]),
														// 'depe'=>$row[3],
														'obse'=>ucfirst($row[2]),
														'fech'=>$row[3],
														'tota'=>$row[4]/60);
				$loop++;
			}
			$rs->Close();
						
			$html->assign_by_ref('ate',$ate);
			
			$html->assign_by_ref('pcDep',$desDependencia);
			$html->assign_by_ref('pcTrab',$desTrabajador);
			$html->assign_by_ref('pcName',$desNombre);
			$html->assign_by_ref('pcCod',$codInventariado);
			
			setlocale(LC_TIME, $this->zonaHoraria);
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			
			$logo = '/var/www/intranet/img/800x600/img.rep.logo.13.gif';			
			$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];			
			$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports';
			
			$filename = sprintf("ate%s%s", '-'.mktime(), '-'.$idCPU);
			
			// Genera Archivo PDF			
			$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/inventario/pcs/reportes/reportAten1.tpl.php'),false,$logo,$options);

			$destination = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/reports/' . $filename . '.pdf';
			header("Location: $destination");
			exit;
			// $html->display('oti/inventario/pcs/reportes/reportAten1.tpl.php');
		}
	}

	
	function FormAgregaInventariado($codDep=NULL,$codTrabajador=NULL,$codInventariado=NULL,$desNombre=NULL,$codPatrimonial=NULL,
									$desSerial=NULL,$idTipo=NULL,$idMarca=NULL,$idModelo=NULL,$desTipo=NULL,$fecIngreso=NULL,
									$codEmp=NULL,$codEntrada=NULL,$numGaran=NULL,$idTipoDisp=NULL,$idUbiDisp=NULL,$idTecnoDisp=NULL,$idMarcaDisp=NULL,
									$idModeloDisp=NULL,$idDisp=NULL,$idDispNew=NULL,$idSoftNew=NULL,$idSoft=NULL,$errors=false){

		if(!$_SESSION['mod_ind_insertar']){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}
			
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso) ? $fecIngreso : NULL;

		if($idTipoDisp&&$idTipoDisp!='none'&&$idUbiDisp&&$idUbiDisp!='none'&&$idTecnoDisp&&$idTEcnoDisp!='none'&&
		   $idMarcaDisp&&$idMarcaDisp!='none'&&$idModeloDisp&&$idModeloDisp!='none'){
			$idTipoDisp = NULL; $idUbiDisp=NULL; $idTecnoDisp=NULL; $idMarcaDisp = NULL; $idModeloDisp = NULL;
		}
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddInventory';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.action=document.{$frmName}.action+pAction;\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAction,pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('codInventariado',$codInventariado);
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codPatrimonial',$codPatrimonial);
		$html->assign_by_ref('desSerial',$desSerial);
		$html->assign_by_ref('desTipo',$desTipo);
		$html->assign_by_ref('numGaran',$numGaran);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDep, true, true, array('val'=>'none','label'=>'No especificada')));
		unset($sql_st);
		
		// Contenido Select Usuario
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' ".
						  "ORDER BY 2",($codDep) ? $codDep : 0);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $codTrabajador, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);

		// Contenido Select Tipo CPU Inventariado
		$sql_st = "SELECT codigo_tipo, Lower(descripcion) ".
				  "FROM user_inventario.tipo_cpu ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Contenido Select Marca CPU Inventariado
		$sql_st = "SELECT codigo_marca, Lower(descripcion) ".
				  "FROM user_inventario.marca_cpu ".
				  "ORDER BY 2";
		$html->assign_by_ref('selMarca',$this->ObjFrmSelect($sql_st, $idMarca, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Modelo CPU Inventariado
		$sql_st = sprintf("SELECT codigo_modelo,Lower(descripcion) ".
						  "FROM user_inventario.modelo_cpu ".
						  "WHERE codigo_marca=%d ".
						  "ORDER BY 2",($idMarca) ? $idMarca : 0);
		$html->assign('selModelo',$this->ObjFrmSelect($sql_st, $idModelo, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Fecha Ingreso del CPU
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, Lower(nombre) ".
				  "FROM helpdesk.dbo.d_empresa ".
				  "WHERE flag='A' and codigo_rubro=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmp, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);
		
		// Contenido Select Entrada
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM user_inventario.tipo_entrada ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEntrada',$this->ObjFrmSelect($sql_st, $codEntrada, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);

		// Lista los Dispositivos ya Agregados
		$idDisp = (is_array($idDisp)) ? $idDisp : array();
		if($idDispNew)  array_push($idDisp, $idDispNew);
		if($idDisp&&count($idDisp)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idDisp);$i++){
				if(!empty($idDisp[$i])&&!is_null($idDisp[$i])){
					$idDisp[$i] = explode(',',$idDisp[$i]);
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("EXECUTE sp_listDispositivo_intra %d,%d,%d,%d,%d",
									  $this->PrepareParamSQL($idDisp[$i][0]),
									  $this->PrepareParamSQL($idDisp[$i][1]),
									  $this->PrepareParamSQL($idDisp[$i][2]),
									  $this->PrepareParamSQL($idDisp[$i][3]),
									  $this->PrepareParamSQL($idDisp[$i][4]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('disp', array('id' => implode(',',$idDisp[$i]),
														'tipo' => ucwords($row[0]),
														'ubic' => ucwords($row[1]),
														'tecn' => ucwords($row[2]),
														'marc' => ucwords($row[3]),
														'mode' => ucwords($row[4])));
						$rs->Close();
					}
					unset($rs);
				}
			}
		}
		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT codigo_dispositivo, Lower(descrip_dispositivo) ".
				  "FROM user_inventario.dispositivo_inventario ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoDisp',$this->ObjFrmSelect($sql_st, $idTipoDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Ubicacion Dispositivo
		$sql_st = "SELECT codigo_tipo_dispositivo, Lower(descrip_tipo_tecnologia) ".
				  "FROM user_inventario.tipo_dispositivo ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selUbiDisp',$this->ObjFrmSelect($sql_st, $idUbiDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Tecnologia Dispositivo
		$sql_st = sprintf("SELECT codigo_tipo_tecnologia, Lower(descrip_tipo_tecnologia) ".
						  "FROM user_inventario.tipo_tecnologia ".
						  "WHERE flag='A' and codigo_dispositivo=%d and codigo_tipo_dispositivo=%d ".
						  "ORDER BY 2",($idTipoDisp&&$idTipoDisp!='none') ? $idTipoDisp : 0, ($idUbiDisp&&$idUbiDisp!='none') ? $idUbiDisp : 0);
		$html->assign_by_ref('selTecnoDisp',$this->ObjFrmSelect($sql_st, $idTecnoDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Marca Dispositivo
		$sql_st = sprintf("SELECT codigo_marca_disp, Lower(descrip_marca_disp) ".
						  "FROM user_inventario.marca_dispositivo ".
						  "WHERE flag='A' and codigo_dispositivo=%d ".
						  "ORDER BY 2",($idTipoDisp&&$idTipoDisp!='none') ? $idTipoDisp : 0);
		$html->assign_by_ref('selMarcaDisp',$this->ObjFrmSelect($sql_st, $idMarcaDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Modelo Dispositivo
		$sql_st = sprintf("SELECT codigo_modelo, Lower(descripcion) ".
						  "FROM user_inventario.modelo_dispositivo ".
						  "WHERE flag='A' and codigo_marca=%d and codigo_tipo_dispositivo=%d and codigo_tipo_tecnologia=%d ".
						  "ORDER BY 2",($idMarcaDisp&&$idMarcaDisp!='none') ? $idMarcaDisp : 0, ($idUbiDisp&&$idUbiDisp!='none') ? $idUbiDisp : 0, ($idTecnoDisp&&$idTecnoDisp!='none') ? $idTecnoDisp : 0);
		$html->assign_by_ref('selModeloDisp',$this->ObjFrmSelect($sql_st, $idModeloDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Lista el Software ya Agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("EXECUTE sp_listSoftware_intra %d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'desc' => ucwords($row[0])));
						$rs->Close();
					}
					unset($rs);
				}
			}
		}

		// Contenido Select Software Nuevo
		$sql_st = "SELECT codigo_software, Lower(nombre_sw) ".
				  "FROM user_inventario.software ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSoftNew',$this->ObjFrmSelect($sql_st, NULL, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('capacitacion/headerArm.tpl.php');
		$html->display('capacitacion/frmAddEncuesta.tpl.php');
		$html->display('capacitacion/footerArm.tpl.php');
	
	}

	function AgregaInventariado($codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
								$idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran,$idDisp,$idSoft){

		if(!$_SESSION['mod_ind_insertar']){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}

		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		// Comprueba Valores
		if(!$codInventariado){ $bInv = true; $this->errors .= 'El Codigo Inventario debe ser especificado<br>'; }	
		if(!$desNombre){ $bNom = true; $this->errors .= 'El Nombre PC debe ser especificado<br>'; }
		if(!$idTipo){ $bTip = true; $this->errors .= 'El Tipo de CPU debe ser especificado<br>'; }
		if(!$idMarca){ $bMar = true; $this->errors .= 'La Marca CPU debe ser especificada<br>'; }
		if(!$idModelo){ $bMod = true; $this->errors .= 'El Modelo CPU debe ser especificado<br>'; }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaInventariado($fecIngreso,'Ingreso')); }

		if($bInv||$bNom||$bTip||$bMar||$bMod||$bFIng){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('inventarioinformatico_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaInventariado($codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
								$idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran,NULL,NULL,NULL,NULL,NULL,
								$idDisp,NULL,NULL,$idSoft,$errors);
			$objIntranet->Footer();
		}else{
			// Manipula los Arrays de Dispositivos y Software
			$idDisp = (is_array($idDisp)) ? $idDisp : array();
			$idSoft = (is_array($idSoft)) ? $idSoft : array();
			
			$this->abreConnDB();
			// $this->conn->debug = true;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE sp_insCPUInven_intra '%s','%s','%s','%s',%d,%d,%d,%s,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($codInventariado),
							  $this->PrepareParamSQL($desNombre),
							  ($codPatrimonial) ? $this->PrepareParamSQL($codPatrimonial) : NULL,
							  ($desSerial) ? $this->PrepareParamSQL($desSerial) : NULL,
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($idMarca),
							  $this->PrepareParamSQL($idModelo),
							  ($desTipo) ? "'".$this->PrepareParamSQL($desTipo)."'" : 'NULL',
							  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : 'NULL',
							  ($codEmp&&$codEmp!='none') ? $this->PrepareParamSQL($codEmp) : 'NULL',
							  ($numGaran) ? $this->PrepareParamSQL($numGaran) : 'NULL',
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
					$idCPU = $row[1];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
			if(!$RETVAL){
				// Inserta Dispositivos si los hubiese
				for($i=0;$i<count($idDisp);$i++){
					$idDisp[$i] = explode(',',$idDisp[$i]);
					
					// Inserta los Dispositivos correspondientes al CPU
					$sql_SP = sprintf("EXECUTE sp_insPartsInven_intra %d,%d,%d,%d,%d,'%s'",
									  $this->PrepareParamSQL($idDisp[$i][0]),
									  $this->PrepareParamSQL($idDisp[$i][1]),
									  $this->PrepareParamSQL($idDisp[$i][2]),
									  $this->PrepareParamSQL($idDisp[$i][3]),
									  $this->PrepareParamSQL($idDisp[$i][4]),
									  $_SESSION['cod_usuario']);
					// echo $sql_SP;
					$rsPart = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rsPart){
						$RETVAL=1;
						break;
					}else{
						if($partData = $rsPart->FetchRow()){
							$RETVAL = $partData[0];
							$idParte = $partData[1];
							
							if(!$RETVAL){
								// Inserta el registro de las Partes del CPU al Objeto PC
								$sql_SP = sprintf("EXECUTE sp_insPCInven_intra %d,%d,%s,%s,%s,'%s'",
												  $this->PrepareParamSQL($idCPU),
												  $this->PrepareParamSQL($idParte),
												  ($codTrabajador&&$codTrabajador!='none') ? $this->PrepareParamSQL($codTrabajador) : 0,
												  ($codDep&&$codDep!='none') ? $this->PrepareParamSQL($codDep) : 0,
												  ($codEntrada&&$codEntrada!='none') ? $this->PrepareParamSQL($codEntrada) : 'NULL',
												  $_SESSION['cod_usuario']);
								// echo $sql_SP;
								$rsPC = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rsPC){
									$RETVAL=1;
									break;
								}else{
									if($pcData = $rsPC->FetchRow()){
										$RETVAL = $pcData[0];
										if($RETVAL)
											break;
									}else{
										$RETVAL = 1;
										break;
									}
									$rsPC->Close();
								}
								unset($rsPC);
							}else
								break;
						}else{
							$RETVAL = 1;
							break;
						}
						$rsPart->Close();
					}
					unset($rsPart);					
				}
				
				if(!$RETVAL){
					// Inserta los Software's correspondientes al CPU
					for($i=0;$i<count($idSoft);$i++){
						$sql_SP = sprintf("EXECUTE sp_insSoftInven_intra %d,%d,'%s'",
										  $this->PrepareParamSQL($idCPU),
										  $this->PrepareParamSQL($idSoft[$i]),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaInventariado($idCPU,$codDep=NULL,$codTrabajador=NULL,$codInventariado=NULL,$desNombre=NULL,
									$codPatrimonial=NULL,$desSerial=NULL,$idTipo=NULL,$idMarca=NULL,$idModelo=NULL,
									$desTipo=NULL,$fecIngreso=NULL,$codEmp=NULL,$codEntrada=NULL,$numGaran=NULL,$idTipoDisp=NULL,
									$idUbiDisp=NULL,$idTecnoDisp=NULL,$idMarcaDisp=NULL,$idModeloDisp=NULL,$idDisp=NULL,
									$idDispNew=NULL,$idSoftNew=NULL,$idSoft=NULL,$reLoad=false,$errors=false){

		if((!$_SESSION['mod_ind_modificar']&& $this->userIntranet['CODIGO']!=646)||!$idCPU){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}
		
		// Limpia la variable $idCPU
		$idCPU = urldecode($idCPU);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyInventory';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.action=document.{$frmName}.action+pAction;\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAction,pAccion'));

		// Obtiene Datos del Inventariado
		if(!$reLoad){
			$this->abreConnDB();
			// $this->conn->debug = true;
			// Obtiene los Datos del Inventariado
			$sql_SP = sprintf("EXECUTE sp_listPCCPU_intra %d",$this->PrepareParamSQL($idCPU));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				list($codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
					 $idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran) = $rs->FetchRow();
				$rs->Close();
			}
			unset($rs);

			// Obtiene los Dispositivos del Inventariado
			$sql_SP = sprintf("EXECUTE sp_listDispCPU_intra %d",$this->PrepareParamSQL($idCPU));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$html->append('disp', array('id' => $row[0],
												'tipo' => ucwords($row[1]),
												'ubic' => ucwords($row[2]),
												'tecn' => ucwords($row[3]),
												'marc' => ucwords($row[4]),
												'mode' => ucwords($row[5])));
				$rs->Close();
			}
			unset($rs);

			// Obtiene los Software del Inventariado
			$sql_SP = sprintf("EXECUTE sp_listSoftCPU_intra %d",$this->PrepareParamSQL($idCPU));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$html->append('soft', array('id' => $row[0],
												'desc' => ucwords($row[1])));
				$rs->Close();
			}
			unset($rs);
		}

		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso) ? $fecIngreso : NULL;
		
		if($idTipoDisp&&$idTipoDisp!='none'&&$idUbiDisp&&$idUbiDisp!='none'&&$idTecnoDisp&&$idTEcnoDisp!='none'&&
		   $idMarcaDisp&&$idMarcaDisp!='none'&&$idModeloDisp&&$idModeloDisp!='none'){
			$idTipoDisp = NULL; $idUbiDisp=NULL; $idTecnoDisp=NULL; $idMarcaDisp = NULL; $idModeloDisp = NULL;
		}
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('codInventariado',$codInventariado);
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codPatrimonial',$codPatrimonial);
		$html->assign_by_ref('desSerial',$desSerial);
		$html->assign_by_ref('desTipo',$desTipo);
		$html->assign_by_ref('numGaran',$numGaran);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDep, true, true, array('val'=>'none','label'=>'No especificada')));
		unset($sql_st);
		
		// Contenido Select Usuario
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' ".
						  "ORDER BY 2",($codDep) ? $codDep : 0);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $codTrabajador, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);

		// Contenido Select Tipo CPU Inventariado
		$sql_st = "SELECT codigo_tipo, Lower(descripcion) ".
				  "FROM user_inventario.tipo_cpu ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Contenido Select Marca CPU Inventariado
		$sql_st = "SELECT codigo_marca, Lower(descripcion) ".
				  "FROM user_inventario.marca_cpu ".
				  "ORDER BY 2";
		$html->assign_by_ref('selMarca',$this->ObjFrmSelect($sql_st, $idMarca, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Modelo CPU Inventariado
		$sql_st = sprintf("SELECT codigo_modelo,Lower(descripcion) ".
						  "FROM user_inventario.modelo_cpu ".
						  "WHERE codigo_marca=%d ".
						  "ORDER BY 2",($idMarca) ? $idMarca : 0);
		$html->assign('selModelo',$this->ObjFrmSelect($sql_st, $idModelo, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha Ingreso del CPU
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, Lower(nombre) ".
				  "FROM helpdesk.dbo.d_empresa ".
				  "WHERE flag='A' and codigo_rubro=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmp, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);
		
		// Contenido Select Entrada
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM user_inventario.tipo_entrada ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEntrada',$this->ObjFrmSelect($sql_st, $codEntrada, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);

		// Lista los Dispositivos ya Agregados
		$idDisp = (is_array($idDisp)) ? $idDisp : array();
		
		// Si hay un dispositivo nuevo lo agrega al Array
		if($idDispNew)  array_push($idDisp, $idDispNew);
		if($idDisp&&count($idDisp)>0){
			$this->abreConnDB();
			//$this->conn->debug = true;
			for($i=0;$i<count($idDisp);$i++){
				if(!empty($idDisp[$i])&&!is_null($idDisp[$i])){
					// Obtiene los Datos de c/Dispositivo Agregado
					if(strstr($idDisp[$i],',')){
						$idDisp[$i] = explode(',',$idDisp[$i]);
						$sql_SP = sprintf("EXECUTE sp_listDispositivo_intra %d,%d,%d,%d,%d",
										  $this->PrepareParamSQL($idDisp[$i][0]),
										  $this->PrepareParamSQL($idDisp[$i][1]),
										  $this->PrepareParamSQL($idDisp[$i][2]),
										  $this->PrepareParamSQL($idDisp[$i][3]),
										  $this->PrepareParamSQL($idDisp[$i][4]));
					}else
						$sql_SP = sprintf("EXECUTE sp_listParteCPU_intra %d",
										  $this->PrepareParamSQL($idDisp[$i]));
	
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('disp', array('id' => (is_array($idDisp[$i])) ? implode(',',$idDisp[$i]) : $idDisp[$i],
														'tipo' => ucwords($row[0]),
														'ubic' => ucwords($row[1]),
														'tecn' => ucwords($row[2]),
														'marc' => ucwords($row[3]),
														'mode' => ucwords($row[4])));
						$rs->Close();
					}
					unset($rs);
				}
			}
		}
		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT codigo_dispositivo, Lower(descrip_dispositivo) ".
				  "FROM user_inventario.dispositivo_inventario ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoDisp',$this->ObjFrmSelect($sql_st, $idTipoDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Ubicacion Dispositivo
		$sql_st = "SELECT codigo_tipo_dispositivo, Lower(descrip_tipo_tecnologia) ".
				  "FROM user_inventario.tipo_dispositivo ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selUbiDisp',$this->ObjFrmSelect($sql_st, $idUbiDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Tecnologia Dispositivo
		$sql_st = sprintf("SELECT codigo_tipo_tecnologia, Lower(descrip_tipo_tecnologia) ".
						  "FROM user_inventario.tipo_tecnologia ".
						  "WHERE flag='A' and codigo_dispositivo=%d and codigo_tipo_dispositivo=%d ".
						  "ORDER BY 2",($idTipoDisp&&$idTipoDisp!='none') ? $idTipoDisp : 0, ($idUbiDisp&&$idUbiDisp!='none') ? $idUbiDisp : 0);
		$html->assign_by_ref('selTecnoDisp',$this->ObjFrmSelect($sql_st, $idTecnoDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Marca Dispositivo
		$sql_st = sprintf("SELECT codigo_marca_disp, Lower(descrip_marca_disp) ".
						  "FROM user_inventario.marca_dispositivo ".
						  "WHERE flag='A' and codigo_dispositivo=%d ".
						  "ORDER BY 2",($idTipoDisp&&$idTipoDisp!='none') ? $idTipoDisp : 0);
		$html->assign_by_ref('selMarcaDisp',$this->ObjFrmSelect($sql_st, $idMarcaDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Modelo Dispositivo
		$sql_st = sprintf("SELECT codigo_modelo, Lower(descripcion) ".
						  "FROM user_inventario.modelo_dispositivo ".
						  "WHERE flag='A' and codigo_marca=%d and codigo_tipo_dispositivo=%d and codigo_tipo_tecnologia=%d ".
						  "ORDER BY 2",($idMarcaDisp&&$idMarcaDisp!='none') ? $idMarcaDisp : 0, ($idUbiDisp&&$idUbiDisp!='none') ? $idUbiDisp : 0, ($idTecnoDisp&&$idTecnoDisp!='none') ? $idTecnoDisp : 0);
		$html->assign_by_ref('selModeloDisp',$this->ObjFrmSelect($sql_st, $idModeloDisp, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Lista el Software ya Agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("EXECUTE sp_listSoftware_intra %d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'desc' => ucwords($row[0])));
						$rs->Close();
					}
					unset($rs);
				}
			}
		}

		// Contenido Select Software Nuevo
		$sql_st = "SELECT codigo_software, Lower(nombre_sw) ".
				  "FROM user_inventario.software ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSoftNew',$this->ObjFrmSelect($sql_st, NULL, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Setea el ID del Inventariado que se esta Modificando
		$html->assign_by_ref('id',$idCPU);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oti/inventario/headerArm.tpl.php');
		$html->display('oti/inventario/pcs/frmModifyInventory.tpl.php');
		$html->display('oti/inventario/footerArm.tpl.php');
	}

	function ModificaInventariado($idCPU,$codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
								$idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran,$idDisp,$idSoft){

		if((!$_SESSION['mod_ind_modificar']&& $this->userIntranet['CODIGO']!=646)||!$idCPU){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}
		
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		// Comprueba Valores
		if(!$codInventariado){ $bInv = true; $this->errors .= 'El Codigo Inventario debe ser especificado<br>'; }	
		if(!$desNombre){ $bNom = true; $this->errors .= 'El Nombre PC debe ser especificado<br>'; }
		if(!$idTipo){ $bTip = true; $this->errors .= 'El Tipo de CPU debe ser especificado<br>'; }
		if(!$idMarca){ $bMar = true; $this->errors .= 'La Marca CPU debe ser especificada<br>'; }
		if(!$idModelo){ $bMod = true; $this->errors .= 'El Modelo CPU debe ser especificado<br>'; }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaInventariado($fecIngreso,'Ingreso')); }

		if($bInv||$bNom||$bTip||$bMar||$bMod||$bFIng){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('inventarioinformatico_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaInventariado($idCPU,$codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
								$idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran,NULL,NULL,NULL,NULL,NULL,
								$idDisp,NULL,NULL,$idSoft,true,$errors);
			$objIntranet->Footer();
		}else{
			// Manipula los Arrays de Dispositivos y Software
			$idDisp = (is_array($idDisp)) ? $idDisp : array();
			$idSoft = (is_array($idSoft)) ? $idSoft : array();
			
			$this->abreConnDB();
			// $this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 

			$sql_SP = sprintf("EXECUTE sp_modCPUInven_intra %d,'%s','%s','%s','%s',%d,%d,%d,%s,%s,%s,%s,'%s'",
							  $this->PrepareParamSQL($idCPU),
							  $this->PrepareParamSQL($codInventariado),
							  $this->PrepareParamSQL($desNombre),
							  ($codPatrimonial) ? $this->PrepareParamSQL($codPatrimonial) : NULL,
							  ($desSerial) ? $this->PrepareParamSQL($desSerial) : NULL,
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($idMarca),
							  $this->PrepareParamSQL($idModelo),
							  ($desTipo) ? "'".$this->PrepareParamSQL($desTipo)."'" : 'NULL',
							  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : 'NULL',
							  ($codEmp&&$codEmp!='none') ? $this->PrepareParamSQL($codEmp) : 'NULL',
							  ($numGaran) ? $this->PrepareParamSQL($numGaran) : 'NULL',
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow())
					$RETVAL = $row[0];
				else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
						
			if(!$RETVAL){
				function DispAnt($var){
					return (!strstr($var,','));
				}
				function DispNew($var){
					return (strstr($var,','));
				}
				$idDispAnt = array_filter($idDisp,"DispAnt");
				$idDisp = array_filter($idDisp,"DispNew");
				
				reset($idDispAnt);
				// Borra los Dispositivos Descartados si los hubiese
				$sql_SP = sprintf("EXECUTE sp_delPartesCPU_intra '%s','%d'",
								  (count($idDispAnt)>0) ? $this->PrepareParamSQL(implode(',',$idDispAnt)) : 0,
								  $this->PrepareParamSQL($idCPU));
				// echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL=0;
					$rs->Close();
				}
				unset($rs);
				
				if(!$RETVAL){
					// Modifica los Dispositivos Antiguos
					while(list($i) = each($idDispAnt)){
						$sql_SP = sprintf("EXECUTE sp_modPCCPU_intra %d,%d,%s,%s,%s,'%s'",
										  $this->PrepareParamSQL($idDispAnt[$i]),
										  $this->PrepareParamSQL($idCPU),
										  ($codTrabajador&&$codTrabajador!='none') ? $this->PrepareParamSQL($codTrabajador) : 0,
										  ($codDep&&$codDep!='none') ? $this->PrepareParamSQL($codDep) : 0,
										  ($codEntrada&&$codEntrada!='none') ? $this->PrepareParamSQL($codEntrada) : 'NULL',
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsPart = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsPart){
							$RETVAL=1;
							break;
						}else{
							if($partData = $rsPart->FetchRow())
								$RETVAL = $partData[0];
							else{
								$RETVAL = 1;
								break;
							}
							$rsPart->Close();
						}
					}
					
					if(!$RETVAL){
						// Inserta Dispositivos si los hubiese
						reset($idDisp);
						while(list($i) = each($idDisp)){
							$idDisp[$i] = explode(',',$idDisp[$i]);
							
							// Inserta los Dispositivos correspondientes al CPU
							$sql_SP = sprintf("EXECUTE sp_insPartsInven_intra %d,%d,%d,%d,%d,'%s'",
											  $this->PrepareParamSQL($idDisp[$i][0]),
											  $this->PrepareParamSQL($idDisp[$i][1]),
											  $this->PrepareParamSQL($idDisp[$i][2]),
											  $this->PrepareParamSQL($idDisp[$i][3]),
											  $this->PrepareParamSQL($idDisp[$i][4]),
											  $_SESSION['cod_usuario']);
							// echo $sql_SP;
							$rsPart = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rsPart){
								$RETVAL=1;
								break;
							}else{
								if($partData = $rsPart->FetchRow()){
									$RETVAL = $partData[0];
									$idParte = $partData[1];
									
									if(!$RETVAL){
										// Inserta el registro de las Partes del CPU al Objeto PC
										$sql_SP = sprintf("EXECUTE sp_insPCInven_intra %d,%d,%d,%d,%s,'%s'",
														  $this->PrepareParamSQL($idCPU),
														  $this->PrepareParamSQL($idParte),
														  ($codTrabajador&&$codTrabajador!='none') ? $this->PrepareParamSQL($codTrabajador) : 'NULL',
														  ($codDep&&$codDep!='none') ? $this->PrepareParamSQL($codDep) : 'NULL',
														  ($codEntrada&&$codEntrada!='none') ? $this->PrepareParamSQL($codEntrada) : 'NULL',
														  $_SESSION['cod_usuario']);
										// echo $sql_SP;
										$rsPC = & $this->conn->Execute($sql_SP);
										unset($sql_SP);
										if (!$rsPC){
											$RETVAL=1;
											break;
										}else{
											if($pcData = $rsPC->FetchRow()){
												$RETVAL = $pcData[0];
												if($RETVAL)
													break;
											}else{
												$RETVAL = 1;
												break;
											}
											$rsPC->Close();
										}
										unset($rsPC);
									}else
										break;
								}else{
									$RETVAL = 1;
									break;
								}
								$rsPart->Close();
							}
							unset($rsPart);					
						}
					}
				}
								
				if(!$RETVAL){
					// Borra los Software Antiguos si los hubiese
					$sql_SP = sprintf("EXECUTE sp_delSoftCPU_intra %d",
									  $this->PrepareParamSQL($idCPU));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else
						$rs->Close();
					unset($rs);

					// Inserta los Software's correspondientes al CPU
					for($i=0;$i<count($idSoft);$i++){
						$sql_SP = sprintf("EXECUTE sp_insSoftInven_intra %d,%d,'%s'",
										  $this->PrepareParamSQL($idCPU),
										  $this->PrepareParamSQL($idSoft[$i]),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();
				
			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function MuestraInventariado($idCPU){
		if(!$_SESSION['mod_ind_leer']||!$idCPU){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}
		
		// Limpia la variable $idCPU
		$idCPU = urldecode($idCPU);

		// Genera Objeto HTML
		$html = new Smarty;

		// Obtiene Datos del Inventariado
		$this->abreConnDB();
		// $this->conn->debug = true;
		// Obtiene los Datos del Inventariado
		$sql_SP = sprintf("EXECUTE sp_listPCCPU_lite_intra %d",$this->PrepareParamSQL($idCPU));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			list($codDep,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
				 $idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran) = $rs->FetchRow();
			$rs->Close();
		}
		unset($rs);

		// Obtiene los Dispositivos del Inventariado
		$sql_SP = sprintf("EXECUTE sp_listDispCPU_intra %d",$this->PrepareParamSQL($idCPU));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			while($row = $rs->FetchRow())
				$html->append('disp', array('id' => $row[0],
											'tipo' => ucwords($row[1]),
											'ubic' => ucwords($row[2]),
											'tecn' => ucwords($row[3]),
											'marc' => ucwords($row[4]),
											'mode' => ucwords($row[5]),
											'seri' => trim($row[6])));
			$rs->Close();
		}
		unset($rs);

		// Obtiene los Software del Inventariado
		$sql_SP = sprintf("EXECUTE sp_listSoftCPU_intra %d",$this->PrepareParamSQL($idCPU));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			while($row = $rs->FetchRow())
				$html->append('soft', array('id' => $row[0],
											'desc' => ucwords($row[1])));
			$rs->Close();
		}
		unset($rs);

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('codInventariado',$codInventariado);
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codPatrimonial',$codPatrimonial);
		$html->assign_by_ref('desSerial',$desSerial);
		$html->assign_by_ref('desTipo',$desTipo);
		$html->assign_by_ref('numGaran',$numGaran);
		
		// Dependencia
		$html->assign_by_ref('desDependencia',$codDep);
	
		// Usuario
		$html->assign_by_ref('desTrabajador',$codTrabajador);

		// Tipo CPU Inventariado
		$html->assign_by_ref('desTipo',$idTipo);

		// Marca CPU Inventariado
		$html->assign_by_ref('desMarca',$idMarca);
		
		// Modelo CPU Inventariado
		$html->assign('desModelo',$idModelo);
		
		// Fecha Ingreso del CPU
		$html->assign_by_ref('numMesIng',substr($fecIngreso,0,2));
		$html->assign_by_ref('numDiaIng',substr($fecIngreso,3,2));
		$html->assign_by_ref('numAnyoIng',substr($fecIngreso,6,4));
		
		// Empresa
		$html->assign_by_ref('desEmpresa',$codEmp);
		
		// Entrada
		$html->assign_by_ref('desEntrada',$codEntrada);

		// Fecha de Generacion del Reporte
		setlocale(LC_TIME, $this->zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = '/var/www/intranet/img/800x600/img.rep.logo.13.gif';		
		$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];		
		$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports';
		
		$filename = sprintf("pc%s%s", '-'.mktime(), '-'.$idCPU);
		
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/inventario/pcs/reportes/reportDetailInventory.tpl.php'),false,$logo,$options);

		$destination = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/reports/' . $filename . '.pdf';
		header("Location: $destination");
		exit;
		// Muestra el Resultado de la Busqueda
		// $html->display('oti/inventario/pcs/showInventory.tpl.php');

	}
	

	function FormBuscaPartesyPiezas($page=NULL,$idTipo=NULL,$idUbi=NULL,$idTecno=NULL,$idMarca=NULL,$idModelo=NULL,$idEstado=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('estado',$idEstado);

		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT codigo_dispositivo, Lower(descrip_dispositivo) ".
				  "FROM user_inventario.dispositivo_inventario ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Ubicacion Dispositivo
		$sql_st = "SELECT codigo_tipo_dispositivo, Lower(descrip_tipo_tecnologia) ".
				  "FROM user_inventario.tipo_dispositivo ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selUbi',$this->ObjFrmSelect($sql_st, $idUbi, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);

		// Contenido Select Tecnologia Dispositivo
		$sql_st = sprintf("SELECT codigo_tipo_tecnologia, Lower(descrip_tipo_tecnologia) ".
						  "FROM user_inventario.tipo_tecnologia ".
						  "WHERE flag='A' and codigo_dispositivo=%d and codigo_tipo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0);
		$html->assign_by_ref('selTecno',$this->ObjFrmSelect($sql_st, $idTecno, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);

		// Contenido Select Marca Dispositivo
		$sql_st = sprintf("SELECT codigo_marca_disp, Lower(descrip_marca_disp) ".
						  "FROM user_inventario.marca_dispositivo ".
						  "WHERE flag='A' and codigo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0);
		$html->assign_by_ref('selMarca',$this->ObjFrmSelect($sql_st, $idMarca, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);

		// Contenido Select Modelo Dispositivo
		$sql_st = sprintf("SELECT codigo_modelo, Lower(descripcion) ".
						  "FROM user_inventario.modelo_dispositivo ".
						  "WHERE flag='A' and codigo_marca=%d and codigo_tipo_dispositivo=%d and codigo_tipo_tecnologia=%d ".
						  "ORDER BY 2",($idMarca&&$idMarca!='none') ? $idMarca : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0, ($idTecno&&$idTecno!='none') ? $idTecno : 0);
		$html->assign_by_ref('selModelo',$this->ObjFrmSelect($sql_st, $idModelo, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('oti/inventario/headerArm.tpl.php');
		$html->display('oti/inventario/partes/search.tpl.php');
		if(!$search) $html->display('oti/inventario/footerArm.tpl.php');
	}

	function BuscaPartesyPiezas($page,$idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$idEstado){

		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaPartesyPiezas($page,$idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$idEstado,true);

		// Genera Objeto HTML
		$html = new Smarty;

		// Setea datos del Formulario
		$html->assign('frmName2','frmSearchExtended');
		$html->assign('frmName','frmData');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array('idTipo'=>$idTipo,
									'idUbi'=>$idUbi,
									'idTecno'=>$idTecno,
									'idMarca'=>$idMarca,
									'idModelo'=>$idModelo,
									'estado'=>$idEstado));
	
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		if(!empty($idTipo)&&!is_null($idTipo)&&$idTipo!='none')
			$condConsulta[] = "p.codigo_tip_disp={$idTipo}";
		if(!empty($idUbi)&&!is_null($idUbi)&&$idUbi!='none')
			$condConsulta[] = "p.codigo_tipo_dispositivo={$idUbi}";
		if(!empty($idTecno)&&!is_null($idTecno)&&$idTecno!='none')
			$condConsulta[] = "p.codigo_tipo_tecnologia={$idTecno}";
		if(!empty($idMarca)&&!is_null($idMarca)&&$idMarca!='none')
			$condConsulta[] = "p.codigo_marca_disp={$idMarca}";
		if(!empty($idModelo)&&!is_null($idModelo)&&$idModelo!='none')
			$condConsulta[] = "p.codigo_modelo={$idModelo}";
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = "p.flag='{$idEstado}'";
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		$sql_SP = sprintf("EXECUTE sp_busIDParte_intra %s,0,0",
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDParte_intra %s,%d,%d",
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($idParte = $rsId->FetchRow()){
					// Obtiene Todos los Datos de la Parte
					$sql_SP = sprintf("EXECUTE sp_busDatosParte_intra %d",$idParte[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($partData = $rsData->FetchRow())
							$html->append('arrPar', array('id' => $idParte[0],
														  'obse' => ucfirst($partData[0]),
														  'cPat' => $partData[1],
														  'nSer' => $partData[2],
														  'esta' => $partData[3],
														  'disp' =>	ucwords($partData[4]),
														  'ubic' => ucwords($partData[5]),
														  'tecn' => ucwords($partData[6]),
														  'marc' => ucwords($partData[7]),
														  'mode' => ucwords($partData[8]),
														  'desc' => ucfirst($partData[9]),
														  'orig' => $partData[10]));
						$rsData->Close();
					}
					unset($rsData);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		$html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscar', $this->arr_accion['BUSCA_PARTE'], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oti/inventario/partes/searchResult.tpl.php');
		$html->display('oti/inventario/footerArm.tpl.php');
	}

	function VistaExtendidaPartesyPiezas($idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$idEstado){
		// Genera Objeto HTML
		$html = new Smarty;

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		if(!empty($idTipo)&&!is_null($idTipo)&&$idTipo!='none')
			$condConsulta[] = "codigo_tip_disp={$idTipo}";
		if(!empty($idUbi)&&!is_null($idUbi)&&$idUbi!='none')
			$condConsulta[] = "codigo_tipo_dispositivo={$idUbi}";
		if(!empty($idTecno)&&!is_null($idTecno)&&$idTecno!='none')
			$condConsulta[] = "codigo_tipo_tecnologia={$idTecno}";
		if(!empty($idMarca)&&!is_null($idMarca)&&$idMarca!='none')
			$condConsulta[] = "codigo_marca_disp={$idMarca}";
		if(!empty($idModelo)&&!is_null($idModelo)&&$idModelo!='none')
			$condConsulta[] = "codigo_modelo={$idModelo}";
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = "p.flag='{$idEstado}'";
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busIDParte_intra %s,1,0",
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			while($idParte = $rsId->FetchRow()){
				// Obtiene Todos los Datos de la Parte
				$sql_SP = sprintf("EXECUTE sp_busDatosExtenParte_intra %d",$idParte[0]);
				//echo $sql_SP;
				$rsData = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if(!$rsData)
					print $this->conn->ErrorMsg();
				else{
					if($partData = $rsData->FetchRow())
						$html->append('arrPar', array('desc' => $partData[0],
													  'cPat' => $partData[1],
													  'nSer' => $partData[2],
													  'disp' =>	$partData[3],
													  'ubic' => $partData[4],
													  'tecn' => $partData[5],
													  'marc' => $partData[6],
													  'mode' => $partData[7],
													  'esta' => $partData[8],
													  'obse' => $partData[9]));
					$rsData->Close();
				}
				unset($rsData);
			}
			$rsId->Close();
		}
		unset($rsId);

		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=partesypiezas-".mktime().".csv");
		$html->display('oti/inventario/partes/viewExtended.tpl.php');
		exit;
	}

	function FormAgregaPartesyPiezas($desNombre=NULL,$codPatrimonial=NULL,$desSerial=NULL,$fecIngreso=NULL,
									 $codEmp=NULL,$numGaran=NULL,
									 $idTipo=NULL,$idUbi=NULL,$idTecno=NULL,$idMarca=NULL,$idModelo=NULL,
									 $indEstado=NULL,$indTrans=NULL,$codInventariado=NULL,$indCambio=NULL,
									 $idCambio=NULL,$indEstadoCambio=NULL,$numDuracionCambio=NULL,$desObservacion=NULL,$errors=false){

		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		if($indTrans=='Y'&&$codInventariado){
			$nInv = $this->ReturnOneRowSQL(sprintf("EXEC sp_busCodigoInventarioPC_intra '%s'",$codInventariado));
			if($nInv[0] == 0){
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia no existe.<br />';
				$indCambio = $codInventariado = null;
				$errors = & $this->muestraMensajeInfo($this->errors,true);
			}elseif($nInv[0] > 1){
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia existe en m�s de un equipo.<br />';
				$indCambio = $codInventariado = null;
				$errors = & $this->muestraMensajeInfo($this->errors,true);
			}
		}

		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPart';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.action=document.{$frmName}.action+pAction;\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAction,pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codPatrimonial',$codPatrimonial);
		$html->assign_by_ref('desSerial',$desSerial);
		$html->assign_by_ref('numGaran',$numGaran);
		$html->assign_by_ref('indEstado',$indEstado);
		$html->assign_by_ref('indTrans',$indTrans);
		$html->assign_by_ref('codInventariado',$codInventariado);
		$html->assign_by_ref('indCambio',$indCambio);
		$html->assign_by_ref('indEstadoCambio',$indEstadoCambio);
		$html->assign_by_ref('numDuracionCambio',$numDuracionCambio);
		$html->assign_by_ref('desObservacion',$desObservacion);

		// Fecha Ingreso del Dispositivo
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Contenido Select Empresa
		$sql_st = "SELECT codigo, Lower(nombre) ".
				  "FROM helpdesk.dbo.d_empresa ".
				  "WHERE flag='A' and codigo_rubro=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmp, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);
		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT codigo_dispositivo, Lower(descrip_dispositivo) ".
				  "FROM user_inventario.dispositivo_inventario ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Ubicacion Dispositivo
		$sql_st = "SELECT codigo_tipo_dispositivo, Lower(descrip_tipo_tecnologia) ".
				  "FROM user_inventario.tipo_dispositivo ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selUbi',$this->ObjFrmSelect($sql_st, $idUbi, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Tecnologia Dispositivo
		$sql_st = sprintf("SELECT codigo_tipo_tecnologia, Lower(descrip_tipo_tecnologia) ".
						  "FROM user_inventario.tipo_tecnologia ".
						  "WHERE flag='A' and codigo_dispositivo=%d and codigo_tipo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0);
		$html->assign_by_ref('selTecno',$this->ObjFrmSelect($sql_st, $idTecno, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Marca Dispositivo
		$sql_st = sprintf("SELECT codigo_marca_disp, Lower(descrip_marca_disp) ".
						  "FROM user_inventario.marca_dispositivo ".
						  "WHERE flag='A' and codigo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0);
		$html->assign_by_ref('selMarca',$this->ObjFrmSelect($sql_st, $idMarca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Modelo Dispositivo
		$sql_st = sprintf("SELECT codigo_modelo, Lower(descripcion) ".
						  "FROM user_inventario.modelo_dispositivo ".
						  "WHERE flag='A' and codigo_marca=%d and codigo_tipo_dispositivo=%d and codigo_tipo_tecnologia=%d ".
						  "ORDER BY 2",($idMarca&&$idMarca!='none') ? $idMarca : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0, ($idTecno&&$idTecno!='none') ? $idTecno : 0);
		$html->assign_by_ref('selModelo',$this->ObjFrmSelect($sql_st, $idModelo, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		if($indCambio){
			// Contenido Select Dispositivos al Cambio
			$sql_st = sprintf("EXEC sp_selPartePCCambio_intra '%s',%d",
							  $this->PrepareParamSQL($codInventariado),
							  $this->PrepareParamSQL($idTipo));
			$html->assign_by_ref('selCambio',$this->ObjFrmSelect($sql_st, $idCambio, true, true));
			unset($sql_st);
		}

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oti/inventario/headerArm.tpl.php');
		$html->display('oti/inventario/partes/frmAddPart.tpl.php');
		$html->display('oti/inventario/footerArm.tpl.php');
	}
	
	function AgregaPartesyPiezas($desNombre,$codPatrimonial,$desSerial,$fecIngreso,$codEmp,$numGaran,
								 $idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$indEstado,$indTrans,
								 $codInventariado,$indCambio,$idCambio,$indEstadoCambio,$numDuracionCambio,$desObservacion){
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;
		
		// Comprueba Valores
		$bNom = $bFIng = $bGar = $bTip = $bUbi = $bTec = $bMar = $bMod = $bEst = $bInv = $bInvE = $bDCam = $bCam = $bECa = false;
		
		if(!$desNombre){ $bNom = true; $this->errors .= 'La Descripci�n debe ser especificada<br />'; }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaInventariado($fecIngreso,'Ingreso')); }
		if($numGaran&&!is_numeric($numGaran)){ $bGar = true; $this->errors .= 'La Garant�a debe ser un valor numerico (meses).<br />'; }
		if(!$idTipo){ $bTip = true; $this->errors .= 'El Tipo de Dispositivo debe ser especificado<br />'; }
		if(!$idUbi){ $bUbi = true; $this->errors .= 'La Ubicaci�n del Dispositivo debe ser especificada<br />'; }
		if(!$idTecno){ $bTec = true; $this->errors .= 'La Tecnolog�a del Dispositivo debe ser especificada<br />'; }
		if(!$idMarca){ $bMar = true; $this->errors .= 'La Marca del Dispositivo debe ser especificada<br />'; }
		if(!$idModelo){ $bMod = true; $this->errors .= 'El Modelo del Dispositivo debe ser especificado<br />'; }
		if(!$indEstado){ $bEst = true; $this->errors .= 'El Estado del Dispositivo debe ser especificado<br />'; }
		if($indTrans=='Y'&&!$codInventariado){ $bInv = true; $this->errors .= 'El Cod. Inv. PC debe ser especificado si la parte se destina a una PC<br />'; }
		if($indTrans=='Y'&&$codInventariado){
			$nInv = $this->ReturnOneRowSQL(sprintf("EXEC sp_busCodigoInventarioPC_intra '%s'",$codInventariado));
			if($nInv[0] == 0){
				$bInvE = true;
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia no existe.<br />';
				$indCambio = $codInventariado = null;
			}elseif($nInv[0] > 1){
				$bInvE = true;
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia existe en m�s de un equipo.<br />';
				$indCambio = $codInventariado = null;
			}
		}
		if($indTrans=='Y'&&!($numDuracionCambio&&is_numeric($numDuracionCambio))){ $bDCam = true; $this->errors .= 'La Duracion Trans. debe ser especificada si la parte se destina a una PC. Adem�s, debe ser num�rica (minutos).<br>'; }
		if($indTrans=='Y'&&$indCambio&&!$idCambio){ $bCam = true; $this->errors .= 'El(Los) Dispositivos a Cambiar deben ser especificados<br>'; }
		if($indTrans=='Y'&&$indCambio&&!$indEstadoCambio){ $bECa = true; $this->errors .= 'El Estado de los Dispositivos a Cambiar debe ser especificado<br>'; }

		if($bNom||$bFIng||$bGar||$bTip||$bUbi||$bTec||$bMar||$bMod||$bEst||$bInv||$bInvE||$bDCam||$bCam||$bECa){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('inventarioinformatico_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaPartesyPiezas($desNombre,$codPatrimonial,$desSerial,$fecIngreso,$codEmp,$numGaran,
										  $idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$indEstado,$indTrans,
										  $codInventariado,$indCambio,$idCambio,$indEstadoCambio,$numDuracionCambio,$desObservacion,$errors);
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE sp_insParte_intra '%s','%s','%s',%s,%s,%s,%s,%d,%d,%d,%d,%d,'%s','%s','%s'",
							  $this->PrepareParamSQL($desNombre),
							  ($codPatrimonial) ? $this->PrepareParamSQL($codPatrimonial) : NULL,
							  ($desSerial) ? $this->PrepareParamSQL($desSerial) : NULL,
							  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : 'NULL',
							  ($codEmp&&$codEmp!='none') ? $this->PrepareParamSQL($codEmp) : 'NULL',
							  ($numGaran) ? $this->PrepareParamSQL($numGaran) : 'NULL',
							  ($desObservacion) ? "'".$this->PrepareParamSQL($desObservacion)."'" : 'NULL',
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($idUbi),
							  $this->PrepareParamSQL($idTecno),
							  $this->PrepareParamSQL($idMarca),
							  $this->PrepareParamSQL($idModelo),
							  $this->PrepareParamSQL($indEstado),
							  $this->PrepareParamSQL($indTrans),
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
					$idParte = $row[1];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
			if(!$RETVAL){
				if($indTrans=='Y'){
					// Transfiere la Parte a la PC Destino
					$sql_SP = sprintf("EXECUTE sp_ponePartePC_intra %d,'%s','%s'",
									  $this->PrepareParamSQL($idParte),
									  $this->PrepareParamSQL($codInventariado),
									  $_SESSION['cod_usuario']);
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						if($row = $rs->FetchRow())
							$RETVAL = $row[0];
						else
							$RETVAL = 1;
						$rs->Close();
					}
					unset($rs);
                    // Si la transferencia es por cambio, retira las partes correspondientes            
					if(!$RETVAL){
						if($indCambio){
							$sql_SP = sprintf("EXECUTE sp_sacaPartePC_intra '%s','%s','%s','%s'",
											  $this->PrepareParamSQL((is_array($idCambio)) ? implode(',',$idCambio) : $idCambio),
											  $this->PrepareParamSQL($codInventariado),
											  $this->PrepareParamSQL($indEstadoCambio),
											  $_SESSION['cod_usuario']);
							// echo $sql_SP;
							$rs = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rs)
								$RETVAL=1;
							else{
								if($row = $rs->FetchRow())
									$RETVAL = $row[0];
								else
									$RETVAL = 1;
								$rs->Close();
							}
							unset($rs);
						}
					}
					// Registra la Transferencia en el HelpDesk
					if(!$RETVAL){
						$sql_SP = sprintf("EXECUTE sp_regParteHelpDesk_intra %d, %d,'%s',%d,'%s','%s'",
										  ($indCambio) ? 5 : 1,
										  $this->PrepareParamSQL($idTipo),
										  $this->PrepareParamSQL($codInventariado),
										  $this->PrepareParamSQL($numDuracionCambio),
										  $this->PrepareParamSQL($desObservacion),
										  $_SESSION['cod_usuario']);
						 // echo $sql_SP;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							if($row = $rs->FetchRow())
								$RETVAL = $row[0];
							else
								$RETVAL = 1;
							$rs->Close();
						}
						unset($rs);
					}
				}
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}

	function FormModificaPartesyPiezas($idParte,$desNombre=NULL,$codPatrimonial=NULL,$desSerial=NULL,$fecIngreso=NULL,
									 $codEmp=NULL,$numGaran=NULL,
									 $idTipo=NULL,$idUbi=NULL,$idTecno=NULL,$idMarca=NULL,$idModelo=NULL,$indEstado=NULL,
									 $indTrans=NULL,$codInventariado=NULL,$indCambio=NULL,$idCambio=NULL,$indEstadoCambio=NULL,
									 $numDuracionCambio=NULL,$desObservacion=NULL,$reLoad=false,$errors=false){

		if(!$idParte){
			$this->MuestraStatTrans($this->arr_accion['MALA_TRANS']);
			return;
		}
		
		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		if($indTrans=='Y'&&$codInventariado){
			$nInv = $this->ReturnOneRowSQL(sprintf("EXEC sp_busCodigoInventarioPC_intra '%s'",$codInventariado));
			if($nInv[0] == 0){
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia no existe.<br />';
				$indCambio = $codInventariado = null;
				$errors = & $this->muestraMensajeInfo($this->errors,true);
			}elseif($nInv[0] > 1){
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia existe en m�s de un equipo.<br />';
				$indCambio = $codInventariado = null;
				$errors = & $this->muestraMensajeInfo($this->errors,true);
			}
		}

		// Abre la Conexion a la Base de Datos
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		// Obtiene Datos de la Parte
		if(!$reLoad){
			// Obtiene los Datos de la Parte
			$sql_SP = sprintf("EXECUTE sp_listDatosParte_intra %d",$this->PrepareParamSQL($idParte));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				list($desNombre,$codPatrimonial,$desSerial,$fecIngreso,$codEmp,$numGaran,$idTipo,$idUbi,
				     $idTecno,$idMarca,$idModelo,$indEstado,$desObservacion) = $rs->FetchRow();
				$rs->Close();
			}
			unset($rs);
		}
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyPart';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.action=document.{$frmName}.action+pAction;\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAction,pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codPatrimonial',$codPatrimonial);
		$html->assign_by_ref('desSerial',$desSerial);
		$html->assign_by_ref('numGaran',$numGaran);
		$html->assign_by_ref('indEstado',$indEstado);
		$html->assign_by_ref('indTrans',$indTrans);
		$html->assign_by_ref('codInventariado',$codInventariado);
		$html->assign_by_ref('indCambio',$indCambio);
		$html->assign_by_ref('indEstadoCambio',$indEstadoCambio);
		$html->assign_by_ref('numDuracionCambio',$numDuracionCambio);
		$html->assign_by_ref('desObservacion',$desObservacion);
		
		// Fecha Ingreso del Dispositivo
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));

		// Contenido Select Empresa
		$sql_st = "SELECT codigo, Lower(nombre) ".
				  "FROM helpdesk.dbo.d_empresa ".
				  "WHERE flag='A' and codigo_rubro=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEmpresa',$this->ObjFrmSelect($sql_st, $codEmp, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);
		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT codigo_dispositivo, Lower(descrip_dispositivo) ".
				  "FROM user_inventario.dispositivo_inventario ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Ubicacion Dispositivo
		$sql_st = "SELECT codigo_tipo_dispositivo, Lower(descrip_tipo_tecnologia) ".
				  "FROM user_inventario.tipo_dispositivo ".
				  "WHERE flag='A' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selUbi',$this->ObjFrmSelect($sql_st, $idUbi, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Tecnologia Dispositivo
		$sql_st = sprintf("SELECT codigo_tipo_tecnologia, Lower(descrip_tipo_tecnologia) ".
						  "FROM user_inventario.tipo_tecnologia ".
						  "WHERE flag='A' and codigo_dispositivo=%d and codigo_tipo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0);
		$html->assign_by_ref('selTecno',$this->ObjFrmSelect($sql_st, $idTecno, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Marca Dispositivo
		$sql_st = sprintf("SELECT codigo_marca_disp, Lower(descrip_marca_disp) ".
						  "FROM user_inventario.marca_dispositivo ".
						  "WHERE flag='A' and codigo_dispositivo=%d ".
						  "ORDER BY 2",($idTipo&&$idTipo!='none') ? $idTipo : 0);
		$html->assign_by_ref('selMarca',$this->ObjFrmSelect($sql_st, $idMarca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select Modelo Dispositivo
		$sql_st = sprintf("SELECT codigo_modelo, Lower(descripcion) ".
						  "FROM user_inventario.modelo_dispositivo ".
						  "WHERE flag='A' and codigo_marca=%d and codigo_tipo_dispositivo=%d and codigo_tipo_tecnologia=%d ".
						  "ORDER BY 2",($idMarca&&$idMarca!='none') ? $idMarca : 0, ($idUbi&&$idUbi!='none') ? $idUbi : 0, ($idTecno&&$idTecno!='none') ? $idTecno : 0);
		$html->assign_by_ref('selModelo',$this->ObjFrmSelect($sql_st, $idModelo, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		if($indCambio){
			// Contenido Select Dispositivos al Cambio
			$sql_st = sprintf("EXEC sp_selPartePCCambio_intra '%s',%d",
							  $this->PrepareParamSQL($codInventariado),
							  $this->PrepareParamSQL($idTipo));
			$html->assign_by_ref('selCambio',$this->ObjFrmSelect($sql_st, $idCambio, true, true));
			unset($sql_st);
		}
		
		// Setea el Valor del ID de la Parte a Modificar
		$html->assign_by_ref('id',$idParte);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oti/inventario/headerArm.tpl.php');
		$html->display('oti/inventario/partes/frmModifyPart.tpl.php');
		$html->display('oti/inventario/footerArm.tpl.php');
	}

	function ModificaPartesyPiezas($idParte,$desNombre,$codPatrimonial,$desSerial,$fecIngreso,$codEmp,$numGaran,
								 $idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$indEstado,$indTrans,
								 $codInventariado,$indCambio,$idCambio,$indEstadoCambio,$numDuracionCambio,$desObservacion){

		//Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;

		// Comprueba Valores
		$bNom = $bFIng = $bGar = $bTip = $bUbi = $bTec = $bMar = $bMod = $bEst = $bInv = $bInvE = $bDCam = $bCam = $bECa = false;
		
		if(!$desNombre){ $bNom = true; $this->errors .= 'La Descripci�n debe ser especificada<br />'; }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaInventariado($fecIngreso,'Ingreso')); }
		if($numGaran&&!is_numeric($numGaran)){ $bGar = true; $this->errors .= 'La Garant�a debe ser un valor numerico (meses).<br />'; }
		if(!$idTipo){ $bTip = true; $this->errors .= 'El Tipo de Dispositivo debe ser especificado<br />'; }
		if(!$idUbi){ $bUbi = true; $this->errors .= 'La Ubicaci�n del Dispositivo debe ser especificada<br />'; }
		if(!$idTecno){ $bTec = true; $this->errors .= 'La Tecnolog�a del Dispositivo debe ser especificada<br />'; }
		if(!$idMarca){ $bMar = true; $this->errors .= 'La Marca del Dispositivo debe ser especificada<br />'; }
		if(!$idModelo){ $bMod = true; $this->errors .= 'El Modelo del Dispositivo debe ser especificado<br />'; }
		if(!$indEstado){ $bEst = true; $this->errors .= 'El Estado del Dispositivo debe ser especificado<br />'; }
		if($indTrans=='Y'&&!$codInventariado){ $bInv = true; $this->errors .= 'El Cod. Inv. PC debe ser especificado si la parte se destina a una PC<br />'; }
		if($indTrans=='Y'&&$codInventariado){
			$nInv = $this->ReturnOneRowSQL(sprintf("EXEC sp_busCodigoInventarioPC_intra '%s'",$codInventariado));
			if($nInv[0] == 0){
				$bInvE = true;
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia no existe.<br />';
				$indCambio = $codInventariado = null;
			}elseif($nInv[0] > 1){
				$bInvE = true;
				$this->errors .= 'El Cod. Inv. PC especificado (' . $codInventariado . ') para la transferencia existe en m�s de un equipo.<br />';
				$indCambio = $codInventariado = null;
			}
		}
		if($indTrans=='Y'&&!($numDuracionCambio&&is_numeric($numDuracionCambio))){ $bDCam = true; $this->errors .= 'La Duracion Trans. debe ser especificada si la parte se destina a una PC. Adem�s, debe ser num�rica (minutos).<br>'; }
		if($indTrans=='Y'&&$indCambio&&!$idCambio){ $bCam = true; $this->errors .= 'El(Los) Dispositivos a Cambiar deben ser especificados<br>'; }
		if($indTrans=='Y'&&$indCambio&&!$indEstadoCambio){ $bECa = true; $this->errors .= 'El Estado de los Dispositivos a Cambiar debe ser especificado<br>'; }

		if($bNom||$bFIng||$bGar||$bTip||$bUbi||$bTec||$bMar||$bMod||$bEst||$bInv||$bInvE||$bDCam||$bCam||$bECa){
			$objIntranet = new Intranet();
			$objIntranet->Header('Inventario Informatico',false,array('suspEmb'));
			$objIntranet->Body('inventarioinformatico_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaPartesyPiezas($idParte,$desNombre,$codPatrimonial,$desSerial,$fecIngreso,$codEmp,$numGaran,
								 $idTipo,$idUbi,$idTecno,$idMarca,$idModelo,$indEstado,$indTrans,
								 $codInventariado,$indCambio,$idCambio,$indEstadoCambio,$numDuracionCambio,$desObservacion,1,$errors);
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE sp_modParte_intra %d,'%s','%s','%s',%s,%s,%s,%s,%d,%d,%d,%d,%d,'%s','%s','%s'",
							  $this->PrepareParamSQL($idParte),
							  $this->PrepareParamSQL($desNombre),
							  ($codPatrimonial) ? $this->PrepareParamSQL($codPatrimonial) : NULL,
							  ($desSerial) ? $this->PrepareParamSQL($desSerial) : NULL,
							  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : 'NULL',
							  ($codEmp&&$codEmp!='none') ? $this->PrepareParamSQL($codEmp) : 'NULL',
							  ($numGaran) ? $this->PrepareParamSQL($numGaran) : 'NULL',
							  ($desObservacion) ? "'".$this->PrepareParamSQL($desObservacion)."'" : 'NULL',
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($idUbi),
							  $this->PrepareParamSQL($idTecno),
							  $this->PrepareParamSQL($idMarca),
							  $this->PrepareParamSQL($idModelo),
							  $this->PrepareParamSQL($indEstado),
							  $this->PrepareParamSQL($indTrans),
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;
			
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow())
					$RETVAL = $row[0];
				else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);

			if(!$RETVAL){
				if($indTrans=='Y'){
					// Transfiere la Parte a la PC Destino
					$sql_SP = sprintf("EXECUTE sp_ponePartePC_intra %d,'%s','%s'",
									  $this->PrepareParamSQL($idParte),
									  $this->PrepareParamSQL($codInventariado),
									  $_SESSION['cod_usuario']);
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						if($row = $rs->FetchRow())
							$RETVAL = $row[0];
						else
							$RETVAL = 1;
						$rs->Close();
					}
					unset($rs);
                    // Si la transferencia es por cambio, retira las partes correspondientes            
					if(!$RETVAL){
						if($indCambio){
							$sql_SP = sprintf("EXECUTE sp_sacaPartePC_intra '%s','%s','%s','%s'",
											  $this->PrepareParamSQL((is_array($idCambio)) ? implode(',',$idCambio) : $idCambio),
											  $this->PrepareParamSQL($codInventariado),
											  $this->PrepareParamSQL($indEstadoCambio),
											  $_SESSION['cod_usuario']);
							// echo $sql_SP;
							$rs = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rs)
								$RETVAL=1;
							else{
								if($row = $rs->FetchRow())
									$RETVAL = $row[0];
								else
									$RETVAL = 1;
								$rs->Close();
							}
							unset($rs);
						}
					}
					// Registra la Transferencia en el HelpDesk
					if(!$RETVAL){
						$sql_SP = sprintf("EXECUTE sp_regParteHelpDesk_intra %d, %d,'%s',%d,'%s','%s'",
										  ($indCambio) ? 5 : 1,
										  $this->PrepareParamSQL($idTipo),
										  $this->PrepareParamSQL($codInventariado),
										  $this->PrepareParamSQL($numDuracionCambio),
										  $this->PrepareParamSQL($desObservacion),
										  $_SESSION['cod_usuario']);
						 // echo $sql_SP;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							if($row = $rs->FetchRow())
								$RETVAL = $row[0];
							else
								$RETVAL = 1;
							$rs->Close();
						}
						unset($rs);
					}
				}
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function EliminaPartesyPiezas($id){
		$ids = implode(",",$id);
		
		$this->abreConnDB();
		// $this->conn->debug = true;

		// Inicia la Transaccion
		$this->conn->BeginTrans(); 		
		
		$sql_SP = sprintf("EXECUTE sp_daDeBajaParte_intra '%s', '%s'",
						  $this->PrepareParamSQL($ids),
						  $_SESSION['cod_usuario']);
		 // echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			$RETVAL=1;
		else{
			if($row = $rs->FetchRow())
				$RETVAL = $row[0];
			else
				$RETVAL = 1;
			$rs->Close();
		}
		unset($rs);
		
		var_dump($RETVAL);

		if($RETVAL)
			$this->conn->RollbackTrans();
		else
			$this->conn->CommitTrans();

		$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
		$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
		$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[2]['val']}";
		header("Location: $destination");
		exit;

	}

}
?>
