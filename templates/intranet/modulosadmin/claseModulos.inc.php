<?
error_reporting(E_WARNING);
require_once('claseObjetosForm.inc.php');

class Modulos extends ObjetosForm{

	var $modInfo;
	var $menu_items;
	var $menuPager;
	var $subMenu_items;
	var $subMenuPager;
	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* CLASE MODULOS												 */
	/* ------------------------------------------------------------- */
	function Modulos(){
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][0];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	}

        // Funcion para mostrar el envio de mensajes grupales

        function mensajesGrupos(){

                $idusr= $_SESSION['cod_usuario'];
#                $sql_st = "SELECT publicado, estado, codigo_trabajador FROM db_general.dbo.h_trabajador ".
#                          "WHERE (email='${idusr}') AND ((publicado=1 AND estado='ACTIVO') or codigo_trabajador ".
#                          "in (253,884,1047,1048,772,1054,111,153,1092,276,1121,446) or email in ('jdamian','oespinoza','icarazo','jrodriguez','oada','cpiazzini','psolorzano','frichter','departamentomedico','ggonzalesv' ))";

                $sql_st = "SELECT publicado, estado, codigo_trabajador FROM db_general.dbo.h_trabajador ".
                          "WHERE email='${idusr}' and email in ('orh','odn','frichter','ocii','jdamian','ogtie','dcruzb','prensa') ";

                $conn1 = & ADONewConnection('mssql');

                if(!$conn1->Connect(CJDBMSSQL, CJDBMSSQLUSER, CJDBMSSQLPASS))
                        print $conn1->ErrorMsg();

                $rs = $conn1->Execute($sql_st);
                unset($sql_st);

                if (!$rs){
                        print $conn1->ErrorMsg();
                }else{
                        $html_tr = NULL;
                        while ($row = $rs->FetchRow()) {
                                $html_tr="<a href=/msggrupos.php ><img src=\"/img/lb_msggrp.gif\" width=\"113\" height=\"17\" name=\"ico_hea2\" border=\"0\"></a> ";
																/*$html_tr="<a href=/msggrupos.php onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('lb_ico','','/img/lb_msggrp.gif','ico_hea2','','/img/ico_msggrp_o.gif',0)\"><img src=\"/img/ico_msggrp.gif \" width=\"18\" height=\"17\" name=\"ico_hea2\" border=\"0\"></a> ";*/

                                return $html_tr;
                        }
                }
                unset($row);
                $rs->Close(); # optional
                unset($rs);
                $conn1->Close();
                unset($conn1);        
	}

	
	/* ----------------------------------------------------- */
	/* Funciones para desplegar un Menu Pager para las       */
	/* Aplicaciones del Modulo.								 */
	/* ----------------------------------------------------- */
	/* CLASE MODULOS										 */
	/* ----------------------------------------------------- */

	
	function AsignaStatItemsMenu(){
		while(list($i) = each($this->menu_items)){
			if($this->menuPager == $this->menu_items[$i]['val'])
				$this->menu_items[$i]['stat'] = true;
			else
				$this->menu_items[$i]['stat'] = false;
		}
	}
	
	function GeneraMenuPager($tpl=1,$fetch=true){
		$this->AsignaStatItemsMenu();
		
		end($this->menu_items);
		$kEnd = key($this->menu_items);
		reset($this->menu_items);
		$kIni = key($this->menu_items);
		while(list($i) = each($this->menu_items)) {
			if($this->menu_items[$i]['stat']){
				if($i == $kIni){
					$this->menu_items[$i]['img'][] = 'item_first.on.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.on.gif';
					$this->menu_items[$i]['img'][] = ($i != $kEnd) ? 'item.on.off.gif' : 'item_last.on.gif';
				}elseif($i == $kEnd){
					$this->menu_items[$i]['img'][] = 'item.off.on.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.on.gif';
					$this->menu_items[$i]['img'][] = 'item_last.on.gif';
				}else{
					$this->menu_items[$i]['img'][] = 'item.off.on.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.on.gif';
					$this->menu_items[$i]['img'][] = 'item.on.off.gif';
				}
			}else{
				if($i == $kIni){
					$this->menu_items[$i]['img'][] = 'item_first.off.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.off.gif';
				}elseif($i == $kEnd){
					if(count($this->menu_items[$i_prev]['img'])<3)
						$this->menu_items[$i]['img'][] = 'item.off.off.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.off.gif';
					$this->menu_items[$i]['img'][] = 'item_last.off.gif';
				}else{
					if(count($this->menu_items[$i_prev]['img'])<3)
						$this->menu_items[$i]['img'][] = 'item.off.off.gif';
					$this->menu_items[$i]['img'][] = 'item_fnd.off.gif';
				}
			}
			$i_prev = $i;
		}
		
		// Crea el Objeto HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('menuItems',$this->menu_items);
		if($fetch)
			return $html->fetch("modulos/menuPager{$tpl}.tpl.php");
		else
			$html->display("modulos/menuPager{$tpl}.tpl.php");
	}

	/* ----------------------------------------------------- */
	/* Funciones para desplegar un Sub Menu Pager para las   */
	/* Aplicaciones del Modulo.								 */
	/* ----------------------------------------------------- */
	/* CLASE MODULOS										 */
	/* ----------------------------------------------------- */

	
	function AsignaStatItemsSubMenu(){
		for($i=0;$i<count($this->subMenu_items);$i++){
			if($this->subMenuPager == $this->subMenu_items[$i]['val'])
				$this->subMenu_items[$i]['stat'] = true;
			else
				$this->subMenu_items[$i]['stat'] = false;
		}
	}
	
	function GeneraSubMenuPager($tpl=1){
	
		$this->AsignaStatItemsSubMenu();
		
		// Crea el Objeto HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('menuItems',$this->subMenu_items);
		return $html->fetch("modulos/subMenuPager{$tpl}.tpl.php");
	}

	/* ----------------------------------------------------- */
	/* Funcion para obtener los datos relacionados al Modulo */
	/* ----------------------------------------------------- */
	/* CLASE MODULOS										 */
	/* ----------------------------------------------------- */

	function ModuloInfo($id){
		$this->abreConnDB();
		
		$sql_st = "SELECT des_path ".
				  "FROM modulo ".
				  "WHERE id_modulo={$id}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) 
				$this->modInfo = array('PATH' => $row[0]);
			else{
				$this->userIntranet = false;
				$this->errors .= "El Modulo no existe.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}
	
	/* ----------------------------------------------------- */
	/* Funcion para mostrar mensaje de Informacion de		 */
	/* un Proceso Realizado									 */
	/* ----------------------------------------------------- */
	/* CLASE MODULOS										 */
	/* ----------------------------------------------------- */

	function muestraMensajeInfo($msg,$fetch=false){
		$html = new Smarty;
		$html->assign('msg',$msg);
		if($fetch)
			return $html->fetch('modulos/msginfo.tpl.php');
		else
			$html->display('modulos/msginfo.tpl.php');
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}

	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar todos los 								 */
	/* Modulo a traves de una tabla HTML de 3 columnas
	/* ------------------------------------------------------------- */
	/* CLASE MODULOS												 */
	/* ------------------------------------------------------------- */

	function muestraModulos($idmod,$mode=0,$tipo=1,$fetch=false){}

	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar todos los Submodulos de un determinado	 */
	/* Modulo a traves de una tabla HTML de 3 columnas
	/* ------------------------------------------------------------- */
	/* CLASE MODULOS												 */
	/* ------------------------------------------------------------- */

	function muestraSubmodulos($idmod,$nivel=1,$mode=0,$tipo=1,$fetch=false){}

	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar todos los Submodulos de un determinado	 */
	/* Modulo a traves de una tabla HTML de 3 columnas
	/* ------------------------------------------------------------- */
	/* CLASE MODULOS												 */
	/* ------------------------------------------------------------- */

	function muestraIndexSubmodulosHTML($idmod,$mode=0,$tipo=1,$fetch=false){}

	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar Interface Principal para la				 */
	/* Administracion de Modulos (Index)							 */
	/* ------------------------------------------------------------- */
	/* CLASE MODULOS												 */
	/* ------------------------------------------------------------- */
	
	function muestraModuloAdminIndexHTML($txt_busca=NULL,$id_modulotipo=1){}


	
	/* ---------------------------------------------------------------- */
	/* Funcion para mostrar el Formulario HTML para Ins o Mod un Modulo */
	/* ---------------------------------------------------------------- */
	/* CLASE MODULOS 												    */
	/* ---------------------------------------------------------------- */
		
	function insertaModuloHTML($datos=NULL,$id_dependencia=NULL,$id_subdependencia=NULL,$cod_usuario=NULL,$accion=ACCION_INSERTAR_FALSE,$id=NULL){}
	
	/* -------------------------------------------------------------- */
	/* Funcion para Limpiar Datos que van desde el Formulario HTML	  */
	/* hacia las Funciones Insertar y Modificar Grupo	 			  */
	/* -------------------------------------------------------------- */
	/* MODULO GRUPOS												  */
	/* -------------------------------------------------------------- */
	
	function verificaUsuariosModulo(&$usuarios){}
	
	function obtieneRaizModulo($id,&$des_raiz,&$num_profundidad){}
	
	/* ------------------------------------------------------------- */
	/* Funcion para insertar un Grupo a la Base de Datos			 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	
	function insertaModuloDB($datos, $usuarios){}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Modificar un Grupo en la Base de Datos			 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	
	function modificaModuloDB($datos, $usuarios, $id){}

        /* ------------------------------------------------------------- */
        /* Funcion para Registrar las Acciones en los modulos            */
        /* ------------------------------------------------------------- */
        /* MODULO GRUPOS , CLASE MODULOS                                 */
        /* ------------------------------------------------------------- */
	/* USO:     this->logModulo('Prueba de Log'); 			 */
	/* ------------------------------------------------------------- */

	function logModulo ($des_log){}
}
?>