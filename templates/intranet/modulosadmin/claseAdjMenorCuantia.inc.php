<?php
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class AdjMenorCuantia extends Modulos{

    function AdjMenorCuantia($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
        $this->datosUsuarioMSSQL();
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_ACTIVIDAD => 'frmSearchActi',
							BUSCA_ACTIVIDAD => 'searchActi',
							FRM_AGREGA_ACTIVIDAD => 'frmAddActi',
							AGREGA_ACTIVIDAD => 'addActi',
							FRM_MODIFICA_ACTIVIDAD => 'frmModifyActi',
							MODIFICA_ACTIVIDAD => 'modifyActi',
							GENERA_PDF => 'pdf',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		///$this->datosUsuarioPortal();
		/*
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Adjudicaciones de Menor Cuant�a',false,array('amc'));
			$objIntranet->Body('amc_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		*/
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		/*
		if($this->userIntranet['COD_DEP']!=18){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase  
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Adjudicaciones de Menor Cuant�a',false,array('amc'));
			$objIntranet->Body('amc_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}*/
		
		// Items del Menu Principal
		/*if($_SESSION['mod_ind_leer'])*/ $this->menu_items[0] = array ( 'val' => 'frmSearchActi', label => 'BUSCAR' );
		/*if($_SESSION['mod_ind_insertar'])*/ $this->menu_items[1] = array ( 'val' => 'frmAddActi', label => 'AGREGAR' );
		/*if($_SESSION['mod_ind_modificar']) $this->menu_items[2] = array ( 'val' => 'frmModifyActi', label => 'MODIFICAR' );*/

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	/**/
	function ValidaFechaActividad($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('transparencia/amc/headerArm.tpl.php');
		$html->display('transparencia/amc/showStatTrans.inc.php');
		$html->display('transparencia/amc/footerArm.tpl.php');
	}
	
	function FormBuscaActividad($page=NULL,$desNombre=NULL,$desDescripcion=NULL,$tip_disp=NULL,$tip_mon=NULL,$fecIniDir=NULL,$fecFinDir=NULL,$search=false){
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir) ? $fecIniDir : "06/01/2000";
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir) ? $fecFinDir : date('m/d/Y');
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		$sql_st = "SELECT id,descripcion ".
				  				"FROM tipo_moneda ".
				 					"ORDER BY 2";
		$html->assign_by_ref('selTipMon',$this->ObjFrmSelect($sql_st, $tip_mon, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		//echo $sql_st;
		unset($sql_st);
		
		//$this->abreConnDB();
		//$this->connDebug=true;		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT id,descripcion ".
				  				"FROM tipo_dispositivo_legal ".
									"ORDER BY 2";
		$html->assign_by_ref('selTipDisp',$this->ObjFrmSelect($sql_st, $tip_disp, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		//echo $sql_st;
		unset($sql_st);		
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecIniDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecFinDir,6,4), true, array('val'=>'none','label'=>'--------')));		
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('transparencia/amc/headerArm.tpl.php');
		$html->display('transparencia/amc/search.tpl.php');
		if(!$search) $html->display('transparencia/amc/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaActividad();
	}

	function BuscaActividad($page,$desNombre,$desDescripcion,$tip_disp,$tip_mon,$fecIniDir,$fecFinDir){
	
		//echo $fecIniDir."xx".	$fecFinDir;
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		if($tip_disp!="none"){
		$condConsulta[] = "id_tipo_dispositivo_legal=$tip_disp";
		}
		if($tip_mon!="none"){
		$condConsulta[] = "id_tipo_moneda=$tip_mon";
		}		
		$condConsulta[] = "(Upper(nro_proceso) like Upper('%$desNombre%') or Upper(descripcion) like Upper('%$desNombre%'))";
  	$condConsulta[] = "convert(datetime,fecha_convocatoria,101)>convert(datetime,'$fecIniDir',101) and convert(datetime,fecha_convocatoria,101)<DATEADD(dd,1,convert(datetime,'$fecFinDir',101))";
	
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaActividad($page,$desNombre,$desDescripcion,$tip_disp,$tip_mon,$fecIniDir,$fecFinDir,true);
		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;		
		
		$sql_SP = sprintf("EXECUTE sp_busIDDispositivos %s,%s,%s,0,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDispositivos %s,%s,%s,%s,%s",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("SELECT mc.id, mc.nro_proceso, mc.descripcion, td.descripcion, tm.descripcion, mc.comentario, convert(varchar(10),convert(datetime,mc.fecha_convocatoria, 101),103), convert(varchar(10),convert(datetime,mc.fecha_presentacion, 101),103), convert(varchar(10),convert(datetime,mc.fecha_otorgamiento, 101),103), ".
									 "CASE WHEN mc.ind_activo = 1 THEN 'Activo' ELSE 'Inactivo' END ".
							 		 "FROM menor_cuantia mc, tipo_moneda tm, tipo_dispositivo_legal td ".
							 		 "WHERE mc.id_tipo_moneda=tm.id and mc.id_tipo_dispositivo_legal=td.id   ".
																		 
							  	 "and mc.id=%d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
					$html->append('arrActi', array('id' => $row[0],
												   'npro' => $row[1],
												   'desc' => $row[2],
												   'disp' => $row[3],
													 'tipo' => $row[4],
												   'valor' => $row[5],
												   'fconv' => $row[6],
												   'fpres' => $row[7],												   
												   'fotor' => $row[8],
													 'esta' => $row[9]));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		$html->assign('desNombre',$desNombre);
		$html->assign('tip_disp',$tip_disp);
		$html->assign('tip_mon',$tip_mon);
		$html->assign('fecIniDir',$fecIniDir);
			$html->assign('fecFinDir',$fecFinDir);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ACTIVIDAD], true));

		// Muestra el Resultado de la Busqueda
		$html->display('transparencia/amc/searchResult.tpl.php');
		$html->display('transparencia/amc/footerArm.tpl.php');
	}
	/**/
	function FormManipulaActividad($id=NULL,$desNombre=NULL,$desDescripcion=NULL,$tip_disp=NULL,$tip_mon=NULL,$desComentario=NULL,$fecConvocatoria=NULL,$fecPresentacion=NULL,$fecOtorgamiento=NULL,
 							     $indActivo=NULL,$reLoad=false,$errors){

		$this->abreConnDB();
		$this->connDebug=true;		

								/**/
		if($idAmc && !$reLoad){

			$sql_st = sprintf("SELECT nro_proceso,descripcion,comentario,convert(varchar(10),convert(datatime,fecha_convocatoria, 101),103), convert(varchar(10),convert(datatime,fecha_presentacion, 101),103), convert(varchar(10),convert(datatime,fecha_otorgamiento, 101),103),des_fuente 
			   						 ,CASE WHEN ind_activo = true THEN 'Y' ELSE 'N' END,CASE WHEN ind_imagen is true THEN 'Y' ELSE 'N' END 
									 
							   FROM menor_cuantia 
							  WHERE id=%d",$this->PrepareParamSQL($idActividad));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($desNombre,$desDescripcion,$tip_disp,$tip_mon,$desValor,$fecConvocatoria,$fecPresentacion,$fecOtorgamiento,$desFuente,$indActivo) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
		
		//Manipulacion de las Fechas
		
		//$fecConvocatoria = ($fecConvocatoria!='// :'&&$fecConvocatoria&&!strstr($fecConvocatoria,'none')) ? $fecConvocatoria : date("m/d/Y");		
		$fecPresentacion = ($fecPresentacion!='// :'&&$fecPresentacion&&!strstr($fecPresentacion,'none')) ? $fecPresentacion : date("m/d/Y");
		$fecOtorgamiento = ($fecOtorgamiento!='// :'&&$fecOtorgamiento&&!strstr($fecOtorgamiento,'none')) ? $fecOtorgamiento : date("m/d/Y");
		
		$fecConvocatoria = ($fecConvocatoria!='//'&&$fecConvocatoria) ? $fecConvocatoria : date('m/d/Y');
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddActi';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('desDescripcion',$desDescripcion);
		$html->assign_by_ref('tip_disp',$tip_disp);
		$html->assign_by_ref('tip_mon',$tip_mon);
		$html->assign_by_ref('desComentario',$desComentario);
		$html->assign_by_ref('fecConvocatoria',$fecConvocatoria);
		$html->assign_by_ref('fecPresentacion',$fecPresentacion);
		$html->assign_by_ref('fecOtorgamiento',$fecOtorgamiento);
		$html->assign_by_ref('indActivo',$indActivo);		
		//$html->assign_by_ref('desFuente',$desFuente);		
	
		// Fecha Actividad
		$html->assign_by_ref('selMesConv',$this->ObjFrmMes(1, 12, true, substr($fecConvocatoria,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaConv',$this->ObjFrmDia(1, 31, substr($fecConvocatoria,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoConv',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecConvocatoria,6,4), true, array('val'=>'none','label'=>'--------')));

		$html->assign_by_ref('selMesPres',$this->ObjFrmMes(1, 12, true, substr($fecPresentacion,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaPres',$this->ObjFrmDia(1, 31, substr($fecPresentacion,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoPres',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecPresentacion,6,4), true, array('val'=>'none','label'=>'--------')));

		$html->assign_by_ref('selMesOtor',$this->ObjFrmMes(1, 12, true, substr($fecOtorgamiento,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaOtor',$this->ObjFrmDia(1, 31, substr($fecOtorgamiento,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoOtor',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecOtorgamiento,6,4), true, array('val'=>'none','label'=>'--------')));

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);

		//$this->abreConnDB();
		//$this->connDebug=true;		
		// Contenido Select Norma Tipo
		$sql_st = "SELECT id,descripcion ".
				  				"FROM tipo_moneda ".
				  				"ORDER BY 1";
		$html->assign_by_ref('selTipMon',$this->ObjFrmSelect($sql_st, $tip_mon, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		//echo $sql_st;
		unset($sql_st);
		
		//$this->abreConnDB();
		//$this->connDebug=true;		
		// Contenido Select Tipo Dispositivo
		$sql_st = "SELECT id,descripcion ".
				 				  "FROM tipo_dispositivo_legal ".
				  				"ORDER BY 1";
		$html->assign_by_ref('selTipDisp',$this->ObjFrmSelect($sql_st, $tip_disp, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		//echo $sql_st;
		unset($sql_st);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('transparencia/amc/headerArm.tpl.php');
		$html->display('transparencia/amc/frmHandleActi.tpl.php');
		$html->display('transparencia/amc/footerArm.tpl.php');
	
	}
	
		function ReporteDiario($desNombre,$fecIniDir,$fecFinDir,$tip_disp,$tip_mon){
		$this->abreConnDB();
		//$this->conn->debug=true;
		// Genera HTML
		$html = new Smarty;
		
		//echo $tip_mon."dd";
	
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscar';
		//$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		
					// Obtiene Todos los Datos del documento
					$sql_SP = "SELECT mc.id, mc.nro_proceso, mc.descripcion, td.descripcion, tm.descripcion, mc.comentario,  convert(varchar(10),convert(datetime,mc.fecha_convocatoria, 101),103), convert(varchar(10),convert(datetime,mc.fecha_presentacion, 101),103), convert(varchar(10),convert(datetime,mc.fecha_otorgamiento, 101),103), ".
									 "CASE WHEN mc.ind_activo = 1 THEN 'Activo' ELSE 'Inactivo' END ".
							 		 "FROM menor_cuantia mc, tipo_moneda tm, tipo_dispositivo_legal td ".
							 		 "WHERE mc.id_tipo_moneda=tm.id and mc.id_tipo_dispositivo_legal=td.id and
									 (Upper(mc.nro_proceso) like Upper('%$desNombre%') or Upper(mc.descripcion) like Upper('%$desNombre%')) and
									 convert(datetime,fecha_convocatoria,101)>convert(datetime,'$fecIniDir',101) and convert(datetime,fecha_convocatoria,101)<convert(datetime,'$fecFinDir',101)
									  
									   ";
					if($tip_disp!='none'){
						$sql_SP.=" and mc.id_tipo_dispositivo_legal=$tip_disp ";
					} 
					if($tip_mon!='none'){
						$sql_SP.=" and mc.id_tipo_moneda=$tip_mon ";
					} 
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
					$html->append('arrActi', array('id' => $row[0],
												   'npro' => $row[1],
												   'desc' => $row[2],
												   'disp' => $row[3],
													 'tipo' => $row[4],
												   'valor' => $row[5],
												   'fconv' => $row[6],
												   'fpres' => $row[7],												   
												   'fotor' => $row[8],
													 'esta' => $row[9]));
						$rs->Close();
					}
					unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$html->assign('fecIniDir',$fecIniDir);
		$html->assign('fecFinDir',$fecFinDir);

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/dispositivos/';
		$filename = 'Norma'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('transparencia/amc/reportDetailNormas.tpl.php'),false,$logo);

		$destination = '/institucional/aplicativos/dispositivos/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;	
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}	
	
	function ManipulaActividad($id,$desNombre,$desDescripcion,$tip_disp,$tip_mon,$desComentario,$fecConvocatoria,$fecPresentacion,$fecOtorgamiento,
 							     $indActivo){
									 
				//		echo "matrix";			 
							
		//Manipulacion de las Fechas
		$fecConvocatoria = ($fecConvocatoria!='// :'&&$fecConvocatoria&&!strstr($fecConvocatoria,'none')) ? $fecConvocatoria : NULL;
		$fecPresentacion = ($fecPresentacion!='// :'&&$fecPresentacion&&!strstr($fecPresentacion,'none')) ? $fecPresentacion : NULL;
		$fecOtorgamiento = ($fecOtorgamiento!='// :'&&$fecOtorgamiento&&!strstr($fecOtorgamiento,'none')) ? $fecOtorgamiento : NULL;				
		

		// Comprueba Valores	
		if(!$desNombre){ $bPro = true; $this->errors .= 'El T�tulo debe ser especificado<br>'; }
		if(!$desDescripcion){ $bDes = true; $this->errors .= 'La Sumilla debe ser especificada<br>'; }
		if(!$desComentario){ $bCom = true; $this->errors .= 'El Comentario debe ser especificado<br>'; }
		if($fecConvocatoria){ $bFConv = ($this->ValidaFechaActividad($fecConvocatoria,'Convocatoria')); }
		//if($fecPresentacion){ $bFPres = ($this->ValidaFechaActividad($fecPresentacion,'Presentaci�n')); }	
		//if($fecOtorgamiento){ $bFOtor = ($this->ValidaFechaActividad($fecOtorgamiento,'Otorgamiento')); }		

		if($bPro||$bDes||$bCom||$bFConv){
			$objIntranet = new Intranet();
			$objIntranet->Header('Adjudicaciones de Memor Cuant�a',false,array('amc'));
			$objIntranet->Body('dispositivos_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormManipulaActividad($id,$desNombre,$desDescripcion,$tip_disp,$tip_mon,$desComentario,$fecConvocatoria,$fecPresentacion,$fecOtorgamiento,
 							     $indActivo,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			//$this->conn->debug = true;
			// Inicia la Transaccion
			//$this->conn->BeginTrans(); 
			
			//echo "mmm";
			
			$sql_FT = sprintf("EXEC %s '%s','%s','%s',%d,%d,%s,'%s',%d,%d ",
							  $id ? 'modAjdMenorCuantia' : 'addDispositivosOGAJ',
							  //$id ? $this->PrepareParamSQL($id).',' : '',
							  $this->PrepareParamSQL($desNombre),
							  $this->PrepareParamSQL($desDescripcion),
							  $this->PrepareParamSQL($desComentario),
								"0",								
								$tip_mon,
							  ($fecConvocatoria) ? "'".$this->PrepareParamSQL($fecConvocatoria)."'" : 'NULL',
							  //($fecPresentacion) ? "'".$this->PrepareParamSQL($fecPresentacion)."'" : 'NULL',
							  //($fecOtorgamiento) ? "'".$this->PrepareParamSQL($fecOtorgamiento)."'" : 'NULL',
								$_SESSION['cod_usuario'],
							  ($indActivo=='Y') ? 1 : 0,
								$tip_disp);
						// echo $sql_FT;
								$rs = & $this->conn->Execute($sql_FT);
								unset($sql_FT);
								if (!$rs)
									$RETVAL=0;
								else{
									$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 0;
									$rs->Close();
								}
								unset($rs);		
			//exit;
			/*if(!$RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();/**/
		
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= $id ? "&menu={$this->menu_items[2]['val']}": "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;			
		}
	}/**/	
}
?>