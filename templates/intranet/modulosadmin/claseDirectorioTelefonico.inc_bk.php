<?php

include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class DirectorioTelefonico extends Modulos{

	var $DB;
	var $emailDomain = 'CONVENIO_SITRADOC.gob.pe';
	var $imgTitulo = 'direc.CONVENIO_SITRADOC_tit.gif';
	
    function DirectorioTelefonico($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Setea Parametros para la Conexion a la Base de Datos
		
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
        //$this->datosUsuarioMSSQL();
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_ACTIVIDAD => 'frmSearchActi',
							BUSCA_ACTIVIDAD => 'searchActi',
							//FRM_AGREGA_ACTIVIDAD => 'frmAddActi',
							//AGREGA_ACTIVIDAD => 'addActi',
							BUSCA_PERSONA_EXTENDIDO => 'frmSearchExtended',
							FRM_MODIFICA_ACTIVIDAD => 'frmModifyActi',
							MODIFICA_ACTIVIDAD => 'modifyActi',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Items del Menu Principal
		/*if($_SESSION['mod_ind_leer'])*/ $this->menu_items[0] = array ( 'val' => 'frmSearchActi', label => 'BUSCAR' );
		/*if($_SESSION['mod_ind_insertar']) $this->menu_items[1] = array ( 'val' => 'frmAddActi', label => 'AGREGAR' );*/
		if($_SESSION['mod_ind_modificar']) $this->menu_items[1] = array ( 'val' => 'frmModifyActi', label => 'MODIFICAR' );/**/

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	/**/
	function ValidaFechaActividad($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('telefonos/headerArm.tpl.php');
		$html->display('telefonos/showStatTrans.inc.php');
		$html->display('telefonos/footerArm.tpl.php');
	}
	
	function FormBuscaActividad($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('telefonos/headerArm.tpl.php');
		$html->display('telefonos/search.tpl.php');
		if(!$search) $html->display('telefonos/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaActividad();
	}
/**/
	function BuscaActividad($page,$tipBusqueda,$desNombre){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscar';
		//$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		$html->assign('frmName','frmSearchExtended');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array(
									
									'tipBusqueda'=>$tipBusqueda,
									'desNombre'=>$desNombre
									));
		
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;

	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaActividad($page,$tipBusqueda,$desNombre,true);

				// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$condConsulta = array();
		$condConsulta[] = "t.coddep=d.codigo_dependencia and t.publicado_intranet='1' and t.estado='ACTIVO'";
		if($tipBusqueda==1){
			$condConsulta[] = "d.dependencia like '%$desNombre%'";
		}elseif($tipBusqueda==2){
			$condConsulta[] = "(t.apellidos_trabajador like '%$desNombre%' or t.nombres_trabajador like '%$desNombre%')";
			}elseif($tipBusqueda==3){
			$condConsulta[] = "t.anexo like '%$desNombre%'";
			}elseif($tipBusqueda==4){
			$condConsulta[] = "d.siglas like '$desNombre'";
			}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		$sql_SP = sprintf("EXECUTE sp_busIDDirectorioTel %d,%s,%s,0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		//echo $numRegs."hola";
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			//echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			$sql_SP = sprintf("EXECUTE sp_busIDDirectorioTel %d,%s,%s,%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					
					
					$sql_SP = sprintf("
											
											SELECT 
											t.coddep,
											d.dependencia,
											sd.descripcion,d.piso,t.apellidos_trabajador,t.nombres_trabajador,t.email,t.anexo,t.codigo_trabajador, AT.*, t.dni 
											FROM dbo.h_trabajador      t 
											left join      h_subdependencia sd on t.coddep=sd.ID_DEPENDENCIA and t.idsubdependencia=sd.id_subdependencia  and t.ESTADO='ACTIVO' AND PUBLICADO_INTRANET=1 
											left join dbo.h_dependencia d on t.CODDEP=d.CODIGO_DEPENDENCIA 
											left join dbo.h_Trabajador_asignacion_telefonica at  on at.codigo_trabajador=t.codigo_trabajador  
											WHERE t.codigo_trabajador=%d 
											ORDER BY  
											t.apellidos_trabajador",$id[0]);

											
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
						{
						
						$archivoexiste=0;
						
						$fileUrl = "/var/www/intranet/institucional/misce/directorio/fotosTrab/".$row[22].".jpg";			
						                                                                                                                                             if (@fclose(@fopen( $fileUrl, "r"))) 
                                                                                                                                             {
                                                                                                                                             $archivoexis=1;
                                                                                                                                             } else {
                                                                                                                                             $archivoexis=0;
                                                                                                                                             }
																							
						
								$html->append('arrActi', array('id' => $row[0],
														  								'NDep' => ucwords($row[1]),
																							'NSDep' =>$row[2],
																							'Piso' =>$row[3],
														  								'ApellTrab' => ucwords($row[4]),
																							'NomTrab' => ucwords($row[5]),
																							'Email' => $row[6],
																							'anexo' => $row[7],
																							'codigoTrab' => $row[8],
																							'NumeroCel' => $row[11],
																							'NumeroRPM' => $row[12],
																							'CantidadCel' => $row[13],
																							'AsignacionLocal' => $row[14],
																							'AsignacionCel' => $row[15],
																							'AsignacionNacional' => $row[16],
																							'AsignacionInternacional' => $row[17],
																							'NroDNI' => $row[22],
																							'NroDNIexist' =>  $archivoexis
																																											
																));
								}
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
			
		}else
			$start = 0;
			
		if(($tipBusqueda==1)&& $desNombre!=""){
			$sql="select dependencia from db_general.dbo.h_dependencia where dependencia like '%{$desNombre}'";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$depe=$rs->fields[0];
			$html->assign_by_ref('depe',$depe);
		}
		}
		
		if(($tipBusqueda==4)&& $desNombre!=""){
			$sql="select dependencia from db_general.dbo.h_dependencia where SIGLAS like '{$desNombre}'";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$depe=$rs->fields[0];
			$html->assign_by_ref('depe',$depe);
		}
		}		
			
		$html->assign('tipBusqueda',$tipBusqueda);	
					
		// Setea indicadores de derecho
		
		
		$html->assign('indMod',$_SESSION['mod_ind_modificar']);

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscar', $this->arr_accion[BUSCA_ACTIVIDAD], true));

		// Muestra el Resultado de la Busqueda
		
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('emailDomain',$this->emailDomain);
		$html->display('telefonos/searchResult.tpl.php');
		$html->display('telefonos/footerArm.tpl.php');
		
	}
	
/**/
	function FormModificaActividad($id,$codDep=NULL,$trabajador=NULL,$indActivo=NULL,$reLoad=false,$desTelefono=NULL,$errors=false){
	//global $desTelefono;
	
	if(!$_SESSION['mod_ind_modificar']){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
	
	}
	
/**/
		if($id && !$reLoad ){
		$this->abreConnDB();
		//$this->conn->debug = true;
			
			//OJO CONSULTA - CAMBIARLA
			$sql_st = sprintf("SELECT t.coddep,d.dependencia,d.piso,t.apellidos_trabajador,t.nombres_trabajador,t.email,t.anexo
													 FROM dbo.h_trabajador t, dbo.h_dependencia d
													WHERE t.coddep=d.codigo_dependencia and estado='activo' 
													and t.codigo_trabajador=%d 
													and publicado_intranet='1'",$this->PrepareParamSQL($id));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
				if (!$rs)
					$RETVAL=0;
				else{
					$codDep= $rs->fields[0];
					$desDependencia = $rs->fields[0];
					$desTrabajador = $rs->fields[1];				
					$desTelefono = $rs->fields[6];
					$trabajador = $id;
					
					}				
			$rs->Close();
			
			}
		/**/
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyActi';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desTelefono',$desTelefono);
		$html->assign_by_ref('codDep',$codDep);
		$html->assign_by_ref('trabajador',$trabajador);

		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDep, true, true, array('val'=>'none','label'=>'No especificada')));
		unset($sql_st);
		
		// Contenido Select Usuario
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' /*and publicado_intranet='1'*/ ".
						  "ORDER BY 2",($codDep) ? $codDep : 0);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);	
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);

		//$this->abreConnDB();
		//$this->connDebug=true;		
		//echo $sql_st;
		unset($sql_st);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('telefonos/headerArm.tpl.php');
		$html->display('telefonos/frmModifica.tpl.php');
		$html->display('telefonos/footerArm.tpl.php');
	
	}
	
	/**/
	function VistaExtendidaDirectorio($tipBusqueda,$desNombre){
		
		/*$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		$this->FormBuscaActividad($page,$tipBusqueda,$desNombre,true);*/
		
		$html = new Smarty;
	
			$condConsulta = array();
			$condConsulta[] = "t.coddep=d.codigo_dependencia and t.publicado_intranet='1'";
		if($tipBusqueda==1){
			$condConsulta[] = "d.dependencia like '%$desNombre%'";
		}elseif($tipBusqueda==2){
			$condConsulta[] = "(t.apellidos_trabajador like '%$desNombre%' or t.nombres_trabajador like '%$desNombre%')";
			}elseif($tipBusqueda==3){
			$condConsulta[] = "t.anexo like '%$desNombre%'";
			}elseif($tipBusqueda==4){
			$condConsulta[] = "d.siglas like '$desNombre'";
			}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busIDDirectorioTel %s,%s,%s,1,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		/**/
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			while($id = $rsId->FetchRow()){
			/**/
				// Obtiene Todos los Datos de la Parte
				$sql_SP = sprintf("SELECT t.coddep,d.dependencia,sd.descripcion,d.piso,t.apellidos_trabajador,t.nombres_trabajador,t.email,t.anexo,t.codigo_trabajador, 
				convert(varchar(20),t.dni )
													FROM dbo.h_trabajador t, dbo.h_dependencia d, h_subdependencia sd
													WHERE t.codigo_trabajador=%d and t.coddep_pub=d.codigo_dependencia and d.codigo_dependencia=sd.id_dependencia and t.idsubdependencia=sd.id_subdependencia and t.estado='activo' and t.publicado_intranet='1' 
													ORDER BY t.apellidos_trabajador",$id[0]);
					//echo $sql_SP;
				$rsData = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if(!$rsData)
					print $this->conn->ErrorMsg();
				else{
					if($row = $rsData->FetchRow())	
								$html->append('arrActi', array('id' => $row[0],
														 	 								'NDep' => ucwords($row[1]),
																							'NSDep' =>$row[2],
																							'Piso' =>$row[3],
															  							'ApellTrab' => ucwords($row[4]),
																							'NomTrab' => ucwords($row[5]),
																							'Email' => $row[6],
																							'anexo' => $row[7],
																							'codigoTrab' => $row[8],
																							'Dni' => "'".$row[9].' '
																																											
																));
					$rsData->Close();
				}//else
				unset($rsData);
				/**/
			}//while
			$rsId->Close();
		}//else
		unset($rsId);

		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('telefonos/viewExtended.tpl.php');
		exit;
		/**/
	}
	
	
	//DESDE AQUI NO TOCAR
	function ModificaActividad($id,$codDep,$trabajador,$indActivo,$desTelefono){
		// Comprueba Valores	
		if($codDep<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($trabajador<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }

		if($bC1||$bC2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Directorio Telef�nico',false,array('suspEmb'));
			$objIntranet->Body('direc.CONVENIO_SITRADOC_tit.gif.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaActividad($id,$codDep,$trabajador,$indActivo,$reLoad,$desTelefono,$errors);
			
			$objIntranet->Footer();
		}else{
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE modDirTelefonos %d,%d,%d,%d,'%s'",
								$id,
							  ($codDep) ? $this->PrepareParamSQL($codDep) : "NULL",
							  ($trabajador) ? $this->PrepareParamSQL($trabajador) : "NULL",
							  ($desTelefono) ? $this->PrepareParamSQL($desTelefono) : "NULL",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
}

?>