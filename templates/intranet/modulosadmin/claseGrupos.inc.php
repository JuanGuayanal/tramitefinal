<?
require_once('claseModulos.inc.php');

class Grupos extends Modulos{
	
	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	function Grupos(){
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Listar todos los Grupos Existentes (Index)		 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	
	function muestraGrupoIndexHTML($cod_grupo=NULL){
		$frmName = "frmBusca";
?>
	<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
	<table width="540" border="0" cellspacing="0" cellpadding="2">
	  <tr> 
		<td> 
		  <hr width="100%" size="1">
		</td>
	  </tr>
	  <tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				
            <td class="td-lb-login" width="360"><b><img src="/img/800x600/ico-mas.gif" width="16" height="22" align="absmiddle">
			<a href="<? echo "{$_SERVER['PHP_SELF']}?accion=".ACCION_INSERTAR_FALSE; ?>">AGREGAR GRUPO</a>
              <input type="hidden" name="accion" value="<?echo ACCION_BUSCAR_TRUE;?>">
              </b></td>
				<td align="right" width="100"> 
				  <input type="text" name="cod_grupo" class="ip-login contenido">
				</td>
				<td align="center" width="80"> 
				  <input type="submit" value="Buscar" class="but-login">
				</td>
			  <td>&nbsp;</td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td> 
		  <hr width="100%" size="1">
		</td>
	  </tr>
	  <tr> 
		<td>&nbsp;</td>
	  </tr>
<?
		$this->abreConnDB();
		
		$sql_st =  "SELECT id_grupo, UPPER(cod_grupo), ind_activo ";
		$sql_st .= "FROM grupo";
		$sql_st .= (!empty($cod_grupo)) ? " WHERE cod_grupo like '%${cod_grupo}%'" : " ";
		$sql_st .= "ORDER BY 2";
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()){
?>
	  <tr> 
		<td>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
              <td rowspan="3" valign="top"><img src="/img/800x600/ico-grupo.gif" width="17" height="16" hspace="3"></td>
			  <td colspan="2" width="523" class="item" height="16"> 
                <b><a href="<? echo "{$_SERVER['PHP_SELF']}?id={$row[0]}&accion=".ACCION_MODIFICAR_FALSE; ?>"><?=$row[1];?></a></b>
              </td>
			</tr>
			<tr> 
			  <td class="sub-item" width="60" valign="top">Derechos:</td>
			  <td width="463" class="texto"> 
<?
				$sql_mods_st = "SELECT m.id_modulopadre, m.id_modulotipo, m.des_titulocorto ".
							   "FROM modulo m, modulo_derecho md ".
							   "WHERE md.id_grupo={$row[0]} and m.id_modulo=md.id_modulo ".
							   "ORDER BY des_raiz";

				$rs_mods = & $this->conn->Execute($sql_mods_st);
				unset($sql_mods_st);
				if (!$rs_mods){
					print $this->conn->ErrorMsg();
				}else{
					$numrows = $rs_mods->RecordCount();
					if($numrows<=0)
						echo "Ninguno";
					$cont=0;
					while($row_mods = $rs_mods->FetchRow()){
						echo "<a href=\"../grupoderechos/index.php?idgrp={$row[0]}";
						echo (!is_null($row_mods[0])) ? "&idmodpad={$row_mods[0]}" : "";
						echo "&tip_modulo={$row_mods[1]}&accion=".ACCION_INSERTAR_FALSE."\">{$row_mods[2]}</a>";
						if($cont<$numrows-1)
							echo " - ";
						$cont++;
					}
					$rs_mods->Close();
					unset($rs_mods);
				}
?>
              </td>
            </tr>
			<tr> 
			  <td class="sub-item">Estado:</td>
			  <td class="textored"> 
                <?echo ($row[2]=='t') ? 'Activo' : 'Inactivo';?>
              </td>
            </tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td>
        <hr width="100%" size="1">
      </td>
	  </tr>
<?
				
			}
			$rs->Close();
			unset($rs);
		}		
?>
	  <tr> 
		<td>&nbsp;</td>
	  </tr>
	</table>
	</form>
<?
	}
	
	
	/* --------------------------------------------------------------- */
	/* Funcion para mostrar el Formulario HTML para Ins o Mod un Grupo */
	/* --------------------------------------------------------------- */
	/* MODULO GRUPOS												   */
	/* --------------------------------------------------------------- */
		
	function insertaGrupoHTML($datos=NULL,$id_dependencia=NULL,$id_subdependencia=NULL,$cod_usuario=NULL,$accion=ACCION_INSERTAR_FALSE,$id=NULL){
		//Nombre del Formulario Principal
		$frmName = "frmGrupo";
		$jscript_st = "seleccionaItemSelectForm(2,document.forms['${frmName}'].elements['cod_usuarios[]'])\r\n";
		$jscript_st .= "document.${frmName}.accion.value=${accion}\r\n";
		$this->insertaScriptSubmitForm($frmName,$jscript_st);
	
		// Seleccion de Datos si la Accion es MODIFICAR
		if(!is_null($id)){
			$this->abreConnDB();
 			$sql_st="SELECT cod_grupo, des_grupo, ind_activo, to_char(fec_creacion,'DD/MM/YYYY HH24:MI:SS'), ".
					"to_char(fec_modificacion,'DD/MM/YYYY HH24:MI:SS'), usr_audit ".
					"FROM grupo ".
					"WHERE id_grupo=$id";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$row = $rs->FetchRow();
				$rs->Close();
			}
		}
?>
	<form name="<?=$frmName;?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
	  <table width="540" border="0" cellspacing="0" cellpadding="2">
		<tr> 
		  <td colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td colspan="2" class="td-lb-login" width="540"><b>DEL GRUPO</b></td>
		</tr>
		<tr> 
		  <td colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="430">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto"><b>Identificador</b></td>
		  <td> 
			<input type="text" name="cod_grupo" class="ip-login contenido" size="42" value="<?echo (empty($datos[0]))? $row[0] : $datos[0];?>" maxlength="25">
		  </td>
		</tr>
		<tr> 
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="110">Descripci&oacute;n</td>
		  <td width="430"> 
			<textarea name="des_grupo" class="ip-login contenido" cols="40" rows="4"><?echo (empty($datos[1]))? $row[1] : $datos[1];?></textarea>
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="110"><b>Activo</b></td>
		  <td width="390"> 
<?
		$this->insertaIndActivoFrm((empty($datos[2]))? $row[2] : $datos[2]);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
		<tr> 
		  <td colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td colspan="2" class="td-lb-login" width="540"><b>DE LOS INTEGRANTES</b></td>
		</tr>
		<tr> 
		  <td colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="110"><b>Dependencia</b></td>
		  <td width="390"> 
<?
		$this->insertaDependenciaFrm($id_dependencia,"id_dependencia",true,true);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="110"><b>Sub Dependencia</b></td>
		  <td width="390"> 
<?
		$this->insertaSubdependenciaFrm("id_subdependencia",true);
		$this->insertaSubdependenciaScript("document.${frmName}.id_subdependencia","document.${frmName}.id_dependencia",$id_subdependencia);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="110">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
		<tr> 
		  <td colspan="2" width="540">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			  <tr> 
<?
		$usuarios = $this->insertaUsuariosFrm($id_subdependencia);
?>
				<td width="250" align="center" class="texto">Usuarios ( <? echo $usuarios[0]; ?> )</td>
				<td width="40" align="center">&nbsp;</td>
				<td width="250" align="center" class="texto"><b>Integrantes del Grupo</b></td>
			  </tr>
			  <tr> 
				<td width="250" align="center"> 
				  <select id="cod_usuario_muestra" name="cod_usuario_muestra[]" size="10" multiple class="ip-login contenido">
					<option>-------------------------------------</option>
					  <? echo $usuarios[1]; ?>
				  </select>
				</td>
				<td width="40" align="center"> 
<?
		$this->insertaScriptItemsSelectForm("document.forms['${frmName}'].elements['cod_usuario_muestra[]']", "document.forms['${frmName}'].elements['cod_usuarios[]']");
?>
				</td>
				<td width="250" align="center"> 
				  <select name="cod_usuarios[]" size="10" multiple class="ip-login contenido">
					<option>-------------------------------------</option>
<?
		if(count($cod_usuario)>0){
			$this->consultaUsuariosGrupoFrm($cod_usuario);
		}else{
			if(!is_null($id))
				$this->consultaUsuariosGrupoFrm(NULL,$id);
		}
?>
				  </select>
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
		<tr> 
		  <td width="150">
			<input type="hidden" name="accion" value="<?=$accion?>">
		  </td>
		  <td width="390">&nbsp;</td>
		</tr>
<?
		if(!is_null($id)){
?>
		<tr> 
		  <td class="texto" colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td colspan="2" width="540" class="td-lb-login"><b>DE LA MODIFICACI&Oacute;N</b></td>
		</tr>
		<tr> 
		  <td class="texto" colspan="2" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td class="texto" colspan="2" width="540"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
			  <tr> 
				<td class="texto" width="110">&nbsp;</td>
				<td width="130">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="125">&nbsp;</td>
				<td width="150">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="texto" width="110"><b>Usuario</b></td>
				<td width="130" class="texto"> 
				  <?=$row[5];?>
				</td>
				<td width="20">&nbsp;</td>
				<td width="125">&nbsp;</td>
				<td width="150">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="texto" width="110">&nbsp;</td>
				<td width="130">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="125">&nbsp;</td>
				<td width="150">&nbsp;</td>
			  </tr>
			  <tr> 
				<td class="texto" width="110"><b>Fecha Creaci&oacute;n</b></td>
				<td width="130" class="texto"> 
				  <?=$row[3];?>
				</td>
				<td width="20">&nbsp;</td>
				<td width="125" class="texto"><b>Fecha Modificaci&oacute;n</b></td>
				<td width="150" class="texto"> 
				  <?=$row[4];?>
				</td>
			  </tr>
			  <tr> 
				<td class="texto" width="110"> 
				  <input type="hidden" name="id" value="<?=$id;?>">
				</td>
				<td width="130">&nbsp;</td>
				<td width="20">&nbsp;</td>
				<td width="125">&nbsp;</td>
				<td width="150">&nbsp;</td>
			  </tr>
			</table>
		  </td>
		</tr>
<?
		}
?>
		<tr> 
		  <td class="texto" colspan="2" align="center"> 
			<input type="submit" value="<? echo (empty($id)) ? "INGRESAR DATOS" : "MODIFICAR DATOS"; ?>" class="but-login" 
			onClick="document.<?=$frmName;?>.accion.value=<? echo ($accion == ACCION_INSERTAR_FALSE) ? ACCION_INSERTAR_TRUE : ACCION_MODIFICAR_TRUE;?>;seleccionaItemSelectForm(2,document.forms['<?=$frmName;?>'].elements['cod_usuarios[]']);MM_validateForm('cod_grupo','Identificador','R');return document.MM_returnValue">
			&nbsp;&nbsp;&nbsp; 
			<input type="reset" value="BORRAR DATOS" class="but-login">
		  </td>
		</tr>
		<tr> 
		  <td class="texto" width="150">&nbsp;</td>
		  <td width="390">&nbsp;</td>
		</tr>
	  </table>
	</form>
<?
	}
	
	/* -------------------------------------------------------------- */
	/* Funcion para Limpiar Datos que van desde el Formulario HTML	  */
	/* hacia las Funciones Insertar y Modificar Grupo	 			  */
	/* -------------------------------------------------------------- */
	/* MODULO GRUPOS												  */
	/* -------------------------------------------------------------- */
	
	function enviaDatosGrupo($cod_grupo, $des_grupo, $ind_activo, $usuarios, $id=NULL){
		$des_grupo = (empty($_POST['des_grupo'])) ? NULL : trim($_POST['des_grupo']);
		if(count($usuarios)>0){
			$usuarios = array_unique($usuarios);
			$str_usuarios = implode(',',$usuarios);
			$usuarios = explode(',',$str_usuarios);
			unset($str_usuarios);
		}
	
		if(is_null($id)){
			$this->insertaGrupoDB($cod_grupo, $des_grupo, $ind_activo, $usuarios);
		}else{
			$this->modificaGrupoDB($id, $cod_grupo, $des_grupo, $ind_activo, $usuarios);
		}
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para insertar un Grupo a la Base de Datos			 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	
	function insertaGrupoDB($cod_grupo, $des_grupo, $ind_activo, $usuarios){
	
		$ins_st = "INSERT INTO grupo(cod_grupo, ";
		$ins_st .= (!is_null($des_grupo)) ? "des_grupo, " : "";
		$ins_st .= "ind_activo, usr_audit) ";
						
		$ins_st .= "VALUES('${cod_grupo}', ";
		$ins_st .= (!is_null($des_grupo)) ? "'${des_grupo}', " : "";
		$ins_st .= "'${ind_activo}', '{$_SESSION['cod_usuario']}')";
		
		$this->abreConnDB();

		if ($this->conn->Execute($ins_st) === false) {
			print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
		}else{
			if(count($usuarios)>0){
				// Selecciona el ID del Grupo Insertado
				$sql_st = "SELECT id_grupo FROM grupo WHERE cod_grupo='${cod_grupo}'"; 
				$rs = & $this->conn->Execute($sql_st);
				unset($sql_st);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					if($row = $rs->FetchRow())
						$id_grupo=$row[0];
					$rs->Close();
				}
				// Agrega los usuarios del Grupo

				for($i=0; $i<count($usuarios); $i++){
					$ins_usr_st = "INSERT INTO grupo_usuario(cod_usuario, id_grupo) values('{$usuarios[$i]}',${id_grupo})";
					if ($this->conn->Execute($ins_usr_st) === false) {
						print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
					}
					unset($ins_usr_st);
				}
			}
?>
<table width="531" border="0" cellspacing="0" cellpadding="3">
  <tr> 
	<td>&nbsp;</td>
  </tr>
  <tr> 
	<td class="aviso" align="center"><b>El Grupo se ha insertado correctamente</b></td>
  </tr>
  <tr>
	<td align="center"> 
	  <input type="button" value="Ingresar otro Grupo" class="but-login" onClick="MM_goToURL('parent','<?=$_SERVER['PHP_SELF'];?>');return document.MM_returnValue">
	  &nbsp;&nbsp;&nbsp;
	  <input type="button" value="Asignar derechos al Grupo" class="but-login" onClick="MM_goToURL('parent','../grupoderechos/index.php?idgrp=<?=$id_grupo?>&accion=<?=ACCION_INSERTAR_FALSE?>');return document.MM_returnValue">
	</td>
  </tr>
  <tr> 
	<td>&nbsp;</td>
  </tr>
</table>
<?
		}
		unset($ins_st); 
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Modificar un Grupo en la Base de Datos			 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPOS												 */
	/* ------------------------------------------------------------- */
	
	function modificaGrupoDB($id_grupo, $cod_grupo, $des_grupo, $ind_activo, $usuarios){
	
		// Arma la sentencia UPDATE
		$upd_st = "UPDATE grupo set cod_grupo='${cod_grupo}', ".
		$upd_st .= (!is_null($des_grupo)) ? "des_grupo='${des_grupo}', " : "des_grupo=NULL, ";
		$upd_st .= "ind_activo='$ind_activo', fec_modificacion=now(), usr_audit='{$_SESSION['cod_usuario']}' ";
		$upd_st .= "WHERE id_grupo=${id_grupo}";
		
		$this->abreConnDB();
	
		if ($this->conn->Execute($upd_st) === false) {
			print 'error modificando: '.$this->conn->ErrorMsg().'<BR>';
		}else{
			// Borra la estructura de usuarios del Grupo
			$del_st = "DELETE FROM grupo_usuario WHERE id_grupo='${id_grupo}'"; 
			if ($this->conn->Execute($del_st) === false) {
				print 'error borrando: '.$this->conn->ErrorMsg().'<BR>';
			}
			unset($del_st);

			// Agrega los usuarios del Grupo
			for($i=0; $i<count($usuarios); $i++){
				$ins_usr_st = "INSERT INTO grupo_usuario(cod_usuario, id_grupo) values('{$usuarios[$i]}',${id_grupo})";
				if ($this->conn->Execute($ins_usr_st) === false) {
					print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
				}
				unset($ins_usr_st);
			}
?>
<table width="531" border="0" cellspacing="0" cellpadding="3">
  <tr> 
	<td>&nbsp;</td>
  </tr>
  <tr> 
	<td class="aviso" align="center"><b>El Grupo se ha modificado 
	  correctamente</b></td>
  </tr>
  <tr>
	<td align="center"> 
	  <input type="button" value="Asignar derechos al Grupo" class="but-login" onClick="MM_goToURL('parent','../grupoderechos/index.php?idgrp=<?=$id_grupo?>&accion=<?=ACCION_INSERTAR_FALSE?>');return document.MM_returnValue">
	  &nbsp;&nbsp;&nbsp;
	  <input type="button" value="Modulo Grupos" class="but-login" onClick="MM_goToURL('parent','<?=$_SERVER['PHP_SELF'];?>');return document.MM_returnValue">
	</td>
  </tr>
  <tr> 
	<td>&nbsp;</td>
  </tr>
</table>
<?
		}
		unset($upd_st); 
	}
	
}
?>