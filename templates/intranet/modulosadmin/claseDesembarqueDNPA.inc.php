<?php
// error_reporting(E_ALL ^ E_NOTICE);
putenv("TDSDUMP=/tmp/dnpa.log");
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class Desembarque extends Modulos {

	function Desembarque($menu){
		/*
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
		*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][15];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'MUESTRA_DATOS_USUARIO' => 'frmShowWel',
							'FRM_BUSCA_DESEMBARQUE' => 'frmSearchDes',
							'BUSCA_DESEMBARQUE' => 'searchDes',
							'DOWN_BUS_DES' => 'dwnSearchDes',
							'FRM_INGRESA_DESEMBARQUE' => 'frmAddDes',
							'FRM_INFO_ADICIONAL' => 'frmInfAdd',
							'INGRESA_DESEMBARQUE' => 'addDes',
							'DOWN_ING_DES' => 'dwnAddDes',
							'BUEN_ING_DES' => 'goodAddDes',
							'MAL_ING_DES' => 'badAddDes',
							'XML_EMBARCACION' => 'xmlBoat',
							'XML_ESPECIE' => 'xmlSpecies',
							'XML_DESTINO' => 'xmlDestiny'
							);

		// $this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmShowWel', 'label' => 'tus datos' ),
							1 => array ( 'val' => 'frmSearchDes', 'label' => 'buscar' ),
							2 => array ( 'val' => 'frmAddDes', 'label' => 'ingresar' ),
							3 => array ( 'val' => 'frmInfAdd', 'label' => 'de inter�s' )
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
	}
	
	function GetAccion($accion){
		return $this->arr_accion[$accion];
	}
	
	function ValidaFecha($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,3,2)),sprintf('%d',substr($fecha,0,2)),substr($fecha,6,4)) )
			$errors .= '- La Fecha ' . $error . ' no es Valida.<br />';
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function XMLEmbarcacion(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		//$sql_SP = "EXECUTE sp_listEmbarcaciones";
		$sql_SP = "SELECT id_emb, nombre_emb, matricula_emb
  FROM DB_DNEPP.USER_DNEPP.EMBARCACIONNAC
  
  ORDER BY 2";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('emb',array('id'=>$row[0],
										  'nomb'=>$row[1],
										  'matr'=>$row[2]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEmbarcaciones.tpl.php');
	}
	
	function XMLEspecie(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$sql_SP = "EXECUTE sp_listEspecies";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('esp',array('id'=>$row[0],
										  'nomb'=>$row[1]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEspecies.tpl.php');
	}
	
	function XMLDestino(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$sql_SP = "EXECUTE sp_listDestinos";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('des',array('id'=>$row[0],
										  'nomb'=>$row[1]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLDestinos.tpl.php');
	}
	
	function MuestraIndex(){
		$this->MuestraDatosUsuario();
		//$this->XMLEmbarcacion();
	}
	
	function MuestraMenu(){
		$this->GeneraMenuPager(5,false);
	}
	
	function MuestraDatosUsuario(){
		$html = & new Smarty;
		
		$html->assign('frmName','frmBuscar');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/muestraDatosUsuario.tpl.php');
	}
	
	function FormBuscaDesembarque($page=null,$idEmb=null,$desEmb=null,$idEsp=null,$desEsp=null,$idDes=null,$desDes=null,$fecDesIni=null,$fecDesFin=null){
		$html = & new Smarty;
		
		$frmName = 'frmBuscar';
		$html->assign('frmName',$frmName);
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);

		$html->assign('datos',compact('idEmb',
									  'desEmb',
									  'idEsp',
									  'desEsp',
									  'idDes',
									  'desDes',
									  'fecDesIni',
									  'fecDesFin'));
		
		$html->display('dnpa/desembarque/formBuscaDesembarque.tpl.php');
	}
	
	function BuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin){

                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desEmb);

		/*
		// Genera Objeto HTML
		$html = new Smarty;

		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin);
	
		// Ejecuta el conteo General
		$this->abreConnDB();
		$this->conn->debug = true;
		*/
		/*
		$sql_SP = sprintf("EXECUTE sp_busIDDesArtesanal %d,%s,%s,%s,%s,%s,%f,%f,0,0",
							($idEmb) ? "'".$this->PrepareParamSQL($idEmb)."'" : 'NULL',
							($desEmb) ? "'".$this->PrepareParamSQL($desEmb)."'" : 'NULL',
							($idEsp) ? "'".$this->PrepareParamSQL($idEsp)."'" : 'NULL',
							($desEsp) ? "'".$this->PrepareParamSQL($desEsp)."'" : 'NULL',
							($idDes) ? "'".$this->PrepareParamSQL($idDes)."'" : 'NULL',
							($desDes) ? "'".$this->PrepareParamSQL($desDes)."'" : 'NULL',
							($fecDesIni) ? "'".$this->PrepareParamSQL($fecDesIni)."'" : 'NULL',
							($fecDesFin) ? "'".$this->PrepareParamSQL($fecDesFin)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// echo $sql_SP;		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}*/
		/*
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDesArtesanal %d,%s,%s,%s,%s,%s,%f,%f",
								($idEmb) ? "'".$this->PrepareParamSQL($idEmb)."'" : 'NULL',
								($desEmb) ? "'".$this->PrepareParamSQL($desEmb)."'" : 'NULL',
								($idEsp) ? "'".$this->PrepareParamSQL($idEsp)."'" : 'NULL',
								($desEsp) ? "'".$this->PrepareParamSQL($desEsp)."'" : 'NULL',
								($idDes) ? "'".$this->PrepareParamSQL($idDes)."'" : 'NULL',
								($desDes) ? "'".$this->PrepareParamSQL($desDes)."'" : 'NULL',
								($fecDesIni) ? "'".$this->PrepareParamSQL($fecDesIni)."'" : 'NULL',
								($fecDesFin) ? "'".$this->PrepareParamSQL($fecDesFin)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Ejecuta SP para listar Datos de Cada Infraestructura
					$sql_SP = sprintf("sp_busDatosDesArtesanal %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						print $this->conn->ErrorMsg();
					else{
						if($row = $rs->FetchRow())
							$html->append('arrDes', array('id' => $row[0],
												  'nEmb' => $row[1],
												  'mEmb' => $row[2],
												  'dEsp' => $row[3],
												  'dDes' => $row[4],
												  'nVol' => number_format($row[5],2),
												  'nPre' =>	number_format($row[6],2),
												  'fDes' => $row[7],
												  'dTur' => $row[8]));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		*/
		/*
		// Setea accion para el resultado Accion
			$html->assign('accion',$this->arr_accion);
		
		// Setea datos del Formulario CSV
			$html->assign('frmName','frmCSV');
			$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);			
		
		// Setea Datos del Resultado de la Busqueda
			$html->assign('outStart',($start!=1) ? $start+1 : $start);
			$html->assign_by_ref('outEnd',$stop);
			$html->assign_by_ref('outTotal',$numRegs);
			$html->assign_by_ref('menuSearchPaginable',$this->MenuSearchPaginable($numRegs, $page, $frmName, $this->arr_accion[BUSCAR], true));

		// Muestra el Resultado de la Busqueda
			$html->display('dnpa/desembarque/buscaDesembarque.tpl.php' : 'dnpa/desembarque/descargaBuscaDesembarque.tpl.php');
			*/
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin,true);
		
		//echo "dd".$idEmb;
		//echo "<br>jj".$idEsp;

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		if($idEmb>0)
			$condConsulta[] = "id_embarcacion=$idEmb";
		if($idEsp>0)
			$condConsulta[] = "id_especie=$idEsp";				
		if($idDest>0)
			$condConsulta[] = "id_destino=$idDest";
		if($fecDesIni&& $fecDesIni!="")
			$condConsulta[] = "fec_creacion>convert(datetime,'$fecDesIni',103)";
		if(($fecDesIni&& $fecDesIni!="")&&($fecDesFin&& $fecDesFin!=""))
			$condConsulta[] = "fec_creacion<dateadd(dd,1,convert(datetime,'$fecDesFin',103))";
		// Arma el Condicional que sera paramero del Store Procedure
		//$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
			$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_SP = sprintf("EXECUTE sp_busIDDesArtesanal %s,%s,0,0",
							($desEmb) ? "'".$this->PrepareParamSQL($desEmb)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDesArtesanal %s,%s,%s,%s",
								($nopc) ? "'".$this->PrepareParamSQL($nopc)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosDesArtesanal %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
							$html->append('arrDes', array('id' => $row[0],
												  'nEmb' => $row[0],
												  'mEmb' => $row[1],
												  'dEsp' => $row[2],
												  'dDes' => $row[3],
												  'nVol' => number_format($row[4],2),
												  'nPre' =>	number_format($row[5],2),
												  'fDes' => $row[6],
												  'dTur' => $row[8]));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscar', $this->arr_accion[BUSCA_DESEMBARQUE], true));

		// Muestra el Resultado de la Busqueda
		$html->display('dnpa/desembarque/buscaDesembarque.tpl.php');
		$html->display('dnpa/desembarque/footerArm.tpl.php');
			
		}
	
	
	
	/*
	function BuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin,$down=false){
		
		$idEmb = empty($idEmb) ? null : $idEmb;
		
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
	
		// Se Muestra el Formulario de Busqueda Inicial
		if(!$down)
			$this->FormBuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin);
		
		$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;

		// Ejecuta el conteo General
		$this->abreConnDB();
	 	$this->conn->debug = true;

		$stmt = $this->conn->PrepareSP('sp_busIDDesembarque');
		//echo "gg".$smt;
		$this->conn->InParameter($stmt,$idEmb,'IDEMB',5,SQLINT4);
		$this->conn->InParameter($stmt,$desEmb,'DESEMB',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$idEsp,'IDESP',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$desEsp,'DESESP',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$idDes,'IDDES',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$desDes,'DESDES',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$fecDesIni,'FECHAINI',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$fecDesFin,'FECHAFIN',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$_SESSION['cod_usuario'],'CODUSER',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$start,'START',4,SQLINT4);
		$this->conn->InParameter($stmt,$this->numMaxResultsSearch,'MAXROWS',4,SQLINT4);
		// return IDS Desembarques
		$ids = '';
		$this->conn->OutParameter($stmt,$ids,'IDS');
		// return value in mssql - RETVAL is hard-coded name 
		$this->conn->OutParameter($stmt,$numRegs,'RETVAL');
		$this->conn->Execute($stmt);

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Obtiene Todos los Datos del Desembarque
			
			$stmt = $this->conn->PrepareSP('sp_busDatosDesembarque'); # note that the parameter name does not have @ in front!
			$this->conn->InParameter($stmt,$ids,'IDS',-1,SQLVARCHAR);
			$rs = & $this->conn->Execute($stmt);
			if(!$rs)
				print $this->conn->ErrorMsg();
			else{
				while($row = $rs->FetchRow())
					$html->append('arrDes', array('id' => $row[0],
												  'nEmb' => $row[1],
												  'mEmb' => $row[2],
												  'dEsp' => $row[3],
												  'dDes' => $row[4],
												  'nVol' => number_format($row[5],2),
												  'nPre' =>	number_format($row[6],2),
												  'fDes' => $row[7],
												  'dTur' => $row[8]));
				$rs->Close();
			}
			unset($rs);
		}else
			$start = 0;

		$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		// $html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		$html->assign('datos',compact('idEmb',
									  'desEmb',
									  'idEsp',
									  'desEsp',
									  'idDes',
									  'desDes',
									  'fecDesIni',
									  'fecDesFin'));
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->GetAccion('BUSCA_DESEMBARQUE'), true));

		// Muestra el Resultado de la Busqueda
		$html->display((!$down) ? 'dnpa/desembarque/buscaDesembarque.tpl.php' : 'dnpa/desembarque/descargaBuscaDesembarque.tpl.php');
	}
	
	
	/*
	function DescargaBuscaDesembarque($idEmbarcacion,$desEmbarcacion,$idEspecie,$desEspecie,$idDestino,$desDestino,$desFechaIni,$desFechaFin){
		//Begin writing headers
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: public'); 
		header('Content-Description: File Transfer');
		
		header('Content-type: application/csv');
		$desFile = 'desembarque-' . str_replace('/','',$desFechaIni);
		$desFile .= (($desFechaIni!=$desFechaFin)&&$desFechaFin) ? '_' . str_replace('/','',$desFechaFin) : '';
		$desFile .= '.csv';
		header('Content-Disposition: attachment; filename="' . $desFile . '"');
		$this->BuscaDesembarque(null,$idEmbarcacion,$desEmbarcacion,$idEspecie,$desEspecie,$idDestino,$desDestino,$desFechaIni,$desFechaFin,true);
		exit;
	}
	*/
	
	function FormIngresaDesembarque($fecDesembarque=null,$idTurno=null,$idEmb=null,$idEsp=null,$idDes=null,$numVol=null,$numPre=null,$registro=null,$errors=null){
		$html = & new Smarty;
		
		$html->assign('frmName','frmIngresa');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);

		$html->assign_by_ref('fecDesembarque',$fecDesembarque);
		$html->assign_by_ref('idTurno',$idTurno);
		$html->assign_by_ref('idEmb',$idEmb);
		$html->assign_by_ref('idEsp',$idEsp);
		$html->assign_by_ref('idDes',$idDes);
		$html->assign_by_ref('numVol',$numVol);
		$html->assign_by_ref('numPre',$numPre);
				
		// Contenido Select Turno
		$sql_st = "SELECT id_turno, lower(descripcion) ".
				  "FROM dbo.turno ";
		$html->assign_by_ref('selTurno',$this->ObjFrmSelect($sql_st, $idTurno, true, true, array('val'=>'none','label'=>'-- Seleccione --')));
		unset($sql_st);

		for($i=0;$i<count($registro);$i++)
			$html->append('registro',explode('||||',$registro[$i]));
			
		$html->assign_by_ref('errors',$errors);
		
		// Variables de Session
		$html->assign_by_ref('sessionTime',$_SESSION['session_max_time']);
		
		$html->display('dnpa/desembarque/formIngresaDesembarque.tpl.php');
	}
	
	function IngresaDesembarque($fecDesembarque,$idTurno,$idEmb,$idEsp,$idDes,$numVol,$numPre,$registro){
	
		//echo "hi".$registro[0];
		$contador=count($registro);
		//echo "el contador es ".$contador;
		for($i=0;$i<count($registro);$i++){
				$mm=$registro[$i];
				$parte=explode("||||",$mm);
				$idEmbarcacion[$i]=$parte[0];
				$idEspecie[$i]=$parte[1];
				$idDestino[$i]=$parte[2];
				$volumen[$i]=$parte[3];
				$precio[$i]=$parte[4];				
				
		}
		//echo "el primer registro es ".$registro[0];	
		//echo "<br>especie es ".$idEspecie[0];	
		//echo "<br>especie2 es ".$idEspecie[1];
		//echo "<br>hi2".$registro[1];
				
		//Manipulacion de las Fechas
		$fecDesembarque = ($fecDesembarque!='//'&&$fecDesembarque&&!strstr($fecDesembarque,'none')) ? $fecDesembarque : NULL;
		
		// Comprueba Valores
		$bFDes = $bTur = $bReg = false;
		
		if($fecDesembarque){ $bFDes = ($this->ValidaFecha($fecDesembarque,'Desembarque')); }else{ $bFDes = true; $this->errors .= 'La Fecha de Desembarque debe ser especificada<br />'; }
		if(!$idTurno){ $bTur = true; $this->errors .= 'El Turno debe ser especificado<br />'; }
		if(!is_array($registro)){ $bReg = true; $this->errors .= 'Debe especificar datos para el desembarque.<br />'; }

		if($bFDes||$bTur||$bReg){
			$objIntranet = new Intranet();
			$objIntranet->Header('Desembarque Diario',false,array('desDNPA'));
			$objIntranet->Body('desDNPA_tit.gif',false,false);
			$this->MuestraMenu();
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormIngresaDesembarque($fecDesembarque,$idTurno,$idEmb,$idEsp,$idDes,$numVol,$numPre,$registro,$errors);
			$objIntranet->Footer(false);
		}else{

			$this->abreConnDB();
			//$this->conn->debug = true;
			
			for($i=0;$i<$contador;$i++){

				$idEmbF=$idEmbarcacion[$i];
				$idEspF=$idEspecie[$i];
				$idDestinoF=$idDestino[$i];
				$volumenF=$volumen[$i];
				$precioF=$precio[$i];
				
				//echo "especiessssss ".$idEspF;				
			
						$sql_SP = sprintf("EXECUTE sp_insDesembarque2 '%s',%d,%d,%s,%s,%f,%f,%s,%s",
											($fecDesembarque) ? $this->PrepareParamSQL($fecDesembarque) : "'",
											($idTurno) ? $this->PrepareParamSQL($idTurno) : "'",
											($idEmbF) ? $this->PrepareParamSQL($idEmbF) : "'",
											($idEspF) ? "'".$this->PrepareParamSQL($idEspF)."'" : "' '",
											($idDestinoF) ? "'".$this->PrepareParamSQL($idDestinoF)."'" : "' '",
											($volumenF) ? $this->PrepareParamSQL($volumenF) : NULL,
											($precioF) ? $this->PrepareParamSQL($precioF) : NULL,
											($tzTrans) ? "'".$this->PrepareParamSQL($tzTrans)."'" : "' '",
											$_SESSION['cod_usuario']
											);
						 //echo $sql_SP; exit;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
							$rs->Close();
						}
						unset($rs);
			}
			
			$destination = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->GetAccion('MAL_ING_DES') : $this->GetAccion('BUEN_ING_DES');
			$destination .= '&menu=' . $this->menuPager;
			$destination .= ($RETVAL) ? '' : '&tz=' . $tzTrans;
			
			//echo "as ".$destination;
			
			header('Location: ' . $destination);
			exit;
		}
	}
	
	function EstadoIngresaDesembarque($status,$timetrans=null){
		if($status==$this->GetAccion('BUEN_ING_DES'))
			$msgStat = '<strong>El ingreso de datos se ha llevado a cabo con &eacute;xito.</strong>';
		elseif($status==$this->GetAccion('MAL_ING_DES'))
			$msgStat = '<strong>Hubo un error al realizar el ingreso de datos. Vuelva a intentarlo en breves momentos. Gracias.</strong>';
			
		$html = & new Smarty;
		
		$html->assign_by_ref('msgStat',$msgStat);
		$html->assign_by_ref('timetrans',$timetrans);
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/estadoIngresaDesembarque.tpl.php');
	}
	
	function ListaIngresaDesembarque($tzTrans){
		$html = & new Smarty;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$stmt = $this->conn->PrepareSP('sp_listDesembarqueTrans'); # note that the parameter name does not have @ in front!
		$this->conn->InParameter($stmt,$tzTrans,'TIMETRANS',-1,SQLVARCHAR);
		$rs = & $this->conn->Execute($stmt);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('arrReg',array('nEmb'=>$row[0],
											 'mEmb'=>$row[1],
											 'dEsp'=>$row[2],
											 'dDes'=>$row[3],
											 'nVol'=>$row[4],
											 'nPre'=>$row[5],
											 'fDes'=>$row[6]));
			}
			$rs->Close();
		}
		unset($rs);			
		
		//Begin writing headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public"); 
		header("Content-Description: File Transfer");
		
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=desembarque-".$tzTrans.".csv");
		$html->display('dnpa/desembarque/listaIngresaDesembarque.tpl.php');
	}
	
	function muestraInfoInteres(){
		$html = & new Smarty;
		
		$html->assign('frmName','frmBuscar');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/muestraInfoInteres.tpl.php');
	
	}
}
?>
