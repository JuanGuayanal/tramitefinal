<?
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');
include_once 'mimemail/htmlMimeMail.php';

class PagosxDevolucionxDecomiso extends Modulos{

	var $folderXML = '../tramite/xml';
	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function PagosxDevolucionxDecomiso($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans',
							FRM_BUSCA_CUENTASXPAGAR =>'frmSearchCuentasxPagar',
							BUSCA_CUENTASXPAGAR =>'searchCuentasxPagar',
							BUSCA_PERSONA_EXTENDIDO => 'frmSearchExtendedPer',
							FRM_REGISTRA_DECOMISO => 'frmRegistroCuentasxCobrar',
							REGISTRA_DECOMISO => 'registroCuentasxCobrar',
							FRM_REGISTRA_TASA => 'frmRegistroTasa',
							FRM_CAMBIA_ESTADO => 'frmChangeStatus',
							CAMBIA_ESTADO => 'changeStatus',
							FRM_MODIFICA_DATOS => 'frmModifyDatos',
							MODIFICA_DATOS => 'modifyDatos',
							FRM_AGREGA_CONTACTO => 'frmAddContacto',
							AGREGA_CONTACTO => 'addContacto',
							IMPRIMIR_CLAVE => 'printClave',
							MUESTRA_DETALLE_PERSONA => 'showDetailPer',
							IMPRIME_DETALLE =>'ImprimeDetalle',
							FRM_GENERA_REPORTE => 'frmGeneraReporte',
							GENERA_REPORTE => 'generaReporte',
							MUESTRA_DETALLE => 'showDetail',
							IMPRIME_DETALLE => 'printDetail'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		if($this->userIntranet['COD_DEP']==7||$_SESSION['cod_usuario']=="jtumay"){
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchCuentasxPagar', label => 'BUSCAR' ),
											 1 => array ( 'val' => 'frmRegistroCuentasxCobrar', label => 'AGREGAR'),
											 2 => array ( 'val' => 'frmModifyPersona', label => 'MODIFICAR'),
											 3 => array ( 'val' => 'frmAddContacto', label => 'CONTACTO')
											);
		}else{
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchCuentasxPagar', label => 'BUSCAR' )
											);
		}
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
    }

	function MuestraStatTrans($accion){
		global $id,$clave;//Id de la Persona
		global $TipoDoc;
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. DEBE REALIZAR PREVIAMENTE UNA B�SQUEDA. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		if($id){//�Existe id de la Persona?
		
					$imgStat = 'stat_true.gif';
					$imgOpcion = 'img_printer2.gif';
					$altOpcion = 'Obtener Clave';
					$lnkOpcion = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_CLAVE]}&id={$id}&clave={$clave}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

					$html->assign_by_ref('id',$id);
					$html->assign_by_ref('imgOpcion',$imgOpcion);
					$html->assign_by_ref('altOpcion',$altOpcion);
					$html->assign_by_ref('lnkOpcion',$lnkOpcion);
		}
		
		//echo "xx".$TipoDoc."xx";
		if($TipoDoc && $TipoDoc>0)
			$html->assign_by_ref('TipoDoc',$TipoDoc);
				
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_PERSONA]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('oad/showStatTrans.inc.php');
		$html->display('oad/footerArm.tpl.php');
	}

	function MuestraIndex(){
			$this->FormBuscaCuentasxPagarDecomisoDevolucion();
	}

	function FechaActual(){
		setlocale (LC_TIME, $this->zonaHoraria);
		return ucfirst(strtolower(strftime("%d/%m/%Y")));
	}
	function HoraActual(){
		//setlocale (LC_TIME, $this->zonaHoraria);
		//return ucfirst(strtolower(strftime("%T")));	
		$this->abreConnDB();
//		$this->conn->debug = true;

		$sql="select convert(varchar,getDate(),108)";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rs->fields[0];
			}
		return($hora);
	}
	/* Funcion para validar Email */
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	} 	
	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	function FormBuscaCuentasxPagarDecomisoDevolucion($page=NULL,$desNombre=NULL,$checkTodos=NULL,$checkMatricula=NULL,$checkEmbarcacion=NULL,$checkArmador=NULL,$FechaIni=NULL,$FechaFin=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscaCuentasxPagar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('ruc',$ruc);

		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Departamento
		$sql_st = "SELECT codigo_departamento, lower(departamento) ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 2";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa2',$this->ObjFrmSelect($sql_st, $codDepa2, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$accionHeader=0;
		$html->assign_by_ref('accionHeader',$accionHeader);		
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/search.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	
	function busTipPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select id_tipo_persona 
		         from db_general.dbo.tipo_persona 
				 where ID_TIPO_PERSONA=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
					$tipPersona=$row[0];
					//echo "el valor del contador es: ".$contador;
			}
			//unset($rs);
		return($tipPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}

	function busPrivPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="select rubro 
		         from db_general.dbo.persona 
				 where id=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					//while ($row = $rs->FetchRow())
					$privPersona=$rs->fields[0];
					//echo "el valor del contador es: ".$privPersona;
			}
			//unset($rs);
		return($privPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}
	
	function calculaInteres($monto,$fechaInicial,$dias,$fechaPago){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$interes=$monto;
		/*$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<dateadd(dd,1,getDate()) 
					ORDER BY convert(datetime,fecha_tasa,103)";*/
		if($fechaPago && $fechaPago!="")
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<convert(datetime,'$fechaPago',103) 
					ORDER BY convert(datetime,fecha_tasa,103)";
		else
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<getDate() 
					ORDER BY convert(datetime,fecha_tasa,103)";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row=$rs->FetchRow()){
				//$interes=$interes+$interes*($row[0]/36500);
				$interes=$interes+$interes*(pow((1+($row[0]/100)),(1/360))-1);
				//echo $interes."<br>";
			}	
			$rs->Close();
		}
		unset($rs);		
		//$interes=$monto*$tasa;
		return($interes);
	}
	
	function BuscaCuentasxPagarDecomisoDevolucion($page,$desNombre,$checkTodos,$checkMatricula,$checkEmbarcacion,$checkArmador,$FechaIni,$FechaFin){

		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscaPersona';
		$html->assign('frmName','frmSearchExtendedPer');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array('desNombre'=>$desNombre,
									'checkTodos'=>$checkTodos,
									'checkMatricula'=>$checkMatricula,
									'checkEmbarcacion'=>$checkEmbarcacion,
									'checkArmador'=>$checkArmador,
									'FechaIni'=>$FechaIni,
									'FechaFin'=>$FechaFin
									));
		
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaCuentasxPagarDecomisoDevolucion($page,$desNombre,$checkTodos,$checkMatricula,$checkEmbarcacion,$checkArmador,$FechaIni,$FechaFin,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
				
		// Condicionales Segun ingreso del Usuario
		/*$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";*/
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";
		
		if(!empty($codDepa2)&&!is_null($codDepa2)&&$codDepa2!='none'){
			$condConsulta[] = "p.codigo_departamento='$codDepa2' ";
			if(!empty($codProv)&&!is_null($codProv)&&$codProv!='none'){
				$condConsulta[] = "p.codigo_provincia='$codProv' ";
				if(!empty($codDist)&&!is_null($codDist)&&$codDist!='none'){
					$condConsulta[] = "p.codigo_distrito='$codDist' ";
				}
			}
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		//echo "<br>la tabla".$table."<br>el wherte".$where."<br>tipo de cor".$TipoCor."<br>";

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);
	

			$sql_SP = sprintf("EXECUTE sp_busIdCuentaPagar %s,%s,%s,0,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
				$sql_SP = sprintf("EXECUTE sp_busIdCuentaPagar %s,%s,%s,%s,%s",
									($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$start,
									$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosCuentaPagar %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow())
							$html->append('arrCuenta', array('id' => $id[0],
														  'nombreEmb' => ucwords($NotiData[1]),
														  'matriculaEmb' => $NotiData[2],
														  'armadorEmb' => $NotiData[3],
														  'fecha' => ucfirst($NotiData[4]),
														  'monto' => strtoupper($NotiData[5]),
														  'doc' => strtoupper($NotiData[6]),
														  'flag' => strtoupper($NotiData[7]),
														  'dias' => $NotiData[8],
														  'fechaPago' => $NotiData[9],
														  'interes' => $this->calculaInteres($NotiData[5],$NotiData[4],$NotiData[6],$NotiData[9]),
														 'nrocomprobante' => $NotiData[10],	
														 'nrosiaf' => $NotiData[11],	
														 'nrocheque' => $NotiData[12]	
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		
		$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('userDirectorio',$_SESSION['cod_usuario']);

		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_CUENTASXPAGAR], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscaCuentasxPagar', $this->arr_accion[BUSCA_CUENTASXPAGAR], true));

		// Muestra el Resultado de la Busqueda
		$html->display('ot/pagosDevolucionxDecomiso/searchResult.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		
	}
	
	function VistaExtendidaDocPersona($desNombre,$sector,$codDepa,$tipPersona,$ruc){
		$html = new Smarty;

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,1,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			while($id = $rsId->FetchRow()){
				// Obtiene Todos los Datos de la Parte
				$sql_SP = sprintf("EXECUTE sp_busDatosPersona %d",$id[0]);
				//echo $sql_SP;
				$rsData = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if(!$rsData)
					print $this->conn->ErrorMsg();
				else{
					if($PerData = $rsData->FetchRow())
							$html->append('arrPer', array('id' => $id[0],
														  'desNom' => ucwords($PerData[1]),
														  'tipPer' => $PerData[2],
														  'nro' => $PerData[3],
														  'dire' => ucfirst($PerData[4]),
														  'tel' => strtoupper($PerData[5]),
														  'mail' => $PerData[6],
														  'tipPersona' => $this->busTipPersona($id[0])
														  //'acui' => $this->busPrivPersona($id[0])
														  ));
					$rsData->Close();
				}
				unset($rsData);
			}
			$rsId->Close();
		}
		unset($rsId);

		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('oad/tramite/viewExtendedPer.tpl.php');
		exit;
	}
	
	function FormRegistraCuentasxCobrar($RZoRUC=NULL,$BuscaRZ=NULL,$Buscar=NULL,
									$embarcacion=NULL,$armador=NULL,$matriculaEmb=NULL,$moneda=NULL,$monto=NULL,
									$tipoDocc=NULL,$numero2=NULL,$anyo2=NULL,$siglasDepe2=NULL,$desFechaIni=NULL,$errors=false){

		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/**/
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRegistroCuentasxCobrar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		
		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('BuscaRZ',$BuscaRZ);
		$html->assign_by_ref('concepto',$concepto);
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('hoy',date('d/m/Y'));
		$html->assign_by_ref('tipGarantia',$tipGarantia);
		// Setea Muestra de Campos del Moneda
		$sql_st = "SELECT id,descripcion+' '+siglas ".
				  "FROM db_general.dbo.moneda ".
				  "ORDER BY 2";
		$html->assign('selMoneda',$this->ObjFrmSelect($sql_st, $moneda, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT distinct cdi.id_clase_documento_interno, lower(cdi.descripcion) ".
				  "FROM dbo.clase_documento_interno cdi,cuentainterno c ".
				  "WHERE cdi.procedencia='i' AND cdi.categoria='D' ".
				  "and cdi.id_clase_documento_interno=c.id_tipo_documento and c.coddep=".$this->userIntranet['COD_DEP']." ".
				  "and cdi.id_clase_documento_interno not in (46,47,59,55,38) ".
				  "ORDER BY 2";
				  
				  ///				  "and cdi.id_clase_documento_interno not in (46,47,59,55,38,2) ".
		$html->assign_by_ref('selTipoDocc',$this->ObjFrmSelect($sql_st, $tipoDocc, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
				
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where codigo_dependencia in(8,24,25,12,47) ".
				  "AND CONDICION='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		if($Buscar==1 && $RZoRUC!=""){
			$sql_st = "select ID_EMB, NOMBRE_EMB
						from db_DNEPP.USER_DNEPP.EMBARCACIONNAC
						where (NOMBRE_EMB like '%$RZoRUC%' or MATRICULA_EMB like '%$RZoRUC%' )
						order by 2
					  ";
			$html->assign_by_ref('selEmbarcacion',$this->ObjFrmSelect($sql_st, $embarcacion, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
			unset($sql_st);
		}else{
		$html->assign_by_ref('selEmbarcacion',$this->ObjFrmSelect($sql_st, $embarcacion, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		
		if($embarcacion>0){
			$this->abreConnDB();
			/*$sql="select e.ID_EMB,e.NOMBRE_EMB,e.matricula_emb,a.razonsocial_arm,a.id_arm
						from db_DNEPP.USER_DNEPP.EMBARCACIONNAC e,db_DNEPP.USER_DNEPP.EMBXARM ea,
							 DB_DNEPP.USER_DNEPP.ARMADOR a
						WHERE e.ID_EMB=$embarcacion and e.id_emb=ea.id_emb and ea.estado_embxarm=1
							and ea.id_arm=a.id_arm";*/
			$sql="select e.ID_EMB,e.NOMBRE_EMB,e.matricula_emb,case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
					else sol.razon_social end,sol.id
						from db_DNEPP.USER_DNEPP.EMBARCACIONNAC e,db_DNEPP.USER_DNEPP.EMBXpersona ea,
							 db_general.dbo.persona sol
						WHERE e.ID_EMB=$embarcacion and e.id_emb=ea.id_emb and ea.estado_embxpers=1
							and ea.id_pers=sol.id";				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					//echo "matrix";
					$matriculaEmb=$rs->fields[2];
					$armador=$rs->fields[3];
					$idArmador=$rs->fields[4];
					$html->assign_by_ref('matriculaEmb',$matriculaEmb);
					$html->assign_by_ref('armador',$armador);
					$html->assign_by_ref('idArmador',$idArmador);
				}
		}
				
		$accionHeader=1;
		$html->assign_by_ref('accionHeader',$accionHeader);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/frmRegistroCuentasxPagar.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	function RegistraCuentasxCobrar($RZoRUC,$BuscaRZ,$Buscar,
									$embarcacion,$armador,$matriculaEmb,$moneda,$monto,
									$tipoDocc,$numero2,$anyo2,$siglasDepe2,$desFechaIni){
									
		if($tipoDocc>0){
			$this->abreConnDB();
			//$this->conn->debug = true;		
			$sql="select count(*)
				  from documento
				WHERE id_clase_documento_interno=$tipoDocc and indicativo_oficio like '%$numero2-%' and year(auditmod)=$anyo2 and coddep=$siglasDepe2
			 ";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				//print $this->conn->ErrorMsg();
				return;
			}else{
				$contador = $rs->fields[0];
			}//fin del else
			unset($rs);	
		}
		
									
		// Comprueba Valores
		if($embarcacion<1){ $b1 = true; $this->errors .= 'La Embarcaci�n debe ser especificada.<br>'; }
		if(!$armador){ $b2 = true; $this->errors .= 'El armador debe ser especificado.<br>'; }
		if(!matriculaEmb){ $b3 = true; $this->errors .= 'La matr�cula debe ser especificada.<br>'; }
		if($moneda<1){ $b4 = true; $this->errors .= 'El tipo de moneda debe ser especificado.<br>'; }
		if(!$monto){ $b5 = true; $this->errors .= 'El monto debe ser especificado.<br>'; }
		if(!$desFechaIni){ $b6 = true; $this->errors .= 'La de fecha de custodia debe ser especificado.<br>'; }
		if($contador==0||$contador<0||!$contador){ $b7 = true; $this->errors .= 'El documento debe existir.<br>'; }

		if($b1||$b2||$b3||$b4||$b5||$b6||$b7){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('tramite_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormRegistraCuentasxCobrar($RZoRUC,$BuscaRZ,$Buscar,
									$embarcacion,$armador,$matriculaEmb,$moneda,$monto,
									$tipoDocc,$numero2,$anyo2,$siglasDepe2,$desFechaIni,$errors);
			
			$objIntranet->Footer();
		}else{

			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE sp_registraCuentaPagar %d,%s,%s,%d,%s,%d,%s,%d,%d,%s,%d,'%s'",
							  ($embarcacion) ? $this->PrepareParamSQL($embarcacion) : "NULL",
							  ($armador) ? "'".$this->PrepareParamSQL($armador)."'" : "' '",
							  ($matriculaEmb) ? "'".$this->PrepareParamSQL($matriculaEmb)."'" : "' '",
							  ($moneda) ? $this->PrepareParamSQL($moneda) : "NULL",
							  ($monto) ? "'".$this->PrepareParamSQL($monto)."'" : "NULL",
							  ($tipoDocc) ? $this->PrepareParamSQL($tipoDocc) : "NULL",
							  ($numero2) ? "'".$this->PrepareParamSQL($numero2)."'" : "NULL",
							  ($anyo2) ? $this->PrepareParamSQL($anyo2) : "NULL",
							  ($siglasDepe2) ? $this->PrepareParamSQL($siglasDepe2) : "NULL",
							  ($desFechaIni) ? "'".$this->PrepareParamSQL($desFechaIni)."'" : "NULL",
							  $this->userIntranet['COD_DEP'],
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormRegistraTasa($tasa=NULL,$desFechaIni=NULL,$valor,$errors=false){
		// Genera Objeto HTML
		$html = new Smarty;

		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/*Se calcula todas las tasas existentes*/
			$this->abreConnDB();
			//$this->conn->debug = true;		
					$sql_SP = "select id,FECHA_TASA,TASA_ANUAL
									from DB_SANCION.dbo.tasa_diaria_legal 
									WHERE FECHA_TASA IS NOT NULL
									 and convert(datetime,fecha_tasa,103)>=convert(datetime,'01/10/2007',103)
									 order by convert(datetime,fecha_tasa,103) desc";
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while ($row = $rs->FetchRow())
							$html->append('arrCuenta', array('id' => $row[0],
														  'fechaTasa' => $row[1],
														  'tasaAnual' => $row[2]
														  ));
						$rs->Close();
					}
					unset($rs);		
		
		/**/
		if(!valor||$valor==""){
			$valor=2;
		}elseif($valor==2){
			$tasaDiaria=$tasa/360;
			$this->abreConnDB();
			//$this->conn->debug = true;
			$sql_SP = sprintf("exec DB_SANCION.user_siprosac.sp_insert_tasa '%s','%s','%s','%s'",$tasaDiaria,$tasa,$_SESSION['cod_usuario'],$desFechaIni);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				echo "Se agreg� correctamente";
			}			
		}
		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRegistroTasa';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		
		$html->assign_by_ref('tasa',$tasa);
		$html->assign_by_ref('valor',$valor);
		$hoy=date('d/m/Y');
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/frmRegistroTasa.tpl.php');
		//$html->display('oad/footerArm.tpl.php');	
	}
	
	function FormCambiaEstado($id,$nroComprobante=NULL,$nroSIAF=NULL,$tipResol=NULL,$numero2=NULL,$anyo2=NULL,$siglasDepe2=NULL,$opcdevolucion,$reLoad=false,$errors=false){
		global $desFechaIni;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}

		// Genera Objeto HTML
		$html = new Smarty;
		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmChangeStatus';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nroComprobante',$nroComprobante);
		$html->assign_by_ref('nroSIAF',$nroSIAF);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('desFechaIni',$desFechaIni);

		if($_REQUEST['xpagoreal']==""){ $xxpagoreal=1;}else{ $xxpagoreal=$_REQUEST['xpagoreal']; }
		$html->assign_by_ref('xpagoreal',$xxpagoreal);

		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_tramite_documentario.dbo.tipo_resolucion ".
				  "where id in (1,2,3,4,7) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $tipResol, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where codigo_dependencia in(8,24,25,12,47) ".
				  "AND CONDICION='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/frmChangeStatus.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}

	function CambiaEstado($id,$nroComprobante,$nroSIAF,$tipResol,$numero2,$anyo2,$siglasDepe2,$opcdevolucion){
		global $desFechaIni;
			$this->abreConnDB();
			//$this->conn->debug = true;
					$var=$numero2."-".$anyo2;	
					$sql_SP = "select id
									from DB_TRAMITE_DOCUMENTARIO.dbo.resolucion 
									WHERE coddep=$siglasDepe2 and id_tipo_resolucion=$tipResol
									and nro_resol like '%{$var}%' ";
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						$idResolucion=$rs->fields[0];
						$rs->Close();
					}
					unset($rs);		
	
		// Comprueba Valores	
		if($tipResol<1){ $bC1 = true; $this->errors .= 'El Tipo de resoluci�n debe ser especificado<br>'; }
		if($anyo2<1){ $bC2 = true; $this->errors .= 'El anyo debe ser especificado<br>'; }
		if($siglasDepe2<1){ $bC3 = true; $this->errors .= 'La sigla debe ser especificado<br>'; }

		/* Se establece como no obligatorio */
		// if(!$nroComprobante){ $bC4 = true; $this->errors .= 'El n�mero de comprobante debe ser especificado<br>'; }
		// if(!$nroSIAF){ $bC5 = true; $this->errors .= 'El n�mero SIAF debe ser especificada<br>'; }
		
		if(!$numero2){ $bC6 = true; $this->errors .= 'El n�mero de resoluci�n debe ser especificada<br>'; }
		if(!$idResolucion||$idResolucion==0||$idResolucion<0){ $bC7 = true; $this->errors .= 'Ingrese un n�mero de resoluci�n v�lido<br>'; }

		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormCambiaEstado($id,$nroComprobante,$nroSIAF,$tipResol,$numero2,$anyo2,$siglasDepe2,true,$errors);
			
			$objIntranet->Footer();
		}else{
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE sp_changeStatus_16042012 %d,'%s','%s',%d,'%s',%d,'%s',%d ",
								$id,
							  ($nroComprobante) ? $this->PrepareParamSQL($nroComprobante) : "NULL",
							  ($nroSIAF) ? $this->PrepareParamSQL($nroSIAF) : "NULL",
							  ($idResolucion) ? $this->PrepareParamSQL($idResolucion) : "NULL",
							  ($desFechaIni) ? $this->PrepareParamSQL($desFechaIni) : date('d/m/Y'),
							  $this->userIntranet['COD_DEP'],
							  $_SESSION['cod_usuario'],
								$opcdevolucion
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaDatos($id,$nroComprobante=NULL,$nroSIAF=NULL,$tipResol=NULL,$numero2=NULL,$anyo2=NULL,$siglasDepe2=NULL,$reLoad=false,$errors=false){
		global $desFechaIni;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}

		// Genera Objeto HTML
		$html = new Smarty;
		
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyDatos';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nroComprobante',$nroComprobante);
		$html->assign_by_ref('nroSIAF',$nroSIAF);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('desFechaIni',$desFechaIni);

		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_tramite_documentario.dbo.tipo_resolucion ".
				  "where id in (1,2,3,4,7) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $tipResol, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where codigo_dependencia in(8,24,25,12,47) ".
				  "AND CONDICION='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/frmModifyDatos.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}

	function ModificaDatos($id,$nroComprobante,$nroSIAF,$tipResol,$numero2,$anyo2,$siglasDepe2){
		global $desFechaIni;
			$this->abreConnDB();
			//$this->conn->debug = true;
	
		// Comprueba Valores	
		if(!$nroComprobante){ $bC1 = true; $this->errors .= 'El n�mero de comprobante debe ser especificado<br>'; }
		if(!$nroSIAF){ $bC2 = true; $this->errors .= 'El n�mero SIAF debe ser especificada<br>'; }

		if($bC1||$bC2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaDatos($id,$nroComprobante,$nroSIAF,$tipResol,$numero2,$anyo2,$siglasDepe2,true,$errors);
			
			$objIntranet->Footer();
		}else{
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE sp_modifyDatosPago %d,'%s','%s',%d,'%s'",
								$id,
							  ($nroComprobante) ? $this->PrepareParamSQL($nroComprobante) : "NULL",
							  ($nroSIAF) ? $this->PrepareParamSQL($nroSIAF) : "NULL",
							  $this->userIntranet['COD_DEP'],
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}	
	
	function FormGeneraReporte($search2=false){
		global $desFechaFin,$FechaFinVenc,$status,$dependencia;
		global $GrupoOpciones1,$tipGarantia;
		global $RazonSocial,$desFechaIni,$FechaFinCust;

		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('FechaFinVenc',$FechaFinVenc);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('depe',$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('cod',$this->userIntranet['CODIGO']);
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('FechaFinCust',$FechaFinCust);
		$html->assign_by_ref('status',$status);
		
		$html->assign_by_ref('tipoDocc',$tipoDocc);
		$html->assign_by_ref('tipGarantia',$tipGarantia);
		
		$codigoTrabajador=$this->userIntranet['CODIGO'];
			
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where codigo_dependencia in(8,24,25,12,47) ".
				  "AND CONDICION='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
				
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecInicio,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
		//$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));		

		$accionHeader=2;
		$html->assign_by_ref('accionHeader',$accionHeader);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display('ot/pagosDevolucionxDecomiso/headerArm.tpl.php');
		$html->display('ot/pagosDevolucionxDecomiso/reportes.tpl.php');
		if(!$search2) $html->display('oad/footerArm.tpl.php');
	}
	
	/**/function GeneraReporte($GrupoOpciones1,$desFechaFin,$FechaFinVenc,$status,$dependencia,$RazonSocial,$desFechaIni,$FechaFinCust){
		global $tipGarantia;
		//echo "HOLA".$GrupoOpciones1;exit;		
		switch($GrupoOpciones1){
			case 1:
			$this->ListadoTotal($GrupoOpciones1,$desFechaFin,$FechaFinVenc,$status,$dependencia,$RazonSocial,$desFechaIni,$FechaFinCust,$tipGarantia);
			break;
			case 2:
			$this->ListadoTotal($GrupoOpciones1,$desFechaFin,$FechaFinVenc,$status,$dependencia,$RazonSocial,$desFechaIni,$FechaFinCust,$tipGarantia);
			break;
		}
	}
	
	function ListadoTotal($GrupoOpciones1,$desFechaFin,$FechaFinVenc,$status,$dependencia,$RazonSocial,$desFechaIni,$FechaFinCust,$tipGarantia){
		//echo "xxxx";
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		//echo "x".$desFechaIni." ".$FechaFinCust."x";

			$sql_st = "	 SELECT p.id,p.ID_EMB,p.NOMBRE_EMB,p.MATRICULA_EMB,p.ARMADOR_EMB,p.FECHA_DEPOSITO,
	 			 			p.MONTO_DEPOSITO,cdi.descripcion+' '+d.indicativo_oficio,p.FLAG,
							datediff(dd,convert(datetime,p.fecha_deposito,103),getDate()),
							case when flag='A' then 'En curso'
								 when flag='p' then 'Devuelto' end,p.fecha_pago,
								 p.nro_comprobante,
								p.nro_registro_siaf,
								p.NRO_CHEQUE,
								case p.opc_devolucion
								when 1 then 'Empresa'
								when 2 then 'CONVENIO_SITRADOC(RDR)' end as tipo_devolucion
	    FROM DB_TRAMITE_DOCUMENTARIO.dbo.CUENTASXPAGAR p,dbo.clase_documento_interno cdi,documento d
		WHERE p.id_documento=d.id_documento
		   and d.id_clase_documento_interno=cdi.id_clase_documento_interno
							   ";
							   
		if($status==1)
				$sql_st .= " and flag='A' ";
		elseif($status==2)
				$sql_st .= " and flag='P' ";				
		if($desFechaFin && $FechaFinVenc)
			$sql_st.= " and convert(datetime,p.fecha_pago,103)>=convert(datetime,'$desFechaFin',103) and convert(datetime,p.fecha_pago,103)<dateadd(dd,1,convert(datetime,'$FechaFinVenc',103)) ";
		
		if($desFechaIni && $FechaFinCust)
			$sql_st.= " and convert(datetime,p.fecha_deposito,103)>=convert(datetime,'$desFechaIni',103) and convert(datetime,p.fecha_deposito,103)<dateadd(dd,1,convert(datetime,'$FechaFinCust',103)) ";			
		
		if($RazonSocial&&$RazonSocial!="")
			$sql_st.= " and p.armador_emb like '%$RazonSocial%'";			

			$sql_st.= " ORDER BY 1 desc";
	  //  echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$sumaInteres=0;
			$sumaMontos=0;
			while ($row = $rs->FetchRow()){
				$interesGenerado=$this->calculaInteres($row[6],$row[5],$row[7]);
				$sumaInteres=$sumaInteres+$interesGenerado;
				$sumaMontos=$sumaMontos+$row[6];
							$html->append('list', array('id' => $row[0],
														  'nroCarta' => ucwords($row[1]),
														  'emb' => $row[2],
														  'matri' => $row[3],
														  'arm' => strtoupper($row[4]),
														  'fecDeposito' => strtoupper($row[5]),
														  'monto' => $row[6],
														  'fecCust' => $row[7],
														  'fecVenc' => $row[8],
														  'dias' => $row[9],
														  'flag' => $row[10],
														  'fechaPago' => $row[11],
														  'interes' => $this->calculaInteres($row[6],$row[5],$row[7],$row[11]),
														  'nrocomprobante' => $row[12],
														  'nrosiaf' => $row[13],
														  'nrocheque' => $row[14],
														  'tipodevolucion' => $row[15]
														  ));
			}											  
			$rs->Close();
		}
		unset($rs);		

		
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('sumaInteres',$sumaInteres);
		$html->assign_by_ref('sumaMontos',$sumaMontos);
		
		/*if($GrupoOpciones1==2){
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';		
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'hoja1'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('oad/cartaFianzas/showListadoPdf.tpl.php'),true,$logo);
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;		
		
		}else*/if($GrupoOpciones1==1){
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('ot/pagosDevolucionxDecomiso/viewExtend.tpl.php');
			exit;
		}
	}
	
	function DetalleDocumento($id, $print=false){
	
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		
		$sql=sprintf("SELECT p.id,p.ID_EMB,p.NOMBRE_EMB,p.MATRICULA_EMB,p.ARMADOR_EMB,p.FECHA_DEPOSITO,
	 			 			p.MONTO_DEPOSITO,cdi.descripcion+' '+d.indicativo_oficio,p.FLAG,

       case isnull(p.fecha_pago,'')
       when '' then datediff(dd,convert(datetime,p.fecha_deposito,103),getDate())
       else datediff(dd,convert(datetime,p.fecha_deposito,103),CONVERT(datetime,p.fecha_pago,103)) end,

							p.nro_comprobante,p.nro_registro_siaf,p.fecha_pago,r.nro_resol
	    FROM DB_TRAMITE_DOCUMENTARIO.dbo.CUENTASXPAGAR p left join DB_TRAMITE_DOCUMENTARIO.dbo.resolucion r on p.id_resolucion=r.id,
			dbo.clase_documento_interno cdi,documento d
		WHERE p.id_documento=d.id_documento
		   and d.id_clase_documento_interno=cdi.id_clase_documento_interno and 
					      p.id=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$embarcacion=$rs->fields[2];
			$matricula=$rs->fields[3];
			$armador=$rs->fields[4];
			$fecDeposito=$rs->fields[5];
			$monto=$rs->fields[6];
			$documento=$rs->fields[7];
			$flag=$rs->fields[8];
			$dias=$rs->fields[9];
			$nroComprobante=$rs->fields[10];
			$nroSiaf=$rs->fields[11];
			$fechaPago=$rs->fields[12];
			$nroResolucion=$rs->fields[13];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('embarcacion',$embarcacion);				  
		$html->assign_by_ref('matricula',$matricula);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('fecDeposito',$fecDeposito);
		$html->assign_by_ref('documento',$documento);	  
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('flag',$flag);
		$html->assign_by_ref('dias',$dias);
		$html->assign_by_ref('nroComprobante',$nroComprobante);
		$html->assign_by_ref('nroSiaf',$nroSiaf);
		$html->assign_by_ref('fechaPago',$fechaPago);
		$html->assign_by_ref('nroResolucion',$nroResolucion);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('fecha2',$fecha2);
		$html->assign_by_ref('user',$user);
		$html->assign_by_ref('user2',$user2);	
		$html->assign_by_ref('fecDer',$fecDer);
		$html->assign_by_ref('avance',$avance);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fechaMaxPlazo',$fechaMaxPlazo);
		
		$interes=$monto;
		if($fechaPago && $fechaPago!="")
			$sql_SP = "SELECT TASA_ANUAL,fecha_tasa FROM db_sancion.dbo.tasa_diaria_legal
						WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fecDeposito',103)
						and convert(datetime,fecha_tasa,103)<convert(datetime,'$fechaPago',103) 
						ORDER BY  convert(datetime,fecha_tasa,103)";
		else
			$sql_SP = "SELECT TASA_ANUAL,fecha_tasa FROM db_sancion.dbo.tasa_diaria_legal
						WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fecDeposito',103)
						and convert(datetime,fecha_tasa,103)<getDate()
						ORDER BY  convert(datetime,fecha_tasa,103)";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row=$rs->FetchRow()){
				//$interes=$interes+$interes*($row[0]/36500);
				$factor=(pow((1+($row[0]/100)),(1/360))-1);
				$interes2=$interes*$factor;
				$interes=$interes+$interes2;
				//$interes=round($interes*100)/100;
				$html->append('interes', array(//'id'=>$id,
										   'tasa'=>$row[0],
										   'fecTasa'=>$row[1],
										   'interes'=>$interes,
										   'interes2'=>$interes2,
										   'factor'=>$factor
										   ));				
				//echo $interes."<br>";
			}	
			$rs->Close();
		}
		unset($rs);
		
		$montoxPagar=$interes;
		$html->assign_by_ref('montoxPagar',$montoxPagar);
				
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));		
		
		//$html->display('oad/tramite/showDetallesDoc.tpl.php');
		$print ? $html->display('ot/pagosDevolucionxDecomiso/printDetalle.tpl.php') : $html->display('ot/pagosDevolucionxDecomiso/showDetalles.tpl.php');
	}
	
 function ImprimeDetalle($id){
  $this->DetalleDocumento($id, true);
 }	
	

}
?>
