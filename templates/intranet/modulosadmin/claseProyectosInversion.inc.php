<?php
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class ProyectosInversion extends Modulos{

    function ProyectosInversion($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][7];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCAR => 'frmSearch',
							BUSCAR => 'search',
							FRM_AGREGA_PROYECTO => 'frmAddProyect',
							AGREGA_PROYECTO => 'addProyect',
							FRM_MODIFICA_PROYECTO => 'frmModifyProyect',
							MODIFICA_PROYECTO => 'modifyProyect',
							ELIMINA_PROYECTO => 'deleteProyect',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		$this->datosUsuarioMSSQL();
		
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		if($this->userIntranet['COD_DEP']!=18 && $this->userIntranet['COD_DEP']!=13){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		   
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		
		if($this->userIntranet['COD_DEP']==25)
			$this->idOrgRes = '18';
		elseif($this->userIntranet['COD_DEP']==24)
			$this->idOrgRes = '1';

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearch', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmAddProyect', label => 'AGREGAR' ),
							2 => array ( 'val' => 'frmModifyProyect', label => 'MODIFICAR' )
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }

	//Fue comentado para no ingresar fechas 24/07/2006	

	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera

		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}

	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('ogdpe/proyectos/headerArm.tpl.php');
		$html->display('ogdpe/proyectos/showStatTrans.inc.php');
		$html->display('ogdpe/proyectos/footerArm.tpl.php');
	}
	
	function FormBuscaProyecto($page=NULL,$desNombre=NULL,$idSector=NULL,$codDepa=NULL,$idEstudio=NULL,$idSituacion=NULL,$idEntidad=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $idSector, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Departamento
		$sql_st = "SELECT codigo_departamento, lower(departamento) ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 2";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Nivel de Estudios
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.tipo_nivel_estudio ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstudio',$this->ObjFrmSelect($sql_st, $idEstudio, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		// Contenido Select Situacion
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.situacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSituacion',$this->ObjFrmSelect($sql_st, $idSituacion, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		// Contenido Select Entidad
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.entidad ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEntidad',$this->ObjFrmSelect($sql_st, $idEntidad, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('ogdpe/proyectos/headerArm.tpl.php');
		$html->display('ogdpe/proyectos/search.tpl.php');
		if(!$search) $html->display('ogdpe/proyectos/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaProyecto();
	}

	
	function BuscaProyecto($page,$desNombre,$idSector,$codDepa,$idEstudio,$idSituacion,$idEntidad){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaProyecto($page,$desNombre,$idSector,$codDepa,$idEstudio,$idSituacion,$idEntidad,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		if(!empty($idSector)&&!is_null($idSector)&&$idSector!='none')
			$condConsulta[] = "p.id_sector={$idSector}";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "p.codigo_departamento='{$codDepa}'";
		if(!empty($idEstudio)&&!is_null($idEstudio)&&$idEstudio!='none')
			$condConsulta[] = "p.id_tipo_nivel_estudio={$idEstudio}";
		if(!empty($idSituacion)&&!is_null($idSituacion)&&$idSituacion!='none')
			$condConsulta[] = "p.id_situacion={$idSituacion}";
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
	
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);	

		$sql_SP = sprintf("EXECUTE sp_busca_id_proyecto %s,%s,%s,0,0",
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		// echo $sql_SP;		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para EjecutA?????ar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busca_id_proyecto %s,%s,%s,%d,%d",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Ejecuta SP para listar Datos de Cada Proyecto
					$sql_SP = sprintf("EXECUTE sp_busca_datos_proyecto %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow()){
							$html->append('arrProy', array('id' => $row[0],
														  'desc' => $row[1],
														  'sect' => ucwords($row[2]),
														  'depa' => ucwords($row[3]),
														  'prov' => ucwords($row[4]),
														  'dist' => ucwords($row[5]),
														  'estu' => ucwords($row[6]),
														  'situ' => ucwords($row[7]),
														  'enti' => ucwords($row[8])));
						}
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
					
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCAR], true));

		// Muestra el Resultado de la Busqueda
		$html->display('ogdpe/proyectos/searchResult.tpl.php');
		$html->display('ogdpe/proyectos/footerArm.tpl.php');
	}
	
	function FormManipulaProyecto($id=NULL,$desNombre=NULL,$codSNIP=NULL,$desObjetivo=NULL,$desAltInv=NULL,$numMonto=NULL,$idSector=NULL,$codDepa=NULL,
									$codProv=NULL,$codDist=NULL, $desLocalidad=NULL,$desCPobla=NULL,$idEstudio=NULL,$idEstudioVia=NULL,$idSituacion=NULL,
									$idEntForm=NULL,$idEntEjec=NULL,$indEnt=NULL,$fecPresentacion=NULL,$fecIngreso=NULL,$fecSalida=NULL, $txtNroBeneficiarios=NULL, $AnnoPrimeraEtapa=NULL, $numMontoPrimEtapa=NULL,
									$AnnoSegundaEtapa=NULL, $numMontoSegEtapa=NULL, $AnnoTerceraEtapa=NULL, $numMontoTerEtapa=NULL, $AnnoCuartaEtapa=NULL, $numMontoCuartaEtapa=NULL, 
									$numMontoPIM=NULL, $numMontoDevengado=NULL, $reLoad=false, $errors=false){
		// Obtiene Datos del Proyecto
		if($id&&!$reLoad){
			// Almacena en una variable $idProy el primer ID a modificar
			// del Array $id
			$idProy = $id[0];

			$this->abreConnDB();
			// $this->conn->debug = true;
			// Obtiene los Datos del Proyecto
			$sql_SP = sprintf("EXECUTE sp_listProyecto_intra %d",$this->PrepareParamSQL($idProy));
			//echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow())
					list($desNombre,
						 $codSNIP,
	 					 $desObjetivo,
						 $desAltInv,
						 $numMonto,
						 $idSector,
						 $codDepa,
						 $codProv,
						 $codDist,
						 $desLocalidad,
						 $desCPobla,
						 $idEstudio,
						 $idEstudioVia,
						 $idSituacion,
						 $idEntForm,
						 $idEntEjec,
						 $indEnt,
						 $fecPresentacion,
						 $fecIngreso,
						 $fecSalida, 
						 $txtNro_Beneficiarios,
						 $AnnoPrimeraEtapa,
						 $numMontoPrimEtapa,
						 $AnnoSegundaEtapa,
						 $numMontoSegEtapa,
						 $AnnoTerceraEtapa,
						 $numMontoTerEtapa,
						 $AnnoCuartaEtapa,
						 $numMontoCuartaEtapa,
						 $numMontoPIM,
						 $numMontoDevengado) = $row;
				$rs->Close();
			}
			unset($rs);
		}
		
		//echo "ANNO_4 ".$numMontoPIM."<br>" ;
		//echo "MONTO_4 ".$numMontoDevengado."<br>" ;
		// Manipulacion de las Fechas
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecNoticia,'none')) ? $fecIngreso : NULL;
		$fecSalida = ($fecSalida!='//'&&$fecSalida&&!strstr($fecNoticia,'none')) ? $fecSalida : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		// Genera el Menu Pager de la Cabecera
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddProyect';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		// A�ade JavaScript para el envio del Formulario
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		
		// Setea Valores de los Campos del Formulario
	
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('codSNIP',$codSNIP);
		$html->assign_by_ref('desObjetivo',$desObjetivo);
		$html->assign_by_ref('desAltInv',$desAltInv);
		$html->assign_by_ref('numMonto',$numMonto);
		$html->assign_by_ref('desLocalidad',$desLocalidad);
		$html->assign_by_ref('desCPobla',$desCPobla);
		$html->assign_by_ref('indEnt',$indEnt);
		$html->assign_by_ref('txtNro_Beneficiarios',$txtNro_Beneficiarios);
		$html->assign_by_ref('AnnoPrimeraEtapa',$AnnoPrimeraEtapa);	
		$html->assign_by_ref('numMontoPrimEtapa',$numMontoPrimEtapa);	
		$html->assign_by_ref('AnnoSegundaEtapa',$AnnoSegundaEtapa);	
		$html->assign_by_ref('numMontoSegEtapa',$numMontoSegEtapa);
		$html->assign_by_ref('AnnoTerceraEtapa',$AnnoTerceraEtapa);		
		$html->assign_by_ref('numMontoTerEtapa',$numMontoTerEtapa);				
		$html->assign_by_ref('AnnoCuartaEtapa',$AnnoCuartaEtapa);		
		$html->assign_by_ref('numMontoCuartaEtapa',$numMontoCuartaEtapa);
		$html->assign_by_ref('numMontoPIM',$numMontoPIM);		
		$html->assign_by_ref('numMontoDevengado',$numMontoDevengado);				
				
		// Contenido Select Sector
		$sql_st = "SELECT id_pliego, lower(descripcion) ".
				  "FROM db_general.dbo.pliego ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $idSector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Nivel de Estudios
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.tipo_nivel_estudio ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstudio',$this->ObjFrmSelect($sql_st, $idEstudio, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Nivel de Estudio para Viabilidad
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.tipo_nivel_estudio ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstudioVia',$this->ObjFrmSelect($sql_st, $idEstudioVia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Contenido Select Situacion
		$sql_st = "SELECT id, Lower(descripcion) ".
				  "FROM dbo.situacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSituacion',$this->ObjFrmSelect($sql_st, $idSituacion, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));

		// Contenido Select Entidad Formuladora
		$sql_st = sprintf("SELECT id, Lower(descripcion) ".
						  "FROM dbo.entidad ".
						  "WHERE id_clase_entidad=%d ".
						  "ORDER BY 2",($indEnt) ? '3' : '2');
		$html->assign_by_ref('selEntForm',$this->ObjFrmSelect($sql_st, $idEntForm, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Contenido Select Entidad Ejecutora
		$sql_st = sprintf("SELECT id, Lower(descripcion) ".
						  "FROM dbo.entidad ".
						  "WHERE id_clase_entidad=%d ".
						  "ORDER BY 2",($indEnt) ? '0' : '1');
		$html->assign_by_ref('selEntEjec',$this->ObjFrmSelect($sql_st, $idEntEjec, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Fecha Presentacion
		$html->assign_by_ref('selMesPre',$this->ObjFrmMes(1, 12, true, substr($fecPresentacion,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaPre',$this->ObjFrmDia(1, 31, substr($fecPresentacion,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoPre',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecPresentacion,6,4), true, array('val'=>'none','label'=>'--------')));

		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIngreso,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIngreso,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecIngreso,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecSalida,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecSalida,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecSalida,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea el Valor para los ID's de Proyectos a Modificar
		$html->assign_by_ref('id',$id);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('ogdpe/proyectos/headerArm.tpl.php');
		$html->display('ogdpe/proyectos/frmHandleProyect.tpl.php');
		$html->display('ogdpe/proyectos/footerArm.tpl.php');
	}

	function ManipulaProyecto($id,$desNombre,$codSNIP,$desObjetivo,$desAltInv,$numMonto,$idSector,$codDepa,$codProv,$codDist,$desLocalidad,
							  $desCPobla,$idEstudio,$idEstudioVia,$idSituacion,$idEntForm,$idEntEjec,$indEnt,$fecPresentacion
							  , $fecIngreso,$fecSalida,$txtNroBeneficiarios,$AnnoPrimeraEtapa,$numMontoPrimEtapa,$AnnoSegundaEtapa, $numMontoSegEtapa, $AnnoTerceraEtapa,
							  $numMontoTerEtapa,$AnnoCuartaEtapa,$numMontoCuartaEtapa, $numMontoPIM, $numMontoDevengado
							  ){

		// Verifica el Valor del ID Proyecto
		$idProy = ($id) ? $id[0] : NULL;

	//Fue comentado para no ingresar fechas 24/07/2006	

		//Manipulacion de las Fechas
		$fecPresentacion = ($fecPresentacion!='//'&&$fecPresentacion&&!strstr($fecPresentacion,'none')) ? $fecPresentacion : NULL;
		$fecIngreso = ($fecIngreso!='//'&&$fecIngreso&&!strstr($fecIngreso,'none')) ? $fecIngreso : NULL;
		$fecSalida = ($fecSalida!='//'&&$fecSalida&&!strstr($fecSalida,'none')) ? $fecSalida : NULL;

		// Comprueba Valores	
		if(!$desNombre){ $bNom = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if($numMonto&&!is_numeric($numMonto)){ $bMon = true; $this->errors .= 'El Monto debe ser un valor num�rico<br>'; }
		if(!$idSector){ $bSec = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if(!$codDepa){ $bDep = true; $this->errors .= 'La Regi�n debe ser especificada<br>'; }
		if(!$idSituacion){ $bSit = true; $this->errors .= 'La Situaci�n debe ser especificada<br>'; }
		//if(!$idEntForm){ $bEFo = true; $this->errors .= 'La Unidad Formuladora debe ser especificada<br>'; }
		//if(!indEnt&&!$idEntForm){ $bEEj = true; $this->errors .= 'La Unidad Ejecutora debe ser especificada<br>'; }

	//Fue comentado para no ingresar fechas 24/07/2006	

		if($fecPresentacion){ $bFPre = ($this->ValidaFechaProyecto($fecPresentacion,'Presentacion')); }
		if($fecIngreso){ $bFIng = ($this->ValidaFechaProyecto($fecIngreso,'Ingreso')); }else{ $bFIng = true; $this->errors .= 'La Fecha de Ingreso debe ser especificada<br>'; };
		if($fecSalida){ $bFSal = ($this->ValidaFechaProyecto($fecSalida,'Salida')); }

		if($bNom||$bMon||$bSec||$bDep||$bSit||$bEFo||$bEEj||$bFPre||$bFSal){
			$objIntranet = new Intranet();
			$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			
			$this->FormManipulaProyecto($id,$desNombre,$codSNIP,$desObjetivo,$desAltInv,$numMonto,$idSector,$codDepa,$codProv,$codDist,$desLocalidad,
							  $desCPobla,$idEstudio,$idEstudioVia,$idSituacion,$idEntForm,$idEntEjec,$indEnt,$fecPresentacion,$fecIngreso,$fecSalida
							  ,true,$errors,$txtNroBeneficiarios,$AnnoPrimeraEtapa,$numMontoPrimEtapa,$AnnoSegundaEtapa, $numMontoSegEtapa, $AnnoTerceraEtapa,
							  $numMontoTerEtapa, $AnnoCuartaEtapa, $numMontoCuartaEtapa, $numMontoPIM, $numMontoDevengado);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE sp_%sProyecto_intra %s'%s',%s,%s,%s,%s,%s,'%s','%s','%s',%s,%s,%d,%d,%d,%d,%s,%s,%s,%s,'%s','%s',%d,'%s',%d,%s,%d,%s,%d,%s,%s,%s",
							  $idProy ? 'mod' : 'ins',
							  $idProy ? $this->PrepareParamSQL($idProy).',' : NULL,
							  $this->PrepareParamSQL($desNombre),
							  ($codSNIP) ? "'".$this->PrepareParamSQL($codSNIP)."'" : 'NULL',
							  ($desObjetivo) ? "'".$this->PrepareParamSQL($desObjetivo)."'" : 'NULL',
							  ($desAltInv) ? "'".$this->PrepareParamSQL($desAltInv)."'" : 'NULL',
							  ($numMonto) ? $this->PrepareParamSQL($numMonto) : 'NULL',
							  $this->PrepareParamSQL($idSector),
							  $this->PrepareParamSQL($codDepa),
							  $this->PrepareParamSQL($codProv),
							  $this->PrepareParamSQL($codDist),
							  ($desLocalidad) ? "'".$this->PrepareParamSQL($desLocalidad)."'" : 'NULL',
							  ($desCPobla) ? "'".$this->PrepareParamSQL($desCPobla)."'" : 'NULL',
							  $this->PrepareParamSQL($idEstudio),
							  $this->PrepareParamSQL($idEstudioVia), 
							  $this->PrepareParamSQL($idSituacion),
							  $this->PrepareParamSQL($idEntForm),
							  (!$indEnt) ? $this->PrepareParamSQL($idEntEjec) : 'NULL',
							  //Fue comentado para no ingresar fechas 24/07/2006	
							  ($fecPresentacion) ? "'".$this->PrepareParamSQL($fecPresentacion)."'" : "NULL",
							  ($fecIngreso) ? "'".$this->PrepareParamSQL($fecIngreso)."'" : "NULL",
							  ($fecSalida) ? "'".$this->PrepareParamSQL($fecSalida)."'" : "NULL",
							  $_SESSION['cod_usuario'],
							  $txtNroBeneficiarios,
							  $AnnoPrimeraEtapa,
							  $numMontoPrimEtapa,
							  $AnnoSegundaEtapa,
							  $numMontoSegEtapa,
							  $AnnoTerceraEtapa,
							  $numMontoTerEtapa,
							  $AnnoCuartaEtapa,
							  $numMontoCuartaEtapa,
							  $numMontoPIM, 
							  $numMontoDevengado
							  );
			//echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		
			
			if($RETVAL){
				$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
				$destination .= $this->arr_accion['MALA_TRANS'];
				$destination .= "&menu={$this->menu_items[2]['val']}";
				header("Location: $destination");
				exit;
			}else{
				if($idProy){
					// Elimina del Array el ID ya modificado
					$idProy = array_shift($id);
				
					if(count($id)>0){
						$objIntranet = new Intranet();
						$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
						$objIntranet->Body('proyectosinversion_tit.gif');
						
						$msg = '<b>Se ha modificado el Proyecto satisfactoriamente.<br>Continuando con el sgte. Proyecto a modificar.</b>';
						$this->muestraMensajeInfo($msg);
						$this->FormManipulaProyecto($id);
						
						$objIntranet->Footer();
						exit;
					}
				}

				$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
				$destination .= $this->arr_accion['BUENA_TRANS'];
				$destination .= "&menu=";
				$destination .= $idProy ? $this->menu_items[2]['val'] : $this->menu_items[1]['val'];

				header("Location: $destination");
				exit;
			}
		}
	}
	
	function EliminaProyecto($id){
		if(!$id){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
	
		$this->abreConnDB();
		
		for($i=0;$i<count($id);$i++){
			$sql_SP = sprintf("EXECUTE sp_delProyecto_intra %d",
							  $this->PrepareParamSQL($id[$i]));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				$RETVAL=1;
				break;
			}else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
				if($RETVAL) break;
			}
			unset($rs);	
		}

		$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
		$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
		$destination .= "&menu={$this->menu_items[0]['val']}";
		header("Location: $destination");
		exit;
	}

}
?>
