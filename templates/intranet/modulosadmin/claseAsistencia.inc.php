<?
error_reporting(E_ALL);
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class Asistencia extends Modulos{

	function Asistencia() {
		
				// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Obtiene los Datos del Modulo Actual
		//$this->ModuloInfo($_SESSION['modulo_id']);

		/*
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][14];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];

		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_VISITA' => 'frmSearchVisit',
							'BUSCA_VISITA' => 'searchVisit',
							'IMPRIME_VISITA' => 'printVisit',
							'CREA_CSV_VISITA' => 'CreaCsvVisit'							
							);
		
		$this->datosUsuarioMSSQL();
	}
	

	function MuestraIndex(){
		$this->FormBuscaVisita();
	}
	
	function FormBuscaVisita($tipo=NULL){
	
	$this->abreConnDB();
		//$this->conn->debug = true;
		//echo md5("10165999709412356");
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin,$ora_ini,$ora_fin,$min_ini,$min_fin,$tipo2,$tipo3;
		
		$horInicio = ($horInicio!=':'&&$horInicio) ? $horInicio : "08:00";
		$horFin = ($horFin!=':'&&$horFin) ? $horFin : "18:00";
		
		if(!$campo_fecha || $campo_fecha==""){
			$campo_fecha=date('d/m/Y');
		}
		if(!$campo_fecha2 || $campo_fecha2==""){
			$campo_fecha2=date('d/m/Y');
		}
		
		// Crea el Objeto HTML
		$html = & new Smarty;
		$frmName = 'frmBuscaVisita';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		$html->assign_by_ref('tipo',$tipo);
		$llaveInicial="{";
		$llaveFinal="}";
		$html->assign_by_ref('llaveInicial',$llaveInicial);
		$html->assign_by_ref('llaveFinal',$llaveFinal);
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		
		
		//echo "x".$this->userIntranet['CODIGO']."x";
		// Contenido Select Dependencia
		if($this->userIntranet['CODIGO']==239 || 		 				
		$this->userIntranet['CODIGO']==1843){
				$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				"FROM db_general.dbo.h_dependencia ".
				"WHERE condicion='ACTIVO' ".
				"AND CODIGO_DEPENDENCIA not in 	(49,63,90,104,106,107,108,109,110,111,112,113,114,136, ".
				"64,134,77,138,132,116,135,127,133,80,140,139,117,118, ".
				"141,142,143,144,145,146,147,148,149,150,151,152,153, ".
				"154,155,156,157,158,159,164,165,162,163,164,165,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183) ".
				"ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Todas')));
		}else{
					$soloNombre=1;
					$html->assign_by_ref('soloNombre',$soloNombre);
					$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
								"FROM db_general.dbo.h_dependencia ".
								"WHERE condicion='ACTIVO' ".
								"AND CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP']." ".
								" ORDER BY 2";
					$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true));
					$tipo2=$this->userIntranet['COD_DEP'];
		}
		unset($sql_st);
		$html->assign_by_ref('tipo2',$tipo2);
		// Contenido Select Usuario
		
		if(
		$this->userIntranet['CODIGO']==239 || 		 
		$this->userIntranet['CODIGO']==1843){
		
				if($tipo2==25)
				{
					$str_cod_dep=" (25,53,54)";
					$sql_st = "SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
					"FROM db_general.dbo.h_trabajador ".
					"WHERE coddep in ".$str_cod_dep." and estado='ACTIVO' and id_regimen in (1,2,3) ".
					//"AND CODIGO_TRABAJADOR=%d".
					"ORDER BY 2";							

				}else{
					$str_cod_dep=" (%d) ";
					$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
					"FROM db_general.dbo.h_trabajador ".
					"WHERE coddep in ".$str_cod_dep." and estado='ACTIVO' and id_regimen in (1,2,3) ".
					//"AND CODIGO_TRABAJADOR=%d".
					"ORDER BY 2",$tipo2);							

				}
		

						  
		//$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'No especificado')));
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		
		}elseif($this->userIntranet['DIRE']==1 || $this->userIntranet['DIRE']==5){
		$soloNombre=3;
		$html->assign_by_ref('soloNombre',$soloNombre);
		
							
				if($this->userIntranet['COD_DEP']==25)
				{
					$str_cod_dep=" (25,53,54)";

							$sql_st = "SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep in ".$str_cod_dep." and estado='ACTIVO' and id_regimen in (1,2,3)  ".
						  //"AND CODIGO_TRABAJADOR=%d".
						  "ORDER BY 2";					
					
				}else{		
							$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep in (%d) and estado='ACTIVO' and id_regimen in (1,2,3)  ".
						  //"AND CODIGO_TRABAJADOR=%d".
						  "ORDER BY 2",$this->userIntranet['COD_DEP']);
				}
		//$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'No especificado')));
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		}else{
		
		
		
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep in (%d) and estado='ACTIVO'   ".
						  "AND CODIGO_TRABAJADOR=%d".
						  "ORDER BY 2",$this->userIntranet['COD_DEP'],$this->userIntranet['CODIGO']);
						  
		//$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'No especificado')));
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true));
		$tipo3=$this->userIntranet['CODIGO'];
		}

		$html->assign_by_ref('tipo3',$tipo3);


		unset($sql_st);				
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('oti/asistencia/frmSearch.tpl.php');		
	}
	
	function BuscaVisita($tipo,$mensaje){
		//echo "xxx";
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin,$ora_ini,$ora_fin,$min_ini,$min_fin,$tipo2,$tipo3;
		global $print;
		
		$print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
		
		if($campo_fecha==""){
			$campo_fecha=date('d/m/Y');
		}
		if($campo_fecha2==""){
			$campo_fecha2=date('d/m/Y');
		}
		if ($campo_fecha&&$campo_fecha2&& $campo_fecha!=""&& $campo_fecha2!=""){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los a�os no es correcta.</b></font>';}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los meses no es correcto.</b></font>';
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los d�as no es correcta.</b></font>';}
					}
			}
		}		

		}


		$html = & new Smarty;
		
		$horInicio=$ora_ini.":".$min_ini;
		$horFin=$ora_fin.":".$min_fin;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		//,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		if($print!=1)
		$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		//echo $where;
		
		if($tipo3>0){
			$sql_SP =  "SELECT EMAIL, DNI FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODIGO_TRABAJADOR=".$tipo3;
			//$sql_SP =  "SELECT EMAIL FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODIGO_TRABAJADOR=".$tipo3;
			//echo $sql_SP;
			//exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$MAILtRABAJADOR=$rs->fields[0];
				$dniTrabajador=$rs->fields[1];
			}
		}
		
			$sql_SP =  "SELECT convert(datetime,'$campo_fecha',103)";
			//echo $sql_SP;
			//exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$fecha1fecha1=$rs->fields[0];
			}
		
			$sql_SP =  "SELECT convert(datetime,'$campo_fecha2',103)";
			//echo $sql_SP;
			//exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$fecha2fecha2=$rs->fields[0];
			}
		
		// Obtiene los datos de Cada Modulo
		//$sql_SP =  sprintf("EXEC db_general.dbo.LISTA_MARCACIONES_CAS '%s','%s','%s'",$_SESSION['cod_usuario'],$campo_fecha,$campo_fecha2);
		$sql_SP =  sprintf("EXEC db_general.dbo.LISTA_MARCACIONES_CAS '%s','%s','%s'",$MAILtRABAJADOR,$fecha1fecha1,$fecha2fecha2);
		//echo $sql_SP;
		//exit;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()){
				$html->append('visi',array('ejercicio' => ucwords($row[0]),
										   'mes' => $row[1],
										   'fecha' => $row[2],
										   //'nombre' => $row[3],
										   'hora_ini' => ucwords($row[4]),
										   'hora_fin' => ucwords($row[5]),
										   //'hora_total' => $row[6],
										   'hora_fijada' => ucwords($row[6]),
										   //'hora_total_fijada' => $row[8],
										   //'nro_semana' => $row[9],	
										   //'horas' => $row[10],	
										   'dia' => ucwords(strtolower($row[8])),
										   //'nroDias'=>$row[8],
										   'total_horas_diarias'=>$row[10],
										   'total_horas_semanales'=>$row[11],
										    'obs'=>$row[12],		
										   ));
				$nombreX= $row[3];
				$semanaX= $row[9];
				$horasX= $row[10];
				$horaFijada=$row[6];
				}
			$rs->Close();
		}
		unset($rs);
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('nombreX',$nombreX);
		$html->assign_by_ref('semanaX',$semanaX);
		$html->assign_by_ref('horasX',$horasX);
		$html->assign_by_ref('MAILtRABAJADOR',$MAILtRABAJADOR);
		$html->assign_by_ref('dniTrabajador',$dniTrabajador);
		//echo $dniTrabajador;
		
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('horaFijada',$horaFijada);
		
		$html->assign_by_ref('tipo',$tipo);
		
		$html->display('oti/asistencia/searchResult.tpl.php');
	}

	function ImprimeVisita($tipo,$campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin,$print){
		global $MAILtRABAJADOR;
		//global $tipo2,$print;
		//$print = ($_POST['print']) ? $_POST['print'] : $_GET['print'];
		if ($campo_fecha&&$campo_fecha2){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo "error en los meses";
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo "error en los dias";}
					}
			}
		}		

		}


		$html = & new Smarty;
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		
		//$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML

		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		
					$sql_SP =  "SELECT convert(datetime,'$campo_fecha',103)";
			//echo $sql_SP;
			//exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$fecha1fecha1=$rs->fields[0];
			}
		
			$sql_SP =  "SELECT convert(datetime,'$campo_fecha2',103)";
			//echo $sql_SP;
			//exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$fecha2fecha2=$rs->fields[0];
			}


		//echo $where;
		
		// Obtiene los datos de Cada Modulo
		$sql_SP =  sprintf("EXEC db_general.dbo.LISTA_MARCACIONES_CAS '%s','%s','%s'",$MAILtRABAJADOR,$fecha1fecha1,$fecha2fecha2);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$horaSuma=0;
			while($row = $rs->FetchRow()){
				unset($nombreDia);
				unset($nroSemana);
				$nroSemana=$row[9];
				if($row[12]==1){
					$nombreDia="Domingo";
					$nroSemana=$nroSemana-1;
				}elseif($row[12]==2){
					$nombreDia="Lunes";
				}elseif($row[12]==3){
					$nombreDia="Martes";
				}elseif($row[12]==4){
					$nombreDia="Mi�rcoles";
				}elseif($row[12]==5){
					$nombreDia="Jueves";
				}elseif($row[12]==6){
					$nombreDia="Viernes";
				}elseif($row[12]==7){
					$nombreDia="S�bado";
				}
				if($row[12]==2){
				$horaSuma=$row[8];
				}else{
				$horaSuma=$horaSuma+$row[8];
				}
				$html->append('visi',array('ejercicio' => ucwords($row[0]),
										   'mes' => $row[1],
										   'fecha' => $row[2],
										   //'nombre' => $row[3],
										   'hora_ini' => ucwords($row[4]),
										   'hora_fin' => ucwords($row[5]),
										   //'hora_total' => $row[6],
										   'hora_fijada' => ucwords($row[6]),
										   //'hora_total_fijada' => $row[8],
										   //'nro_semana' => $row[9],	
										   //'horas' => $row[10],	
										   'dia' => ucwords(strtolower($row[8])),
										   //'nroDias'=>$row[8],
										   'total_horas_diarias'=>$row[10],
										   'total_horas_semanales'=>$row[11],
										    'obs'=>$row[12],		
										   ));
				$nombreX= $row[3];
				$semanaX= $row[9];
				$horasX= $row[10];
				$horaFijada=$row[6];
				}
			$rs->Close();
		}
		unset($rs);
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('nombreX',$nombreX);
		$html->assign_by_ref('semanaX',$semanaX);
		$html->assign_by_ref('horasX',$horasX);
		
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('horaFijada',$horaFijada);
		
		$html->assign_by_ref('tipo',$tipo);
		
		/*if($print==3){
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=consulta-".mktime().".csv");
			
			$html->display('oti/asistencia/CreaCSVVisitantes.tpl.php');
			exit;
		}else{
		*/
		$html->display('oti/asistencia/printVisit.tpl.php');
		/*}*/
	}
	
	
	function CreaCsvVisitas($id){
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horInicioP,$horFin,$horFinP,$ora_ini,$ora_fin,$min_ini,$min_fin;
		global $print;
		
		$print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
		if ($campo_fecha&&$campo_fecha2){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los a�os no es correcta.</b></font>';}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los meses no es correcta.</b></font>';
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los d�as no es correcta.</b></font>';}
					}
			}
		}		

		}


		$html = & new Smarty;
		
		$horInicio=$ora_ini.":".$min_ini;
		$horFin=$ora_fin.":".$min_fin;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		//,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horInicioP',$horInicioP);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('horFinP',$horFinP);		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
				
		if($print!=1)
		$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		$sql_SP =  sprintf("EXEC sp_busVisitaFechas '%s','%s'",$this->PrepareParamSQL($tipo),$this->PrepareParamSQL($where));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow())
				$html->append('visi',array('ejercicio' => ucwords($row[0]),
										   'mes' => $row[1],
										   'fecha' => $row[2],
										   'hora_ini' => ucwords($row[3]),
										   'hora_fin' => ucwords($row[4]),
										   'hora_total' => ucwords($row[5]),
										   'tipo' => $row[6],
										   'nomb' => $row[7],
										   ));
			$rs->Close();
		}
		unset($rs);
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('tipo',$tipo);
		
		$html->display('oti/asistencia/searchResult.tpl.php');
	}
}
?>