<?
require('objetosForm.php');
require('objetosJavaScript.php');

/* ****************************************************************** */
/* 					FUNCIONES DE LOS MODULOS						  */
/* ****************************************************************** */


/* ---------------------------------------------------- */
/* Funcion para mostrar el Formulario HTML	  			*/
/* para Insertar o Modificar una Exposicion   			*/
/* ---------------------------------------------------- */
/* MODULO EXPOSICION				  		  			*/
/* ---------------------------------------------------- */

function insertaExposicionHTML($id_exposicion=NULL){
	$frmName = "frmExpo";
	if(!is_null($id_exposicion)){
		global $type_db, $userdb, $passdb, $db;
	
		$conn = & ADONewConnection($type_db['postgres']);
		$conn->debug = false;
		if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
			print $conn->ErrorMsg();
		else{
			$sql_st="SELECT tit_exposicion, des_exposicion, des_autor, e.id_subdependencia, date_part('day', fec_exposicion), ".
					"date_part('month', fec_exposicion), date_part('year', fec_exposicion), num_diapositivas, ".
					"des_diapositivas, fln_exposicion, e.ind_activo, to_char(e.fec_creacion,'DD/MM/YYYY HH24:MI:SS'), ".
					"to_char(e.fec_modificacion,'DD/MM/YYYY HH24:MI:SS'), e.usr_audit, des_lugar, id_dependencia ".
					"FROM exposicion e, subdependencia sd ".
					"WHERE id_exposicion=$id_exposicion and e.id_subdependencia=sd.id_subdependencia";
			$rs = & $conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $conn->ErrorMsg();
			}else{
				$row = $rs->FetchRow();
				$rs->Close();
			}
			$conn->Close();
		}
	}
	insertaFechaScript();
?>
<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
  <table width="540" border="0" cellspacing="0" cellpadding="2">
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="5" class="td-lb-login" width="540"><b>DE LA EXPOSICI&Oacute;N</b></td>
    </tr>
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>T&iacute;tulo</b></td>
      <td width="130"> 
        <input type="text" name="tit_exposicion" class="ip-login contenido" size="32" value="<?=$row[0];?>">
      </td>
      <td width="5">&nbsp;</td>
      <td class="texto" width="115">Fecha Realizaci&oacute;n</td>
      <td width="120"> 
        <?
		$row[4]= ($row[4]!=NULL) ? $row[4] : 'NULL';
		$row[5]= ($row[5]!=NULL) ? $row[5] : 'NULL';
		$row[6]= ($row[6]!=NULL) ? $row[6] : 'NULL';
		insertaFechaForm('exp_dia','exp_mes','exp_anyo',$frmName,1990,date('Y'),$row[4],$row[5],$row[6]);?>
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170">Descripci&oacute;n</td>
      <td width="130"> 
        <textarea name="des_exposicion" class="ip-login contenido" cols="30"><?=$row[1];?></textarea>
      </td>
      <td width="5">&nbsp;</td>
      <td class="texto" width="115">Lugar</td>
      <td width="120"> 
        <input type="text" name="des_lugar" class="ip-login contenido" size="20" value="<?=$row[14];?>">
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>Publicaci&oacute;n Activa</b></td>
      <td width="130"> 
        <?insertaIndActivoFrm($row[10]);?>
      </td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="5" class="td-lb-login" width="540"><b>DEL PROPIETARIO</b></td>
    </tr>
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170">Autor</td>
      <td width="130"> 
        <input type="text" name="des_autor" class="ip-login contenido" size="32" value="<?=$row[2];?>">
      </td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>Dependencia</b></td>
      <td colspan="4"> 
        <?insertaDependenciaFrm($row[15]);?>
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>Sub Dependencia</b></td>
      <td colspan="4"> 
        <select name="id_subdependencia" class="ip-login contenido">
        </select>
        <?insertaSubdependenciaScript("document.${frmName}.id_subdependencia","document.${frmName}.id_dependencia",$row[3]);?>
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="5" class="td-lb-login" width="540"><b>DEL CONTENIDO</b></td>
    </tr>
    <tr> 
      <td colspan="5" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>N&uacute;mero de Diapositivas</b></td>
      <td width="130"> 
        <input type="text" name="num_diapositivas" class="ip-login contenido" size="4" value="<?=$row[7];?>">
      </td>
      <td width="5">&nbsp;</td>
      <td class="texto" width="115"><b>Directorio Extensi&oacute;n</b></td>
      <td width="120"> 
        <input type="text" name="des_diapositivas" class="ip-login contenido" size="20" value="<?=$row[8];?>">
      </td>
    </tr>
    <tr> 
      <td width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>Archivo Download</b></td>
      <td width="130"> 
        <input type="text" name="fln_exposicion" class="ip-login contenido" size="32" value="<?=$row[9];?>">
      </td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
<?
	if(!is_null($id_exposicion)){
?>
    <tr> 
      <td class="texto" colspan="5">
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td class="td-lb-login" colspan="5"><b>DE LA MODIFICACI&Oacute;N</b></td>
    </tr>
    <tr> 
      <td class="texto" colspan="5">
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td class="texto" width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="170"><b>Usuario</b></td>
      <td width="130" class="texto"><?=$row[13];?></td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr>
      <td class="texto" width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
    <tr>
      <td class="texto" width="170"><b>Fecha Creaci&oacute;n</b></td>
      <td width="130" class="texto"><?=$row[11];?></td>
      <td width="5">&nbsp;</td>
      <td width="115" class="texto"><b>Fecha Modificaci&oacute;n</b></td>
      <td width="120" class="texto"><?=$row[12];?></td>
    </tr>
    <tr>
      <td class="texto" width="170">
        <input type="hidden" name="id_exposicion" value="<?=$id_exposicion;?>">
      </td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
<?
	}
?>
    <tr> 
      <td class="texto" colspan="5" align="center"> 
        <input type="submit" value="INGRESAR DATOS" class="but-login" onClick="MM_validateForm('tit_exposicion','T�tulo','R','num_diapositivas','N�mero de Diapositivas','RisNum','des_diapositivas','Directorio/Extensi�n','R','fln_exposicion','Archivo Download','R');return document.MM_returnValue">
        &nbsp;&nbsp;&nbsp; 
        <input type="reset" value="BORRAR DATOS" class="but-login">
      </td>
    </tr>
    <tr> 
      <td class="texto" width="170">&nbsp;</td>
      <td width="130">&nbsp;</td>
      <td width="5">&nbsp;</td>
      <td width="115">&nbsp;</td>
      <td width="120">&nbsp;</td>
    </tr>
  </table>
</form>
<?
}

/* ------------------------------------------------------------- */
/* Funcion para Insertar una Exposicion a la Base de Datos		 */
/* ------------------------------------------------------------- */
/* MODULO EXPOSICIONES											 */
/* ------------------------------------------------------------- */

function insertaExposicionDB($tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $fec_exposicion, $num_diapositivas, 
							 $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
		print $conn->ErrorMsg();
	else{
		$ins_st = "INSERT INTO exposicion(tit_exposicion, ";
		$ins_st .= (!is_null($des_exposicion)) ? "des_exposicion, " : "";
		$ins_st .= (!is_null($des_autor))? "des_autor, " : "";
		$ins_st .= "id_subdependencia, ";
		$ins_st .= (!is_null($fec_exposicion))? "fec_exposicion, " : "";
		$ins_st .= "num_diapositivas, des_diapositivas, fln_exposicion, ind_activo, usr_audit";
		$ins_st .= (!is_null($des_lugar))? ", des_lugar" : "";
		$ins_st .= ") ";
				   		
		$ins_st .= "VALUES('${tit_exposicion}', ";
		$ins_st .= (!is_null($des_exposicion)) ? "'${des_exposicion}', " : "";
		$ins_st .= (!is_null($des_autor))? "'${des_autor}', " : "";
		$ins_st .= "$id_subdependencia, ";
		$ins_st .= (!is_null($fec_exposicion))? "to_date('${fec_exposicion}','DD/MM/YYYY'), " : "";
		$ins_st .= "${num_diapositivas}, '${des_diapositivas}', '${fln_exposicion}', '$ind_activo', '{$_SESSION['cod_usuario']}'";
		$ins_st .= (!is_null($des_lugar))? ", '${des_lugar}'" : "";
		$ins_st .= ")";   

		if ($conn->Execute($ins_st) === false) {
			print 'error insertando: '.$conn->ErrorMsg().'<BR>';
		}else{
			muestraTablaStatusDB("La Exposici&oacute;n se ha insertado correctamente.","Modulo Exposiciones","Ingresar otra Exposici&oacute;n");
		}
		unset($ins_st); 
		$conn->Close();
	}
}

/* ------------------------------------------------------------- */
/* Funcion para Modificar una Exposicion en la Base de Datos	 */
/* ------------------------------------------------------------- */
/* MODULO EXPOSICIONES											 */
/* ------------------------------------------------------------- */

function modificaExposicionDB($id_exposicion,$tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $fec_exposicion, $num_diapositivas, 
							 $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
		print $conn->ErrorMsg();
	else{
		$upd_st = "UPDATE exposicion set tit_exposicion='${tit_exposicion}', ".
		
		$upd_st .= (!is_null($des_exposicion)) ? "des_exposicion='${des_exposicion}', " : "des_exposicion=NULL, ";
		
		$upd_st .= (!is_null($des_autor))? "des_autor='${des_autor}', " : "des_autor=NULL, ";
		
		$upd_st .= "id_subdependencia=$id_subdependencia, ";

		$upd_st .= (!is_null($fec_exposicion))? "fec_exposicion=to_date('${fec_exposicion}','DD/MM/YYYY'), " : "fec_exposicion=NULL, ";

		$upd_st .= "num_diapositivas=${num_diapositivas}, des_diapositivas='${des_diapositivas}', fln_exposicion='${fln_exposicion}', ".
				   "ind_activo='$ind_activo', fec_modificacion=now(), usr_audit='{$_SESSION['cod_usuario']}'";
		
		$upd_st .= (!is_null($des_lugar))? ", des_lugar='${des_lugar}'" : ", des_lugar=NULL";
		
		$upd_st .= " WHERE id_exposicion=${id_exposicion}";
		
		if ($conn->Execute($upd_st) === false) {
			print 'error modificando: '.$conn->ErrorMsg().'<BR>';
		}else{
			muestraTablaStatusDB("La Exposici&oacute;n se ha modificado correctamente","Modulo Exposiciones","Ingresar otra Exposici&oacute;n");
		}
		unset($upd_st); 

		$conn->Close();
	}
}

/* -------------------------------------------------------------- */
/* Funcion para Limpiar Datos que van desde el Formulario HTML	  */
/* hacia las Funciones Insertar y Modificar Exposicion 			  */
/* -------------------------------------------------------------- */
/* MODULO EXPOSICIONES											  */
/* -------------------------------------------------------------- */

function enviaDatosExposicion($id_exposicion=NULL){
	$_POST['des_exposicion'] = (empty($_POST['des_exposicion'])) ? NULL : trim($_POST['des_exposicion']);
	$_POST['des_autor'] = (empty($_POST['des_autor'])) ? NULL : trim($_POST['des_autor']);
	$_POST['fec_exposicion'] = ($_POST['exp_dia']=='NULL'||$_POST['exp_mes']=='NULL'||$_POST['exp_anyo']=='NULL') ? NULL : "{$_POST['exp_dia']}/{$_POST['exp_mes']}/{$_POST['exp_anyo']}";
	$_POST['des_lugar'] = (empty($_POST['des_lugar'])) ? NULL : trim($_POST['des_lugar']);
	if(is_null($id_exposicion)){
		insertaExposicionDB($_POST['tit_exposicion'], $_POST['des_exposicion'], $_POST['des_autor'], $_POST['id_subdependencia'],
							$_POST['fec_exposicion'], $_POST['num_diapositivas'], $_POST['des_diapositivas'], $_POST['fln_exposicion'],
							$_POST['ind_activo'], $_POST['des_lugar']);
	}else{
		modificaExposicionDB($id_exposicion,$_POST['tit_exposicion'], $_POST['des_exposicion'], $_POST['des_autor'], $_POST['id_subdependencia'],
							$_POST['fec_exposicion'], $_POST['num_diapositivas'], $_POST['des_diapositivas'], $_POST['fln_exposicion'],
							$_POST['ind_activo'], $_POST['des_lugar']);
	}
}

/* ------------------------------------------------------------- */
/* Funcion para Listar todos los Grupos Existentes (Index)		 */
/* ------------------------------------------------------------- */
/* MODULO GRUPOS												 */
/* ------------------------------------------------------------- */

function muestraGrupoIndexHTML(){
?>
<form name="form1" method="post" action="">
<table width="540" border="0" cellspacing="0" cellpadding="2">
  <tr> 
    <td> 
      <hr width="100%" size="1">
    </td>
  </tr>
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="td-lb-login" width="360"><b>BUSQUEDA DEL GRUPO</b></td>
            <td align="right" width="100"> 
              <input type="text" name="cod_grupo" class="ip-login contenido">
            </td>
            <td align="center" width="80"> 
              <input type="submit" value="Buscar" class="but-login">
            </td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td> 
      <hr width="100%" size="1">
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td rowspan="3" valign="top"><img src="file:///C|/Web/intranet/img/800x600/ico-grupo.gif" width="17" height="16"></td>
            <td colspan="2" width="523" class="item"><b>Identificador</b></td>
          </tr>
          <tr> 
            <td class="sub-item" width="123" valign="top">Modulos con Derecho:</td>
            <td width="400">&nbsp;</td>
          </tr>
          <tr> 
            <td class="sub-item">Estado:</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<?
}

/* --------------------------------------------------------------- */
/* Funcion para mostrar el Formulario HTML para Ins o Mod un Grupo */
/* --------------------------------------------------------------- */
/* MODULO GRUPOS												   */
/* --------------------------------------------------------------- */

function insertaGrupoHTML($id=NULL,$datos=NULL,$id_dependencia=NULL,$id_subdependencia=NULL,$cod_usuario=NULL){
	//Nombre del Formulario Principal
	$frmName = "frmGrupo";
	insertaScriptSubmitForm($frmName,"seleccionaItemSelectForm(2,document.forms['${frmName}'].elements['cod_usuario[]'])\r\ndocument.${frmName}.ind_accion.value=0\r\n");

	// Seleccion de Datos si la Accion es MODIFICAR
	if(!is_null($id)){
		global $type_db, $userdb, $passdb, $db;
		
		$conn = & ADONewConnection($type_db['postgres']);
		$conn->debug = true;
		if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
			print $conn->ErrorMsg();
		else{
			print_r($conn);
			$sql_st="SELECT cod_grupo, des_grupo, ind_activo, to_char(fec_creacion,'DD/MM/YYYY HH24:MI:SS'), ".
					"to_char(fec_modificacion,'DD/MM/YYYY HH24:MI:SS'), usr_audit ".
					"FROM grupo ".
					"WHERE id_grupo=$id";
			$rs = & $conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $conn->ErrorMsg();
			}else{
				$row = $rs->FetchRow();
				//$rs->Close();
			}
			//$conn->Close();
		}
	}
?>
<form name="<?=$frmName;?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
  <table width="540" border="0" cellspacing="0" cellpadding="2">
    <tr> 
      <td colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="2" class="td-lb-login" width="540"><b>DEL GRUPO</b></td>
    </tr>
    <tr> 
      <td colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="430">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto"><b>Identificador</b></td>
      <td> 
        <input type="text" name="cod_grupo" class="ip-login contenido" size="42" value="<?echo (empty($datos[0]))? $row[0] : $datos[0];?>" maxlength="25">
      </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="110">Descripci&oacute;n</td>
      <td width="430"> 
        <textarea name="des_grupo" class="ip-login contenido" cols="40" rows="4"><?echo (empty($datos[1]))? $row[1] : $datos[1];?></textarea>
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="110"><b>Activo</b></td>
      <td width="390"> 
        <?insertaIndActivoFrm((empty($datos[2]))? $row[2] : $datos[2]);?>
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="2" class="td-lb-login" width="540"><b>DE LOS INTEGRANTES</b></td>
    </tr>
    <tr> 
      <td colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="110"><b>Dependencia</b></td>
      <td width="390"> 
        <?insertaDependenciaFrm($id_dependencia,"id_dependencia",true,true);?>
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="110"><b>Sub Dependencia</b></td>
      <td width="390"> 
        <? insertaSubdependenciaFrm("id_subdependencia",true);
		   insertaSubdependenciaScript("document.${frmName}.id_subdependencia","document.${frmName}.id_dependencia",$id_subdependencia);?>
      </td>
    </tr>
    <tr> 
      <td width="110">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" width="540">
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
			<? $usuarios = insertaUsuariosFrm($id_subdependencia); ?>
            <td width="250" align="center" class="texto">Usuarios ( <? echo $usuarios[0]; ?> )</td>
            <td width="40" align="center">&nbsp;</td>
            <td width="250" align="center" class="texto"><b>Integrantes del Grupo</b></td>
          </tr>
          <tr> 
            <td width="250" align="center"> 
              <select id="cod_usuario_muestra" name="cod_usuario_muestra[]" size="10" multiple class="ip-login contenido">
                <option>-------------------------------------</option>
				<? echo $usuarios[1]; ?>
              </select>
            </td>
            <td width="40" align="center"> 
  			  <? insertaScriptItemsSelectForm("document.forms['${frmName}'].elements['cod_usuario_muestra[]']", "document.forms['${frmName}'].elements['cod_usuario[]']"); ?>
            </td>
            <td width="250" align="center"> 
              <select name="cod_usuario[]" size="10" multiple class="ip-login contenido">
                <option>-------------------------------------</option>
<?
	if(count($cod_usuario)>0){
		consultaUsuariosFrm($cod_usuario,NULL,&$conn);
	}else{
		if(!is_null($id))
			consultaUsuariosFrm(NULL,$id,&$conn);
	}
?>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td width="150">
        <input type="hidden" name="ind_accion" value="0">
      </td>
      <td width="390">&nbsp;</td>
    </tr>
<?
	if(!is_null($id)){
?>
    <tr> 
      <td class="texto" colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="2" width="540" class="td-lb-login"><b>DE LA MODIFICACI&Oacute;N</b></td>
    </tr>
    <tr> 
      <td class="texto" colspan="2" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td class="texto" colspan="2" width="540"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td class="texto" width="110">&nbsp;</td>
            <td width="130">&nbsp;</td>
            <td width="20">&nbsp;</td>
            <td width="125">&nbsp;</td>
            <td width="150">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto" width="110"><b>Usuario</b></td>
            <td width="130" class="texto"> 
              <?=$row[5];?>
            </td>
            <td width="20">&nbsp;</td>
            <td width="125">&nbsp;</td>
            <td width="150">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto" width="110">&nbsp;</td>
            <td width="130">&nbsp;</td>
            <td width="20">&nbsp;</td>
            <td width="125">&nbsp;</td>
            <td width="150">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto" width="110"><b>Fecha Creaci&oacute;n</b></td>
            <td width="130" class="texto"> 
              <?=$row[3];?>
            </td>
            <td width="20">&nbsp;</td>
            <td width="125" class="texto"><b>Fecha Modificaci&oacute;n</b></td>
            <td width="150" class="texto"> 
              <?=$row[4];?>
            </td>
          </tr>
          <tr> 
            <td class="texto" width="110"> 
              <input type="hidden" name="id" value="<?=$id;?>">
            </td>
            <td width="130">&nbsp;</td>
            <td width="20">&nbsp;</td>
            <td width="125">&nbsp;</td>
            <td width="150">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <?
	}
?>
    <tr> 
      <td class="texto" colspan="2" align="center"> 
        <input type="submit" value="<? echo (empty($id)) ? "INGRESAR DATOS" : "MODIFICAR DATOS"; ?>" class="but-login" onClick="document.<?=$frmName;?>.ind_accion.value=1;seleccionaItemSelectForm(2,document.forms['<?=$frmName;?>'].elements['cod_usuario[]']);MM_validateForm('cod_grupo','Identificador','R');return document.MM_returnValue">
        &nbsp;&nbsp;&nbsp; 
        <input type="reset" value="BORRAR DATOS" class="but-login">
      </td>
    </tr>
    <tr> 
      <td class="texto" width="150">&nbsp;</td>
      <td width="390">&nbsp;</td>
    </tr>
  </table>
</form>
<?
}

/* -------------------------------------------------------------- */
/* Funcion para Limpiar Datos que van desde el Formulario HTML	  */
/* hacia las Funciones Insertar y Modificar Grupo	 			  */
/* -------------------------------------------------------------- */
/* MODULO GRUPOS												  */
/* -------------------------------------------------------------- */

function enviaDatosGrupo($cod_grupo, $des_grupo, $ind_activo, $usuarios, $id=NULL){
	$des_grupo = (empty($_POST['des_grupo'])) ? NULL : trim($_POST['des_grupo']);
	if(count($usuarios)>0){
		$usuarios = array_unique($usuarios);
		$str_usuarios = implode(',',$usuarios);
		$usuarios = explode(',',$str_usuarios);
		unset($str_usuarios);
	}

	if(is_null($id)){
		insertaGrupoDB($cod_grupo, $des_grupo, $ind_activo, $usuarios);
	}else{
		modificaGrupoDB($id, $cod_grupo, $des_grupo, $ind_activo, $usuarios);
	}
}

/* ------------------------------------------------------------- */
/* Funcion para insertar un Grupo a la Base de Datos			 */
/* ------------------------------------------------------------- */
/* MODULO GRUPOS												 */
/* ------------------------------------------------------------- */

function insertaGrupoDB($cod_grupo, $des_grupo, $ind_activo, $usuarios){
	global $type_db, $userdb, $passdb, $db;

	$ins_st = "INSERT INTO grupo(cod_grupo, ";
	$ins_st .= (!is_null($des_grupo)) ? "des_grupo, " : "";
	$ins_st .= "ind_activo, usr_audit) ";
					
	$ins_st .= "VALUES('${cod_grupo}', ";
	$ins_st .= (!is_null($des_grupo)) ? "'${des_grupo}', " : "";
	$ins_st .= "'${ind_activo}', '{$_SESSION['cod_usuario']}')";
	
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
		print $conn->ErrorMsg();
	else{
		if ($conn->Execute($ins_st) === false) {
			print 'error insertando: '.$conn->ErrorMsg().'<BR>';
		}else{
			if(count($usuarios)>0){
				// Selecciona el ID del Grupo Insertado
				$sql_st = "SELECT id_grupo FROM grupo WHERE cod_grupo='${cod_grupo}'"; 
				$rs = & $conn->Execute($sql_st);
				unset($sql_st);
				if (!$rs){
					print $conn->ErrorMsg();
				}else{
					if($row = $rs->FetchRow())
						$id_grupo=$row[0];
					$rs->Close();
				}
				// Agrega los usuarios del Grupo

				for($i=0; $i<count($usuarios); $i++){
					$ins_usr_st = "INSERT INTO grupo_usuario(cod_usuario, id_grupo) values('{$usuarios[$i]}',${id_grupo})";
					echo $ins_usr_st;
					if ($conn->Execute($ins_usr_st) === false) {
						print 'error insertando: '.$conn->ErrorMsg().'<BR>';
					}
					unset($ins_usr_st);
				}
			}
?>
<table width="531" border="0" cellspacing="0" cellpadding="3">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td class="aviso" align="center"><b>El Grupo se ha insertado correctamente</b></td>
  </tr>
  <tr>
    <td align="center"> 
      <input type="button" value="Ingresar otro Grupo" class="but-login" onClick="MM_goToURL('parent','<?=$_SERVER['PHP_SELF'];?>');return document.MM_returnValue">
      &nbsp;&nbsp;&nbsp;
      <input type="button" value="Asignar derechos al Grupo" class="but-login" onClick="MM_goToURL('parent','../grupoderechos/index.php?idgrp=<?=$id_grupo?>');return document.MM_returnValue">
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<?
		}
		unset($ins_st); 
		$conn->Close();
	}
}

/* ------------------------------------------------------------- */
/* Funcion para Modificar un Grupo en la Base de Datos			 */
/* ------------------------------------------------------------- */
/* MODULO GRUPOS												 */
/* ------------------------------------------------------------- */

function modificaGrupoDB($id_grupo, $cod_grupo, $des_grupo, $ind_activo, $usuarios){
	global $type_db, $userdb, $passdb, $db;

	// Arma la sentencia UPDATE
	$upd_st = "UPDATE grupo set cod_grupo='${cod_grupo}', ".
	$upd_st .= (!is_null($des_grupo)) ? "des_grupo='${des_grupo}', " : "des_grupo=NULL, ";
	$upd_st .= "ind_activo='$ind_activo', fec_modificacion=now(), usr_audit='{$_SESSION['cod_usuario']}' ";
	$upd_st .= "WHERE id_grupo=${id_grupo}";

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
		print $conn->ErrorMsg();
	else{
		if ($conn->Execute($upd_st) === false) {
			print 'error modificando: '.$conn->ErrorMsg().'<BR>';
		}else{
			if(count($usuarios)>0){
				// Borra la estructura de usuarios del Grupo
				$del_st = "DELETE FROM grupo_usuario WHERE id_grupo='${id_grupo}'"; 
				if ($conn->Execute($del_st) === false) {
					print 'error borrando: '.$conn->ErrorMsg().'<BR>';
				}
				unset($del_st);

				// Agrega los usuarios del Grupo
				for($i=0; $i<count($usuarios); $i++){
					$ins_usr_st = "INSERT INTO grupo_usuario(cod_usuario, id_grupo) values('{$usuarios[$i]}',${id_grupo})";
					if ($conn->Execute($ins_usr_st) === false) {
						print 'error insertando: '.$conn->ErrorMsg().'<BR>';
					}
					unset($ins_usr_st);
				}
			}
?>
<table width="531" border="0" cellspacing="0" cellpadding="3">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td class="aviso" align="center"><b>El Grupo se ha modificado 
      correctamente</b></td>
  </tr>
  <tr>
    <td align="center"> 
      <input type="button" value="Asignar derechos al Grupo" class="but-login" onClick="MM_goToURL('parent','../grupoderechos/index.php?idgrp=<?=$id_grupo?>');return document.MM_returnValue">
      &nbsp;&nbsp;&nbsp;
      <input type="button" value="Modulo Grupos" class="but-login" onClick="MM_goToURL('parent','<?=$_SERVER['PHP_SELF'];?>');return document.MM_returnValue">
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<?
		}
		unset($upd_st); 

		$conn->Close();
	}
}

/* ------------------------------------------------------------- */
/* Funcion para Mostrar el Formulario HTML para asignar			 */
/* Derechos a un Grupo sobre los Modulos Existentes				 */
/* ------------------------------------------------------------- */
/* MODULO DERECHOS-GRUPO										 */
/* ------------------------------------------------------------- */

function insertaGrupoDerechoHTML($idgrp,$tip_modulo=NULL,$idmodpad=NULL){
	$frmName = "frmGrpDer";
	insertaScriptSubmitForm($frmName);
?>
<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table width="540" border="0" cellspacing="1" cellpadding="2">
  <tr> 
    <td colspan="4" class="texto" width="540"> 
      <hr width="100%" size="1">
    </td>
  </tr>
  <tr> 
    <td colspan="4" width="540"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="td-lb-login" width="140"><b>TIPO DE MODULO</b></td>
            <td width="400"> 
<?
	$sql_st = "SELECT id_modulotipo, des_modulotipo FROM modulo_tipo WHERE ind_activo IS true ORDER BY 2";
	muestraSelectUniversal('tip_modulo', $sql_st, $tip_modulo);
?>
              &nbsp;&nbsp;&nbsp;
              <input type="submit" value="Desplegar" class="but-login">
              <input type="hidden" name="idgrp" value="<?=$idgrp;?>">
              <input type="hidden" name="idmodpad" value="<?=$idmodpad;?>">
            </td>
          </tr>
        </table>
    </td>
  </tr>
  <tr> 
    <td colspan="4" class="texto" width="540"> 
      <hr width="100%" size="1">
    </td>
  </tr>
  <tr> 
    <td colspan="4" width="540">&nbsp; </td>
  </tr>
<?
	if(!is_null($tip_modulo)){
?>
  <tr> 
    <td rowspan="2" align="center" width="300" class="texto" bgcolor="#CCE7F9"><b>NOMBRE</b></td>
    <td colspan="3" align="center" width="240" class="texto" bgcolor="#CCE7F9"><b>DERECHOS</b></td>
  </tr>
  <tr> 
    <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Leer</td>
    <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Insertar</td>
    <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Modificar</td>
  </tr>
<?
		muestraGrupoModulosDerechos($idmodpad,$idgrp,$tip_modulo);
?>
  <tr> 
    <td colspan="4" width="540">&nbsp; </td>
  </tr>
  <tr align="center"> 
      <td colspan="4" width="540"> 
        <input type="hidden" name="ind_accion" value="0">
        <input type="submit" value="INGRESAR DERECHOS" class="but-login" onClick="document.<?=$frmName;?>.ind_accion.value=1;">
      &nbsp;&nbsp;&nbsp; 
      <input type="reset" value="LIMPIAR DERECHOS" class="but-login" name="reset">
    </td>
  </tr>

<?
	}
?>
</table>
</form>
<?
}

/* ------------------------------------------------------------- */
/* Funcion para Insertar a la Base de Datos los derechos		 */
/* asignados a un Grupo sobre determinados Modulos				 */
/* ------------------------------------------------------------- */
/* MODULO DERECHOS-GRUPO										 */
/* ------------------------------------------------------------- */

function insertaGrupoDerechosDB($id_grupo, $derechos, $id_modulopadre, $tip_modulo){
	global $type_db, $userdb, $passdb, $db;
	if(count($derechos)>0){
		$error = false;
		$cont = 0;
		while (list ($id_modulo) = each ($derechos)) {
			if(count($derechos[$id_modulo])>0){
				$ins_st[$cont] = "INSERT INTO modulo_derecho(id_modulo, id_grupo";
				$val_st[$cont] = "VALUES (${id_modulo}, ${id_grupo}";
				while(list ($tip_derecho, $val) = each ($derechos[$id_modulo])){
					$ins_st[$cont] .= ",${tip_derecho}";
					$val_st[$cont] .= ",'$val'";
				}
				$ins_st[$cont] .= ") " . $val_st[$cont] . ")";
				unset($val_st);
				$cont++;		
			}
		}

		$conn = & ADONewConnection($type_db['postgres']);
		$conn->debug = false;
		if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
			print $conn->ErrorMsg();
		else{
			// Borra los Anteriores Derechos del Grupo
			$sql_mod_st = "SELECT id_modulo from modulo where id_modulopadre=";
			$sql_mod_st .= (!empty($id_modulopadre)) ? $id_modulopadre : 'NULL';
			$sql_mod_st .= " and id_modulotipo=${tip_modulo} and ind_activo IS true";
			$rs = & $conn->Execute($sql_mod_st);
			unset($sql_mod_st);
			if (!$rs){
				print $conn->ErrorMsg();
			}else{
				while($row = $rs->FetchRow())
					$del_st[]="DELETE FROM modulo_derecho WHERE id_grupo=${id_grupo} and id_modulo={$row[0]}";
				unset($row);
				$rs->Close();
			}			
			for($i=0; $i<count($del_st); $i++){
				if ($conn->Execute($del_st[$i]) === false){
					print 'error borrando: '.$conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($del_st);
			
			for($i=0; $i<count($ins_st); $i++){
				if ($conn->Execute($ins_st[$i]) === false){
					print 'error insertando: '.$conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($ins_st); 
			$conn->Close();
			
			// Muestra el Mensaje Status de la Operacion
			if($error)
				muestraTablaStatusDB("Ha ocurrido un error al asignar los derechos.<br>Vuelva a intentarlo por favor.","Modulo Derechos de Grupo");
			else
				muestraTablaStatusDB("Asignaci�n de Derechos de manera correcta.","Modulo Derechos de Grupo");
		}
	}else{
		$conn = & ADONewConnection($type_db['postgres']);
		$conn->debug = false;
		if(!$conn->Connect("dbname={$db['postgres'][0]} user={$userdb['postgres'][0]} password={$passdb['postgres'][0]}"))
			print $conn->ErrorMsg();
		else{
			$error = false;
			
			// Borra los Anteriores Derechos del Grupo
			$sql_mod_st = "SELECT id_modulo from modulo where id_modulopadre=";
			$sql_mod_st .= (!empty($id_modulopadre)) ? $id_modulopadre : 'NULL';
			$sql_mod_st .= " and id_modulotipo=${tip_modulo} and ind_activo IS true";
			$rs = & $conn->Execute($sql_mod_st);
			unset($sql_mod_st);
			if (!$rs){
				print $conn->ErrorMsg();
			}else{
				while($row = $rs->FetchRow())
					$del_st[]="DELETE FROM modulo_derecho WHERE id_grupo=${id_grupo} and id_modulo={$row[0]}";
				unset($row);
				$rs->Close();
			}			
			for($i=0; $i<count($del_st); $i++){
				if ($conn->Execute($del_st[$i]) === false){
					print 'error borrando: '.$conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($del_st);
			$conn->Close();
		}

		// Muestra el Mensaje Status de la Operacion
		if($error)
			muestraTablaStatusDB("Ha ocurrido un error al asignar los derechos.<br>Vuelva a intentarlo por favor.","Modulo Derechos de Grupo");
		else
			muestraTablaStatusDB("Asignaci�n de Derechos de manera correcta.<br>El grupo no tendra ningun derecho sobre los modulos.","Modulo Derechos de Grupo");
	}
}
?>
