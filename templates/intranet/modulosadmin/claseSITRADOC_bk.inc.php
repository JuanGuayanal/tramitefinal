<?php
// error_reporting(E_ALL ^ E_NOTICE);
putenv("TDSDUMP=/tmp/dnpa.log");
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class SITRADOC_bk extends Modulos {

	function SITRADOC_bk($menu){
		/*
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
		*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		$this->datosUsuarioMSSQL();
		$this->datosPermisoSITRADOC();
		if(!$_SESSION['administracion']){
			$_SESSION['administracion'] = $this->userSITRADOC['ADMINISTRACION'];
			$_SESSION['docantiguos'] = $this->userSITRADOC['DOCANTIGUOS'];
			$_SESSION['resolucion'] = $this->userSITRADOC['RESOLUCION'];
			$_SESSION['correspondencia'] = $this->userSITRADOC['CORRESPONDENCIA'];
		}		
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'MUESTRA_DATOS_USUARIO' => 'frmShowWel',
							'FRM_BUSCA_DOCUMENTO' => 'frmSearchDes',
							'BUSCA_DOCUMENTO' => 'searchDes',
							'DOWN_BUS_DES' => 'dwnSearchDes',
							'FRM_INGRESA_DESEMBARQUE' => 'frmAddDes',
							'FRM_INFO_ADICIONAL' => 'frmInfAdd',
							'INGRESA_DESEMBARQUE' => 'addDes',
							'DOWN_ING_DES' => 'dwnAddDes',
							'BUEN_ING_DES' => 'goodAddDes',
							'MAL_ING_DES' => 'badAddDes',
							'XML_EMBARCACION' => 'xmlBoat',
							'XML_ESPECIE' => 'xmlSpecies',
							'XML_DESTINO' => 'xmlDestiny'
							);

		// $this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmShowWel', 'label' => 'tus datos' ),
							1 => array ( 'val' => 'frmSearchDes', 'label' => 'buscar' ),
							2 => array ( 'val' => 'frmAddDes', 'label' => 'ingresar' ),
							3 => array ( 'val' => 'frmInfAdd', 'label' => 'de inter�s' )
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
	}
	
	function GetAccion($accion){
		return $this->arr_accion[$accion];
	}
	
	function ValidaFecha($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,3,2)),sprintf('%d',substr($fecha,0,2)),substr($fecha,6,4)) )
			$errors .= '- La Fecha ' . $error . ' no es Valida.<br />';
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function XMLEmbarcacion(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$sql_SP = "EXECUTE sp_listEmbarcaciones";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('emb',array('id'=>$row[0],
										  'nomb'=>$row[1],
										  'matr'=>$row[2]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEmbarcaciones.tpl.php');
	}
	
	function XMLEspecie(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$sql_SP = "EXECUTE sp_listEspecies";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('esp',array('id'=>$row[0],
										  'nomb'=>$row[1]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEspecies.tpl.php');
	}
	
	function XMLDestino(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$sql_SP = "EXECUTE sp_listDestinos";
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('des',array('id'=>$row[0],
										  'nomb'=>$row[1]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLDestinos.tpl.php');
	}
	
	function MuestraIndex(){
		$this->MuestraDatosUsuario();
	}
	
	function MuestraMenu(){
		$this->GeneraMenuPager(5,false);
	}
	
	function MuestraDatosUsuario(){
		$html = & new Smarty;
		
		$html->assign('frmName','frmBuscar');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/muestraDatosUsuario.tpl.php');
	}
	
	function FormBuscaDesembarque($page=null,$idEmb=null,$desEmb=null,$idEsp=null,$desEsp=null,$idDes=null,$desDes=null,$fecDesIni=null,$fecDesFin=null){
		global $tipDocumento,$nroTD,$tipBusqueda,$asunto,$observaciones,$radiobutton,$RZ,$indicativo;
		$html = & new Smarty;
		
		$frmName = 'frmBuscar';
		$html->assign('frmName',$frmName);
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('radiobutton',$radiobutton);		
		$html->assign_by_ref('RZ',$RZ);		
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('fecDesIni',$fecDesIni);
		$html->assign_by_ref('fecDesFin',$fecDesFin);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);

		$html->assign('datos',compact('idEmb',
									  'desEmb',
									  'idEsp',
									  'desEsp',
									  'idDes',
									  'desDes',
									  'fecDesIni',
									  'fecDesFin',
									  'tipDocumento',
									  'nroTD'));
		
		$html->display('oad/tramite/buscaDesembarque.tpl.php');
	}
	
	function busSiglasdet($dat){//haya las siglas(Dependencia) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select Upper(dep.siglas) 
		         from movimiento_documento md, db_general.dbo.h_dependencia dep
				 where 
				 /*md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento where id_documento=$dat)*/
				 md.id_documento=$dat and md.derivado=0 
				  and md.id_dependencia_destino=dep.codigo_dependencia"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					/*
					while ($row = $rs->FetchRow()){
						$depe=$row[0];
						$depe=strtoupper($depe);}
					$rs->Close();//Agregado el 22/03/2005
					*/
					$i=-1;
					while(!$rs->EOF){
					$i++;
					$arregloP[$i]=$rs->fields[0];
					$rs->MoveNext();
					}

						$depe=$arregloP[0];
						$contador=count($arregloP);
						//echo "el contador".$depe;
						if($contador==1){
							$depe1=$depe;
						}
						if($contador>1){
							$depe1=$depe;
							for($h=1;$h<$contador;$h++){
								$depe1=$depe1.' - '.$arregloP[$h];
							}
						}//fin del if($contador>0)

			}
			unset($rs);//Agregado el 22/03/2005
		return($depe1);
	}
	
	function busSiglasubi($dat){//haya la ubicaci�n(Dependencia) a la cual deriv� Tr�mite Documentario
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select Upper(dep.siglas) 
		         from movimiento_documento md, db_general.dbo.h_dependencia dep
				 where md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where id_documento=$dat)
				  and md.id_dependencia_destino=dep.codigo_dependencia"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow()){
						$depe=$row[0];
						$depe=strtoupper($depe);
						}
					$rs->Close();//Agregado el 22/03/2005
			}
			unset($rs);//Agregado el 22/03/2005
		return($depe);
	}	
	
	function BuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin,$down=false){
		global $tipDocumento,$nroTD,$tipBusqueda,$asunto,$observaciones,$radiobutton,$RZ,$indicativo;
		
		//echo $radiobutton."xxx";
		$idEmb = empty($idEmb) ? null : $idEmb;
		
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
	
		// Se Muestra el Formulario de Busqueda Inicial
		if(!$down)
		  
			$this->FormBuscaDesembarque($page,$idEmb,$desEmb,$idEsp,$desEsp,$idDes,$desDes,$fecDesIni,$fecDesFin);
		
		$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;

		// Ejecuta el conteo General
		$this->abreConnDB();
	 	$this->conn->debug = true;

		
		$stmt = $this->conn->PrepareSP('sp_busIDDocEsp');
		//$this->conn->InParameter($stmt,$idEmb,'IDEMB',5,SQLINT4);
		$this->conn->InParameter($stmt,$nroTD,'NROTRAMITE',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$RZ,'RZ',-1,SQLVARCHAR);
		//$this->conn->InParameter($stmt,$desEsp,'DESESP',-1,SQLVARCHAR);
		//$this->conn->InParameter($stmt,$idDes,'IDDES',-1,SQLVARCHAR);
		//$this->conn->InParameter($stmt,$desDes,'DESDES',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$fecDesIni,'FECHAINI',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$fecDesFin,'FECHAFIN',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$this->userIntranet['COD_DEP'],'CODDEP',-1,SQLVARCHAR);
		$this->conn->InParameter($stmt,$start,'START',4,SQLINT4);
		$this->conn->InParameter($stmt,$this->numMaxResultsSearch,'MAXROWS',4,SQLINT4);
		// return IDS Desembarques
		$ids = '';
		$this->conn->OutParameter($stmt,$ids,'IDS');
		// return value in mssql - RETVAL is hard-coded name 
		$this->conn->OutParameter($stmt,$numRegs,'RETVAL');
		$this->conn->Execute($stmt);
echo "consulta".$numRegs;
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Obtiene Todos los Datos del Desembarque*/
			$stmt = $this->conn->PrepareSP('sp_busDatosDocEspV2'); # note that the parameter name does not have @ in front!
			$this->conn->InParameter($stmt,$ids,'IDS',-1,SQLVARCHAR);
			$rs = & $this->conn->Execute($stmt);
			if(!$rs)
				print $this->conn->ErrorMsg();
			else{
				while($row = $rs->FetchRow())
					$html->append('arrDes', array('RZ' => $row[0],
												  'nroTD' => $row[1],
												  'fecIng' => $row[2],
												  'asunto' => $row[3],
												  'ind' => $row[4],
												  //'nVol' => number_format($row[5],2),
												  //'nPre' =>	number_format($row[6],2),
												  'det' => $this->busSiglasdet($row[9]),
												  //'ini' => $this->busSiglasubi($row[9]),
												  'ini' => $row[10],												  
												  'clave' => $row[7],
												  'dTur' => $row[8]));
				$rs->Close();
			}
			unset($rs);
		}else
			$start = 0;

		$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		// $html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		$html->assign('datos',compact('idEmb',
									  'desEmb',
									  'idEsp',
									  'desEsp',
									  'idDes',
									  'desDes',
									  'fecDesIni',
									  'fecDesFin'));
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->GetAccion('BUSCA_DOCUMENTO'), true));

		// Muestra el Resultado de la Busqueda
		$html->display((!$down) ? 'oad/tramite/buscaDetalles.tpl.php' : 'dnpa/desembarque/descargaBuscaDesembarque.tpl.php');
	}
	
	function DescargaBuscaDesembarque($idEmbarcacion,$desEmbarcacion,$idEspecie,$desEspecie,$idDestino,$desDestino,$desFechaIni,$desFechaFin){
		//Begin writing headers
		header('Pragma: public');
		//header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: public'); 
		header('Content-Description: File Transfer');
		
		header('Content-type: application/csv');
		$desFile = 'desembarque-' . str_replace('/','',$desFechaIni);
		$desFile .= (($desFechaIni!=$desFechaFin)&&$desFechaFin) ? '_' . str_replace('/','',$desFechaFin) : '';
		$desFile .= '.csv';
		header('Content-Disposition: attachment; filename="' . $desFile . '"');
		$this->BuscaDesembarque(null,$idEmbarcacion,$desEmbarcacion,$idEspecie,$desEspecie,$idDestino,$desDestino,$desFechaIni,$desFechaFin,true);
		exit;
	}

	function FormIngresaDesembarque($fecDesembarque=null,$idTurno=null,$registro=null,$errors=null){
		$html = & new Smarty;
		
		$html->assign('frmName','frmIngresa');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);

		$html->assign_by_ref('fecDesembarque',$fecDesembarque);
		$html->assign_by_ref('idTurno',$idTurno);
		
		// Contenido Select Turno
		$sql_st = "SELECT id_turno, lower(descripcion) ".
				  "FROM dbo.turno ";
		$html->assign_by_ref('selTurno',$this->ObjFrmSelect($sql_st, $idTurno, true, true, array('val'=>'none','label'=>'-- Seleccione --')));
		unset($sql_st);

		for($i=0;$i<count($registro);$i++)
			$html->append('registro',explode('||||',$registro[$i]));
			
		$html->assign_by_ref('errors',$errors);
		
		// Variables de Session
		$html->assign_by_ref('sessionTime',$_SESSION['session_max_time']);
		
		$html->display('dnpa/desembarque/formIngresaDesembarque.tpl.php');
	}
	
	function IngresaDesembarque($fecDesembarque,$idTurno,$registro){
		//Manipulacion de las Fechas
		$fecDesembarque = ($fecDesembarque!='//'&&$fecDesembarque&&!strstr($fecDesembarque,'none')) ? $fecDesembarque : NULL;
		
		// Comprueba Valores
		$bFDes = $bTur = $bReg = false;
		
		if($fecDesembarque){ $bFDes = ($this->ValidaFecha($fecDesembarque,'Desembarque')); }else{ $bFDes = true; $this->errors .= 'La Fecha de Desembarque debe ser especificada<br />'; }
		if(!$idTurno){ $bTur = true; $this->errors .= 'El Turno debe ser especificado<br />'; }
		if(!is_array($registro)){ $bReg = true; $this->errors .= 'Debe especificar datos para el desembarque.<br />'; }

		if($bFDes||$bTur||$bReg){
			$objIntranet = new Intranet();
			$objIntranet->Header('Desembarque Diario',false,array('desDNPA'));
			$objIntranet->Body('desDNPA_tit.gif',false,false);
			$this->MuestraMenu();
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormIngresaDesembarque($fecDesembarque,$idTurno,$desembarque,$errors);
			$objIntranet->Footer(false);
		}else{
			$this->abreConnDB();
			$this->conn->debug = true;
			
			// Inicia la Transaccion
			//$this->conn->BeginTrans(); 
		 	$this->conn->debug = true;
		
			$tzTrans = mktime();
			
			for($i=0;$i<count($registro);$i++){
				list($idEmb,$idEsp,$idDes,$numVol,$numPre) = explode('||||',$registro[$i]);
				//$stmt = $this->conn->PrepareSP('dbo.sp_ing_dnpa_prueba'); # note that the parameter name does not have @ in front!
				$stmt = $this->conn->PrepareSP('sp_insDesembarque'); # note that the parameter name does not have @ in front!
				$this->conn->InParameter($stmt,$fecDesembarque,'FECHA',-1,SQLVARCHAR);
				$this->conn->InParameter($stmt,$idTurno,'ID_TURNO',1,SQLINT4);
				$this->conn->InParameter($stmt,$idEmb,'ID_EMB',5,SQLINT4);
				$this->conn->InParameter($stmt,$idEsp,'ID_ESPECIE',-1,VARCHAR);
				$this->conn->InParameter($stmt,$idDes,'ID_DESTINO',-1,VARCHAR);
				$this->conn->InParameter($stmt,$numVol,'TM',20,SQLFLT8);
				$this->conn->InParameter($stmt,$numPre,'PRECIO',20,SQLFLT8);
				$this->conn->InParameter($stmt,$tzTrans,'TIMETRANS',-1,SQLVARCHAR);
				$this->conn->InParameter($stmt,$_SESSION['cod_usuario'],'CODUSER',-1,SQLVARCHAR);
				# return value in mssql - RETVAL is hard-coded name 
				$this->conn->OutParameter($stmt,$RETVAL,'RETVAL');
				$this->conn->Execute($stmt);

				if($RETVAL)
					break;
			}
			
			/*if($RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();*/

			$destination = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->GetAccion('MAL_ING_DES') : $this->GetAccion('BUEN_ING_DES');
			$destination .= '&menu=' . $this->menuPager;
			$destination .= ($RETVAL) ? '' : '&tz=' . $tzTrans;
			
			header('Location: ' . $destination);
			exit;
		}
	}
	
	function EstadoIngresaDesembarque($status,$timetrans=null){
		if($status==$this->GetAccion('BUEN_ING_DES'))
			$msgStat = '<strong>El ingreso de datos se ha llevado a cabo con &eacute;xito.</strong>';
		elseif($status==$this->GetAccion('MAL_ING_DES'))
			$msgStat = '<strong>Hubo un error al realizar el ingreso de datos. Vuelva a intentarlo en breves momentos. Gracias.</strong>';
			
		$html = & new Smarty;
		
		$html->assign_by_ref('msgStat',$msgStat);
		$html->assign_by_ref('timetrans',$timetrans);
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/estadoIngresaDesembarque.tpl.php');
	}
	
	function ListaIngresaDesembarque($tzTrans){
		$html = & new Smarty;
		
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		$stmt = $this->conn->PrepareSP('sp_listDesembarqueTrans'); # note that the parameter name does not have @ in front!
		$this->conn->InParameter($stmt,$tzTrans,'TIMETRANS',-1,SQLVARCHAR);
		$rs = & $this->conn->Execute($stmt);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('arrReg',array('nEmb'=>$row[0],
											 'mEmb'=>$row[1],
											 'dEsp'=>$row[2],
											 'dDes'=>$row[3],
											 'nVol'=>$row[4],
											 'nPre'=>$row[5],
											 'fDes'=>$row[6]));
			}
			$rs->Close();
		}
		unset($rs);	
		
		//Begin writing headers
		header("Pragma: public");
		//header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public"); 
		header("Content-Description: File Transfer");
		
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=desembarque-".$tzTrans.".csv");
		$html->display('dnpa/desembarque/listaIngresaDesembarque.tpl.php');
	}
	
	function muestraInfoInteres(){
		$html = & new Smarty;
		
		$html->assign('frmName','frmBuscar');
		$html->assign_by_ref('frmURL',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('dnpa/desembarque/muestraInfoInteres.tpl.php');
	
	}
}
?>
