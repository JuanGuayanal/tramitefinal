<?
include_once('claseObjetosForm.inc.php');
include_once('claseModulos.inc.php');

class Intranet extends ObjetosForm{
	// Define parametros de la aplicacion
	var $indexPage = "intranet.php";
	var $loginPage = "index.php";
	var $exitPage = "exit.php";
	var $sugerenciaPage = "sugerencias.php";
	var $gruposMailPage = "msggrupos.php";
	var $_mailDomain = 'CONVENIO_SITRADOC.gob.pe';
	var $mods = null;
	
	function Intranet(){
		$this->BeginSession();
		$this->EntryRigth();
		$this->mods = & new Modulos();
	}

	function Header($titulo=NULL,$indIndex=false,$styles=false,$onLoad=false,$prueba=false){
		// Crea el Objeto HTML
		$html = new Smarty;
	
		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('titulo',$titulo);
		$html->assign_by_ref('indIndex',$indIndex);
		$html->assign_by_ref('styles',$styles);
		$html->assign_by_ref('onLoad',$onLoad);
		if($prueba)
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,2,1,true));
		else
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,1,1,true));
		$html->assign_by_ref('modPers',$this->mods->muestraIndexSubmodulosHTML(NULL,1,2,true));
		$html->assign_by_ref('fecHoy',$this->CurrentDate());
		$html->assign_by_ref('userIntra',$_SESSION['cod_usuario']);
		$html->assign_by_ref('domiIntra',$this->_mailDomain);

                $html->assign_by_ref('groupPage',$this->mods->mensajesGrupos());
//		$html->assign_by_ref('groupPage',$this->gruposMailPage);
		$html->assign_by_ref('sugePage',$this->sugerenciaPage);
		$html->assign_by_ref('indexPage',$this->indexPage);
		$html->assign_by_ref('exitPage',$this->exitPage);
		
		$html->display('intranet/header.tpl.php');
	}
	
	function Body($imgTitulo=NULL,$menuLeft=NULL,$blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->assign_by_ref('menuLeft',$menuLeft);
		$html->assign_by_ref('imgTitulo',$imgTitulo);
		$html->display('intranet/body.tpl.php');		
	}

	
	function Footer($blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->display('intranet/footer.tpl.php');
	}

        function director(){

                if($this->mods->mensajesGrupos()){
                        return true;
                }else{return false;}

        }

}
?>
