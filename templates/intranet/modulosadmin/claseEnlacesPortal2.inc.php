<?php
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class EnlacesPortal extends Modulos{

    function EnlacesPortal($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_NOTICIA => 'frmSearchNew',
							BUSCA_NOTICIA => 'searchNew',
							FRM_AGREGA_NOTICIA => 'frmAddNew',
							AGREGA_NOTICIA => 'addNew',
							FRM_MODIFICA_NOTICIA => 'frmModifyNew',
							MODIFICA_NOTICIA => 'modifyNew',
							REPORTE_1 => 'report1',
							REPORTE_2 => 'report2',
							EMBARCACIONES => 'Embarcaciones',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		
		//$this->datosUsuarioPortal();
		$this->datosUsuarioMSSQL();
		
		/*
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Enlaces del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}*/

		// Items del Menu Principal
		/*if($_SESSION['mod_ind_leer'])*/ $this->menu_items[0] = array ( 'val' => 'frmSearchNew', label => 'BUSCAR' );
		/*if($_SESSION['mod_ind_insertar'])*//* $this->menu_items[1] = array ( 'val' => 'frmAddNew', label => 'AGREGAR' );*/
		/*if($_SESSION['mod_ind_modificar'])*/ $this->menu_items[1] = array ( 'val' => 'frmModifyNew', label => 'MODIFICAR' );

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	
	function ValidaFechaNoticia($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('portal/enlaces/headerArm.tpl.php');
		$html->display('portal/enlaces/showStatTrans.inc.php');
		$html->display('portal/enlaces/footerArm.tpl.php');
	}
	
	function FormBuscaNoticia($page=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('portal/enlaces/headerArm.tpl.php');
		$html->display('portal/enlaces/search.tpl.php');
		if(!$search) $html->display('portal/enlaces/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaNoticia();
	}
	
 function obtenerNroDiasUtil($fecIni){
	  $this->abreConnDB();
	  //$this->connDebug=true;
	  
	  //$sql="select datediff(dd,Convert(datetime,'$fecIni',103),getDate())";
	  $rs = & $this->conn->Execute($sql);
	  unset($sql);
	  if (!$rs){
	   print $this->conn->ErrorMsg();
	  }else{
	  	$nroDias=$rs->fields[0];
		//$rs->Close();
	  }
		if($nroDias>0){
				$sql2="select count(*) from db_tramite_documentario.dbo.dianolaborable where dia > Convert(datetime,'$fecIni',103) 
							 and dia < getDate() ";
				$rs2 = & $this->conn->Execute($sql2);
				unset($sql2);
				if (!$rs2){
				 print $this->conn->ErrorMsg();
				}else{
					$cont=$rs2->fields[0];
				$rs2->Close();
				}
				unset($rs2);
				
					  $retraso=$nroDias-$cont;
		}else{
			$retraso="Sin retraso";
		}			  
	  return($retraso);	  
 }	

	function BuscaNoticia($page,$desNombre){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaNoticia($page,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		$this->conn->debug = true;

		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 ||$coddep_dig==54){ $coddep_dig=25; }		
		$condConsulta[] = "codigo_dependencia=".$coddep_dig." ";		
		echo "$condConsulta";
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

           // Evalua que no contenga sentencias SQL
	        $this->evaluaNoSql($nopc);

							$sql_SP = sprintf("EXECUTE sp_busIDEnlace %s,%s,%s,0,0",
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDEnlace %s,%s,%s,%s,%s",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosEnlace %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($row = $rs->FetchRow())
							$html->append('arrNoti', array('id' => $row[0],
														   'desc' => $row[1],
														   'fech' => $row[2],
														   'esta' => $row[3],
															 'prox' => $row[4],
															 'tipo' => $row[5],
															 'frec' => $row[6],
															 'yy' => $row[7],
															 'dias'=> $this->obtenerNroDiasUtil($row[7])
															 ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
					
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_NOTICIA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('portal/enlaces/searchResult.tpl.php');
		$html->display('portal/enlaces/footerArm.tpl.php');
	}

	function FormManipulaNoticia($id=NULL,$Selformacion=NULL,$desTitulo=NULL,$desEnlace=NULL,$SelFrecuencia=NULL,$desFechaIni=NULL,$desFechaFin=NULL,$desArchivo=NULL,
 							     $indActivo=NULL,$reLoad=false,$errors=NULL){
		global $adjunto;						
		if($id&&!$reLoad){
			$this->abreConnDB();
			
			$sql_st = sprintf("SELECT e.ID_ENLACE,e.titulo,e.url,convert(varchar,e.fecha_publicacion,103),
	 		convert(varchar,e.fecha_proxima,103),f.descripcion,ti.descripcion,e.nomb_archivo,f.id_frecuencia
	 from enlace e,frecuencia f,tipo_informacion ti
	 where e.id_frecuencia=f.id_frecuencia
	 and e.id_tipo_informacion=ti.id_tipo_informacion
	 and id_enlace=%d",$this->PrepareParamSQL($id));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				$desTitulo=$rs->fields[1];
				$url=$rs->fields[2];
				$fecPubli=$rs->fields[3];
				$fecProcPubli=$rs->fields[4];
				$desFrecuencia=$rs->fields[5];
				$desTipInfo=$rs->fields[6];
				$nomArchivo=$rs->fields[7];
				$idFrecuencia=$rs->fields[8];
			}
			unset($rs);
		}
		switch($idFrecuencia){
			case 1:
					 $sql="select convert(varchar,dateadd(dd,1,getDate()),103) ";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=0;
						else{
							$ProximaPubli=$rs->fields[0];
						}
					break;						 
			case 2:
					 $sql="select convert(varchar,dateadd(mm,1,getDate()),103)";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=0;
						else{
							$ProximaPubli=$rs->fields[0];
						}
					break;						 
			
			case 3:
					 $sql="select convert(varchar,dateadd(mm,3,getDate()),103)";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=0;
						else{
							$ProximaPubli=$rs->fields[0];
						}
					break;						 
			
			case 4:
					 $sql="select convert(varchar,dateadd(mm,6,getDate()),103)";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=0;
						else{
							$ProximaPubli=$rs->fields[0];
						}
					break;						 
			
			case 5:
					 $sql="select convert(varchar,dateadd(yy,1,getDate()),103)";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=0;
						else{
							$ProximaPubli=$rs->fields[0];
						}
					break;						 
			
		}
		//echo "cc".$ProximaPubli."cc";
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddNew';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idNoticia',$idNoticia);
		$html->assign_by_ref('desTitulo',$desTitulo);
		$html->assign_by_ref('desFrecuencia',$desFrecuencia);
		$html->assign_by_ref('desNoticia',$desNoticia);
		$html->assign_by_ref('url',$url);
		$html->assign_by_ref('desImg',$desImg);
		$html->assign_by_ref('desTipInfo',$desTipInfo);
		
		$html->assign_by_ref('numAnImgExp',$numAnImgExp);
		$html->assign_by_ref('numAlImgExp',$numAlImgExp);
		
		$html->assign_by_ref('fecPubli',$fecPubli);
		$html->assign_by_ref('fecProcPubli',$fecProcPubli);
		$html->assign_by_ref('indImgExp',$indImgExp);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		$html->assign_by_ref('apellido',$this->userIntranet['APELLIDO']);
		$html->assign_by_ref('nombre',$this->userIntranet['NOMBRE']);
		
		$html->assign_by_ref('nomArchivo',$nomArchivo);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		
		$html->assign_by_ref('ProximaPubli',$ProximaPubli);
		
		// Fecha Noticia
		$html->assign_by_ref('selMesNot',$this->ObjFrmMes(1, 12, true, substr($fecNoticia,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaNot',$this->ObjFrmDia(1, 31, substr($fecNoticia,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoNot',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecNoticia,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selHoraNot',$this->ObjFrmHora(0, 23, true, (substr($fecNoticia,11,2)) ? substr($fecNoticia,11,2) : NULL, true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selMinNot',$this->ObjFrmMinuto(1, true, (substr($fecNoticia,14,2)) ? substr($fecNoticia,14,2) : NULL, true, array('val'=>'none','label'=>'---')));

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('portal/enlaces/headerArm.tpl.php');
		$html->display('portal/enlaces/frmModificaEnlace.tpl.php');
		$html->display('portal/enlaces/footerArm.tpl.php');
	
	}
	/**/
	function ManipulaNoticia($id,&$adjunto,$desFechaIni){
		global $url,$nomArchivo;					

		if(!$desFechaIni){ $bFec = true; $this->errors .= 'La fecha de pr�xima actualizaci�n debe ser especificada<br>'; }
		if(!$adjunto){ $bAdj = true; $this->errors .= 'El adjunto debe ser especificado<br>'; }
		
		if($bAdj){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormManipulaNoticia($idNoticia,$desTitulo,$desResumen,$desNoticia,$fecNoticia,$desFuente,
							$indActivo,$indImg,$desImg,$indImgExp,$desImgExp,$numAnImgExp,
							$numAlImgExp,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			$this->conn->debug = true;
			// Inicia la Transaccion
			//$this->conn->BeginTrans(); 
			
			//echo $id."xx<br>";
			
			$sql_SP = sprintf("EXECUTE webminpro.AdjArchivoP %d",
							  $id);
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			//echo "xx".$RETVAL."xx";
			//exit;
////////////////////////////////////////////
		/*Otra prueba para insertar un archivo en el servidor*/
//echo $url."xxxx";	
if(!empty($_FILES['adjunto']['tmp_name']) AND is_uploaded_file($_FILES['adjunto']['tmp_name'])) 
{ 
//echo $url."matrix";exit;
//$dd=filesize($_FILES['adjunto']['tmp_name']);
//echo "gg".$dd."gg";exit;
//On va v�rifier la taille du fichier en ne passant pas par $_FILES['fichier_source']['size'] pour �viter les failles de s�curit� 
//if(filesize($_FILES['adjunto']['tmp_name'])<51200) //562017
if(filesize($_FILES['adjunto']['tmp_name'])<5640000)
{ 
	//echo $url."matrix";exit;

	//Copie le fichier dans le r�pertoire de destination 
	//if(move_uploaded_file($_FILES['adjunto']['tmp_name'],'/var/www/intranetp/institucional/aplicativos/altadireccion/tmp/prueba.csv')) 
	//echo $url."prueba.xls";
	$nombre=$adjunto['name'];
	//$urlV="/var/www/intranet/institucional/aplicativos/enlaces/descarga/".$nombre;
	if($nomArchivo && $nomArchivo!=""){
		$urlV="/var/www/intranet/institucional/aplicativos/enlaces".$url.$nomArchivo;
	}else{
		$urlV="/var/www/intranet/institucional/aplicativos/enlaces".$url.$nombre;
	}
	//echo "xx".$urlV."xx";exit;
	if(move_uploaded_file($_FILES['adjunto']['tmp_name'],$urlV)) 
	//if(move_uploaded_file($_FILES['adjunto']['tmp_name'],'../altadireccion/tmp/')) 
	{ 
		//Le fichier a �t� upload� correctement 
		//echo 'Ok, archivo enviado correctamente';//exit;
		$r=44;
		/**/
		/*$destination2 = '/var/www/intranetp/institucional/aplicativos/altadireccion/tmp/prueba.csv'; 
		$fp = fopen ( $destination2 , "r+" ); 
		while (( $data = fgetcsv ( $fp , 1000 , "," )) !== FALSE ) { // Mientras hay l�neas que leer... 
			echo $data[0]."-".$data[1]."-".$data[2]."-".$data[3]."-".$data[4]."-".$data[5]."-".$data[6]."-".$data[7]."-".$data[8]."-".$data[9]."-".$data[10]."-".$data[11]."-".$data[12]."<br>";

		}
		 
		fclose ( $fp );
		*/		 
		/**/
	} 
	else 
	{ 
		//Erreur 
		echo 'Error al enviar el archivo al servidor'; 
		} 
	} 
}
//exit;
/////////////////////////////////////////////				
				//echo "cc".$RETVAL."cc";exit;
			//echo "<br>xx".$RETVAL."xx";	

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			//$destination .= $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu=";
			$destination .= $id ? $this->menu_items[1]['val'] : $this->menu_items[1]['val'];
			// echo $destination; exit;
			header("Location: $destination");
			exit;
		}
	}	

	function Reporte2($print){
		// Genera HTML de Muestra
		$html = new Smarty;
		$this->abreConnDB();
		//$this->conn->debug = true;

		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 ||$coddep_dig==54){ $coddep_dig=25; }
		
		$sql="SELECT e.ID_ENLACE,e.titulo,e.url,convert(varchar,e.fecha_publicacion,103),
	 		convert(varchar,e.fecha_proxima,103),ti.descripcion,f.descripcion,FECHA_PROXIMA,d.siglas
	 from enlace e,frecuencia f,tipo_informacion ti, dbo.h_dependencia d
	 where e.id_frecuencia=f.id_frecuencia and e.codigo_dependencia=d.codigo_dependencia
	 and e.id_tipo_informacion=ti.id_tipo_informacion /*and e.codigo_dependencia=".$coddep_dig."*/ ORDER BY e.titulo";

					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
							while ($row = $rs->FetchRow())
							$html->append('arrNoti', array('id' => $row[0],
														   'desc' => $row[1],
														   'fech' => $row[2],
														   'esta' => $row[3],
															 'prox' => $row[4],
															 'tipo' => $row[5],
															 'frec' => $row[6],
															 'yy' => $row[7],
															 'depe' => $row[8],
															 'dias'=> $this->obtenerNroDiasUtil($row[7])
															 ));
						$rs->Close();
					}
					unset($rs);							
	
	$html->assign_by_ref('actividad',$actividad);
	$html->assign_by_ref('diferencia',$diferencia);
	$html->assign_by_ref('noticia',$noticia);
	$html->assign_by_ref('diferencia1',$diferencia1);
	$html->assign_by_ref('norma',$norma);
	$html->assign_by_ref('diferencia2',$diferencia2);
	
	$print ? $html->display('oad/tramite/DinamicReports/ReportToSGandVice/printListadoOverView.tpl.php') : $html->display('portal/enlaces/frmReportesEnlaceDias.tpl.php');						

	}	
}
?>
