<?php

class ModulosAdmin extends Modulos {

	var $frmDerUrl = '/admintranet/moduloderechos/index.php';
	var $accionDer = 1;

	function ModulosAdmin($menu=null) {
		if(!$_SESSION['mod_ind_leer']){
                        $this->logModulo('Acceso no Autorizado');
                        return $this = false;
                }

		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][0];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_MODULO' => 'frmSearchModule',
							'BUSCA_MODULO' => 'searchModule',
							'FRM_AGREGA_MODULO' => 'frmAddModule',
							'AGREGA_MODULO' => 'addModule',
							'FRM_MODIFICA_MODULO' => 'frmModifyModule',
							'MODIFICA_MODULO' => 'modifyModule',
							'BUENA_TRANS' => 'goodTrans',
							'MALA_TRANS' => 'badTrans'							
							);

		// Items del Menu Principal
		if($_SESSION['mod_ind_leer']) $this->menu_items[] = array ( 'val' => 'frmSearchModule', 'label' => 'BUSCAR' );
		if($_SESSION['mod_ind_insertar']) $this->menu_items[] =	array ( 'val' => 'frmAddModule', 'label' => 'AGREGAR' );
		if($_SESSION['mod_ind_modificar']) $this->menu_items[] =	array ( 'val' => 'frmModifyModule', 'label' => 'MODIFICAR' );

		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('admin/modulos/headerArm.tpl.php');
		$html->display('admin/modulos/showStatTrans.inc.php');
		$html->display('admin/modulos/footerArm.tpl.php');
	}

	
	function GetRaizModulo($id,&$des_raiz,&$num_profundidad){
		$this->abreConnDB();
		
		$sql_st = "SELECT des_raiz || id_modulo || '/', num_profundidad+1 FROM modulo WHERE id_modulo=$id";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if($row = $rs->FetchRow()){
				$des_raiz=$row[0];
				$num_profundidad=$row[1];
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
	}
	
	function MuestraIndex() {
		$this->FormBuscaModulo();
	}
	
	function FormBuscaModulo($page=NULL,$idModTipo=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$sql_st = "SELECT id_modulotipo, des_modulotipo " .
				  "FROM modulo_tipo " .
				  "WHERE ind_activo IS true ".
				  "ORDER BY 2";
		// Contenido Select Tipo Dispositivo
		$html->assign_by_ref('selModTipo',$this->ObjFrmSelect($sql_st, $idModTipo, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$html->assign_by_ref('tipModulo',$tipModulo);
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('admin/modulos/headerArm.tpl.php');
		$html->display('admin/modulos/search.tpl.php');
		if(!$search) $html->display('admin/modulos/footerArm.tpl.php');
	}
	
	function BuscaModulo($page,$idModTipo,$desNombre){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaModulo($page,$idModTipo,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		$sql_SP =  sprintf("SELECT count(id_modulo) " .
						   "FROM modulo " .
						   "WHERE %1\$s " .
								 "(Upper(des_titulocorto) like Upper('%%%2\$s%%') " . 
								 "or UPPER(des_titulolargo) like UPPER('%%%2\$s%%'))",
							($idModTipo!='none') ? 'id_modulotipo=' . $this->PrepareParamSQL($idModTipo) . ' and ' : '',
							($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL);
							
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);


		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 0 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP =  sprintf("SELECT id_modulo, UPPER(des_titulocorto), CASE WHEN ind_activo IS true THEN 'Activo' ELSE 'Inactivo' END, num_profundidad, id_modulopadre, id_modulotipo " .
							   "FROM modulo " .
							   "WHERE %1\$s " .
									 "(Upper(des_titulocorto) like Upper('%%%2\$s%%') " . 
									 "or UPPER(des_titulolargo) like UPPER('%%%2\$s%%')) " .
							   "ORDER BY id_modulotipo, des_raiz || text(id_modulo) " . 
							   "LIMIT %3\$s OFFSET %4\$s",
								($idModTipo!='none') ? 'id_modulotipo=' . $this->PrepareParamSQL($idModTipo) . ' and ' : '',
								($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL,
								$this->numMaxResultsSearch,
								$start);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsMod = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsMod)
				print $this->conn->ErrorMsg();
			else{
				while($modData = $rsMod->FetchRow()){
					// Obtiene los Usuarios con derecho sobre el Modulo
					$sql_SP = sprintf("SELECT cod_usuario, CASE WHEN ind_leer IS true THEN '(L)' ELSE '' END, " .
											 "CASE WHEN ind_insertar IS true THEN '(I)' ELSE '' END, " .
											 "CASE WHEN ind_modificar IS true THEN '(M)' ELSE '' END " .
									  "FROM modulo_derecho " .
									  "WHERE id_modulo=%d and cod_usuario is not NULL " .
									  "ORDER BY 1",
									  $modData[0]);

					//echo $sql_SP;
					$rsUsr = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					$usrsMod = array();
					if(!$rsUsr)
						print $this->conn->ErrorMsg();
					else{
						while($usrData = $rsUsr->FetchRow())
							$usrsMod[] = array('user' => $usrData[0],
											   'read' => $usrData[1],
											   'inse' => $usrData[2],
											   'modi' => $usrData[3]);
						$rsUsr->Close();
					}
					unset($rsUsr);
					
					// Obtiene los Grupos con derecho sobre el Modulo
					$sql_SP = sprintf("SELECT g.id_grupo, g.cod_grupo, CASE WHEN ind_leer IS true THEN '(L)' ELSE '' END, " .
											 "CASE WHEN ind_insertar IS true THEN '(I)' ELSE '' END, " .
											 "CASE WHEN ind_modificar IS true THEN '(M)' ELSE '' END " .
									  "FROM grupo g, modulo_derecho md ".
						  			  "WHERE g.id_grupo=md.id_grupo and md.id_modulo=%d and md.id_grupo is NOT NULL and g.ind_activo IS true " .
									  "ORDER BY 2",
									  $modData[0]);

					//echo $sql_SP;
					$rsGrp = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					$grpsMod = array();
					if(!$rsGrp)
						print $this->conn->ErrorMsg();
					else{
						while($grpData = $rsGrp->FetchRow())
							$grpsMod[] = array('igrp' => $grpData[0],
											   'cgrp' => $grpData[1],
											   'read' => $grpData[2],
											   'inse' => $grpData[3],
											   'modi' => $grpData[4]);
						$rsGrp->Close();
					}
					unset($rsGrp);
					
					// Registra el Modulo
					$html->append('modulo',	array('id' => $modData[0],
												  'name' => $modData[1],
												  'stat' => $modData[2],
												  'prof' => $modData[3],
												  'tMod' => $modData[5],
												  'usrs' => $usrsMod,
												  'grps' => $grpsMod));
				}
				$rsMod->Close();
			}
			unset($rsMod);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario
		$html->assign('frmName','frmCSV');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('ind_mod',$_SESSION['mod_ind_modificar']);
		
		// Setea datos del Formulario para Modificacion de Grupos y Usuarios
		$html->assign('frmDerUrl', $this->frmDerUrl);
		$html->assign('accionDer', $this->accionDer);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion['BUSCA_MODULO'], true));

		// Muestra el Resultado de la Busqueda
		$html->display('admin/modulos/searchResult.tpl.php');
		$html->display('admin/modulos/footerArm.tpl.php');	
	}
	
	function FrmManipulaModulo($id=NULL,$desTitCor=NULL,$desTitLar=NULL,$idModTipo=NULL,$idModPadre=NULL,$desModulo=NULL,$desImagen=NULL,
							   $desIcono=NULL,$numOrden=NULL,$indActivo=NULL,$desPath=NULL,$idTarget=NULL,$desAtribTarget=NULL,$idDep=NULL,$idSDep=NULL,$users=NULL,$reLoad=false,$errors=null){
		// Genera Objeto HTML
		$html = new Smarty;

		$this->abreConnDB();
		// $this->conn->debug = true;

		if($id&&!$reLoad){
			// Obtiene los Datos del Proyecto
			$sql_SP = sprintf("SELECT des_titulocorto, des_titulolargo, id_modulotipo, id_modulopadre, des_modulo, img_modulo ".
									 ", ico_modulo, ord_modulo, ind_activo, des_path, des_target, des_atributostarget ".
									 ", usr_audit, to_char(fec_creacion,'DD/MM/YYYY HH24:MI:SS'), to_char(fec_modificacion,'DD/MM/YYYY HH24:MI:SS') ".
							  "FROM modulo ".
							  "WHERE id_modulo=%d",
							  $this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					// Setea Campos de los Datos Ingresados por el Usuario
					$idModTipo = $row[2];
					$idModPadre = $row[3];
					$idTarget = $row[10];
					$html->assign('mod', array('id'=>$id,
											   'tCor'=>$row[0],
											   'tLar'=>$row[1],
											   'desc'=>$row[4],
											   'img'=>$row[5],
											   'ico'=>$row[6],
											   'nOrd'=>$row[7],
											   'iAct'=>$row[8],
											   'path'=>$row[9],
											   'aTar'=>$row[11],
											   'uMod'=>$row[12],
											   'fCre'=>$row[13],
											   'fMod'=>$row[14]));
				}
				$rs->Close();
			}
			unset($rs);
		}elseif($id){
			// Obtiene los Datos del Proyecto
			$sql_SP = sprintf("SELECT usr_audit, to_char(fec_creacion,'DD/MM/YYYY HH24:MI:SS'), to_char(fec_modificacion,'DD/MM/YYYY HH24:MI:SS') ".
							  "FROM modulo ".
							  "WHERE id_modulo=%d",
							  $this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					// Setea Campos de los Datos Ingresados por el Usuario
					$html->assign('mod', array('id'=>$id,
											   'tCor'=>$desTitCor,
											   'tLar'=>$desTitLar,
											   'desc'=>$desModulo,
											   'img'=>$desImagen,
											   'ico'=>$desIcono,
											   'nOrd'=>$numOrden,
											   'iAct'=>$indActivo,
											   'path'=>$desPath,
											   'aTar'=>$desAtribTarget,
											   'uMod'=>$row[0],
											   'fCre'=>$row[1],
											   'fMod'=>$row[2]));
				}
				$rs->Close();
			}
			unset($rs);
		}else{
			// Setea Campos de los Datos Ingresados por el Usuario
			$html->assign('mod', array('id'=>$id,
									   'tCor'=>$desTitCor,
									   'tLar'=>$desTitLar,
									   'desc'=>$desModulo,
									   'img'=>$desImagen,
									   'ico'=>$desIcono,
									   'nOrd'=>$numOrden,
									   'iAct'=>$indActivo,
									   'path'=>$desPath,
									   'aTar'=>$desAtribTarget));
		}
		
		// Genera el Menu Pager de la Cabecera
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddModule';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		// A�ade JavaScript para el envio del Formulario
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Contenido Select Modulo Tipo
		$sql_st = "SELECT id_modulotipo, des_modulotipo ".
				  "FROM modulo_tipo ".
				  "WHERE ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selModTipo',$this->ObjFrmSelect($sql_st, $idModTipo, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Contenido Select Modulo Padre
		$sql_st = sprintf("SELECT id_modulo, lpad(des_titulocorto, ((num_profundidad*4)*6)+length(des_titulocorto), '&nbsp;')  ".
						  "FROM modulo ".
						  "WHERE ind_activo IS TRUE and id_modulotipo=%d ".
						  "ORDER BY des_raiz || text(id_modulo)",
						  $this->PrepareParamSQL($idModTipo));
		$html->assign_by_ref('selModPadre',$this->ObjFrmSelect($sql_st, $idModPadre, true, false, array('val'=>'none','label'=>'No tiene padre')));
		unset($sql_st);

		// Contenido Select Target
		$sql_st = "SELECT cod_target, des_target ".
				  "FROM html_target ".
				  "WHERE ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTarget',$this->ObjFrmSelect($sql_st, $idTarget, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$html->assign_by_ref('selDep',$this->ObjFrmSelDep($idDep,true,true,array('val'=>'none','label'=>'Ninguna en Particular')));
		$html->assign_by_ref('jscriptSubDep',$this->JSsubDep($frmName,$idSDep,'none','idSDep','idDep'));
		$html->assign_by_ref('idSDep',$idSDep);

		$sql_st1 =  sprintf("SELECT u.cod_usuario, u.cod_usuario || CASE WHEN t.id_trabajador IS NOT NULL THEN ' ('||initcap(substr(t.des_apellidos,1,(CASE WHEN strpos(t.des_apellidos,' ')>0 THEN strpos(t.des_apellidos,' ')-1 ELSE length(t.des_apellidos) END)))||', '||initcap(substr(t.des_nombres,1,(CASE WHEN strpos(t.des_nombres,' ')>0 THEN strpos(t.des_nombres,' ')-1 ELSE length(t.des_nombres) END)))||')' ELSE '' END ".
							"FROM usuario u LEFT JOIN trabajador t ON u.cod_usuario=t.cod_trabajador and ind_activo is true ".
							"%s ".
							"ORDER BY 2 ",
							($idSDep&&$idSDep!='none') ? 'WHERE id_subdependencia=' . $this->PrepareParamSQL($idSDep) : '');
		$sql_st2 = null;
		if($id&&!$reLoad){
			$sql_st2 = sprintf("SELECT u.cod_usuario, u.cod_usuario || CASE WHEN t.id_trabajador IS NOT NULL THEN ' ('||initcap(substr(t.des_apellidos,1,(CASE WHEN strpos(t.des_apellidos,' ')>0 THEN strpos(t.des_apellidos,' ')-1 ELSE length(t.des_apellidos) END)))||', '||initcap(substr(t.des_nombres,1,(CASE WHEN strpos(t.des_nombres,' ')>0 THEN strpos(t.des_nombres,' ')-1 ELSE length(t.des_nombres) END)))||')' ELSE '' END ".
								"FROM modulo_derecho md, usuario u LEFT JOIN trabajador t ON u.cod_usuario=t.cod_trabajador ".
								"WHERE md.cod_usuario=u.cod_usuario and md.id_modulo=%d %s ".
								"ORDER BY 2 ",
								$this->PrepareParamSQL($id),
								($idSDep&&$idSDep!='none') ? 'and id_subdependencia=' . $this->PrepareParamSQL($idSDep) : '');
		}else{
			$sql_st2 =  sprintf("SELECT u.cod_usuario, u.cod_usuario || CASE WHEN t.id_trabajador IS NOT NULL THEN ' ('||initcap(substr(t.des_apellidos,1,(CASE WHEN strpos(t.des_apellidos,' ')>0 THEN strpos(t.des_apellidos,' ')-1 ELSE length(t.des_apellidos) END)))||', '||initcap(substr(t.des_nombres,1,(CASE WHEN strpos(t.des_nombres,' ')>0 THEN strpos(t.des_nombres,' ')-1 ELSE length(t.des_nombres) END)))||')' ELSE '' END ".
								"FROM usuario u LEFT JOIN trabajador t ON u.cod_usuario=t.cod_trabajador ".
								"WHERE %s ".
								"ORDER BY 2 ",
								(count($users)>0) ? "cod_usuario in ('" . implode("','", $users) . "')" : "1=2");
		}
		$html->assign_by_ref('objItemsChange',$this->ObjFrmSelItemChange($sql_st1,$sql_st2,array('usuarios','users'),array('Usuarios','Usuarios con Derechos'),10,'ipsel1',$frmName,array('val'=>'','label'=>'-----------------------------'),true,false));
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('admin/modulos/headerArm.tpl.php');
		$html->display('admin/modulos/frmHandle.tpl.php');
		$html->display('admin/modulos/footerArm.tpl.php');
	}
	
	function AgregaModulo($desTitCor,$desTitLar,$idModTipo,$idModPadre,$desModulo,$desImagen,
						  $desIcono,$numOrden,$indActivo,$desPath,$idTarget,$desAtribTarget,$users){
						  
		// Comprueba Valores	
		$bTit = $bMTi = $bAct = $bPat = $bPat = $bTar = false;
		if(!$desTitCor){ $bTit = true; $this->errors .= 'El Titulo Corto debe ser especificado<br>'; }
		if(!$idModTipo||$idModTipo=='none'){ $bMTi = true; $this->errors .= 'El Tipo de Modulo debe ser especificado<br>'; }
		if(!$indActivo){ $bAct = true; $this->errors .= 'El indicador de estado debe ser especificado<br>'; }
		if(!$desPath){ $bPat = true; $this->errors .= 'El Path debe ser especificado<br>'; }
		if(!$idTarget||$idTarget=='none'){ $bTar = true; $this->errors .= 'El Target debe ser especificado<br>'; }

		if($bTit||$bMTi||$bAct||$bPat||$bTar){
			$objIntranet = new Intranet();
			$objIntranet->Header('Administraci�n de Modulos',false,array('suspEmb'));
			$objIntranet->Body('modulosadmin_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FrmManipulaModulo(null,$desTitCor,$desTitLar,$idModTipo,$idModPadre,$desModulo,$desImagen,
							   $desIcono,$numOrden,$indActivo,$desPath,$idTarget,$desAtribTarget,null,null,$users,true,$errors);
			
			$objIntranet->Footer();
		}else{
		
			$desRaiz = '/';
			$numProf = 0;
			
			if(!is_null($idModPadre)&&!empty($idModPadre)&&$idModPadre!='none')
				$this->obtieneRaizModulo($idModPadre,$desRaiz,$numProf);
		
			// Selecciona el ID del Modulo a Insertar
			$sql_st = "SELECT nextval('modulo_id_seq'::text)";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				if($row = $rs->FetchRow())
					$id=$row[0];
				$rs->Close();
			}
	
			$ins_st = sprintf("INSERT INTO modulo(id_modulo,des_titulocorto,des_titulolargo,id_modulotipo,id_modulopadre,des_modulo,img_modulo".
										 ",ico_modulo,ord_modulo,ind_activo,des_path,des_target,des_atributostarget,des_raiz,num_profundidad,usr_audit) ".
							  "VALUES(%d,'%s',%s,%d,%s,%s,%s,%s,%d,'%s','%s','%s',%s,'%s',%d,'%s')",
							  $this->PrepareParamSQL($id),
							  $this->PrepareParamSQL($desTitCor),
							  ($desTitLar) ? "'" . $this->PrepareParamSQL($desTitLar) ."'" : 'NULL',
							  $this->PrepareParamSQL($idModTipo),
							  ($idModPadre) ? "'" . $this->PrepareParamSQL($idModPadre) ."'" : 'NULL',
							  ($desModulo) ? "'" . $this->PrepareParamSQL($desModulo) ."'" : 'NULL',
							  ($desImagen) ? "'" . $this->PrepareParamSQL($desImagen) ."'" : 'NULL',
							  ($desIcono) ? "'" . $this->PrepareParamSQL($desIcono) ."'" : 'NULL',
							  ($numOrden) ? "'" . $this->PrepareParamSQL($numOrden) ."'" : 'NULL',
							  $this->PrepareParamSQL($indActivo),
							  $this->PrepareParamSQL($desPath),
							  $this->PrepareParamSQL($idTarget),
							  ($desAtribTarget) ? "'" . $this->PrepareParamSQL($desAtribTarget) ."'" : 'NULL',
							  $this->PrepareParamSQL($desRaiz),
							  $this->PrepareParamSQL($numProf),
							  $this->PrepareParamSQL($_SESSION['cod_usuario']));						  
			
			$this->abreConnDB();
			// $this->conn->debug = true;
			$this->conn->BeginTrans();
			
			$bSuccess = true;
	
			if ($this->conn->Execute($ins_st) === false) {
				print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
				$bSuccess = false;
			}else{
				unset($ins_st);
				if(count($users)>0){
					// Agrega los usuarios con derecho sobre el Modulo
					for($i=0; $i<count($users); $i++){
						$ins_st = sprintf("INSERT INTO modulo_derecho(id_modulo, cod_usuario, ind_leer) ".
										  "VALUES(%d,'%s',true)",
										  $this->PrepareParamSQL($id),
										  $this->PrepareParamSQL($users[$i]));
						if ($this->conn->Execute($ins_st) === false) {
							print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
							$bSuccess = false;
							break;
						}
						unset($ins_st);
					}
				}
			}
			if($bSuccess)
				$this->conn->CommitTrans();
			else
				$this->conn->RollbackTrans();
			
			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($bSuccess) ? $this->arr_accion['BUENA_TRANS'] : $this->arr_accion['MALA_TRANS'];
			$destination .= "&menu=";
			$destination .= $this->menu_items[1]['val'];

			header("Location: $destination");
			exit;

		}
	}
	
	function ModificaModulo($id,$desTitCor,$desTitLar,$idModTipo,$idModPadre,$desModulo,$desImagen,
						  $desIcono,$numOrden,$indActivo,$desPath,$idTarget,$desAtribTarget,$users){
						  
		// Comprueba Valores	
		$bID = $bTit = $bMTi = $bAct = $bPat = $bPat = $bTar = false;
		if(!$id){ $bID = true; $this->errors .= 'El ID del modulo debe ser especificado<br>'; }
		if(!$desTitCor){ $bTit = true; $this->errors .= 'El Titulo Corto debe ser especificado<br>'; }
		if(!$idModTipo||$idModTipo=='none'){ $bMTi = true; $this->errors .= 'El Tipo de Modulo debe ser especificado<br>'; }
		if(!$indActivo){ $bAct = true; $this->errors .= 'El indicador de estado debe ser especificado<br>'; }
		if(!$desPath){ $bPat = true; $this->errors .= 'El Path debe ser especificado<br>'; }
		if(!$idTarget||$idTarget=='none'){ $bTar = true; $this->errors .= 'El Target debe ser especificado<br>'; }

		if($bID||$bTit||$bMTi||$bAct||$bPat||$bTar){
			$objIntranet = new Intranet();
			$objIntranet->Header('Administraci�n de Modulos',false,array('suspEmb'));
			$objIntranet->Body('modulosadmin_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FrmManipulaModulo($id,$desTitCor,$desTitLar,$idModTipo,$idModPadre,$desModulo,$desImagen,
							   $desIcono,$numOrden,$indActivo,$desPath,$idTarget,$desAtribTarget,null,null,$users,true,$errors);
			
			$objIntranet->Footer();
		}else{
		
			$desRaiz = '/';
			$numProf = 0;
			
			if(!is_null($idModPadre)&&!empty($idModPadre)&&$idModPadre!='none')
				$this->obtieneRaizModulo($idModPadre,$desRaiz,$numProf);
		
			$upd_st = sprintf("UPDATE modulo ".
							  "SET des_titulocorto='%s',des_titulolargo=%s,id_modulotipo=%d,id_modulopadre=%s,des_modulo=%s,img_modulo=%s".
								 ",ico_modulo=%s,ord_modulo=%d,ind_activo='%s',des_path='%s',des_target='%s',des_atributostarget=%s,des_raiz='%s',num_profundidad=%d,usr_audit='%s',fec_modificacion=now() ".
							  "WHERE id_modulo=%d",
							  $this->PrepareParamSQL($desTitCor),
							  ($desTitLar) ? "'" . $this->PrepareParamSQL($desTitLar) ."'" : 'NULL',
							  $this->PrepareParamSQL($idModTipo),
							  ($idModPadre&&$idModPadre!='none') ? "'" . $this->PrepareParamSQL($idModPadre) ."'" : 'NULL',
							  ($desModulo) ? "'" . $this->PrepareParamSQL($desModulo) ."'" : 'NULL',
							  ($desImagen) ? "'" . $this->PrepareParamSQL($desImagen) ."'" : 'NULL',
							  ($desIcono) ? "'" . $this->PrepareParamSQL($desIcono) ."'" : 'NULL',
							  ($numOrden) ? "'" . $this->PrepareParamSQL($numOrden) ."'" : 'NULL',
							  $this->PrepareParamSQL($indActivo),
							  $this->PrepareParamSQL($desPath),
							  $this->PrepareParamSQL($idTarget),
							  ($desAtribTarget) ? "'" . $this->PrepareParamSQL($desAtribTarget) ."'" : 'NULL',
							  $this->PrepareParamSQL($desRaiz),
							  $this->PrepareParamSQL($numProf),
							  $this->PrepareParamSQL($_SESSION['cod_usuario']),
  							  $this->PrepareParamSQL($id));
			
			$this->abreConnDB();
			// $this->conn->debug = true;
			$this->conn->BeginTrans();
			
			$bSuccess = true;
	
			if ($this->conn->Execute($upd_st) === false) {
				print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
				$bSuccess = false;
			}else{
				unset($upd_st);

				// Borra los usuarios q ya no tienen derecho sobre el Modulo
				$del_st = sprintf("DELETE FROM modulo_derecho ".
								  "WHERE id_modulo=%d and cod_usuario IS NOT NULL %s",
								  $this->PrepareParamSQL($id),
								  (count($users)>0) ? "and cod_usuario NOT IN ('". implode("','",$users) ."')" : "");
				if ($this->conn->Execute($del_st) === false) {
					print 'error borrando: '.$this->conn->ErrorMsg().'<BR>';
					$bSuccess = false;
				}else{
					unset($del_st);
					
					if(count($users)>0){
						// Selecciona los Usuarios q ya han sido Insertados anteriormente
						$sql_st = sprintf("SELECT cod_usuario ".
										  "FROM modulo_derecho ".
										  "WHERE id_modulo=%d and cod_usuario IS NOT NULL and cod_usuario IN ('%s')",
										  $this->PrepareParamSQL($id),
										  implode("','",$users));
						$rs = & $this->conn->Execute($sql_st);
						unset($sql_st);
						if (!$rs){
							print $this->conn->ErrorMsg();
						}else{
							while($row = $rs->FetchRow())
								$usersIns[] = $row[0];
							$rs->Close();
						}
						unset($rs);
						
						// Elimina los usuarios ya insertados del Array usuarios mandado desde el Form
						$users = (isset($usersIns)) ? array_diff($users,$usersIns) : $users;
				
						// Agrega los usuarios con derecho sobre el Modulo (Derecho de Lectura por Default)
						while(list($key,$user) = each($users)){
							$ins_st = sprintf("INSERT INTO modulo_derecho(id_modulo, cod_usuario, ind_leer) ".
											  "VALUES(%d,'%s',true)",
											  $this->PrepareParamSQL($id),
											  $this->PrepareParamSQL($user));
							if ($this->conn->Execute($ins_st) === false) {
								print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
								$bSuccess = false;
								break;
							}
							unset($ins_st);
						}
					}
				}
			}
			if($bSuccess)
				$this->conn->CommitTrans();
			else
				$this->conn->RollbackTrans();
			
			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($bSuccess) ? $this->arr_accion['BUENA_TRANS'] : $this->arr_accion['MALA_TRANS'];
			$destination .= "&menu=";
			$destination .= $this->menu_items[2]['val'];

			header("Location: $destination");
			exit;

		}
	}

}

?>
