<?
require_once('clasePapeletas.inc.php');

class PapeletasDependencia extends Papeletas{

	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* CLASE PAPELETAS DEPENDENCIA									 */
	/* ------------------------------------------------------------- */
	function PapeletasDependencia($menu){
		// Asigna Parametros de Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][13];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Asigna Valores al Array de Datos de Usuario
		$this->datosUsuarioMSSQL();

		// Acciones Permitidas en la Aplicacion
		$this->arr_accion = array(
							MUESTRA_SUMARIO => 'showSumary',
							MUESTRA_EN_CURSO => 'showEnCurso',
							FRM_CALIFICA => 'frmCalifica',
							CALIFICA => 'califica',
							FRM_BUSCA_PAPELETA => 'frmSearch',
							BUSCA_PAPELETA => 'searchPapeleta',
							FRM_REPORTE => 'frmReporte',
							GENERA_REPORTE => 'newReport',
							IMPRIMIR_PAPELETA => 'printPapeleta',
							MUESTRA_OBSERVACION => 'showObservacion',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'							
							);
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array (val => 'showSumary', label => 'SUMARIO'),
		                    1 => array (val => 'showEnCurso', label => 'CALIFICAR'),
							2 => array (val => 'frmSearch', label => 'BUSCAR'),
							3 => array (val => 'frmReporte', label => 'REPORTE')
							);
		
		// Accion Realizada
		$this->menuPager = $menu;
	}
	
	function CantidadPapeletaPorEstado($stat){
		$this->abreConnDB();
		// $this->conn->debug = true;
		$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.papeleta p, db_general.dbo.h_trabajador t ".
				  "WHERE t.codigo_trabajador=p.codigo_trabajador and t.coddep={$this->userIntranet[COD_DEP]} and ".
				  		"id_estado=$stat ".
				  		"--and fecha_inicio > getdate()";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$row = $rs->FetchRow();
			$rs->Close();
		}
		return $row[0];	
	}
	
	function MuestraSumario(){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('statPapeleta',$this->statPapeleta);
		
		$html->assign('nCurso',$this->CantidadPapeletaPorEstado($this->statPapeleta[EN_CURSO]));
		$html->assign('nAprobado',$this->CantidadPapeletaPorEstado($this->statPapeleta[APROBADO]));
		$html->assign('nDesaprobado',$this->CantidadPapeletaPorEstado($this->statPapeleta[DESAPROBADO]));
		
		$html->display('papeletas/dependencia/sumario.tpl.php');
	}
	
	function ListaPapeletasEnCurso(){
		$this->abreConnDB();
		
		$sql_st = "SELECT p.id_papeleta, UPPER(t.apellidos_trabajador) + ' ' + UPPER(t.nombres_trabajador), ".
						 "LOWER(m.desc_motivo_ausencia), ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) 
						 THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
					"WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
					"ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".
						 "id_estado ".
				  "FROM db_general.dbo.h_trabajador t, dbo.papeleta p, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE t.codigo_trabajador=p.codigo_trabajador and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default and ".
				  		"t.coddep={$this->userIntranet['COD_DEP']} and ".
						"p.id_estado={$this->statPapeleta[EN_CURSO]} --and fecha_inicio > getdate() ".
				  "ORDER BY fecha_inicio";
				  
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$cont = 0;
			while ($row = $rs->FetchRow()){
				$pap[$cont] = array('trabajador'=>$row[1],
									'motivo'=>ucfirst($row[2]),
									'fecha'=>$row[3]
									);
				if($row[4]==$this->statPapeleta[EN_CURSO]){
					$pap[$cont]['imgStat'] = 'stat_none.gif';
					$pap[$cont]['imgOpcion'] = 'ico_calificar.gif';
					$pap[$cont]['altOpcion'] = 'Calificar';
					$pap[$cont]['lnkOpcion'] = "{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[FRM_CALIFICA]}&id={$row[0]}&menu=showEnCurso";
				}
				$cont++;
			}
			$rs->Close();
		}
		return $pap;
	}
	
	function MuestraIndex(){
		$this->MuestraSumario();
	}
	
	function MuestraPapeletaPorCalificar(){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea Campos del Formulario
		$pap = & $this->ListaPapeletasEnCurso();
		$html->assign('nPap',(is_array($pap)) ? count($pap) : 0);
		$html->assign_by_ref('pap',$pap);
		$html->display('papeletas/dependencia/porCalificar.tpl.php');
	}

	function FrmCalificaPapeleta($idPap){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.apellidos_trabajador + ' ' + t.nombres_trabajador, d.dependencia, m.desc_motivo_ausencia, ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".		
						 "p.destino, p.asunto, CONVERT(varchar(10),p.fecha_envio,103)".
				  "FROM dbo.PAPELETA p, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE p.id_papeleta={$idPap} and t.codigo_trabajador=p.codigo_trabajador and t.coddep=d.codigo_dependencia and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				//$html->assign('menu',$this->GeneraMenuPager());
				//$html->assign_by_ref('menuPap',$this->menuPager);
				
				// Setea Caracteristicas en el Formulario
				$frmName = 'frmCalifica';
				$html->assign_by_ref('frmName',$frmName);
				$html->assign('frmUrl',$_SERVER['PHP_SELF']);
				$html->assign_by_ref('accion',$this->arr_accion);
				$html->assign_by_ref('statPap',$this->statPapeleta);
								
				// Setea Campos del Formulario
				$html->assign('id',$idPap);
				$html->assign('des_nombre',$row[0]);
				$html->assign('des_dependencia',$row[1]);
				$html->assign('des_motivo',$row[2]);
				$html->assign('fecha',$row[3]);
				$html->assign('des_destino',$row[4]);
				$html->assign('des_asunto',$row[5]);
				$html->assign('fec_envio',$row[6]);
				
				$html->display('papeletas/dependencia/frmCalifica.tpl.php');
			}
			$rs->Close();
		}
	
	}
	
	function CalificaPapeleta($idPap,$idEstado,$desObs){
		$this->abreConnDB();
		
		$upd_st = sprintf("EXECUTE dbo.CALIFICA_PAPELETA_ASISTENCIA %d,%d,'%s',%d",
						  $idPap,
						  $idEstado,
						  $this->PrepareParamSQL($desObs),
						  $this->userIntranet[CODIGO]);
		$rs = & $this->conn->Execute($upd_st);
		unset($upd_st);
		if (!$rs)
			print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if (!($statTrans = $rs->FetchRow())) {
				$errors = 'Error en proceso de inserción. Consulte con el <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">Administrador</a>';
				$rs->Close();
			}
			$rs->Close();
		}
		
		switch($statTrans[0]){
			case 'A':
				$errors = "- Error en Inserción de la Papeleta.<br>";
				break;
			default:
				$msg = "<b>La Papeleta se ha calificado de manera Correcta.<br>Un email ha sido enviado a la Oficina de Recursos Humanos, informando el permiso.</b>";
				break;
		}
		
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('msg',($errors) ? $this->muestraMensajeInfo($this->errors.$errors,true) : $this->muestraMensajeInfo($msg,true));
		
		$html->display('papeletas/dependencia/calificaStatus.tpl.php');
	}

	function FrmBuscaPapeleta($mesPap=NULL,$statPap=NULL,$pap=NULL){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea Campos del Formulario
		$html->assign('selMesPap',$this->ObjFrmMes(1, date('n'), true, ($mesPap) ? $mesPap : date('n'), true, array('val'=>'none','label'=>'Todos')));
		
		$sql_st = 'SELECT id_estado, des_estado FROM papeleta_estado WHERE ind_activo=1';
		$html->assign('selStatPap',$this->ObjFrmSelect($sql_st, $statPap, true, true, array('val'=>'none','label'=>'Cualquiera')));
		$html->assign('nPap',(is_array($pap)) ? count($pap) : 0);
		$html->assign_by_ref('pap',$pap);
		$html->display('papeletas/dependencia/frmBuscar.tpl.php');

	}
	
	function BuscaPapeleta($mesPap,$statPap,$actual=false){
		$this->abreConnDB();
		
		$sql_st = sprintf("SELECT p.id_papeleta, UPPER(m.desc_motivo_ausencia), ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".
						 "p.id_estado, e.des_estado, t.apellidos_trabajador+' '+t.nombres_trabajador ".
				  "FROM dbo.papeleta p, dbo.papeleta_estado e, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m, db_general.dbo.h_trabajador t ".
				  "WHERE p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default and p.codigo_trabajador=t.codigo_trabajador and t.coddep={$this->userIntranet['COD_DEP']} and ".
						"p.id_estado=e.id_estado and DATEPART(year,p.fecha_inicio) = DATEPART(year,getdate())".
						"%s%s%s ".
				  " ORDER BY p.fecha_inicio DESC",
				  ($statPap!='none') ?  " and p.id_estado={$statPap}" : '',
				  ($mesPap!='none'&&$mesPap) ? sprintf(" and DATEPART(month,p.fecha_inicio)=%d",$mesPap) : '',
				  ($actual) ? ' and p.fecha_inicio > getdate()' : '');
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$cont=0;
			while ($row = $rs->FetchRow()){
				$pap[$cont] = array('trabajador'=>$row[5],
									'motivo'=>$row[1],
							        'fecha'=>$row[2],
							        'estado'=>$row[4]
							);

							/*$str_pre=1;
							if($row[3]==1){ $str_pre=0; }
							if($row[3]==0){ $str_pre=0; }							
							if($row[3]==2){ $str_pre=0; }
							if($row[3]==3){ $str_pre=0; }
							
							echo '---'.$row[0].'---'.$row[3].'--'.$str_pre.'<br>';*/
							
				if($row[3]==$this->statPapeleta[EN_CURSO]){
					$pap[$cont]['imgStat'] = 'stat_none.gif';
					$pap[$cont]['imgOpcion'] = 'ico_calificar.gif';
					$pap[$cont]['altOpcion'] = 'Calificar';
					$pap[$cont]['lnkOpcion'] = "{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[FRM_CALIFICA]}&id={$row[0]}&menu=showEnCurso";
				}elseif($row[3]==2 || $row[3]==5){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}&prev=1','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

				}elseif($row[3]==4){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}&prev=0','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";
					
				}elseif($row[3]==$this->statPapeleta[DESAPROBADO]){
					$pap[$cont]['imgStat'] = 'stat_false.gif';
					$pap[$cont]['imgOpcion'] = 'ico_doc.gif';
					$pap[$cont]['altOpcion'] = 'Ver Observaciones';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[MUESTRA_OBSERVACION]}&id={$row[0]}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=220,height=150'); void('')";;
				}
				$cont++;
			}
			unset($row);
			$rs->Close();
			unset($rs);			
		}
		
		$this->FrmBuscaPapeleta($mesPap,$statPap,$pap);
	}
	
	function FrmReportePapeleta($fecInicio=NULL,$fecFin=NULL,$userRep=NULL,$tipRep=NULL){
		//Manipulacion de las Fechas de Ingreso de la Actividad (mm/dd/YYYY)
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');

		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('tipRep',$tipRep);
		
		// Setea Campos del Formulario
		$sql_st  = "SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
				   "FROM db_general.dbo.h_trabajador ".
				   "WHERE estado='ACTIVO' and coddep={$this->userIntranet[COD_DEP]} and condicion='NOMBRADO' ".
				   "ORDER BY 2";
		$html->assign('selUser',$this->ObjFrmSelect($sql_st, $userRep, true, true, array('val'=>'none','label'=>'Todos los Trabajadores')));
		unset($sql_st);
		$html->assign_by_ref('selMesRep',$this->ObjFrmMes(1, date('n'), true, ($mesRep) ? $mesRep : date('n'), true));
		
		// Setea los Campos de la Fecha/Hora de Inicio
		$html->assign('selMesIni',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true));
		$html->assign('selDiaIni',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true));
		$anyoIni = substr($fecInicio,6,4);
		$html->assign('selAnyoIni',$this->ObjFrmAnyo($anyoIni, $anyoIni+1, $anyoIni, true));
		// Setea Campos Fecha de Fin
		$html->assign('selMesFin',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true));
		$html->assign('selDiaFin',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true));
		$anyoFin = substr($fecFin,6,4);
		$html->assign('selAnyoFin',$this->ObjFrmAnyo($anyoFin, $anyoFin+1, $anyoFin, true));

		// Lista los Reportes Generados Anteriormente
		$sql_st = sprintf("EXEC listRepGenDep %d, %s, %d",
						  $this->PrepareParamSQL($this->userIntranet[COD_DEP]),
						  ($userRep!='none'&&$userRep) ? $this->PrepareParamSQL($userRep) : 'NULL',
						  $this->PrepareParamSQL($this->userIntranet[CODIGO]));
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$html->assign_by_ref('nRep',$rs->RecordCount());
			while ($row = $rs->FetchRow())
				$html->append('rep', array('fecha'=> sprintf("Reporte del %s al %s",$row[1],$row[2]),
										   'file'=> $row[0],
										   'fecCreacion'=> strftime("%A, %d de %B del %Y a las %H:%M %Ss",strtotime($row[3])),
										   'trabajador' => (trim($row[4])) ? ucwords($row[4]) : 'Todos los Trabajadores'));
			$rs->Close();
		}

		$html->display('papeletas/dependencia/frmReporte.tpl.php');
	}
	
	function GeneraReporteDependencia($fecInicio,$fecFin,$userRep,$tipRep){
		$userRep = ($userRep == 'none') ? false : $userRep;
		if($this->GeneraReportePapeleta($userRep,$this->userIntranet[COD_DEP],$fecInicio,$fecFin,2,$tipRep))
			$this->FrmReportePapeleta($fecInicio,$fecFin,$userRep,$tipRep);
	}
}
?>