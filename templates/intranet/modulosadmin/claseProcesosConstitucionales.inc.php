<?php
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class ProcesosConstitucionales extends Modulos{

    function ProcesosConstitucionales($menu){
	
	echo "Pagina no disponible";exit;
	
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
				
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_EXP => 'frmSearchNew',
							BUSCA_EXP => 'searchNew',
							FRM_COMPLETA_DOC => 'frmUpdateNew',
							COMPLETA_DOC => 'updateNew',
							FRM_ADDOBS_DOCDIR =>'frmAddObsDocDir',
							ADDOBS_DOCDIR =>'addObsDocDir',
							MUESTRA_DETALLE_DOCDIR => 'showDetailDocDir',
							IMPRIME_FLUJO =>'ImprimeFlujo',
							FRM_GENERAREPORTE=>'frmGeneraReporte',
							GENERAREPORTE=>'GeneraReporte',
							REPORTE1=>'Reporte1',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		$this->datosUsuarioMSSQL();
		//$_SESSION['coddep'] = $this->userIntranet['COD_DEP'];


		/*
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		*/
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		/*
		if($this->userIntranet['COD_DEP']!=18){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		/*if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}*/
		
		// Items del Menu Principal
		/*
		if($_SESSION['mod_ind_leer']) $this->menu_items[0] = array ( 'val' => 'frmSearchNew', label => 'BUSCAR' );
		if($_SESSION['mod_ind_insertar']) $this->menu_items[1] = array ( 'val' => 'frmAddNew', label => 'AGREGAR' );
		if($_SESSION['mod_ind_modificar']) $this->menu_items[2] = array ( 'val' => 'frmModifyNew', label => 'MODIFICAR' );
		*/
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchNew', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmUpdateNew', label => 'AGREGAR' ),
							//2 => array ( 'val' => 'frmAddObsDocDir', label => 'AVANCE'),
							2 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTE')
							);
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	
	function ValidaFechaNoticia($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/showStatTrans.inc.php');
		$html->display('pp/footerArm.tpl.php');
	}
	
	function FormBuscaDocumento($page=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroTD=NULL,$asunto=NULL,$observaciones=NULL,$fecIniDir=NULL,$fecFinDir=NULL,$search=false){
		global $materia2,$correlativo2,$exp,$exp2,$exp3;
		global $a;
		global $desFechaIni;
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir) ? $fecIniDir : "08/01/2005";
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir) ? $fecFinDir : date('m/d/Y');

		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('fecIniDir',$fecIniDir);		
		$html->assign_by_ref('materia2',$materia2);		
		$html->assign_by_ref('correlativo2',$correlativo2);		
		$html->assign_by_ref('a',$a);		
		$html->assign_by_ref('exp',$exp);		
		$html->assign_by_ref('exp2',$exp2);		
		$html->assign_by_ref('exp3',$exp3);
		$html->assign_by_ref('desFechaIni',$desFechaIni);		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecIniDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecFinDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/procesoConst/search.tpl.php');
		if(!$search) $html->display('pp/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		//echo "justomatrix is here";
		$this->FormBuscaDocumento();
	}

	function busper($dat,$j){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$codigoDependencia=15;
		$sql="select Upper(apellidos_trabajador)+' '+Upper(nombres_trabajador)
				from db_general.dbo.h_trabajador t,movimiento_documento md,movimiento_tratamiento mt,dirigido d
				where md.id_documento=$dat 
				and md.derivado=0 and md.id_dependencia_destino=". $codigoDependencia ."  
				and md.id_movimiento_documento=mt.id_movimiento
				and mt.derivado=0  
     			and d.id_movimiento_tratamiento=mt.id_movimiento_tratamiento and d.codigo_persona=t.codigo_trabajador
		        and d.id_dirigido=0
				and t.coddep=". $codigoDependencia ." "; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$persona=$rs->fields[0];
						$persona=strtoupper($persona);
						$rs->Close();
			}
			unset($rs);
		switch($j){
			case 1:
				return($persona);
				break;
		}
	}	

	function buscaOtrasRZ($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
		                  when sol.id_tipo_persona=2 then sol.razon_social end
				from db_general.dbo.persona sol,db_procuraduria.dbo.RZ_procuraduria rz
				where rz.id_proceso=$dat 
		        and rz.id_persona=sol.id
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function buscaMedCautelar($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,auditmod,103)+': '+descripcion
				from db_procuraduria.dbo.medida
				where id_proceso=$dat 
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function buscaEtapas($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,auditmod,103)+': '+descripcion
				from db_procuraduria.dbo.etapas
				where id_proceso=$dat 
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function BuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir){
		global $fecIniDir2,$fecFinDir2,$page,$materia2,$correlativo2,$exp,$exp2,$exp3;
		global $desFechaIni;
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir&&!strstr($fecIniDir,'none')) ? $fecIniDir : NULL;
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir&&!strstr($fecFinDir,'none')) ? $fecFinDir : NULL;
		if($fecIniDir2&&$fecFinDir2){
			$fecIniDir=$fecIniDir2;
			$fecFinDir=$fecFinDir2;
		}
		/*
		if($fecIniDir){$bFIng = $this->ValidaFechaProyecto($fecIniDir,'Inicio');}
		if($fecFinDir){ $bFSal = ($this->ValidaFechaProyecto($fecFinDir,'Fin')); }
		*/
		if ($fecIniDir&&$fecFinDir){

		$mes_ini=substr($fecIniDir,0,2);
		$mes_fin=substr($fecFinDir,0,2);
		$dia_ini=substr($fecIniDir,3,2);
		$dia_fin=substr($fecFinDir,3,2);
		$anyo_ini=substr($fecIniDir,6,4);
		$anyo_fin=substr($fecFinDir,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecIniDir, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFinDir, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "error en los meses";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "error en los dias";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
					}
			}
		}		

		}
		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
			//exit;
			$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);
			
		}else{
		
		//$this->abreConnDB();
		//$this->conn->debug = true;
		/*Inicio de la parte Original: No se tiene en cuenta que se va a realizar una consulta sin listado de documentos
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDocDir';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocDir($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,true);
		
		Fin de la parte Original*/
		/*Inicio de la prueba del Listado de Documentos*/
		// Setea datos del Formulario
		//Esta parte lo pongo para que se muestre un resultado de a lo m�s 20 resultados.
		//$this->numMaxResultsSearch = 20;
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,true);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);

		// Setea datos del Formulario
		$html->assign('frmName','frmSearchExtended');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array('tipDocumento'=>$tipDocumento,
									'tipBusqueda'=>$tipBusqueda,
									'nroTD'=>$nroTD,
									'asunto'=>$asunto,
									'observaciones'=>$observaciones,
									'procedimiento'=>$procedimiento,
									'fecIniDir'=>$fecIniDir,
									'fecFinDir'=>$fecFinDir,
									'indicativo'=>$indicativo,
									'siglasDep'=>$siglasDep,
									'tipodDoc'=>$tipodDoc,
									'page'=>$page,
									'materia2'=>$materia2,
									'correlativo2'=>$correlativo2,
									'exp'=>$exp,
									'exp2'=>$exp2,
									'exp3'=>$exp3,
									'desFechaIni'=>$desFechaIni
									));
		
		/*Fin de la prueba del Listado de Documentos*/
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$codigoDependencia=15;
		
		switch($tipDocumento){
				case 1:
					switch($tipBusqueda){
						case 1://Nuevos documentos externos recibidos de Otra Dependencia
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							//$condConsulta[] = "md.finalizado=0";
							$condConsulta[] = "d.id_documento=md.id_documento and d.id_tipo_documento=1 and d.id_persona=p.id ";
							break;
						case 6://Documentos Externos recibidos en la Dependencia provenientes de OTD
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[]="md.id_dependencia_origen=7";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Los documentos externos derivados de esta dependencia a otras dependencias
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) and id_oficio>0 ";
							$condConsulta[] = "md.id_documento not in (select id_documento from movimiento_documento where id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento group by id_documento) and id_dependencia_destino=" . $codigoDependencia ." group by id_documento)";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							break;
						case 9://Todos los documentos externos derivados a esta oficina
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[] = "d.ID_TIPO_DOCUMENTO=1 and d.id_estado_documento=1";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_oficio is NULL and id_dependencia_origen=7 and id_dependencia_destino=" . $codigoDependencia .") or (id_oficio>0 and id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_persona=p.id and d.id_documento=md.ID_DOCUMENTO ";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						case 4://Todos los Documentos Externos Finalizados en esta dependencia
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";							
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=1";
							break;
						}
					break;
		
				case 2:
					switch($tipBusqueda){
						case 1://Todos los Nuevos documentos derivados a la oficina//en este caso todos los derivados de OAD
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 ";
							//$condConsulta[] = "md.finalizado=0";
							break;
						case 6://Todos los Nuevos documentos derivados a la oficina//en este caso todos los derivados de OAD
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[]="md.id_dependencia_origen=7";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Todos los derivados de esta dependencia a otras dependencias
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) ";//esto es para obtener todos los derivados que se han realizado de esta dependencia a otra							
							$condConsulta[] = "md.id_movimiento_documento not in (select max(id_movimiento_documento) from movimiento_documento where id_oficio>0 AND id_dependencia_destino=" . $codigoDependencia ." group by id_documento) ";//esto es para garantizar de que no muestre los que nos hayan devuelto de una dependencia x a esta dependencia de nuevo							
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento in (1) ";
							break;
						case 9://Todos los documentos y expedientes
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[] = "d.ID_TIPO_DOCUMENTO=2 and d.id_estado_documento=1";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_oficio is NULL and id_dependencia_origen=7 and id_dependencia_destino=" . $codigoDependencia .") or (id_oficio>0 and id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_persona=p.id and d.id_documento=md.ID_DOCUMENTO";
							break;
						case 4://Todos los Nuevos documentos externos y expedientes finalizados en esta dependencia
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="db_tramite_documentario.dbo.movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id";
							$condConsulta[] = "md.finalizado=1";
							break;
						}
					break;
					
				}
		
		// Condicionales Segun ingreso del Usuario
		if(!empty($asunto)&&!is_null($asunto))
			$condConsulta[] = "Upper(d.asunto) LIKE Upper('%{$asunto}%')";
		if(!empty($observaciones)&&!is_null($observaciones))
			$condConsulta[] = "Upper(d.observaciones) LIKE Upper('%{$observaciones}%')";
		if(!empty($RazonSocial)&&!is_null($RazonSocial)&&$RazonSocial!='none')
			$condConsulta[] = "d.id_persona=$RazonSocial";
		if(!empty($RZ)&&!is_null($RZ))
			$condConsulta[] = "((Upper(p.razon_social) LIKE Upper('%{$RZ}%')) or (Upper(p.apellidos) LIKE Upper('%{$RZ}%')) or (Upper(p.nombres) LIKE Upper('%{$RZ}%')))";
		if($fecIniDir&&$fecFinDir){
			$condConsulta[] ="d.auditmod>=convert(datetime,'$fecIniDir',101) and d.auditmod<=dateadd(dd,1,convert(datetime,'$fecFinDir',101))";
			}
		if(!empty($nroTD)&&!is_null($nroTD))
			$condConsulta[] = "Upper(d.num_tram_documentario) LIKE Upper('%{$nroTD}%')";
		if(($materia2>=1&& $materia2<=4)||(!empty($correlativo2)&&!is_null($correlativo2))||(!empty($exp)&&!is_null($exp))||(!empty($exp2)&&!is_null($exp2))||(!empty($exp3)&&!is_null($exp3))||(!empty($desFechaIni)&&!is_null($desFechaIni))){
			$condTable[]="db_procuraduria.dbo.proceso p";
			$condConsulta[]="p.id_documento=d.id_documento";
			if($materia2>=1&& $materia2<=4){
				$condConsulta[]="p.materia=$materia2";
			}
			if(!empty($correlativo2)&&!is_null($correlativo2)){
				$condConsulta[] = "Upper(p.correlativo) LIKE Upper('%{$correlativo2}%')";
			}
			if(!empty($exp)&&!is_null($exp))
				$condConsulta[] = "Upper(p.exp1) LIKE Upper('%{$exp}%')";
			if(!empty($exp2)&&!is_null($exp2))
				$condConsulta[] = "Upper(p.exp2) LIKE Upper('%{$exp2}%')";
			if(!empty($exp3)&&!is_null($exp3))
				$condConsulta[] = "Upper(p.exp3) LIKE Upper('%{$exp3}%')";
			if(!empty($desFechaIni)&&!is_null($desFechaIni))
				$condConsulta[] = "p.fecVenc='$desFechaIni'";
			
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE DB_PROCURADURIA.dbo.sp_busIDDocCompleto %d,%s,%s,%s,0,0",
								$codigoDependencia,
								($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');

		//echo $sql_SP;
		//exit;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE  DB_PROCURADURIA.dbo.sp_busIDDocCompleto %d,%s,%s,%s,%s,%s",
								$codigoDependencia,
								($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);

			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){	
					// Obtiene Todos los Datos del documento
						$sql_SP = sprintf("EXECUTE  DB_PROCURADURIA.dbo.sp_busDatosDocCompleto %d,%d",$id[0],$codigoDependencia);

					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);

					if(!$rs)
						print $this->conn->ErrorMsg();
					else{						   
						while($DocumentoData = $rs->FetchRow())
							
							$html->append('arrDocDir', array('id' => $id[0],
														  'sol' => ucwords($DocumentoData[0]),
														  'nroTD' => $DocumentoData[1],
														  'fecing' => $DocumentoData[2],
														  'asunto' => ucfirst($DocumentoData[3]),
														  'oficio' => strtoupper($DocumentoData[4]),
														  'folios' => $DocumentoData[5],
														  'claseDocInt' => $DocumentoData[6],
														  'tip' => $tipBusqueda,
														  'per' => $this->busper($id[0],1),//Determina al trabajador que posee el Documento
														  'tipDoc' => $tipDocumento,
														  'existResol' => $DocumentoData[13],
														  'tipoResol' => $DocumentoData[12],
														  'obsfinal' => $DocumentoData[10],
														  'obsRev' => $DocumentoData[11],
														  'nivel1' => $DocumentoData[7],
														  'nivel2' => $DocumentoData[8],
														  'nivel3' => $DocumentoData[9],
														  'sePractCor' => $DocumentoData[15],
														  'seCreaNoti' => $DocumentoData[14],
														  'proc' => $DocumentoData[16],
														  'cor' => $DocumentoData[17],
														  'fecVenc' => $DocumentoData[18]
														  ));


														  
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		$html->assign_by_ref('dependenciaJ',$codigoDependencia);
		//$html->assign_by_ref('dire',$this->SearchPrivilegios($this->userIntranet['CODIGO']));
		//$html->assign_by_ref('nomDire',$this->SearchDirector($codigoDependencia));
		$trabajador=$this->userIntranet['APELLIDO']." ".$this->userIntranet['NOMBRE'];
		$html->assign_by_ref('trabajador',$trabajador);
		$siglaDep="PP";
		$html->assign_by_ref('siglaDep',$siglaDep);
		$var="&page={$page}&tipDocumento={$tipDocumento}&tipBusqueda={$tipBusqueda}&nroTD={$nroTD}&asunto={$asunto}&observaciones={$observaciones}&fecIniDir={$fecIniDir}&fecFinDir={$fecFinDir}&materia2={$materia2}&correlativo2={$correlativo2}";
		$html->assign_by_ref('var',$var);
		
		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		/*Prueba Justomatrix is back*/
		$html->assign_by_ref('codTrabajador',$this->userIntranet['CODIGO']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DOCDIR], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscar', $this->arr_accion[BUSCA_EXP], true));
		
		// Muestra el Resultado de la Busqueda
		$html->display('pp/procesoConst/searchResult.tpl.php');
		$html->display('pp/footerArm.tpl.php');
		}//fin del if($fecInicio&&$fecFin)
	}
	
	function FormCompletaDocumento($id,$naturaleza=NULL,$monto=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,
	                               $materia=NULL,$natu=NULL,$tipProc=NULL,$tipoResp=NULL,$juzgado=NULL,$exp=NULL,
								   $sala=NULL,$exp2=NULL,$corte=NULL,$exp3=NULL,$medida=NULL,$opcion2=NULL,$nombre=NULL,
								   $Busca=NULL,$RZ=NULL,$reLoad=false,$errors=NULL){
		global $nroOTD,$RZ;
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $med;
		global $etapa;
		global $idProceso;
		global $idCondNew,$idCond,$correlativo;
		global $a,$page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $codDistJud,$codSede;
		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		if($id){
			$this->abreConnDB();
			//$this->conn->Debug=true;
			
			$sql="select d.num_tram_documentario,case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
													   when sol.id_tipo_persona=2 then sol.razon_social end
					from db_tramite_documentario.dbo.documento d,db_general.dbo.persona sol
					where d.id_persona=sol.id and d.id_documento=$id
			";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				$RETVAL=0;
			else{
				$nroOTD=$rs->fields[0];
				$RZ=$rs->fields[1];
			}
			
		
		}
		
		if($id&&!$reLoad){
			$this->abreConnDB();
			
			$sql_st = sprintf("SELECT id,id_documento,id_naturaleza,monto,id_tipo_proceso,id_tipo_responsabilidad".
			   						 ",codigo_departamento,codigo_provincia,codigo_distrito".
									 ",materia,delito,juzgado,exp1,sala,exp2".
									 ",corte,exp3,codigo_distritojudicial,codigo_sede,fecVenc,correlativo ".
							  "FROM db_procuraduria.dbo.proceso ".
							  "WHERE id_documento=%d",$this->PrepareParamSQL($id));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($idProceso,$idDocumento,$naturaleza,$monto,$tipProc,
						 $tipoResp,$codDepa,$codProv,$codDist,$materia,$delito,
						 $juzgado,$exp,$sala,$exp2,$corte,$exp3,$codDistJud,$codSede,$desFechaIni,$correlativoBD) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
				
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddNew';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		if($idProceso>0){
		    //echo "holas justomatrix".$idProceso;
			$this->abreConnDB();
			//$this->conn->Debug=true;
			
			$sql="select id,descripcion,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108) 
			      from db_procuraduria.dbo.medida
						where id_proceso=$idProceso
				  ";
			//echo $sql;
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('med', array( 'id'=>$row[0],
												   'medida'=>$row[1],
												   'fecha'=>$row[2]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
			
			$sql="select id,descripcion,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108) 
			      from db_procuraduria.dbo.etapas
						where id_proceso=$idProceso
				  ";
			//echo $sql;
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('eta', array( 'id'=>$row[0],
												   'etapa'=>$row[1],
												   'fecha'=>$row[2]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
		}

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idProceso',$idProceso);
		$html->assign_by_ref('nroOTD',$nroOTD);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('naturaleza',$naturaleza);
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('natu',$natu);
		$html->assign_by_ref('juzgado',$juzgado);
		$html->assign_by_ref('exp',$exp);
		$html->assign_by_ref('sala',$sala);
		$html->assign_by_ref('exp2',$exp2);
		$html->assign_by_ref('corte',$corte);
		$html->assign_by_ref('exp3',$exp3);
		$html->assign_by_ref('medida',$medida);
		$html->assign_by_ref('opcion2',$opcion2);
		$html->assign_by_ref('nombre',$nombre);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('list',$list);
		$html->assign_by_ref('tel',$tel);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('reLoad',$reLoad);
		$html->assign_by_ref('idProceso',$idProceso);
		$html->assign_by_ref('etapa',$etapa);
		
		$html->assign_by_ref('idCond',$idCond);
		
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('opcion3',$opcion3);
		$html->assign_by_ref('correlativoBD',$correlativoBD);
		
		/*Para q retorne a la b�squeda*/
		/*$page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2*/
		$html->assign('datos',array('tipDocumento'=>$tipDocumento,
									'tipBusqueda'=>$tipBusqueda,
									'nroTD'=>$nroTD,
									'asunto'=>$asunto,
									'observaciones'=>$observaciones,
									'procedimiento'=>$procedimiento,
									'fecIniDir'=>$fecIniDir,
									'fecFinDir'=>$fecFinDir,
									'indicativo'=>$indicativo,
									'siglasDep'=>$siglasDep,
									'tipodDoc'=>$tipodDoc,
									'page'=>$page,
									'materia2'=>$materia2,
									'correlativo2'=>$correlativo2
									));
		/**/
				
		$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "ORDER BY 2";
		$html->assign_by_ref('selMateria',$this->ObjFrmSelect($sql_st, $materia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		if($materia>0){
			$sql="select ISNULL(MAX(contador),0)+1 from db_procuraduria.dbo.contador where id_materia=$materia
				  ";
			//echo $sql;
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
				$correlativo=$rs->fields[0];
				if($correlativo<10)
				   $correlativo="00".$correlativo;
				elseif($correlativo<100)
					$correlativo="0".$correlativo;
			}
		}
		
		if($materia==1){
			$correlativo="P-".$correlativo."-".date('Y');
		}elseif($materia==2){
			$correlativo="C-".$correlativo."-".date('Y');
		}elseif($materia==3){
			$correlativo="Const-".$correlativo."-".date('Y');
		}elseif($materia==4){
			$correlativo="Lab-".$correlativo."-".date('Y');
		}
		$html->assign_by_ref('correlativo',$correlativo);
		$html->assign_by_ref('materia',$materia);
			
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.naturaleza ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		$html->assign_by_ref('selNaturaleza',$this->ObjFrmSelect($sql_st, $naturaleza, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
			
			$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_proceso ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		$html->assign_by_ref('selTipoProceso',$this->ObjFrmSelect($sql_st, $tipProc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		if($materia==1){
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (2,4,5) ".
						  "ORDER BY 2";
		}else{
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (1,3) ".
						  "ORDER BY 2";
		}
		$html->assign_by_ref('selTipoResp',$this->ObjFrmSelect($sql_st, $tipoResp, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		if((!$Busca&&$nombre!="")){
		// Contenido Select del Asunto para DINSECOVI
		$sql_st = "SELECT id, case when id_tipo_persona=1 then apellidos+' '+nombres ".
				  "when id_tipo_persona=2 then lower(razon_social) end ".
				  "FROM db_general.dbo.persona ".
				  "WHERE (Upper(razon_social) like Upper('%$nombre%')) or (Upper(nombres) like Upper('%$nombre%')) or (Upper(apellidos) like Upper('%$nombre%'))".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RZ, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		/**/
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_distritojudicial, descripcion ".
				  "FROM db_procuraduria.dbo.distritojudicial ".
				  "ORDER BY 2";
		$html->assign('selDistJud',$this->ObjFrmSelect($sql_st, $codDistJud, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		/**/
		$sql_st = sprintf("SELECT codigo_sede, Lower(descripcion) ".
				  		  "FROM db_procuraduria.dbo.sede ".
				  		  "WHERE codigo_distritojudicial='%s' ".
				  		  "ORDER BY 2",(!is_null($codDistJud)||!empty($codDistJud)) ? $codDistJud : 'xx');
		$html->assign('selSede',$this->ObjFrmSelect($sql_st, $codSede, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		// Lista el Software ya Agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,case when id_tipo_persona=1 then apellidos+' '+nombres
										when id_tipo_persona=2 then lower(razon_social) end ,nro_documento
				  						FROM db_general.dbo.persona 
										where id=%d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'nroDoc' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/procesoConst/frmCompletaDoc.tpl.php');
		$html->display('pp/footerArm.tpl.php');
	
	}

	function CompletaDocumento($id,$naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ){
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $idCond,$correlativo;
		global $idProceso;
		global $etapa;
		
		global $page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $fecIniDir2,$fecFinDir2;
		global $codDistJud,$codSede;

		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos		
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		// Comprueba Valores	
		if(!$juzgado){ $bTit = true; $this->errors .= 'El nombre del Juzgado debe ser especificado<br>'; }

		if($bTit){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('tramite_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormCompletaDocumento($id,$naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			//$this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXEC  %s %d,%d,%d,'%s',%s,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d",
							  ($idProceso>0) ? "db_procuraduria.dbo.modProceso" : "db_procuraduria.dbo.insProceso",
							  $id,
							  $this->PrepareParamSQL($materia),
							  $this->PrepareParamSQL($naturaleza),
							  ($this->PrepareParamSQL($natu)) ? $this->PrepareParamSQL($natu) : "NULL",
							  ($this->PrepareParamSQL($monto)) ? $this->PrepareParamSQL($monto) : "",
							  $this->PrepareParamSQL($tipProc),
							  $this->PrepareParamSQL($tipoResp),
							  $this->PrepareParamSQL($codDepa),
							  $this->PrepareParamSQL($codProv),
							  $this->PrepareParamSQL($codDist),
							  $this->PrepareParamSQL($juzgado),
							  $this->PrepareParamSQL($exp),
							  ($this->PrepareParamSQL($sala)) ? $this->PrepareParamSQL($sala) : " ",
							  ($this->PrepareParamSQL($exp2)) ? $this->PrepareParamSQL($exp2) : " ",
							  ($this->PrepareParamSQL($corte)) ? $this->PrepareParamSQL($corte) : " ",
							  ($this->PrepareParamSQL($exp3)) ? $this->PrepareParamSQL($exp3) : " ",
							  $_SESSION['cod_usuario'],
							  $correlativo,
							  $this->PrepareParamSQL($codDistJud),
							  $this->PrepareParamSQL($codSede),
							  ($this->PrepareParamSQL($desFechaIni)) ? $this->PrepareParamSQL($desFechaIni) : "NULL",
							  $opcion3
							  );
			// echo $sql_FT;
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									if($row = $rs->FetchRow()){
										$RETVAL = $row[0];
										$idProc = $row[1];
									}else
										$RETVAL = 1;
									$rs->Close();
								}
								unset($rs);
								
						
				if(!$RETVAL&& !$idProceso){
					// Inserta los Software's correspondientes al CPU
					for($i=0;$i<count($idSoft);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insProcesoRZ %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft[$i]),
										  $_SESSION['cod_usuario'],
										  $this->PrepareParamSQL($idCond[$i])										  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}

				if(!$RETVAL&& $medida!=""){
					// Inserta las Medidas Cautelares correspondientes al proceso
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insMedida %d,'%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($medida),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}

				if(!$RETVAL&& $etapa!=""){
					// Inserta los Software's correspondientes al CPU
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insEtapa %d,'%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($etapa),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}

						
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();
			/*
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
			*/
			/**/
				if (!$RETVAL) {
					$a=1;
					$html = new Smarty;
					$html->assign_by_ref('a',$a);
					//echo "holas";
					$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['BUSCA_EXP'] . '&page=' . $page . '&tipDocumento=' . $tipDocumento . '&tipBusqueda=' . $tipBusqueda . '&a=' . $a . '&fecIniDir2=' . $fecIniDir . '&fecFinDir2=' . $fecFinDir. '&materia2=' . $materia2 . '&correlativo2=' . $correlativo2;
					//echo $destination;
					header('Location: ' . $destination);
				}else{ 
						$destination = $_SERVER['PHP_SELF'] . '?accion=';
						$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
						$destination .= "&menu={$this->menu_items[1]['val']}";
						//echo $destination;exit;
						header("Location: $destination");
						exit;
				}
			/**/
		}
	}
	
	function FormAgregaAvancesDir($idObsDir,$observaciones=NULL,$errors=false){
		if(empty($idObsDir)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		//$html->assign_by_ref('FechaActual',$this->FechaActual());
		//$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddObsDocDir';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('idObsDir',$idObsDir);
		$html->assign_by_ref('observaciones',$observaciones);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/procesoConst/frmAddObsDir.tpl.php');
		$html->display('oad/footerArm.tpl.php');			
	}
	
	function AgregaAvancesDir($idObsDir,$observaciones){
		// Comprueba Valores	
		if(!$observaciones){ $b1 = true; $this->errors .= 'El avance debe ser especificado<br>'; }

		if($b1){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaAvancesDir($idObsDir,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_procJudicial %d,'%s',%d,'%s'",
								$idObsDir,
							  $this->PrepareParamSQL($observaciones),
							  $this->userIntranet['COD_DEP'],
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
		}
	}
	
	function buscaAceptacion($dat){
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,audit_rec,103)+' '+convert(varchar,audit_rec,108)
		         from movimiento_documento
				 where id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento where id_documento=$dat and id_dependencia_destino=15 ) "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					$rec=$rs->fields[0];
					$rs->Close();//Agregado el 22/03/2005
				}
				unset($rs);//Agregado el 22/03/2005
		if($rec){$acepta=$rec;}
		else{$acepta="";}
		return($acepta);
	}

	function DetalleDocumentoDir($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql=sprintf("select d.id_documento,d.id_tipo_documento,d.num_tram_documentario,
		                     d.indicativo_oficio,case when d.id_tipo_documento=1 then d.asunto
							                          when d.id_tipo_documento=2 then tup.descripcion 
													  when d.id_tipo_documento=4 then d.asunto end,
							 convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
							 cdi.descripcion,dep1.siglas,dep2.siglas,
							 a.nivel1,a.nivel2,
							 a.nivel3,convert(varchar,a.auditmod,103),convert(varchar,a.auditmod,108),
							 fd.observaciones,convert(varchar,fd.auditmod,103),convert(varchar,fd.auditmod,108),a.usuario,fd.usuario,
							 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),md.avance,
							 d.observaciones,
							 case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	                              when sol.id_tipo_persona=2 then sol.razon_social end,sol.direccion,sol.email,sol.telefono
					from dbo.clase_documento_interno cdi,
					      documento d  left join db_general.dbo.persona sol on d.id_persona=sol.id
						               left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
									   left join almacen_documento a on d.id_documento=a.id_documento and a.coddep=".$this->userIntranet['COD_DEP']." 					
									   left join finaldoc fd on d.id_documento=fd.id_documento and fd.coddep=".$this->userIntranet['COD_DEP']." ,
						 movimiento_documento md INNER JOIN db_general.dbo.h_dependencia dep1 on md.id_dependencia_origen=dep1.codigo_dependencia
													 LEFT JOIN db_general.dbo.h_dependencia dep2 on md.id_dependencia_destino=dep2.codigo_dependencia
					  where d.id_clase_documento_interno=cdi.id_clase_documento_interno and
					  		d.id_documento=md.id_documento and 
					      d.id_documento=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$idTipoDoc=$rs->fields[1];
			$numTram=$rs->fields[2];
			$indicativo=$rs->fields[3];
			$asunto=$rs->fields[4];
			$fecRec=$rs->fields[5];
			$claseDoc=$rs->fields[6];
			$depOrigen=$rs->fields[7];
			//$depDestino=$this->busSiglasDocMult($id);
			$depDestino2=$rs->fields[8];
			$auditRecibido=$this->buscaAceptacion($id);
			$nivel1=$rs->fields[9];
			$nivel2=$rs->fields[10];
			$nivel3=$rs->fields[11];
			$fec2=$rs->fields[12];
			$hora2=$rs->fields[13];
			$fecha2=$fec2." ".$hora2;
			$observaciones=$rs->fields[14];
			$fec1=$rs->fields[15];
			$hora1=$rs->fields[16];
			$fecha1=$fec1." ".$hora1;
			$user=$rs->fields[17];
			$user2=$rs->fields[18];
			$fecDer=$rs->fields[19];
			$avance=$rs->fields[20];
			$obs=$rs->fields[21];
			$RazonSocial=$rs->fields[22];
			$direccion=$rs->fields[23];
			$email=$rs->fields[24];
			$telefono=$rs->fields[25];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idTipoDoc',$idTipoDoc);				  
		$html->assign_by_ref('numTram',$numTram);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('fecRec',$fecRec);			
		$html->assign_by_ref('claseDoc',$claseDoc);	  
		$html->assign_by_ref('depOrigen',$depOrigen);
		$html->assign_by_ref('depDestino',$depDestino);
		$html->assign_by_ref('depDestino2',$depDestino2);
		$html->assign_by_ref('auditRecibido',$auditRecibido);
		$html->assign_by_ref('nivel1',$nivel1);
		$html->assign_by_ref('nivel2',$nivel2);
		$html->assign_by_ref('nivel3',$nivel3);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('fecha2',$fecha2);
		$html->assign_by_ref('user',$user);
		$html->assign_by_ref('user2',$user2);	
		$html->assign_by_ref('fecDer',$fecDer);
		$html->assign_by_ref('avance',$avance);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('telefono',$telefono);
		$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
									convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
									convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
									dir.observaciones,mt.link,mt.observaciones
		                        from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
							where md.derivado=0 and md.id_dependencia_destino=%d 
							and d.id_documento=md.id_documento
							and mt.derivado=0
							and md.id_movimiento_documento=mt.id_movimiento
							and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
							and d.id_documento=%d",$this->userIntranet['COD_DEP'],$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('doc', array(//'id'=>$id,
										   'nombre'=>$row[0],
										   'avance'=>$row[1],
										   //'idOficio' => $this->bucaDetalleDocumentoDir($row[2]),
										   'diaEnvio'=>$row[3],
										   'horaEnvio'=>$row[4],
										   'diaRec'=>$row[5],
										   'horaRec'=>$row[6],
										   'obs'=>$row[7],
										   'link'=>$row[8],
										   'obsSecre'=>$row[9]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		
		
		if($idTipoDoc==4){
			$sql_st = sprintf("SELECT d.indicativo_oficio,d.asunto,d.observaciones,Upper(dep1.siglas),Upper(dep2.siglas),
		                          Upper(c.descripcion),convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  md.avance 
							FROM db_tramite_documentario.dbo.movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                                     left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                                 db_tramite_documentario.dbo.documento d, dbo.clase_documento_interno c
							WHERE md.id_documento=%d and md.id_oficio>0 and d.id_documento=md.id_oficio and d.id_clase_documento_interno=c.id_clase_documento_interno
						      order by d.id_documento  
							",
							$id);
		}else{
			$sql_st = sprintf("SELECT d.indicativo_oficio,d.asunto,d.observaciones,Upper(dep1.siglas),Upper(dep2.siglas),
		                          Upper(c.descripcion),convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  md.avance 
							FROM db_tramite_documentario.dbo.movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                                     left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                                 db_tramite_documentario.dbo.documento d, dbo.clase_documento_interno c
							WHERE md.id_documento=%d and md.id_oficio>0 and d.id_documento=md.id_oficio and d.id_clase_documento_interno=c.id_clase_documento_interno
						      order by d.id_documento  
							",
							$id);
		}
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('dir2',array('ind' => $row[0],
										   'cla' => $row[5],
										   'depo' => $row[3],
										   'depd' => $row[4],
										   'asu' => ucfirst($row[1]),
										   'obs' => ucfirst($row[2]),
										   'fDer' => $row[6],
										   'avance' => $row[7]
										   ));
			$rs->Close();
		}
		unset($rs);
		//Obtenemos los datos del Proceso Constitucianal asociado al Documento
		$sql_SP = sprintf("SELECT natu.descripcion,pro.monto,tiproc.descripcion,tr.descripcion,dist.descripcion,mat.descripcion
							FROM db_procuraduria.dbo.proceso pro,db_tramite_documentario.dbo.documento d,
							     db_procuraduria.dbo.sede sed,db_procuraduria.dbo.distritojudicial dist,
								 db_procuraduria.dbo.naturaleza natu,db_procuraduria.dbo.tipo_proceso tiproc,
								 db_procuraduria.dbo.tipo_responsabilidad tr,db_procuraduria.dbo.materia mat
							WHERE
							     d.id_documento=%d and d.id_documento=pro.id_documento and 
								 pro.codigo_distritojudicial=sed.codigo_distritojudicial and sed.codigo_sede=pro.codigo_sede and 
								 pro.codigo_distritojudicial=dist.codigo_distritojudicial and natu.id=pro.id_naturaleza and
								 pro.id_tipo_responsabilidad=tr.id and
								 pro.id_tipo_proceso=tiproc.id and mat.id=pro.materia
							",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{

				$naturaleza=$rs->fields[0];
				$monto=$rs->fields[1];										   
				$tipProceso=$rs->fields[2];
				$tipResponsabilidad=$rs->fields[3];
				$distrito=$rs->fields[4];
				$materia=$rs->fields[5];
				
				$html->assign_by_ref('naturaleza',$naturaleza);
				$html->assign_by_ref('monto',$monto);
				$html->assign_by_ref('tipProceso',$tipProceso);
				$html->assign_by_ref('tipResponsabilidad',$tipResponsabilidad);
				$html->assign_by_ref('distrito',$distrito);
				$html->assign_by_ref('materia',$materia);
		}
		unset($rs);
		
		$sql_SP = sprintf("SELECT med.descripcion,med.auditmod
					from db_procuraduria.dbo.proceso pro,db_procuraduria.dbo.medida med
					where pro.id_documento=%d and pro.id=med.id_proceso",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('med', array(//'id'=>$id,
										   'descripcion'=>$row[0],
										   'fecha'=>$row[1]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("SELECT et.descripcion,et.auditmod
					from db_procuraduria.dbo.proceso pro,db_procuraduria.dbo.etapas et
					where pro.id_documento=%d and pro.id=et.id_proceso",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('eta', array(//'id'=>$id,
										   'descripcion'=>$row[0],
										   'fecha'=>$row[1]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('pp/procesoConst/printDetallesDoc.tpl.php') : $html->display('pp/procesoConst/showDetallesDoc.tpl.php');
	}
	
 function ImprimeFlujo($id){
  $this->DetalleDocumentoDir($id, true);
 }
 
	function FormGeneraReporte($search2=false){
		global $tipReporte,$nrotramite;
		global $fecInicio,$fecFin;
		global $GrupoOpciones1;
		global $codSede,$codDistJud,$materia;
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('nrotramite',$nrotramite);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		
		$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "ORDER BY 2";
		$html->assign_by_ref('selMateria',$this->ObjFrmSelect($sql_st, $materia, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_distritojudicial, descripcion ".
				  "FROM db_procuraduria.dbo.distritojudicial ".
				  "ORDER BY 1";
		$html->assign('selDistJud',$this->ObjFrmSelect($sql_st, $codDistJud, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_sede, Lower(descripcion) ".
				  		  "FROM db_procuraduria.dbo.sede ".
				  		  "WHERE codigo_distritojudicial='%s' ".
				  		  "ORDER BY 2",(!is_null($codDistJud)||!empty($codDistJud)) ? $codDistJud : 'xx');
		$html->assign('selSede',$this->ObjFrmSelect($sql_st, $codSede, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecInicio,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
		//$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));		

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/procesoConst/reportes.tpl.php');
		if(!$search2) $html->display('pp/footerArm.tpl.php');
	}
	
	function GeneraReporte($tipReporte,$nrotramite,$fecInicio,$fecFin,$GrupoOpciones1,$codSede,$codDistJud,$materia){
	//$fecInicio= ($_POST['fecInicio']) ? $_POST['fecInicio'] : $_GET['fecInicio'];
		switch($GrupoOpciones1){
			case 1:
			$this->Reporte1($fecInicio,$fecFin,$materia);
			break;
			case 2:
			$this->Reporte2($fecInicio,$fecFin,$codSede,$codDistJud);
			break;
			case 3:
			$this->ListaRelacionExpedientes($codDistJud);
			break;
		}
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s  ",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename
									));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}
	
	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}

	function Reporte1($fecInicio,$fecFin,$materia){

		/**/$anio=date('Y');
		//Manipulacion de las Fechas
		$fecInicio = ($fecInicio!='//'&&$fecInicio&&!strstr($fecInicio,'none')) ? $fecInicio : NULL;
		$fecFin = ($fecFin!='//'&&$fecFin&&!strstr($fecFin,'none')) ? $fecFin : NULL;

		if($fecInicio){$bFIng = $this->ValidaFechaProyecto($fecInicio,'Inicio');}
		

		if($fecFin){ $bFSal = ($this->ValidaFechaProyecto($fecFin,'Fin')); }
		if ($fecInicio&&$fecFin){

		//echo $fecInicio."<br>";
		//echo $fecFin."<br>";
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecInicio, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFin, $regs2);
		
		

		
		if ($regs[3]>$regs2[3]){echo "<strong>ERROR EN LOS ANYOS</strong>";exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "<strong>ERROR EN LOS MESES</strong>";exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "<strong>ERROR EN LOS D�AS</strong>";exit;}
					}
			}
		}		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
		}else{
		
			// Genera HTML
			$html = new Smarty;
			
			$this->abreConnDB();
			$this->conn->debug = false;
			$html->assign_by_ref('fecInicio',$fecInicio);
			$html->assign_by_ref('fecFin',$fecFin);
			$fecInicio2=$regs[2].'/'.$regs[1].'/'.$regs[3];
			$fecFin2=$regs2[2].'/'.$regs2[1].'/'.$regs2[3];
			$html->assign_by_ref('fecFin2',$fecFin2);
			$html->assign_by_ref('fecInicio2',$fecInicio2);

			$sql_st=sprintf("select p.id,p.id_documento,nat.descripcion,p.monto,d.num_tram_documentario,
			                tp.descripcion,tr.descripcion,dist.descripcion,mat.descripcion,p.juzgado,
							p.exp1,p.sala,p.exp2,p.corte,p.exp3,convert(varchar,d.auditmod,103),
							case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
							     when sol.id_tipo_persona=2 then sol.razon_social end,p.correlativo 
					 from db_procuraduria.dbo.proceso p, db_procuraduria.dbo.naturaleza nat,
					      db_procuraduria.dbo.materia mat, db_procuraduria.dbo.tipo_proceso tp,
						  db_procuraduria.dbo.tipo_responsabilidad tr,db_tramite_documentario.dbo.documento d,
						  db_procuraduria.dbo.sede sed,db_procuraduria.dbo.distritojudicial dist,
						  db_general.dbo.persona sol
					 where d.id_documento=p.id_documento and 
					  		p.auditmod>=convert(datetime,'$fecInicio',101) and 
					  		p.auditmod<=dateadd(dd,1,convert(datetime,'$fecFin',101)) and
							p.id_naturaleza=nat.id and p.id_tipo_proceso=tp.id and p.id_tipo_responsabilidad=tr.id
					  and p.materia=mat.id and p.codigo_distritojudicial=dist.codigo_distritojudicial and p.codigo_sede=sed.codigo_sede
					  and p.codigo_distritojudicial=sed.codigo_distritojudicial 
					  and d.id_persona=sol.id %s
					  order by p.id desc 
					  ",($materia>0) ? " and p.materia=$materia " : " ");
					  
			//echo $sql_st; exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow())
					$html->append('list',array('id' => $row[0],
												'idDoc' => ucwords($row[1]),
												'natu' => $row[2],
												'monto' => $row[3],
												'numTram' => $row[4],
												'tipProc' => $row[5],
												'tipResp' => $row[6],
												'dist' => $row[7],
												'mat' => $row[8],
												'juz' => $row[9],
												'exp1' => $row[10],
												'sala' => $row[11],
												'exp2' => $row[12],
												'corte' => $row[13],
												'exp3' => $row[14],
												'fecRec' => $row[15],
												'razSoc' => $row[16],
												'otRazSoc' => $this->buscaOtrasRZ($row[0]),
												'medCautelar' => $this->buscaMedCautelar($row[0]),
												'etapas' => $this->buscaEtapas($row[0]),
												'cor' => $row[17]
												//'usuario' => $this->buscaRespExpediente($row[9])
												));
				$rs->Close();
			}
			
			unset($rs);
			
			//setlocale(LC_TIME,$this->_zonaHoraria);
			setlocale(LC_TIME,"spanish");
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$dependencia=($dependencia>0)? $this->buscaSiglasDependencia($dependencia) : $this->userIntranet['DEPENDENCIA'];
			//$html->assign_by_ref('dependencia',$dependencia);
			//$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
			//$html->assign_by_ref('a',$a);
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
			
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'list'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/ListadoProcesos.tpl.php'),true,$logo);//true para horizontal y false para vertical
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;
		}//fin del if($fecInicio,$fecFin)
		}
		/**/		
	}

	function Reporte2($fecInicio,$fecFin,$codSede,$codDistJud){

		/**/$anio=date('Y');
		//Manipulacion de las Fechas
		$fecInicio = ($fecInicio!='//'&&$fecInicio&&!strstr($fecInicio,'none')) ? $fecInicio : NULL;
		$fecFin = ($fecFin!='//'&&$fecFin&&!strstr($fecFin,'none')) ? $fecFin : NULL;

		if($fecInicio){$bFIng = $this->ValidaFechaProyecto($fecInicio,'Inicio');}
		

		if($fecFin){ $bFSal = ($this->ValidaFechaProyecto($fecFin,'Fin')); }
		if ($fecInicio&&$fecFin){

		//echo $fecInicio."<br>";
		//echo $fecFin."<br>";
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecInicio, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFin, $regs2);
		
		

		
		if ($regs[3]>$regs2[3]){echo "<strong>ERROR EN LOS ANYOS</strong>";exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "<strong>ERROR EN LOS MESES</strong>";exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "<strong>ERROR EN LOS D�AS</strong>";exit;}
					}
			}
		}		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
		}else{
		
			// Genera HTML
			$html = new Smarty;
			
			$this->abreConnDB();
			$this->conn->debug = false;
			$html->assign_by_ref('fecInicio',$fecInicio);
			$html->assign_by_ref('fecFin',$fecFin);
			$fecInicio2=$regs[2].'/'.$regs[1].'/'.$regs[3];
			$fecFin2=$regs2[2].'/'.$regs2[1].'/'.$regs2[3];
			$html->assign_by_ref('fecFin2',$fecFin2);
			$html->assign_by_ref('fecInicio2',$fecInicio2);

			$sql_st="select p.id,p.id_documento,nat.descripcion,p.monto,d.num_tram_documentario,
			                tp.descripcion,tr.descripcion,dist.descripcion,mat.descripcion,p.juzgado,
							p.exp1,p.sala,p.exp2,p.corte,p.exp3,convert(varchar,d.auditmod,103),
							case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
							     when sol.id_tipo_persona=2 then sol.razon_social end,p.correlativo 
					 from db_procuraduria.dbo.proceso p, db_procuraduria.dbo.naturaleza nat,
					      db_procuraduria.dbo.materia mat, db_procuraduria.dbo.tipo_proceso tp,
						  db_procuraduria.dbo.tipo_responsabilidad tr,db_tramite_documentario.dbo.documento d,
						  db_procuraduria.dbo.distritojudicial dist,db_procuraduria.dbo.sede sed,
						  db_general.dbo.persona sol
					 where d.id_documento=p.id_documento and 
					  		p.auditmod>=convert(datetime,'$fecInicio',101) and 
					  		p.auditmod<=dateadd(dd,1,convert(datetime,'$fecFin',101)) and
							p.id_naturaleza=nat.id and p.id_tipo_proceso=tp.id and p.id_tipo_responsabilidad=tr.id
					  and p.materia=mat.id and p.codigo_distritojudicial=dist.codigo_distritojudicial and p.codigo_sede=sed.codigo_sede
					  and p.codigo_distritojudicial=sed.codigo_distritojudicial
					  and d.id_persona=sol.id and p.codigo_sede='$codSede' and p.codigo_distritojudicial='$codDistJud'
					  order by p.id desc 
					  ";
					  
			//echo $sql_st; exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow())
					$html->append('list',array('id' => $row[0],
												'idDoc' => ucwords($row[1]),
												'natu' => $row[2],
												'monto' => $row[3],
												'numTram' => $row[4],
												'tipProc' => $row[5],
												'tipResp' => $row[6],
												'dist' => $row[7],
												'mat' => $row[8],
												'juz' => $row[9],
												'exp1' => $row[10],
												'sala' => $row[11],
												'exp2' => $row[12],
												'corte' => $row[13],
												'exp3' => $row[14],
												'fecRec' => $row[15],
												'razSoc' => $row[16],
												'otRazSoc' => $this->buscaOtrasRZ($row[0]),
												'medCautelar' => $this->buscaMedCautelar($row[0]),
												'etapas' => $this->buscaEtapas($row[0]),
												'cor' => $row[17]
												//'usuario' => $this->buscaRespExpediente($row[9])
												));
				$rs->Close();
			}
			
			unset($rs);
			
			//setlocale(LC_TIME,$this->_zonaHoraria);
			setlocale(LC_TIME,"spanish");
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$dependencia=($dependencia>0)? $this->buscaSiglasDependencia($dependencia) : $this->userIntranet['DEPENDENCIA'];
			//$html->assign_by_ref('dependencia',$dependencia);
			//$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
			//$html->assign_by_ref('a',$a);
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
			
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'list'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/ListadoProcesos.tpl.php'),true,$logo);//true para horizontal y false para vertical
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;
		}//fin del if($fecInicio,$fecFin)
		}
		/**/		
	}

	
	function ListaRelacionExpedientes($codDistJud){
		$this->abreConnDB();
		//$this->conn->debug = true;

		// Obtiene los Datos del Inventariado
		//$sql_SP = "select descripcion,codigo_distritojudicial+' '+descripcion from db_procuraduria.dbo.sede where codigo_distritojudicial='$codDistJud'";
		//$sql_SP = "select descripcion,codigo_sede+' '+descripcion from db_procuraduria.dbo.sede where codigo_distritojudicial='$codDistJud' ";
		/**/$sql_SP = "select sed.descripcion,sed.codigo_sede+' '+sed.descripcion
					  from db_procuraduria.dbo.sede sed
					  where sed.codigo_distritojudicial='$codDistJud' and sed.codigo_sede in (select codigo_sede from db_procuraduria.dbo.proceso)";
		/**/
		/*
		$sql_SP = "select sed.descripcion,sed.codigo_sede+' '+sed.descripcion 
		              from db_procuraduria.dbo.sede sed,db_procuraduria.dbo.proceso p 
					  where p.codigo_distritojudicial='$codDistJud' and p.codigo_distritojudicial=sed.codigo_distritojudicial ";
		*/
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			
			if($row = $rs->FetchRow()){
				$desDependencia = $row[0];
				$desTrabajador = $row[1];
			}
			/**/
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[1];
						$rs->MoveNext();
						}
						/*
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
						*/
			
			/**/
			// list(null,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
			//	 $idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran) = $rs->FetchRow();
			$rs->Close();
		}
		unset($rs);
		
		//echo "holas".$row[1];

		// Obtiene las Atenciones sobre el inventariado
		$sql_st = "select distinct sed.descripcion,p.id,p.correlativo,case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
		                                     when sol.id_tipo_persona=2 then sol.razon_social end,p.juzgado,p.exp1
						from db_procuraduria.dbo.proceso p,db_general.dbo.persona sol,
						      db_procuraduria.dbo.rz_procuraduria rz,db_procuraduria.dbo.sede sed
							  where p.id=rz.id_proceso and rz.id_persona=sol.id
							  and p.codigo_distritojudicial='$codDistJud' and p.codigo_sede=sed.codigo_sede 
							  and sed.codigo_distritojudicial='$codDistJud'
									 ";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			// Crea el Objeto HTML
			$html = & new Smarty;
			$cUsr = $cDep = $loop = 0;
			while ($row = $rs->FetchRow()){
				if($loop==0){
					$userTemp = $row[0];
					//$userTemp = $arregloP[0];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				if($userTemp!=$row[0]){
					$cUsr++;
					$userTemp = $row[0];
					//$userTemp = $arregloP[$cUsr];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				$ate['users'][$cUsr]['ates'][] = array(	'moti'=>ucfirst($row[2]),
														// 'trab'=>ucwords($row[2]),
														// 'depe'=>$row[3],
														'obse'=>ucfirst($row[3]),
														'fech'=>$row[4],
														'tota'=>$row[5]);
				$loop++;
			}
			$rs->Close();
						
			$html->assign_by_ref('ate',$ate);
			
			$html->assign_by_ref('pcDep',$desDependencia);
			$html->assign_by_ref('pcTrab',$desTrabajador);
			$html->assign_by_ref('pcName',$desNombre);
			$html->assign_by_ref('pcCod',$codInventariado);
			
			setlocale(LC_TIME, $this->zonaHoraria);
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$header = "holas";

			$logo = '/var/www/intranet/img/800x600/img.rep.logo.13.gif';			
			$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];			
			
			//$options = $options. " --footer D D D ";
			//$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports';
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			
			$filename = sprintf("ate%s%s", '-'.mktime(), '-'.$idCPU);
			
			// Genera Archivo PDF			
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/reportAten1.tpl.php'),false,$logo,$options);

			//$destination = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/reports/' . $filename . '.pdf';
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Location: $destination");
			exit;
			// $html->display('oti/inventario/pcs/reportes/reportAten1.tpl.php');
		}
	}
	
}
?>