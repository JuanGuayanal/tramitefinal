<?
include_once('adodb.inc.php');
require_once('Smarty.class.php');
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');
require_once('modulosadmin/claseGeneral.inc.php');
class AdministracionTramite extends General{

	var $userSITRADOC;  	// Contiene los datos MSSQL del Usuario que esta Utilizando la Intranet

	function datosPermisoSITRADOC(){
		$this->abreConnDB();
		
		$sql_st = "SELECT case when ind_resolucion='TRUE' then 't' else 'f' end, case when ind_correspondencia='TRUE' then 't' else 'f' end,
					      case when ind_finalizacion='TRUE' then 't' else 'f' end,case when ind_archivo='TRUE' then 't' else 'f' end,
						  case when ind_reasignacion='TRUE' then 't' else 'f' end,case when ind_anulacion='TRUE' then 't' else 'f' end,
						  case when ind_correccionDoc='TRUE' then 't' else 'f' end,case when administracion='TRUE' then 't' else 'f' end,
						  case when ind_docAntiguos='TRUE' then 't' else 'f' end
					 ".
				  "FROM db_general.dbo.h_trabajador t, dbo.permiso p ".
				  "WHERE t.email='{$_SESSION['cod_usuario']}' and t.codigo_trabajador=p.codigo_trabajador";
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$this->userSITRADOC = array(
									  'RESOLUCION' => $row[0],
									  'CORRESPONDENCIA' => $row[1],
									  'FINALIZACION' => $row[2],
									  'ARCHIVO' => $row[3],
									  'REASIGNACION' => $row[4],
									  'ANULACION' => $row[5],
									  'CORRECCIONDOC' => $row[6],
									  'ADMINISTRACION' => $row[7],
									  'DOCANTIGUOS' => $row[8]
									  );
			}else{
				$this->userSITRADOC = false;
				$this->errors .= "No ha sido registrado como usuario valido en el SITRADOC.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}
		


}

?>