<?php
//putenv("TDSDUMP=/var/log/SuspEmb.log");
//putenv("ORACLE_SID=PROD");
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';
include_once 'claseEmbarcacionSISESAT.inc.php';

class SuspendeEmbarcacion extends Modulos{

	var $id_orgres;
	var $sufiDocumento;
	var $motInfraccion = array(12,13,14,15,16,17,18);

    function SuspendeEmbarcacion($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Obtiene los Datos del Modulo Actual
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][5];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Obtiene los Datos del Usuario en la DB MSSQL
		$this->datosUsuarioMSSQL();
		
		
//		echo '----'.$this->userIntranet['CODIGO'].'----';
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		if($this->userIntranet['COD_DEP']!=13&&$this->userIntranet['COD_DEP']!=25&&
		$this->userIntranet['COD_DEP']!=24&&$this->userIntranet['COD_DEP']!=48
		&&$this->userIntranet['COD_DEP']!=53&&$this->userIntranet['COD_DEP']!=54
		){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspensionesPP_tit.gif');
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCAR => 'frmSearch',
							BUSCAR => 'search',
							LISTA_SUSPENSION => 'listSuspension',
							FRM_AGREGA_SUSPENSION => 'frmAddSuspension',
							AGREGA_SUSPENSION => 'addSuspension',
							FRM_MODIFICA_SUSPENSION => 'frmModSuspension',
							MODIFICA_SUSPENSION => 'ModificaSuspension',
							FRM_LEVANTA_SUSPENSION => 'frmRaiseSuspension',
							LEVANTA_SUSPENSION => 'raiseSuspension',
							FRM_MODIFICA_TRANSMISOR => 'frmModifyTranmisor',
							MODIFICA_TRANSMISOR => 'modifyTranmisor',
							BUENA_TRANS => 'googTrans',
							MALA_TRANS => 'badTrans'
							);

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearch', label => 'BUSCAR' ),
		          1 => array ( 'val' => 'frmAddSuspension', label => 'SUSPENDER' ),
							2 => array ( 'val' => 'frmRaiseSuspension', label => 'LEVANTAR SUSP.' ),
							3 => array ( 'val' => 'frmModSuspension', label => 'MODIFICAR SUSP.' )
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		switch($this->userIntranet['COD_DEP']){
			case 25:
				$this->sufiDocumento = '-SITRADOC/DIGSECOVI-Dsvs';
				break;
			case 53:
				$this->sufiDocumento = '-SITRADOC/DIGSECOVI-Dif';
				break;
			case 54:
				$this->sufiDocumento = '-SITRADOC/DIGSECOVI-Dsvs';
				break;						
			case 48:
				$this->sufiDocumento = '-SITRADOC/OGA-Oec';
				break;
		}
    }
	
	function ValidaFechaSuspension($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		//$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/showStatTrans.inc.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	}
	
	function FormBuscaEmbarcacion($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/search.tpl.php');
		if(!$search) $html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaEmbarcacion();
	}

	
	function BuscaEmbarcacion($page,$tipBusqueda,$desNombre){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaEmbarcacion($page,$tipBusqueda,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,NULL,NULL,0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL');
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,NULL,NULL,%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($embID = $rsId->FetchRow()){
					// Obtiene Todos los Datos de la Embarcacion
					$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$embID[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($embData = $rsData->FetchRow())
							$html->append('arrEmb', array('id' => $embData[0],
														  'desc' => $embData[1],
														  'matri' => $embData[2],
														  'trans' => $embData[3],
														  'ePes' => $embData[4],
														  'eZar' => $embData[5],
														  'nSus' =>	$embData[6]));
						$rsData->Close();
					}
					unset($rsData);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCAR], true));

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/embarcacionesPP/searchResult.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	}
	
	function ListaSuspencionEmbarcacion($idEmb){
		$idEmb = urldecode($idEmb);

		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaEmbarcacion(NULL,NULL,NULL,true);
		
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmListSuspension';
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
			
		// Ejecuta el conteo General
		$this->abreConnDB();
		// $this->conn->debug = true;

		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 ||$coddep_dig==54){ $coddep_dig=25; }

		$sql_SP = sprintf("EXECUTE sp_buscaSuspEmb_intra %d,%d",
							$this->PrepareParamSQL($idEmb),
							$coddep_dig);
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->assign('nombreEmb',$row[1]);
				$html->append('arrSus', array('id' => $row[0],
											  'dSus' => $row[2],
											  'mSus' => $row[3],
											  'fIni' => $row[4],
											  'fFin' => $row[5]));
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);		

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Muestra el Resultado de la Busqueda
		
		$html->display('dnepp/embarcacionesPP/listSuspension.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	}
	
	function FormAgregaSuspension($idEmb,$resolucion=NULL,$desInfraccion=NULL,$documento=NULL,$fecDocumento=NULL,$numDias=NULL,$cumplimiento=NULL,$fecCumplimiento=NULL,$fecIncursion=NULL,$desZona=NULL,$docArmador=NULL,$errors=NULL){
		if(!$idEmb){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if($this->userIntranet['COD_DEP']==25  || $this->userIntranet['COD_DEP']==53 || $this->userIntranet['COD_DEP']==54){
			$this->FormAgregaSuspensionDINSECOVI($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
		}elseif($this->userIntranet['COD_DEP']==48){
			$this->FormAgregaSuspensionOEC($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
		}
	}
	
	function FormAgregaSuspensionDINSECOVI($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors){
		$idEmb = urldecode($idEmb);
		//Manipulacion de las Fechas
		$fecDocumento = ($fecDocumento!='//'&&$fecDocumento) ? $fecDocumento : date('m/d/Y');
		$fecCumplimiento = ($fecCumplimiento!='//'&&$fecCumplimiento) ? $fecCumplimiento : date('m/d/Y');
		$fecIncursion = ($fecIncursion!='//'&&$fecIncursion) ? $fecIncursion : date('m/d/Y');

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddSuspension';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.documento.value=''\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
			
		// Obtiene los Datos de la Embarcaci�n
		$this->abreConnDB();
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",
							$this->PrepareParamSQL($idEmb));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$html->assign('nombreEmb',$row[1]);
				$html->assign('matriEmb',$row[2]);
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Setea Valores de los Campos del Formulario
		$html->assign_by_ref('id',$idEmb);
		
		// Motivos validos para activar la Infraccion
		$html->assign_by_ref('motInfrac',$this->motInfraccion);
		
		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
		// Motivo Suspension
		$sql_st = "SELECT id_motembxsusp, lower(desc_motembxsus), horsusp_motembxsus/24 ".
						  "FROM user_dnepp.motivo_embxsusp ".
						  "WHERE id_depcia={$coddep_dig} and estado_embxsusp=1 ".
						  "ORDER BY 2";
						  
		$html->assign_by_ref('selResolucion',$this->ObjFrmSelect($sql_st, $resolucion, true, true, array('val'=>'NULL','label'=>'Escoja una Resoluci�n'),1));
		unset($sql_st);
		$html->assign('selResNrela',1);
		$html->assign_by_ref('idResolucion',$resolucion);
		
		$html->assign_by_ref('desInfraccion',$desInfraccion);
		$html->assign_by_ref('documento',$documento);
		// Fecha Documento a DISEVIA
		$html->assign_by_ref('selMesDoc',$this->ObjFrmMes(1, 12, true, substr($fecDocumento,0,2), true));
		$html->assign_by_ref('selDiaDoc',$this->ObjFrmDia(1, 31, substr($fecDocumento,3,2), true));
		$html->assign_by_ref('selAnyoDoc',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecDocumento,6,4), true));
		
		$html->assign_by_ref('numDias',$numDias);

		$html->assign_by_ref('cumplimiento',$cumplimiento);
		// Fecha Documento Cumplimiento DISEVIA
		$html->assign_by_ref('selMesCump',$this->ObjFrmMes(1, 12, true, substr($fecCumplimiento,0,2), true));
		$html->assign_by_ref('selDiaCump',$this->ObjFrmDia(1, 31, substr($fecCumplimiento,3,2), true));
		$html->assign_by_ref('selAnyoCump',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecCumplimiento,6,4), true));
		
		// Setea los Campos de la Fecha de Incursi�n
		$html->assign_by_ref('selMesInc',$this->ObjFrmMes(1, 12, true, substr($fecIncursion,0,2), true));
		$html->assign_by_ref('selDiaInc',$this->ObjFrmDia(1, 31, substr($fecIncursion,3,2), true));
		$html->assign_by_ref('selAnyoInc',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecIncursion,6,4), true));
		
		$html->assign_by_ref('desZona',$desZona);
		$html->assign_by_ref('docArmador',$docArmador);
			
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/addSuspension.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	
	}

	function FormAgregaSuspensionOEC($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors){
		$idEmb = urldecode($idEmb);
		//Manipulacion de las Fechas
		$fecDocumento = ($fecDocumento!='//'&&$fecDocumento) ? $fecDocumento : date('m/d/Y');
		$fecCumplimiento = ($fecCumplimiento!='//'&&$fecCumplimiento) ? $fecCumplimiento : date('m/d/Y');
		$fecIncursion = ($fecIncursion!='//'&&$fecIncursion) ? $fecIncursion : date('m/d/Y');

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddSuspension';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.documento.value=''\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
			
		// Obtiene los Datos de la Embarcaci�n
		$this->abreConnDB();
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",
							$this->PrepareParamSQL($idEmb));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$html->assign('nombreEmb',$row[1]);
				$html->assign('matriEmb',$row[2]);
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Setea Valores de los Campos del Formulario
		$html->assign_by_ref('id',$idEmb);
		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
		// Motivo Suspension
		$sql_st = "SELECT id_motembxsusp, lower(desc_motembxsus), horsusp_motembxsus/24 ".
						  "FROM user_dnepp.motivo_embxsusp ".
						  "WHERE id_depcia={$coddep_dig} ".
						  "ORDER BY 2";
		$html->assign_by_ref('selResolucion',$this->ObjFrmSelect($sql_st, $resolucion, true, true, array('val'=>'NULL','label'=>'Escoja una Resoluci�n'),1));
		unset($sql_st);
		$html->assign('selResNrela',1);
		$html->assign_by_ref('idResolucion',$resolucion);
		
		$html->assign_by_ref('desInfraccion',$desInfraccion);
		$html->assign_by_ref('documento',$documento);
		// Fecha Documento a DICAPI
		$html->assign_by_ref('selMesDoc',$this->ObjFrmMes(1, 12, true, substr($fecDocumento,0,2), true));
		$html->assign_by_ref('selDiaDoc',$this->ObjFrmDia(1, 31, substr($fecDocumento,3,2), true));
		$html->assign_by_ref('selAnyoDoc',$this->ObjFrmAnyo(date('Y')-5, date('Y'), substr($fecDocumento,6,4), true));
		
		$html->assign_by_ref('numDias',$numDias);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/addSuspensionOEC.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	
	}

	function AgregaSuspension($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador){
		if($this->userIntranet['COD_DEP']==25 || $this->userIntranet['COD_DEP']==53 || $this->userIntranet['COD_DEP']==54)
			$this->AgregaSuspensionDep($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador);
		elseif($this->userIntranet['COD_DEP']==24)
			$this->AgregaSuspensionDep($idEmb,$documento);
		elseif($this->userIntranet['COD_DEP']==48)
			$this->AgregaSuspensionDep($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,NULL,NULL,NULL,NULL,NULL);
	}
	
	function AgregaSuspensionDep($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador){
		$idEmb = urldecode($idEmb);
		$documento = urldecode($documento);
		
		if(!$documento){ $bDoc = true; $this->errors .= 'El Documento debe ser especificado'; }
		$bFDoc = ($this->ValidaFechaSuspension($fecDocumento,'del Oficio'));
		if(empty($numDias)&&is_null($numDias)){ $bNDias = true; $this->errors .= 'El N� de Dias Susp. debe ser especificado'; }
		$bFCump = ($cumplimiento&&$this->ValidaFechaSuspension($fecCumplimiento,'Doc. Cumplimiento'));
		
		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
		
		$bFInc = ($coddep_dig==25&&$this->ValidaFechaSuspension($fecIncursion,'Incursi�n'));

		if($bDoc||$bFDoc||$bFCump||$bFInc||$bNDias){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);			
			$this->FormAgregaSuspension($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;

			$coddep_dig=$this->userIntranet['COD_DEP'];
			if($coddep_dig==53){ $strsigla_dig="-SITRADOC/DIGSECOVI-Dif'"; }
			if($coddep_dig==54){ $strsigla_dig="-SITRADOC/DIGSECOVI-Dsvs'"; }			
			if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
			
			$sql_SP = sprintf("EXECUTE sp_insSuspPPEmb_intra %d,%d,%s,'%s','%s',%d,%s,%s,%s,%s,%s,%d,%d,%d",
							  $this->PrepareParamSQL($idEmb),
							  $this->PrepareParamSQL($resolucion),
							  (in_array($resolucion,$this->motInfraccion)) ? "'".$this->PrepareParamSQL($desInfraccion)."'" : "NULL",
							  $this->PrepareParamSQL($documento).$this->sufiDocumento,
							  $this->PrepareParamSQL($fecDocumento),
							  $this->PrepareParamSQL($numDias*24),
							  ($tipDocumento==1&&$cumplimiento) ? "'".$this->PrepareParamSQL($cumplimiento)."'" : "NULL",
							  ($tipDocumento==1&&$cumplimiento) ? "'".$this->PrepareParamSQL($fecCumplimiento)."'" : "NULL",
							  ($fecIncursion) ? "'".$this->PrepareParamSQL($fecIncursion)."'" : "NULL",
							  ($desZona) ? "'".$this->PrepareParamSQL($desZona)."'" : "NULL",
							  ($docArmador) ? "'".$this->PrepareParamSQL($docArmador).$strsigla_dig : "NULL",
							  $this->PrepareParamSQL($this->userIntranet['CODIGO']),
							  $this->PrepareParamSQL($coddep_dig),
							  ($resolucion==1&&$numDias==0) ? 0 : 48);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $this->modInfo['PATH'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	
	//
	
		function FormModiSuspension($idEmb,$resolucion=NULL,$desInfraccion=NULL,$documento=NULL,$fecDocumento=NULL,$numDias=NULL,$cumplimiento=NULL,$fecCumplimiento=NULL,$fecIncursion=NULL,$desZona=NULL,$docArmador=NULL,$errors=NULL){
		if(!$idEmb){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if($this->userIntranet['COD_DEP']==25 || $this->userIntranet['COD_DEP']==53 || $this->userIntranet['COD_DEP']==54){
			$this->FormModiSuspensionDINSECOVI($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
		}elseif($this->userIntranet['COD_DEP']==48){
			$this->FormAgregaSuspensionOEC($idEmb,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
		}
	}
	
	function FormModiSuspensionDINSECOVI($id,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$nombreEmb,$matriEmb,$errors){
		$id = urldecode($id);
		//Manipulacion de las Fechas
		$fecDocumento = ($fecDocumento!='//'&&$fecDocumento) ? $fecDocumento : date('m/d/Y');
		$fecCumplimiento = ($fecCumplimiento!='//'&&$fecCumplimiento) ? $fecCumplimiento : date('m/d/Y');
		$fecIncursion = ($fecIncursion!='//'&&$fecIncursion) ? $fecIncursion : date('m/d/Y');
		//echo "xxx";
		$this->abreConnDB();
		//$this->conn->debug=true;
		$sql_SP="SELECT 	es.ID_MOTEMBXSUSP as MOTIVO, 
										es.INFRACCION_EMBXSUSP as INFRACCION , 
										es.DOCADISEVIA_EMBXSUSP as OFICIO_DISEVIA, 
										Convert(varchar(10),es.FECHDOCDISEVIA_EMBXSUSP,101)  as FECHA_OFICIO, 
										NULL as NRO_DIAS, 
										Convert(varchar(10),es.FECHAINCURSION_EMBXSUSP,101) as  FECHA_INCURSION, 
										es.ZONADEPESCA_EMBXSUSP as ZONA_PESCA, 
										es.OFICIOAARMADOR_EMBXSUSP as OFICIO_ARMADOR, 
										es.DOCCUMPDISEVIA_EMBXSUSP as DOC_CUMPLIMIENTO, 
										Convert(varchar(10),es.FECHFINSUSP_EMBXSUSP,101) as FECHA_CUMPLIMIENTO,
										emb.nombre_emb NOMBRE,
										emb.matricula_emb as MATRICULA
							FROM user_dnepp.embxsusp es,user_dnepp.embarcacionnac emb							
							WHERE es.ID_EMBXSUSP=$id and es.id_emb=emb.id_emb";
						
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$resolucion=$rs->fields[0];
			$desInfraccion=$rs->fields[1];
			$documento=$rs->fields[2];
			$fecDocumento=$rs->fields[3];
			$numDias=$rs->fields[4];
			$fecIncursion=$rs->fields[5];
			$desZona=$rs->fields[6];
			$docArmador=$rs->fields[7];
			$Cumplimiento=$rs->fields[8];
			$fecCumplimiento=$rs->fields[9];
			$nombreEmb=$rs->fields[10];
			$matriEmb=$rs->fields[11];		
			
						
			unset($row);
			$rs->Close();
		}
		unset($rs);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModSuspension';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.documento.value=''\r\ndocument.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
			
		// Obtiene los Datos de la Embarcaci�n
		$this->abreConnDB();
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",
							$this->PrepareParamSQL($idEmb));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$html->assign('nombreEmb',$row[1]);
				$html->assign('matriEmb',$row[2]);
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Setea Valores de los Campos del Formulario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nombreEmb',$nombreEmb);
		$html->assign_by_ref('matriEmb',$matriEmb);
		
		// Motivos validos para activar la Infraccion
		$html->assign_by_ref('motInfrac',$this->motInfraccion);

		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }		
		// Motivo Suspension
		$sql_st = "SELECT id_motembxsusp, lower(desc_motembxsus), horsusp_motembxsus/24 ".
						  "FROM user_dnepp.motivo_embxsusp ".
						  "WHERE id_depcia={$coddep_dig} and estado_embxsusp=1 ".
						  "ORDER BY 2";
						  
		$html->assign_by_ref('selResolucion',$this->ObjFrmSelect($sql_st, $resolucion, true, true, array('val'=>'NULL','label'=>'Escoja una Resoluci�n'),1));
		unset($sql_st);
		$html->assign('selResNrela',1);
		$html->assign_by_ref('idResolucion',$resolucion);
		
		$html->assign_by_ref('desInfraccion',$desInfraccion);
		$html->assign_by_ref('documento',$documento);
		// Fecha Documento a DISEVIA
		$html->assign_by_ref('selMesDoc',$this->ObjFrmMes(1, 12, true, substr($fecDocumento,0,2), true));
		$html->assign_by_ref('selDiaDoc',$this->ObjFrmDia(1, 31, substr($fecDocumento,3,2), true));
		$html->assign_by_ref('selAnyoDoc',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecDocumento,6,4), true));
		
		$html->assign_by_ref('numDias',$numDias);

		$html->assign_by_ref('cumplimiento',$cumplimiento);
		
		// Fecha Documento Cumplimiento DISEVIA
		$html->assign_by_ref('selMesCump',$this->ObjFrmMes(1, 12, true, substr($fecCumplimiento,0,2), true, array('val'=>'NULL','label'=>'Mes')));
		$html->assign_by_ref('selDiaCump',$this->ObjFrmDia(1, 31, substr($fecCumplimiento,3,2), true, array('val'=>'NULL','label'=>'D�a')));
		$html->assign_by_ref('selAnyoCump',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecCumplimiento,6,4), true, array('val'=>'NULL','label'=>'A�o')));
		
		
		
		// Setea los Campos de la Fecha de Incursi�n
		$html->assign_by_ref('selMesInc',$this->ObjFrmMes(1, 12, true, substr($fecIncursion,0,2), true));
		$html->assign_by_ref('selDiaInc',$this->ObjFrmDia(1, 31, substr($fecIncursion,3,2), true));
		$html->assign_by_ref('selAnyoInc',$this->ObjFrmAnyo(date('Y')-4, date('Y')+1, substr($fecIncursion,6,4), true));
		
		
		$html->assign_by_ref('desZona',$desZona);
		$html->assign_by_ref('docArmador',$docArmador);
			
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/ModSuspension.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	
	}
	
		function ModificaSuspension($id,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador){
		$id = urldecode($id);
		$documento = urldecode($documento);
		
		if(!$documento){ $bDoc = true; $this->errors .= 'El Documento debe ser especificado'; }
		$bFDoc = ($this->ValidaFechaSuspension($fecDocumento,'del Oficio'));
		if(empty($numDias)&&is_null($numDias)){ $bNDias = true; $this->errors .= 'El N� de Dias Susp. debe ser especificado'; }
		$bFCump = ($cumplimiento&&$this->ValidaFechaSuspension($fecCumplimiento,'Doc. Cumplimiento'));

		$coddep_dig=$this->userIntranet['COD_DEP'];
		if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
		
		$bFInc = ($coddep_dig==25&&$this->ValidaFechaSuspension($fecIncursion,'Incursi�n'));

		if($bDoc||$bFDoc||$bFCump||$bFInc||$bNDias){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);			
			$this->FormModiSuspensionDINSECOVI($id,$resolucion,$desInfraccion,$documento,$fecDocumento,$numDias,$cumplimiento,$fecCumplimiento,$fecIncursion,$desZona,$docArmador,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;

			$coddep_dig=$this->userIntranet['COD_DEP'];
			if($coddep_dig==53){ $strsigla_dig="-SITRADOC/DIGSECOVI-Dif'"; }
			if($coddep_dig==54){ $strsigla_dig="-SITRADOC/DIGSECOVI-Dsvs'"; }			
			if($coddep_dig==53 || $coddep_dig==54){ $coddep_dig=25; }
			
			$sql_SP = sprintf("EXECUTE sp_modSuspPPEmb_intra %d,%d,%s,'%s','%s',%d,%s,%s,%s,%s,%s,%d,%d,%d",
							  $this->PrepareParamSQL($id),
							  $this->PrepareParamSQL($resolucion),
							  (in_array($resolucion,$this->motInfraccion)) ? "'".$this->PrepareParamSQL($desInfraccion)."'" : "NULL",
							  $this->PrepareParamSQL($documento).$this->sufiDocumento,
							  $this->PrepareParamSQL($fecDocumento),
							  $this->PrepareParamSQL($numDias*24),
							  //($tipDocumento==1&&$cumplimiento) ? "'".$this->PrepareParamSQL($cumplimiento)."'" : "NULL",
							  ($cumplimiento) ? "'".$this->PrepareParamSQL($cumplimiento)."'" : "NULL",
							  //($tipDocumento==1&&$cumplimiento) ? "'".$this->PrepareParamSQL($fecCumplimiento)."'" : "NULL",
							  ($cumplimiento) ? "'".$this->PrepareParamSQL($fecCumplimiento)."'" : "NULL",
							  ($fecIncursion) ? "'".$this->PrepareParamSQL($fecIncursion)."'" : "NULL",
							  ($desZona) ? "'".$this->PrepareParamSQL($desZona)."'" : "NULL",
							  ($docArmador) ? "'".$this->PrepareParamSQL($docArmador).$strsigla_dig : "NULL",
							  $this->PrepareParamSQL($this->userIntranet['CODIGO']),
							  $this->PrepareParamSQL($coddep_dig),
							  ($resolucion==1&&$numDias==0) ? 0 : 48);
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		
			

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $this->modInfo['PATH'] . 'indexPP.php?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
		}
	}


	
	
	//
	
	
	
	
	
	
	function FormLevantaSuspension($idSusp,$fecLevante=NULL,$indNow=1,$desObservacion=NULL,$errors=NULL){
		if(!$idSusp){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if($this->userIntranet['COD_DEP']==25  || $this->userIntranet['COD_DEP']==53 || $this->userIntranet['COD_DEP']==54){
			$this->FormLevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion,$errors);
		}elseif($this->userIntranet['COD_DEP']==24){
			$this->FormLevantaSuspensionDNEPP($idSusp,$fecLevante,$indNow,$desObservacion,$errors);
		}elseif($this->userIntranet['COD_DEP']==48){
			$this->FormLevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion,$errors);
		}
	}

	function FormLevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion,$errors){
		$idSusp = urldecode($idSusp);
		$desObservacion = urldecode($desObservacion);
		//Manipulacion de las Fechas
		$fecLevante = ($fecLevante!='//'&&$fecLevante) ? $fecLevante : date('m/d/Y');

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmUpSusp';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		// $st_jscript = "document.{$frmName}.documento.value=''\r\ndocument.{$frmName}.accion.value=pAccion";
		// $html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
			
		// Obtiene los Datos de la Embarcaci�n
		$this->abreConnDB();
		$sql_SP = sprintf("EXECUTE sp_listaDatosSusp_intra %d",
							$this->PrepareParamSQL($idSusp));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$html->assign('susp',array('id' => $idSusp,
										   'emb' => $row[0],
										   'matr' => $row[1],
										   'dSus' => $row[2],
										   'fIni' => $row[3],
										   'fFin' => $row[4]));
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Setea Valores de los Campos del Formulario
		$html->assign_by_ref('indNow',$indNow);
		$html->assign_by_ref('desObservacion',$desObservacion);

		// Fecha Levante Suspension
		$html->assign_by_ref('selMesLev',$this->ObjFrmMes(1, 12, true, substr($fecLevante,0,2), true));
		$html->assign_by_ref('selDiaLev',$this->ObjFrmDia(1, 31, substr($fecLevante,3,2), true));
		$html->assign_by_ref('selAnyoLev',$this->ObjFrmAnyo(date('Y'), date('Y')+1, substr($fecLevante,6,4), true));
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('dnepp/embarcacionesPP/headerArm.tpl.php');
		$html->display('dnepp/embarcacionesPP/frmLevantaSusp.tpl.php');
		$html->display('dnepp/embarcacionesPP/footerArm.tpl.php');
	}

	function LevantaSuspension($idSusp,$fecLevante,$indNow,$desObservacion){
		if($this->userIntranet['COD_DEP']==25  || $this->userIntranet['COD_DEP']==53 || $this->userIntranet['COD_DEP']==54){
			$this->LevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion);
		}elseif($this->userIntranet['COD_DEP']==24){
			$this->LevantaSuspensionDNEPP($idSusp,$fecLevante,$indNow,$desObservacion);
		}elseif($this->userIntranet['COD_DEP']==48){
			$this->LevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion);
		}
	}
	
	function LevantaSuspensionDep($idSusp,$fecLevante,$indNow,$desObservacion){
		$idSusp = urldecode($idSusp);
		$desObservacion = urldecode($desObservacion);
		
		$bFLev = (!$indNow&&$this->ValidaFechaSuspension($fecLevante,'de Levante'));

		if($bFLev){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormLevantaSuspension($idSusp,$fecLevante,$indNow,$desObservacion,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			// $this->conn->debug = true;
			$sql_SP = sprintf("EXECUTE sp_levSuspPPEmb_intra %d,'%s',%s,%s,%d",
							  $this->PrepareParamSQL($idSusp),
							  $this->PrepareParamSQL($fecLevante),
							  ($indNow) ? $this->PrepareParamSQL($indNow) : 'NULL',
							  ($desObservacion) ? "'".$this->PrepareParamSQL($desObservacion)."'" : 'NULL',
							  $this->userIntranet['CODIGO']);
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $this->modInfo['PATH'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;

		}
	}
 
	function FormModificaTransmisor($idEmb,$idProveedor=NULL,$numTrans=NULL,$errors=false){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmChangeTrans';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
			
		// Obtiene los Datos de la Embarcaci�n
		$this->abreConnDB();
		$sql_SP = sprintf("EXECUTE sp_listaTransEmb_intra %d",
							$this->PrepareParamSQL($idEmb));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$idProveedor = $row[0];
				$numTrans = $row[1];
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);

		$html->assign_by_ref('idEmb',$idEmb);
		$html->assign_by_ref('numTrans', $numTrans);
		$html->assign_by_ref('idProveedor', $idProveedor);
		// Empresa Proveedora
		$sql_st = "SELECT id_proveedor, lower(des_proveedor) ".
				  "FROM webminpro.proveedor_sisesat ".
				  "ORDER BY 2";
		$html->assign_by_ref('selProveedor',$this->ObjFrmSelect($sql_st, $idProveedor, true, true, array('val'=>'none','label'=>'Escoja un Proveedor'),1));
		unset($sql_st);

		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/embarcacionesPP/frmChangeTrans.tpl.php');
	}
	
 	function ModificaTransmisor($idEmb,$idProveedor,$numTrans,$idAntProveedor=NULL){

		$idEmb = urldecode($idEmb);
		$idProveedor = urldecode($idProveedor);
		$numTrans = urldecode($numTrans);

		$this->abreConnDB();
	    //$this->conn->debug = true;

		// Inicia la Transaccion
		$this->conn->BeginTrans(); 

		$sql_SP = sprintf("EXECUTE sp_updTransEmb_intra %d,%s,%s",
						  $this->PrepareParamSQL($idEmb),
						  ($idProveedor) ? "'".$this->PrepareParamSQL($idProveedor)."'" : 'NULL',
						  ($numTrans) ? "'".$this->PrepareParamSQL($numTrans)."'" : 'NULL');
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			$RETVAL=1;
		else{
			$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
			$rs->Close();
		}
		unset($rs);
		
		if(!$RETVAL){
		
			$embSIS = new EmbarcacionSISESAT;
			if($idAntProveedor==4){
				if($idAntProveedor!=$idProveedor){
					// Da de baja ID de equipo en DB CLS
					$RETVAL = $embSIS->RetiraTransmisor($idEmb);
				}else{
				   
					// Cambia Id de Equipo en DB CLS
					$RETVAL = $embSIS->ModificaTransmisor($idEmb,$numTrans);
				}
			}elseif($idProveedor==4){
				// Cambia Id de Equipo en DB CLS
				$RETVAL = $embSIS->ModificaTransmisor($idEmb,$numTrans);
			}
		}		

		if($RETVAL)
			$this->conn->RollbackTrans(); 				
		else
			$this->conn->CommitTrans();

		$destination = "https://" . $_SERVER['HTTP_HOST'] . $this->modInfo['PATH'] . '?accion=';
		$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
		$destination .= "&menu={$this->menu_items[0]['val']}";
		header("Location: $destination");
		exit;
	}

}
?>