<?
require_once('claseModulos.inc.php');

class Papeletas extends Modulos{

	// Atributos de la Aplicacion
	var $arr_accion;
	
	// Horarios de Trabajo
	var $horaIni = 8;
	
	// Atributos de la Papeleta
	var $minTimeBeforeSent = 1;		// Minimo de Minutos antes del Envio de la Papeleta
	var $maxTimeBeforeSent = 21600;	// Maximo de Minutos antes del Envio de la Papeleta
	var $maxTimePapeleta = 72;		// Maximo de Horas de Permiso de Ausencia
	
	var $startWorkHour = 8;			// Hora que Comienza el Horario de Trabajo
	var $startWorkMin = 15;			// Minutos que Comienza el Horario de Trabajo
	var $endWorkHour = 17;			// Hora que Termina el Horario de Trabajo
	var $endWorkMin = 30;			// Munutos que Termina el Horario de Trabajo
	
	var $intervalMinu = 5;			// Nro de Minutos de Intervalo para la Creaci�n de Papeletas
	
	var $motPapPerm = "'0003','0008','0013','0025','0026','0029','0032','0027','0037','0039'";
	
	var $statPapeleta = array(EN_CURSO => 1, APROBADO => 2, DESAPROBADO => 3); // Estado de la Papeletas
	
	/* ------------------------------------------------------------- */
	/* Funcion para Ingresar un Reporte a la DB													 */
	/* ------------------------------------------------------------- */
	/* CLASE PAPELETAS												 */
	/* ------------------------------------------------------------- */
	
	function InsertaReporteDB($fecInicio,$fecFin,$filename,$usrRep=false,$dep=false,$indTipRep=false){
	
		$this->abreConnDB();

		$ins_st = sprintf("EXECUTE insReportePapeleta %s,%s,'%s','%s','%s',%d,%d,%d,%d",
						  ($usrRep) ? $usrRep : 'NULL',
						  ($dep) ? $dep : 'NULL',
						  $fecInicio,
						  $fecFin,
						  $this->PrepareParamSQL($filename),
						  ($indTipRep==1) ? 1 : 0,
						  ($indTipRep==2) ? 1 : 0,
						  ($indTipRep==3) ? 1 : 0,
						  $this->userIntranet[CODIGO]
						 );


		$rs = & $this->conn->Execute($ins_st);
		//echo $ins_st;
		

		
		unset($ins_st);
		if (!$rs)
			print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if (!($statTrans = $rs->FetchRow()))
				print 'Error en proceso de inserci�n. Consulte con el <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">Administrador</a>';
			$rs->Close();
		}
		
		switch($statTrans[0]){
			case 'A':
				$this->errors .= "- Error en Inserci�n del Reporte.<br>";
				$this->muestraMensajeInfo($this->errors);
				return false;
				break;
			case 'B':
				unlink('reports/' . $statTrans[1]);
				return true;
				break;
			case 1:
				return true;
				break;
		}
	}


	function GeneraReporte_ORHH_GESTION($dep,$user,$xid_option_selecc,$year_sel,$mes, $finicial_rep,$ftermino_rep)
	{
	
	//$this->abreConnDB();
	$html = new Smarty;
	$fechaGen=date('d/m/Y');
	if($user=='none'){ $user=0; }
	if($year_sel=='0'){ $year_sel=0; }
	if($mes=='0' || $mes==''){ $mes=0; }


		$sql_dep= "select d.dependencia from db_general.dbo.h_dependencia d 
					where d.codigo_dependencia=".$dep."";	
					//echo 	$sql_dep;
		$rs = & $this->conn->Execute($sql_dep);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$depName_sel = $row[0];
			}
			$rs->Close();
		}
		unset($rs);

		
		$sql_st = "exec listPapeletasReporte_ORHH_listado_personas ".$dep.", ".$user.",  ".$xid_option_selecc.", 
		".$year_sel.",".$mes.",'".$finicial_rep."','".$ftermino_rep."',1";
		//echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow()){
			$detalle_trabajador='';
			
			//echo $row[0];

						
						
					$sql_st_sub = "exec listPapeletasReporte_ORHH_listado_personas ".$dep.", ".$row[0].",  ".$xid_option_selecc.", 
					".$year_sel.",".$mes.",'".$finicial_rep."','".$ftermino_rep."',0";
					//echo $sql_st_sub;
					$rssub = & $this->conn->Execute($sql_st_sub);
					unset($sql_st);
					if (!$rssub)
						print $this->conn->ErrorMsg();
					else{
						while ($rowsub = $rssub->FetchRow()){

								$detalle_trabajador.='<tr>
													<td width="10%" align="center"><font color="#000000" size="1" face="Arial">'.$rowsub[0].'</font></td>
													<td width="30%" align="center"><font color="#000000" size="1" face="Arial">'.$rowsub[3].'</font></td>
													<td width="15%" align="center"><font color="#000000" size="1" face="Arial">'.$rowsub[4].'</font></td>
													<td width="15%" align="center"><font color="#000000" size="1" face="Arial">'.$rowsub[5].'</font></td>		
													</tr>';
									//echo $detalle_trabajador;
								}
								$rssub->Close();
							}
							
							
						$html->append('trabajadores',
						array(
						'codigo'=>$row[0],
						'trabajador'=>$row[1],
						'detalle_trabajador'=>$detalle_trabajador
						)
						);		
							
				
				}
				$rs->Close();
			}
			
		unset($rs);
		$html->assign_by_ref('fechaGen',$fechaGen);	
		$html->assign_by_ref('depName',$depName_sel);			


	$showpdf=$html->fetch('papeletas/report_orhh_oga.tpl.php');	
	echo $showpdf;
	}

	function GeneraReportePapeleta($userRep=false,$depRep=false,$fecInicio=false,$fecFin=false,$indTipRep=false,$tipRep=3,$fileType='pdf'){
		$fecInicio = ($fecInicio) ? explode('/',$fecInicio) : false;
		$fecFin = ($fecFin) ? explode('/',$fecFin) : false;

		$fecFin = date( "d/m/Y", mktime(0,0,0,($fecFin[0]) ? $fecFin[0] : ($fecInicio[0]+1),($fecFin[1]) ? $fecFin[1] : 0,($fecFin[2]) ? $fecFin[2] : date('Y')));
		$fecInicio = date( "d/m/Y", mktime(0,0,0,$fecInicio[0],($fecInicio[1]) ? $fecInicio[1] : 1,($fecInicio[2]) ? $fecInicio[2] : date('Y')));
		
		$this->abreConnDB();
		 //$this->conn->debug = true;
		 
		 //echo "hola";exit;

		// Obtiene el Trabajador y la Dependencia

		$sql_st = sprintf("EXEC listUsuarioDependencia %s, %s",
						  ($depRep) ? $this->PrepareParamSQL($depRep) : $this->PrepareParamSQL($this->userIntranet[COD_DEP]),
						  ($userRep) ? $this->PrepareParamSQL($userRep) : 'NULL');		
		
		
		//echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$depName = $row[0] ? $row[0] : false;
				$userName = $row[1] ? $row[1] : false;
			}
			$rs->Close();
		}
		unset($rs);




			$str_reporte='listPapeletasReporte';
			
			$sql_st = sprintf("EXEC ".$str_reporte." %s, %s, '%s', '%s', '%s', '%s', %d",
						  ($userRep) ? $this->PrepareParamSQL($userRep) : 'NULL',
						  ($depRep) ? $this->PrepareParamSQL($depRep) : 'NULL',
						  $fecInicio,
						  $fecFin,
						  sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMin),
						  sprintf('%02d:%02d',$this->endWorkHour,$this->endWorkMin),
						  $tipRep);
		//if($userRep)
		if($userRep==2324)
		{
//		echo $sql_st.'<br>';
		}
		//echo $sql_st;
		// Obtiene las Papeletas Aprobadas

		//echo '1'.$sql_st.'<br>'; 
		$rs = & $this->conn->Execute($sql_st);
		//echo '2<br>'; 
		unset($sql_st);
		if (!$rs){
			print 'Error:'.$this->conn->ErrorMsg();
		//echo '3<br>'; 
		}else{

		//echo '4<br>'; 
		
			// Crea el Objeto HTML
			$html = new Smarty;
		//echo '5<br>'; 			
			$cUsr = $cDep = $loop = 0;
			//echo '6<br>';
			while ($row = $rs->FetchRow()){
			

				if($loop==0){
					$depTemp = $row[11];
					$userTemp = $row[10];
					$pap[$cDep]['name'] = $depTemp;
					$pap[$cDep]['users'][$cUsr]['name'] = $userTemp;
				}
				if($depTemp!=$row[11]){
					$cDep++;
					$cUsr = -1;
					$depTemp = $row[11];
					$pap[$cDep]['name'] = $depTemp; 
				}
				if($userTemp!=$row[10]){
					$cUsr++;
					$userTemp = $row[10];
					$pap[$cDep]['users'][$cUsr]['name'] = $userTemp;
				}


				

				$pap[$cDep]['users'][$cUsr]['paps'][$row[12]][] = array('numero'=>sprintf('%06d',$row[0]),
																		'motivo'=>ucfirst($row[1]),
																		'inicio'=>$row[2],
																		'fin'=>$row[3],
																		'dias'=>$row[4],
																		'horas'=>$row[5],
																		'total'=>$row[6],
																		'obser'=>ucfirst($row[9]),
																		'user'=>$row[10],
																		'depen'=>$row[11],
																		'estado'=>$row[12]);		

				//cod_papeleta, asunto, fecha_ini, fecha_fin, dif, hora, horas, sina, nombre, oficina, estado, numdd
				
				$loop++;
			}
			$rs->Close();
			
			$html->assign_by_ref('pap',$pap);
			//echo '1.-';
			//$userName=$this->userIntranet[APELLIDO].', '.$this->userIntranet[NOMBRE];
			
			$html->assign_by_ref('userName',$userName);
			//echo '2.-';
			$html->assign_by_ref('depName',$depName);
			//echo '3.-';			
			// $html->assign('fechaRep',sprintf("Correspondiente al %sa�o %s",($mesRep) ? ' mes de '.ucfirst(strftime("%B", mktime(0,0,0,$mesRep,1,$anyo))).' del ' : '',$anyo));
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

			$logo = sprintf('/var/www/intranet/img/800x600/img.rep.logo.%d.gif', ($depRep) ? $depRep : $this->userIntranet[COD_DEP]);
			
			$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];
			
			$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports/';
			
			$filename = sprintf("rep%s%s",
								'-'.$this->userIntranet[CODIGO],
								'-'.mktime());
			//echo '3.-';											
			
			var_dump($userRep);

			$this->CreaArchivoPDF($filename,$path,$html->fetch(($userRep) ? 'papeletas/reportUser2.tpl.php' : 'papeletas/reportDep2.tpl.php'),false,$logo,$options);			
			if( $this->InsertaReporteDB($fecInicio,$fecFin,$filename . '.' . $fileType,$userRep,$depRep,$indTipRep) ){
				return true;
			}else{
				unlink('reports/' . $filename . '.' . $fileType );
				$this->errors .= "No se pudo Generar el Reporte.<br>";
				$this->muestraMensajeInfo($this->errors);
			}
		}
	}

	function generaContenidoReporteAnualPapeleta($anyo){
		$mesTope = ($anyo < date('Y')) ? 13 : date('n')+1;
		$this->abreConnDB();
		$content = include('HTML/papeletasUsuario/headerreportegenera.inc.php');
		$total_horas_anyo = 0;		
		for($i=1;$i<$mesTope;$i++){
			$sql_st = "SELECT p.id_papeleta, m.desc_motivo_ausencia, CONVERT(varchar(10),p.fecha_inicio,103) + ' ' + CONVERT(varchar(5),p.fecha_inicio,108), ".
							 "CONVERT(varchar(10),p.fecha_termino,103) + ' ' + CONVERT(varchar(5),p.fecha_termino,108), CONVERT(int,DATEDIFF(hour,p.fecha_inicio,p.fecha_termino)/24), ".
							 "DATEDIFF(hour,p.fecha_inicio,p.fecha_termino)-(24 * CONVERT(int,DATEDIFF(hour,p.fecha_inicio,p.fecha_termino)/24)), ".
							 "DATEDIFF(hour,p.fecha_inicio,p.fecha_termino) ".
					  "FROM dbo.papeleta p, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
					  "WHERE p.codigo_trabajador={$this->userIntranet[CODIGO]} and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default and p.id_estado={$this->statPapeleta[APROBADO]} ".
							"and ( p.fecha_inicio >= CONVERT(datetime,'" . date( "d/m/Y", mktime(0,0,0,$i,1,$anyo) ) . "',103) and ".
							"p.fecha_termino < CONVERT(datetime,'" . date( "d/m/Y", mktime(0,0,0,$i+1,1,$anyo) ) . "',103) ) ". 
					  "ORDER BY 3 ASC";
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$content .= '<table width="650" border="0" align="center" cellpadding="3" cellspacing="0">'.
							'<tr><td colspan="7" align="center" bgcolor="#999999"><font face="Arial" size="1" color="#FFFFFF"><b>'.
							strtoupper(strftime("%B", mktime(0,0,0,$i,1,$anyo))).'</b></font></td></tr></table><br>';
				$content .= include('HTML/papeletasUsuario/headerreportTR.inc.php');
				$total_horas_mes = 0;
				while ($row = $rs->FetchRow()){
					$content .= include('HTML/papeletasUsuario/reportTR.inc.php');
					$total_horas_mes += $row[6];
					$total_horas_anyo += $row[6];
				}
				if($rs->RecordCount()<1)
					$content .= '<tr><td colspan="7" align="center" bgcolor="#FFFFFF"><font face="Arial" size="1" color="#3270aa">No hay permisos de Salida.</font></td></tr>';
				else
					$content .= include('HTML/papeletasUsuario/reportTRTotal.inc.php');
				$content .= include('HTML/papeletasUsuario/footerreportTR.inc.php');
				$rs->Close();
			}
		}
		$content .= include('HTML/papeletasUsuario/footerreportegenera.inc.php');		
		return $content;	
	}
	
	function ImprimePapeleta($idPap,$prev){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.apellidos_trabajador + ' ' + t.nombres_trabajador, /*0*/
						  d.dependencia, 						/*1*/
						  m.desc_motivo_ausencia,  				/*2*/".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END,   				/*3*/".
						 "
						 p.destino,   			/*4*/ 
						 p.asunto,    			/*5*/
						 CONVERT(varchar(10),p.fecha_aprobacion,101),    			/*6*/
						 right('000000000'+rtrim(ltrim(str(isnull(p.cod_papeleta,1)))),9),    			/*7*/
						 t.CONDICION,     				/*8*/
						 t.id_regimen,     				/*9*/
						 p.ANO_EJE     					/*10*/
						 ".
				  "FROM dbo.PAPELETA p, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE p.id_papeleta={$idPap} and t.codigo_trabajador=p.codigo_trabajador and t.coddep=d.codigo_dependencia and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default ";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
								
				// Setea Campos del Formulario
				$html->assign('numPapeleta',sprintf('%06d',$row[7]));



				if($row[9]==1){ $des_regimenx='NOMBRADO';	 			}
				if($row[9]==2){ $des_regimenx='CAS';					}
				if($row[9]==3){ $des_regimenx='PRACTICANTE';			}				
				$html->assign('des_regimen',$des_regimenx);
				$html->assign('des_trabajador',$row[0]);
				$html->assign('des_dependencia',$row[1]);
				$html->assign('des_motivo',$row[2]);
				$html->assign('fecha',$row[3]);
				$html->assign('des_destino',$row[4]);
				$html->assign('des_asunto',$row[5]);
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign('fec_aprobacion',ucfirst(strftime("%A %d de %B del %Y", strtotime($row[6]))));
				
				if($prev==1)
				{
				$html->display('papeletas/printPapeletaprev.tpl.php');
				//$html->display('papeletas/printPapeleta.tpl.php');
				}else{
				$html->assign('codigo_generado',$row[7]);
				$html->assign('year_generado',$row[10]);				
				
				$html->display('papeletas/printPapeleta.tpl.php');
				}
			}
			$rs->Close();
		}
	}
	
	function MuestraObservacion($idPap){
		$this->abreConnDB();
		$sql_st = "SELECT observacion, CONVERT(varchar(10),fecha_aprobacion,101) ".
				  "FROM dbo.PAPELETA ".
				  "WHERE id_papeleta={$idPap}";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				// Setea Campos del Formulario
				$html->assign('des_observacion',$row[0]);
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign('fec_calificacion',ucfirst(strftime("%A %d de %B del %Y", strtotime($row[1]))));
				
				$html->display('papeletas/showObservacion.tpl.php');
			}
			$rs->Close();
		}
	}





	function GeneraReportePapeletaUsuario($userRep,$mes,$fileType,$ano_eje){
		
		

		$this->abreConnDB();
		
		$depRep=$this->userIntranet[COD_DEP];
		$sql_st = " EXEC listUsuarioDependencia_individual ".$depRep.", ".$userRep." ";		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow()){
				$depName = $row[0] ? $row[0] : false;
				$userName = $row[1] ? $row[1] : false;
				$id_regimen_trab = $row[2] ? $row[2] : false;				
			}
			$rs->Close();
		}
		unset($rs);
$startWorkHourX=8;
$startWorkMinX=15;
if($id_regimen_trab==1){ $endWorkHourX=17;	 	$endWorkMinX=30;		}
if($id_regimen_trab==2){ $endWorkHourX=17;	 	$endWorkMinX=45;		}
if($id_regimen_trab==3){ $endWorkHourX=16;	 	$endWorkMinX=30;		}





$mes=substr($mes,0,2);
$tipRep=3;

$depRep2=$depRep;
$userRep2=$userRep;
if($userRep==710)
{
//$depRep2=54;
//$userRep2=1017;
}


			$sql_stcont = "EXEC listPapeletasReporte_x_usuario_contador ".$userRep2.", ".
						  $depRep2.", ".$ano_eje.", ".$mes.", "
						  ."'".sprintf('%02d:%02d',$startWorkHourX,$startWorkMinX)."', "
						  ."'".sprintf('%02d:%02d',$endWorkHourX,$endWorkMinX)."', ".$tipRep;
		$rscont = & $this->conn->Execute($sql_stcont);

		unset($sql_stcont);
		if (!$rscont)
			print $this->conn->ErrorMsg();
		else{
			if($rowcont = $rscont->FetchRow()){
				$contreg_ind = $rowcont[0] ? $rowcont[0] : 0;
			}
			$rscont->Close();
		}
		unset($rscont);
		if($userRep==710)
		{
		//echo $contreg_ind;
		}
	if($contreg_ind>0)
	{

	$bdlink=$this->conectar_DB();
		$str_reporte='listPapeletasReporte_x_usuario';
		$sql_SP3 = "EXEC ".$str_reporte." ".$userRep2.", ".
		$depRep2.", ".$ano_eje.", ".$mes.", "
		."'".sprintf('%02d:%02d',$startWorkHourX,$startWorkMinX)."', "
		."'".sprintf('%02d:%02d',$endWorkHourX,$endWorkMinX)."', ".$tipRep;


		if($userRep==710)
		{
		//echo $sql_SP3 ;
		}
		

		//echo $sql_SP3;
				$resultw=mssql_query($sql_SP3,$bdlink);		
				if (!$resultw) 
				{
				}else{
				
			$html = new Smarty;
			$cUsr = $cDep = $loop = 0;				
				
					while( $row2 = mssql_fetch_array( $resultw ) )
						{ 
						echo $row5[1];
						if($loop==0){
							$depTemp = $row2[11];
							$userTemp = $row2[10];
							$pap[$cDep]['name'] = $depTemp;
							$pap[$cDep]['users'][$cUsr]['name'] = $userTemp;
						}
						
						
						if($depTemp!=$row2[11]){
							$cDep++;
							$cUsr = -1;
							$depTemp = $row2[11];
							$pap[$cDep]['name'] = $depTemp; 
						}
						if($userTemp!=$row2[10]){
							$cUsr++;
							$userTemp = $row2[10];
							$pap[$cDep]['users'][$cUsr]['name'] = $userTemp;
						}
				
				
						$pap[$cDep]['users'][$cUsr]['paps'][$row2[12]][] = array('numero'=>sprintf('%06d',$row2[0]),
																				'motivo'=>ucfirst($row2[1]),
																				'inicio'=>$row2[2],
																				'fin'=>$row2[3],
																				'dias'=>$row2[4],
																				'horas'=>$row2[5],
																				'total'=>$row2[6],
																				'obser'=>ucfirst($row2[9]),
																				'user'=>$row2[10],
																				'depen'=>$row2[11],
																				'estado'=>$row2[12]);		
		
						//cod_papeleta, asunto, fecha_ini, fecha_fin, dif, hora, horas, sina, nombre, oficina, estado, numdd
						
						$loop++;
				

						} 
						mssql_free_result($resultw); 	
						unset($resultw);


					$html->assign_by_ref('pap',$pap);
					//echo '1.-';
					//$userName=$this->userIntranet[APELLIDO].', '.$this->userIntranet[NOMBRE];
			
					$html->assign_by_ref('userName',$userName);
					//echo '2.-';
					$html->assign_by_ref('depName',$depName);
					//echo '3.-';			
					// $html->assign('fechaRep',sprintf("Correspondiente al %sa�o %s",($mesRep) ? ' mes de '.ucfirst(strftime("%B", mktime(0,0,0,$mesRep,1,$anyo))).' del ' : '',$anyo));
					$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
					$logo = sprintf('/var/www/intranet/img/800x600/img.rep.logo.%d.gif', ($depRep) ? $depRep : $this->userIntranet[COD_DEP]);
					
					$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];
					
					$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports/';
			
					$filename = sprintf("rep%s%s",
										'-'.$this->userIntranet[CODIGO],
										'-'.mktime());
					//echo '3.-';											
			$usuario=0;
			$usuario=$usuario+$userRep;
			//var_dump($userRep);

				$this->CreaArchivoPDF($filename,$path,$html->fetch(($userRep) ? 'papeletas/reportUser2.tpl.php' : 'papeletas/reportDep2.tpl.php'),false,$logo,$options);			
				

				if( $this->InsertaReporteDB_x_usu($userRep,$depRep,$mes,$filename . '.pdf', $ano_eje) )
				{
					return true;
				}else{
					unlink('reports/' . $filename . '.' . $fileType );
					$this->errors .= "No se pudo Generar el Reporte.<br>";
					$this->muestraMensajeInfo($this->errors);
				}

				}		

	$this->desconectar_DB($bdlink);
	




		
	}else{
			$this->errors .= "No tiene ninguna papeleta registrada..<br>";
			$this->muestraMensajeInfo($this->errors);
	
	}
	


}

	function conectar_DB()
	{
		if (!($dab=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))) 
		   { 
			  echo "Error conectando a la base de datos."; 
			  exit(); 
		   } 
		   if (!mssql_select_db("DB_PAPELETA",$dab)) 
		   { 
			  echo "Error seleccionando la base de datos."; 
			  exit(); 
		   }		
		 return $dab;
	} 



	function desconectar_DB($dblink)
	{
		mssql_close($dblink); 
		unset($dblink);
	}
	


function InsertaReporteDB_x_usu($usrRep,$dep,$mes,$filename,$ano_eje)
{

	$bdlink=$this->conectar_DB();
		$sql_SP3 =	"EXECUTE insReportePapeletausu ".$usrRep.", ".$dep.",".$ano_eje.", ".$mes.",'".$filename."' ";

		//echo $sql_SP3;
				$resultw=mssql_query($sql_SP3,$bdlink);		
				if (!$resultw) 
				{
					echo 'Error en el proceso de Inserci�n.<br>';
				}else{
					while( $row2 = mssql_fetch_array( $resultw ) )
						{ 	

							switch($row2[0])
							{
								case 'A':
									unlink('reports/' . $statTrans[1]);
									$this->errors .= "- Error en Inserci�n del Reporte.<br>";
									$this->muestraMensajeInfo($this->errors);
									return false;
									break;
								case 1:
									return true;
									break;
							}

						} 
						mssql_free_result($resultw); 	
						unset($resultw);
				}
				
		$this->desconectar_DB($bdlink);


}
	

}

?>