<?php

include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class DispositivosLegales extends Modulos{

    function DispositivosLegales($menu,$subMenu){
		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
						 
		/*
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Dispositivos Legales del Portal',false,array('noticias'));
			$objIntranet->Body('actiminiportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][1];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][2];
		$this->userDB = $this->arr_userDB['postgres'][1];
		$this->passDB = $this->arr_passDB['postgres'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_DISPOSITIVO' => 'frmSearchDisp',
							'BUSCA_DISPOSITIVO' => 'searchDisp',
							'FRM_AGREGA_DISPOSITIVO' => 'frmAddDisp',
							'AGREGA_DISPOSITIVO' => 'addDisp',
							'FRM_MODIFICA_DISPOSITIVO' => 'frmModifyDisp',
							'MODIFICA_DISPOSITIVO' => 'modifyDisp',
							'FRM_BUSCA_TIPO' => 'frmSearchType',
							'BUSCA_TIPO' => 'searchType',
							'FRM_AGREGA_TIPO' => 'frmAddType',
							'AGREGA_TIPO' => 'addType',
							'FRM_MODIFICA_TIPO' => 'frmModifyType',
							'MODIFICA_TIPO' => 'modifyType',
							'FRM_BUSCA_TEMA' => 'frmSearchTheme',
							'BUSCA_TEMA' => 'searchTheme',
							'FRM_AGREGA_TEMA' => 'frmAddTheme',
							'AGREGA_TEMA' => 'addTheme',
							'FRM_MODIFICA_TEMA' => 'frmModifyTheme',
							'MODIFICA_TEMA' => 'modifyTheme',
							'BUENA_TRANS' => 'goodTrans',
							'MALA_TRANS' => 'badTrans'							
							);

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchDisp', label => "DISPOSITIVOS" ),
						  1 => array ( 'val' => 'frmSearchType', label => 'TIPOS' ),
							1 => array ( 'val' => 'frmSearchTheme', label => 'TEMAS' ),
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];

		// Items del Sub Menu para cada Menu
		if($this->menuPager==$this->menu_items[0]['val']){
			if($_SESSION['mod_ind_leer']) $this->subMenu_items[] = array ( 'val' => 'frmSearchDisp', label => 'BUSCAR' );
			if($_SESSION['mod_ind_insertar']) $this->subMenu_items[] =	array ( 'val' => 'frmAddDisp', label => 'AGREGAR' );
			if($_SESSION['mod_ind_modificar']) $this->subMenu_items[] =	array ( 'val' => 'frmModifyDisp', label => 'MODIFICAR' );
		}elseif($this->menuPager==$this->menu_items[1]['val']){
			if($_SESSION['mod_ind_leer']) $this->subMenu_items[] = array ( 'val' => 'frmSearchType', label => 'BUSCAR' );
			if($_SESSION['mod_ind_insertar']) $this->subMenu_items[] =	array ( 'val' => 'frmAddType', label => 'AGREGAR' );
			if($_SESSION['mod_ind_modificar']) $this->subMenu_items[] =	array ( 'val' => 'frmModifyType', label => 'MODIFICAR' );
		}elseif($this->menuPager==$this->menu_items[2]['val']){
			if($_SESSION['mod_ind_leer']) $this->subMenu_items[] = array ( 'val' => 'frmSearchTheme', label => 'BUSCAR' );
			if($_SESSION['mod_ind_insertar']) $this->subMenu_items[] =	array ( 'val' => 'frmAddTheme', label => 'AGREGAR' );
			if($_SESSION['mod_ind_modificar']) $this->subMenu_items[] =	array ( 'val' => 'frmModifyTheme', label => 'MODIFICAR' );
		}
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];
		
		// Obtiene los Datos del Usuario en la DB PostgreSQL

		$this->datosUsuarioPortal();

		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Dispositivos Legales del Portal',false,array('noticias'));
			$objIntranet->Body('actiminiportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
    }
	
	function ValidaFechaDispositivo($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_PC]);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_PARTE]);

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		//$html->display('portal/dispositivos/headerArm.tpl.php');
		$html->display('portal/dispositivos/showStatTrans.inc.php');
		$html->display('portal/dispositivos/footerArm.tpl.php');
	}
	
	function FormBuscaDispositivo($page=NULL,$desNombre=NULL,$search=false){
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		//$html->display('portal/dispositivos/headerArm.tpl.php');
		$html->display('portal/dispositivos/dispositivos/search.tpl.php');
		if(!$search) $html->display('portal/dispositivos/footerArm.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaDispositivo();
	}

	
	function BuscaDispositivo($page,$desNombre){
		// Genera HTML
		$html = new Smarty;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDispositivo($page,$desNombre,true);

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);	

		$sql_st = sprintf("SELECT count(*) ".
						  "FROM public.norma_legal ".
						  "WHERE (Upper(cod_norma) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
								 "or Upper(des_norma) like Upper('%%%1\$s%%'))",
						  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL);
		// echo $sql_st;		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar Busqueda
			$start =  ($page == 1) ? 0 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_st = sprintf("SELECT id_norma, cod_norma, to_char(fec_norma, 'DD/MM/YYYY HH24:MI'), ".
									 "CASE WHEN ind_activo is true THEN 'Activo' ELSE 'Inactivo' END ".
							  "FROM public.norma_legal ".
							  "WHERE (Upper(cod_norma) like Upper('%%%1\$s%%') or Upper(des_resumen) like Upper('%%%1\$s%%') ".
									 "or Upper(des_norma) like Upper('%%%1\$s%%')) ".
							  "ORDER BY fec_creacion desc ".
							  "LIMIT %2\$s OFFSET %3\$s",
							  ($desNombre) ? $this->PrepareParamSQL($desNombre) : NULL,
							  $this->numMaxResultsSearch,
							  $start);
			//echo $sql_st;
	
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while($row = $rs->FetchRow())
					$html->append('arrDisp', array('id' => $row[0],
												   'desc' => $row[1],
												   'fech' => $row[2],
												   'esta' => $row[3]));
				$rs->Close();
			}
			unset($rs);
		}else
			$start = 0;
					
		// Setea indicadores de derecho
		$html->assign('indMod',$_SESSION['mod_ind_modificar']);

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DISPOSITIVO], true));

		// Muestra el Resultado de la Busqueda
		$html->display('portal/dispositivos/dispositivos/searchResult.tpl.php');
		$html->display('portal/dispositivos/footerArm.tpl.php');
	}
	
	function FormManipulaDispositivo($idDispositivo=NULL,$idTipo=NULL,$desCodigo=NULL,$desResumen=NULL,$desDispositivo=NULL,$fecDispositivo=NULL,
 							     $idTema=NULL,$indActivo=NULL,$indAnexo=NULL,$idTipOrg=NULL,$idOrg=NULL,$indFile=NULL,$idTipFile=NULL,$desFile=NULL,
							     $reLoad=false,$errors=NULL){
								
		$this->abreConnDB();
		// $this->conn->debug = true;
		if($idDispositivo&&!$reLoad){
			$sql_st = sprintf("SELECT nl.id_norma,nl.id_normatipo,nl.cod_norma,nl.des_resumen,nl.des_norma,to_char(nl.fec_norma,'MM/DD/YYYY HH24:MI'),nl.id_normatema".
			   						 ",CASE WHEN nl.ind_activo is true THEN 'Y' ELSE 'N' END,CASE WHEN nl.ind_anexo is true THEN 'Y' ELSE 'N' END".
									 ",public.getTipoNormaPadre(%1\$d),nl.id_normapadre,nl.ind_archivo,id_archivotipo,des_archivo ".
							  "FROM public.norma_legal nl ".
							  "WHERE id_norma=%1\$d",$this->PrepareParamSQL($idDispositivo));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($idDispositivo,$idTipo,$desCodigo,$desResumen,$desDispositivo,$fecDispositivo,
 					     $idTema,$indDispvo,$indAnexo,$idTipOrg,$idOrg,$indFile,$idTipFile,$desFile) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
		//Manipulacion de las Fechas
		$fecDispositivo = ($fecDispositivo!='//'&&$fecDispositivo&&!strstr($fecDispositivo,'none')) ? $fecDispositivo : NULL;
		
		// Manipulaci�n de Datos
		$idTipOrg = $indAnexo!='N' ? $idTipOrg : NULL;
		$idOrg = $indAnexo!='N' ? $idOrg : NULL;
		$idTipFile = $indFile!='N' ? $idTipFile : NULL;
		$desFile = $indFile!='N' ? $desFile : NULL;

		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddDisp';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$datosFrm = compact("idDispositivo","desCodigo","desResumen","desDispositivo",
 					        "indActivo","indAnexo","indFile","desFile");
		$html->assign_by_ref('datosFrm',$datosFrm);
		
		$html->assign_by_ref('idTipo',$idTipo);
		
		// Fecha Publicacion Dispositivo
		$html->assign_by_ref('selMesPub',$this->ObjFrmMes(1, 12, true, substr($fecDispositivo,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaPub',$this->ObjFrmDia(1, 31, substr($fecDispositivo,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoPub',$this->ObjFrmAnyo(date('Y')-8, date('Y'), substr($fecDispositivo,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Contenido Select Norma Tipo
		$sql_st = "SELECT id_normatipo, des_normatipo ".
				  "FROM public.norma_legal_tipo ".
				  "WHERE ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo',$this->ObjFrmSelect($sql_st, $idTipo, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		unset($sql_st);

		// Contenido Select Norma Tema
		$sql_st = "SELECT id_normatema, des_normatema ".
				  "FROM public.norma_legal_tema ".
				  "WHERE ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTema',$this->ObjFrmSelect($sql_st, $idTema, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		unset($sql_st);

		// Contenido Select Tipo Norma Origen
		$sql_st = "SELECT id_normatipo, des_normatipo ".
				  "FROM public.norma_legal_tipo ".
				  "WHERE id_normatipo<>13 and ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipOrg',$this->ObjFrmSelect($sql_st, $idTipOrg, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		unset($sql_st);

		// Contenido Select Norma Origen
		$sql_st = sprintf("SELECT id_norma, cod_norma ".
						  "FROM public.norma_legal ".
						  "WHERE ind_activo is true %s %s ".
						  "ORDER BY 2",
						  $idDispositivo ? "and id_norma<>" . $idDispositivo : NULL,
						  ($idTipOrg&&$idTipOrg!='none') ? "and id_normatipo=" . $idTipOrg  : "and 1=2");
		$html->assign_by_ref('selOrg',$this->ObjFrmSelect($sql_st, $idOrg, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		unset($sql_st);

		// Contenido Select Tipo Archivo
		$sql_st = "SELECT id_archivotipo, des_tipo ".
				  "FROM public.archivo_tipo ".
				  "WHERE ind_activo is true ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipFile',$this->ObjFrmSelect($sql_st, $idTipFile, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display('portal/dispositivos/headerArm.tpl.php');
		$html->display('portal/dispositivos/dispositivos/frmHandle.tpl.php');
		$html->display('portal/dispositivos/footerArm.tpl.php');
	
	}

	function ManipulaDispositivo($idDispositivo,$idTipo,$desCodigo,$desResumen,$desDispositivo,$fecDispositivo,
 							     $idTema,$indActivo,$indAnexo,$idTipOrg,$idOrg,$indFile,$idTipFile,$desFile){
							
		//Manipulacion de las Fechas
		$fecDispositivo = ($fecDispositivo!='// :'&&$fecDispositivo&&!strstr($fecDispositivo,'none')) ? $fecDispositivo : NULL;
		
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImg = trim($desImg)!='/mipe/img/prensa/' ? $desImg : NULL; 
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$desImgExp = trim($desImgExp)!='/mipe/img/prensa/' ? $desImgExp : NULL; 
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Comprueba Valores	
		if(!$idTipo){ $bTip = true; $this->errors .= 'El Tipo de Norma debe ser especificado<br />'; }
		if(!$desCodigo){ $bCod = true; $this->errors .= 'El C&oacute;digo debe ser especificado<br />'; }
		// if(!$desResumen){ $bRes = true; $this->errors .= 'El Resumen debe ser especificado<br>'; }
		// if(!$desDispositivo){ $bDisp = true; $this->errors .= 'La Dispositivo debe ser especificada<br>'; }
		if($fecDispositivo){ $bFDisp = $this->ValidaFechaDispositivo($fecDispositivo,'Dispositivo'); }else{ $bFDisp = true; $this->errors .= 'La Fecha de Publicaci&oacute;n de la Norma debe ser especificada<br />'; }
		if(!$idTema){ $bTem = true; $this->errors .= 'El Tema de la Norma debe ser especificado<br />'; }
		if(!$indActivo){ $bAct = true; $this->errors .= 'El Estado de la Norma debe ser especificado<br />'; }
		if($indAnexo=='Y'&&(!$idTipOrg||$idTipOrg=='none')){ $bTOr = true; $this->errors .= 'El Tipo de Norma de Origen debe ser especificado, si la norma es un Anexo<br />'; }
		if($indAnexo=='Y'&&(!$idOrg||$idOrg=='none')){ $bOrg = true; $this->errors .= 'La Norma de Origen debe ser especificada, si la norma es un Anexo<br />'; }
		if($indFile=='Y'&&(!$idTipFile||$idTipFile=='none')){ $bTFi = true; $this->errors .= 'El Tipo de Archivo debe ser especificado, si la norma tiene un archivo<br />'; }
		if($indFile=='Y'&&!desFile){ $bFil = true; $this->errors .= 'El Nombre de Archivo debe ser especificado, si la norma tiene un archivo<br />'; }

		if($bTip||$bCod||$bFDisp||$bTem||$bAct||$bTOr||$bOrg||$bTFi||$bFil){
			$objIntranet = new Intranet();
			$objIntranet->Header('Dispositivos Legales',false,array('noticias'));
			$objIntranet->Body('displegalesportal_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormManipulaDispositivo($idDispositivo,$idTipo,$desCodigo,$desResumen,$desDispositivo,$fecDispositivo,
 							     $idTema,$indActivo,$indAnexo,$idTipOrg,$idOrg,$indFile,$idTipFile,$desFile,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			// $this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_FT = sprintf("SELECT %s(%s%d,'%s',%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%d)",
							  $idDispositivo ? 'modDispLegalPortal' : 'addDispLegalPortal',
							  $idDispositivo ? $this->PrepareParamSQL($idDispositivo).',' : '',
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($desCodigo),
							  ($desResumen) ? "'".$this->PrepareParamSQL($desResumen)."'" : 'NULL',
							  ($desDispositivo) ? "'".$this->PrepareParamSQL($desDispositivo)."'" : 'NULL',
							  ($fecDispositivo) ? "'".$this->PrepareParamSQL($fecDispositivo)."'" : 'NULL',
							  $this->PrepareParamSQL($idTema),
							  ($indAnexo=='Y') ? 'true' : 'false',
							  ($indAnexo=='Y') ? $this->PrepareParamSQL($idOrg) : 'NULL',
							  ($indFile=='Y') ? 'true' : 'false',
							  ($indFile=='Y') ? $this->PrepareParamSQL($idTipFile) : 'NULL',
							  ($indFile=='Y') ? "'".$this->PrepareParamSQL($desFile)."'" : 'NULL',
							  ($indActivo=='Y') ? 'true' : 'false',
							  $this->userIntranet['CODIGO']);
			// echo $sql_FT;
			$rs = & $this->conn->Execute($sql_FT);
			unset($sql_FT);
			if (!$rs)
				$RETVAL=0;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 0;
				$rs->Close();
			}
			unset($rs);		

			if(!$RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= (!$RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu=";
			$destination .= $idDispositivo ? $this->menu_items[2]['val'] : $this->menu_items[1]['val'];
			// echo $destination; exit;
			header("Location: $destination");
			exit;
		}
	}
}
?>
