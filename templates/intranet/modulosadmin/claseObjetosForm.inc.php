<?
require_once('modulosadmin/claseJScriptForm.inc.php');	

class ObjetosForm extends JScriptForm{

	/* --------------------------------------------------------- */
	/* Funcion para Mostrar el Status de una Operacion hecha	 */
	/* sobre la Base de Datos de determinado Modulo (Buttons)	 */
	/* --------------------------------------------------------- */
	
	function muestraTablaStatusDB($mensaje,$lbmod,$lbins=''){
?>
	
<table width="531" border="0" cellspacing="0" cellpadding="3">
  <tr> 
		<td>&nbsp;</td>
	  </tr>
	  <tr> 
		<td class="aviso" align="center"><b><?=$mensaje;?></b></td>
	  </tr>
	  <tr>
		
    <td align="center"> 
      <?
		if($lbins!=''){?>
      <input type="button" value="<?=$lbins?>" class="but-login" onClick="MM_goToURL('parent','<?=$_SERVER['PHP_SELF'];?>');return document.MM_returnValue">
		  &nbsp;&nbsp;&nbsp;<?
		}?>
		  <input type="button" value="<?=$lbmod?>" class="but-login">
    </td>
	  </tr>
	  <tr> 
		<td>&nbsp;</td>
	  </tr>
	</table>
<?
	}

	function ObjFrmDia($start, $end, $val=NULL, $fetch=false, $none=NULL){
		for($i=$start,$cont=0;$i<$end+1;$i++,$cont++) {
			$options[$cont]['val'] = sprintf("%02d",$i);
			$options[$cont]['label'] = sprintf("%02d",$i);
			$options[$cont]['sel'] = (sprintf("%02d",$i)==$val) ? 'selected' : '';
		}
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}
	
	function ObjFrmMes($start=1, $end=12, $literal=false, $val=NULL, $fetch=false, $none=NULL){
		for($i=$start,$cont=0;$i<$end+1;$i++,$cont++) {
			$options[$cont]['val'] = sprintf("%02d",$i);
			$options[$cont]['label'] = ($literal) ? ucfirst( strftime("%B",mktime(0,0,0,$i,1,date('Y'))) ) : sprintf("%02d",$i);
			$options[$cont]['sel'] = (sprintf("%02d",$i)==$val) ? 'selected' : '';
		}
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}

	function ObjFrmAnyo($start, $end, $val=NULL, $fetch=false, $none=NULL){
		for($i=$start,$cont=0;$i<$end+1;$i++,$cont++) {
			$options[$cont]['val'] = sprintf("%04d",$i);
			$options[$cont]['label'] = sprintf("%04d",$i);
			$options[$cont]['sel'] = (sprintf("%02d",$i)==$val) ? 'selected' : '';
		}
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}

	function ObjFrmHora($start, $end, $literal=false, $val=NULL, $fetch=false, $none=NULL){
		setlocale(LC_TIME,'C');
		$html = new Smarty;
		for($i=$start,$cont=0;$i<$end+1;$i++,$cont++) {
			$html->append('options',array('val'  => sprintf("%02d",$i),
										  'label'=> ($literal) ? strftime('%I %p',mktime($i)) : $i,
										  'sel'  => (sprintf("%02d",$i)==$val) ? 'selected' : ''));
		}
		setlocale(LC_TIME,$this->zonaHoraria);
		$html->assign('none',is_array($none) ? $none : false);
		// $html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}

	function ObjFrmMinuto($interval, $literal=false, $val=NULL, $fetch=false, $none=NULL){
		for($i=0,$cont=0;$i<60;$i=$i+$interval,$cont++) {
			$options[$cont]['val'] = sprintf("%02d",$i);
			$options[$cont]['label'] = ($literal) ? ':'.sprintf("%02d",$i) : sprintf("%02d",$i);
			$options[$cont]['sel'] = (sprintf("%02d",$i)==$val) ? 'selected' : '';
		}
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}
		
	function insertaMesForm($name,$valMes=NULL,$literal=true,$mesStart=1,$mesEnd=12,$label=NULL,$class='ip-login contenido'){
		printf("<select name=\"%s\" class=\"%s\">",$name,$class);
		if (!is_null($label))
			printf("<option value=\"NULL\"%s>%s</option>",($valMes=='NULL') ? ' selected' : '', $label);
		for($i=$mesStart;$i<$mesEnd+1;$i++)
			printf("<option value=\"%s\"%s>%s</option>", 
					sprintf("%02d",$i),
					($valMes==sprintf("%02d",$i)) ? ' selected' : '',
					($literal) ? ucfirst( strftime("%B",mktime(0,0,0,$i,1,date('Y'))) ) : sprintf("%02d",$i));
		print("</select>");
	}
	
	function insertaAnyoForm($name,$valMes=NULL,$label=NULL,$anyoStart=2003,$anyoEnd=2003,$class='ip-login contenido'){
		printf("<select name=\"%s\" class=\"%s\">",$name,$class);
		if (!is_null($label))
			printf("<option value=\"NULL\"%s>%s</option>",($valMes=='NULL') ? ' selected' : '', $label);
		for($i=$anyoStart;$i<$anyoEnd+1;$i++)
			printf("<option value=\"%s\"%s>%s</option>", 
					sprintf("%04d",$i),
					($valAnyo==sprintf("%04d",$i)) ? ' selected' : '',
					sprintf("%04d",$i));
		print("</select>");
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Insertar Campo Fecha en Formulario (3 ComboList) */
	/* ------------------------------------------------------------- */
	
	function insertaFechaForm($dia='des_dia',$mes='des_mes',$anyo='des_anyo',$frm='form1',$anyoStar=1990,$anyoEnd=2003,$valDia='NULL',$valMes='NULL',$valAnyo='NULL',$class='ip-login contenido',$papeletas=false,$showDia=true,$showMes=true,$showAnyo=true){
?>
	<table width="100" border="0" cellspacing="2" cellpadding="0">
	  <tr> 
		<td> 
		  <select name="<?echo $dia;?>" class="<?=$class;?>">
			<option value="NULL"<?if($valDia=='NULL') echo " selected";?>>--</option>
<?
		if($showDia){
			for($i=1;$i<32;$i++)
				printf("<option value=\"%s\"%s>%s</option>", sprintf("%02d",$i), ($valDia==sprintf("%02d",$i)) ? ' selected' : '', sprintf("%02d",$i));
		}
?>
		  </select>
		</td>
		<td> 
		  <select name="<?echo $mes;?>" class="<?=$class;?>" onChange="<?echo ($papeletas) ? 'asignaDiasPapeleta' : 'asignaDias';?>(document.<?=$frm?>.<?=$dia?>, document.<?=$frm?>.<?=$mes?>, document.<?=$frm?>.<?=$anyo?>)">
			<option value="NULL"<?if($valMes=='NULL') echo " selected";?>>--</option>
<?
		if($showMes){
			for($i=1;$i<13;$i++)
				printf("<option value=\"%s\"%s>%s</option>", sprintf("%02d",$i), ($valDia==sprintf("%02d",$i)) ? ' selected' : '', sprintf("%02d",$i));
		}
?>
		  </select>
		</td>
		<td> 
		  <select name="<?echo $anyo;?>" class="<?=$class;?>" onChange="<?echo ($papeletas) ? 'asignaMesesPapeleta' : 'asignaDias';?>(document.<?=$frm?>.<?=$dia?>, document.<?=$frm?>.<?=$mes?>, document.<?=$frm?>.<?=$anyo?>)">
			<option value="NULL"<?if($valAnyo=='NULL') echo " selected";?>>----</option>
<?
		if($showAnyo){
			for($i=$anyoStar;$i<$anyoEnd+1;$i++)
				printf("<option value=\"%s\"%s>%s</option>", sprintf("%04d",$i), ($valDia==sprintf("%04d",$i)) ? ' selected' : '', sprintf("%04d",$i));
		}
?>
		  </select>
		</td>
	  </tr>
	</table>
<?
	}

	/* ------------------------------------------------------------- */
	/* Funcion para Insertar Campo Hora en Formulario (2 ComboList)  */
	/* ------------------------------------------------------------- */
	
	function insertaHoraForm($hora='des_hora',$min='des_min',$valHora='NULL',$valMin='NULL',$interval=1,$class='ip-login contenido',$hora_star=8,$hora_end=18){
?>
	
<table width="70" border="0" align="left" cellpadding="0" cellspacing="2">
  <tr> 
		<td> 
		  <select name="<?echo $hora;?>" class="<?=$class;?>">
			<option value="NULL"<?if($valHora=='NULL') echo " selected";?>>--</option>
<?
		for($i=$hora_star;$i<$hora_end+1;$i++){
?>
			<option value="<?=sprintf("%02d",$i);?>"<?if($valHora==sprintf("%02d",$i)) echo " selected";?>><?=sprintf("%02d",$i);?></option>
<?
		}
?>
		  </select>
		</td>
		<td> 
		  <select name="<?echo $min;?>" class="<?=$class;?>">
			<option value="NULL"<?if($valMin=='NULL') echo " selected";?>>--</option>
<?
		for($i=0;$i<60;$i=$i+$interval){
?>
			<option value="<?=sprintf("%02d",$i);?>"<?if($valMin==sprintf("%02d",$i)) echo " selected";?>><?=sprintf("%02d",$i);?></option>
<?
		}
?>
		  </select>
		</td>
	  </tr>
	</table>
<?
	}
	
	/* ------------------------------------------------------------------- */
	/* Funcion para Insertar Campo Dependencia en Formulario (1 ComboList) */
	/* ------------------------------------------------------------------- */
	
	function ObjFrmSelDep($val=NULL,$fetch=false,$ucwords=false,$none=NULL){
		$this->abreConnDB();
		$sql_st="SELECT sd.id_subdependencia, sd.id_dependencia, d.des_dependencia, sd.des_subdependencia ".
				"FROM dependencia d, subdependencia sd ".
				"WHERE sd.id_dependencia=d.id_dependencia and d.ind_activo is true and sd.ind_activo is true ".
				"ORDER BY sd.id_dependencia, d.des_dependencia, ord_subdependencia";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$idDep=NULL;
			$jscript = "<script language=\"JavaScript\">\r\n";
			$jscript .= "dependenciaDes = new Array()\r\n";
			$jscript .= "dependenciaVal = new Array()\r\n";
			$i=0; 
			$cont = 0;
			while ($row = $rs->FetchRow()) {
				if($idDep != $row[1]){
					$idDep = $row[1];
					$i=0;
					$jscript .= "dependenciaDes[{$idDep}] = new Array()\r\n";
					$jscript .= "dependenciaVal[{$idDep}] = new Array()\r\n";
					// Forma Array de Opciones del Select
					$options[$cont]['val'] = $row[1];
					$row[2] = (strlen($row[2])>40) ? substr($row[2],0,40)."..." : $row[2];
					$options[$cont]['label'] = ($ucwords) ? ucwords(strtolower($row[2])) : $row[2];
					//$options[$cont]['label'] = (strlen($options[$cont]['label'])>50) ? substr($options[$cont]['label'],0,50)."..." : $options[$cont]['label'];
					$options[$cont]['sel'] = ($row[1]==$val) ? 'selected' : '';
					$cont++;
				}
				$row[3] = (strlen($row[3])>40) ? substr($row[3],0,40)."..." : $row[3];
				$jscript .= sprintf("dependenciaDes[%s][%s]='%s'\r\n", $idDep, $i, ($ucwords) ? ucwords(strtolower($row[3])) : $row[3]);
				$jscript .= sprintf("dependenciaVal[%s][%s]='%s'\r\n", $idDep, $i, ($ucwords) ? ucwords(strtolower($row[0])) : $row[0]);
				$i++;
			}
			$jscript .= "</script>\r\n";
			unset($row);
			$rs->Close();
		}
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign('options',$options);
		if($fetch)
			return array('OBJECT' => $html->fetch('objsform/selectDependencia.tpl.php'), 'JSCRIPT' => $jscript);
		else
			$html->display('objsform/selectDependencia.tpl.php');
		
	}
	
	function insertaDependenciaFrm($val=NULL,$select='id_dependencia',$todas=false,$onChangeSubmit=false){
	
		$this->abreConnDB();
		
		$sql_st="select sd.id_subdependencia, sd.id_dependencia, d.des_dependencia, sd.des_subdependencia ".
				"from dependencia d, subdependencia sd ".
				"WHERE sd.id_dependencia=d.id_dependencia and d.ind_activo is true and sd.ind_activo is true ".
				"ORDER BY sd.id_dependencia, d.des_dependencia, ord_subdependencia";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			echo "<select name=\"${select}\" class=\"ip-login contenido\" onChange=\"muestraSubdependencias(this);";
			echo ($onChangeSubmit) ? "submitForm()" : "";
			echo "\">";
			if($todas){
				echo "<option value=\"\"";
				echo ($val=="") ? " selected" : "";
				echo ">Todas las Dependencias</option>";
			}

			$id_dependencia=NULL;
			$script = "<script language=\"JavaScript\">\r\n";
			$script .= "dependenciaDes = new Array()\r\n";
			$script .= "dependenciaVal = new Array()\r\n";
			$i=0; 
			while ($row = $rs->FetchRow()) {
				if($id_dependencia!=$row[1]){
					$id_dependencia=$row[1];
					$i=0;
					echo "<option value=\"{$row[1]}\"";
					echo ($val==$row[1]) ? " selected" : "";
					echo ">{$row[2]}</option>";
					$script .= "dependenciaDes[{$id_dependencia}] = new Array()\r\n";
					$script .= "dependenciaVal[{$id_dependencia}] = new Array()\r\n";
				}
				$script .= "dependenciaDes[${id_dependencia}][${i}]='{$row[3]}'\r\n";
				$script .= "dependenciaVal[${id_dependencia}][${i}]='{$row[0]}'\r\n";
				$i++;
			}
			$script .= "</script>\r\n";
			echo "</select>";
			unset($row);
			$rs->Close(); # optional
		}
		echo $script;
	}
	
	/* ---------------------------------------------------------------------- */
	/* Funcion para Insertar Campo Subdependencia en Formulario (1 ComboList) */
	/* ---------------------------------------------------------------------- */
	
	function insertaSubdependenciaFrm($select='id_subdependencia',$onChangeSubmit=false){
		echo "<select name=\"${select}\" class=\"ip-login contenido\"";
		echo ($onChangeSubmit) ? ' onChange="submitForm()"' : '';
		echo "></select>";
	}
	
	/* --------------------------------------------------------------------------- */
	/* Funcion para Insertar Campo Indicador Activo en Formulario (2 RadioButtons) */
	/* --------------------------------------------------------------------------- */
	
	function insertaIndActivoFrm($val='t',$name='ind_activo'){
?>
	<table width="100" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
		<td class="texto" width="50"> 
		  <input type="radio" name="<?=$name;?>" value="t"<? echo ($val=='t'||empty($val)) ? " checked" : "";?>>
		  Si </td>
		<td class="texto" width="50"> 
		  <input type="radio" name="<?=$name;?>" value="f"<? echo ($val=='f') ? " checked" : "";?>>
		  No </td>
	  </tr>
	</table>
<?
	}
	
	/* ------------------------------------------------------------ */
	/* Funcion para Insertar Campo Usuarios en Formulario (Options) */
	/* Lista todos los usuarios de una dependencia o de todas ellas */
	/* ------------------------------------------------------------ */
	
	function ObjFrmSelItemChange($sql_st1,$sql_st2,$selName,$selLabel,$selSize,$selClass,$frmName,$none=NULL,$fetch=true,$ucwords=true){
		$html = new Smarty;

		$html->assign_by_ref('frmName',$frmName);
		$html->assign('script',$this->JSItemsChange());
		
		$html->assign('selName1',$selName[0]);
		$html->assign('selName2',$selName[1]);
		$html->assign('selLabel1',$selLabel[0]);
		$html->assign('selLabel2',$selLabel[1]);
		$html->assign_by_ref('selSize',$selSize);
		$html->assign_by_ref('selClass',$selClass);
		$html->assign('none',is_array($none) ? $none : false);
		
		$html->assign('selContent1',$this->ObjFrmSelect($sql_st1,NULL,true,$ucwords));
		$html->assign('selContent2',($sql_st2) ? $this->ObjFrmSelect($sql_st2,NULL,true,$ucwords) : NULL);
				
		if($fetch)
			return $html->fetch('objsform/ObjFrmSelItemChange.tpl.php');
		else
			$html->display('objsform/ObjFrmSelItemChange.tpl.php');		
	}
	
	function insertaUsuariosFrm($id=NULL){

		$this->abreConnDB();
		$sql_st  = ($id) ? "SELECT cod_trabajador " : "SELECT cod_usuario ";
		$sql_st .= ($id) ? "FROM trabajador " : "FROM usuario ";
		$sql_st .= ($id) ? "WHERE id_subdependencia=${id} and ind_activo is true " : "";
		$sql_st .= "ORDER BY 1";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$numrows = $rs->RecordCount();
			while ($row = $rs->FetchRow()) {
				$tmp_option .= "<option value=\"{$row[0]}\">{$row[0]}</option>";
			}
			unset($row);
			$rs->Close(); # optional
		}
		return array(0 => "${numrows}", 1 => "${tmp_option}");
	}
	
	/* ------------------------------------------------------------ */
	/* Funcion para Mostrar los Usuarios con los codigos que		*/
	/* contiene un dato de tipo Array (Options)						*/
	/* ------------------------------------------------------------ */
	
	function consultaUsuariosGrupoFrm($cod_usuario,$id_grupo=NULL){
		$this->abreConnDB();

		if(is_null($id_grupo))
			$sql_st  = "SELECT cod_usuario ".
					   "FROM usuario ".
					   "WHERE cod_usuario IN ('" . implode("','",$cod_usuario) . "') ".
					   "ORDER BY 1";
		else
			$sql_st = "SELECT cod_usuario ".
					  "FROM grupo_usuario ".
					  "WHERE id_grupo=${id_grupo} ".
					  "ORDER BY 1";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$numrows = $rs->RecordCount();
			while ($row = $rs->FetchRow()) {
				echo "<option value=\"{$row[0]}\">{$row[0]}</option>";
			}
			unset($row);
			$rs->Close(); # optional
		}
	}

	/* ------------------------------------------------------------ */
	/* Funcion para Mostrar los Usuarios de un Modulo con los 		*/
	/* codigos que contiene un dato de tipo Array (Options)			*/
	/* ------------------------------------------------------------ */
	
	function consultaUsuariosModuloFrm($cod_usuario,$id_modulo=NULL){
		$this->abreConnDB();

		if(is_null($id_modulo))
			$sql_st  = "SELECT cod_usuario ".
					   "FROM usuario ".
					   "WHERE cod_usuario IN ('" . implode("','",$cod_usuario) . "') ".
					   "ORDER BY 1";
		else
			$sql_st = "SELECT cod_usuario ".
					  "FROM modulo_derecho ".
					  "WHERE id_modulo=${id_modulo} and cod_usuario IS NOT NULL ".
					  "ORDER BY 1";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$numrows = $rs->RecordCount();
			while ($row = $rs->FetchRow()) {
				echo "<option value=\"{$row[0]}\">{$row[0]}</option>";
			}
			unset($row);
			$rs->Close(); # optional
		}
	}
	
	/* ------------------------------------------------------ */
	/* Funcion para Mostrar un Select Universal 			  */
	/* en base a una Tabla de la Base de Datos (Select)		  */
	/* ------------------------------------------------------ */

	function ObjFrmSelect($sql_st, $val=NULL, $fetch=false, $ucwords=false, $none=NULL, $nrela=1){
		$jscript = '';
		$this->abreConnDB();
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$cont = 0;
			while ($row = $rs->FetchRow()) {
				$options[$cont]['val'] = $row[0];
	//			$options[$cont]['label'] = ($ucwords) ? ucwords(strtolower($row[1])) : $row[1]; 09/04/2013
				$options[$cont]['label'] = ($ucwords) ? strtoupper($row[1]) : $row[1];
				$options[$cont]['sel'] = ($row[0]==$val) ? 'selected' : '';
				if(array_key_exists(2,$row)){
					$jscript .= ($cont == 0) ? "\r\n<script language=\"JavaScript1.2\">\r\noptions{$nrela} = new Array()\r\n" : '';
					$jscript .= "options{$nrela}['{$row[0]}'] = '" . ucfirst($row[2]) . "'\r\n";    
		//			$jscript .= "options{$nrela}['{$row[0]}'] = '" . $row[2] . "'\r\n";   09/04/2013
					
				}
				$cont++;
			}
			$jscript .= (!empty($jscript)) ? '</script>' : '';
			unset($row);
			$rs->Close();
		}
		
		$html = new Smarty;
		$html->assign('none',is_array($none) ? $none : false);
		$html->assign_by_ref('jscript',$jscript);
		$html->assign_by_ref('options',$options);
		if($fetch)
			return $html->fetch('objsform/select.tpl.php');
		else
			$html->display('objsform/select.tpl.php');		
	}	

	function muestraSelectUniversal($nameSel, $sql_st, $val=NULL, $onChange=false, $none=false, $class="ip-login contenido", $label="(Seleccione una)"){
		$this->abreConnDB();
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			// $numrows = $rs->RecordCount();
			$result = "<select name=\"${nameSel}\" class=\"${class}\"";
			$result .= ($onChange) ? " onChange=\"submitForm()\">" : ">";
			$result .= ($none) ? "<option value=\"NULL\"> $label </option>" : '';
			while ($row = $rs->FetchRow()) {
				$result .= "<option value=\"{$row[0]}\"";
				$result .= ($row[0]==$val) ? ' selected' : '';
				$result .= ">".ucwords($row[1])."</option>";
			}
			$result .= "</select>";
			unset($row);
			$rs->Close(); # optional
		}
		return $result;		
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar todos los Modulos en Forma Jerarquica	 */
	/* ------------------------------------------------------------- */
	
	function insertaSelectModulo($nameSel, $val=NULL, $tipModulo=1, $none=true, $onChange=false){
		$this->abreConnDB();
		// Obtiene los datos de Cada Modulo
		$sql_st = "SELECT id_modulo, des_titulocorto, num_profundidad ".
				  "FROM modulo ".
				  "WHERE ind_activo IS TRUE and id_modulotipo=${tipModulo} ".
				  "ORDER BY des_raiz || text(id_modulo)";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			echo "<select name=\"${nameSel}\" class=\"ip-login contenido\">";
			echo ($none) ? '<option value="">Ninguno</option>' : '';
			while($row = $rs->FetchRow()){
				echo "<option value=\"{$row[0]}\"";
				echo ($row[0]==$val) ? ' selected' : '';
				echo ">" . str_repeat('&nbsp;',($row[2]*2)) . $row[1] . "</option>";
			}
			echo "</select>";
			$rs->Close();
			unset($rs);
		}
	}

	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar todos los Target permitidos de un Link	 */
	/* ------------------------------------------------------------- */
	
	function insertaSelectLinkTarget($nameSel, $val=NULL, $onChange=false){
		echo "<select name=\"${nameSel}\" class=\"ip-login contenido\"";
		echo ($onChange) ? " onChange=\"submitForm()\">" : ">";
		for($i=0;$i<count($this->targetLink);$i++){
			echo "<option value=\"{$this->targetLink[$i]}\"";
			echo ($this->targetLink[$i]==$val) ? ' selected' : '';
			echo ">" . $this->targetLink[$i] . "</option>";
		}
		echo "</select>";
	}
	
	/* ------------------------------------------------------ */
	/* Funcion para Mostrar un RadioButton Universal 		  */
	/* dentro de una tabla HTML personalizada				  */
	/* en base a una Tabla de la Base de Datos 				  */
	/* ------------------------------------------------------ */
	
	function ObjFrmRadioButton($sql_st, $radName, $val=NULL, $nCols=2, $fetch=false, $ucwords=false, $none=NULL){
		$this->abreConnDB();
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$nItems = $rs->RecordCount();
			$cont = 0;
			while ($row = $rs->FetchRow()) {
				$options[$cont]['val'] = $row[0];
				$options[$cont]['label'] = ($ucwords) ? ucwords(strtolower($row[1])) : $row[1];
				$options[$cont]['check'] = ($row[0]==$val) ? 'checked' : '';
				$cont++;
			}
			unset($row);
			$rs->Close();
			unset($rs);
		}
		$html = new Smarty;
		$html->assign('radName',$radName);
		$html->assign('nCols',$nCols);
		$html->assign('nRows',ceil($nItems/$nCols));
		$html->assign('nItems',$nItems);
		$html->assign('options',$options);
		if($fetch)
			return $html->fetch('objsform/ObjFrmRadioButton.tpl.php');
		else
			$html->display('objsform/ObjFrmRadioButton.tpl.php');		
	}
	
	function MenuSearchPaginable($numResults, $page, $frmName, $frmAccion, $fetch=false){
		$numPages=ceil($numResults/$this->numMaxResultsSearch);
		if($numPages>1){
			// Setea el Numero de Pagina donde apuntara el recorrido Izquierdo
			if($page>$this->numMaxPagesSearch)
				$numPageIzq = $this->numMaxPagesSearch*floor(($page-1)/$this->numMaxPagesSearch);
			// Forma el Array contenedor de los Numeros de Pagina a mostrar
			$ini = ($page<=$this->numMaxPagesSearch) ? 1 : $numPageIzq+1;
			$top = ( ($numPages>$this->numMaxPagesSearch) && (($ini+$this->numMaxPagesSearch) < $numPages) ) ? ($ini+($this->numMaxPagesSearch-1)) : $numPages;
			for($i=$ini;$i<$top+1;$i++)
				$arrNumPages[] = $i;
			// Setea el Numero de Pagina donde apuntara el recorrido Derecho
			if($top<$numPages)
				$numPageDer = $top+1;
			
			// Genera HTML
			$html = new Smarty;
			$html->assign_by_ref('frmName',$frmName);
			$html->assign_by_ref('frmAccion',$frmAccion);
			$html->assign('numPageIzq',isset($numPageIzq) ? $numPageIzq : false);
			$html->assign_by_ref('numPage',$page);
			$html->assign_by_ref('arrNumPages',$arrNumPages);
			$html->assign('numPageDer',isset($numPageDer) ? $numPageDer : false);
			
			// Obtiene Menu y SubMenu por Default si los hubiese
			$html->assign_by_ref('menuPager',$this->menuPager);
			$html->assign_by_ref('subMenuPager',$this->subMenuPager);
			
			if($fetch)
				return $html->fetch('objsform/menuSearchPaginable.tpl.php');
			else
				$html->display('objsform/menuSearchPaginable.tpl.php');		
		}else
			return false;
	}

	function ObjMenuSearchPager($numResults, $page, $frmName, $frmAccion, $fetch=false){
		return $this->MenuSearchPaginable($numResults, $page, $frmName, $frmAccion, $fetch);
	}
}
?>