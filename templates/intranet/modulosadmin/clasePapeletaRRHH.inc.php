<?
require_once('clasePapeletas.inc.php');

class PapeletasRRHH extends Papeletas{

	// Atributos de la Aplicacion
	var $idPap;
	var $observacionPap;
	
	var $userReporte;
	var $mesReporte;
	var $anyoReporte;

	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* CLASE PAPELETAS DEPENDENCIA									 */
	/* ------------------------------------------------------------- */
	function PapeletasRRHH($menu){
		// Asigna Parametros de Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][13];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Asigna Valores al Array de Datos de Usuario
		$this->datosUsuarioMSSQL();

		// Acciones Permitidas en la Aplicacion
		$this->arr_accion = array(
							MUESTRA_SUMARIO => 'showSumary',
							MUESTRA_EN_CURSO => 'showEnCurso',
							FRM_CALIFICA => 'frmCalifica',
							CALIFICA => 'califica',
							FRM_CALIFICA_DOCTOR => 'frmCalifica_Doctor',
							CALIFICA_DOCTOR => 'califica_Doctor',
							FRM_BUSCA_PAPELETA => 'frmSearch',
							BUSCA_PAPELETA => 'searchPapeleta',
							FRM_REPORTE => 'frmReporte',
							GENERA_REPORTE_ORHH => 'newReport',
							IMPRIMIR_PAPELETA => 'printPapeleta',
							MUESTRA_OBSERVACION => 'showObservacion'
							);
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array (val => 'showSumary', label => 'SUMARIO'),
							1 => array (val => 'frmSearch', label => 'BUSCAR'),
							2 => array (val => 'frmReporte', label => 'REPORTE')
							);
		
		// Accion Realizada
		$this->menuPager = $menu;
	}
	
	function CantidadPapeletaPorEstado($stat){
		$this->abreConnDB();
		$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.papeleta ".
				  "WHERE id_estado=$stat ".
				  		"--and fecha_inicio > getdate()";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$row = $rs->FetchRow();
			$rs->Close();
		}
		return $row[0];	
	}
	
	function MuestraSumario(){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('statPapeleta',$this->statPapeleta);
		
		$html->assign('nAprobado',$this->CantidadPapeletaPorEstado($this->statPapeleta[APROBADO]));
		$html->assign('nDesaprobado',$this->CantidadPapeletaPorEstado($this->statPapeleta[DESAPROBADO]));
		
		$html->display('papeletaRRHH/sumario.tpl.php');
	}
	
	function MuestraIndex(){
		$this->MuestraSumario();
	}

	function FrmBuscaPapeleta($depPap=NULL,$mesPap=NULL,$statPap=NULL,$pap=NULL){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Select de la Dependencia
		$sql_st  = "SELECT codigo_dependencia, lower(SUBSTRING(dependencia,1,60)) ".
				   "FROM db_general.dbo.h_dependencia ".
				   "WHERE condicion='ACTIVO' ".
				   "ORDER BY 2";
		$html->assign('selDepPap',$this->ObjFrmSelect($sql_st, $depPap, true, true, array('val'=>'none','label'=>'Todas las Dependencias')));
		unset($sql_st);
		// Setea Campos del Formulario
		$html->assign('selMesPap',$this->ObjFrmMes(1, date('n'), true, ($mesPap) ? $mesPap : date('n'), true, array('val'=>'none','label'=>'Todos')));
		
		$sql_st = "SELECT id_estado, des_estado FROM papeleta_estado WHERE ind_activo=1 and id_estado<>{$this->statPapeleta[EN_CURSO]}";
		$html->assign('selStatPap',$this->ObjFrmSelect($sql_st, $statPap, true, true, array('val'=>'none','label'=>'Cualquiera')));
		$html->assign('nPap',(is_array($pap)) ? count($pap) : 0);
		$html->assign_by_ref('pap',$pap);
		$html->display('papeletaRRHH/frmBuscar.tpl.php');

	}
	
	function BuscaPapeleta($depPap,$mesPap,$statPap,$actual=false){
		$this->abreConnDB();
		
		
		$codigo_trabajador= $this->userIntranet['CODIGO'];
		//if($codigo_trabajador==2558)
		if($codigo_trabajador==2558 || $codigo_trabajador==710)
		{
		$str_motivo_ausencia="  p.codigo_motivo_ausencia='0003' and ";
			
		}else{
		$str_motivo_ausencia='';
		}
		
		
		$sql_st = sprintf("SELECT p.id_papeleta, UPPER(m.desc_motivo_ausencia), ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".
						 "p.id_estado, e.des_estado, t.apellidos_trabajador+' '+t.nombres_trabajador, lower(d.dependencia), p.codigo_motivo_ausencia ".
				  "FROM dbo.papeleta p, dbo.papeleta_estado e, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d ".
				  "WHERE 
				  ".$str_motivo_ausencia."
				  p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default and p.codigo_trabajador=t.codigo_trabajador and t.coddep=d.codigo_dependencia and ".
						"p.id_estado=e.id_estado and DATEPART(year,p.fecha_inicio) = DATEPART(year,getdate())".
						"%s%s%s%s ".
				  "ORDER BY 7,6,p.fecha_inicio DESC",
				  ($statPap!='none') ? " and p.id_estado={$statPap}" : ' and p.id_estado<>1',
				  ($depPap!='none'&&$depPap) ? " and t.coddep={$depPap}" : '',
				  ($mesPap!='none'&&$mesPap) ? sprintf(" and DATEPART(month,p.fecha_inicio)=%d",$mesPap) : '',
				  ($actual) ? ' --and p.fecha_inicio > getdate()' : '');
				  if($codigo_trabajador==710)
				  {
				//  echo $sql_st;
				  }
				  
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$cont=0;
			while ($row = $rs->FetchRow()){
				$pap[$cont] = array('dependencia'=>ucwords($row[6]),
									'trabajador'=>$row[5],
									'motivo'=>$row[1],
							        'fecha'=>$row[2],
							        'estado'=>$row[4]
							);
							if($codigo_trabajador==710)
							{
							//echo 'estado'.$row[3];
							}
							$str_pre=0;
							if($row[3]==1){ $str_pre=1; }
							if($row[3]==0){ $str_pre=1; }							
							if($row[3]==2){ $str_pre=1; }
							if($row[3]==3){ $str_pre=1; }
							if($row[3]==5){ $str_pre=1; }
							
														
														//echo $row[0].'--'.$row[3].'<br>';
				if($row[3]==2){
/*					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";
*/

				//echo '---'.$row[7].'<br>';
					if($row[7]=='0003')
					{
					
							if($codigo_trabajador==2558|| $codigo_trabajador==710)
							{
							$pap[$cont]['imgStat'] = 'stat_none.gif';
							$pap[$cont]['imgOpcion'] = 'ico_calificar.gif';
							$pap[$cont]['altOpcion'] = 'Calificar Cita Med.';
							$pap[$cont]['lnkOpcion'] = "{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[FRM_CALIFICA_DOCTOR]}&id={$row[0]}&menu=showEnCurso";
							}else{

									$pap[$cont]['imgStat'] = 'stat_none.gif';
									$pap[$cont]['imgOpcion'] = 'ico-info.gif';
									$pap[$cont]['altOpcion'] = 'Pendiente en Doctor';
									$pap[$cont]['lnkOpcion'] = "#";
							
							}
							
					}else{
					$pap[$cont]['imgStat'] = 'stat_none.gif';
					$pap[$cont]['imgOpcion'] = 'ico_calificar.gif';
					$pap[$cont]['altOpcion'] = 'Calificar';
					$pap[$cont]['lnkOpcion'] = "{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[FRM_CALIFICA]}&id={$row[0]}&menu=showEnCurso";
					}
					
				}elseif($row[3]==5){

					$pap[$cont]['imgStat'] = 'stat_none.gif';
					$pap[$cont]['imgOpcion'] = 'ico_calificar.gif';
					$pap[$cont]['altOpcion'] = 'Calificar';
					$pap[$cont]['lnkOpcion'] = "{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[FRM_CALIFICA]}&id={$row[0]}&menu=showEnCurso";


				}elseif($row[3]==2){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}&prev=".$str_pre."','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

				}elseif($row[3]==4){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";					
					
				}elseif($row[3]==3){
					$pap[$cont]['imgStat'] = 'stat_false.gif';
					$pap[$cont]['imgOpcion'] = 'ico_doc.gif';
					$pap[$cont]['altOpcion'] = 'Ver Observaciones';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[MUESTRA_OBSERVACION]}&id={$row[0]}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=220,height=150'); void('')";;
				}
				$cont++;
			}
			unset($row);
			$rs->Close();
			unset($rs);			
		}
		
		$this->FrmBuscaPapeleta($depPap,$mesPap,$statPap,$pap);
	}
	
	function FrmReportePapeleta($mesRep=NULL,$userRep='none',$depRep=NULL){
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		// Limpia el Valor de los Parametros
		$mesRep = ($mesRep) ? $mesRep : date('n');
		$userRep = ($userRep) ? $userRep : 'none';
		$depRep = ($depRep) ? $depRep : 9;

		$xid_option_selecc=$_REQUEST['xid_option_selecc'];
		if($xid_option_selecc==''){$xid_option_selecc=0;}
		$year_sel=$_REQUEST['year_sel'];
		
		$userRep_sel=$_REQUEST['user'];
		
		if($year_sel==''){$year_sel=date('Y');}		
		$year_sel_hoy=date('Y');;
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Select de la Dependencia
		$sql_st  = "SELECT codigo_dependencia, lower(substring(dependencia,1,55)) ".
				   "FROM db_general.dbo.h_dependencia ".
				   " WHERE condicion='ACTIVO' ".
				   "ORDER BY 2";
		  // echo $sql_st; //--WHERE condicion='ACTIVO'
		
		$html->assign('selDepRep',$this->ObjFrmSelect($sql_st, $depRep, true, true));
		unset($sql_st);
		// Setea el Select de los Trabajadores
		$trabajador_op='';
				$trabajador_op.= '<option value="none" ';
				if($userRep_sel=='none'){ $trabajador_op.= ' selected '; }
				$trabajador_op.= '>'.'Todos los Trabajadores'.'</option>';		
				
		$sql_st  = sprintf("SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
						   "FROM db_general.dbo.h_trabajador ".
						   "WHERE estado='ACTIVO' and coddep=%d  ".
						   "ORDER BY 2",
						   $depRep);
						//   --and condicion='NOMBRADO'
		$rs = & $this->conn->Execute($sql_st);		
		unset($sql_st);
			
		if (!$rs)
			{//print 'error : '.$this->conn->ErrorMsg().'<BR>';
			}else{
			$numrows = $rs->RecordCount();
				while ($row = $rs->FetchRow())
				{
	
					$trabajador_op.= '<option value="'.$row[0].'" ';
					if($row[0]==$userRep_sel){ $trabajador_op.= ' selected '; }
					$trabajador_op.= '>'.$row[1].'</option>';
	
				}
			$rs->Close();
		}
						
		//echo $sql_st;
		//$html->assign('selUser',$this->ObjFrmSelect($sql_st, $userRep, true, true, array('val'=>'none','label'=>'Todos los Trabajadores')));
		//echo $trabajador_op;
		
		$html->assign_by_ref('selUsuario',$trabajador_op);
		unset($sql_st);
		
		if($year_sel_hoy>$year_sel){ $mes_lim=12; }else{ $mes_lim=date('n'); }		
		$html->assign_by_ref('selMesRep',$this->ObjFrmMes(1, $mes_lim, true, ($mesRep) ? $mesRep : date('n'), true));

			$year_sel_op='';
			for ($i = 2010; $i <= $year_sel_hoy; $i++) 
			{			
				$year_sel_op.= '<option value="'.$i.'" ';
				if($i==$year_sel){ $year_sel_op.= ' selected '; }
				$year_sel_op.= '>'.$i.'</option>';
			}
			
		$html->assign_by_ref('selyearsel',$year_sel_op);

	
		$html->assign('selUser',$this->ObjFrmSelect($sql_st, $userRep, true, true, array('val'=>'none','label'=>'Todos los Trabajadores')));



		$html->assign_by_ref('xid_option_selecc',$xid_option_selecc);		
		
		$rep = & $this->ListaReportesGenerados($mesRep,$userRep,$depRep);
		$html->assign('nRep',(is_array($rep)) ? count($rep) : 0);
		
				$html->assign_by_ref('mesRep_sel',$mesRep_sel); //mes_seleccionado
				$html->assign_by_ref('depRep_sel',$depRep); //dep_seleccionado				
				$html->assign_by_ref('userRep_sel',$userRep); //user_seleccionado				
				$html->assign_by_ref('year_sel',$year_sel);

		$finicial_rep=$_REQUEST['finicial_rep'];
		$ftermino_rep=$_REQUEST['ftermino_rep'];		

				$html->assign_by_ref('finicial_rep',$finicial_rep);
				$html->assign_by_ref('ftermino_rep',$ftermino_rep);				
				
				

		
		$html->assign_by_ref('rep',$rep);
		$html->display('papeletaRRHH/frmReporte.tpl.php');
	}
	
	function ListaReportesGenerados($mesRep,$userRep,$depRep){
		$this->abreConnDB();
		
		$sql_st = sprintf("SELECT filename,MONTH(FEC_INICIO),YEAR(FEC_INICIO),CONVERT(varchar(10),fecha_creacion,101)+' '+CONVERT(varchar(8),fecha_creacion,108), ".
				  "lower(t.apellidos_trabajador)+' '+lower(t.nombres_trabajador), fecha_creacion, lower(d.dependencia) ".
				  "FROM dbo.papeleta_reporte r, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d ".
				  "WHERE t.codigo_trabajador=r.codigo_trabajador and ind_RRHH=1 and t.coddep=d.codigo_dependencia and ".
				  		"r.codigo_dependencia={$depRep}%s%s and r.usr_audit={$this->userIntranet[CODIGO]} 
						and CONVERT(int,YEAR(FEC_INICIO))=DATEPART(year,getdate()) ".
				  "UNION ".
				  "SELECT filename,MONTH(FEC_INICIO),YEAR(FEC_INICIO),CONVERT(varchar(10),fecha_creacion,101)+' '+CONVERT(varchar(8),fecha_creacion,108), ".
				  "NULL, fecha_creacion, lower(d.dependencia) ".
				  "FROM dbo.papeleta_reporte r, db_general.dbo.h_dependencia d ".
				  "WHERE codigo_trabajador IS NULL and ind_RRHH=1 and r.codigo_dependencia=d.codigo_dependencia and ".
				  		"r.codigo_dependencia={$depRep}%s%s 
						and r.usr_audit={$this->userIntranet[CODIGO]} 
						and 
						CONVERT(int,YEAR(FEC_INICIO))=DATEPART(year,getdate()) ".
				  "ORDER BY fecha_creacion DESC",
				  
				  

/*
		$sql_st = sprintf("SELECT filename,mes_reporte,anyo_reporte,CONVERT(varchar(10),fecha_creacion,101)+' '+CONVERT(varchar(8),fecha_creacion,108), ".
				  "lower(t.apellidos_trabajador)+' '+lower(t.nombres_trabajador), fecha_creacion, lower(d.dependencia) ".
				  "FROM dbo.papeleta_reporte r, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d ".
				  "WHERE t.codigo_trabajador=r.codigo_trabajador and ind_RRHH=1 and t.coddep=d.codigo_dependencia and ".
				  		"r.codigo_dependencia={$depRep}%s%s and r.usr_audit={$this->userIntranet[CODIGO]} and CONVERT(int,anyo_reporte)=DATEPART(year,getdate()) ".
				  "UNION ".
				  "SELECT filename,mes_reporte,anyo_reporte,CONVERT(varchar(10),fecha_creacion,101)+' '+CONVERT(varchar(8),fecha_creacion,108), ".
				  "NULL, fecha_creacion, lower(d.dependencia) ".
				  "FROM dbo.papeleta_reporte r, db_general.dbo.h_dependencia d ".
				  "WHERE codigo_trabajador IS NULL and ind_RRHH=1 and r.codigo_dependencia=d.codigo_dependencia and ".
				  		"r.codigo_dependencia={$depRep}%s%s and r.usr_audit={$this->userIntranet[CODIGO]} and CONVERT(int,anyo_reporte)=DATEPART(year,getdate()) ".
				  "ORDER BY fecha_creacion DESC",
*/
				  
				  ($userRep!='none') ? " and r.codigo_trabajador={$userRep}" : '',
				  ($mesRep!='none') ? sprintf(" and MONTH(FEC_INICIO)='%s'",$mesRep) : '',
				  ($userRep!='none') ? " and r.codigo_trabajador={$userRep}" : '',
				  ($mesRep!='none') ? sprintf(" and MONTH(FEC_INICIO)='%s'",$mesRep) : '');
		
//		echo $sql_st;
		
		$rs = & $this->conn->Execute($sql_st);
		
		
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$numrows = $rs->RecordCount();
			while ($row = $rs->FetchRow()){
				$fecha = sprintf("Reporte %s del %s",(is_null($row[1])||empty($row[1])) ? '' : " de ".strftime("%B", mktime(0,0,0,$row[1],1,$row[2])),$row[2]);
				$rep[] = array('fecha'=> $fecha,
							   'file'=> $row[0],
							   'fecCreacion'=> strftime("%A, %d de %B del %Y a las %H:%M %Ss",strtotime($row[3])),
							   'trabajador' => ($row[4]) ? ucwords($row[4]) : 'Todos los Trabajadores',
							   'dependencia' => ucwords($row[6])
							  );
			}
			$rs->Close();
		}
		return $rep;
	}
	
	function GeneraReporteRRHH($mesRep,$userRep,$depRep){
		/*$mesRep = ($mesRep=='none') ? false : $mesRep;
		$userRep = ($userRep=='none') ? false : $userRep;
		$depRep = ($depRep=='none') ? false : $depRep;*/


	$dep=$_REQUEST['dep'];		
	$user=$_REQUEST['user'];		
	$xid_option_selecc=$_REQUEST['xid_option_selecc'];		
	$year_sel=$_REQUEST['year_sel'];			
	$mes=$_REQUEST['mes'];		
	$finicial_rep=$_REQUEST['finicial_rep'];
	$ftermino_rep=$_REQUEST['ftermino_rep'];		
		
		
		$this->GeneraReporte_ORHH_GESTION($dep,$user,$xid_option_selecc,$year_sel,$mes, $finicial_rep,$ftermino_rep);
		
		/*if($this->GeneraReportePapeleta($mesRep,$userRep,$depRep,true))
			$this->FrmReportePapeleta($mesRep,$userRep,$depRep);*/
	}




function FrmCalificaPapeleta($idPap){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.apellidos_trabajador + ' ' + t.nombres_trabajador, d.dependencia, m.desc_motivo_ausencia, ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".		
						 "p.destino, p.asunto, CONVERT(varchar(10),p.fecha_envio,103)".
				  "FROM dbo.PAPELETA p, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE p.id_papeleta={$idPap} and t.codigo_trabajador=p.codigo_trabajador and t.coddep=d.codigo_dependencia and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				//$html->assign('menu',$this->GeneraMenuPager());
				//$html->assign_by_ref('menuPap',$this->menuPager);
				
				// Setea Caracteristicas en el Formulario
				$frmName = 'frmCalifica';
				$html->assign_by_ref('frmName',$frmName);
				$html->assign('frmUrl',$_SERVER['PHP_SELF']);
				$html->assign_by_ref('accion',$this->arr_accion);
				$html->assign_by_ref('statPap',$this->statPapeleta);
								
				// Setea Campos del Formulario
				$html->assign('id',$idPap);
				$html->assign('des_nombre',$row[0]);
				$html->assign('des_dependencia',$row[1]);
				$html->assign('des_motivo',$row[2]);
				$html->assign('fecha',$row[3]);
				$html->assign('des_destino',$row[4]);
				$html->assign('des_asunto',$row[5]);
				$html->assign('fec_envio',$row[6]);
				
				$html->display('papeletas/rrhh/frmCalifica.tpl.php');
			}
			$rs->Close();
		}
	
	}
	


function FrmCalificaPapeletaDoctor($idPap){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.apellidos_trabajador + ' ' + t.nombres_trabajador, d.dependencia, m.desc_motivo_ausencia, ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".		
						 "p.destino, p.asunto, CONVERT(varchar(10),p.fecha_envio,103)".
				  "FROM dbo.PAPELETA p, db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE p.id_papeleta={$idPap} and t.codigo_trabajador=p.codigo_trabajador and t.coddep=d.codigo_dependencia and p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				//$html->assign('menu',$this->GeneraMenuPager());
				//$html->assign_by_ref('menuPap',$this->menuPager);
				
				// Setea Caracteristicas en el Formulario
				$frmName = 'frmCalifica_Doctor';
				$html->assign_by_ref('frmName',$frmName);
				$html->assign('frmUrl',$_SERVER['PHP_SELF']);
				$html->assign_by_ref('accion',$this->arr_accion);
				$html->assign_by_ref('statPap',$this->statPapeleta);
								
				// Setea Campos del Formulario
				$html->assign('id',$idPap);
				$html->assign('des_nombre',$row[0]);
				$html->assign('des_dependencia',$row[1]);
				$html->assign('des_motivo',$row[2]);
				$html->assign('fecha',$row[3]);
				$html->assign('des_destino',$row[4]);
				$html->assign('des_asunto',$row[5]);
				$html->assign('fec_envio',$row[6]);
				
				$html->display('papeletas/rrhh/frmCalifica_Doctor.tpl.php');
			}
			$rs->Close();
		}
	
	}
	
	
	
	function CalificaPapeleta($idPap,$idEstado,$desObs){
		$this->abreConnDB();
		
		
		
		$upd_st = sprintf("EXECUTE dbo.CALIFICA_PAPELETA_ASISTENCIA_ORH %d,%d,'%s',%d",
						  $idPap,
						  $idEstado,
						  $this->PrepareParamSQL($desObs),
						  $this->userIntranet[CODIGO]);
		//echo $upd_st;

		$rs = & $this->conn->Execute($upd_st);
		unset($upd_st);
		if (!$rs)
			print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if (!($statTrans = $rs->FetchRow())) {
				$errors = 'Error en proceso de inserción. Consulte con el <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">Administrador</a>';
				$rs->Close();
			}
			$rs->Close();
		}
		
		switch($statTrans[0]){
			case 'A':
				$errors = "- Error en Inserción de la Papeleta.<br>";
				break;
			default:
				$msg = "<b>La Papeleta se ha calificado de manera Correcta.<br>Un email ha sido enviado a la Oficina de Recursos Humanos, informando el permiso.</b>";
				break;
		}
		
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('msg',($errors) ? $this->muestraMensajeInfo($this->errors.$errors,true) : $this->muestraMensajeInfo($msg,true));
		
		$html->display('papeletas/rrhh/calificaStatus.tpl.php');
	}



	function CalificaPapeletaDoctor($idPap,$idEstado,$desObs){
		$this->abreConnDB();
		
		
		
		$upd_st = sprintf("EXECUTE dbo.CALIFICA_PAPELETA_ASISTENCIA_DOCTOR %d,%d,'%s',%d",
						  $idPap,
						  $idEstado,
						  $this->PrepareParamSQL($desObs),
						  $this->userIntranet[CODIGO]);
		//echo $upd_st;

		$rs = & $this->conn->Execute($upd_st);
		unset($upd_st);
		if (!$rs)
			print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if (!($statTrans = $rs->FetchRow())) {
				$errors = 'Error en proceso de inserción. Consulte con el <a href="mailto:intranet@CONVENIO_SITRADOC.gob.pe">Administrador</a>';
				$rs->Close();
			}
			$rs->Close();
		}
		
		switch($statTrans[0]){
			case 'A':
				$errors = "- Error en Inserción de la Papeleta.<br>";
				break;
			default:
				$msg = "<b>La Papeleta se ha calificado de manera Correcta.<br>Un email ha sido enviado a la Oficina de Recursos Humanos, informando el permiso.</b>";
				break;
		}
		
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('msg',($errors) ? $this->muestraMensajeInfo($this->errors.$errors,true) : $this->muestraMensajeInfo($msg,true));
		
		$html->display('papeletas/rrhh/calificaStatus.tpl.php');
	}



}
?>