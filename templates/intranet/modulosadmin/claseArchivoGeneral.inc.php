<?
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class ArchivoGeneral extends Modulos{

	function ArchivoGeneral($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_BUSCA_ARCHIVO =>'frmSearchAte',
							BUSCA_ARCHIVO =>'searchAte',
							FRM_BUSCA_ARCHIVO_DEPE =>'frmSearchArchivoxDepe',
							BUSCA_ARCHIVO_DEPE =>'searchArchivoxDepe',
							FRM_ARCHIVAR => 'frmArchivaDoc',
							ARCHIVAR => 'archivaDoc',
							MUESTRA_DETALLE_FLUJODOCDIR => 'showDetailFlujoDocDir',
							IMPRIME_DETALLE_DIR => 'printDetailDir',
							FRM_ACEPTA_DOC => 'frmAceptaDoc',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							GENERAREPORTE => 'generaReporte',
							LISTADOTOTAL => 'ListadoTotal',						
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
							1 => array ( 'val' => 'frmSearchArchivoxDepe', label => 'LISTAR' ),
							2 => array ( 'val' => 'frmArchivaDoc', label => 'ARCHIVAR' ),
							3 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTES')
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
	
		$this->pathTemplate = 'oad/archivo/';		
    }
	
	function MuestraStatTrans($accion){

		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_ARCHIVO]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/showStatTrans.inc.php');
		$html->display('oad/footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormBuscaArchivo();
	}
	
	function FormBuscaArchivo($page=NULL,$stt=NULL,$nopc=NULL,$search=false){
		global $trabajador,$dependencia;
		global $nivelIII,$nivelII,$nivelI,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('nopc',$nopc);
		
		$html->assign_by_ref('observaciones',$observaciones);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT codigo_dependencia,dependencia ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todas las Dependencias')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelI 
					WHERE FLAG='A' 
					AND CODDEP =%d 
					order by 2",($dependencia>0) ? $dependencia : 0);
		$html->assign_by_ref('selNivelI',$this->ObjFrmSelect($sql_st, $nivelI, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelII 
					WHERE FLAG='A' 
					AND ID_NIVELI =%d 
					order by 2",($nivelI>0) ? $nivelI : 0);
		$html->assign_by_ref('selNivelII',$this->ObjFrmSelect($sql_st, $nivelII, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);				

		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelIII 
					WHERE FLAG='A' 
					AND ID_NIVELII =%d 
					order by 2",($nivelII>0) ? $nivelII : 0);
		$html->assign_by_ref('selNivelIII',$this->ObjFrmSelect($sql_st, $nivelIII, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display('oad/archivo/headerArm.tpl.php');
		$html->display('oad/archivo/search.tpl.php');
		if(!$search) $html->display('oad/archivo/footerArm.tpl.php');
	}
	
	function BuscaArchivo($page,$stt,$nopc){
		global $trabajador,$dependencia;
		global $nivelIII,$nivelII,$nivelI,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaArchivo($page,$stt,$nopc,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		//$condConsulta[] = "r.usuario=t.email";
		if($nivelI>0&& $nivelI!=""){
			$condTable[] = "NIVELI n1";
			$condConsulta[] = "a.niveli=n1.serie_niveli and n1.id=$nivelI ";
		}
		
		if($nivelII>0&& $nivelI!=""){
			$condTable[] = "NIVELII n2";
			$condConsulta[] = "a.nivelii=n2.serie_nivelii and n1.id=n2.id_niveli and n2.id=$nivelII ";
		}		
		
		if($dependencia>0){
			$condConsulta[] = "a.coddep=$dependencia ";
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		    	        
		$sql_SP = sprintf("EXECUTE sp_busIdArchivo %s,%s,0,0",
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIdArchivo %s,%s,%s,%s",
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosArchivo %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($AtencionData = $rs->FetchRow())
							$html->append('arrAte', array('id' => $id[0],
														  'trab' => ucfirst($AtencionData[0]),
														  'idDocPadre' => $AtencionData[1],
														  'medio' => $AtencionData[2],
														  'indicativo' => ucwords($AtencionData[3]),
														  'obs' => ucwords($AtencionData[4]),
														  'nivel1' => strtoupper($AtencionData[5]),
														  'nivel2' => strtoupper($AtencionData[6]),
														  'nivel3' => strtoupper($AtencionData[7]),
														  'sigla' =>$AtencionData[8],
														  'depe' =>$AtencionData[9],
														  'tec' => ucwords($AtencionData[11]),
														  'fecAcept' => strtoupper($AtencionData[10]),
														  'tipate2' => strtoupper($AtencionData[13]),
														  'tipate3' => strtoupper($AtencionData[14]),
														  'hora' =>$AtencionData[15],
														  'dia' =>$AtencionData[16],
														  'horaini' =>substr($AtencionData[17],0,16),
														  'horafin' =>substr($AtencionData[18],0,16),
														  'pc' =>$AtencionData[19]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ARCHIVO], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/archivo/searchResult.tpl.php');
		$html->display('oad/archivo/footerArm.tpl.php');
	}
	
	function FormBuscaArchivoDependencia($page=NULL,$stt=NULL,$nopc=NULL,$search=false){
		global $trabajador,$dependencia;
		global $nivelIII,$nivelII,$nivelI,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDepe';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('nopc',$nopc);
		
		$html->assign_by_ref('observaciones',$observaciones);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT codigo_dependencia,dependencia ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todas las Dependencias')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelI 
					WHERE FLAG='A' 
					AND CODDEP =%d 
					order by 2",($dependencia>0) ? $dependencia : 0);
		$html->assign_by_ref('selNivelI',$this->ObjFrmSelect($sql_st, $nivelI, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelII 
					WHERE FLAG='A' 
					AND ID_NIVELI =%d 
					order by 2",($nivelI>0) ? $nivelI : 0);
		$html->assign_by_ref('selNivelII',$this->ObjFrmSelect($sql_st, $nivelII, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);				

		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelIII 
					WHERE FLAG='A' 
					AND ID_NIVELII =%d 
					order by 2",($nivelII>0) ? $nivelII : 0);
		$html->assign_by_ref('selNivelIII',$this->ObjFrmSelect($sql_st, $nivelIII, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display('oad/archivo/headerArm.tpl.php');
		$html->display('oad/archivo/dependencia/search.tpl.php');
		if(!$search) $html->display('oad/archivo/footerArm.tpl.php');
	}
	
	function BuscaArchivoDependencia($page,$stt,$nopc){
		global $trabajador,$dependencia;
		global $nivelIII,$nivelII,$nivelI,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDepe';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaArchivoDependencia($page,$stt,$nopc,true);


		$sql_st="select d.id_documento,tup.id_tupa,case when d.id_tipo_documento=1 then d.asunto
			                                                  when d.id_tipo_documento=2 then tup.descripcion
															  when d.id_tipo_documento=4 then d.asunto end,
					  case when d.id_tipo_documento=4 then dbo.buscaReferencia(md.id_movimiento_documento)
					       else d.num_tram_documentario end,case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	             when sol.id_tipo_persona=2 then sol.razon_social 
				 else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
				 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),t.apellidos_trabajador+' '+t.nombres_trabajador
				from documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
				                 left join db_general.dbo.persona sol on d.id_persona=sol.id,
     					movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO'
				WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
					and md.id_dependencia_destino=".$this->userIntranet['COD_DEP']." ORDER BY md.audit_mod desc ";

	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('id' => $row[0],
										   'tu' => $row[1],
										   'tup' => $row[2],
										   'nroTD' => $row[3],
										   'razSoc' => $row[4],
										   'fecIngP' => $row[5],
										   'fecIngD' => $row[6],
										   'trab' => $row[7],
										   'fecT' => $row[8]
										   ));
			$rs->Close();
		}
		unset($rs);

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ARCHIVO], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/archivo/dependencia/searchResult.tpl.php');
		$html->display('oad/archivo/footerArm.tpl.php');
	}	

	function FormArchiva($nivelI,$nivelII,$nivelIII,$errors=false){
		global $ids;//Para la finalizaci�n autom�tica
		
		if(empty($ids)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmArchivaDoc';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('ids',$ids);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelI 
					WHERE FLAG='A' 
					AND CODDEP =%d 
					order by 2",$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('selNivelI',$this->ObjFrmSelect($sql_st, $nivelI, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelII 
					WHERE FLAG='A' 
					AND ID_NIVELI =%d 
					order by 2",($nivelI>0) ? $nivelI : 0);
		$html->assign_by_ref('selNivelII',$this->ObjFrmSelect($sql_st, $nivelII, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);				

		$sql_st = sprintf("SELECT ID,DESCRIPCION 
					FROM dbo.nivelIII 
					WHERE FLAG='A' 
					AND ID_NIVELII =%d 
					order by 2",($nivelII>0) ? $nivelII : 0);
		$html->assign_by_ref('selNivelIII',$this->ObjFrmSelect($sql_st, $nivelIII, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/archivo/dependencia/frmArchiva.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	function Archiva($nivelI,$nivelII,$nivelIII){
		global $ids;//Para la finalizaci�n autom�tica
		
		// Comprueba Valores	
		if(count($ids)==0 || !$ids){ $bobs = true; $this->errors .= 'Debe seleccionar al menos un registro.<br>'; }

		if($bobs){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormArchiva($nivelI,$nivelII,$nivelIII,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
				//Para la finalizaci�n autom�tica de documentos
				if ($ids){
					$ss=count($ids);
					//echo "son datos de docs que se van a finalizar<br>";
							for($q=0;$q<$ss;$q++){
								$sql_SP = sprintf("EXECUTE sp_pasa_al_archivo %d,%d,%d,%d,%d,'%s'",
													$ids[$q],
												  ($nivelI) ? $this->PrepareParamSQL($nivelI) : "NULL",
												  ($nivelII) ? $this->PrepareParamSQL($nivelII) : "NULL",
												  ($nivelIII) ? $this->PrepareParamSQL($nivelIII) : "NULL",
												  $this->userIntranet['COD_DEP'],
												  $_SESSION['cod_usuario']
												  );
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
									$rs->Close();
								}
								unset($rs);
							}
					//$idDoc = $ids[0];
				}
			
			/*
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}&subMenu={$this->subMenu_items[6]['val']}";
			$destination .= ($idFin) ? "&cerrar=T" : "";
			header("Location: $destination");
			exit;
			*/
			/**/
			//echo "matrix";
			//exit;
						$msjAlerta=1;
						$html = new Smarty;
						$html->assign_by_ref('msjAlerta',$msjAlerta);
						//echo "holas";
						$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['BUSCA_ARCHIVO'] . '&menu=SumarioDir';
						//echo $destination;
						header('Location: ' . $destination);

			/**/
			
		}
	}
	
	/**/function busSiglasDocMultV2($dat){//haya las siglas(Dependencia) actual del Documento Externo o Expediente
		$this->abreConnDB();
		$this->conn->debug =false;
		$sql="select Upper(dep.siglas) 
		         from movimiento_documento md, db_general.dbo.h_dependencia dep
				 where md.id_documento=$dat and md.id_oficio is null and md.estado='V'
				  and md.id_dependencia_destino=dep.codigo_dependencia"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				//$depe1=0;
				}
				else{
				
					$i=-1;
					while(!$rs->EOF){
					$i++;
					$arregloP[$i]=$rs->fields[0];
					$rs->MoveNext();
					}

						$depe=$arregloP[0];
						$contador=count($arregloP);
						//echo "el contador".$depe;
						if($contador>0){
							$depe1=$depe;
							for($h=1;$h<$contador;$h++){
								$depe1=$depe1.'-'.$arregloP[$h];
							}
						}//fin del if($contador>0)
					}
			//unset($rs);
		return($depe1);
	}/**/	
	
	function DetallesFlujoDirector($id, $print=false){
	
	    //echo "el tipo de doc es: ".$tipDoc;
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		
		$sql=sprintf("select d.id_documento,d.id_tipo_documento,d.num_tram_documentario,
		                     d.indicativo_oficio,case when d.id_tipo_documento=1 then d.asunto
							                          when d.id_tipo_documento=2 then tup.descripcion 
													  when d.id_tipo_documento=4 then d.asunto end,
							 convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
							 cdi.descripcion,dep1.siglas,dep2.siglas,
							 a.nivel1,a.nivel2,
							 a.nivel3,convert(varchar,a.auditmod,103),convert(varchar,a.auditmod,108),
							 fd.observaciones,convert(varchar,fd.auditmod,103),convert(varchar,fd.auditmod,108),a.usuario,fd.usuario,
							 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),md.avance,
							 d.observaciones,
							 case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	                              when sol.id_tipo_persona=2 then sol.razon_social end,sol.direccion,sol.email,sol.telefono,
							 sol.nro_documento,d.folios,tup.numero_dias,convert(varchar,d.auditmod,103),d.fecha_max_plazo,md.fecha_plazo
					from dbo.clase_documento_interno cdi,
					      documento d  left join db_general.dbo.persona sol on d.id_persona=sol.id
						               left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
									   left join almacen_documento a on d.id_documento=a.id_documento and a.coddep=".$this->userIntranet['COD_DEP']." 					
									   left join finaldoc fd on d.id_documento=fd.id_documento and fd.coddep=".$this->userIntranet['COD_DEP']." ,
						 movimiento_documento md INNER JOIN db_general.dbo.h_dependencia dep1 on md.id_dependencia_origen=dep1.codigo_dependencia
													 LEFT JOIN db_general.dbo.h_dependencia dep2 on md.id_dependencia_destino=dep2.codigo_dependencia
					  where d.id_clase_documento_interno=cdi.id_clase_documento_interno and
					  		d.id_documento=md.id_documento and 
					      d.id_documento=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$idTipoDoc=$rs->fields[1];
			$nroExp=$rs->fields[2];
			$indicativo=$rs->fields[3];
			$asunto=$rs->fields[4];
			$fecRec=$rs->fields[5];
			$claseDoc=$rs->fields[6];
			$depOrigen=$rs->fields[7];
			$depDestino=$this->busSiglasDocMultV2($id);
			$depDestino2=$rs->fields[8];
			//$auditRecibido=$this->buscaAceptacion($id);
			$nivel1=$rs->fields[9];
			$nivel2=$rs->fields[10];
			$nivel3=$rs->fields[11];
			$fec2=$rs->fields[12];
			$hora2=$rs->fields[13];
			$fecha2=$fec2." ".$hora2;
			$observaciones=$rs->fields[14];
			$fec1=$rs->fields[15];
			$hora1=$rs->fields[16];
			$fecha1=$fec1." ".$hora1;
			$user=$rs->fields[17];
			$user2=$rs->fields[18];
			$fecDer=$rs->fields[19];
			$avance=$rs->fields[20];
			$obs=$rs->fields[21];
			$RazonSocial=$rs->fields[22];
			$direccion=$rs->fields[23];
			$email=$rs->fields[24];
			$telefono=$rs->fields[25];
			$ruc=$rs->fields[26];
			$folios=$rs->fields[27];
			$nroDiasTupa=$rs->fields[28];
			$auditmod=$rs->fields[29];
			$fechaMaxPlazo=$rs->fields[30];
			$fechaPlazo=$rs->fields[31];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idTipoDoc',$idTipoDoc);				  
		$html->assign_by_ref('nroExp',$nroExp);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('fecRec',$fecRec);			
		$html->assign_by_ref('claseDoc',$claseDoc);	  
		$html->assign_by_ref('depOrigen',$depOrigen);
		$html->assign_by_ref('depDestino',$depDestino);
		$html->assign_by_ref('depDestino2',$depDestino2);
		$html->assign_by_ref('auditRecibido',$auditRecibido);
		$html->assign_by_ref('nivel1',$nivel1);
		$html->assign_by_ref('nivel2',$nivel2);
		$html->assign_by_ref('nivel3',$nivel3);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('fecha2',$fecha2);
		$html->assign_by_ref('user',$user);
		$html->assign_by_ref('user2',$user2);	
		$html->assign_by_ref('fecDer',$fecDer);
		$html->assign_by_ref('avance',$avance);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('folio',$folios);
		$html->assign_by_ref('nroDiasTupa',$nroDiasTupa);
		//$diasUtiles=$this->obtenerNroDiasUtil($auditmod);
		$html->assign_by_ref('nroDias',$diasUtiles);
		$Retrazo=$diasUtiles-$nroDiasTupa;
		$html->assign_by_ref('Retrazo',$Retrazo);
		$html->assign_by_ref('fechaMaxPlazo',$fechaMaxPlazo);
		$html->assign_by_ref('fechaPlazo',$fechaPlazo);
		
		if(($this->userIntranet['DIRE']==1&& (in_array($this->userIntranet['COD_DEP'],array(1,13,5,16,36,50,2))))||(in_array($this->userIntranet['CODIGO'],array(646,185,935,785,715)))){
			$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
										convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
										convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
										dir.observaciones,mt.link,mt.observaciones,dir.fecha_plazo
									from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
								where  
								d.id_documento=md.id_documento
								and md.id_movimiento_documento=mt.id_movimiento
								and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
								and d.id_documento=%d order by dir.id_dirigido",$this->PrepareParamSQL($id));
			$control=1;
			$html->assign_by_ref('control',$control);
		}elseif($this->userIntranet['COD_DEP']==8&& $this->userIntranet['DIRE']==6){	
			$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
										convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
										convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
										dir.observaciones,mt.link,mt.observaciones,dir.fecha_plazo
									from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
								where md.id_dependencia_destino in (8,10,12,9,45,48) 
								and d.id_documento=md.id_documento
								/*and mt.id_movimiento_tratamiento in (select max(id_movimiento_tratamiento) from movimiento_tratamiento group by id_movimiento)*/
								and md.id_movimiento_documento=mt.id_movimiento
								and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
								and d.id_documento=%d order by dir.id_dirigido",$this->PrepareParamSQL($id));
		}else{
			$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
										convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
										convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
										dir.observaciones,mt.link,mt.observaciones,dir.fecha_plazo
									from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
								where md.id_dependencia_destino=%d 
								and d.id_documento=md.id_documento
								/*and mt.id_movimiento_tratamiento in (select max(id_movimiento_tratamiento) from movimiento_tratamiento group by id_movimiento)*/
								and md.id_movimiento_documento=mt.id_movimiento
								and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
								and d.id_documento=%d order by dir.id_dirigido",$this->userIntranet['COD_DEP'],$this->PrepareParamSQL($id));
		}								
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('doc', array(//'id'=>$id,
										   'nombre'=>$row[0],
										   'avance'=>$row[1],
										   'idOficio' => $this->bucaDetalleDocumentoDir($row[2]),
										   'diaEnvio'=>$row[3],
										   'horaEnvio'=>$row[4],
										   'diaRec'=>$row[5],
										   'horaRec'=>$row[6],
										   'obs'=>$row[7],
										   'link'=>$row[8],
										   'obsSecre'=>$row[9],
										   'fPlazo'=>$row[10]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		$sql="select dep.siglas from finaldoc fd,db_general.dbo.h_dependencia dep where fd.coddep=dep.codigo_dependencia and fd.id_documento=$id";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			
			$sitActual=$rs->fields[0];
			$html->assign_by_ref('sitActual',$sitActual);
		}
		unset($rs);
		
		$sql="select distinct tt.descripcion from doc_tipo_tratamiento dtt,tipo_tratamiento tt
						where dtt.id_documento=$id and dtt.id_tipo_tratamiento=tt.id_tipo_tratamiento";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('trat',array('desc' => $row[0]
										   ));
			$rs->Close();
		}
		unset($rs);
		
		$sql2="select ta.descripcion,a.num_documento_anexo,a.contenido,a.observaciones,a.folios,
		                convert(varchar,a.audit_mod,103)+' '+convert(varchar,a.audit_mod,108)
					from anexo a,tipo_anexo ta where 
					a.id_tipo_anexo=ta.id_tipo_anexo
					and a.id_documento=$id";
		$rs = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('anex',array('tip' => $row[0],
											'num' => $row[1],
											'cont' => $row[2],
											'obs' => $row[3],
											'folio' => $row[4],
											'fecIng' => $row[5]
										   ));
			$rs->Close();
		}
		unset($rs);
		
		$sql="select case when c.id_tipo_correspondencia=1 then 'NOTIFICACI�N'
            when c.id_tipo_correspondencia=2 then 'OTROS' end,
		numero,destinatario,domicilio,
		case when c.id_tipo_mensajeria=0 then 'NACIONAL'
		     when c.id_tipo_mensajeria=1 then 'LOCAL' end,
	      fecEntCourier,case when flag=1 then 'NOTIFICADA'
		                     when flag=2 then 'RECHAZADA'
							 WHEN flag=3 then 'NO PRACTICADA' end
		  ,fecNot,nomPerActo,peracto         
		  from correspondencia c
		  where id_documento=$id";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('noti',array('tipCorrespondencia' => $row[0],
											'nroCor' => $row[1],
											'destinatario' => $row[2],
											'domicilio' => $row[3],
											'tipMensajeria' => $row[4],
											'fecEntCourier' => $row[5],
											'estado' => $row[6],
											'fecNoti' => $row[7],
											'nomPerActo' => $row[8],
											'perActo' => $row[9]
										   ));
			$rs->Close();
		}
		unset($rs);
		
		
		$sql="select tr.descrip_completa,r.nro_resol,r.sumilla,r.fini,r.ffin,r.fpublicacion,convert(varchar,convert(datetime,r.ffirma,101),103)
					from resolucion r,tipo_resolucion tr
					where r.id_tipo_resolucion=tr.id and r.id_documento=$id";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
		/*
			$tipResol=$rs->fields[0];
			$nroResol=$rs->fields[1];
			$sumilla=$rs->fields[2];
			$fInicio=$rs->fields[3];
			$fFin=$rs->fields[4];
			$fPub=$rs->fields[5];
			$fFirma=$rs->fields[6];
			$html->assign_by_ref('tipResol',$tipResol);
			$html->assign_by_ref('nroResol',$nroResol);
			$html->assign_by_ref('sumilla',$sumilla);
			$html->assign_by_ref('fInicio',$fInicio);
			$html->assign_by_ref('fFin',$fFin);
			$html->assign_by_ref('fPub',$fPub);
			$html->assign_by_ref('fFirma',$fFirma);
		*/	
			while ($row = $rs->FetchRow())
				$html->append('resol',array('tipResol' => $row[0],
										   'nroResol' => $row[1],
										   'sumilla' => $row[2],
										   'fInicio' => $row[3],
										   'fFin' => ucfirst($row[4]),
										   'fPub' => ucfirst($row[5]),
										   'fFirma' => $row[6]
										   ));
			$rs->Close();			
		}
		unset($rs);
		

			$sql_st = sprintf("SELECT d.indicativo_oficio,d.asunto,d.observaciones,Upper(dep1.siglas),Upper(dep2.siglas),
		                          Upper(c.descripcion),convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  md.avance,md.fecha_plazo 
							FROM movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                                     left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                                 documento d, dbo.clase_documento_interno c
							WHERE md.id_documento=%d and md.id_oficio>0 and d.id_documento=md.id_oficio and d.id_clase_documento_interno=c.id_clase_documento_interno
						      order by md.audit_mod  
		
							",
							$id);
		
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('Exp',array('ind' => $row[0],
										   'cla' => $row[5],
										   'depo' => $row[3],
										   'depd' => $row[4],
										   'asu' => ucfirst($row[1]),
										   'obs' => ucfirst($row[2]),
										   'fDer' => $row[6],
										   'avance' => $row[7],
										   'fPlazo' => $row[8]
										   ));
			$rs->Close();
		}
		unset($rs);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('FechaActual',ucfirst(strftime("%A, %d de %B del %Y a las ")));
		
			//Para sacar la hora del Servidor SQL
			$sqlH="select substring(convert(varchar,getDate(),108),1,8)";
			$rsH = & $this->conn->Execute($sqlH);
			unset($sqlH);
			if (!$rsH)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rsH->fields[0];
				$html->assign_by_ref('HoraActual',$hora);
			}
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		//$html->display('oad/tramite/showDetallesDoc.tpl.php');
		if($control==1){
			$print ? $html->display('oad/tramite/DinamicReports/ReportToSGandVice/printDetalles.tpl.php') : $html->display('oad/tramite/DinamicReports/ReportToSGandVice/showDetalles.tpl.php');
		}else{
			$print ? $html->display('oad/tramite/showDetalles/printDetallesDoc.tpl.php') : $html->display('oad/tramite/showDetalles/showDetallesDoc.tpl.php');
		}
	}
	
	function bucaDetalleDocumentoDir($dat){
		if($dat>0){
			$this->abreConnDB();
			//$this->conn->debug=true;
			$sql=sprintf("select cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones
						from dbo.clase_documento_interno cdi,documento d 
						  where d.id_clase_documento_interno=cdi.id_clase_documento_interno and
							  d.id_documento=%d",$this->PrepareParamSQL($dat));
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$claseDoc=$rs->fields[0];
				$indicativo=$rs->fields[1];
				$asunto=$rs->fields[2];
				$observaciones=$rs->fields[3];
				$dev=$claseDoc." ".$indicativo."<br>&nbsp;&nbsp;Asunto:".$asunto."<br>&nbsp;&nbsp;Observaciones:".$observaciones;
				//$dev=$claseDoc." ".$indicativo."<br>&nbsp;&nbsp;Asunto:".$asunto;
			}
			return($dev);
		}
	}	
		
 function ImprimeFlujoDocDir($id){
  $this->DetallesFlujoDirector($id, true);
 }
 
	function AceptaDocumento(){
		global $ids;//Para la finalizaci�n autom�tica
		//echo "matrix is here";exit;
			$this->abreConnDB();
			//$this->conn->debug = true;
				//Para la finalizaci�n autom�tica de documentos
				if ($ids){
					$ss=count($ids);
							for($q=0;$q<$ss;$q++){
								$sql_SP = sprintf("EXECUTE FRM_ACEPTA_DOC %d",
													$ids[$q]
												  );
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
									$rs->Close();
								}
								unset($rs);
							}
					//$idDoc = $ids[0];
				}
				//exit;		
			/**/
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}";
			//echo $destination;
			header("Location: $destination");
			exit;
			/**/
			/*
				$objIntranet = new Intranet();
				$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
				$objIntranet->Body('tramite_tit.gif',false,false);
				if($RETVAL){
					echo "<strong>HUBO UN ERROR AL EJECUTAR LA TAREA, S�RVASE INTENTARLO EN BREVES MOMENTOS. GRACIAS</strong><br>";
				}else{
					echo "<font size=1><strong>LA EJECUCI�N DE LA TAREA SE LLEV� A CABO CON �XITO</strong><br></font>";
				} 
				$this->BuscaDocDir($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir);
				$objIntranet->Footer(false);
				exit;
			*/
	}
	
	function FormGeneraReporte($search2=false){
		global $GrupoOpciones1;
		//$this->abreConnDB();
		//$this->conn->debug = true;

		//Manipulacion de las Fechas
		$fecAtencion = ($fecAtencion!='//'&&$fecAtencion) ? $fecAtencion : date('m/Y');
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		
		//Mes y anyo
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecAtencion,0,2), true));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAtencion,3,4), true));

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/archivo/reportes.tpl.php');
		if(!$search2) $html->display('oad/footerArm.tpl.php');
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0 --bottom 0.5cm --top 0.5cm --left 1.0cm --header ... --footer t.1 --headfootsize 9.0 --headfootfont times-roman"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}	
	
	function formateaTexto($cadena){
		$mm=chr(10);
		$nn=chr(13);
		//echo "prueba"; exit;
		$cadena= ereg_replace("'"," ",$cadena);
		$cadena= ereg_replace("''"," ",$cadena);
		$comilla='"';
		$cadena= ereg_replace($comilla," ",$cadena);
		$cadena= ereg_replace($mm," ",$cadena);
		$cadena= ereg_replace($nn," ",$cadena);
		//$cadena= ereg_replace($nn," - ",$cadena);
		return ($cadena);
	}	
	
	function GeneraReporte($GrupoOpciones1,$mes,$anyo){
		switch($GrupoOpciones1){
			case 1:
			$a=1;
			$this->ListadoTotal($a);
			break;
			case 2:
			$a=2;
			$this->ListadoTotal($a);
			break;
		}
	}
	
	function ListadoTotal($a){
		//echo $a."xx";
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$this->abreConnDB();
		//$this->conn->debug = true;
			$sql_st = "  select a.ID,a.ID_DOC_PADRE,a.ID_DOC,a.INDICATIVO,a.OBSERVACIONES,n1.serie_niveli+' '+n1.descripcion,
  		 					n2.serie_nivelii+' '+n2.descripcion,A.NIVELIII,dep.siglas,dep.dependencia,a.audit_aceptacion
  						from ARCHIVO_GENERAL a,DB_GENERAL.dbo.H_DEPENDENCIA dep,dbo.niveli n1,
	     					dbo.nivelii n2 
						where a.coddep=dep.codigo_dependencia
	 	  					AND n1.serie_niveli=a.niveli and n1.coddep=a.coddep
							  and n1.id=n2.id_niveli and n2.serie_nivelii=a.nivelii
		  
							ORDER BY a.id desc";
			
												
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('id' => $row[0],
										   'referencia' => $row[3],
										   //'obs' => $this->formateaTexto($row[4]),
										   'nivelI' => $row[5],
										   'nivelII' => $row[6],
										   'siglas' => $row[8],
										   'fecAcep' => $row[10]
										   ));
			$rs->Close();
		}
		unset($rs);		

		
		$html->assign_by_ref('coddep',$coddep);
		
		if($a==2){
		//setlocale(LC_TIME,$this->_zonaHoraria);
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
		$filename = 'hoja1'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oad/archivo/showListadoPDF.tpl.php'),false,$logo);//true es para vertical,y false para horizontal

		$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
		
		}
		if($a==1){
			//echo $a."matrix<br>";
					// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('oad/archivo/viewExtendArchivo.tpl.php');
			exit;
			
		}
	}
	 
}
?>
