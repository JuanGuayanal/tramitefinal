<?
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class Atenciones extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function Atenciones($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][1];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_AGREGA_ATENCION =>'frmAddAtention',
							AGREGA_ATENCION => 'addAtention',
							FRM_BUSCA_ATENCION =>'frmSearchAte',
							BUSCA_ATENCION =>'searchAte',
							FRM_MODIFICA_ATENCION => 'frmModifyAtention',
							MODIFICA_ATENCION => 'modifyAtention',
							FRM_AGREGA_INFTECNICO =>'frmAddInfTecnico',
							AGREGA_INFTECNICO =>'addInfTecnico',
							FRM_MODIFICA_INFTECNICO =>'frmModInfTecnico',
							MODIFICA_INFTECNICO =>'modInfTecnico',							
							MUESTRA_DETALLE => 'showDetail',
							IMPRIMIR_DOCUMENTO => 'printDocumento',
							IMPRIME_INF_TECNICO => 'printInfTecnico',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							REPORTE_ATE1 => 'ReporteAte1',
							REPORTE_ATE2 => 'ReporteAte2',
							REPORTE_ATE3 => 'ReporteAte3',
							GENERAREPORTE => 'GeneraReporte',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		if($this->userIntranet['CODIGO']==646)
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmAddAtention', label => 'AGREGAR' ),
							2 => array ( 'val' => 'frmModifyAtention', label => 'MODIFICAR' ),
							3 => array ( 'val' => 'frmAddInfTecnico', label => 'INF TECNICO' ),
							4 => array ( 'val' => 'frmModInfTecnico', label => 'MODIFICA INFORME' ),
							5 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTES'),
							6 => array ( 'val' => 'frmAddAtention', label => 'AGREGAR' )
							);
		else
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmAddAtention', label => 'AGREGAR' ),
							2 => array ( 'val' => 'frmModifyAtention', label => 'MODIFICAR' ),
							3 => array ( 'val' => 'frmAddInfTecnico', label => 'INF TECNICO' ),
							4 => array ( 'val' => 'frmModInfTecnico', label => 'MODIFICA INFORME' ),
							5 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTES')
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		$this->pathTemplate = 'oti/helpdesk/';		
    }
	
	function ObtieneIP(){
		global $c;
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		$nombre=$_SERVER['REMOTE_HOST'];
		$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);
		//$c=$GLOBALS['COMPUTERNAME'];
		//echo "ggg".$nombre_host."dddd";
		//$a = getenv(REMOTE_USER);
		//$b = get_current_user();
		//$c =phpinfo();
		/*$c =getenv("COMPUTERNAME");
		$d =$_SERVER['COMPUTERNAME'];
		$e=getenv("HTTP_USER_AGENT"); 
		$f=getenv("HTTP_CLIENT_IP");
		*/
		//printf("%s\n",getenv("COMPUTERNAME"));
		//$blah = shell_exec("echo %ComputerName%");
		//$blah = shell_exec("REMOTE_USER");
		//print $blah."justomatrix";
		
		$check = substr($ip, 0, 7); 
		if ($check == $blocked1 || $check == $blocked2) { return; }
		$ip=$ip."/".$nombre."/ ".$nombre2."/ ".$nombre3."/ ".$a."/ ".$b."/ ".$c."/D ".$nombre_host;
		return($ip);
	}

	function MuestraStatTrans($accion){
	global $codReporte;
	global $id;
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		if($codReporte){
		
					$imgStat = 'stat_true.gif';
					$imgOpcion = 'img_printer2.gif';
					$altOpcion = 'Imprimir Reporte';
					$lnkOpcion = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_DOCUMENTO]}&codReporte={$codReporte}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=500'); void('')";

					$html->assign_by_ref('codReporte',$codReporte);
					$html->assign_by_ref('imgOpcion',$imgOpcion);
					$html->assign_by_ref('altOpcion',$altOpcion);
					$html->assign_by_ref('lnkOpcion',$lnkOpcion);
		}
		if($this->userIntranet['CODIGO']=="xxx"){
			$id=4690;
			   /* $desTitulo = " TAMBI�N EL NEGRO FRODO JAJAJA";	
				$desMensaje = "TAMBI�N EL NEGRO FRODO JAJAJA";			
				$eFrom = "CLAUDIA_PAOLA@" . $this->emailDomain;
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				$eDest = "lflores@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
				
				$mail->setFrom($eFrom);
				$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));	*/	
		}		
		if($id){
			$lnkOpcion2 = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIME_INF_TECNICO]}&id={$id}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=500'); void('')";
			$html->assign_by_ref('id',$id);
			$html->assign_by_ref('lnkOpcion2',$lnkOpcion2);
		}

		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_ATENCION]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormBuscaAtencion();
	}
	
	function ImprimeDocumento($codReporte){
	
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$sql_SP = sprintf("EXECUTE sp_busDatosAte3 %d",
								  $codReporte
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('diagnostico',ucfirst($row[7]));
														  $html->assign('indicativo',$row[8]);
														  $html->assign('oficina',$row[9]);
														  $html->assign('liquidacion',$row[4]);
														  $html->assign('tecnico',ucwords($row[11]));
														  $html->assign('tipAte1',ucfirst(strtolower($row[12])));
														  $html->assign('tipAte2',ucfirst(strtolower($row[13])));
														  $html->assign('tipAte3',ucfirst(strtolower($row[14])));
														  $html->assign('ate1',ucfirst(strtolower($row[4])));
														  $html->assign('ate2',ucfirst(strtolower($row[5])));
														  $html->assign('ate3',ucfirst(strtolower($row[6])));
														  $html->assign('modPC',ucfirst(strtolower($row[21])));
														  //'dia' =>$row[16];
														  $html->assign('inicio',substr($row[17],0,16));
														  $html->assign('fin',substr($row[18],0,16));
														  $html->assign('nroPC',$row[19]);
														  $html->assign('numero',$row[22]);
														  $html->assign('problema',$row[23]);
														  $html->assign('trabajo',$row[24]);
														  $html->assign('fecIni',$row[25]);
														  $html->assign('horIni',$row[26]);
														  $html->assign('fecFin',$row[27]);
														  $html->assign('horFin',$row[28]);
														  //'tiempo' => $this->obtieneDif($row[20])			;
				/**/
				// Setea Campos del Formulario
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
				$html->assign_by_ref('logo',$logo);
				
				$html->display($this->pathTemplate . 'reportes/printHojaServicio.tpl.php');
			}
			$rs->Close();
		}
	}

	function ImprimeInfTecnico($id,$nroInforme){
	
		$this->abreConnDB();
		//$this->conn->debug = true;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT t.NOMBRES_TECNICO+' '+t.APELLIDOS_TECNICO
				FROM dbo.h_tecnico t,db_general.dbo.h_trabajador tr,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.usuario=tr.email and tr.codigo_trabajador=t.codigo_trabajador
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$tecnico=$rs2->fields[0];
		}
		
		if($id>0){
			$sql3="SELECT d.asunto,d.observaciones 
					FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
					where r.codigo_reporte=$id and r.id_documento=d.id_documento
					";
		}elseif($nroInforme>0){
			$sql3="SELECT d.asunto,d.observaciones,d.id_documento 
					FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
					where r.id_documento=d.id_documento and d.indicativo_oficio like '%$nroInforme%'
					and year(d.auditmod)=2007 
					";
		}		
		$rs3 = & $this->conn->Execute($sql3);
		unset($sql3);
		if (!$rs3)
			$this->conn->ErrorMsg();
		else{
			$asunto=$rs3->fields[0];
			$observaciones=$rs3->fields[1];
			$codigoDocumento=$rs3->fields[2];
			$html->assign_by_ref('asunto',$asunto);
			$html->assign_by_ref('observaciones',strtoupper($observaciones));
		}
		
		if($id>0){
			$sql="select max(contador) from db_tramite_documentario.dbo.cuentainternot where coddep=13 and id_tipo_documento=4 /*and email='".$_SESSION['cod_usuario']."'*/";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				$this->conn->ErrorMsg();
			else{
				$numero=$rs->fields[0];
				$html->assign_by_ref('numero',$numero);
			}
		}elseif($nroInforme>0){
			$html->assign_by_ref('numero',$nroInforme);
		}
		
		if($id>0){
			$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
											   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
											   dep.dependencia,cp.area_pc,r.codigo_parte
								FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
									 db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
									 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
								WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
									  p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
									  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
									  $id
									  );
		}elseif($nroInforme>0){
			$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
											   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
											   dep.dependencia,cp.area_pc,r.codigo_parte
								FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
									 db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
									 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
								WHERE r.id_documento=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
									  p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
									  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
									  $codigoDocumento
									  );
		}
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));
														  $codigoParte=$row[11];
														  $html->assign('codigoParte',ucfirst($row[11]));
														  
			}
			$rs->Close();				
		}
		
		if($codigoParte>0 && $codigoParte!=""){
		$this->abreConnDB();
		//$this->conn->debug = true;		
			$sql="SELECT codigo_parte, Upper(descrip_dispositivo) as DISPOSITIVO, Upper(descrip_marca_disp) AS MARCA, Upper(mo.descripcion) AS MODELO, Upper(p.numero_serie) AS SERIAL, Upper(p.codigo_patrimonial) AS PATRIMONIAL FROM db_inventario_2002.user_inventario.parte p LEFT JOIN db_inventario_2002.user_inventario.dispositivo_inventario d ON p.codigo_tip_disp=d.codigo_dispositivo LEFT JOIN db_inventario_2002.user_inventario.tipo_dispositivo td ON p.codigo_tipo_dispositivo=td.codigo_tipo_dispositivo LEFT JOIN db_inventario_2002.user_inventario.tipo_tecnologia t ON p.codigo_tipo_tecnologia=t.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=t.codigo_tipo_dispositivo AND p.codigo_tip_disp=t.codigo_dispositivo LEFT JOIN db_inventario_2002.user_inventario.marca_dispositivo ma ON p.codigo_marca_disp=ma.codigo_marca_disp 
					AND p.codigo_tip_disp=ma.codigo_dispositivo LEFT JOIN db_inventario_2002.user_inventario.modelo_dispositivo mo ON p.codigo_modelo=mo.codigo_modelo AND p.codigo_marca_disp=mo.codigo_marca AND p.codigo_tipo_tecnologia=MO.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=mo.codigo_tipo_dispositivo 
					WHERE (p.CODIGO_TIP_DISP=16 or p.CODIGO_TIP_DISP=18) AND p.flag='O' 
					/*AND p.IMP_RED=1*/ 
					AND CODIGO_PARTE=$codigoParte";
			/*Se quita esta condici�n: "p.IMP_RED=1" ya que tambi�n puede ser en base a un scanner*/		
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print 'error : '.$this->conn->ErrorMsg().'<BR>';
			else{
				$DispositivoParte=$rs->fields[1];
				$MarcaParte=$rs->fields[2];
				$ModeloParte=$rs->fields[3];
				$SerialParte=$rs->fields[4];
				$PatrimonialParte=$rs->fields[5];
				$html->assign('DispositivoParte',$DispositivoParte);
				$html->assign('MarcaParte',$MarcaParte);
				$html->assign('ModeloParte',$ModeloParte);
				$html->assign('SerialParte',$SerialParte);
				$html->assign('PatrimonialParte',$PatrimonialParte);
			}					
		}
		//exit;
				// Setea Campos del Formulario
				setlocale(LC_TIME, "spanish");
				$html->assign('fechaGen',ucfirst(strftime("%d de %B del %Y")));
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$html->assign_by_ref('tecnico',$tecnico);
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
				
				$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];		
				$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		
				$filename = 'infTec'.mktime();
					
				$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/printDocumento2.tpl.php'),false,$logo);
				//$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/printImpresora.tpl.php'),false,$logo);
				$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
				header("Content-type: application/pdf");
				header("Location: $destination");
				exit;		
	}


	function FormBuscaAtencion($page=NULL,$stt=NULL,$nopc=NULL,$search=false){
		global $trabajador,$dependencia;
		global $atencion,$tipAte,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('nopc',$nopc);
		
		$html->assign_by_ref('observaciones',$observaciones);
		
		// Contenido Select del T�CNICO
		$sql_st = "SELECT CODIGO_TRABAJADOR,APELLIDOS_TECNICO+' '+NOMBRES_TECNICO ".
					"FROM dbo.h_tecnico ".
					"WHERE ESTADO='ACTIVO' ".
					"AND CODIGO_TRABAJADOR>0  ".
					"order by 2";
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select del Dependencia
		$sql_st = "SELECT codigo_dependencia,dependencia ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todas las Dependencias')));
		unset($sql_st);
		
		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipAte',$this->ObjFrmSelect($sql_st, $tipAte, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipAte) ? $tipAte : 0);
		$html->assign_by_ref('selAtencion',$this->ObjFrmSelect($sql_st, $atencion, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);		

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function obtieneDif($min){
		if($min>=1440){//nro de Dias
			$nroDias=floor($min/1440);
			$resto1=$min%1440;
			$nroHoras=floor($resto1/60);
			$nroMin=$resto1%60;
			$tiempo=$nroDias." dia(s) ".$nroHoras." hora(s) ".$nroMin." min"; 
		}elseif($min<1440&& $min>=60){//nro Horas
			$nroHoras=floor($min/60);
			$nroMin=$min%60;
			$tiempo=$nroHoras." hora(s) ".$nroMin." min";
		}elseif($min<60){//nro Minutos
			$tiempo=$min." min";
		}
		return($tiempo);
	}
	
	function buscaInformeTecnico($id){
		$this->abreConnDB();
		$this->conn->debug = false;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT count(*)
				FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$numero=$rs2->fields[0];
		}
		return($numero);
	}

	function BuscaAtencion($page,$stt,$nopc){
		global $trabajador,$dependencia;
		global $atencion,$tipAte,$observaciones;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaAtencion($page,$stt,$nopc,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$condConsulta[] = "r.usuario=t.email";
		
		if($stt!='T'){
			$condConsulta[] = " r.flag='$stt' ";				
		}
		if($trabajador>0){
			$condConsulta[] = "t.codigo_trabajador=$trabajador ";
		}
		if($dependencia>0){
			$condConsulta[] = "r.codigo_dependencia=$dependencia ";
		}
		
		if($tipAte>0){
			$condTable[] = "dbo.h_reporte_soporte2002 rs";
			$condTable[] = "dbo.h_soporte s";
			$condConsulta[] = "r.codigo_reporte=rs.codig_reporte";
			$condConsulta[] = "rs.codigo_soporte=s.codigo_soporte";
			$condConsulta[] = "s.codigo_tipo_soporte=$tipAte";
			if($atencion>0){
				$condConsulta[] = "s.codigo_soporte=$atencion";
			}
		}
		if($observaciones&& $observaciones!=""){
			$condConsulta[] = "r.descrip_realizado like '%$observaciones%'";
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
	        $this->evaluaNoSql($nopc);

		$sql_SP = sprintf("EXECUTE sp_busIDAte %s,%s,%s,0,0",
							($nopc) ? "'".$this->PrepareParamSQL($nopc)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDAte %s,%s,%s,%s,%s",
								($nopc) ? "'".$this->PrepareParamSQL($nopc)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosAte2 %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($AtencionData = $rs->FetchRow())
							$html->append('arrAte', array('id' => $id[0],
														  'trab' => ucfirst($AtencionData[0]),
														  'dep' => $AtencionData[1],
														  'medio' => $AtencionData[2],
														  'tecnico' => ucwords($AtencionData[3]),
														  'ate1' => strtoupper($AtencionData[4]),
														  'ate2' => strtoupper($AtencionData[5]),
														  'ate3' => strtoupper($AtencionData[6]),
														  'desc' => ucfirst($AtencionData[7]),
														  'flag' =>$AtencionData[8],
														  'depe' =>$AtencionData[9],
														  //'min' =>$AtencionData[10],
														  'min' => $this->obtieneDif($AtencionData[10]),
														  'tec' => ucwords($AtencionData[11]),
														  'tipate1' => strtoupper($AtencionData[12]),
														  'tipate2' => strtoupper($AtencionData[13]),
														  'tipate3' => strtoupper($AtencionData[14]),
														  'hora' =>$AtencionData[15],
														  'dia' =>$AtencionData[16],
														  'horaini' =>substr($AtencionData[17],0,16),
														  'horafin' =>substr($AtencionData[18],0,16),
														  'pc' =>$AtencionData[19],
														  'tiempo' => $this->obtieneDif($AtencionData[20]),
														  'seCreoInfTec' => $this->buscaInformeTecnico($id[0])
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign_by_ref('ip',$this->ObtieneIP());
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ATENCION], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function FormAgregaAtencion($nopc=NULL,$tipo1=NULL,$tipo2=NULL,$tipo3=NULL,$aten1=NULL,$aten2=NULL,$aten3=NULL,$stt=NULL,$obs=NULL,$tiempo=NULL,$errors=false){
		global $nombrePC;
		$nombrePC=($_POST['nombrePC']) ? $_POST['nombrePC'] : $_GET['nombrePC'];		
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		if($nombrePC&& $nombrePC!=""&& $nopc!=""&& $nopc!="0000"){
			$this->abreConnDB();
			//$this->conn->debug = true;		
			$sql="SELECT DISTINCT e.piso+e.siglas/*c.identificador_pc*/,c.codigo_cpu,e.piso
    				from db_inventario_2002.user_inventario.cpu c, db_inventario_2002.user_inventario.pc d,db_general.dbo.h_dependencia e 
    				Where c.area_pc='$nopc' and c.codigo_cpu=d.codigo_cpu and d.codigo_dependencia=e.codigo_dependencia";
	
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$nombreRealPC=$rs->fields[0];
						$nombreRealPC=$nombreRealPC."-".$nopc;
						$piso=$rs->fields[2];
							
							$nombreFisicoPC=$this->ObtieneNombrePC();
							$tamano=strlen($nombreFisicoPC);
							$matrixI=$tamano-4;
							$matrixF=substr($nombreFisicoPC,$matrixI,4);
							$matrixF2=substr($nombreFisicoPC,0,2);
						
						//if($nombrePC!=$nombreRealPC){$error=1;}
						//if($nopc!=$matrixF|| $piso!=$matrixF2){$error=1;}
						if($nopc!=$matrixF){$error=1;}
					}
		}
		if($nopc=="0000"&& ($_SESSION['cod_usuario']=="ujordan" || $_SESSION['cod_usuario']=="jrodriguez" || $_SESSION['cod_usuario']=="obarja")){
			$tamano=strlen($nombrePC);
			$matrixI=$tamano-4;
			$matrixF=substr($nombrePC,$matrixI,2);
			//echo $matrixF."gg";
			if($matrixF=="13")
				$error=2;
		}
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nopc',$nopc);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('tiempo',$tiempo);
		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('nombrePC',$nombrePC);
		$html->assign_by_ref('nombreFisicoPC',$nombreFisicoPC);
		
		$html->assign_by_ref('nombreRealPC',$nombreRealPC);
		$html->assign_by_ref('error',$error);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo1',$this->ObjFrmSelect($sql_st, $tipo1, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo1) ? $tipo1 : 0);
		$html->assign_by_ref('selAten1',$this->ObjFrmSelect($sql_st, $aten1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo2 de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo2',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select 2del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo2) ? $tipo2 : 0);
		$html->assign_by_ref('selAten2',$this->ObjFrmSelect($sql_st, $aten2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo3',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo3) ? $tipo3 : 0);
		$html->assign_by_ref('selAten3',$this->ObjFrmSelect($sql_st, $aten3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddAtention.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function ObtieneCadena(){
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		$nombre=$_SERVER['REMOTE_HOST'];
		$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);

		$check = substr($ip, 0, 7); 
		if ($check == $blocked1 || $check == $blocked2) { return; }
		//$ip=$ip."/".$nombre."/ ".$nombre2."/ ".$nombre3."/ ".$a."/ ".$b."/ ".$c."/D ".$nombre_host;
		
		$tamano=strlen($nombre_host);
		$cadenaI=$tamano-22;
		
		$cadena=substr($nombre_host,$cadenaI,2);		
		
		return($cadena);
	}
	
	function ObtieneNombrePC(){
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		$nombre=$_SERVER['REMOTE_HOST'];
		$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);

		$check = substr($ip, 0, 7); 
		if ($check == $blocked1 || $check == $blocked2) { return; }
		//$ip=$ip."/".$nombre."/ ".$nombre2."/ ".$nombre3."/ ".$a."/ ".$b."/ ".$c."/D ".$nombre_host;
		
		$tamano=strlen($nombre_host);
		//echo "HOLAS".$nombre_host;exit;
		$cadenaI=$tamano-18;
		
		$cadena=substr($nombre_host,0,$cadenaI);		
		
		return($cadena);
	}		

	function AgregaAtencion($nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo){
		global $nombrePC;
		$nombrePC=($_POST['nombrePC']) ? $_POST['nombrePC'] : $_GET['nombrePC'];
		$nopc=($_POST['nopc']) ? $_POST['nopc'] : $_GET['nopc'];

		if($nopc=="0000"&& ($_SESSION['cod_usuario']=="ujordan" || $_SESSION['cod_usuario']=="jrodriguez" || $_SESSION['cod_usuario']=="obarja" || $_SESSION['cod_usuario']=="jcrodriguez" || $_SESSION['cod_usuario']=="jtumay")){
			//echo "matrix";exit;
			$tamano=strlen($nombrePC);
			$matrixI=$tamano-4;
			$matrixF=substr($nombrePC,$matrixI,2);
			//echo $matrixF."gg";exit;
			if($this->ObtieneCadena()=="13"){
				$bAt4 = true; $this->errors .= 'Registre la tarea en una PC que no sea de la OGTIE<br>';
			}
		}
		if($nopc&& $nopc!="0000"){
			$cadena=$this->ObtieneNombrePC();
			
			$this->abreConnDB();
			//$this->conn->debug = true;		
			$sql="SELECT DISTINCT e.piso+e.siglas/*c.identificador_pc*/,c.codigo_cpu,e.piso
    				from db_inventario_2002.user_inventario.cpu c, db_inventario_2002.user_inventario.pc d,db_general.dbo.h_dependencia e 
    				Where c.area_pc='$nopc' and c.codigo_cpu=d.codigo_cpu and d.codigo_dependencia=e.codigo_dependencia";
	
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$nombreRealPC=$rs->fields[0];
						$nombreRealPC=$nombreRealPC."-".$nopc;
						$piso=$rs->fields[2];
		
							$tamano=strlen($cadena);
							$matrixI=$tamano-4;
							$matrixF=substr($cadena,$matrixI,4);
							$matrixF2=substr($cadena,0,2);
							//echo $cadena;
						
						//if($nombrePC!=$nombreRealPC){$error=1;}
						//if($nopc!=$matrixF|| $piso!=$matrixF2){$bAt5 = true; $this->errors .= 'Registre la tarea en una PC del Usuario<br>';}
						if($nopc!=$matrixF){$bAt5 = true; $this->errors .= 'Registre la tarea en una PC del Usuario<br>';}
					}			
		}
		
		//echo "Nombre PC".$nombrePC;exit;
		// Comprueba Valores	
		if(!$nopc){ $bPC = true; $this->errors .= 'El N�mero de la PC debe ser especificado<br>'; }
		if(!$obs){ $bObs = true; $this->errors .= 'La observaci�n debe ser especificado<br>'; }
		if(!$tipo1||$tipo1<0||$tipo1=="none"){ $bTip1 = true; $this->errors .= 'El Tipo de Soporte debe ser especificada<br>'; }
		if(!$aten1||$aten1<0||$aten1=="none"){ $bAt1 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo1>0&&$aten1<1){ $bAt11 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo2>0&&$aten2<1){ $bAt2 = true; $this->errors .= 'La Segunda Atenci�n debe ser especificada<br>'; }
		if($tipo3>0&&$aten3<1){ $bAt3 = true; $this->errors .= 'La Tercera Atenci�n debe ser especificada<br>'; }
		if(strlen($obs)<20){ $bAt6 = true; $this->errors .= 'Debe ingresar una observaci�n superior a los 20 caracteres<br>'; }
		

		if($bPC||$bObs||$bTip1||$bAt1||$bAt11||$bAt2||$bAt3||$bAt4||$bAt5||$bAt6){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaAtencion($nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;

	                // Evalua que no contenga sentencias SQL
        	        $this->evaluaNoSql($nopc);
                	$this->evaluaNoSql($obs);

			$sql_SP = sprintf("EXECUTE sp_insReporte '%s',%d,%d,%d,%d,%d,%d,'%s',%s,%d,'%s'",
							  $this->PrepareParamSQL($nopc),
							  $this->PrepareParamSQL($tipo1),
							  $this->PrepareParamSQL($aten1),
							  ($tipo2) ? $this->PrepareParamSQL($tipo2) : "NULL",
							  ($aten2) ? $this->PrepareParamSQL($aten2) : "NULL",
							  ($tipo3) ? $this->PrepareParamSQL($tipo3) : "NULL",
							  ($aten3) ? $this->PrepareParamSQL($aten3) : "NULL",
							  $this->PrepareParamSQL($stt),
							  ($obs) ? "'".strtoupper($this->PrepareParamSQL($obs))."'" : "NULL",
							  ($tiempo) ? $this->PrepareParamSQL($tiempo) : "NULL",
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			if($nopc=="0000"){
				$html = new Smarty;
				/*Para el env�o de Mail cuando se agrega una Pc 0000*/
				$desTitulo = "HELPDESK";
				$sql="SELECT substring(lower(descripcion_soporte),1,53)
						FROM dbo.h_soporte where codigo_soporte=$aten1
						";
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$atencion1=$rs->fields[0];
					}						
				$sql="SELECT substring(lower(descripcion_soporte),1,53)
						FROM dbo.h_soporte where codigo_soporte=$aten2
						";
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$atencion2=$rs->fields[0];
					}						
				$sql="SELECT substring(lower(descripcion_soporte),1,53)
						FROM dbo.h_soporte where codigo_soporte=$aten3
						";
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$atencion3=$rs->fields[0];
					}
				
				$mensaje_preliminar = "EL USUARIO ".$_SESSION['cod_usuario'] . "@" . $this->emailDomain." HA CREADO UNA ATENCI�N CON C�DIGO DE PC: 0000. Favor tener en cuenta";
				$html->assign_by_ref('mensaje_preliminar',$mensaje_preliminar);
				$html->assign_by_ref('nopc',$nopc);
				$html->assign_by_ref('obs',$obs);
				$html->assign_by_ref('atencion1',$atencion1);
				$html->assign_by_ref('atencion2',$atencion2);
				$html->assign_by_ref('atencion3',$atencion3);
				
				$desMensaje = $html->fetch('oti/helpdesk/mail/atencionTxt.tpl.php');			
				$eFrom = "helpdesk@" . $this->emailDomain;
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				$eDest = "all_oti@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
				
				$mail->setFrom($eFrom);
				$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));
			}
			/**/		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaAtencion($id,$nopc=NULL,$tipo1=NULL,$tipo2=NULL,$tipo3=NULL,$aten1=NULL,$aten2=NULL,$aten3=NULL,$stt=NULL,$obs=NULL,$tiempo=NULL,$reLoad=false,$errors=false){
		global $desProblema,$desTrabajo;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("EXECUTE sp_listAtencion %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$nopc = $row[0];
					$tipo1 = $row[1];
					$aten1 = $row[2];
					$tipo2 = $row[3];
					$aten2 = $row[4];
					$tipo3 = $row[5];
					$aten3 = $row[6];
					$stt = $row[7];
					$obs = $row[8];
				}
				$rs->Close();
			}
			unset($rs);
		}
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nopc',$nopc);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('tiempo',$tiempo);
		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('tipo2',$tipo2);
		$html->assign_by_ref('tipo3',$tipo3);
		
		$html->assign_by_ref('desProblema',$desProblema);
		$html->assign_by_ref('desTrabajo',$desTrabajo);
		
		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo1',$this->ObjFrmSelect($sql_st, $tipo1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo1) ? $tipo1 : 0);
		$html->assign_by_ref('selAten1',$this->ObjFrmSelect($sql_st, $aten1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo2 de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo2',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select 2del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo2) ? $tipo2 : 0);
		$html->assign_by_ref('selAten2',$this->ObjFrmSelect($sql_st, $aten2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo3',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo3) ? $tipo3 : 0);
		$html->assign_by_ref('selAten3',$this->ObjFrmSelect($sql_st, $aten3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmModifyAtention.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
		function ModificaAtencion($id,$nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs){
		global $codReporte;
		global $desProblema,$desTrabajo;
		// Comprueba Valores	
		if(!$nopc){ $bPC = true; $this->errors .= 'El N�mero de la PC debe ser especificado<br>'; }
		if(!$obs){ $bObs = true; $this->errors .= 'La observaci�n debe ser especificado<br>'; }
		if(!$tipo1){ $bTip1 = true; $this->errors .= 'El Tipo de Soporte debe ser especificada<br>'; }
		if(!$aten1){ $bAt1 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo1>0&&$aten1<1){ $bAt11 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo2>0&&$aten2<1){ $bAt2 = true; $this->errors .= 'La Segunda Atenci�n debe ser especificada<br>'; }
		if($tipo3>0&&$aten3<1){ $bAt3 = true; $this->errors .= 'La Tercera Atenci�n debe ser especificada<br>'; }

		if($bPC||$bObs||$bTip1||$bAt1||$bAt11||$bAt2||$bAt3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaAtencion($id,$nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			$sql_SP = sprintf("EXECUTE sp_modAtencion3 %d,'%s','%s','%s',%d,%d,%d,%d,%d,%d,'%s','%s','%s'",
							  $id,
							  $this->PrepareParamSQL($stt),
							  strtoupper($this->PrepareParamSQL($obs)),
							  $_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($tipo1),
							  $this->PrepareParamSQL($aten1),
							  ($tipo2) ? $this->PrepareParamSQL($tipo2) : "NULL",
							  ($aten2) ? $this->PrepareParamSQL($aten2) : "NULL",
							  ($tipo3) ? $this->PrepareParamSQL($tipo3) : "NULL",
							  ($aten3) ? $this->PrepareParamSQL($aten3) : "NULL",
							  $this->PrepareParamSQL($nopc),
							  ($stt=="O" && $desProblema && $desProblema!="") ? $this->PrepareParamSQL($desProblema) : "NULL",
							  ($stt=="O" && $desTrabajo && $desTrabajo!="") ? $this->PrepareParamSQL($desTrabajo) : "NULL"
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			$destination .= "&codReporte={$id}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormAgregaInfTecnico($id,$asunto=NULL,$diagnostico=NULL,$sol=NULL,$errors=false){
		global $opcion,$documentos,$opcionParte,$partePC;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
/**/
		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc,r.codigo_trabajador
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));//Ejemplo pc 1311
														  $codInternoPC=$row[11];
														  $html->assign('codInternoPC',ucfirst($row[11]));//Ejemplo el c�digo interno de la 1311 es 443(codigo_trabajador de la tabla de h_reporte_2002)
										}
			}

/**/

			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				//$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/";
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
					      and d.coddep=$siglasDepe2 and d.indicativo_oficio like '%{$var}%'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 and
										      /*md.derivado=1 and md.finalizado=0 and md.id_dependencia_destino=".$this->userIntranet['COD_DEP']."*/
											  cdi.id_clase_documento_interno=d.id_clase_documento_interno and md.finalizado=0
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				
			}//fin del if($Buscar)

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddInfTecnico';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('diagnostico',$diagnostico);
		$html->assign_by_ref('sol',$sol);
		$html->assign_by_ref('opcion',$opcion);
		$html->assign_by_ref('opcionParte',$opcionParte);
		
		if($opcionParte==1){
			// Contenido Select del tipo de SOPORTE
			$sql_st = "SELECT codigo_parte, Lower(descrip_dispositivo)+' '+Lower(t.descrip_tipo_tecnologia)+' '+Lower(descrip_marca_disp)+' '+Lower(mo.descripcion)+' '+Upper(p.numero_serie)+' '+Upper(p.codigo_patrimonial)
  						FROM db_inventario_2002.user_inventario.parte p LEFT JOIN db_inventario_2002.user_inventario.dispositivo_inventario d ON p.codigo_tip_disp=d.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_dispositivo td ON p.codigo_tipo_dispositivo=td.codigo_tipo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_tecnologia t ON p.codigo_tipo_tecnologia=t.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=t.codigo_tipo_dispositivo AND p.codigo_tip_disp=t.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.marca_dispositivo ma ON p.codigo_marca_disp=ma.codigo_marca_disp AND p.codigo_tip_disp=ma.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.modelo_dispositivo mo ON p.codigo_modelo=mo.codigo_modelo AND p.codigo_marca_disp=mo.codigo_marca AND  p.codigo_tipo_tecnologia=MO.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=mo.codigo_tipo_dispositivo
  						WHERE p.codigo_parte in (SELECT codigo_parte
                           	FROM db_inventario_2002.user_inventario.pc
                           	WHERE codigo_cpu=$codInternoPC)   						   
					  UNION
 					   SELECT codigo_parte, Lower(descrip_dispositivo)+' '+Lower(t.descrip_tipo_tecnologia)+' '+Lower(descrip_marca_disp)+' '+Lower(mo.descripcion)+' '+Upper(p.numero_serie)+' '+Upper(p.codigo_patrimonial)
 						FROM db_inventario_2002.user_inventario.parte p LEFT JOIN db_inventario_2002.user_inventario.dispositivo_inventario d ON p.codigo_tip_disp=d.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_dispositivo td ON p.codigo_tipo_dispositivo=td.codigo_tipo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_tecnologia t ON p.codigo_tipo_tecnologia=t.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=t.codigo_tipo_dispositivo AND p.codigo_tip_disp=t.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.marca_dispositivo ma ON p.codigo_marca_disp=ma.codigo_marca_disp AND p.codigo_tip_disp=ma.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.modelo_dispositivo mo ON p.codigo_modelo=mo.codigo_modelo AND p.codigo_marca_disp=mo.codigo_marca AND  p.codigo_tipo_tecnologia=MO.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=mo.codigo_tipo_dispositivo
 						WHERE p.CODIGO_TIP_DISP=16 AND p.flag='O' AND p.IMP_RED=1 and codigo_dependencia=".$this->userIntranet['COD_DEP']."						   
 					ORDER BY 2";
						  
			$html->assign_by_ref('selPartePC',$this->ObjFrmSelect($sql_st, $partePC, true, true, array('val'=>'none','label'=>'Escoja')));
			unset($sql_st);
		}
		
		$sql_st="select d.id_documento,case when d.id_tipo_documento=4 then db_tramite_documentario.dbo.buscaReferencia(md.id_movimiento_documento)
					       else d.num_tram_documentario end,case when d.id_tipo_documento=1 then d.asunto
			                                                  when d.id_tipo_documento=2 then tup.descripcion
															  when d.id_tipo_documento=4 then d.asunto end,
					  case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	             when sol.id_tipo_persona=2 then sol.razon_social 
				 else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
				 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),t.apellidos_trabajador+' '+t.nombres_trabajador
				from db_tramite_documentario.dbo.documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
				                 left join db_general.dbo.persona sol on d.id_persona=sol.id,
     					db_tramite_documentario.dbo.movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO'
				WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
					and md.id_dependencia_destino=13 and md.codigo_trabajador=87";
		$html->assign_by_ref('selDocumentos',$this->ObjFrmSelect($sql_st, $documentos, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);					
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddInfTecnico.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');	
	}
	
	function AgregaInfTecnico($id,$asunto,$diagnostico,$sol){
		global $opcion,$documentos,$opcionParte,$partePC;
		// Comprueba Valores	
		if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaInfTecnico($id,$asunto,$diagnostico,$sol,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql_SP = sprintf("EXECUTE sp_insInfTecnico %d,'%s','%s','%s','%s','%s',%d,%d,%d",
							  $id,
							  "-".date('Y')."-CONVENIO_SITRADOC/".$this->userIntranet['SIGLA_DEP']."-".$_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($asunto),
							  $this->PrepareParamSQL($diagnostico),
							  $this->PrepareParamSQL($sol),
							  $_SESSION['cod_usuario'],
							  ($opcion>0) ? $opcion : "0",
							  ($opcion>0) ? $documentos : "NULL",
							  ($opcionParte==1 && $partePC>0) ? $partePC : "NULL"
 							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			$destination .= "&id={$id}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaInfTecnico($id,$asunto=NULL,$diagnostico=NULL,$sol=NULL,$errors=false){
		global $opcion,$documentos,$opcionParte,$partePC;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;

		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc,r.codigo_trabajador,r.descrip_realizado,doc.asunto,doc.observaciones,
										   r.CODIGO_PARTE
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu,
								 db_tramite_documentario.dbo.documento doc
								   
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo
								  and r.id_documento=doc.id_documento ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $codigoDependencia=$row[1];
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));//Ejemplo pc 1311
														  $codInternoPC=$row[11];
														  $html->assign('codInternoPC',ucfirst($row[11]));//Ejemplo el c�digo interno de la 1311 es 443(codigo_trabajador de la tabla de h_reporte_2002)
														  $html->assign('descrip',ucfirst($row[12]));
														  $html->assign('diagnostico',ucfirst($row[13]));
														  $html->assign('sol',ucfirst($row[14]));
														  $partePC=$row[15];
														  //$html->assign('codParte',ucfirst($codParte));
										}
			}

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModInfTecnico';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		//$html->assign_by_ref('diagnostico',$diagnostico);
		//$html->assign_by_ref('sol',$sol);
		$html->assign_by_ref('opcion',$opcion);
		$html->assign_by_ref('opcionParte',$opcionParte);
		
		if($opcionParte==1){
			// Contenido Select del tipo de SOPORTE
			$sql_st = "SELECT codigo_parte, Lower(descrip_dispositivo)+' '+Lower(t.descrip_tipo_tecnologia)+' '+Lower(descrip_marca_disp)+' '+Lower(mo.descripcion)+' '+Upper(p.numero_serie)+' '+Upper(p.codigo_patrimonial)
  						FROM db_inventario_2002.user_inventario.parte p LEFT JOIN db_inventario_2002.user_inventario.dispositivo_inventario d ON p.codigo_tip_disp=d.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_dispositivo td ON p.codigo_tipo_dispositivo=td.codigo_tipo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_tecnologia t ON p.codigo_tipo_tecnologia=t.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=t.codigo_tipo_dispositivo AND p.codigo_tip_disp=t.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.marca_dispositivo ma ON p.codigo_marca_disp=ma.codigo_marca_disp AND p.codigo_tip_disp=ma.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.modelo_dispositivo mo ON p.codigo_modelo=mo.codigo_modelo AND p.codigo_marca_disp=mo.codigo_marca AND  p.codigo_tipo_tecnologia=MO.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=mo.codigo_tipo_dispositivo
  						WHERE p.codigo_parte in (SELECT codigo_parte
                           	FROM db_inventario_2002.user_inventario.pc
                           	WHERE codigo_cpu=$codInternoPC)   						   
					  UNION
 					   SELECT codigo_parte, Lower(descrip_dispositivo)+' '+Lower(t.descrip_tipo_tecnologia)+' '+Lower(descrip_marca_disp)+' '+Lower(mo.descripcion)+' '+Upper(p.numero_serie)+' '+Upper(p.codigo_patrimonial)
 						FROM db_inventario_2002.user_inventario.parte p LEFT JOIN db_inventario_2002.user_inventario.dispositivo_inventario d ON p.codigo_tip_disp=d.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_dispositivo td ON p.codigo_tipo_dispositivo=td.codigo_tipo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.tipo_tecnologia t ON p.codigo_tipo_tecnologia=t.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=t.codigo_tipo_dispositivo AND p.codigo_tip_disp=t.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.marca_dispositivo ma ON p.codigo_marca_disp=ma.codigo_marca_disp AND p.codigo_tip_disp=ma.codigo_dispositivo
                               LEFT JOIN db_inventario_2002.user_inventario.modelo_dispositivo mo ON p.codigo_modelo=mo.codigo_modelo AND p.codigo_marca_disp=mo.codigo_marca AND  p.codigo_tipo_tecnologia=MO.codigo_tipo_tecnologia AND p.codigo_tipo_dispositivo=mo.codigo_tipo_dispositivo
 						WHERE p.CODIGO_TIP_DISP=16 AND p.flag='O' AND p.IMP_RED=1 and codigo_dependencia=".$codigoDependencia."						   
 					ORDER BY 2";
						  
			$html->assign_by_ref('selPartePC',$this->ObjFrmSelect($sql_st, $partePC, true, true, array('val'=>'none','label'=>'Escoja')));
			unset($sql_st);
		}
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmModInfTecnico.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');	
	}
	
	function ModificaInfTecnico($id,$asunto,$diagnostico,$sol){
		global $opcion,$documentos,$opcionParte,$partePC;
		// Comprueba Valores	
		if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaInfTecnico($id,$asunto,$diagnostico,$sol,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			$sql_SP = sprintf("EXECUTE sp_modInfTecnico %d,'%s','%s','%s','%s',%d",
							  $id,
							  $this->PrepareParamSQL($asunto),
							  $this->PrepareParamSQL($diagnostico),
							  $this->PrepareParamSQL($sol),
							  $_SESSION['cod_usuario'],
							  ($opcionParte==1 && $partePC>0) ? $partePC : "NULL"
 							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[4]['val']}";
			$destination .= "&id={$id}";
			header("Location: $destination");
			exit;
		}
	}	
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0 --header ... --footer .t. "){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}
	
	function FormGeneraReporte($search2=false){
		global $desFechaIni,$fecDesembarqueIni,$desFechaFin,$fecDesembarqueFin;
		global $GrupoOpciones1,$nroInforme;
		//$this->abreConnDB();
		//$this->conn->debug = true;

		//Manipulacion de las Fechas
		$fecAtencion = ($fecAtencion!='//'&&$fecAtencion) ? $fecAtencion : date('m/Y');
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('nroInforme',$nroInforme);
		
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('fecDesembarqueIni',$fecDesembarqueIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('fecDesembarqueFin',$fecDesembarqueFin);
		
		
		//Mes y anyo
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecAtencion,0,2), true));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAtencion,3,4), true));

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'reportes.tpl.php');
		if(!$search2) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function GeneraReporte($tipReporte,$mes,$anyo){
	global $GrupoOpciones1,$desFechaIni,$desFechaFin,$nroInforme;
	
		switch($GrupoOpciones1){
			case 1:
			$this->ReporteAte1($mes,$anyo,$desFechaIni,$desFechaFin);
			break;
			case 2:
			$this->ReporteAte2($mes,$anyo,$desFechaIni,$desFechaFin);
			break;
			case 3:
			$this->ReporteAte3($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Software
			break;
			case 4:
			$this->ReporteAte4($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Hardware
			break;
			case 7:
			$this->ReporteAte5($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Otros
			break;
			case 5:
			$this->ReporteAte6($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Redes
			break;
			case 6:
			$this->ReporteAte7($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Mantenimiento Correctivo
			break;
			case 8:
			$this->ReporteAte8($mes,$anyo);//Reporte Software
			break;
			case 9:
			$this->ReporteEspecial($mes,$anyo);//Reporte Especial
			break;
			case 10:
			//$this->ReporteAte2Esp($mes,$anyo);
			$this->ImprimeInfTecnico($id,$nroInforme);
			break;			
		}
	}

	function ReporteAte1($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		//$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
	
		$sql_st = sprintf("EXECUTE sp_reporteAte1 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate1',array('hw'=>$row[0],
										  'sw'=>$row[1],
										  'redes'=>$row[2],
										  'otros'=>$row[3],
										  'total'=>$row[4],
										  'mc'=>$row[5]));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;
		//$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$html->assign('fechaGen',ucfirst(strftime("%d/%m/%Y   %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate1'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte1.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte2($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$hoy=date('d');
		$ayer=$hoy-1;
		
		$mesinicial=$mes-1;
		if($mesinicial<10){$mesinicial='0'.$mesinicial;}
		$fecha1='15/'.$mesinicial.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}else{
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}
		$anyoinicial=$anyo-1;
		if($mes==1){$fecha1='17/12/'.$anyoinicial;$fecha2='19/01/'.$anyo;$fecha3='20/'.$mes.'/'.$anyo;}
		
		$html->assign_by_ref('fecha1',$desFechaIni);
		$html->assign_by_ref('fecha2',$desFechaFin);
	
		$sql_st = sprintf("EXECUTE sp_reporteAte2 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate2',array('hw'=>$row[0],
										  'hw1'=>$row[1],
										  'hw2'=>$row[2],
										  'hw3'=>$row[3],
										  'sw'=>$row[4],
										  'sw1'=>$row[5],
										  'sw2'=>$row[6],
										  'sw3'=>$row[7],
										  're'=>$row[8],
										  're1'=>$row[9],
										  're2'=>$row[10],
										  're3'=>$row[11],
										  'ot'=>$row[12],
										  'ot1'=>$row[13],
										  'ot2'=>$row[14],
										  'ot3'=>$row[15],
										  'tot'=>$row[16],
										  'tot1'=>$row[17],
										  'tot2'=>$row[18],
										  'tot3'=>$row[19],
										  'mc'=>$row[20],
										  'mc1'=>$row[21],
										  'mc2'=>$row[22],
										  'mc3'=>$row[23],
										  'hw4'=>$row[24],
										  'sw4'=>$row[25],
										  're4'=>$row[26],
										  'mc4'=>$row[27],
										  'ot4'=>$row[28],
										  'tot4'=>$row[29],
										  'hw5'=>$row[30],
										  'sw5'=>$row[31],
										  're5'=>$row[32],
										  'mc5'=>$row[33],
										  'ot5'=>$row[34],
										  'tot5'=>$row[35],
										  'hw6'=>$row[36],
										  'sw6'=>$row[37],
										  're6'=>$row[38],
										  'mc6'=>$row[39],
										  'ot6'=>$row[40],
										  'tot6'=>$row[41],
										  'hw7'=>$row[42],
										  'sw7'=>$row[43],
										  're7'=>$row[44],
										  'mc7'=>$row[45],
										  'ot7'=>$row[46],
										  'tot7'=>$row[47],
										  'hw8'=>$row[48],
										  'sw8'=>$row[49],
										  're8'=>$row[50],
										  'mc8'=>$row[51],
										  'ot8'=>$row[52],
										  'tot8'=>$row[53]										  
										  ));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate2'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte2.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte2Esp($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$hoy=date('d');
		$ayer=$hoy-1;
		
		$mesinicial=$mes-1;
		if($mesinicial<10){$mesinicial='0'.$mesinicial;}
		$fecha1='17/'.$mesinicial.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}else{
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}
		$anyoinicial=$anyo-1;
		if($mes==1){$fecha1='17/12/'.$anyoinicial;$fecha2='19/01/'.$anyo;$fecha3='20/'.$mes.'/'.$anyo;}
		
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);
	
		$sql_st = sprintf("EXECUTE sp_reporteAte2 '%s','%s'",
							$fecha1,
							$fecha3);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate2',array('hw'=>$row[0],
										  'hw1'=>$row[1],
										  'hw2'=>$row[2],
										  'hw3'=>$row[3],
										  'sw'=>$row[4],
										  'sw1'=>$row[5],
										  'sw2'=>$row[6],
										  'sw3'=>$row[7],
										  're'=>$row[8],
										  're1'=>$row[9],
										  're2'=>$row[10],
										  're3'=>$row[11],
										  'ot'=>$row[12],
										  'ot1'=>$row[13],
										  'ot2'=>$row[14],
										  'ot3'=>$row[15],
										  'tot'=>$row[16],
										  'tot1'=>$row[17],
										  'tot2'=>$row[18],
										  'tot3'=>$row[19],
										  'mc'=>$row[20],
										  'mc1'=>$row[21],
										  'mc2'=>$row[22],
										  'mc3'=>$row[23],
										  'hw4'=>$row[24],
										  'sw4'=>$row[25],
										  're4'=>$row[26],
										  'mc4'=>$row[27],
										  'ot4'=>$row[28],
										  'tot4'=>$row[29],
										  'hw5'=>$row[30],
										  'sw5'=>$row[31],
										  're5'=>$row[32],
										  'mc5'=>$row[33],
										  'ot5'=>$row[34],
										  'tot5'=>$row[35],
										  'hw6'=>$row[36],
										  'sw6'=>$row[37],
										  're6'=>$row[38],
										  'mc6'=>$row[39],
										  'ot6'=>$row[40],
										  'tot6'=>$row[41],
										  'hw7'=>$row[42],
										  'sw7'=>$row[43],
										  're7'=>$row[44],
										  'mc7'=>$row[45],
										  'ot7'=>$row[46],
										  'tot7'=>$row[47],
										  'hw8'=>$row[48],
										  'sw8'=>$row[49],
										  're8'=>$row[50],
										  'mc8'=>$row[51],
										  'ot8'=>$row[52],
										  'tot8'=>$row[53]										  
										  
										  ));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate2'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte2Esp.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function num($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	
	function ReporteAte3($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte3 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function numm($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3  and r.usuario='obarja'
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function ReporteAte8($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) and r.usuario='obarja'
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte8 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->numm($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function num2($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num3($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num4($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function num5($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	

	function ReporteAte4($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte4 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate4',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num2($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate4'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte4.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte5($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte5 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate5',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num3($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate5'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte5.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte6($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte6 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate6',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num4($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate6'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte6.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	function ReporteAte7($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte7 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate7',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num5($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate7'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte7.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function cuentaAteToti($fecha1,$fecha2,$codTrab){
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)>=convert(datetime,'$fecha1',103) 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)<=convert(datetime,'$fecha2',103) 
			and r.usuario=tr.email and tr.codigo_trabajador=$codTrab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
		
	}
	
	function cuentaAte($mes,$dia,$anyo,$trab){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email and tr.codigo_trabajador=$trab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}
	
	function cuentaAteTotalDia($mes,$dia,$anyo){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email 
			and.r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}

	function ReporteEspecial($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
			$maxDias=31;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			$maxDias=28;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			$maxDias=30;
			}
		/**/
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte /*and s.codigo_tipo_soporte=5 */
			and r.AUDIT_FIN > convert(datetime,'$fecha1',103) 
			AND r.AUDIT_FIN <= convert(datetime,'$fecha2',103)
			and r.flag='O'  
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = "SELECT APELLIDOS_TECNICO+' '+NOMBRES_TECNICO,CODIGO_TRABAJADOR
					FROM dbo.h_tecnico 
					WHERE ESTADO='ACTIVO' 
					and codigo_trabajador>0 and codigo_trabajador<>646
					order by 1 
					";
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('trab',array('nomb' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant1' => $this->cuentaAte($mes,1,$anyo,$row[1]),
										   'cant2' => $this->cuentaAte($mes,2,$anyo,$row[1]),
										   'cant3' => $this->cuentaAte($mes,3,$anyo,$row[1]),
										   'cant4' => $this->cuentaAte($mes,4,$anyo,$row[1]),
										   'cant5' => $this->cuentaAte($mes,5,$anyo,$row[1]),
										   'cant6' => $this->cuentaAte($mes,6,$anyo,$row[1]),
										   'cant7' => $this->cuentaAte($mes,7,$anyo,$row[1]),
										   'cant8' => $this->cuentaAte($mes,8,$anyo,$row[1]),
										   'cant9' => $this->cuentaAte($mes,9,$anyo,$row[1]),
										   'cant10' => $this->cuentaAte($mes,10,$anyo,$row[1]),
										   'cant11' => $this->cuentaAte($mes,11,$anyo,$row[1]),
										   'cant12' => $this->cuentaAte($mes,12,$anyo,$row[1]),
										   'cant13' => $this->cuentaAte($mes,13,$anyo,$row[1]),
										   'cant14' => $this->cuentaAte($mes,14,$anyo,$row[1]),
										   'cant15' => $this->cuentaAte($mes,15,$anyo,$row[1]),
										   'cant16' => $this->cuentaAte($mes,16,$anyo,$row[1]),
										   'cant17' => $this->cuentaAte($mes,17,$anyo,$row[1]),
										   'cant18' => $this->cuentaAte($mes,18,$anyo,$row[1]),
										   'cant19' => $this->cuentaAte($mes,19,$anyo,$row[1]),
										   'cant20' => $this->cuentaAte($mes,20,$anyo,$row[1]),
										   'cant21' => $this->cuentaAte($mes,21,$anyo,$row[1]),
										   'cant22' => $this->cuentaAte($mes,22,$anyo,$row[1]),
										   'cant23' => $this->cuentaAte($mes,23,$anyo,$row[1]),
										   'cant24' => $this->cuentaAte($mes,24,$anyo,$row[1]),
										   'cant25' => $this->cuentaAte($mes,25,$anyo,$row[1]),
										   'cant26' => $this->cuentaAte($mes,26,$anyo,$row[1]),
										   'cant27' => $this->cuentaAte($mes,27,$anyo,$row[1]),
										   'cant28' => $this->cuentaAte($mes,28,$anyo,$row[1]),
										   'cant29' => $this->cuentaAte($mes,29,$anyo,$row[1]),
										   'cant30' => $this->cuentaAte($mes,30,$anyo,$row[1]),
										   'cant31' => $this->cuentaAte($mes,31,$anyo,$row[1]),
										   'toti' => $this->cuentaAteToti($fecha1,$fecha2,$row[1])
										   
										   ));
			$rs->Close();
		}
		unset($rs);
		
			$html->assign_by_ref('total1',$this->cuentaAteTotalDia($mes,1,$anyo));
			$html->assign_by_ref('total2',$this->cuentaAteTotalDia($mes,2,$anyo));
			$html->assign_by_ref('total3',$this->cuentaAteTotalDia($mes,3,$anyo));
			$html->assign_by_ref('total4',$this->cuentaAteTotalDia($mes,4,$anyo));
			$html->assign_by_ref('total5',$this->cuentaAteTotalDia($mes,5,$anyo));
			$html->assign_by_ref('total6',$this->cuentaAteTotalDia($mes,6,$anyo));
			$html->assign_by_ref('total7',$this->cuentaAteTotalDia($mes,7,$anyo));
			$html->assign_by_ref('total8',$this->cuentaAteTotalDia($mes,8,$anyo));
			$html->assign_by_ref('total9',$this->cuentaAteTotalDia($mes,9,$anyo));
			$html->assign_by_ref('total10',$this->cuentaAteTotalDia($mes,10,$anyo));
			$html->assign_by_ref('total11',$this->cuentaAteTotalDia($mes,11,$anyo));
			$html->assign_by_ref('total12',$this->cuentaAteTotalDia($mes,12,$anyo));
			$html->assign_by_ref('total13',$this->cuentaAteTotalDia($mes,13,$anyo));
			$html->assign_by_ref('total14',$this->cuentaAteTotalDia($mes,14,$anyo));
			$html->assign_by_ref('total15',$this->cuentaAteTotalDia($mes,15,$anyo));
			$html->assign_by_ref('total16',$this->cuentaAteTotalDia($mes,16,$anyo));
			$html->assign_by_ref('total17',$this->cuentaAteTotalDia($mes,17,$anyo));
			$html->assign_by_ref('total18',$this->cuentaAteTotalDia($mes,18,$anyo));
			$html->assign_by_ref('total19',$this->cuentaAteTotalDia($mes,19,$anyo));
			$html->assign_by_ref('total20',$this->cuentaAteTotalDia($mes,20,$anyo));
			$html->assign_by_ref('total21',$this->cuentaAteTotalDia($mes,21,$anyo));
			$html->assign_by_ref('total22',$this->cuentaAteTotalDia($mes,22,$anyo));
			$html->assign_by_ref('total23',$this->cuentaAteTotalDia($mes,23,$anyo));
			$html->assign_by_ref('total24',$this->cuentaAteTotalDia($mes,24,$anyo));
			$html->assign_by_ref('total25',$this->cuentaAteTotalDia($mes,25,$anyo));
			$html->assign_by_ref('total26',$this->cuentaAteTotalDia($mes,26,$anyo));
			$html->assign_by_ref('total27',$this->cuentaAteTotalDia($mes,27,$anyo));
			$html->assign_by_ref('total28',$this->cuentaAteTotalDia($mes,28,$anyo));
			if($maxDias==30||$maxDias==31){
				$html->assign_by_ref('total29',$this->cuentaAteTotalDia($mes,29,$anyo));
				$html->assign_by_ref('total30',$this->cuentaAteTotalDia($mes,30,$anyo));
			}
			if($maxDias==31){
				$html->assign_by_ref('total31',$this->cuentaAteTotalDia($mes,31,$anyo));
			}

		$html->assign_by_ref('maxDias',$maxDias);
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'trab'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte8.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function DetalleInformeTecnico($id){
		// Genera HTML de Muestra
		$html = new Smarty;
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql="select d.asunto,d.observaciones,d.indicativo_oficio
				from db_tramite_documentario.dbo.documento d,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
			";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$diagnostico=$rs->fields[0];
			$sol=$rs->fields[1];
			$indicativo=$rs->fields[2];
			$html->assign_by_ref('diagnostico',$diagnostico);
			$html->assign_by_ref('sol',$sol);
		}
		
				$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );;
		
		//echo $sql_SP;
		$rsData = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsData){
			print $this->conn->ErrorMsg();
		}else{
				//while($DocumentoData = $rsData->FetchRow())
				$usuario=$rsData->fields[0];
				$coddep=$rsData->fields[1];
				$descrip=$rsData->fields[2];
				$flag=$rsData->fields[3];
				$siglas=$rsData->fields[4];
				$marca=$rsData->fields[5];
				$modelo=$rsData->fields[6];
				$codPat=$rsData->fields[7];
				$serie=$rsData->fields[8];
				$dep=$rsData->fields[9];
				$codPC=$rsData->fields[10];
		$html->assign_by_ref('usuario',$usuario);
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('descrip',$descrip);
		$html->assign_by_ref('flag',$flag);
		$html->assign_by_ref('siglas',$siglas);
		$html->assign_by_ref('marca',$marca);
		$html->assign_by_ref('modelo',$modelo);
		$html->assign_by_ref('codPat',$codPat);
		$html->assign_by_ref('serie',$serie);
		$html->assign_by_ref('dep',$dep);
		$html->assign_by_ref('codPC',$codPC);
			//unset($row);
			$rsData->Close();
		}
		unset($rsData);
		
		$titulo="INFORME T�CNICO N� ".$indicativo;
		$html->assign_by_ref('titulo',$titulo);

		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oti/helpdesk/printDetalle.tpl.php') : $html->display('oti/helpdesk/showDetalle.tpl.php');
	}

}
?>
