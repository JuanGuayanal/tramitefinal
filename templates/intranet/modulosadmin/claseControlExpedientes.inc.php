<?
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');
include_once 'mimemail/htmlMimeMail.php';

require_once "class.graphic.php";

class ControlExpedientes extends Modulos{

	var $folderXML = '/var/www/intranet/institucional/aplicativos/oad/tramite/xml';
	var $emailDomain = "MIDIS.gob.pe";

	function ControlExpedientes($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans',
							FRM_BUSCA_PERSONA =>'frmSearchPersona',
							BUSCA_PERSONA =>'searchPersona',
							BUSCA_PERSONA_EXTENDIDO => 'frmSearchExtendedPer',
							FRM_AGREGA_PERSONA => 'frmAddPersona',
							AGREGA_PERSONA => 'addPersona',
							FRM_MODIFICA_PERSONA => 'frmModifyPersona',
							MODIFICA_PERSONA => 'modifyPersona',
							FRM_AGREGA_CONTACTO => 'frmAddContacto',
							AGREGA_CONTACTO => 'addContacto',
							IMPRIMIR_CLAVE => 'printClave',
							MUESTRA_DETALLE_PERSONA => 'showDetailPer',
							IMPRIME_DETALLE =>'ImprimeDetalle',
							CONTROL_EXP => 'frmControlExp',
							CONTROL_EXP2 => 'frmControlExp2',
							LISTADO_EXP_CURSO => 'frmListadoExpCurso',
							GRAFICO_EXP_CURSO => 'frmGraficoExpCurso',
							CONTROL_EXTINT => 'frmControlExtInt',
							CONTROL_EXTINT2 => 'frmControlExtInt2',
							LISTADO_EXTINT_CURSO => 'frmListadoExtIntCurso',
							GRAFICO_EXTINT_CURSO => 'frmGraficoExtIntCurso'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		if($this->userIntranet['COD_DEP']==7||$_SESSION['cod_usuario']=="jtumay"){
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchPersona', label => 'BUSCAR' ),
											 1 => array ( 'val' => 'frmAddPersona', label => 'AGREGAR'),
											 2 => array ( 'val' => 'frmModifyPersona', label => 'MODIFICAR'),
											 3 => array ( 'val' => 'frmAddContacto', label => 'CONTACTO')
											);
		}elseif($this->userIntranet['COD_DEP']==48){
			$this->menu_items = array(0 => array ( 'val' => 'frmAddPersona', label => 'AGREGAR')
											);
		}else{
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchPersona', label => 'BUSCAR' )
											);
		}
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
    }

	function MuestraStatTrans($accion){
		global $id,$clave;//Id de la Persona
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. DEBE REALIZAR PREVIAMENTE UNA B�SQUEDA. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		if($id){//�Existe id de la Persona?
		
					$imgStat = 'stat_true.gif';
					$imgOpcion = 'img_printer2.gif';
					$altOpcion = 'Obtener Clave';
					$lnkOpcion = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_CLAVE]}&id={$id}&clave={$clave}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

					$html->assign_by_ref('id',$id);
					$html->assign_by_ref('imgOpcion',$imgOpcion);
					$html->assign_by_ref('altOpcion',$altOpcion);
					$html->assign_by_ref('lnkOpcion',$lnkOpcion);
		}
				
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_PERSONA]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/showStatTrans.inc.php');
		$html->display('oad/footerArm.tpl.php');
	}

	function MuestraIndex(){
		/*if($this->userIntranet['COD_DEP']==48){
			$this->FormAgregaPersona();
		}else{
			$this->FormBuscaPersona();
		}*/	
		$this->ControlExp();
	}

	function FechaActual(){
		setlocale (LC_TIME, $this->zonaHoraria);
		return ucfirst(strtolower(strftime("%d/%m/%Y")));
	}
	function HoraActual(){
		//setlocale (LC_TIME, $this->zonaHoraria);
		//return ucfirst(strtolower(strftime("%T")));	
		$this->abreConnDB();
//		$this->conn->debug = true;

		$sql="select convert(varchar,getDate(),108)";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rs->fields[0];
			}
		return($hora);
	}
	/* Funcion para validar Email */
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	} 	
	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	function FormBuscaPersona($page=NULL,$desNombre=NULL,$sector=NULL,$codDepa=NULL,$tipPersona=NULL,$search=false){
		global $ruc,$codDepa2,$codProv,$codDist;
		
		/*$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select distinct id,case when id_tipo_persona=1 then apellidos+' '+nombres
									 else razon_social end,len(case when id_tipo_persona=1 then apellidos+' '+nombres
									 else razon_social end),nro_documento,case when id_tipo_identificacion=1 then 'DNI'
												                   when id_tipo_identificacion=8 then 'RUC'
														ELSE 'OTROS' END,
											direccion,representante_legal,LEN(NRO_DOCUMENTO) AS LONGITUD								
									from db_general.dbo.persona
									where id in (
									select embxper.id_pers from db_dnepp.user_dnepp.embxpersona embxper,db_dnepp.user_dnepp.pd2007 pd
									 where embxper.estado_embxpers=1 and embxper.id_emb=pd.id_emb and pd.entra=1
									)
					and len(nro_documento) in (8,11)
					and nro_documento<>'00000000'
					and nro_documento<>'00000000000'
					ORDER BY 1			 
		
			 ";
			$sql="select distinct p.id,case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
									 else p.razon_social end,len(case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
									 else p.razon_social end),p.nro_documento,case when p.id_tipo_identificacion=1 then 'DNI'
												                   when p.id_tipo_identificacion=8 then 'RUC'
														ELSE 'OTROS' END,
											p.direccion,p.representante_legal,LEN(p.NRO_DOCUMENTO) AS LONGITUD,
											depa.departamento,prov.provincia,dist.distrito,p.telefono,p.email,
											case when p.id_tipo_identificacion_rep_leg=1 then 'DNI'
												                   when p.id_tipo_identificacion_rep_leg=8 then 'RUC'
														ELSE 'OTROS' END,
											p.nro_documento_representante
									from db_general.dbo.persona p,db_general.dbo.departamento depa,
										db_general.dbo.provincia prov,
										db_general.dbo.distrito dist
									where p.id in (
									select embxper.id_pers from db_dnepp.user_dnepp.embxpersona embxper,db_dnepp.user_dnepp.pd2007 pd
									 where embxper.estado_embxpers=1 and embxper.id_emb=pd.id_emb and pd.entra=1
									)
					and len(p.nro_documento) in (8,11)
					and p.nro_documento<>'00000000'
					and p.nro_documento<>'00000000000'
					and p.codigo_departamento=depa.codigo_departamento
					and p.codigo_provincia=prov.codigo_provincia and prov.codigo_departamento=depa.codigo_departamento
					and p.codigo_distrito=dist.codigo_distrito and dist.codigo_provincia=prov.codigo_provincia and dist.codigo_departamento=depa.codigo_departamento
					ORDER BY 1";  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					$i=0;
					while ($row = $rs->FetchRow()){
						$i=$i+1;
						$id=$row[0];
						$nombre=$row[1];
						$longitud=$row[2];
						$ruc=$row[3];
						$tipoIdentificacion=$row[4];
						$direccion=$row[5];
						$representante=$row[6];
						$departamento=$row[8];
						$provincia=$row[9];
						$distrito=$row[10];
						$telefono=$row[11];
						$mail=$row[12];
						$tipoIdentificacionRepLeg=$row[13];
						$nroDocRepLeg=$row[14];
						//echo $id.",".$nombre."<br>";
						
						$clave = $this->GeneraPassword(6);
						$valores=$clave-$longitud*12;
						
						if($valores<100000){
							$valores="3".$valores;
						}
						
						echo $id.",".$nombre.",".$tipoIdentificacion.",".$ruc.",".$this->formateaComas($direccion).",".$this->formateaComas($representante).",".$valores.",".$departamento.",".$provincia.",".$distrito.",".$telefono.",".$mail.",".$tipoIdentificacionRepLeg.",".$nroDocRepLeg."<br>";
						
						$nombres="PRUEBA";
						$apellidos="PRUEBA";
						$direccion="PRUEBA";
						$telefono="PRUEBA";
						$fax="PRUEBA";
						$mail="PRUEBA@prueba.com";
						
						//echo "justomatrix";
						
						$sql_SP2 = sprintf("EXECUTE sp_insContacto %d,%s,%s,%s,%s,%s,%s,'%s','%s','%s'",
										  $id,
										  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
										  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
										  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
										  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
										  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
										  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
										  $_SESSION['cod_usuario'],
										  md5($valores),
										  ($radio) ? $radio : "P"
										  );
						 //echo $sql_SP; exit;
						$rs2 = & $this->conn->Execute($sql_SP2);
						unset($sql_SP2);
						if (!$rs2)
							$RETVAL2=1;
						unset($rs2);
					}//fin del while
					//echo "el valor del contador es: ".$contador;
			}		
			*/
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscaPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('ruc',$ruc);

		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Departamento
		$sql_st = "SELECT codigo_departamento, lower(departamento) ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 2";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa2',$this->ObjFrmSelect($sql_st, $codDepa2, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/searchPersona.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	
	function busTipPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select id_tipo_persona 
		         from db_general.dbo.tipo_persona 
				 where ID_TIPO_PERSONA=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
					$tipPersona=$row[0];
					//echo "el valor del contador es: ".$contador;
			}
			//unset($rs);
		return($tipPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}

	function busPrivPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="select rubro 
		         from db_general.dbo.persona 
				 where id=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					//while ($row = $rs->FetchRow())
					$privPersona=$rs->fields[0];
					//echo "el valor del contador es: ".$privPersona;
			}
			//unset($rs);
		return($privPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}
	
	function BuscaDirectorio($page,$desNombre,$sector,$codDepa,$tipPersona){
		global $ruc,$codDepa2,$codProv,$codDist;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscaPersona';
		$html->assign('frmName','frmSearchExtendedPer');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array(
									
									'desNombre'=>$desNombre,
									'sector'=>$sector,
									'codDepa'=>$codDepa,
									'tipPersona'=>$tipPersona,
									'ruc'=>$ruc,
									'codDepa2'=>$codDepa2,
									'codProv'=>$codProv,
									'codDist'=>$codDist
									));
		
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaPersona($page,$desNombre,$sector,$codDepa,$tipPersona,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
				
		// Condicionales Segun ingreso del Usuario
		$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";
		
		if(!empty($codDepa2)&&!is_null($codDepa2)&&$codDepa2!='none'){
			$condConsulta[] = "p.codigo_departamento='$codDepa2' ";
			if(!empty($codProv)&&!is_null($codProv)&&$codProv!='none'){
				$condConsulta[] = "p.codigo_provincia='$codProv' ";
				if(!empty($codDist)&&!is_null($codDist)&&$codDist!='none'){
					$condConsulta[] = "p.codigo_distrito='$codDist' ";
				}
			}
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		//echo "<br>la tabla".$table."<br>el wherte".$where."<br>tipo de cor".$TipoCor."<br>";

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);
	

			$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,0,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
				$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,%s,%s",
									($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$start,
									$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosPersona %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow())
							$html->append('arrPer', array('id' => $id[0],
														  'desNom' => ucwords($NotiData[1]),
														  'tipPer' => $NotiData[2],
														  'nro' => $NotiData[3],
														  'dire' => ucfirst($NotiData[4]),
														  'tel' => strtoupper($NotiData[5]),
														  'mail' => $NotiData[6],
														  'tipPersona' => $this->busTipPersona($id[0]),
														  'acui' => $this->busPrivPersona($id[0]),
														  'flag' => $NotiData[7]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		
		$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('userDirectorio',$_SESSION['cod_usuario']);

		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_PERSONA], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscaPersona', $this->arr_accion[BUSCA_PERSONA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/tramite/searchResultPersona.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		
	}
	
	function VistaExtendidaDocPersona($desNombre,$sector,$codDepa,$tipPersona,$ruc){
		$html = new Smarty;

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,1,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			while($id = $rsId->FetchRow()){
				// Obtiene Todos los Datos de la Parte
				$sql_SP = sprintf("EXECUTE sp_busDatosPersona %d",$id[0]);
				//echo $sql_SP;
				$rsData = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if(!$rsData)
					print $this->conn->ErrorMsg();
				else{
					if($PerData = $rsData->FetchRow())
							$html->append('arrPer', array('id' => $id[0],
														  'desNom' => ucwords($PerData[1]),
														  'tipPer' => $PerData[2],
														  'nro' => $PerData[3],
														  'dire' => ucfirst($PerData[4]),
														  'tel' => strtoupper($PerData[5]),
														  'mail' => $PerData[6],
														  'tipPersona' => $this->busTipPersona($id[0])
														  //'acui' => $this->busPrivPersona($id[0])
														  ));
					$rsData->Close();
				}
				unset($rsData);
			}
			$rsId->Close();
		}
		unset($rsId);

		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('oad/tramite/viewExtendedPer.tpl.php');
		exit;
	}
	
	function FormAgregaPersona($sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$errors=false){
		//echo "En mantenimiento";exit;
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $bBusca;
		global $rubro10;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/**/
		if($bBusca==1){
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql="select count(*)
			      from db_general.dbo.persona where nro_documento='$ruc'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$cuenta=$rs->fields[0];
				if($cuenta>=1){
					$msj="El n�mero ingresado ya existe en la Base de Datos<br>Ingrese otro n�mero. Gracias";
				}
			}

			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi
			      from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";*/
			$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi,s.telef1
			      from bd_sunat.dbo.dato_principal P left join BD_SUNAT.DBO.DATO_SECUNDARIO s on p.numruc=s.numruc and s.telef1<>'-' 	
				  where p.NUMRUC='$ruc'	";	  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					//$razonsocial = $row[0];
					$codDepa = $row[1];
					$codProv = $row[2];
					$codDist = $row[3];
					$direccion = $row[4];
					$identi= $row[5];
					$telefono= $row[6];
					if($identi=="02"){
						$razonsocial = $row[0];
						$tipPersona=2;
						$tipIdent=8;
						if($razonsocial!=""){
							$valorReadonly=1;
						}						
					}else{
						$nombres= $row[0];
						$tipPersona=1;
						$tipIdent=8;}
					
				}
				$rs->Close();
			}
			unset($rs);
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);
		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector ".
				  "ORDER BY 2 DESC";
		//$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddPersona.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	function formateaComas($cadena){
	
		$cadena= ereg_replace(","," ",$cadena);
		
		return ($cadena);
	}	
	
	function formateaTexto($cadena){
	
		$cadena= ereg_replace("          "," ",$cadena);
		$cadena= ereg_replace("         "," ",$cadena);
		$cadena= ereg_replace("        "," ",$cadena);
		$cadena= ereg_replace("       "," ",$cadena);
		$cadena= ereg_replace("      "," ",$cadena);
		$cadena= ereg_replace("     "," ",$cadena);
		$cadena= ereg_replace("    "," ",$cadena);
		$cadena= ereg_replace("   "," ",$cadena);
		$cadena= ereg_replace("  "," ",$cadena);
		
		return ($cadena);
	}
	
	function formateaAcentos($cadena){
	
		$cadena= ereg_replace("�","a",$cadena);
		$cadena= ereg_replace("�","e",$cadena);
		$cadena= ereg_replace("�","i",$cadena);
		$cadena= ereg_replace("�","o",$cadena);
		$cadena= ereg_replace("�","u",$cadena);
		$cadena= ereg_replace("�","A",$cadena);
		$cadena= ereg_replace("�","E",$cadena);
		$cadena= ereg_replace("�","I",$cadena);
		$cadena= ereg_replace("�","O",$cadena);
		$cadena= ereg_replace("�","U",$cadena);
		
		return ($cadena);
	}		
	
	function AgregaPersona($sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones){
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $rubro10;
		global $bBusca;
		
			if ($razonsocial && $razonsocial!="")
				$razonsocial=$this->formateaTexto($razonsocial);
			if ($nombres && $nombres!=""){
				$nombres=$this->formateaTexto($nombres);
				$nombres=$this->formateaAcentos($nombres);
			}	
			if ($apellidos && $apellidos!=""){
				$apellidos=$this->formateaTexto($apellidos);
				$apellidos=$this->formateaAcentos($apellidos);
			}	
			if ($repLegal && $repLegal!="")
				$repLegal=$this->formateaTexto($repLegal);				
				
		// Comprueba Valores
		$tamano=strlen($ruc);
		$tamanoDireccion=strlen($direccion);
		$tamanoNombre=strlen($nombres);
		$tamanoApellido=strlen($apellidos);
		$tamanoRazonSocial=strlen($razonsocial);
		$tamanoRepLegal=strlen($nroRepLegal);
		$tamanoNombreRepLegal=strlen($repLegal);
		if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if($tipIdent==8&& $tamano!=11){ $bC10 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC<br>'; }
		if($tipIdent==1&& $tamano!=8){ $bC11 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI<br>'; }
		//if(!$telefono){ $bC9 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		//if($codDepa<1){ $bC10 = true; $this->errors .= 'El Departamento debe ser especificado<br>'; }
		//if($codProv<1){ $bC11 = true; $this->errors .= 'La Provincia debe ser especificada<br>'; }
		//if($codDist<1){ $bC12 = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC13 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }
		if($tamanoDireccion<10){ $bC14 = true; $this->errors .= 'Al menos 10 caracteres que debe contener la direcci�n<br>'; }
		if($nombres&&$nombres!=""&&$tipPersona==1&& $tamanoNombre<2){ $bC15 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre<br>'; }
		if($apellidos&&$apellidos!=""&&$tipPersona==1&& $tamanoApellido<2){ $bC16 = true; $this->errors .= 'Al menos 2 caracteres debe contener el apellido<br>'; }
		if($razonsocial&&$razonsocial!=""&&$tipPersona==2&& $tamanoRazonSocial<2){ $bC17 = true; $this->errors .= 'Al menos 2 caracteres debe contener la Raz�n Social<br>'; }
		if($tipIdentRepLegal>0&&!$nroRepLegal){ $bC18 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser espcificado<br>'; }
		if($tipIdentRepLegal==8&& $tamanoRepLegal!=11){ $bC19 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC del Representante Legal<br>'; }
		if($tipIdentRepLegal==1&& $tamanoRepLegal!=8){ $bC20 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI del Representante Legal<br>'; }
		if($tipIdentRepLegal>0&&!$repLegal){ $bC21 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tipIdentRepLegal<1){ $bC22 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&!$nroRepLegal){ $bC23 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tamanoNombreRepLegal<2){ $bC24 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre del Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&!$repLegal){ $bC25 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&$tipIdentRepLegal<1){ $bC26 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($tipPersona==2&&$tipIdent==8&&$bBusca!=1&&$ruc!="00000000000"){ $bC27 = true; $this->errors .= 'Debe consultar la data de SUNAT, hacer click en el checkbox.<br>'; }
		/**/
			$this->abreConnDB();
			//$this->conn->debug = true;
			if((!$apellidos&& !$nombres)&& $tipPersona==2){
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and razon_social='$razonsocial' /*and nro_documento='$ruc'*/";
			}else{
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and nombres='$nombres' and apellidos='$apellidos' /*and nro_documento='$ruc'*/";
			}
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe la persona en el Directorio de MIDIS<br>'; }
				}
				
			$sql2="select count(*) from db_general.dbo.persona where nro_documento='$ruc' AND NRO_DOCUMENTO<>'00000000000' and NRO_DOCUMENTO<>'00000000'";
				$rs2 = & $this->conn->Execute($sql2);
				unset($sql2);
				if (!$rs2)
					$RETVAL=1;
				else{
					$contador2=$rs2->fields[0];
					if($contador2>=1){ $bC12 = true; $this->errors .= 'Ya existe el Nro de documento en el Directorio de MIDIS<br>'; }
				}
				
			if($tipPersona==2&&$tipIdent==8&&$bBusca==1&&$ruc!="00000000000"){
				$sql3="select count(*) from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";
				$rs3 = & $this->conn->Execute($sql3);
				unset($sql3);
				if (!$rs3)
					$RETVAL=1;
				else{
					$contador3=$rs3->fields[0];
					if($contador3==0){ $bC28 = true; $this->errors .= 'No existe el Nro de ruc en el Padr�n RUC.<br>'; }
				}				
			}				
			
		/**/
		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC9||$bC10||$bC11||$bC12||$bC13||$bC14||$bC15||$bC16||$bC17||$bC18||$bC19||$bC20||$bC21||$bC22||$bC23||$bC24||$bC25||$bC26||$bC27||$bC28){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaPersona($sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{

			if($rubro1!=1) $rubro1=0;
			if($rubro2!=1) $rubro2=0;
			if($rubro3!=1) $rubro3=0;
			if($rubro4!=1) $rubro4=0;
			if($rubro5!=1) $rubro5=0;
			if($rubro6!=1) $rubro6=0;
			if($rubro10!=1) $rubro10=0;
			
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
						
			$sql_SP = sprintf("EXECUTE sp_insPersona %d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,'%s',%s",
							  ($sector) ? $this->PrepareParamSQL($sector) : "NULL",
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "'SIN DIRECCI�N'",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa&&$codDepa!='none') ? "'".$this->PrepareParamSQL($codDepa)."'" : "'00'",
							  ($codProv&&$codProv!='none') ? "'".$this->PrepareParamSQL($codProv)."'" : "'00'",
							  ($codDist&&$codDist!='none') ? "'".$this->PrepareParamSQL($codDist)."'" : "'00'",
							  $_SESSION['cod_usuario'],
							  //"'".$rubro1.$rubro2.$rubro3.$rubro4.$rubro5.$rubro6."000".$rubro10."'"
							  "'1111111111'"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Para q se actualice el XML
				$this->GeneraXMLRazonSocial($tipPersona);
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			if($this->userIntranet['COD_DEP']==48){
				$destination .= "&menu={$this->menu_items[0]['val']}";
			}else{
				$destination .= "&menu={$this->menu_items[1]['val']}";
			}
			header("Location: $destination");
			exit;
		}
	}

	function FormModificaPersona($id,$sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$reLoad=false,$errors=false){
		//echo "En mantenimiento";exit;
		global $bBusca;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		if($this->userIntranet['COD_DEP']!=12){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		/**/
			if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos de la Persona
			$sql_SP = sprintf("EXECUTE sp_listPersona %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					/**/			
					$id = $row[0];
					$sector = $row[1];
					$tipPersona = $row[2];
					$codDepa = $row[3];
					$codProv = $row[4];
					$codDist = $row[5];
					$tipIdent = $row[6];
					$razonsocial = $row[8];
					$nombres = $row[9];
					$apellidos = $row[10];
					$ruc = $row[11];
					$direccion = $row[12];
					$telefono = $row[13];
					$fax = $row[14];
					$mail = $row[15];
					$repLegal = $row[17];
					$nroRepLegal = $row[18];
					$tipIdentRepLegal = $row[19];
					$observaciones = $row[23];
					/**/
				}
				$rs->Close();
			}
			unset($rs);
			}//fin del if($reload)

		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('bBusca',$bBusca);

		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmModifyPersona.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}

	function ModificaPersona($id,$sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones){
		// Comprueba Valores	
		if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		//if(!$telefono){ $bC9 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		//if($codDepa<1){ $bC10 = true; $this->errors .= 'El Departamento debe ser especificado<br>'; }
		//if($codProv<1){ $bC11 = true; $this->errors .= 'La Provincia debe ser especificada<br>'; }
		//if($codDist<1){ $bC12 = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }

		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaPersona($id,$sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones,true,$errors);
			
			$objIntranet->Footer();
		}else{
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE sp_modPersona %d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,'%s'",
								$id,
							  ($sector) ? $this->PrepareParamSQL($sector) : "NULL",
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "NULL",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa) ? "'".$this->PrepareParamSQL($codDepa)."'" : "NULL",
							  ($codProv) ? "'".$this->PrepareParamSQL($codProv)."'" : "NULL",
							  ($codDist) ? "'".$this->PrepareParamSQL($codDist)."'" : "NULL",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Para q se actualice el XML
				$this->GeneraXMLRazonSocial($tipPersona);
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function GeneraXMLRazonSocial($tipo=1) {
		$this->abreConnDB();
		//$this->conn->debug = true;

		// Obtiene los Datos de la Persona
		$sql_SP = sprintf("SELECT id, CASE WHEN id_tipo_persona=1 THEN Upper(apellidos)+' '+Upper(nombres) ELSE substring(Upper(razon_social),1,100) END ".
						  "FROM db_general.dbo.persona ".
						  "WHERE id_tipo_persona=%d ".
						  "and FLAG='A' ".
						  "ORDER BY 2", $tipo);
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			$ii=0;
			while($row = $rs->FetchRow()){
				$valor=$row[0];
				$cadena=strtoupper($row[1]);
				//$cadena=htmlentities($cadena);
				if($cadena!=""){
					if(ord($cadena[0])>64&&ord($cadena[0])<96)					
						$x=ord($cadena[0])-65;
					if(ord($cadena[1])>64&&ord($cadena[1])<96)					
						$y=ord($cadena[1])-65;
					if((ord($cadena[0])>=48&&ord($cadena[0])<=57)||ord($cadena[0])==38)
						$x=ord($cadena[0]);
					if((ord($cadena[1])>=48&&ord($cadena[1])<=57)||ord($cadena[1])==38)					
						$y=ord($cadena[1]);
					// $arreglo[$x][$y]["Total"]++;
					// $ii=$arreglo[$x][$y]["Total"];
					$arreglo[$x][$y][] = array($valor, $cadena);
				}
				$cadena="";
			}
			$rs->Close();
		}
		unset($rs);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('elements',$arreglo);
		
		// Genera el archivo XML en la carpeta xml
		// header("Content-type: text/xml");
		$filename = $tipo==1 ? 'natural.xml' : 'juridica.xml';
		$fd = fopen($this->folderXML . '/' . $filename, 'w+');
		fputs($fd, $html->fetch('oad/tramite/XMLRazonSocial.tpl.php'));
		fclose($fd);		
	}
	
	function FormAgregaContacto($id,$nombres=NULL,$apellidos=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$radio=NULL,$errors=false){
		if($this->userIntranet['COD_DEP']!=12){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddContacto';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('radio',$radio);
		$html->assign_by_ref('id',$id);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddContacto.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	// Genera una Cadena Aleatoria que sera tomada como Password de Usuario
	function GeneraPassword($length){
		//$vowels = 'aeiouyAEIOUY';//5x2
		$vowels = '123456789012';
		//$consonants = 'bdghjlmnpqrstvwxzBDGHJLMNPQRSTVWXZ';//17x2
		$consonants = '1234567890123456712345678901234567';
		$password = '';
		
		$alt = time() % 2;
		srand(time());
	
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % 34)];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % 12)];
				$alt = 1;
			}
		}
		return $password;
	}
	
	function AgregaContacto($id,$nombres,$apellidos,$direccion,$telefono,$fax,$mail,$radio){
	global $id;//el id dela persona
	$tamanoNombre=strlen($nombres);
	$tamanoApellido=strlen($apellidos);
	$tamanoDireccion=strlen($direccion);
	$tamanoTelefono=strlen($telefono);
		if(!$nombres){ $bC1 = true; $this->errors .= 'El nombre debe ser especificado<br>'; }
		if(!$apellidos){ $bC2 = true; $this->errors .= 'Los Apellidos deben ser especificados<br>'; }
		if(!$direccion){ $bC3 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if(!$telefono){ $bC4 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		if($tamanoNombre<3){ $bC6= true; $this->errors .= 'El nombre debe tener al menos 3 caracteres<br>'; }
		if($tamanoApellido<2){ $bC7= true; $this->errors .= 'El apellido debe tener al menos 2 caracteres<br>'; }
		if($tamanoDireccion<10){ $bC8= true; $this->errors .= 'La direcci�n debe tener al menos 10 caracteres<br>'; }
		if($tamanoTelefono<7){ $bC9= true; $this->errors .= 'El tel�fono debe tener al menos 7 caracteres<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC5 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }

		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('tramite_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaContacto($id,$nombres,$apellidos,$direccion,$telefono,$fax,$mail,$radio,$errors);
			
			$objIntranet->Footer();
		}else{

			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			$clave = $this->GeneraPassword(6);
			
			$sql_SP = sprintf("EXECUTE sp_insContacto %d,%s,%s,%s,%s,%s,%s,'%s','%s','%s'",
							  $id,
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  $_SESSION['cod_usuario'],
							  md5($clave),
							  ($radio) ? $radio : "P"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				/*Ya no se se va a enviar por mail la clave
				//Para q se actualice el XML
				//$this->GeneraXMLRazonSocial($tipPersona);
						$desTitulo = "CLAVE PARA ACCESO A LA INFORMACI�N CONFIDENCIAL";
						$desMensaje = "Sr.".$apellidos." su clave es: ".$clave." ";
									
						$eFrom = "MIDIS@" . $this->emailDomain;
						$eDest = $mail;		
						// Envia el correo de Texto plano con el Adjunto
						$mail = & new htmlMimeMail();
						$mail->setSubject($desTitulo);
						$mail->setText($desMensaje);
						
						$mail->setFrom($eFrom);
						$mail->setBcc('jtumay@MIDIS.gob.pe');
						$mail->setReturnPath($eFrom);
						$result = $mail->send(array($eDest));
				//Fin de Para q se actualice el XML								
				*/
				
						$desTitulo = "CLAVE PARA ACCESO A LA INFORMACI�N CONFIDENCIAL";
						$desMensaje = "Sr.".$apellidos." su clave es: ".$clave." ";
									
						$eFrom = "MIDIS@" . $this->emailDomain;
						$eDest = $mail;		
						// Envia el correo de Texto plano con el Adjunto
						$mail = & new htmlMimeMail();
						$mail->setSubject($desTitulo);
						$mail->setText($desMensaje);
						
						$mail->setFrom($eFrom);
						$mail->setBcc('jtumay@MIDIS.gob.pe');
						$mail->setReturnPath($eFrom);
						$result = $mail->send(array($eDest));
				
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}&id={$id}&clave={$clave}";//Se manda el id de la Persona
			header("Location: $destination");
			exit;
		}
	}
	
	function ImprimeClave($id,$clave){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_SP = sprintf("Select CLAVE from contacto where id_persona=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				// Setea Campos del Formulario
				$html->assign('claveEncriptada',$row[0]);
				$html->assign('clave',$clave);
				//$html->assign('num',$row[1]);
				//$html->assign('rem',strtoupper($row[2]));
				//$html->assign('asunto',$row[3]);
				//$html->assign('folio',$row[4]);
				//$html->assign('fecRec',$row[5]);
				//$clave=$row[6].$row[7];
				//$html->assign('clave',$clave);
				//$html->assign('user',$row[8]);
				
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign_by_ref('FechaActual',$this->FechaActual());
				$html->assign_by_ref('HoraActual',$this->HoraActual());
				
				$html->display('oad/tramite/printClave.tpl.php');
			}
			$rs->Close();
		}
	}
	
	
	function DetallePersona($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql=sprintf("select s.descripcion,tp.descripcion,depa.departamento,prov.provincia,dist.distrito,tti.descripcion,
						case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
							 when p.id_tipo_persona=2 then p.razon_social end as RAZON_SOCIAL,p.nro_documento,p.direccion,p.telefono,p.fax,p.email,p.representante_legal,
						tti2.descripcion,p.nro_documento_representante
						from db_general.dbo.sector s,db_general.dbo.tipo_persona tp,
							 db_general.dbo.departamento depa,db_general.dbo.provincia prov,db_general.dbo.distrito dist,
							 db_general.dbo.t_tipo_identificacion tti,
							 db_general.dbo.persona p left join db_general.dbo.t_tipo_identificacion tti2 on p.id_tipo_identificacion_rep_leg=tti2.codigo_t_identificacion
						where p.id_sector=s.id and p.id_tipo_persona=tp.id_tipo_persona
						  and p.codigo_departamento=depa.codigo_departamento
						  and p.codigo_provincia=prov.codigo_provincia and prov.codigo_departamento=depa.codigo_departamento
						  and p.codigo_distrito=dist.codigo_distrito and dist.codigo_provincia=prov.codigo_provincia and dist.codigo_departamento=depa.codigo_departamento
						  and p.id_tipo_identificacion=tti.codigo_t_identificacion
						  and 
					      p.id=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$sector=$rs->fields[0];
			$tipPersona=$rs->fields[1];
			$departamento=$rs->fields[2];
			$provincia=$rs->fields[3];
			$distrito=$rs->fields[4];
			$tipIdent=$rs->fields[5];
			$RazonSocial=$rs->fields[6];
			$nroDoc=$rs->fields[7];
			$direccion=$rs->fields[8];
			$telefono=$rs->fields[9];
			$fax=$rs->fields[10];
			$email=$rs->fields[11];
			$RepLegal=$rs->fields[12];
			$tipIdentRep=$rs->fields[13];
			$nroDocRep=$rs->fields[14];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('sector',$sector);				  
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('departamento',$departamento);
		$html->assign_by_ref('provincia',$provincia);
		$html->assign_by_ref('distrito',$distrito);			
		$html->assign_by_ref('tipIdent',$tipIdent);	  
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('nroDoc',$nroDoc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('RepLegal',$RepLegal);
		$html->assign_by_ref('tipIdentRep',$tipIdentRep);
		$html->assign_by_ref('nroDocRep',$nroDocRep);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oad/tramite/directorio/printDetallesPersona.tpl.php') : $html->display('oad/tramite/directorio/showDetallesPersona.tpl.php');
	}
	
 function ImprimeDetalle($id){
  $this->DetallePersona($id, true);
 }
 
 function ControlExpedientes_($page=NULL,$tipReporte=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroExp=NULL,$anyo=NULL,$codigo=NULL,$fecInicio=NULL,$fecFin=NULL,$tipCorrespondencia=NULL,$nroCor=NULL,$anyo2=NULL,$fecInicio2=NULL,$fecFin2=NULL,$tipBusqueda2=NULL,$codigo2=NULL){
  global $Opciones1,$filetip,$filetip2,$filetip3,$filetip4,$usuario,$code_val,$code;
  global $sector,$tipAprobacion,$ubicacion;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmControlExpedientes_';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  $html->assign_by_ref('tipReporte',$tipReporte);
  $html->assign_by_ref('tipDocumento',$tipDocumento);
  $html->assign_by_ref('tipBusqueda',$tipBusqueda);
  $html->assign_by_ref('nroExp',$nroExp);
  $html->assign_by_ref('anyo',$anyo);
  $html->assign_by_ref('codigo',$codigo);
  $html->assign_by_ref('tipCorrespondencia',$tipCorrespondencia);
  $html->assign_by_ref('nroCor',$nroCor);
  $html->assign_by_ref('anyo2',$anyo2);
  $html->assign_by_ref('tipBusqueda2',$tipBusqueda2);
  $html->assign_by_ref('codigo2',$codigo2);
  $html->assign_by_ref('Opciones1',$Opciones1);
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('filetip',$filetip);
  $html->assign_by_ref('filetip2',$filetip2);
  $html->assign_by_ref('filetip3',$filetip3);
  $html->assign_by_ref('filetip4',$filetip4);
  $html->assign_by_ref('usuario',$usuario);
  $html->assign_by_ref('code_val',$code_val);
  $html->assign_by_ref('code',$code);
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);

  /*// Contenido Select Congresista
  $sql_st = "SELECT id, substring(razon_social,1,70) ".
        "FROM db_general.dbo.persona ".
        "where id_tipo_persona=2 ".
        "ORDER BY 2";
  $html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);*/
  
  // Fecha Ingreso
  $html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio,6,4), true));

  // Fecha Salida
  $html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true));

  // Fecha Ingreson de la Correspodencia
  $html->assign_by_ref('selMesIng2',$this->ObjFrmMes(1, 12, true, substr($fecInicio2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng2',$this->ObjFrmDia(1, 31, substr($fecInicio2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio2,6,4), true));

  // Fecha Salida de la Correspodencia
  $html->assign_by_ref('selMesSal2',$this->ObjFrmMes(1, 12, true, substr($fecFin2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal2',$this->ObjFrmDia(1, 31, substr($fecFin2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin2,6,4), true));
 
			/*$sql_sp="select count(*) from documento where id_estado_documento=1 and id_tipo_documento in (1,2) and auditmod>convert(datetime, '01/01/2008', 103) ";		
			$rs=& $this->conn->Execute($sql_sp);
			if(!$rs){	print $this->conn->ErrorMsg();		}
			else{		$DocDerivados=$rs->fields[0];				}
			$html->assign('DocDerivados',$DocDerivados);

		$sql_sp="select count(*) from correspondencia where fecentcourier is not null 
		         and convert(datetime,fecentcourier,103)>convert(datetime,'01/01/2008',103)";
			$rs=& $this->conn->Execute($sql_sp);
			if(!$rs){	print $this->conn->ErrorMsg();	}
			else{		$TotalCor=$rs->fields[0];			}		
			$html->assign('TotalCor',$TotalCor);

		$BarScale=80;
		
		$sql1="select count(*) from documento where id_tipo_documento in (1,2) and auditmod>convert(datetime,'01/01/2008',103)";
			$rs=& $this->conn->Execute($sql1);
			if(!$rs){	print $this->conn->ErrorMsg();		}
			else{		$sum=$rs->fields[0];				}
			$html->assign('TotalDoc',$sum);*/
/*Para los los grafos*/
				/*$percent = ($sum) ? (100*$DocDerivados)/$sum : 0;
				$percentInt = (int)$percent * $BarScale / 100;
				$percent2 = (int)$percent;
				if ($percent > 0) {
					$barra = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7>";
					$barra .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=$percentInt Alt=\"$percent2 %\">";
					$barra .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7>";
				} else {
					$barra = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=3 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
				}
				
				$option[] = array('barra'=>$barra);
		$html->assign_by_ref('option',$option);
				$percent = ($sum) ? (100*$TotalCor)/$sum : 0;
				$percentInt = (int)$percent * $BarScale / 100;
				$percent2 = (int)$percent;
				if ($percent > 0) {
					$barra2 = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7>";
					$barra2 .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=$percentInt Alt=\"$percent2 %\">";
					$barra2 .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7>";
				} else {
					$barra2 = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
					$barra2 .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=3 Alt=\"$percent2 %\">";
					$barra2 .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
				}
				
				$option2[] = array('barra2'=>$barra2);
		$html->assign_by_ref('option2',$option2);
				$percent = ($sum) ? (100*$TotalCor)/$sum : 0;
				$percentInt = (int)$percent * $BarScale / 100;
				$percent2 = (int)$percent;
				if ($percent > 0) {
					$barra4 = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7>";
					$barra4 .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=$percentInt Alt=\"$percent2 %\">";
					$barra4 .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7>";
				} else {
					$barra4 = "<img src=\"/img/portada/leftbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
					$barra4 .= "<img src=\"/img/portada/mainbar.gif\" height=14 width=3 Alt=\"$percent2 %\">";
					$barra4 .= "<img src=\"/img/portada/rightbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
				}
				
				$option4[] = array('barra4'=>$barra4);
		$html->assign_by_ref('option4',$option4);*/
 
 /**/
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}
 
 				/*$sql5="select DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS FINALIZADOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where D.V_ASESOR=1 AND DB_TRAMITE_DOCUMENTARIO.dbo.busca_estado_documento_F1(D.ID_DOCUMENTO)='FINALIZADO'
							and d.id_tipo_documento=2
							AND D.ID_ESTADO_DOCUMENTO<>9
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND TUP.TIPO_APROBACION IN (1)		
							AND MD.DERIVADO=0
							and md.finalizado=1
							and d.id_documento=md.id_documento
							AND V_ASESOR=1
							group by DEP2.SIGLAS";*/
				//el v_asesor=1 identifica los expedientes que se han finalizao fuera del plazo
				$sql5="select DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS FINALIZADOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND MD.DERIVADO=0
							and md.finalizado=1
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql5.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipAprobacion==2)
					$sql5.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql5.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql5.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql5.=" and tup.tipo_aprobacion=0 ";
					
				$sql5.=" group by DEP2.SIGLAS";					
							
				if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado5=mssql_query($sql5,$con);
					$num_campos5=mssql_num_fields($resultado5);
					$num_registros5=mssql_num_rows($resultado5);
					$j5=$num_registros5-1;
					$jj5=-1;
						while($datos5=mssql_fetch_array($resultado5)){
								$jj5++;
								for($i5=0;$i5<$num_campos5;$i5++){
									$arreglo5[$jj5][$i5]=$datos5[$i5];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalFinalizados=0;
				for($a5=0;$a5<$num_registros5;$a5++){
						$yyyy=$arreglo5[$a5][0];
						$finalizados[$yyyy]=$arreglo5[$a5][1];
						$TotalFinalizados=$TotalFinalizados+$arreglo5[$a5][1];
				}
				
				$sql6="select DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							
							AND MD.DERIVADO=0
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipAprobacion==2)
					$sql6.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql6.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql6.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql6.=" and tup.tipo_aprobacion=0 ";
			
				$sql6.=" group by DEP2.SIGLAS";
				if(!mssql_query($sql6,$con)){
					echo "error en la consulta";
				}else{
					$resultado6=mssql_query($sql6,$con);
					$num_campos6=mssql_num_fields($resultado6);
					$num_registros6=mssql_num_rows($resultado6);
					$j6=$num_registros6-1;
					$jj6=-1;
						while($datos6=mssql_fetch_array($resultado6)){
								$jj6++;
								for($i6=0;$i6<$num_campos6;$i6++){
									$arreglo6[$jj6][$i6]=$datos6[$i6];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalIngresos=0;
				for($a6=0;$a6<$num_registros6;$a6++){
						$mmmm=$arreglo6[$a6][0];
						$ingresos[$mmmm]=$arreglo6[$a6][1];
						$TotalIngresos=$TotalIngresos+$arreglo6[$a6][1];
				}							

			/*$sql4="select CASE WHEN TUP.TIPO_APROBACION=1 THEN 'POSITIVO'
	   							 				 WHEN TUP.TIPO_APROBACION=0 THEN 'NEGATIVO' END AS TIPO_APROBACION,
							DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS PENDIENTES																												  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2 
							where md.derivado=0 and md.finalizado=0
							and d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND TUP.TIPO_APROBACION IN (1,0)							
							and d.id_documento=md.id_documento
							
							group by DEP2.SIGLAS,TUP.TIPO_APROBACION";*/
				$sql4="select DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS PENDIENTES																												  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2 
							where md.derivado=0 and md.finalizado=0
							and d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
														
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql4.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql4.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql4.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipAprobacion==2)
					$sql4.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql4.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql4.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql4.=" and tup.tipo_aprobacion=0 ";
			
				$sql4.=" group by DEP2.SIGLAS";			
				
				if(!mssql_query($sql4,$con)){
					echo "error en la consulta";
				}else{
					$resultado4=mssql_query($sql4,$con);
					$num_campos4=mssql_num_fields($resultado4);
					$num_registros4=mssql_num_rows($resultado4);
					$j4=$num_registros4-1;
					$jj4=-1;
						while($datos4=mssql_fetch_array($resultado4)){
								$jj4++;
								for($i4=0;$i4<$num_campos4;$i4++){
									$arreglo4[$jj4][$i4]=$datos4[$i4];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				for($a4=0;$a4<$num_registros4;$a4++){
					/*$tipAprobacion4[] = $arreglo4[$a4][0];*/

					/*if($tipAprobacion4[$a4]=="POSITIVO"){
						$xxxx=$arreglo4[$a4][1];
						$depPositivo[$xxxx]=$arreglo4[$a4][2];
					}else{
						$xxxx=$arreglo4[$a4][1];
						$depNegativo[$xxxx]=$arreglo4[$a4][2];
					}*/
					//echo "XX".$xxxx."XX<br>";	
					$xxxx=$arreglo4[$a4][0];
						$depPositivo[$xxxx]=$arreglo4[$a4][1];
				}		
/********Situaci�n de expedientes******/
				/*$sql3="select DEP.SIGLAS AS UBICACION_DEPENDENCIA,d.id_tipo_documento AS TIPO_DOCUMENTO,count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS, CASE WHEN TUP.TIPO_APROBACION=1 THEN 'POSITIVO'
	   							 									   						   	  				  WHEN TUP.TIPO_APROBACION=0 THEN 'NEGATIVO' END AS TIPO_APROBACION,
							COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=1 THEN 1 END) AS XVENCER1,
							COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=2 THEN 1 END) AS XVENCER2,
							COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=3 THEN 1 END) AS XVENCER3,
							DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS PENDIENTES,count(case when d.id_estado_documento=9 then 1 end) as NOTIFICADOS																												  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,DB_GENERAL.dbo.H_DEPENDENCIA DEP,
								 DB_GENERAl.dbo.h_dependencia dep2 
							where md.derivado=0 and md.finalizado=0
							and d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND TUP.TIPO_APROBACION IN (1,0)
							and d.id_documento=md.id_documento
							AND MD.ID_DEPENDENCIA_DESTINO=DEP.CODIGO_DEPENDENCIA 
							group by DEP2.SIGLAS,DEP.SIGLAS,d.id_tipo_documento,TUP.TIPO_APROBACION
							order by DEP2.SIGLAS,DEP.SIGLAS,d.id_tipo_documento,TUP.TIPO_APROBACION
							";*/
				$sql3="select DEP.SIGLAS AS UBICACION_DEPENDENCIA,'',count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS, '' AS TIPO_APROBACION,
							/*COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=1 THEN 1 END)*/'' AS XVENCER1,
							/*COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=2 THEN 1 END)*/'' AS XVENCER2,
							/*COUNT(CASE WHEN (tup.numero_dias-dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)))=3 THEN 1 END)*/'' AS XVENCER3,
							DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS PENDIENTES,count(case when d.id_estado_documento=9 then 1 end) as NOTIFICADOS																												  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,DB_GENERAL.dbo.H_DEPENDENCIA DEP,
								 DB_GENERAl.dbo.h_dependencia dep2 
							where md.derivado=0 and md.finalizado=0
							and d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							
							and d.id_documento=md.id_documento
							AND MD.ID_DEPENDENCIA_DESTINO=DEP.CODIGO_DEPENDENCIA";
				if($sector==2)//Industria
					$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipAprobacion==2)
					$sql3.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql3.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql3.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql3.=" and tup.tipo_aprobacion=0 ";
			
				$sql3.=" group by DEP2.SIGLAS,DEP.SIGLAS
						 order by DEP2.SIGLAS,DEP.SIGLAS";			
							  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$Ext = array();
				//$Exp = array();
				//$Int = array();
				$contPositivo=0;
				$contNegativo=0;
				
				$TotalVencidos=0;
				$TotalXVencer1=0;
				$TotalXVencer2=0;
				$TotalXVencer3=0;
				$TotalNotificados=0;
				
				$TotalXVencerPositivo3=0;
				$TotalXVencerNegativo3=0;
				$TotalXVencerPositivo2=0;
				$TotalXVencerNegativo2=0;
				$TotalXVencerPositivo1=0;
				$TotalXVencerNegativo1=0;
				$TotalPositivoNotificados=0;
				$TotalNegativoNotificados=0;
				$TotalPositivoVencidos=0;
				$TotalNegativoVencidos=0;
				$TotalPendientesPositivo=0;
				$TotalPendientesNegativo=0;
				$TotalPendientesMayor3DiasPositivo=0;
				$TotalPendientesMayor3DiasNegativo=0;
				
				for($a3=0;$a3<$num_registros3;$a3++){
					/*$tipAprobacion[] = $arreglo3[$a3][3];*/
					
					/*if($tipAprobacion[$a3]=="POSITIVO"){*/
						$contPositivo=$contPositivo+1;
						$dependenciaP[] = $arreglo3[$a3][0];
						$vencidosP[] = $arreglo3[$a3][2];
						$xvencer1P[] = $arreglo3[$a3][4];
						$xvencer2P[] = $arreglo3[$a3][5];
						$xvencer3P[] = $arreglo3[$a3][6];
						$dependenciaRespP[] = $arreglo3[$a3][7];
						$PendientesP[] = $arreglo3[$a3][8];
						$NotificadosP[] = $arreglo3[$a3][9];
						
						$yyyy=$arreglo3[$a3][7];
						$depPositivoXVencer3[$yyyy]=$depPositivoXVencer3[$yyyy]+$arreglo3[$a3][6];
						$depPositivoXVencer2[$yyyy]=$depPositivoXVencer2[$yyyy]+$arreglo3[$a3][5];
						$depPositivoXVencer1[$yyyy]=$depPositivoXVencer1[$yyyy]+$arreglo3[$a3][4];
						$depPositivoNotificadosP[$yyyy]=$depPositivoNotificadosP[$yyyy]+$arreglo3[$a3][9];
						$depPositivoVencidosP[$yyyy]=$depPositivoVencidosP[$yyyy]+$arreglo3[$a3][2];
						
						$TotalXVencerPositivo3=$TotalXVencerPositivo3+$arreglo3[$a3][6];
						$TotalXVencerPositivo2=$TotalXVencerPositivo2+$arreglo3[$a3][5];
						$TotalXVencerPositivo1=$TotalXVencerPositivo1+$arreglo3[$a3][4];
						$TotalPositivoNotificados=$TotalPositivoNotificados+$arreglo3[$a3][9];
						$TotalPositivoVencidos=$TotalPositivoVencidos+$arreglo3[$a3][2];
						$TotalPendientesPositivo=$TotalPendientesPositivo+$arreglo3[$a3][8];
						$TotalPendientesMayor3DiasPositivo=$TotalPendientesPositivo-$TotalXVencerPositivo3-$TotalXVencerPositivo2-$TotalXVencerPositivo1-$TotalPositivoVencidos;
					/*}else{
						$contNegativo=$contNegativo+1;
						$dependenciaN[] = $arreglo3[$a3][0];
						$vencidosN[] = $arreglo3[$a3][2];
						$xvencer1N[] = $arreglo3[$a3][4];
						$xvencer2N[] = $arreglo3[$a3][5];
						$xvencer3N[] = $arreglo3[$a3][6];
						$dependenciaRespN[] = $arreglo3[$a3][7];
						$PendientesN[] = $arreglo3[$a3][8];
						$NotificadosN[] = $arreglo3[$a3][9];
						
						$yyyy=$arreglo3[$a3][7];
						$depNegativoXVencer3[$yyyy]=$depNegativoXVencer3[$yyyy]+$arreglo3[$a3][6];
						$depNegativoXVencer2[$yyyy]=$depNegativoXVencer2[$yyyy]+$arreglo3[$a3][5];
						$depNegativoXVencer1[$yyyy]=$depNegativoXVencer1[$yyyy]+$arreglo3[$a3][4];
						$depNegativoNotificadosN[$yyyy]=$depNegativoNotificadosN[$yyyy]+$arreglo3[$a3][9];
						$depNegativoVencidosN[$yyyy]=$depNegativoVencidosN[$yyyy]+$arreglo3[$a3][2];
						
						$TotalXVencerNegativo3=$TotalXVencerNegativo3+$arreglo3[$a3][6];
						$TotalXVencerNegativo2=$TotalXVencerNegativo2+$arreglo3[$a3][5];
						$TotalXVencerNegativo1=$TotalXVencerNegativo1+$arreglo3[$a3][4];
						$TotalNegativoNotificados=$TotalNegativoNotificados+$arreglo3[$a3][9];
						$TotalNegativoVencidos=$TotalNegativoVencidos+$arreglo3[$a3][2];
						$TotalPendientesNegativo=$TotalPendientesNegativo+$arreglo3[$a3][8];
						$TotalPendientesMayor3DiasNegativo=$TotalPendientesNegativo-$TotalXVencerNegativo3-$TotalXVencerNegativo2-$TotalXVencerNegativo1-$TotalNegativoVencidos;
					}*/
					$TotalVencidos=$TotalVencidos+$arreglo3[$a3][2];
					$TotalXVencer1=$TotalXVencer1+$arreglo3[$a3][4];
					$TotalXVencer2=$TotalXVencer2+$arreglo3[$a3][5];
					$TotalXVencer3=$TotalXVencer3+$arreglo3[$a3][6];
					$TotalNotificados=$TotalNotificados+$arreglo3[$a3][9];
				}	//fin del for($a=0...)
				
				$TotaldifVencNotiPositivo=$TotalPositivoVencidos-$TotalPositivoNotificados;
				$TotaldifVencNotiNegativo=$TotalNegativoVencidos-$TotalNegativoNotificados;
				
					//$fecha=date('d/m/Y H:i');
					$desTitulo = "SITUACI�N DE LOS EXPEDIENTES CON SILENCIO POSITIVO Y NEGATIVO";
					//$desTitulo = "RESUMEN DE SU DOCUMENTACI�N AL ".$fecha;
								$ceros=0;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;

 
  $desMensaje="<table width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr> 
    <td><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">
        <!--<tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Estimado(a) ".$nomDirector.": A la fecha remito la situaci�n de los expedientes con silencio positivo y negativo</font></td>
        </tr>-->
        <tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;&nbsp;&nbsp;</font></td>
        </tr>				
      </table></td>
  </tr>
  <tr> 
    <td bgcolor=\"#C6E8FF\"><table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">
        <!--<tr> 
          <td width=\"190\" rowspan=\"2\" align=\"center\" bgcolor=\"#BED5FA\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">OFICINA A CARGO</font></b></td>
		  <td width=\"170\" rowspan=\"2\" align=\"center\" bgcolor=\"#BED5FA\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">UBICACI�N ACTUAL</font></b></td>
		  <td width=\"110\" rowspan=\"2\" align=\"center\" bgcolor=\"#BED5FA\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">PENDIENTES MAYOR 3 D�AS</font></b></td>
          <td width=\"180\" colspan=\"3\" align=\"center\" bgcolor=\"#BED5FA\" title=\"Total de documentos existentes en la dependencia o en poder del usuario\"><b><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"1\">D�as por Vencer
            </font></b></td>
          <td rowspan=\"2\" bgcolor=\"#BED5FA\" align=\"center\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Vencidos<br /></font></b></td>
		  <td align=\"center\" bgcolor=\"#BED5FA\" rowspan=\"2\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Notificados</font></b></td>	
		  <td align=\"center\" bgcolor=\"#BED5FA\" rowspan=\"2\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Total</font></b></td>
		  <td align=\"center\" bgcolor=\"#C8E8F8\" rowspan=\"2\" width=\"2\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></b></td>
		  <td align=\"center\" bgcolor=\"#BED5FA\" rowspan=\"2\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados fuera del plazo</font></b></td>	
        </tr>
        <tr>
          <td align=\"center\" bgcolor=\"#BED5FA\"><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"1\">3</font></td>
          <td align=\"center\" bgcolor=\"#BED5FA\"><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"1\">2</font></td>
          <td align=\"center\" bgcolor=\"#BED5FA\"><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"1\">1</font></td>
        </tr>-->
		<tr bgcolor=\"#6496CB\"> 
          <td width=\"190\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">OFICINA A CARGO</font></b></td>
		  <td width=\"170\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">UBICACI�N ACTUAL</font></b></td>
          <!--<td class=\"textowhite\"><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Vencidos<br /></font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Notificados</font></b></td>-->
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Ingresos</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes</font></b></td>
        </tr>		";
		
		$desMensaje.="<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><!--SILENCIO POSITIVO-->TOTAL</font></strong></td>
		  <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"></font></strong></td>
		  <!--<td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesMayor3DiasPositivo."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerPositivo3."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerPositivo2."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerPositivo1."</font></strong></td>-->
		  <!--<td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotaldifVencNotiPositivo."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPositivoNotificados."</font></strong></td>-->
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalIngresos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalFinalizados."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesPositivo."</font></strong></td>
        </tr>";
		$sumaPendMayor3dias=0;
		
		
        for($j=0;$j<$contPositivo;$j++){
			$difVencNotiP=$vencidosP[$j]-$NotificadosP[$j];
			$pendientesMayor3DiasP=$PendientesP[$j]-$xvencer3P[$j]-$xvencer2P[$j]-$xvencer1P[$j]-$vencidosP[$j];
			$sumaPendMayor3dias=$sumaPendMayor3dias+$pendientesMayor3DiasP;
			
			if($dependenciaRespP[$j]!=$dependenciaRespP[$j-1]){
				$depPositivodifVencNotiP[$dependenciaRespP[$j]]=$depPositivoVencidosP[$dependenciaRespP[$j]]-$depPositivoNotificadosP[$dependenciaRespP[$j]];
				$PendMayor3diasPositivoNotiP[$dependenciaRespP[$j]]=$depPositivo[$dependenciaRespP[$j]]-$depPositivoXVencer3[$dependenciaRespP[$j]]-$depPositivoXVencer2[$dependenciaRespP[$j]]-$depPositivoXVencer1[$dependenciaRespP[$j]]-$depPositivoVencidosP[$dependenciaRespP[$j]];
				$desMensaje.="<tr><td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaRespP[$j]."</font></td>
								  <td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"></font></td>
								  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$PendMayor3diasPositivoNotiP[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depPositivoXVencer3[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depPositivoXVencer2[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depPositivoXVencer1[$dependenciaRespP[$j]]."</font></strong></td>-->
								  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depPositivodifVencNotiP[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depPositivoNotificadosP[$dependenciaRespP[$j]]."</font></strong></td>-->
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$ingresos[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$finalizados[$dependenciaRespP[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?sector=".$sector."&tipAprobacion=".$tipAprobacion."&ubicacion=".$dependenciaRespP[$j]."\">".$depPositivo[$dependenciaRespP[$j]]."</a></font></strong></td>
							</tr>
				";		
			}
						
			$desMensaje.="<tr>";
			if($dependenciaRespP[$j]==$dependenciaRespP[$j-1]){
				$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
				$sumaX=$sumaX+$PendientesP[$j];
			}else{
				//$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaRespP[$j]."(".$depPositivo[$dependenciaRespP[$j]].")</font></td>";
				$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
				unset($sumaX);
				$sumaX=$PendientesP[$j];
			}	
			$desMensaje.=" 
			  
			  <td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaP[$j]."(".$PendientesP[$j].")</font></td>
			  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesMayor3DiasP."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer3P[$j]."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer2P[$j]."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer1P[$j]."</font></td>-->
			  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$difVencNotiP."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$NotificadosP[$j]."</font></td>-->";
			if($dependenciaRespP[$j]==$dependenciaRespP[$j+1]){
			  //$desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
			  $a=$a+1;
			}else{
				$a=$a+1;  
			  //$desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$sumaX."</font></strong></td>";
			  //$desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>";
			  $sumaXT=$sumaXT+$sumaX;
			  unset($a);
			  //$a=0;
			} 
			/*if($dependenciaRespP[$j]!=$dependenciaRespP[$j-1]){
				$desMensaje.="<td align=\"center\" rowspan=".$a." bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$sumaX."</font></td>";
			}*/
				 
			$desMensaje.="<!--<td align=\"center\" ><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>-->
						  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
						  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
						  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
						</tr>";
			if($dependenciaRespP[$j]==$dependenciaRespP[$j+1]){
				$eee=0;
			}else{
				$desMensaje.="<tr height=\"1\"> 
					  <td colspan=\"9\" align=\"left\" ><img src=\"/img/800x600/cab.yellow2.gif\" alt=\"\" width=\"100%\" height=\"1\"/></td>
					</tr>";
			}
		}

		$desMensaje.="<!--<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">SILENCIO NEGATIVO</font></strong></td>
		  <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"></font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesMayor3DiasNegativo."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerNegativo3."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerNegativo2."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencerNegativo1."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotaldifVencNotiNegativo."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalNegativoNotificados."</font></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesNegativo."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#C8E8F8\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
        </tr>-->";
		
		for($k=0;$k<$contNegativo;$k++){
			$difVencNotiN=$vencidosN[$k]-$NotificadosN[$k];
			$pendientesMayor3DiasN=$PendientesN[$k]-$xvencer3N[$k]-$xvencer2N[$k]-$xvencer1N[$k]-$vencidosN[$k];
			$sumaPendMayor3dias=$sumaPendMayor3dias+$pendientesMayor3DiasN;
			
			if($dependenciaRespN[$k]!=$dependenciaRespN[$k-1]){
				$depNegativodifVencNotiN[$dependenciaRespN[$k]]=$depNegativoVencidosN[$dependenciaRespN[$k]]-$depNegativoNotificadosN[$dependenciaRespN[$k]];
				$PendMayor3diasNegativoNotiN[$dependenciaRespN[$k]]=$depNegativo[$dependenciaRespN[$k]]-$depNegativoXVencer3[$dependenciaRespN[$k]]-$depNegativoXVencer2[$dependenciaRespN[$k]]-$depNegativoXVencer1[$dependenciaRespN[$k]]-$depNegativoVencidosN[$dependenciaRespN[$k]];
				$desMensaje.="<tr><td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaRespN[$k]."</font></td>
								  <td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"></font></td>
								  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$PendMayor3diasNegativoNotiN[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativoXVencer3[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativoXVencer2[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativoXVencer1[$dependenciaRespN[$k]]."</font></strong></td>-->
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativodifVencNotiN[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativoNotificadosN[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$depNegativo[$dependenciaRespN[$k]]."</font></strong></td>
								  <td align=\"center\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td></tr>
				";		
			}			
			
			$desMensaje.="<tr>";
			if($dependenciaRespN[$k]==$dependenciaRespN[$k-1]){
				$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
				$sumaY=$sumaY+$PendientesN[$k];
			}else{
				//$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaRespN[$k]."(".$depNegativo[$dependenciaRespN[$k]].")</font></td>";
				$desMensaje.="<td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
				unset($sumaY);
				$sumaY=$PendientesN[$k];
			}	
			$desMensaje.=" 
			  
			  <td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$dependenciaN[$k]."(".$PendientesN[$k].")</font></td>
			  <!--<td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesMayor3DiasN."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer3N[$k]."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer2N[$k]."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$xvencer1N[$k]."</font></td>-->
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$difVencNotiN."</font></td>
			  <td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$NotificadosN[$k]."</font></td>";
			  
			if($dependenciaRespN[$k]==$dependenciaRespN[$k+1]){
			  $desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><font color=\"#FF0000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></td>";
			  //$a=$a+1;
			}else{
				//$a=$a+1;  
			  //$desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$sumaY."</font></strong></td>";
			  $desMensaje.="<td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>";
			  $sumaYT=$sumaYT+$sumaY;
			  //unset($a);
			} 
				 
			$desMensaje.="<td align=\"center\" ><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
						  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;</font></strong></td>
						  </tr>";
			if($dependenciaRespN[$k]==$dependenciaRespN[$k+1]){
				$eee=0;
			}else{
				$desMensaje.="<tr height=\"1\"> 
					  <td colspan=\"9\" align=\"left\" ><img src=\"/img/800x600/cab.yellow2.gif\" alt=\"\" width=\"100%\" height=\"1\"/></td>
					</tr>";
			}			
			
		}
		$difTotalVencNoti=$TotalVencidos-$TotalNotificados;
		$SUMAT=$sumaYT+$sumaXT;
        $desMensaje.="<!--<tr bgcolor=\"#BED5FA\"> 
          <td align=\"center\" bgcolor=\"#BED5FA\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">TOTAL</font></strong></td>
		  <td align=\"center\" bgcolor=\"#BED5FA\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"></font></strong></td>
          <td align=\"center\"><font color=\"#666666\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$sumaPendMayor3dias."</font></td>
		  <td align=\"center\"><font color=\"#666666\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencer3."</font></td>
		  <td align=\"center\"><font color=\"#666666\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencer2."</font></td>
		  <td align=\"center\"><font color=\"#666666\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalXVencer1."</font></td>
          <td align=\"center\" bgcolor=\"#EBF2FE\"><font color=\"#FF0000\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$difTotalVencNoti."</font></td>
		  <td align=\"center\" bgcolor=\"#EBF2FE\"><font color=\"#FF0000\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalNotificados."</font></td>
		  <td align=\"center\" bgcolor=\"#FFFFFF\"><strong><font color=\"#000000\" size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$SUMAT."</font></strong></td>-->
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td></td>
  </tr>
</table>
";

  $html->assign_by_ref('muestraStatusExp',$desMensaje);
  
/**/

/*Query para buscar los pendientes en total*/

				$sql7="select DEP2.SIGLAS AS RESPONSABLE,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							
							AND MD.DERIVADO=0
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql7.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql7.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql7.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipAprobacion==2)
					$sql7.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql7.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql7.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql7.=" and tup.tipo_aprobacion=0 ";
											
				$sql7.=" group by DEP2.SIGLAS
							order by DEP2.SIGLAS";
				if(!mssql_query($sql7,$con)){
					echo "error en la consulta";
				}else{
					$resultado7=mssql_query($sql7,$con);
					$num_campos7=mssql_num_fields($resultado7);
					$num_registros7=mssql_num_rows($resultado7);
					$j7=$num_registros7-1;
					$jj7=-1;
						while($datos7=mssql_fetch_array($resultado7)){
								$jj7++;
								for($i7=0;$i7<$num_campos7;$i7++){
									$arreglo7[$jj7][$i7]=$datos7[$i7];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				for($a7=0;$a7<$num_registros7;$a7++){
						$dependencia_[]=$arreglo7[$a7][0];
						$nroPendientes_[]=$arreglo7[$a7][1];
				}							

		/*Estos son los datos para la generaci�n del pie*/
		$PG = new PowerGraphic;
		
		$PG->title     = 'Distribuci�n de Ingresos por dependencia';
		//$PG->axis_x    = 'Mes';
		$PG->axis_x    = 'Dependencia';
		$PG->axis_y    = 'Nro. Expedientes';
		$PG->graphic_1 = 'Year 2004';
		$PG->graphic_2 = 'Year 2003';
		$PG->type      = 1;
		$PG->skin      = 1;
		$PG->credits   = 0;
		
		for($i=0;$i<count($dependencia_);$i++){
			$PG->x[$i] = $dependencia_[$i];
			$PG->y[$i] = $nroPendientes_[$i];
		}
		
		// Set values
		/*$PG->x[0] = 'Ene';
		$PG->y[0] = 35000;
		
		
		$PG->x[1] = 'Feb';
		$PG->y[1] = 38500;
		
		$PG->x[2] = 'Mar';
		$PG->y[2] = 40800;
		
		$PG->x[3] = 'Abr';
		$PG->y[3] = 45200;
		//$PG->y[3] = $contador4;		
		
		$PG->x[4] = 'May';
		$PG->y[4] = 46800;
		//$PG->y[4] = $contador5;		
		
		$PG->x[5] = 'Jun';
		$PG->y[5] = 55000;
		//$PG->y[5] = $contador6;*/
		/*$PG->x[6] = 'Jul';
		$PG->y[6] = $contador7;
		$PG->x[7] = 'Ago';
		$PG->y[7] = $contador8;
		$PG->x[8] = 'Sep';
		$PG->y[8] = $contador9;
		$PG->x[9] = 'Oct';
		$PG->y[9] = $contador10;
		$PG->x[10] = 'Nov';
		$PG->y[10] = $contador11;
		$PG->x[11] = 'Dic';
		$PG->y[11] = $contador12;				*/

//$matrix=$PG->create_query_string();
//echo "matrix is here".$matrix."matrix is here";exit;			
//$m='class.graphic.php?' . $PG->create_query_string() . '';
//$m.='<span style=\"font-size: 17px;\">&#8226; </span> <br /><br />';
$m.='<span style=\"font-size: 17px;\"></span><!-- <br /><br />-->';
/*$m.='1. Barras Verticales: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="imagen1" /><br /><br />';*/

// Changing the type

$PG->type = 2;
/*$m.='2. Barras Horizontales: <br /><br />';*/
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';

/*$PG->type = 3;
$m.='3. Puntos: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';*/

/*$PG->type = 4;
$m.='4. L�neas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';
*/
/*$PG->type = 5;
//$m.='5. Pie: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';

$PG->type = 6;
$m.='6. Donas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';
*/
// Clear parameters
$PG->reset_values();
		$html->assign_by_ref('m',$m);
		/**/
		
				$sql="select d.id_documento,tup.id_tupa,case when d.id_tipo_documento=1 then d.asunto
																  when d.id_tipo_documento=2 then tup.descripcion
																  when (d.id_tipo_documento=4 or d.id_tipo_documento=5) then d.asunto end,
						  case when d.id_tipo_documento=4 then dbo.buscaReferencia(md.id_movimiento_documento)
							   else d.num_tram_documentario end,
						  case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
							   when sol.id_tipo_persona=2 then sol.razon_social
							   when d.id_tipo_documento=5 then d.referencia 
							   else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103),
							convert(varchar,md.audit_mod,103),t.apellidos_trabajador+' '+t.nombres_trabajador,
							dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)),
							case when (d.id_tipo_documento in (1,2) and d.indicativo_oficio not in ('S/N','SN','S.N.')) then d.indicativo_oficio end,
							md.ultimo_avance,dep.siglas,tup.numero_dias,d.fecha_max_plazo,d.id_estado_documento
						from documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
									 left join db_general.dbo.persona sol on d.id_persona=sol.id,
								movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO',
								db_general.dbo.h_dependencia dep,db_general.dbo.h_dependencia dep2
						WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
								and dep.codigo_dependencia=md.id_dependencia_destino
								and d.id_tipo_documento=2
								and d.id_tup not in (65,66)
								and dep2.codigo_dependencia=tup.codigo_dependencia
								  ";
				if($sector==2)//Industria
					$sql.=" and tup.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql.=" and tup.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql.=" and tup.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */							  
								  
				if($tipAprobacion==4)
					$sql.=" and tup.tipo_aprobacion=1";
				elseif($tipAprobacion==5)
					$sql.=" and tup.tipo_aprobacion=0";
				elseif($tipAprobacion==2)
					$sql.=" and tup.tipo_aprobacion=2";
				elseif($tipAprobacion==3)
					$sql.=" and tup.tipo_aprobacion is null";
					
				if($ubicacion && $ubicacion!="")
					$sql.=" and dep2.siglas='".$ubicacion."' ";
					
				$sql.=" order by 9 desc";							
  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow()){
						$difDias2=$row[8]-$row[12];//DiasUtiles - DiasTupa
						if($difDias2>=0)
							$estado2="VENCIDO";
						else
							$estado2="DENTRO DEL PLAZO";
							
						$html->append('list',array('id' => $row[0],
												   'tu' => $row[1],
												   'tup' => $this->formateaTexto($row[2]),
												   'nroTD' => $row[3],
												   'razSoc' => $row[4],
												   'fecIngP' => $row[5],
												   'fecIngD' => $row[6],
												   'trab' => $row[7],
												   'diasUtiles' => $row[8],
												   'ind' => $row[9],
												   'comentario' => $row[10],
												   'ubicacionDepe' => $row[11],
												   'diasTupa' => $row[12],
												   'fecPlazo' => $row[13],
												   'idEstadoDoc' => $row[14],
												   'estado'=> $estado2
												   ));							
				}//fin del while
			}						
 
 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  $html->display('oad/tramite/controlExpedientes/index.tpl.php');
 } 
 
function compara_fechas($fecha1,$fecha2)
            
 
{
            
 
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("/",$fecha1);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("-",$fecha1);
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("/",$fecha2);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("-",$fecha2);
        $dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
        return ($dif);                         
            
 
}	 
 
 function ControlExp($page=NULL,$tipReporte=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroExp=NULL,$anyo=NULL,$codigo=NULL,$fecInicio=NULL,$fecFin=NULL,$tipCorrespondencia=NULL,$nroCor=NULL,$anyo2=NULL,$fecInicio2=NULL,$fecFin2=NULL,$tipBusqueda2=NULL,$codigo2=NULL){
  global $Opciones1,$filetip,$filetip2,$filetip3,$filetip4,$usuario,$code_val,$code;
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$print,$GrupoOpciones1,$orden;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $orden=($_POST['orden']) ? $_POST['orden'] : $_GET['orden'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmControlExp';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  $html->assign_by_ref('tipReporte',$tipReporte);
  $html->assign_by_ref('tipDocumento',$tipDocumento);
  $html->assign_by_ref('tipBusqueda',$tipBusqueda);
  $html->assign_by_ref('nroExp',$nroExp);
  $html->assign_by_ref('anyo',$anyo);
  $html->assign_by_ref('codigo',$codigo);
  $html->assign_by_ref('tipCorrespondencia',$tipCorrespondencia);
  $html->assign_by_ref('nroCor',$nroCor);
  $html->assign_by_ref('anyo2',$anyo2);
  $html->assign_by_ref('tipBusqueda2',$tipBusqueda2);
  $html->assign_by_ref('codigo2',$codigo2);
  $html->assign_by_ref('Opciones1',$Opciones1);
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('filetip',$filetip);
  $html->assign_by_ref('filetip2',$filetip2);
  $html->assign_by_ref('filetip3',$filetip3);
  $html->assign_by_ref('filetip4',$filetip4);
  $html->assign_by_ref('usuario',$usuario);
  $html->assign_by_ref('code_val',$code_val);
  $html->assign_by_ref('code',$code);
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
  $html->assign_by_ref('orden',$orden);
  $html->assign_by_ref('print',$print);  
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  /*// Contenido Select Congresista
  $sql_st = "SELECT id, substring(razon_social,1,70) ".
        "FROM db_general.dbo.persona ".
        "where id_tipo_persona=2 ".
        "ORDER BY 2";
  $html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);*/
  
  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  // Fecha Ingreso
  $html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio,6,4), true));

  // Fecha Salida
  $html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true));

  // Fecha Ingreson de la Correspodencia
  $html->assign_by_ref('selMesIng2',$this->ObjFrmMes(1, 12, true, substr($fecInicio2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng2',$this->ObjFrmDia(1, 31, substr($fecInicio2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio2,6,4), true));

  // Fecha Salida de la Correspodencia
  $html->assign_by_ref('selMesSal2',$this->ObjFrmMes(1, 12, true, substr($fecFin2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal2',$this->ObjFrmDia(1, 31, substr($fecFin2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin2,6,4), true));
 
if($this->compara_fechas($FechaIni,$FechaFin)>0){
	echo "Criterio incorrecto de fechas";
}else{ 
 
 /**/
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}
 
/*lISTADO DE LOS DOCUMENTOS FINALIZADOS*/
		if ($GrupoOpciones1==2){//Por ubicaci�n
				$sql5="select DEP2.dependencia AS UBICACION, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP, 
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento=2 AND D.ID_TUP=TUP.ID_TUPA 
						and dep2.codigo_dependencia=md.id_dependencia_destino
						and tup.codigo_dependencia>0
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipProceso==2)
					$sql5.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql5.=" and tup.id_clase_tupa=2 ";
					
				if($dependenciaCargo>0)
					$sql5.=" and md.id_dependencia_destino=".$dependenciaCargo;
										
				if($tipAprobacion==2)
					$sql5.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql5.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql5.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql5.=" and tup.tipo_aprobacion=0 ";
					
				$sql5.=" group by DEP2.dependencia";
		}else{//Por Oficina a cargo
				$sql5="select DEP2.dependencia AS RESPONSABLE, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP, 
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento=2 AND D.ID_TUP=TUP.ID_TUPA 
						and dep2.codigo_dependencia=tup.codigo_dependencia 
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipProceso==2)
					$sql5.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql5.=" and tup.id_clase_tupa=2 ";
					
				if($dependenciaCargo>0)
					$sql5.=" and dep2.codigo_dependencia=".$dependenciaCargo;
										
				if($tipAprobacion==2)
					$sql5.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql5.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql5.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql5.=" and tup.tipo_aprobacion=0 ";
					
				$sql5.=" group by DEP2.dependencia";
				
		}//fin del if ($GrupoOpciones1==2)				
				//echo $sql5;			
				if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado5=mssql_query($sql5,$con);
					$num_campos5=mssql_num_fields($resultado5);
					$num_registros5=mssql_num_rows($resultado5);
					$j5=$num_registros5-1;
					$jj5=-1;
						while($datos5=mssql_fetch_array($resultado5)){
								$jj5++;
								for($i5=0;$i5<$num_campos5;$i5++){
									$arreglo5[$jj5][$i5]=$datos5[$i5];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalFinalizados=0;
				for($a5=0;$a5<$num_registros5;$a5++){
						$yyyy=$arreglo5[$a5][0];
						$finalizados[$yyyy]=$arreglo5[$a5][1];
						$TotalFinalizados=$TotalFinalizados+$arreglo5[$a5][1];
				}
				//echo $TotalFinalizados;
				//echo "<br>".$sql5;exit;
				
		if ($GrupoOpciones1==2){//Por ubicaci�n
				$sql6="select DEP2.dependencia AS UBICACION,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=md.id_dependencia_destino
							and tup.codigo_dependencia>0
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql6.=" and md.id_dependencia_destino=".$dependenciaCargo;	
					
				if($tipProceso==2)
					$sql6.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql6.=" and tup.id_clase_tupa=2 ";
					
				if($tipAprobacion==2)
					$sql6.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql6.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql6.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql6.=" and tup.tipo_aprobacion=0 ";
			
				$sql6.=" group by DEP2.dependencia";
		}else{	//Por Oficina a cargo					
				$sql6="select DEP2.dependencia AS RESPONSABLE,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql6.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
					
				if($tipProceso==2)
					$sql6.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql6.=" and tup.id_clase_tupa=2 ";
					
				if($tipAprobacion==2)
					$sql6.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql6.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql6.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql6.=" and tup.tipo_aprobacion=0 ";
			
				$sql6.=" group by DEP2.dependencia";
		}

				//echo $sql6;
				if(!mssql_query($sql6,$con)){
					echo "error en la consulta";
				}else{
					$resultado6=mssql_query($sql6,$con);
					$num_campos6=mssql_num_fields($resultado6);
					$num_registros6=mssql_num_rows($resultado6);
					$j6=$num_registros6-1;
					$jj6=-1;
						while($datos6=mssql_fetch_array($resultado6)){
								$jj6++;
								for($i6=0;$i6<$num_campos6;$i6++){
									$arreglo6[$jj6][$i6]=$datos6[$i6];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalIngresos=0;
				for($a6=0;$a6<$num_registros6;$a6++){
						$mmmm=$arreglo6[$a6][0];
						$ingresos[$mmmm]=$arreglo6[$a6][1];
						$TotalIngresos=$TotalIngresos+$arreglo6[$a6][1];
				}							

				//count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
				if($FechaFin==date('d/m/Y')){
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql7="select DEP2.dependencia AS UBICACION,
									count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=md.id_dependencia_destino
									and tup.codigo_dependencia>0
									and d.id_estado_documento<>9							
									and d.id_documento=md.id_documento
									";
						if($sector==2)//Industria
							$sql7.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and md.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}else{//Por Oficina a Cargo
						$sql7="select DEP2.dependencia AS RESPONSABLE,
									count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=tup.codigo_dependencia
									and d.id_estado_documento<>9							
									and d.id_documento=md.id_documento
									";
						if($sector==2)//Industria
							$sql7.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}
				
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql7="select DEP2.dependencia AS UBICACION,
								count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS																											  
							from LISTADOPENDIENTES_CONVENIIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=d.id_dependencia_destino
								and tup.codigo_dependencia>0
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql7.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and d.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}else{//Por oficina a cargo
						$sql7="select DEP2.dependencia AS RESPONSABLE,
								count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS																											  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql7.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}
				}
				
				if(!mssql_query($sql7,$con)){
					echo "error en la consulta";
				}else{
					$resultado7=mssql_query($sql7,$con);
					$num_campos7=mssql_num_fields($resultado7);
					$num_registros7=mssql_num_rows($resultado7);
					$j7=$num_registros7-1;
					$jj7=-1;
						while($datos7=mssql_fetch_array($resultado7)){
								$jj7++;
								for($i7=0;$i7<$num_campos7;$i7++){
									$arreglo7[$jj7][$i7]=$datos7[$i7];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalVencidos=0;
				for($a7=0;$a7<$num_registros7;$a7++){
					$xxxx=$arreglo7[$a7][0];
						$vencidos[$xxxx]=$arreglo7[$a7][1];
						$TotalVencidos=$TotalVencidos+$arreglo7[$a7][1];
				}		
				
				
				
				if($FechaFin==date('d/m/Y')){
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql4="select DEP2.dependencia AS UBICACION,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=md.id_dependencia_destino
									and tup.codigo_dependencia>0
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and md.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}else{
						$sql4="select DEP2.dependencia AS RESPONSABLE,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=tup.codigo_dependencia
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				
				
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql4="select DEP2.dependencia AS UBICACION,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=d.id_dependencia_destino
								and tup.codigo_dependencia>0
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and d.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}else{//Por oficina a cargo
						$sql4="select DEP2.dependencia AS RESPONSABLE,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					
					}
				}
				
				if(!mssql_query($sql4,$con)){
					echo "error en la consulta";
				}else{
					$resultado4=mssql_query($sql4,$con);
					$num_campos4=mssql_num_fields($resultado4);
					$num_registros4=mssql_num_rows($resultado4);
					$j4=$num_registros4-1;
					$jj4=-1;
						while($datos4=mssql_fetch_array($resultado4)){
								$jj4++;
								for($i4=0;$i4<$num_campos4;$i4++){
									$arreglo4[$jj4][$i4]=$datos4[$i4];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalPendientes=0;
				for($a4=0;$a4<$num_registros4;$a4++){
					$xxxx=$arreglo4[$a4][0];
						$pendientes[$xxxx]=$arreglo4[$a4][1];
						$TotalPendientes=$TotalPendientes+$arreglo4[$a4][1];
				}		
/********Situaci�n de expedientes******/
		if ($GrupoOpciones1==2){//Por ubicaci�n
				if($orden==1){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								order by 3 desc
							";
				}elseif($orden==2){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								order by 3 asc
							";
				}elseif($orden==3){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))							
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 desc
							";								
				}elseif($orden==4){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))							
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 ASc
							";

				}elseif($orden==5){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP,
								 db_general.dbo.tupa tup
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia
							and d.id_tup=tup.id_tupa
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia
									
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 desc
							";								
				}elseif($orden==6){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP,
								db_general.dbo.tupa tup
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia
							and d.id_tup=tup.id_tupa						
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO=2
							AND MD.DERIVADO=0 AND MD.FINALIZADO=0
							and md.id_dependencia_destino=dep.codigo_dependencia
									
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 ASc
							";
								
				}else{
					$sql3="select dep2.codigo_dependencia,dep2.dependencia
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
							and dep2.codigo_dependencia<>34
							and dep2.codigo_dependencia in (/*select id_dependencia_destino from movimiento_documento where derivado=0*/
								select md.id_dependencia_destino from movimiento_documento md,documento d where d.id_documento=md.id_documento and d.id_tipo_documento=2
							)";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" order by dep2.dependencia
							";
				}		
		}else{//Por Oficina a Cargo
				if($orden==1){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,COUNT(*)
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								AND D.ID_TIPO_DOCUMENTO=2
								AND MD.DERIVADO=0 AND MD.FINALIZADO=0
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.="GROUP BY dep2.codigo_dependencia,dep2.dependencia
								order by 3 desc
							";
				}elseif($orden==2){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,COUNT(*)
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								AND D.ID_TIPO_DOCUMENTO=2
								AND MD.DERIVADO=0 AND MD.FINALIZADO=0
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.="GROUP BY dep2.codigo_dependencia,dep2.dependencia
								order by 3 asc
							";
				}elseif($orden==3){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,COUNT(*)
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								aND D.ID_TIPO_DOCUMENTO=2
								aND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY dep2.codigo_dependencia,dep2.dependencia";
					
					$sql3.=" UNION 
							select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep2.codigo_dependencia
									from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
									db_general.dbo.tupa tup
									WHERE
									D.ID_DOCUMENTO=MD.ID_DOCUMENTO
									aND D.ID_TIPO_DOCUMENTO=2
									aND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
									and d.id_tup=tup.id_tupa
									and tup.codigo_dependencia=dep2.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)					
							";
							
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 desc
							";
				}elseif($orden==4){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,COUNT(*)
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								aND D.ID_TIPO_DOCUMENTO=2
								aND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY dep2.codigo_dependencia,dep2.dependencia";
					
					$sql3.=" UNION 
							select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep2.codigo_dependencia
									from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
									db_general.dbo.tupa tup
									WHERE
									D.ID_DOCUMENTO=MD.ID_DOCUMENTO
									aND D.ID_TIPO_DOCUMENTO=2
									aND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
									and d.id_tup=tup.id_tupa
									and tup.codigo_dependencia=dep2.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)					
							";
							
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 Asc
							";

				}elseif($orden==5){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								aND D.ID_TIPO_DOCUMENTO=2
								aND MD.DERIVADO=0 AND MD.FINALIZADO=0
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY dep2.codigo_dependencia,dep2.dependencia";
					
					$sql3.=" UNION 
							select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep2.codigo_dependencia
									from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
									db_general.dbo.tupa tup
									WHERE
									D.ID_DOCUMENTO=MD.ID_DOCUMENTO
									aND D.ID_TIPO_DOCUMENTO=2
									aND MD.DERIVADO=0 AND MD.FINALIZADO=0
									and d.id_tup=tup.id_tupa
									and tup.codigo_dependencia=dep2.codigo_dependencia
									
								)					
							";
							
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 desc
							";
				}elseif($orden==6){
					$sql3="SELECT dep2.codigo_dependencia,dep2.dependencia,count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
							from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
								db_general.dbo.tupa tup
							WHERE
								D.ID_DOCUMENTO=MD.ID_DOCUMENTO
								aND D.ID_TIPO_DOCUMENTO=2
								aND MD.DERIVADO=0 AND MD.FINALIZADO=0
								and d.id_tup=tup.id_tupa
								and tup.codigo_dependencia=dep2.codigo_dependencia
							
							";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY dep2.codigo_dependencia,dep2.dependencia";
					
					$sql3.=" UNION 
							select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep2.codigo_dependencia
									from DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP2,
									db_general.dbo.tupa tup
									WHERE
									D.ID_DOCUMENTO=MD.ID_DOCUMENTO
									aND D.ID_TIPO_DOCUMENTO=2
									aND MD.DERIVADO=0 AND MD.FINALIZADO=0
									and d.id_tup=tup.id_tupa
									and tup.codigo_dependencia=dep2.codigo_dependencia
									
								)					
							";
							
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 Asc
							";

				}else{
					$sql3="select dep2.codigo_dependencia,dep2.dependencia
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
							and dep2.codigo_dependencia<>34
							and dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)";
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" order by dep2.dependencia
							";
				}		
		}				
				//echo $sql3;			  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				
				for($a3=0;$a3<$num_registros3;$a3++){
					
					$nombreDependencia[]=$arreglo3[$a3][1];
					$codigoDependenciaEsp[]=$arreglo3[$a3][0];
				}	//fin del for($a=0...)
				
}//fin del if($this->conpara_fechas)				
  $this->abreConnDB();
   //$this->conn->debug = true;
  	 	 
		$sql_SP = "select convert(varchar,getDate(),103)";
  // echo $sql_SP;
  $rs = & $this->conn->Execute($sql_SP);
  unset($sql_SP);
  if (!$rs){
   print $this->conn->ErrorMsg();
  }else{
   		$fechaAnterior=$rs->fields[0];
   $rs->Close();
  }
  unset($rs);
				
				
				
					//$fecha=date('d/m/Y H:i');
					$desTitulo = "SITUACI�N DE LOS EXPEDIENTES CON SILENCIO POSITIVO Y NEGATIVO";
					
								$ceros=0;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;
if ($GrupoOpciones1==2){
	$textoDetalle="UBICACI�N";
}else{
	$textoDetalle="OFICINA A CARGO";
}
 
  $desMensaje="<table width=\"770\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr> 
    <td><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">
        <tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;&nbsp;&nbsp;</font></td>
        </tr>				
      </table></td>
  </tr>
  <tr> 
    <td bgcolor=\"#C6E8FF\"><table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">
		<tr bgcolor=\"#6496CB\"> 
          <td width=\"350\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$textoDetalle."</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes<br />Anterior</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=3 \" ><img src=\"/img/800x600/b_down.gif\" border=\"0\"></a>Ingresos<a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=4\" ><img src=\"/img/800x600/b_up.gif\" border=\"0\"></a></font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=5 \" ><img src=\"/img/800x600/b_down.gif\" border=\"0\"></a>Vencidos<a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=6\" ><img src=\"/img/800x600/b_up.gif\" border=\"0\"></a></font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=1 \" ><img src=\"/img/800x600/b_down.gif\" border=\"0\"></a>Pendientes<a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=2\" ><img src=\"/img/800x600/b_up.gif\" border=\"0\"></a></font></b></td>
        </tr>		";
		$TotalPendientesAnt=$TotalPendientes-$TotalIngresos+$TotalFinalizados;
		
        for($j=0;$j<$num_registros3;$j++){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;
				$desMensaje.="<tr><td align=\"left\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\">".$nombreDependencia[$j]."</td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesAnt[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$ingresos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$finalizados[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textored\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$vencidos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmListadoExpCurso&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."\" target=\"_blank\" style=\"text-decoration:underline \">".$pendientes[$nombreDependencia[$j]]."</a></font></strong></td>
							</tr>
				";		
						

		}

		$desMensaje.="<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">TOTAL</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesAnt."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalIngresos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalFinalizados."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textored\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalVencidos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientes."</font></strong></td>
        </tr>";		
		
        $desMensaje.="
      </table></td>
  </tr>
  <tr> 
    <td></td>
  </tr>
</table>
";

  $html->assign_by_ref('muestraStatusExp',$desMensaje);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  $html->display('controlExp/index2.tpl.php');
 } 

 function ListadoExpEnCurso(){
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$print,$GrupoOpciones1;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $a=($_POST['a']) ? $_POST['a'] : $_GET['a'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmListadoExpCurso';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
  $html->assign_by_ref('print',$print);
  $html->assign_by_ref('a',$a);
  $html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  //echo $sql_st;
  
  	if($FechaFin==date('d/m/Y')){
				$sql="select d.id_documento,tup.id_tupa,case when d.id_tipo_documento=1 then d.asunto
																  when d.id_tipo_documento=2 then tup.descripcion
																  when (d.id_tipo_documento=4 or d.id_tipo_documento=5) then d.asunto end,
						  case when d.id_tipo_documento=4 then dbo.buscaReferencia(md.id_movimiento_documento)
							   else d.num_tram_documentario end,
						  case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
							   when sol.id_tipo_persona=2 then sol.razon_social
							   when d.id_tipo_documento=5 then d.referencia 
							   else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103),
							convert(varchar,md.audit_mod,103),t.apellidos_trabajador+' '+t.nombres_trabajador,
							dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)),
							case when (d.id_tipo_documento in (1,2) and d.indicativo_oficio not in ('S/N','SN','S.N.')) then d.indicativo_oficio end,
							md.ultimo_avance,dep.siglas,tup.numero_dias,d.fecha_max_plazo,d.id_estado_documento,case when dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."')>=tup.numero_dias then 1 end
						from documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
									 left join db_general.dbo.persona sol on d.id_persona=sol.id,
								movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO',
								db_general.dbo.h_dependencia dep,db_general.dbo.h_dependencia dep2
						WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
								and dep.codigo_dependencia=md.id_dependencia_destino
								and d.id_tipo_documento=2
								and d.id_tup not in (65,66)
								and dep2.codigo_dependencia=tup.codigo_dependencia
								  ";
	}else{
	
				$sql_="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql_);
				unset($sql_);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}
					
				$sql="select d.ID_DOCUMENTO,d.TUPA,d.ASUNTO,d.referencia,
							 case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
							   	  when sol.id_tipo_persona=2 then sol.razon_social END,
							 convert(varchar,d.auditmod,103),
							 convert(varchar,d.audit_mod,103),'' as trabajador,
							 dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."'),
							 '' as indicativo,
							 '',dep.siglas,tup.numero_dias,'','' as estado,case when dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."')>=tup.numero_dias then 1 end
	
     				from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
						 DB_GENERAl.dbo.h_dependencia dep2,db_general.dbo.persona sol,
						 db_general.dbo.h_dependencia dep 
					
					where d.id_tipo_documento=2
							AND D.TUPA=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND D.FECHA='".$fechaPosterior."'
							AND d.razon_social=sol.id
							and d.id_dependencia_destino=dep.codigo_dependencia";
	}
	
			if($GrupoOpciones1==2){
				if($sector==2)//Industria
					$sql.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
			}else{
				if($sector==2)//Industria
					$sql.=" and tup.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql.=" and tup.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql.=" and tup.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
			}				
								  
				if($tipAprobacion==4)
					$sql.=" and tup.tipo_aprobacion=1";
				elseif($tipAprobacion==5)
					$sql.=" and tup.tipo_aprobacion=0";
				elseif($tipAprobacion==2)
					$sql.=" and tup.tipo_aprobacion=2";
				elseif($tipAprobacion==3)
					$sql.=" and tup.tipo_aprobacion is null";
			if($GrupoOpciones1==2){
				if($dependenciaCargo && $dependenciaCargo!="")
					$sql.=" and dep.dependencia='".$dependenciaCargo."' ";
			}else{		
				if($dependenciaCargo && $dependenciaCargo!="")
					$sql.=" and dep2.dependencia='".$dependenciaCargo."' ";
			}		
					
					
				if($a==1){
					$sql.=" order by 5 desc";
				}elseif($a==2){
					$sql.=" order by 5 asc";
				}elseif($a==3){
					$sql.=" order by 9 desc";
				}elseif($a==4){
					$sql.=" order by 9 asc";
				}elseif($a==5){
					$sql.=" order by 12 desc";
				}elseif($a==6){
					$sql.=" order by 12 asc";
				}elseif($a==7){
					$sql.=" order by 16 desc";
				}elseif($a==8){
					$sql.=" order by 16 asc";
				}else{
					$sql.=" order by 9 desc";
				}
					
				//$sql.=" order by 9 desc";
  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow()){
						$difDias2=$row[8]-$row[12];//DiasUtiles - DiasTupa
						if($difDias2>=0)
							$estado2="VENCIDO";
						else
							$estado2="DENTRO DEL PLAZO";
							
						$html->append('list',array('id' => $row[0],
												   'tu' => $row[1],
												   'tup' => $this->formateaTexto($row[2]),
												   'nroTD' => $row[3],
												   'razSoc' => $row[4],
												   'fecIngP' => $row[5],
												   'fecIngD' => $row[6],
												   'trab' => $row[7],
												   'diasUtiles' => $row[8],
												   'ind' => $row[9],
												   'comentario' => $row[10],
												   'ubicacionDepe' => $row[11],
												   'diasTupa' => $row[12],
												   'fecPlazo' => $row[13],
												   'idEstadoDoc' => $row[14],
												   'estado'=> $estado2
												   ));							
				}//fin del while
			}  

 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));  
  
	if($print==3){
				// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('controlExp/viewExtendListadoExpEnCurso.tpl.php');
		exit;
	}elseif($print==2){
		//setlocale(LC_TIME,"spanish");
		//$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/pie-logo.gif';		
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'hoja1'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('controlExp/ShowListadoExpEnCursoPdf.tpl.php'),true,$logo);
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;		
	}else{  
  		($print==1) ? $html->display('controlExp/printListadoExpEnCurso.tpl.php') : $html->display('controlExp/ListadoExpEnCurso.tpl.php');
  	}
 }

 function GraficoExpEnCurso(){
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$GrupoOpciones1;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmGraficoExpEnCurso';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
   $html->assign_by_ref('print',$print);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  			if($FechaFin==date('d/m/Y')){
				if ($GrupoOpciones1==2){//Por ubicaci�n
					$sql="select DEP.siglas AS UBICACION,
								count(*)  AS PENDIENTES
								from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
									 DB_GENERAl.dbo.h_dependencia dep2,
									 DB_GENERAl.dbo.h_dependencia dep 
								where md.derivado=0 and md.finalizado=0
								and d.id_tipo_documento=2
								AND D.ID_TUP=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
								and dep.codigo_dependencia=MD.ID_DEPENDENCIA_DESTINO
								and d.id_documento=md.id_documento
								";
					if($sector==2)//Industria
						$sql.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
						
					if($tipProceso==2)
						$sql.=" and tup.id_clase_tupa=1 ";
					elseif($tipProceso==3)
						$sql.=" and tup.id_clase_tupa=2 ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep.codigo_dependencia=".$dependenciaCargo;
						
					if($tipAprobacion==2)
						$sql.=" and tup.tipo_aprobacion=2 ";
					elseif($tipAprobacion==3)
						$sql.=" and tup.tipo_aprobacion is null ";
					elseif($tipAprobacion==4)
						$sql.=" and tup.tipo_aprobacion=1 ";
					elseif($tipAprobacion==5)
						$sql.=" and tup.tipo_aprobacion=0 ";
				
					$sql.=" group by DEP.siglas";
				}else{//Por Oficina A cargo
					$sql="select DEP2.siglas AS RESPONSABLE,
								count(*)  AS PENDIENTES
								from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
									 DB_GENERAl.dbo.h_dependencia dep2 
								where md.derivado=0 and md.finalizado=0
								and d.id_tipo_documento=2
								AND D.ID_TUP=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
														
								and d.id_documento=md.id_documento
								";
					if($sector==2)//Industria
						$sql.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
						
					if($tipProceso==2)
						$sql.=" and tup.id_clase_tupa=1 ";
					elseif($tipProceso==3)
						$sql.=" and tup.id_clase_tupa=2 ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep2.codigo_dependencia=".$dependenciaCargo;
						
					if($tipAprobacion==2)
						$sql.=" and tup.tipo_aprobacion=2 ";
					elseif($tipAprobacion==3)
						$sql.=" and tup.tipo_aprobacion is null ";
					elseif($tipAprobacion==4)
						$sql.=" and tup.tipo_aprobacion=1 ";
					elseif($tipAprobacion==5)
						$sql.=" and tup.tipo_aprobacion=0 ";
				
					$sql.=" group by DEP2.siglas";
				}	
			}else{
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				
				if ($GrupoOpciones1==2){//Por ubicaci�n
					$sql="select DEP.dependencia AS RESPONSABLE,
							COUNT(*) AS PENDIENTES																												  
						from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
							DB_GENERAl.dbo.h_dependencia dep2,
							DB_GENERAl.dbo.h_dependencia dep 
						where d.id_tipo_documento=2
							AND D.TUPA=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							and dep.codigo_dependencia=d.id_dependencia_destino
							AND D.FECHA='".$fechaPosterior."'";
							
					if($sector==2)//Industria
						$sql.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
						
					if($tipProceso==2)
						$sql.=" and tup.id_clase_tupa=1 ";
					elseif($tipProceso==3)
						$sql.=" and tup.id_clase_tupa=2 ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep.codigo_dependencia=".$dependenciaCargo;
						
					if($tipAprobacion==2)
						$sql.=" and tup.tipo_aprobacion=2 ";
					elseif($tipAprobacion==3)
						$sql.=" and tup.tipo_aprobacion is null ";
					elseif($tipAprobacion==4)
						$sql.=" and tup.tipo_aprobacion=1 ";
					elseif($tipAprobacion==5)
						$sql.=" and tup.tipo_aprobacion=0 ";
				
					$sql.=" group by DEP.dependencia";	
				}else{
					$sql="select DEP2.dependencia AS RESPONSABLE,
							COUNT(*) AS PENDIENTES																												  
						from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
							DB_GENERAl.dbo.h_dependencia dep2 
						where d.id_tipo_documento=2
							AND D.TUPA=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							AND D.FECHA='".$fechaPosterior."'";
							
					if($sector==2)//Industria
						$sql.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
						
					if($tipProceso==2)
						$sql.=" and tup.id_clase_tupa=1 ";
					elseif($tipProceso==3)
						$sql.=" and tup.id_clase_tupa=2 ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep2.codigo_dependencia=".$dependenciaCargo;
						
					if($tipAprobacion==2)
						$sql.=" and tup.tipo_aprobacion=2 ";
					elseif($tipAprobacion==3)
						$sql.=" and tup.tipo_aprobacion is null ";
					elseif($tipAprobacion==4)
						$sql.=" and tup.tipo_aprobacion=1 ";
					elseif($tipAprobacion==5)
						$sql.=" and tup.tipo_aprobacion=0 ";
				
					$sql.=" group by DEP2.dependencia";	
				}			
			}		  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow()){
						//$difDias2=$row[8]-$row[12];//DiasUtiles - DiasTupa
						$dependencia_[]=$row[0];
						$nroPendientes_[]=$row[1];
				}//fin del while
			}  

		/*Estos son los datos para la generaci�n del pie*/
		$PG = new PowerGraphic;
		
		$PG->title     = 'Distribuci�n de Pendientes por dependencia';
		//$PG->axis_x    = 'Mes';
		$PG->axis_x    = 'Dependencia';
		$PG->axis_y    = 'Nro. Expedientes';
		$PG->graphic_1 = 'Year 2004';
		$PG->graphic_2 = 'Year 2003';
		$PG->type      = 1;
		$PG->skin      = 1;
		$PG->credits   = 0;
		
		for($i=0;$i<count($dependencia_);$i++){
			$PG->x[$i] = $dependencia_[$i];
			$PG->y[$i] = $nroPendientes_[$i];
		}
		
		// Set values
		/*$PG->x[0] = 'Ene';
		$PG->y[0] = 35000;
		
		
		$PG->x[1] = 'Feb';
		$PG->y[1] = 38500;
		
		$PG->x[2] = 'Mar';
		$PG->y[2] = 40800;
		
		$PG->x[3] = 'Abr';
		$PG->y[3] = 45200;
		//$PG->y[3] = $contador4;		
		
		$PG->x[4] = 'May';
		$PG->y[4] = 46800;
		//$PG->y[4] = $contador5;		
		
		$PG->x[5] = 'Jun';
		$PG->y[5] = 55000;
		//$PG->y[5] = $contador6;*/
		/*$PG->x[6] = 'Jul';
		$PG->y[6] = $contador7;
		$PG->x[7] = 'Ago';
		$PG->y[7] = $contador8;
		$PG->x[8] = 'Sep';
		$PG->y[8] = $contador9;
		$PG->x[9] = 'Oct';
		$PG->y[9] = $contador10;
		$PG->x[10] = 'Nov';
		$PG->y[10] = $contador11;
		$PG->x[11] = 'Dic';
		$PG->y[11] = $contador12;				*/

//$matrix=$PG->create_query_string();
//echo "matrix is here".$matrix."matrix is here";exit;			
//$m='class.graphic.php?' . $PG->create_query_string() . '';
//$m.='<span style=\"font-size: 17px;\">&#8226; </span> <br /><br />';
$m.='<span style=\"font-size: 17px;\"></span><!-- <br /><br />-->';
/*$m.='1. Barras Verticales: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="imagen1" /><br /><br />';*/

// Changing the type

$PG->type = 2;
/*$m.='2. Barras Horizontales: <br /><br />';*/
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';

/*$PG->type = 3;
$m.='3. Puntos: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';*/

/*$PG->type = 4;
$m.='4. L�neas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';
*/
/*$PG->type = 5;
//$m.='5. Pie: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';

$PG->type = 6;
$m.='6. Donas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';
*/
// Clear parameters
$PG->reset_values();
		$html->assign_by_ref('m',$m);

 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  ($print==1) ? $html->display('controlExp/printGraficoExpEnCurso.tpl.php') : $html->display('controlExp/GraficoExpEnCurso.tpl.php');
 }
 
 
 function ControlExp2($page=NULL,$tipReporte=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroExp=NULL,$anyo=NULL,$codigo=NULL,$fecInicio=NULL,$fecFin=NULL,$tipCorrespondencia=NULL,$nroCor=NULL,$anyo2=NULL,$fecInicio2=NULL,$fecFin2=NULL,$tipBusqueda2=NULL,$codigo2=NULL){
  global $Opciones1,$filetip,$filetip2,$filetip3,$filetip4,$usuario,$code_val,$code;
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$print,$GrupoOpciones1;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmControlExp2';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  $html->assign_by_ref('tipReporte',$tipReporte);
  $html->assign_by_ref('tipDocumento',$tipDocumento);
  $html->assign_by_ref('tipBusqueda',$tipBusqueda);
  $html->assign_by_ref('nroExp',$nroExp);
  $html->assign_by_ref('anyo',$anyo);
  $html->assign_by_ref('codigo',$codigo);
  $html->assign_by_ref('tipCorrespondencia',$tipCorrespondencia);
  $html->assign_by_ref('nroCor',$nroCor);
  $html->assign_by_ref('anyo2',$anyo2);
  $html->assign_by_ref('tipBusqueda2',$tipBusqueda2);
  $html->assign_by_ref('codigo2',$codigo2);
  $html->assign_by_ref('Opciones1',$Opciones1);
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('filetip',$filetip);
  $html->assign_by_ref('filetip2',$filetip2);
  $html->assign_by_ref('filetip3',$filetip3);
  $html->assign_by_ref('filetip4',$filetip4);
  $html->assign_by_ref('usuario',$usuario);
  $html->assign_by_ref('code_val',$code_val);
  $html->assign_by_ref('code',$code);
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('print',$print);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  /*// Contenido Select Congresista
  $sql_st = "SELECT id, substring(razon_social,1,70) ".
        "FROM db_general.dbo.persona ".
        "where id_tipo_persona=2 ".
        "ORDER BY 2";
  $html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);*/
  
  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  // Fecha Ingreso
  $html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio,6,4), true));

  // Fecha Salida
  $html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true));

  // Fecha Ingreson de la Correspodencia
  $html->assign_by_ref('selMesIng2',$this->ObjFrmMes(1, 12, true, substr($fecInicio2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng2',$this->ObjFrmDia(1, 31, substr($fecInicio2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio2,6,4), true));

  // Fecha Salida de la Correspodencia
  $html->assign_by_ref('selMesSal2',$this->ObjFrmMes(1, 12, true, substr($fecFin2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal2',$this->ObjFrmDia(1, 31, substr($fecFin2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin2,6,4), true));
 
 
 /**/
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}
 
/*lISTADO DE LOS DOCUMENTOS FINALIZADOS*/

		if ($GrupoOpciones1==2){//Por ubicaci�n
				$sql5="select DEP2.dependencia AS UBICACION, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP, 
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento=2 AND D.ID_TUP=TUP.ID_TUPA 
						and dep2.codigo_dependencia=md.id_dependencia_destino
						and tup.codigo_dependencia>0
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipProceso==2)
					$sql5.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql5.=" and tup.id_clase_tupa=2 ";
					
				if($dependenciaCargo>0)
					$sql5.=" and md.id_dependencia_destino=".$dependenciaCargo;
										
				if($tipAprobacion==2)
					$sql5.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql5.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql5.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql5.=" and tup.tipo_aprobacion=0 ";
					
				$sql5.=" group by DEP2.dependencia";
		}else{//Por Oficina a cargo
				$sql5="select DEP2.dependencia AS RESPONSABLE, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP, 
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento=2 AND D.ID_TUP=TUP.ID_TUPA 
						and dep2.codigo_dependencia=tup.codigo_dependencia 
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($tipProceso==2)
					$sql5.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql5.=" and tup.id_clase_tupa=2 ";
					
				if($dependenciaCargo>0)
					$sql5.=" and dep2.codigo_dependencia=".$dependenciaCargo;
										
				if($tipAprobacion==2)
					$sql5.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql5.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql5.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql5.=" and tup.tipo_aprobacion=0 ";
					
				$sql5.=" group by DEP2.dependencia";
				
		}//fin del if ($GrupoOpciones1==2)				
				//echo $sql5;			
				if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado5=mssql_query($sql5,$con);
					$num_campos5=mssql_num_fields($resultado5);
					$num_registros5=mssql_num_rows($resultado5);
					$j5=$num_registros5-1;
					$jj5=-1;
						while($datos5=mssql_fetch_array($resultado5)){
								$jj5++;
								for($i5=0;$i5<$num_campos5;$i5++){
									$arreglo5[$jj5][$i5]=$datos5[$i5];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalFinalizados=0;
				for($a5=0;$a5<$num_registros5;$a5++){
						$yyyy=$arreglo5[$a5][0];
						$finalizados[$yyyy]=$arreglo5[$a5][1];
						$TotalFinalizados=$TotalFinalizados+$arreglo5[$a5][1];
				}
				//echo $TotalFinalizados;
				//echo "<br>".$sql5;exit;
				
		if ($GrupoOpciones1==2){//Por ubicaci�n
				$sql6="select DEP2.dependencia AS UBICACION,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=md.id_dependencia_destino
							and tup.codigo_dependencia>0
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql6.=" and md.id_dependencia_destino=".$dependenciaCargo;	
					
				if($tipProceso==2)
					$sql6.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql6.=" and tup.id_clase_tupa=2 ";
					
				if($tipAprobacion==2)
					$sql6.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql6.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql6.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql6.=" and tup.tipo_aprobacion=0 ";
			
				$sql6.=" group by DEP2.dependencia";
		}else{	//Por Oficina a cargo					
				$sql6="select DEP2.dependencia AS RESPONSABLE,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento=2
							
							AND D.ID_TUP=TUP.ID_TUPA
							and dep2.codigo_dependencia=tup.codigo_dependencia
							
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql6.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
					
				if($tipProceso==2)
					$sql6.=" and tup.id_clase_tupa=1 ";
				elseif($tipProceso==3)
					$sql6.=" and tup.id_clase_tupa=2 ";
					
				if($tipAprobacion==2)
					$sql6.=" and tup.tipo_aprobacion=2 ";
				elseif($tipAprobacion==3)
					$sql6.=" and tup.tipo_aprobacion is null ";
				elseif($tipAprobacion==4)
					$sql6.=" and tup.tipo_aprobacion=1 ";
				elseif($tipAprobacion==5)
					$sql6.=" and tup.tipo_aprobacion=0 ";
			
				$sql6.=" group by DEP2.dependencia";
		}
				//echo $sql6;
				if(!mssql_query($sql6,$con)){
					echo "error en la consulta";
				}else{
					$resultado6=mssql_query($sql6,$con);
					$num_campos6=mssql_num_fields($resultado6);
					$num_registros6=mssql_num_rows($resultado6);
					$j6=$num_registros6-1;
					$jj6=-1;
						while($datos6=mssql_fetch_array($resultado6)){
								$jj6++;
								for($i6=0;$i6<$num_campos6;$i6++){
									$arreglo6[$jj6][$i6]=$datos6[$i6];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalIngresos=0;
				for($a6=0;$a6<$num_registros6;$a6++){
						$mmmm=$arreglo6[$a6][0];
						$ingresos[$mmmm]=$arreglo6[$a6][1];
						$TotalIngresos=$TotalIngresos+$arreglo6[$a6][1];
				}							

				//count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
				if($FechaFin==date('d/m/Y')){
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql7="select DEP2.dependencia AS UBICACION,
									count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=md.id_dependencia_destino
									and tup.codigo_dependencia>0
									and d.id_estado_documento<>9							
									and d.id_documento=md.id_documento
									";
						if($sector==2)//Industria
							$sql7.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and md.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}else{//Por Oficina a Cargo
						$sql7="select DEP2.dependencia AS RESPONSABLE,
									count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=tup.codigo_dependencia
									and d.id_estado_documento<>9							
									and d.id_documento=md.id_documento
									";
						if($sector==2)//Industria
							$sql7.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}
				
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql7="select DEP2.dependencia AS UBICACION,
								count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS																											  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=d.id_dependencia_destino
								and tup.codigo_dependencia>0
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql7.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and d.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}else{//Por oficina a cargo
						$sql7="select DEP2.dependencia AS RESPONSABLE,
								count(CASE WHEN (dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias>=0) THEN 1 END)  AS VENCIDOS																											  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql7.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql7.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql7.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql7.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql7.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql7.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql7.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql7.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql7.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql7.=" and tup.tipo_aprobacion=0 ";
					
						$sql7.=" group by DEP2.dependencia";
					}
				}
				
				if(!mssql_query($sql7,$con)){
					echo "error en la consulta";
				}else{
					$resultado7=mssql_query($sql7,$con);
					$num_campos7=mssql_num_fields($resultado7);
					$num_registros7=mssql_num_rows($resultado7);
					$j7=$num_registros7-1;
					$jj7=-1;
						while($datos7=mssql_fetch_array($resultado7)){
								$jj7++;
								for($i7=0;$i7<$num_campos7;$i7++){
									$arreglo7[$jj7][$i7]=$datos7[$i7];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalVencidos=0;
				for($a7=0;$a7<$num_registros7;$a7++){
					$xxxx=$arreglo7[$a7][0];
						$vencidos[$xxxx]=$arreglo7[$a7][1];
						$TotalVencidos=$TotalVencidos+$arreglo7[$a7][1];
				}		
				
				
				
				if($FechaFin==date('d/m/Y')){
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql4="select DEP2.dependencia AS UBICACION,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=md.id_dependencia_destino
									and tup.codigo_dependencia>0
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and md.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}else{
						$sql4="select DEP2.dependencia AS RESPONSABLE,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento=2
									AND D.ID_TUP=TUP.ID_TUPA
									and dep2.codigo_dependencia=tup.codigo_dependencia
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				
				
					if ($GrupoOpciones1==2){//Por ubicaci�n
						$sql4="select DEP2.dependencia AS UBICACION,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=d.id_dependencia_destino
								and tup.codigo_dependencia>0
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and d.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					}else{//Por oficina a cargo
						$sql4="select DEP2.dependencia AS RESPONSABLE,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,DB_GENERAL.DBO.TUPA TUP,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento=2
								AND D.TUPA=TUP.ID_TUPA
								and dep2.codigo_dependencia=tup.codigo_dependencia
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($tipProceso==2)
							$sql4.=" and tup.id_clase_tupa=1 ";
						elseif($tipProceso==3)
							$sql4.=" and tup.id_clase_tupa=2 ";
							
						if($dependenciaCargo>0)
							$sql4.=" and dep2.codigo_dependencia=".$dependenciaCargo;
							
						if($tipAprobacion==2)
							$sql4.=" and tup.tipo_aprobacion=2 ";
						elseif($tipAprobacion==3)
							$sql4.=" and tup.tipo_aprobacion is null ";
						elseif($tipAprobacion==4)
							$sql4.=" and tup.tipo_aprobacion=1 ";
						elseif($tipAprobacion==5)
							$sql4.=" and tup.tipo_aprobacion=0 ";
					
						$sql4.=" group by DEP2.dependencia";
					
					}
				}
				
				if(!mssql_query($sql4,$con)){
					echo "error en la consulta";
				}else{
					$resultado4=mssql_query($sql4,$con);
					$num_campos4=mssql_num_fields($resultado4);
					$num_registros4=mssql_num_rows($resultado4);
					$j4=$num_registros4-1;
					$jj4=-1;
						while($datos4=mssql_fetch_array($resultado4)){
								$jj4++;
								for($i4=0;$i4<$num_campos4;$i4++){
									$arreglo4[$jj4][$i4]=$datos4[$i4];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalPendientes=0;
				for($a4=0;$a4<$num_registros4;$a4++){
					$xxxx=$arreglo4[$a4][0];
						$pendientes[$xxxx]=$arreglo4[$a4][1];
						$TotalPendientes=$TotalPendientes+$arreglo4[$a4][1];
				}		
/********Situaci�n de expedientes******/
		if ($GrupoOpciones1==2){//Por ubicaci�n
				$sql3="select dep2.codigo_dependencia,dep2.dependencia
						from db_general.dbo.h_dependencia dep2
						where dep2.condicion='ACTIVO'
						and dep2.codigo_dependencia<>34
						and dep2.codigo_dependencia in (select id_dependencia_destino from movimiento_documento where derivado=0)";
				if($sector==2)//Industria
					$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
				
				if($dependenciaCargo>0)
					$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" order by dep2.dependencia
						";
		}else{
				$sql3="select dep2.codigo_dependencia,dep2.dependencia
						from db_general.dbo.h_dependencia dep2
						where dep2.condicion='ACTIVO'
						and dep2.codigo_dependencia<>34
						and dep2.codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa)";
				if($sector==2)//Industria
					$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
				
				if($dependenciaCargo>0)
					$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" order by dep2.dependencia
						";
		}				
				//echo $sql3;			  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				
				for($a3=0;$a3<$num_registros3;$a3++){
					
					$nombreDependencia[]=$arreglo3[$a3][1];
					$codigoDependenciaEsp[]=$arreglo3[$a3][0];
				}	//fin del for($a=0...)
				
				
  $this->abreConnDB();
   //$this->conn->debug = true;
  	 	 
		$sql_SP = "select convert(varchar,getDate(),103)";
  // echo $sql_SP;
  $rs = & $this->conn->Execute($sql_SP);
  unset($sql_SP);
  if (!$rs){
   print $this->conn->ErrorMsg();
  }else{
   		$fechaAnterior=$rs->fields[0];
   $rs->Close();
  }
  unset($rs);
				
				
				
					//$fecha=date('d/m/Y H:i');
					$desTitulo = "SITUACI�N DE LOS EXPEDIENTES CON SILENCIO POSITIVO Y NEGATIVO";
					
								$ceros=0;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;

 
  $desMensaje="<table width=\"770\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr> 
    <td><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">
        <tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;&nbsp;&nbsp;</font></td>
        </tr>				
      </table></td>
  </tr>
  <tr> 
    <td bgcolor=\"#C6E8FF\"><table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">
		<tr bgcolor=\"#6496CB\"> 
          <td width=\"350\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">OFICINA A CARGO</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes<br />Anterior</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Ingresos</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Vencidos</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes</font></b></td>
        </tr>		";
		$TotalPendientesAnt=$TotalPendientes-$TotalIngresos+$TotalFinalizados;
		
        for($j=0;$j<$num_registros3;$j++){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;
				$desMensaje.="<tr><td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$nombreDependencia[$j]."</font></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesAnt[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$ingresos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$finalizados[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textored\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$vencidos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmListadoExpCurso&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."\" target=\"_blank\">".$pendientes[$nombreDependencia[$j]]."</a></font></strong></td>
							</tr>
				";		
						

		}

		$desMensaje.="<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">TOTAL</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesAnt."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalIngresos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalFinalizados."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textored\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalVencidos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientes."</font></strong></td>
        </tr>";		
		
        $desMensaje.="
      </table></td>
  </tr>
  <tr> 
    <td></td>
  </tr>
</table>
";

  $html->assign_by_ref('muestraStatusExp',$desMensaje);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  
  if($print==3){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;  
  	for($j=0;$j<$num_registros3;$j++){
						$html->append('list',array('nombreDependencia' => $nombreDependencia[$j],
												   'pendientesAnt' => $pendientesAnt[$nombreDependencia[$j]],
												   'ingresos' => $ingresos[$nombreDependencia[$j]],
												   'finalizados' => $finalizados[$nombreDependencia[$j]],
												   'vencidos' => $vencidos[$nombreDependencia[$j]],
												   'pendientes' => $pendientes[$nombreDependencia[$j]]
												   ));  
  	}
	
	$html->assign_by_ref('TotalPendientesAnt',$TotalPendientesAnt);
	$html->assign_by_ref('TotalIngresos',$TotalIngresos);
	$html->assign_by_ref('TotalFinalizados',$TotalFinalizados);
	$html->assign_by_ref('TotalVencidos',$TotalVencidos);
	$html->assign_by_ref('TotalPendientes',$TotalPendientes);
	
		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('controlExp/viewExtendEstadoTramites.tpl.php');
		exit;	
  }
  
/**/

 
 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  ($print==1) ? $html->display('controlExp/printIndex2.tpl.php') : $html->display('controlExp/index2.tpl.php');
 } 
 
 
 function ControlExtInt($page=NULL,$tipReporte=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroExp=NULL,$anyo=NULL,$codigo=NULL,$fecInicio=NULL,$fecFin=NULL,$tipCorrespondencia=NULL,$nroCor=NULL,$anyo2=NULL,$fecInicio2=NULL,$fecFin2=NULL,$tipBusqueda2=NULL,$codigo2=NULL){
  global $Opciones1,$filetip,$filetip2,$filetip3,$filetip4,$usuario,$code_val,$code;
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$print/*,$GrupoOpciones1*/,$orden;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  //$GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $orden=($_POST['orden']) ? $_POST['orden'] : $_GET['orden'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmControlExtInt';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  $html->assign_by_ref('tipReporte',$tipReporte);
  $html->assign_by_ref('tipDocumento',$tipDocumento);
  $html->assign_by_ref('tipBusqueda',$tipBusqueda);
  $html->assign_by_ref('nroExp',$nroExp);
  $html->assign_by_ref('anyo',$anyo);
  $html->assign_by_ref('codigo',$codigo);
  $html->assign_by_ref('tipCorrespondencia',$tipCorrespondencia);
  $html->assign_by_ref('nroCor',$nroCor);
  $html->assign_by_ref('anyo2',$anyo2);
  $html->assign_by_ref('tipBusqueda2',$tipBusqueda2);
  $html->assign_by_ref('codigo2',$codigo2);
  $html->assign_by_ref('Opciones1',$Opciones1);
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('filetip',$filetip);
  $html->assign_by_ref('filetip2',$filetip2);
  $html->assign_by_ref('filetip3',$filetip3);
  $html->assign_by_ref('filetip4',$filetip4);
  $html->assign_by_ref('usuario',$usuario);
  $html->assign_by_ref('code_val',$code_val);
  $html->assign_by_ref('code',$code);
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  //$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
  $html->assign_by_ref('orden',$orden); 
  $html->assign_by_ref('print',$print);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  /*// Contenido Select Congresista
  $sql_st = "SELECT id, substring(razon_social,1,70) ".
        "FROM db_general.dbo.persona ".
        "where id_tipo_persona=2 ".
        "ORDER BY 2";
  $html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);*/
  
  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where /*codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and*/ codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  // Fecha Ingreso
  $html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio,6,4), true));

  // Fecha Salida
  $html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true));

  // Fecha Ingreson de la Correspodencia
  $html->assign_by_ref('selMesIng2',$this->ObjFrmMes(1, 12, true, substr($fecInicio2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng2',$this->ObjFrmDia(1, 31, substr($fecInicio2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio2,6,4), true));

  // Fecha Salida de la Correspodencia
  $html->assign_by_ref('selMesSal2',$this->ObjFrmMes(1, 12, true, substr($fecFin2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal2',$this->ObjFrmDia(1, 31, substr($fecFin2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin2,6,4), true));
 
if($this->compara_fechas($FechaIni,$FechaFin)>0){
	echo "Criterio incorrecto de fechas";
}else{ 
 
 /**/
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}
 
/*lISTADO DE LOS DOCUMENTOS FINALIZADOS*/
				$sql5="select DEP2.dependencia AS UBICACION, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento in (1,4) 
						and dep2.codigo_dependencia=md.id_dependencia_destino
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and md.id_dependencia_destino in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
					//$sql5.=" and md.id_dependencia_destino in (5,8,9,10,12) ";
					
					
				if($dependenciaCargo>0)
					$sql5.=" and md.id_dependencia_destino=".$dependenciaCargo;
										
					
				$sql5.=" group by DEP2.dependencia";
				//echo $sql5;			
				if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado5=mssql_query($sql5,$con);
					$num_campos5=mssql_num_fields($resultado5);
					$num_registros5=mssql_num_rows($resultado5);
					$j5=$num_registros5-1;
					$jj5=-1;
						while($datos5=mssql_fetch_array($resultado5)){
								$jj5++;
								for($i5=0;$i5<$num_campos5;$i5++){
									$arreglo5[$jj5][$i5]=$datos5[$i5];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalFinalizados=0;
				for($a5=0;$a5<$num_registros5;$a5++){
						$yyyy=$arreglo5[$a5][0];
						$finalizados[$yyyy]=$arreglo5[$a5][1];
						$TotalFinalizados=$TotalFinalizados+$arreglo5[$a5][1];
				}
				//echo $TotalFinalizados;
				//echo "<br>".$sql5;exit;
				
				$sql6="select DEP2.dependencia AS UBICACION,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento in (1,4)
							
							and dep2.codigo_dependencia=md.id_dependencia_destino
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and md.id_dependencia_destino in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
				if($dependenciaCargo>0)
					$sql6.=" and md.id_dependencia_destino=".$dependenciaCargo;	
					
			
				$sql6.=" group by DEP2.dependencia";

				//echo $sql6;
				if(!mssql_query($sql6,$con)){
					echo "error en la consulta";
				}else{
					$resultado6=mssql_query($sql6,$con);
					$num_campos6=mssql_num_fields($resultado6);
					$num_registros6=mssql_num_rows($resultado6);
					$j6=$num_registros6-1;
					$jj6=-1;
						while($datos6=mssql_fetch_array($resultado6)){
								$jj6++;
								for($i6=0;$i6<$num_campos6;$i6++){
									$arreglo6[$jj6][$i6]=$datos6[$i6];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalIngresos=0;
				for($a6=0;$a6<$num_registros6;$a6++){
						$mmmm=$arreglo6[$a6][0];
						$ingresos[$mmmm]=$arreglo6[$a6][1];
						$TotalIngresos=$TotalIngresos+$arreglo6[$a6][1];
				}							

				
				
				if($FechaFin==date('d/m/Y')){
						$sql4="select DEP2.dependencia AS UBICACION,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento in (1,4)
									and dep2.codigo_dependencia=md.id_dependencia_destino
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and md.id_dependencia_destino in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
											
						if($dependenciaCargo>0)
							$sql4.=" and md.id_dependencia_destino=".$dependenciaCargo;
												
						$sql4.=" group by DEP2.dependencia";
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				
				
						$sql4="select DEP2.dependencia AS UBICACION,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento in (1,4)
								and dep2.codigo_dependencia=d.id_dependencia_destino
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and d.id_dependencia_destino in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
							
						if($dependenciaCargo>0)
							$sql4.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						$sql4.=" group by DEP2.dependencia";
				}
				
				if(!mssql_query($sql4,$con)){
					echo "error en la consulta";
				}else{
					$resultado4=mssql_query($sql4,$con);
					$num_campos4=mssql_num_fields($resultado4);
					$num_registros4=mssql_num_rows($resultado4);
					$j4=$num_registros4-1;
					$jj4=-1;
						while($datos4=mssql_fetch_array($resultado4)){
								$jj4++;
								for($i4=0;$i4<$num_campos4;$i4++){
									$arreglo4[$jj4][$i4]=$datos4[$i4];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalPendientes=0;
				for($a4=0;$a4<$num_registros4;$a4++){
					$xxxx=$arreglo4[$a4][0];
						$pendientes[$xxxx]=$arreglo4[$a4][1];
						$TotalPendientes=$TotalPendientes+$arreglo4[$a4][1];
				}		
/********Situaci�n de expedientes******/
			if($orden==1){
				$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
						FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
						WHERE
						D.ID_DOCUMENTO=MD.ID_DOCUMENTO
						AND D.ID_TIPO_DOCUMENTO in (1,4)
						AND MD.DERIVADO=0 AND MD.FINALIZADO=0
						and md.id_dependencia_destino=dep.codigo_dependencia";
				if($sector==2)//Industria
					$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
				
				if($dependenciaCargo>0)
					$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
						order by 3 desc
						";
			}elseif($orden==2){
				$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
						FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
						WHERE
						D.ID_DOCUMENTO=MD.ID_DOCUMENTO
						AND D.ID_TIPO_DOCUMENTO in (1,4)
						AND MD.DERIVADO=0 AND MD.FINALIZADO=0
						and md.id_dependencia_destino=dep.codigo_dependencia";
				if($sector==2)//Industria
					$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
				
				if($dependenciaCargo>0)
					$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
						order by 3 asc
						";
				}elseif($orden==3){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO in (1,4)
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))							
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select id_dependencia_destino from movimiento_documento where derivado=0)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO in (1,4)
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 desc
							";								
				}elseif($orden==4){
					$sql3="SELECT md.id_dependencia_destino,dep.dependencia,COUNT(*)
							FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO in (1,4)
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))							
							";
					if($sector==2)//Industria
						$sql3.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
					if($dependenciaCargo>0)
						$sql3.=" and dep.codigo_dependencia=".$dependenciaCargo;	
												
					$sql3.=" GROUP BY md.id_dependencia_destino,dep.dependencia
								/*order by 3 desc*/
							";
					$sql3.=" UNION select dep2.codigo_dependencia,dep2.dependencia,'0'
							from db_general.dbo.h_dependencia dep2
							where dep2.condicion='ACTIVO'
								and dep2.codigo_dependencia<>34
								And dep2.codigo_dependencia in (select id_dependencia_destino from movimiento_documento where derivado=0)
								AND DEP2.CODIGO_DEPENDENCIA NOT IN (
									SELECT dep.codigo_dependencia
									FROM DOCUMENTO D,MOVIMIENTO_DOCUMENTO MD,DB_GENERAL.dbo.H_DEPENDENCIA DEP
							WHERE
							D.ID_DOCUMENTO=MD.ID_DOCUMENTO
							AND D.ID_TIPO_DOCUMENTO in (1,4)
							AND MD.DERIVADO=0 /*AND MD.FINALIZADO=0*/
							and md.id_dependencia_destino=dep.codigo_dependencia
									AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
									AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
								)";	
								
					if($sector==2)//Industria
						$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
					
					if($dependenciaCargo>0)
						$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;
					
					
					$sql3.=" order by 3 ASc
							";						
			}else{
				$sql3="select dep2.codigo_dependencia,dep2.dependencia
						from db_general.dbo.h_dependencia dep2
						where dep2.condicion='ACTIVO'
						and dep2.codigo_dependencia<>34
						and dep2.codigo_dependencia in (select id_dependencia_destino from movimiento_documento where derivado=0)";
				if($sector==2)//Industria
					$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
				
				if($dependenciaCargo>0)
					$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" order by dep2.dependencia
						";
			}			
				//echo $sql3;			  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
			
				
				for($a3=0;$a3<$num_registros3;$a3++){
					
					$nombreDependencia[]=$arreglo3[$a3][1];
					$codigoDependenciaEsp[]=$arreglo3[$a3][0];
				}	//fin del for($a=0...)
				
}//fin del if($this->conpara_fechas)				
  $this->abreConnDB();
   //$this->conn->debug = true;
  	 	 
		$sql_SP = "select convert(varchar,getDate(),103)";
  // echo $sql_SP;
  $rs = & $this->conn->Execute($sql_SP);
  unset($sql_SP);
  if (!$rs){
   print $this->conn->ErrorMsg();
  }else{
   		$fechaAnterior=$rs->fields[0];
   $rs->Close();
  }
  unset($rs);
				
				
				
					//$fecha=date('d/m/Y H:i');
					$desTitulo = "SITUACI�N DE LOS EXPEDIENTES CON SILENCIO POSITIVO Y NEGATIVO";
					
								$ceros=0;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;
	$textoDetalle="UBICACI�N";
 
  $desMensaje="<table width=\"770\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr> 
    <td><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">
        <tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;&nbsp;&nbsp;</font></td>
        </tr>				
      </table></td>
  </tr>
  <tr> 
    <td bgcolor=\"#C6E8FF\"><table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">
		<tr bgcolor=\"#6496CB\"> 
          <td width=\"350\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$textoDetalle."</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes<br />Anterior</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=3 \" ><img src=\"/img/800x600/b_down.gif\" border=\"0\"></a>Ingresos<a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=4\" ><img src=\"/img/800x600/b_up.gif\" border=\"0\"></a></font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=1 \" ><img src=\"/img/800x600/b_down.gif\" border=\"0\"></a>Pendientes<a href=\"index.php?accion=frmControlExp&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."&GrupoOpciones1=".$GrupoOpciones1."&orden=2\" ><img src=\"/img/800x600/b_up.gif\" border=\"0\"></a></font></b></td>
        </tr>		";
		$TotalPendientesAnt=$TotalPendientes-$TotalIngresos+$TotalFinalizados;
		
        for($j=0;$j<$num_registros3;$j++){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;
				$desMensaje.="<tr><td align=\"left\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\">".$nombreDependencia[$j]."</td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesAnt[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$ingresos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"textograyintranet\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$finalizados[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmListadoExtIntCurso&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."\" target=\"_blank\" style=\"text-decoration:underline \">".$pendientes[$nombreDependencia[$j]]."</a></font></strong></td>
							</tr>
				";		
						

		}

		$desMensaje.="<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">TOTAL</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesAnt."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalIngresos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalFinalizados."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientes."</font></strong></td>
        </tr>";		
		
        $desMensaje.="
      </table></td>
  </tr>
  <tr> 
    <td></td>
  </tr>
</table>
";

  $html->assign_by_ref('muestraStatusExp',$desMensaje);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
  
 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  $html->display('controlExtInt/index2.tpl.php');
 } 

 function ListadoExtIntEnCurso(){
  global $sector,$ubicacion,$dependenciaCargo,$FechaIni,$print;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $a=($_POST['a']) ? $_POST['a'] : $_GET['a'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmListadoExtIntCurso';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
  $html->assign_by_ref('print',$print);
  $html->assign_by_ref('a',$a);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  //echo $sql_st;
  
  	if($FechaFin==date('d/m/Y')){
				$sql="select d.id_documento,'',case when d.id_tipo_documento=1 then d.asunto
																  when d.id_tipo_documento=2 then ''
																  when (d.id_tipo_documento=4 or d.id_tipo_documento=5) then d.asunto end,
						  case when d.id_tipo_documento=4 then dbo.buscaReferencia(md.id_movimiento_documento)
							   else d.num_tram_documentario end,
						  case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
							   when sol.id_tipo_persona=2 then sol.razon_social
							   when d.id_tipo_documento=5 then d.referencia 
							   else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103),
							convert(varchar,md.audit_mod,103),t.apellidos_trabajador+' '+t.nombres_trabajador,
							dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103)),
							case when (d.id_tipo_documento in (1,2) and d.indicativo_oficio not in ('S/N','SN','S.N.')) then d.indicativo_oficio end,
							md.ultimo_avance,dep.siglas,'',d.fecha_max_plazo,d.id_estado_documento/*,
							case when dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."')>=tup.numero_dias then 1 end*/
						from documento d left join db_general.dbo.persona sol on d.id_persona=sol.id,
								movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO',
								db_general.dbo.h_dependencia dep
						WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
								and dep.codigo_dependencia=md.id_dependencia_destino
								and d.id_tipo_documento in (1,4)
								  ";
	}else{
	
				$sql_="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql_);
				unset($sql_);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}
					
				$sql="select d.ID_DOCUMENTO,d.TUPA,d.ASUNTO,d.referencia,
							 case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
							   	  when sol.id_tipo_persona=2 then sol.razon_social END,
							 convert(varchar,d.auditmod,103),
							 convert(varchar,d.audit_mod,103),'' as trabajador,
							 dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."'),
							 '' as indicativo,
							 '',dep.siglas,'','','' as estado/*,
							 case when dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),'".$FechaFin."')>=tup.numero_dias then 1 end*/
	
     				from LISTADOPENDIENTES_CONVENIO_SITRADOC D left join db_general.dbo.persona sol on d.razon_social=sol.id,/*DB_GENERAL.DBO.TUPA TUP,*/
						 /*DB_GENERAl.dbo.h_dependencia dep2,*/
						 db_general.dbo.h_dependencia dep 
					
					where d.id_tipo_documento IN (1,4)
							/*AND D.TUPA=TUP.ID_TUPA*/
							/*and dep2.codigo_dependencia=tup.codigo_dependencia*/
							AND D.FECHA='".$fechaPosterior."'
							
							and d.id_dependencia_destino=dep.codigo_dependencia";
	}
	
				if($sector==2)//Industria
					$sql.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql.=" and md.id_dependencia_destino in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";

				if($dependenciaCargo && $dependenciaCargo!="")
					$sql.=" and dep.dependencia='".$dependenciaCargo."' ";
					
					
				if($a==1){
					$sql.=" order by 5 desc";
				}elseif($a==2){
					$sql.=" order by 5 asc";
				}elseif($a==3){
					$sql.=" order by 9 desc";
				}elseif($a==4){
					$sql.=" order by 9 asc";
				}elseif($a==5){
					$sql.=" order by 12 desc";
				}elseif($a==6){
					$sql.=" order by 12 asc";
				}elseif($a==7){
					$sql.=" order by 16 desc";
				}elseif($a==8){
					$sql.=" order by 16 asc";
				}else{
					$sql.=" order by 9 desc";
				}
					
				//$sql.=" order by 9 desc";
  			//echo $sql;
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow()){
						$difDias2=$row[8]-$row[12];//DiasUtiles - DiasTupa
						if($difDias2>=0)
							$estado2="VENCIDO";
						else
							$estado2="DENTRO DEL PLAZO";
							
						$html->append('list',array('id' => $row[0],
												   'tu' => $row[1],
												   'tup' => $this->formateaTexto($row[2]),
												   'nroTD' => $row[3],
												   'razSoc' => $row[4],
												   'fecIngP' => $row[5],
												   'fecIngD' => $row[6],
												   'trab' => $row[7],
												   'diasUtiles' => $row[8],
												   'ind' => $row[9],
												   'comentario' => $row[10],
												   'ubicacionDepe' => $row[11],
												   'diasTupa' => $row[12],
												   'fecPlazo' => $row[13],
												   'idEstadoDoc' => $row[14],
												   'estado'=> $estado2
												   ));							
				}//fin del while
			}  

 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));  
  
	if($print==3){
				// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('controlExtInt/viewExtendListadoExtIntEnCurso.tpl.php');
		exit;
	}elseif($print==2){
		//setlocale(LC_TIME,"spanish");
		//$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/pie-logo.gif';		
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'hoja1'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('controlExtInt/ShowListadoExtIntEnCursoPdf.tpl.php'),true,$logo);
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;		
	}else{  
  		($print==1) ? $html->display('controlExtInt/printListadoExtIntEnCurso.tpl.php') : $html->display('controlExtInt/ListadoExtIntEnCurso.tpl.php');
  	}
 }
 
 function ControlExtInt2($page=NULL,$tipReporte=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroExp=NULL,$anyo=NULL,$codigo=NULL,$fecInicio=NULL,$fecFin=NULL,$tipCorrespondencia=NULL,$nroCor=NULL,$anyo2=NULL,$fecInicio2=NULL,$fecFin2=NULL,$tipBusqueda2=NULL,$codigo2=NULL){
  global $Opciones1,$filetip,$filetip2,$filetip3,$filetip4,$usuario,$code_val,$code;
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$print,$GrupoOpciones1;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmControlExtInt2';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  $html->assign_by_ref('tipReporte',$tipReporte);
  $html->assign_by_ref('tipDocumento',$tipDocumento);
  $html->assign_by_ref('tipBusqueda',$tipBusqueda);
  $html->assign_by_ref('nroExp',$nroExp);
  $html->assign_by_ref('anyo',$anyo);
  $html->assign_by_ref('codigo',$codigo);
  $html->assign_by_ref('tipCorrespondencia',$tipCorrespondencia);
  $html->assign_by_ref('nroCor',$nroCor);
  $html->assign_by_ref('anyo2',$anyo2);
  $html->assign_by_ref('tipBusqueda2',$tipBusqueda2);
  $html->assign_by_ref('codigo2',$codigo2);
  $html->assign_by_ref('Opciones1',$Opciones1);
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('filetip',$filetip);
  $html->assign_by_ref('filetip2',$filetip2);
  $html->assign_by_ref('filetip3',$filetip3);
  $html->assign_by_ref('filetip4',$filetip4);
  $html->assign_by_ref('usuario',$usuario);
  $html->assign_by_ref('code_val',$code_val);
  $html->assign_by_ref('code',$code);
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('print',$print);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  /*// Contenido Select Congresista
  $sql_st = "SELECT id, substring(razon_social,1,70) ".
        "FROM db_general.dbo.persona ".
        "where id_tipo_persona=2 ".
        "ORDER BY 2";
  $html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);*/
  
  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  // Fecha Ingreso
  $html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio,6,4), true));

  // Fecha Salida
  $html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin,6,4), true));

  // Fecha Ingreson de la Correspodencia
  $html->assign_by_ref('selMesIng2',$this->ObjFrmMes(1, 12, true, substr($fecInicio2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaIng2',$this->ObjFrmDia(1, 31, substr($fecInicio2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoIng2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecInicio2,6,4), true));

  // Fecha Salida de la Correspodencia
  $html->assign_by_ref('selMesSal2',$this->ObjFrmMes(1, 12, true, substr($fecFin2,0,2), true, array('val'=>'none','label'=>'------------------')));
  $html->assign_by_ref('selDiaSal2',$this->ObjFrmDia(1, 31, substr($fecFin2,3,2), true, array('val'=>'none','label'=>'----')));
  $html->assign_by_ref('selAnyoSal2',$this->ObjFrmAnyo(date('Y')-2, date('Y'), substr($fecFin2,6,4), true));
 
 
 /**/
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}
 
/*lISTADO DE LOS DOCUMENTOS FINALIZADOS*/

				$sql5="select DEP2.dependencia AS UBICACION, COUNT(*) AS FINALIZADOS 
						from movimiento_documento md,documento d,
							DB_GENERAl.dbo.h_dependencia dep2,finaldoc fd 
						where d.id_tipo_documento in (1,4)
						and dep2.codigo_dependencia=md.id_dependencia_destino
						AND MD.DERIVADO=0 and md.finalizado=1 
						and d.id_documento=md.id_documento
						and (md.id_dependencia_destino=fd.coddep)
						AND CONVERT(DATETIME,fd.auditmod,103)>convert(datetime,'".$FechaIni."',103)
						AND CONVERT(DATETIME,fd.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
						and md.id_documento=fd.id_documento";
										
				if($sector==2)//Industria
					$sql5.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql5.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql5.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql5.=" and md.id_dependencia_destino=".$dependenciaCargo;
										
				$sql5.=" group by DEP2.dependencia";
				//echo $sql5;			
				if(!mssql_query($sql5,$con)){
					echo "error en la consulta";
				}else{
					$resultado5=mssql_query($sql5,$con);
					$num_campos5=mssql_num_fields($resultado5);
					$num_registros5=mssql_num_rows($resultado5);
					$j5=$num_registros5-1;
					$jj5=-1;
						while($datos5=mssql_fetch_array($resultado5)){
								$jj5++;
								for($i5=0;$i5<$num_campos5;$i5++){
									$arreglo5[$jj5][$i5]=$datos5[$i5];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalFinalizados=0;
				for($a5=0;$a5<$num_registros5;$a5++){
						$yyyy=$arreglo5[$a5][0];
						$finalizados[$yyyy]=$arreglo5[$a5][1];
						$TotalFinalizados=$TotalFinalizados+$arreglo5[$a5][1];
				}
				//echo $TotalFinalizados;
				//echo "<br>".$sql5;exit;
				
				$sql6="select DEP2.dependencia AS UBICACION,
							COUNT(*) AS INGRESOS																											  
		        			from movimiento_documento md,documento d,
								 DB_GENERAl.dbo.h_dependencia dep2
							where  d.id_tipo_documento in (1,4)
							
							and dep2.codigo_dependencia=md.id_dependencia_destino
							AND MD.DERIVADO=0
							AND CONVERT(DATETIME,d.auditmod,103)>convert(datetime,'".$FechaIni."',103)
							AND CONVERT(DATETIME,d.auditmod,103)<dateadd(dd,1,convert(datetime,'".$FechaFin."',103))
							/*and md.finalizado=0*/
							and d.id_documento=md.id_documento";
				if($sector==2)//Industria
					$sql6.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql6.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql6.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
					
				if($dependenciaCargo>0)
					$sql6.=" and md.id_dependencia_destino=".$dependenciaCargo;	
					
				$sql6.=" group by DEP2.dependencia";
				//echo $sql6;
				if(!mssql_query($sql6,$con)){
					echo "error en la consulta";
				}else{
					$resultado6=mssql_query($sql6,$con);
					$num_campos6=mssql_num_fields($resultado6);
					$num_registros6=mssql_num_rows($resultado6);
					$j6=$num_registros6-1;
					$jj6=-1;
						while($datos6=mssql_fetch_array($resultado6)){
								$jj6++;
								for($i6=0;$i6<$num_campos6;$i6++){
									$arreglo6[$jj6][$i6]=$datos6[$i6];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				$TotalIngresos=0;
				for($a6=0;$a6<$num_registros6;$a6++){
						$mmmm=$arreglo6[$a6][0];
						$ingresos[$mmmm]=$arreglo6[$a6][1];
						$TotalIngresos=$TotalIngresos+$arreglo6[$a6][1];
				}							
				
				
				
				if($FechaFin==date('d/m/Y')){
						$sql4="select DEP2.dependencia AS UBICACION,
									COUNT(*) AS PENDIENTES																												  
									from movimiento_documento md,documento d,
										 DB_GENERAl.dbo.h_dependencia dep2 
									where md.derivado=0 and md.finalizado=0
									and d.id_tipo_documento in (1,4)
									and dep2.codigo_dependencia=md.id_dependencia_destino
																
									and d.id_documento=md.id_documento";
						if($sector==2)//Industria
							$sql4.=" and md.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and md.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and md.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($dependenciaCargo>0)
							$sql4.=" and md.id_dependencia_destino=".$dependenciaCargo;
							
						$sql4.=" group by DEP2.dependencia";
				}else{
				
				//$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaIni."',103)),103)";
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				
				
						$sql4="select DEP2.dependencia AS UBICACION,
								COUNT(*) AS PENDIENTES																												  
							from LISTADOPENDIENTES_CONVENIO_SITRADOC D,
								DB_GENERAl.dbo.h_dependencia dep2 
							where d.id_tipo_documento in (1,4)
								and dep2.codigo_dependencia=d.id_dependencia_destino
								AND D.FECHA='".$fechaPosterior."'";
								
						if($sector==2)//Industria
							$sql4.=" and d.id_dependencia_destino in (36,38,39,41,42,46,125,160) ";
						elseif($sector==3)//Pesquer�a
							$sql4.=" and d.id_dependencia_destino in (16,22,23,24,25,26) ";
						elseif($sector==4)//Secretar�a
							$sql4.=" and d.id_dependencia_destino in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
							
						if($dependenciaCargo>0)
							$sql4.=" and d.id_dependencia_destino=".$dependenciaCargo;
							
						$sql4.=" group by DEP2.dependencia";
				}
				//echo $sql4;
				if(!mssql_query($sql4,$con)){
					echo "error en la consulta";
				}else{
					$resultado4=mssql_query($sql4,$con);
					$num_campos4=mssql_num_fields($resultado4);
					$num_registros4=mssql_num_rows($resultado4);
					$j4=$num_registros4-1;
					$jj4=-1;
						while($datos4=mssql_fetch_array($resultado4)){
								$jj4++;
								for($i4=0;$i4<$num_campos4;$i4++){
									$arreglo4[$jj4][$i4]=$datos4[$i4];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$depPositivo=array();
				//$depNegativo=array();
				$TotalPendientes=0;
				for($a4=0;$a4<$num_registros4;$a4++){
					$xxxx=$arreglo4[$a4][0];
						$pendientes[$xxxx]=$arreglo4[$a4][1];
						$TotalPendientes=$TotalPendientes+$arreglo4[$a4][1];
				}		
/********Situaci�n de expedientes******/
				$sql3="select dep2.codigo_dependencia,dep2.dependencia
						from db_general.dbo.h_dependencia dep2
						where dep2.condicion='ACTIVO'
						and dep2.codigo_dependencia<>34
						and dep2.codigo_dependencia in (select id_dependencia_destino from movimiento_documento where derivado=0)";
				if($sector==2)//Industria
					$sql3.=" and dep2.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
				elseif($sector==3)//Pesquer�a
					$sql3.=" and dep2.codigo_dependencia in (16,22,23,24,25,26) ";
				elseif($sector==4)//Secretar�a
					$sql3.=" and dep2.codigo_dependencia in (5,8,9,10,12,7  ,47,115,48) "; /* 12/04/2011 */
				
				if($dependenciaCargo>0)
					$sql3.=" and dep2.codigo_dependencia=".$dependenciaCargo;	
											
				$sql3.=" order by dep2.dependencia
						";
				//echo $sql3;			  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				
				for($a3=0;$a3<$num_registros3;$a3++){
					
					$nombreDependencia[]=$arreglo3[$a3][1];
					$codigoDependenciaEsp[]=$arreglo3[$a3][0];
				}	//fin del for($a=0...)
				
				
  $this->abreConnDB();
   //$this->conn->debug = true;
  	 	 
		$sql_SP = "select convert(varchar,getDate(),103)";
  // echo $sql_SP;
  $rs = & $this->conn->Execute($sql_SP);
  unset($sql_SP);
  if (!$rs){
   print $this->conn->ErrorMsg();
  }else{
   		$fechaAnterior=$rs->fields[0];
   $rs->Close();
  }
  unset($rs);
				
				
				
					//$fecha=date('d/m/Y H:i');
					$desTitulo = "SITUACI�N DE LOS EXPEDIENTES CON SILENCIO POSITIVO Y NEGATIVO";
					
								$ceros=0;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;

 
  $desMensaje="<table width=\"770\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr> 
    <td><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">
        <tr> 
          <td align=\"left\" colspan=\"2\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">&nbsp;&nbsp;&nbsp;</font></td>
        </tr>				
      </table></td>
  </tr>
  <tr> 
    <td bgcolor=\"#C6E8FF\"><table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">
		<tr bgcolor=\"#6496CB\"> 
          <td width=\"350\" class=\"textowhite\" align=\"center\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">OFICINA A CARGO</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes<br />Anterior</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Ingresos</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Finalizados</font></b></td>
		  <td align=\"center\" class=\"textowhite\" ><b><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Pendientes</font></b></td>
        </tr>		";
		$TotalPendientesAnt=$TotalPendientes-$TotalIngresos+$TotalFinalizados;
		
        for($j=0;$j<$num_registros3;$j++){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;
				$desMensaje.="<tr><td valign=\"middle\" bgcolor=\"#FFFFFF\"><font color=\"#666666\" size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$nombreDependencia[$j]."</font></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$pendientesAnt[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$ingresos[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$finalizados[$nombreDependencia[$j]]."</font></strong></td>
								  <td align=\"center\" bgcolor=\"#FFFFFF\" ><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"index.php?accion=frmListadoExpCurso&dependenciaCargo=".$nombreDependencia[$j]."&sector=".$sector."&tipProceso=".$tipProceso."&tipAprobacion=".$tipAprobacion."&FechaIni=".$FechaIni."&FechaFin=".$FechaFin."\" target=\"_blank\">".$pendientes[$nombreDependencia[$j]]."</a></font></strong></td>
							</tr>
				";		
						

		}

		$desMensaje.="<tr bgcolor=\"#BED5FA\"> 
          <td align=\"left\" bgcolor=\"#FEF7C1\"><strong><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">TOTAL</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientesAnt."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalIngresos."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalFinalizados."</font></strong></td>
		  <td align=\"center\" bgcolor=\"#FEF7C1\" class=\"textoblack\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$TotalPendientes."</font></strong></td>
        </tr>";		
		
        $desMensaje.="
      </table></td>
  </tr>
  <tr> 
    <td></td>
  </tr>
</table>
";

  $html->assign_by_ref('muestraStatusExp',$desMensaje);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  
  if($print==3){
			$pendientesAnt[$nombreDependencia[$j]]=$pendientes[$nombreDependencia[$j]]-$ingresos[$nombreDependencia[$j]]+$finalizados[$nombreDependencia[$j]];
			if(!$ingresos[$nombreDependencia[$j]])
				$ingresos[$nombreDependencia[$j]]=0;
			if(!$finalizados[$nombreDependencia[$j]])
				$finalizados[$nombreDependencia[$j]]=0;
			if(!$pendientes[$nombreDependencia[$j]])
				$pendientes[$nombreDependencia[$j]]=0;
			if(!$vencidos[$nombreDependencia[$j]])
				$vencidos[$nombreDependencia[$j]]=0;  
  	for($j=0;$j<$num_registros3;$j++){
						$html->append('list',array('nombreDependencia' => $nombreDependencia[$j],
												   'pendientesAnt' => $pendientesAnt[$nombreDependencia[$j]],
												   'ingresos' => $ingresos[$nombreDependencia[$j]],
												   'finalizados' => $finalizados[$nombreDependencia[$j]],
												   'vencidos' => $vencidos[$nombreDependencia[$j]],
												   'pendientes' => $pendientes[$nombreDependencia[$j]]
												   ));  
  	}
	
	$html->assign_by_ref('TotalPendientesAnt',$TotalPendientesAnt);
	$html->assign_by_ref('TotalIngresos',$TotalIngresos);
	$html->assign_by_ref('TotalFinalizados',$TotalFinalizados);
	$html->assign_by_ref('TotalVencidos',$TotalVencidos);
	$html->assign_by_ref('TotalPendientes',$TotalPendientes);
	
		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('controlExtInt/viewExtendEstadoTramites.tpl.php');
		exit;	
  }
  
/**/

 
 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  ($print==1) ? $html->display('controlExtInt/printIndex2.tpl.php') : $html->display('controlExtInt/index2.tpl.php');
 } 
 
  function GraficoExtIntEnCurso(){
  global $sector,$tipAprobacion,$ubicacion,$tipProceso,$dependenciaCargo,$FechaIni,$GrupoOpciones1;
  $usuario=($_POST['usuario']) ? $_POST['usuario'] : $_GET['usuario'];
  $sector=($_POST['sector']) ? $_POST['sector'] : $_GET['sector'];
  $tipAprobacion=($_POST['tipAprobacion']) ? $_POST['tipAprobacion'] : $_GET['tipAprobacion'];
  $ubicacion=($_POST['ubicacion']) ? $_POST['ubicacion'] : $_GET['ubicacion'];
  $tipProceso=($_POST['tipProceso']) ? $_POST['tipProceso'] : $_GET['tipProceso'];
  $dependenciaCargo=($_POST['dependenciaCargo']) ? $_POST['dependenciaCargo'] : $_GET['dependenciaCargo'];
  $FechaIni=($_POST['FechaIni']) ? $_POST['FechaIni'] : $_GET['FechaIni'];
  $FechaFin=($_POST['FechaFin']) ? $_POST['FechaFin'] : $_GET['FechaFin'];
  $GrupoOpciones1=($_POST['GrupoOpciones1']) ? $_POST['GrupoOpciones1'] : $_GET['GrupoOpciones1'];
  $print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
  $this->abreConnDB();
  //$this->conn->debug = true;
	$anyoActual=date('Y');
  //Manipulacion de las Fechas
  
  $fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : "01/01/2008";
  $fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
  
  $fecInicio2 = ($fecInicio2!='//'&&$fecInicio2) ? $fecInicio2 : "01/01/2008";
  $fecFin2 = ($fecFin2!='//'&&$fecFin2) ? $fecFin2 : date('m/d/Y');
  
  if(!$FechaIni || $FechaIni=="")
	$FechaIni=date('d/m/Y');
  if(!$FechaFin || $FechaFin=="")
	$FechaFin=date('d/m/Y');	 
  // Genera HTML
  $html = new Smarty;
  
  // Setea Caracteristicas en el Formulario
  $frmName = 'frmGraficoExtIntEnCurso';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')); 
 
  // Setea Campos de los Datos Ingresados por el Usuario
  
  $html->assign_by_ref('anyoActual',$anyoActual);
  
  $html->assign_by_ref('sector',$sector);
  $html->assign_by_ref('tipAprobacion',$tipAprobacion);
  $html->assign_by_ref('tipProceso',$tipProceso);
  $html->assign_by_ref('FechaIni',$FechaIni);
  $html->assign_by_ref('FechaFin',$FechaFin);
  $html->assign_by_ref('dependenciaCargo',$dependenciaCargo);
   $html->assign_by_ref('print',$print);
  
  $html->assign_by_ref('FechaActualHoy',date('d/m/Y'));

  $sql_st = "SELECT distinct codigo_dependencia, dependencia ".
        "FROM db_general.dbo.h_dependencia ".
        "where codigo_dependencia in (select codigo_dependencia from db_general.dbo.tupa) ".
		"and codigo_dependencia<>34 ".
        "ORDER BY 2";
  $html->assign_by_ref('selDependenciaCargo',$this->ObjFrmSelect($sql_st, $dependenciaCargo, true, true, array('val'=>'none','label'=>'Todos')));
  unset($sql_st);  
  
  			if($FechaFin==date('d/m/Y')){
					$sql="select DEP.siglas AS UBICACION,
								count(*)  AS PENDIENTES
								from movimiento_documento md,documento d,
									 DB_GENERAl.dbo.h_dependencia dep 
								where md.derivado=0 and md.finalizado=0
								and d.id_tipo_documento in (1,4)
								and dep.codigo_dependencia=MD.ID_DEPENDENCIA_DESTINO
								and d.id_documento=md.id_documento
								";
					if($sector==2)//Industria
						$sql.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep.codigo_dependencia=".$dependenciaCargo;
						
					$sql.=" group by DEP.siglas";
			}else{
				$sql="select convert(varchar,dateadd(dd,1,convert(datetime,'".$FechaFin."',103)),103)";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					print $this->conn->ErrorMsg();
				else{
					$fechaPosterior=$rs->fields[0];
				}				

					$sql="select DEP.dependencia AS RESPONSABLE,
							COUNT(*) AS PENDIENTES																												  
						from LISTADOPENDIENTES_CONVENIO_SITRADOC D,
							DB_GENERAl.dbo.h_dependencia dep 
						where d.id_tipo_documento in (1,4)
							and dep.codigo_dependencia=d.id_dependencia_destino
							AND D.FECHA='".$fechaPosterior."'";
							
					if($sector==2)//Industria
						$sql.=" and dep.codigo_dependencia in (36,38,39,41,42,46,125,160) ";
					elseif($sector==3)//Pesquer�a
						$sql.=" and dep.codigo_dependencia in (16,22,23,24,25,26) ";
					elseif($sector==4)//Secretar�a
						$sql.=" and dep.codigo_dependencia in (5,8,9,10,12,18,13,21,32,6  ,47,115,48) ";
						
					if($dependenciaCargo>0)
						$sql.=" and dep.codigo_dependencia=".$dependenciaCargo;
						
					$sql.=" group by DEP.dependencia";	
			}		  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow()){
						//$difDias2=$row[8]-$row[12];//DiasUtiles - DiasTupa
						$dependencia_[]=$row[0];
						$nroPendientes_[]=$row[1];
				}//fin del while
			}  

		/*Estos son los datos para la generaci�n del pie*/
		$PG = new PowerGraphic;
		
		$PG->title     = 'Distribuci�n de Pendientes por dependencia';
		//$PG->axis_x    = 'Mes';
		$PG->axis_x    = 'Dependencia';
		$PG->axis_y    = 'Nro. Documentos';
		$PG->graphic_1 = 'Year 2004';
		$PG->graphic_2 = 'Year 2003';
		$PG->type      = 1;
		$PG->skin      = 1;
		$PG->credits   = 0;
		
		for($i=0;$i<count($dependencia_);$i++){
			$PG->x[$i] = $dependencia_[$i];
			$PG->y[$i] = $nroPendientes_[$i];
		}
		
		// Set values
		/*$PG->x[0] = 'Ene';
		$PG->y[0] = 35000;
		
		
		$PG->x[1] = 'Feb';
		$PG->y[1] = 38500;
		
		$PG->x[2] = 'Mar';
		$PG->y[2] = 40800;
		
		$PG->x[3] = 'Abr';
		$PG->y[3] = 45200;
		//$PG->y[3] = $contador4;		
		
		$PG->x[4] = 'May';
		$PG->y[4] = 46800;
		//$PG->y[4] = $contador5;		
		
		$PG->x[5] = 'Jun';
		$PG->y[5] = 55000;
		//$PG->y[5] = $contador6;*/
		/*$PG->x[6] = 'Jul';
		$PG->y[6] = $contador7;
		$PG->x[7] = 'Ago';
		$PG->y[7] = $contador8;
		$PG->x[8] = 'Sep';
		$PG->y[8] = $contador9;
		$PG->x[9] = 'Oct';
		$PG->y[9] = $contador10;
		$PG->x[10] = 'Nov';
		$PG->y[10] = $contador11;
		$PG->x[11] = 'Dic';
		$PG->y[11] = $contador12;				*/

//$matrix=$PG->create_query_string();
//echo "matrix is here".$matrix."matrix is here";exit;			
//$m='class.graphic.php?' . $PG->create_query_string() . '';
//$m.='<span style=\"font-size: 17px;\">&#8226; </span> <br /><br />';
$m.='<span style=\"font-size: 17px;\"></span><!-- <br /><br />-->';
/*$m.='1. Barras Verticales: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="imagen1" /><br /><br />';*/

// Changing the type

$PG->type = 2;
/*$m.='2. Barras Horizontales: <br /><br />';*/
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';

/*$PG->type = 3;
$m.='3. Puntos: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';*/

/*$PG->type = 4;
$m.='4. L�neas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br />';
*/
/*$PG->type = 5;
//$m.='5. Pie: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';

$PG->type = 6;
$m.='6. Donas: <br /><br />';
$m.='<img src="index.php?' . $PG->create_query_string() . '" border="1" alt="" /> <br /><br />';
*/
// Clear parameters
$PG->reset_values();
		$html->assign_by_ref('m',$m);

 
  //Setea la Accion por Defecto del Formulario
  $html->assign_by_ref('accion',$this->arr_accion);
  
  // Setea el Numero de Pagina a Mostrar
  $html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
  // Muestra el Formulario  
  //echo "matrix";
  ($print==1) ? $html->display('controlExtInt/printGraficoExtIntEnCurso.tpl.php') : $html->display('controlExtInt/GraficoExtIntEnCurso.tpl.php');
 }

	
}
?>
