<?
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class AccionesAltaDireccion extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function AccionesAltaDireccion($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_AGREGA_ATENCION =>'frmAddAtention',
							AGREGA_ATENCION => 'addAtention',
							FRM_BUSCA_ATENCION =>'frmSearchAte',
							BUSCA_ATENCION =>'searchAte',
							FRM_MODIFICA_ATENCION => 'frmModifyAtention',
							MODIFICA_ATENCION => 'modifyAtention',
							FRM_AGREGA_INFTECNICO =>'frmAddInfTecnico',
							AGREGA_INFTECNICO =>'addInfTecnico',
							MUESTRA_DETALLE => 'showDetail',
							IMPRIMIR_DOCUMENTO => 'printDocumento',
							IMPRIME_INF_TECNICO => 'printInfTecnico',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							REPORTE_ATE1 => 'ReporteAte1',
							REPORTE_ATE2 => 'ReporteAte2',
							REPORTE_ATE3 => 'ReporteAte3',
							GENERAREPORTE => 'GeneraReporte',
							GENERAEXCEL => 'GeneraExcel',
							FINALIZA_ACCION => 'FinalizaAccion',
							FRM_GENERA_REPORTE_NOTI => 'frmGeneraReporteNoti',
							GENERA_REPORTE_NOTI => 'generaReporteNoti',							
							MUESTRA_REPORT1_NIVELIII => 'showReport1NivelIII',
							MUESTRA_REPORT1_NIVELIV => 'showReport1NivelIV',
							MUESTRA_DETALLE_FLUJODOCDIR => 'showDetallesAccion',
							FRM_TRANSFIERE_DOC_ALTADIR => 'frmTransfiereDocAltaDir',
							TRANSFIERE_DOC_ALTADIR => 'transfiereDocAltaDir',
							FRM_TRANSFIERE_DOC_ALTADIR2 => 'frmTransfiereDocAltaDir2',
							FRM_RESPONDE_REQUERIMIENTO => 'frmRespondeRequerimiento',
							FRM_GENERA_RESPUESTA => 'frmGeneraRespuesta',
							GENERA_RESPUESTA => 'generaRespuesta',
							RESPONDE_REQUERIMIENTO => 'respondeRequerimiento',
							FRM_AGREGA_REQUERIMIENTO => 'frmAddRequerimiento',
							AGREGA_REQUERIMIENTO => 'addRequerimiento',
							MUESTRA_CRITERIOS_SELECCION => 'showCriteriosSeleccion',
							LISTADO_DESPACHO_VIRTUAL => 'ListadoDespachoVirtual',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		/*$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmTransfiereDocAltaDir', label => 'CONSOLIDADO' ),
							2 => array ( 'val' => 'frmRespondeRequerimiento', label => 'REQUERIMIENTOS' ),
							3 => array ( 'val' => 'frmGeneraRespuesta', label => 'RESPUESTA' ),
							4 => array ( 'val' => 'frmAddRequerimiento', label => 'AGREGAR' ),
							5 => array ( 'val' => 'frmGeneraReporteNoti', label => 'REPORTE' )
							);
		*/
		/*if($this->userIntranet['COD_DEP']==16||$this->userIntranet['COD_DEP']==36||$this->userIntranet['COD_DEP']==50||$this->userIntranet['COD_DEP']==1)
			$this->menu_items = array(
										0 => array ( 'val' => 'frmGeneraReporteNoti', label => 'CONSULTAS' )
										);		
		else*/
		if($this->userIntranet['COD_DEP']==1)
			$this->menu_items = array(
										//0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
										0 => array ( 'val' => 'frmTransfiereDocAltaDir2', label => 'CONSOLIDADO' )/*,
										1 => array ( 'val' => 'frmGeneraReporteNoti', label => 'CONSULTAS' )*/
										);
		else
			$this->menu_items = array(
										//0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
										0 => array ( 'val' => 'frmTransfiereDocAltaDir', label => 'DESPACHO SG' ),
										1 => array ( 'val' => 'frmTransfiereDocAltaDir2', label => 'DESPACHO DM ' )
										);
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		$this->pathTemplate = 'altaDireccion/';		
    }

	function MuestraStatTrans($accion){
	global $codReporte;
	global $id;
	global $ids;
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		
		
		$html->assign_by_ref('ids',$ids);
			for($q=0;$q<count($ids);$q++){
				$ruta .= "&ids[]={$ids[$q]}";
				$html->assign_by_ref('ruta',$ruta);
			}		
		
		if($codReporte){
		
					$imgStat = 'stat_true.gif';
					$imgOpcion = 'img_printer2.gif';
					$altOpcion = 'Imprimir Reporte';
					$lnkOpcion = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_DOCUMENTO]}&codReporte={$codReporte}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=500'); void('')";

					$html->assign_by_ref('codReporte',$codReporte);
					$html->assign_by_ref('imgOpcion',$imgOpcion);
					$html->assign_by_ref('altOpcion',$altOpcion);
					$html->assign_by_ref('lnkOpcion',$lnkOpcion);
		}
		if($id){
			$lnkOpcion2 = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIME_INF_TECNICO]}&id={$id}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=500'); void('')";
			$html->assign_by_ref('id',$id);
			$html->assign_by_ref('lnkOpcion2',$lnkOpcion2);
		}

		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_ATENCION]);
		
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		//echo $accion."hhh";
		//$html->display('altaDireccion/showStatTrans.inc.php');
		//$html->display($this->pathTemplate . 'footerArm.tpl.php');
		$html->display('altaDireccion/headerArm.tpl.php');
		$html->display('altaDireccion/showStatTrans.inc.php');
		$html->display('altaDireccion/footerArm.tpl.php');
		
	}

	function MuestraIndex(){
		if($this->userIntranet['COD_DEP']==1)
			$this->FormTransfiereDocAltaDir2();
		else
			$this->FormTransfiereDocAltaDir();
	}
	
	function ImprimeDocumento($codReporte){
	
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$sql_SP = sprintf("EXECUTE sp_busDatosAte3 %d",
								  $codReporte
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('diagnostico',ucfirst($row[7]));
														  $html->assign('indicativo',$row[8]);
														  $html->assign('oficina',$row[9]);
														  $html->assign('liquidacion',$row[4]);
														  $html->assign('tecnico',ucwords($row[11]));
														  $html->assign('tipAte1',ucfirst(strtolower($row[12])));
														  $html->assign('tipAte2',ucfirst(strtolower($row[13])));
														  $html->assign('tipAte3',ucfirst(strtolower($row[14])));
														  $html->assign('ate1',ucfirst(strtolower($row[4])));
														  $html->assign('ate2',ucfirst(strtolower($row[5])));
														  $html->assign('ate3',ucfirst(strtolower($row[6])));
														  $html->assign('modPC',ucfirst(strtolower($row[21])));
														  //'dia' =>$row[16];
														  $html->assign('inicio',substr($row[17],0,16));
														  $html->assign('fin',substr($row[18],0,16));
														  $html->assign('nroPC',$row[19]);
														  $html->assign('numero',$row[22]);
														  //'tiempo' => $this->obtieneDif($row[20])			;
				/**/
				// Setea Campos del Formulario
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
				$html->assign_by_ref('logo',$logo);
				
				$html->display($this->pathTemplate . 'printDocumento.tpl.php');
			}
			$rs->Close();
		}
	}

	function ImprimeInfTecnico($id){
	
		$this->abreConnDB();
		$this->conn->debug = false;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT t.NOMBRES_TECNICO+' '+t.APELLIDOS_TECNICO
				FROM dbo.h_tecnico t,db_general.dbo.h_trabajador tr,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.usuario=tr.email and tr.codigo_trabajador=t.codigo_trabajador
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$tecnico=$rs2->fields[0];
		}
		
		$sql3="SELECT d.asunto,d.observaciones 
				FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
				";
		$rs3 = & $this->conn->Execute($sql3);
		unset($sql3);
		if (!$rs3)
			$this->conn->ErrorMsg();
		else{
			$asunto=$rs3->fields[0];
			$observaciones=$rs3->fields[1];
			$html->assign_by_ref('asunto',$asunto);
			$html->assign_by_ref('observaciones',$observaciones);
		}
		
		$sql="select max(contador) from db_tramite_documentario.dbo.cuentainterno where coddep=13 and id_tipo_documento=4";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			$this->conn->ErrorMsg();
		else{
			$numero=$rs->fields[0];
			$html->assign_by_ref('numero',$numero);
		}
		
		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));
				// Setea Campos del Formulario
				setlocale(LC_TIME, "spanish");
				$html->assign('fechaGen',ucfirst(strftime("%d de %B del %Y")));
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$html->assign_by_ref('tecnico',$tecnico);
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
				
				$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];		
				$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		
				$filename = 'infTec'.mktime();
					
				$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/printDocumento2.tpl.php'),false,$logo);
				$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
				header("Content-type: application/pdf");
				header("Location: $destination");
				exit;
			}
			$rs->Close();
		}
	}


	function FormBuscaAtencion($page=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroTD=NULL,$asunto=NULL,$observaciones=NULL,$fecIniDir=NULL,$fecFinDir=NULL,$search=false){
		global $procedimiento;
		global $RazonSocial;
		global $RZ;
		global $indicativo;//Agregado el 09 de Junio 04:17pm.
		global $siglasDep;//Agregado el 09 de Junio 04:46pm.
		global $tipodDoc;
		global $tipDocumento,$tipBusqueda,$dia_ini,$nroTD,$asunto,$observaciones;
		global $dia_ini,$mes_ini,$anyo_ini;
		global $indInterno;
		global $claseindInterno,$m,$idMemo,$indCorresp;
		global $nroAnexo;
		global $anyo3;		
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir) ? $fecIniDir : "06/01/2005";
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir) ? $fecFinDir : date('m/d/Y');
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
				
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDocAltaDire';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('dia_ini',$dia_ini);
		$html->assign_by_ref('mes_ini',$mes_ini);
		$html->assign_by_ref('anyo_ini',$anyo_ini);

		$html->assign_by_ref('claseindInterno',$claseindInterno);
		$html->assign_by_ref('indInterno',$indInterno);
		$html->assign_by_ref('indCorresp',$indCorresp);
		
		$html->assign_by_ref('nroAnexo',$nroAnexo);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('siglasDep',$siglasDep);

		// Contenido Clase de Documento Interno
		$sql_st = "SELECT id_clase_documento_interno, substring(descripcion,1,16) ".
				  "FROM dbo.clase_documento_interno ".
				  "where procedencia='I' and categoria='D' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipodDoc',$this->ObjFrmSelect($sql_st, $tipodDoc, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT Upper(siglas), Upper(siglas) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "where codigo_dependencia<>34  ".
				  "and condicion='ACTIVO' ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep',$this->ObjFrmSelect($sql_st, $siglasDep, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		// Contenido Select Congresista
		$sql_st = "SELECT id, substring(razon_social,1,50) ".
						  "FROM db_general.dbo.persona ".
						  "where id_tipo_persona=2 ".
						  "and id<400 ".
						  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecIniDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecFinDir,6,4), true, array('val'=>'none','label'=>'--------')));

		$html->assign_by_ref('dia_ini',$dia_ini);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function obtieneDif($min){
		if($min>=1440){//nro de Dias
			$nroDias=floor($min/1440);
			$resto1=$min%1440;
			$nroHoras=floor($resto1/60);
			$nroMin=$resto1%60;
			$tiempo=$nroDias." dia(s) ".$nroHoras." hora(s) ".$nroMin." min"; 
		}elseif($min<1440&& $min>=60){//nro Horas
			$nroHoras=floor($min/60);
			$nroMin=$min%60;
			$tiempo=$nroHoras." hora(s) ".$nroMin." min";
		}elseif($min<60){//nro Minutos
			$tiempo=$min." min";
		}
		return($tiempo);
	}
	
	function buscaInformeTecnico($id){
		$this->abreConnDB();
		$this->conn->debug = false;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT count(*)
				FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$numero=$rs2->fields[0];
		}
		return($numero);
	}

	function BuscaAtencion($page,$stt,$nopc){
		global $procedimiento;
		global $RazonSocial;
		global $RZ;
		global $indicativo;//Agregado el 09 de Junio 04:17pm.
		global $siglasDep;//Agregado el 09 de Junio 04:46pm.
		global $tipodDoc;
		global $tipDocumento,$tipBusqueda,$dia_ini,$nroTD,$asunto,$observaciones;
		global $dia_ini,$mes_ini,$anyo_ini;
		global $indInterno;
		global $claseindInterno,$m,$idMemo,$indCorresp;
		global $nroAnexo;
		global $anyo3;		
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir&&!strstr($fecIniDir,'none')) ? $fecIniDir : NULL;
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir&&!strstr($fecFinDir,'none')) ? $fecFinDir : NULL;
		
		if($fecIniDir2&&$fecFinDir2){
			$fecIniDir=$fecIniDir2;
			$fecFinDir=$fecFinDir2;
		}
		//echo $fecIniNot." ".$fecFinNot."esta es una prueba";
		if($fecIniDir){$bFIng = $this->ValidaFechaProyecto($fecIniDir,'Inicio');}
		if($fecFinDir){ $bFSal = ($this->ValidaFechaProyecto($fecFinDir,'Fin')); }
		if ($fecIniDir&&$fecFinDir){

		$mes_ini=substr($fecIniDir,0,2);
		$mes_fin=substr($fecFinDir,0,2);
		$dia_ini=substr($fecIniDir,3,2);
		$dia_fin=substr($fecFinDir,3,2);
		$anyo_ini=substr($fecIniDir,6,4);
		$anyo_fin=substr($fecFinDir,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecIniDir, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFinDir, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";$this->FormBuscaAtencion($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "error en los meses";$this->FormBuscaAtencion($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "error en los dias";$this->FormBuscaAtencion($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
					}
			}
		}		

		}
		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
			//exit;
			$this->FormBuscaAtencion($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);
			
		}else{
		
		//$this->abreConnDB();
		//$this->conn->debug = true;
		/*Inicio de la parte Original: No se tiene en cuenta que se va a realizar una consulta sin listado de documentos
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDocDir';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocDir($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,true);
		
		Fin de la parte Original*/
		/*Inicio de la prueba del Listado de Documentos*/
		// Setea datos del Formulario
		//Esta parte lo pongo para que se muestre un resultado de a lo m�s 20 resultados.
		//$this->numMaxResultsSearch = 20;
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaAtencion($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,true);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);

		// Setea datos del Formulario
		$html->assign('frmName','frmSearchExtended');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array('tipDocumento'=>$tipDocumento,
									'tipBusqueda'=>$tipBusqueda,
									'nroTD'=>$nroTD,
									'asunto'=>$asunto,
									'observaciones'=>$observaciones,
									'procedimiento'=>$procedimiento,
									'fecIniDir'=>$fecIniDir,
									'fecFinDir'=>$fecFinDir,
									'indicativo'=>$indicativo,
									'siglasDep'=>$siglasDep,
									'tipodDoc'=>$tipodDoc,
									'page'=>$page,
									'anyo3'=>$anyo3
									));
		/*Fin de la prueba del Listado de Documentos*/
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();

		if($this->userIntranet['COD_DEP']==13){
			$codigoDependencia=5;
		}else{
			$codigoDependencia=$this->userIntranet['COD_DEP'];
		}
		switch($tipDocumento){
				case 1:
					switch($tipBusqueda){
						case 1://Nuevos documentos externos recibidos de Otra Dependencia
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "md.finalizado=0";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							break;
						case 6://Documentos Externos recibidos en la Dependencia provenientes de OTD
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[]="md.id_dependencia_origen=7";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Los documentos externos derivados de esta dependencia a otras dependencias
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) and id_oficio>0 ";
							$condConsulta[] = "md.id_documento not in (select id_documento from movimiento_documento where id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento group by id_documento) and id_dependencia_destino=" . $codigoDependencia ." group by id_documento)";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							break;
						case 9://Todos los documentos externos derivados a esta oficina
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "d.ID_TIPO_DOCUMENTO=1 and d.id_estado_documento=1";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_oficio is NULL and id_dependencia_origen=7 and id_dependencia_destino=" . $codigoDependencia .") or (id_oficio>0 and id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_persona=p.id and d.id_documento=md.ID_DOCUMENTO ";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						case 4://Todos los Documentos Externos Finalizados en esta dependencia
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=1 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=1";
							break;
						}
					break;
		
				case 2:
					switch($tipBusqueda){
						case 1://Nuevos expedientes derivados a la oficina//en este caso todos los derivados de OAD
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=2 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 6://Expedientes recibidos en la Dependencia provenientes de OTD
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[]="md.id_dependencia_origen=7";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=2 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Los expedientes derivados de esta dependencia a otras dependencias
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) and id_oficio>0 ";
							$condConsulta[] = "md.id_documento not in (select id_documento from movimiento_documento where id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento group by id_documento) and id_dependencia_destino=" . $codigoDependencia ." group by id_documento)";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=2 and d.id_persona=p.id ";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						case 9://Todos los expedintes derivados a esta oficina
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "d.ID_TIPO_DOCUMENTO=2 and d.id_estado_documento=1";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_oficio is NULL and id_dependencia_origen=7 and id_dependencia_destino=" . $codigoDependencia .") or (id_oficio>0 and id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_persona=p.id and d.id_documento=md.ID_DOCUMENTO ";
							break;
						case 4://Expedientes finalizados
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=2 and d.id_persona=p.id ";
							$condConsulta[] = "md.finalizado=1";
							break;
						}
					break;

				case 3:
					switch($tipBusqueda){
						case 1://Todos los Nuevos documentos derivados a la oficina//en este caso todos los derivados de OAD
							//$condTable[] ="requerimiento re";
							/*if($this->userIntranet['COD_DEP']==13){
								$condConsulta[] = "a.id=re.ID_accion and a.flag=0 and re.flag=0 and codigo_dependencia=5 ";
							}else{
								$condConsulta[] = "a.id=re.ID_accion and a.flag=0 and re.flag=0 and codigo_dependencia=".$this->userIntranet['COD_DEP']." ";
							}*/
							$condConsulta[] = "a.flag=1";
							break;
						case 6://Todos los Nuevos documentos derivados a la oficina//en este caso todos los derivados de OAD
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[]="md.id_dependencia_origen=7";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and (d.id_tipo_documento=2 or d.id_tipo_documento=1 or d.id_tipo_documento=4) ";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Todos los derivados de esta dependencia a otras dependencias
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) ";//esto es para obtener todos los derivados que se han realizado de esta dependencia a otra							
							$condConsulta[] = "md.id_movimiento_documento not in (select max(id_movimiento_documento) from movimiento_documento where id_oficio>0 AND id_dependencia_destino=" . $codigoDependencia ." group by id_documento) ";//esto es para garantizar de que no muestre los que nos hayan devuelto de una dependencia x a esta dependencia de nuevo							
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento in (1,2,4) ";
							break;
						case 9://Todos los documentos y expedientes
							$condTable[] ="db_general.dbo.persona p";
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "(d.ID_TIPO_DOCUMENTO=2 or d.id_tipo_documento=1) and d.id_estado_documento=1";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_oficio is NULL and id_dependencia_origen=7 and id_dependencia_destino=" . $codigoDependencia .") or (id_oficio>0 and id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_persona=p.id and d.id_documento=md.ID_DOCUMENTO";
							break;
						case 4://Todos los Nuevos documentos externos y expedientes finalizados en esta dependencia
							//$condTable[] ="movimiento_documento md";
							//$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							//$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and (d.id_tipo_documento=2 or d.id_tipo_documento=1 or d.id_tipo_documento=4) /*and d.id_persona=p.id*/";
							if($this->userIntranet['COD_DEP']==13||$this->userIntranet['COD_DEP']==5)
								$condConsulta[] = "a.flag=3 and a.flag_sec=0";
							elseif($this->userIntranet['COD_DEP']==16)
								$condConsulta[] = "a.flag=3 and a.flag_VP=0";
							elseif($this->userIntranet['COD_DEP']==36)
								$condConsulta[] = "a.flag=3 and a.flag_VI=0";
							elseif($this->userIntranet['COD_DEP']==50)
								$condConsulta[] = "a.flag=3 and a.flag_JG=0";

							break;
						}
					break;
					
				case 4:
					switch($tipBusqueda){
						case 1://Nuevos documentos internos provenientes de otras oficinas o dependencias.
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.id_documento and d.id_tipo_documento=4";
							$condConsulta[] = "md.finalizado=0";
							break;
						case 2://Los documentos internos derivados de esta dependencia a otras dependencia
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "md.id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento WHERE id_dependencia_origen=" . $codigoDependencia ." group by id_documento) ";//esto es para obtener todos los derivados que se han realizado de esta dependencia a otra							
							$condConsulta[] = "md.id_movimiento_documento not in (select max(id_movimiento_documento) from movimiento_documento where id_oficio>0 AND id_dependencia_destino=" . $codigoDependencia ." group by id_documento) ";//esto es para garantizar de que no muestre los que nos hayan devuelto de una dependencia x a esta dependencia de nuevo							
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO and d.id_tipo_documento=4 ";
							break;
						case 3://Nuevos Documentos Internos recibidos por parte de los trabajadores de la misma Oficina
							$condTable[] ="destino_persona dp";
							$condTable[] ="db_general.dbo.h_trabajador tr";
							$condConsulta[] = "dp.id_destino in (select max(id_destino) from destino_persona dp,documento d where dp.id_documento=d.id_documento and dp.estado='U'  group by dp.id_documento)";
							$condConsulta[] = "dp.codigo_persona=tr.codigo_trabajador and tr.coddep=" . $codigoDependencia ." and (tr.director=1)";
							$condConsulta[] = "d.id_tipo_documento=4 and dp.id_documento=d.id_documento";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						case 9://Todos los documentos internos recibidos y derivados a/de esta dependencia
							$condTable[] ="movimiento_documento md";
							$condConsulta[] = "d.ID_TIPO_DOCUMENTO=4 ";
							$condConsulta[] = "md.id_movimiento_documento in (select min(id_movimiento_documento) from movimiento_documento where ((id_dependencia_origen=" . $codigoDependencia .") or (id_dependencia_destino=" . $codigoDependencia .")) group by id_documento)";
							$condConsulta[] = "d.id_documento=md.ID_DOCUMENTO";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						case 4://Documentos Internos Finalizados
							$condTable[] ="movimiento_documento md";
							$condConsulta[]="md.enviado=1 and md.derivado=0 and md.id_dependencia_destino=".$codigoDependencia." ";
							$condConsulta[] = "d.id_documento=md.id_documento and d.id_tipo_documento=4";
							$condConsulta[] = "md.finalizado=1";
							break;
						case 5://Nuevos Documentos Internos creados por el Director para los Trabajadores de la Oficina
							$condTable[] ="destino_persona dp";
							$condTable[] ="db_general.dbo.h_trabajador tr";
							$condConsulta[] = "dp.id_destino in (select max(id_destino) from destino_persona dp,documento d where dp.id_documento=d.id_documento group by dp.id_documento)";
							$condConsulta[] = "dp.codigo_persona=tr.codigo_trabajador and tr.coddep=" . $codigoDependencia ." and (tr.director<>1 or tr.director is null )";
							$condConsulta[] = "d.id_tipo_documento=4 and dp.id_documento=d.id_documento";
							$condConsulta[] = "d.id_documento not in (select id_documento from finaldoc where coddep=".$codigoDependencia.")";
							break;
						}
					break;
				}
		
		// Condicionales Segun ingreso del Usuario
		if(!empty($asunto)&&!is_null($asunto))
			$condConsulta[] = "Upper(d.asunto) LIKE Upper('%{$asunto}%')";
		if(!empty($observaciones)&&!is_null($observaciones))
			$condConsulta[] = "Upper(d.observaciones) LIKE Upper('%{$observaciones}%')";
		if(!empty($procedimiento)&&!is_null($procedimiento)&&$tipDocumento==2){
			$condTable[] = "db_general.dbo.tupa tup";
			$condConsulta[] = "d.id_tup=tup.id_tupa";
			$condConsulta[] = "Upper(tup.descripcion) LIKE Upper('%{$procedimiento}%')";}
		if(!empty($RazonSocial)&&!is_null($RazonSocial)&&$RazonSocial!='none')
			$condConsulta[] = "d.id_persona=$RazonSocial";
		if(!empty($RZ)&&!is_null($RZ))
			$condConsulta[] = "((Upper(p.razon_social) LIKE Upper('%{$RZ}%')) or (Upper(p.apellidos) LIKE Upper('%{$RZ}%')) or (Upper(p.nombres) LIKE Upper('%{$RZ}%')))";
		if($fecIniDir&&$fecFinDir){
			if($fecIniDir==$fecFinDir){
				$condConsulta[] ="d.auditmod>=convert(datetime,'$fecIniDir',101) and d.auditmod<=dateadd(dd,1,convert(datetime,'$fecIniDir',101))";
			}else{
				$condConsulta[] ="d.auditmod>=convert(datetime,'$fecIniDir',101) and d.auditmod<=dateadd(dd,1,convert(datetime,'$fecFinDir',101))";
			}
		}
		if(!empty($nroTD)&&!is_null($nroTD))
			$condConsulta[] = "Upper(d.num_tram_documentario) LIKE Upper('%{$nroTD}%')";
		
		/*
		if((!empty($indicativo)&&!is_null($indicativo))&& (!empty($siglasDep)&&!is_null($siglasDep)&& $siglasDep!='none')&& (!empty($tipodDoc)&&!is_null($tipodDoc)&& $tipodDoc!='none')){
			if($anyo3>0){
				$indicativo2=$this->buscaIndicativoEspecial($tipodDoc,$indicativo,$siglasDep,$anyo3);
			}else{
				$indicativo2=$this->buscaIndicativoEspecial($tipodDoc,$indicativo,$siglasDep);
			}
			//$condConsulta[] = "Upper(d.indicativo_oficio) LIKE Upper('%{$indicativo2}%')";
			$condConsulta[] = "Upper(d.id_documento) LIKE Upper('%{$indicativo2}%')";
			//$condConsulta[] = "d.id_documento=$indicativo2";
		}
		elseif((!empty($indicativo)&&!is_null($indicativo))||(!empty($siglasDep)&&!is_null($siglasDep)&& $siglasDep!='none')){
			if(!empty($siglasDep)&&!is_null($siglasDep)&& $siglasDep!='none'){
				$condConsulta[] = "Upper(d.indicativo_oficio) LIKE Upper('%{$indicativo}-2005-CONVENIO_SITRADOC/{$siglasDep}%')";
			}else{
				$condConsulta[] = "Upper(d.indicativo_oficio) LIKE Upper('%{$indicativo}%')";
			}
		}
		*/
		if(!empty($nroAnexo)&&!is_null($nroAnexo)){
			$condConsulta[] = "d.id_documento=".$this->buscaNroTramitexAnexo($nroAnexo)." ";	
		}

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		//echo "table: ".$table;

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		if ($tipDocumento==4 && ($tipBusqueda==1||$tipBusqueda==2||$tipBusqueda==3||$tipBusqueda==4||$tipBusqueda==9||$tipBusqueda==5)){//agregado ultimo
					$sql_SP = sprintf("EXECUTE sp_busIdDocIntD %d,%s,%s,0,0",
										$codigoDependencia,
										($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
										($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		}else{
			$sql_SP = sprintf("EXECUTE sp_busIdAccion %d,%s,%s,%s,0,0",
								$codigoDependencia,
								($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		}
		//echo $sql_SP;
		//exit;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
		if ($tipDocumento==4 && ($tipBusqueda==1||$tipBusqueda==2||$tipBusqueda==3||$tipBusqueda==4||$tipBusqueda==9||$tipBusqueda==5)){//agregado ultimo
					$sql_SP = sprintf("EXECUTE sp_busIdDocIntD %d,%s,%s,%s,%s",
										$codigoDependencia,
										($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
										($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
										$start,
										$stop);
		}else{
			$sql_SP = sprintf("EXECUTE sp_busIdAccion %d,%s,%s,%s,%s,%s",
								$codigoDependencia,
								($nroTD) ? "'".$this->PrepareParamSQL($nroTD)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
		}
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){	
					// Obtiene Todos los Datos del documento
					if ($tipDocumento==4 ){
						$sql_SP = sprintf("EXECUTE sp_busDatosDocInternoV4 %d,%d",$id[0],$codigoDependencia);
					}else{
						$sql_SP = sprintf("EXECUTE sp_busDatosAccionJ %d,%d",$id[0],$codigoDependencia);
					}
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);

					if(!$rs)
						print $this->conn->ErrorMsg();
					else{						   
						while($DocumentoData = $rs->FetchRow())

							$html->append('arrDocDir', array('id' => $id[0],
														  'sol' => ucwords($DocumentoData[4]),
														  'ref' => $DocumentoData[2],
														  'asunto' => ucfirst($DocumentoData[2]),
														  'comment' => strtoupper($DocumentoData[3]),
														  'fec' => $DocumentoData[4],
														  'nroMemo' => $DocumentoData[5],
														  'der' => $DocumentoData[6],
														  'remi' => $DocumentoData[7]
														  ));


														  
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		$html->assign_by_ref('dependenciaJ',$codigoDependencia);
		//$html->assign_by_ref('dire',$this->SearchPrivilegios($this->userIntranet['CODIGO']));
		//$html->assign_by_ref('nomDire',$this->SearchDirector($codigoDependencia));
		$trabajador=$this->userIntranet['APELLIDO']." ".$this->userIntranet['NOMBRE'];
		$html->assign_by_ref('trabajador',$trabajador);
		$html->assign_by_ref('siglaDep',$this->userIntranet['SIGLA_DEP']);
		
		/*Para que retorne a la b�squeda se env�a previamente los valores de los par�metros*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);
		$html->assign_by_ref('anyo3',$anyo3);
		/**/
		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		/*Prueba Justomatrix is back*/
		$html->assign_by_ref('codTrabajador',$this->userIntranet['CODIGO']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DOCDIR], true));
			$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscarDocAltaDire', $this->arr_accion[BUSCA_ATENCION], true));
		
		// Muestra el Resultado de la Busqueda
		$html->display('altaDireccion/searchResult.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		}//fin del if($fecInicio&&$fecFin)
	}

	function FormAgregaAtencion($nopc=NULL,$tipo1=NULL,$tipo2=NULL,$tipo3=NULL,$aten1=NULL,$aten2=NULL,$aten3=NULL,$stt=NULL,$obs=NULL,$tiempo=NULL,$errors=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nopc',$nopc);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('tiempo',$tiempo);
		$html->assign_by_ref('stt',$stt);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo1',$this->ObjFrmSelect($sql_st, $tipo1, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo1) ? $tipo1 : 0);
		$html->assign_by_ref('selAten1',$this->ObjFrmSelect($sql_st, $aten1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo2 de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo2',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select 2del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo2) ? $tipo2 : 0);
		$html->assign_by_ref('selAten2',$this->ObjFrmSelect($sql_st, $aten2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo3',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, substring(lower(descripcion_soporte),1,53) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo3) ? $tipo3 : 0);
		$html->assign_by_ref('selAten3',$this->ObjFrmSelect($sql_st, $aten3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddAtention.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}

	function AgregaAtencion($nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo){
		// Comprueba Valores	
		if(!$nopc){ $bPC = true; $this->errors .= 'El N�mero de la PC debe ser especificado<br>'; }
		if(!$obs){ $bObs = true; $this->errors .= 'La observaci�n debe ser especificado<br>'; }
		if(!$tipo1){ $bTip1 = true; $this->errors .= 'El Tipo de Soporte debe ser especificada<br>'; }
		if(!$aten1){ $bAt1 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo1>0&&$aten1<1){ $bAt11 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo2>0&&$aten2<1){ $bAt2 = true; $this->errors .= 'La Segunda Atenci�n debe ser especificada<br>'; }
		if($tipo3>0&&$aten3<1){ $bAt3 = true; $this->errors .= 'La Tercera Atenci�n debe ser especificada<br>'; }

		if($bPC||$bObs||$bTip1||$bAt1||$bAt11||$bAt2||$bAt3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaAtencion($nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE sp_insReporte '%s',%d,%d,%d,%d,%d,%d,'%s',%s,%d,'%s'",
							  $this->PrepareParamSQL($nopc),
							  $this->PrepareParamSQL($tipo1),
							  $this->PrepareParamSQL($aten1),
							  ($tipo2) ? $this->PrepareParamSQL($tipo2) : "NULL",
							  ($aten2) ? $this->PrepareParamSQL($aten2) : "NULL",
							  ($tipo3) ? $this->PrepareParamSQL($tipo3) : "NULL",
							  ($aten3) ? $this->PrepareParamSQL($aten3) : "NULL",
							  $this->PrepareParamSQL($stt),
							  ($obs) ? "'".strtoupper($this->PrepareParamSQL($obs))."'" : "NULL",
							  ($tiempo) ? $this->PrepareParamSQL($tiempo) : "NULL",
							  $_SESSION['cod_usuario']);
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaAtencion($id,$nopc=NULL,$tipo1=NULL,$tipo2=NULL,$tipo3=NULL,$aten1=NULL,$aten2=NULL,$aten3=NULL,$stt=NULL,$obs=NULL,$tiempo=NULL,$reLoad=false,$errors=false){
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("EXECUTE sp_listAtencion %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$nopc = $row[0];
					$tipo1 = $row[1];
					$aten1 = $row[2];
					$tipo2 = $row[3];
					$aten2 = $row[4];
					$tipo3 = $row[5];
					$aten3 = $row[6];
					$stt = $row[7];
					$obs = $row[8];
				}
				$rs->Close();
			}
			unset($rs);
		}
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nopc',$nopc);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('tiempo',$tiempo);
		$html->assign_by_ref('stt',$stt);
		$html->assign_by_ref('tipo2',$tipo2);
		$html->assign_by_ref('tipo3',$tipo3);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo1',$this->ObjFrmSelect($sql_st, $tipo1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo1) ? $tipo1 : 0);
		$html->assign_by_ref('selAten1',$this->ObjFrmSelect($sql_st, $aten1, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo2 de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo2',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select 2del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo2) ? $tipo2 : 0);
		$html->assign_by_ref('selAten2',$this->ObjFrmSelect($sql_st, $aten2, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del tipo de SOPORTE
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM dbo.h_tipo_soporte ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipo3',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);

		// Contenido Select del SOPORTE
		$sql_st = sprintf("SELECT codigo_soporte, lower(descripcion_soporte) ".
				  "FROM dbo.h_soporte ".
						  "WHERE codigo_tipo_soporte=%d ".
						  "ORDER BY 2",($tipo3) ? $tipo3 : 0);
		$html->assign_by_ref('selAten3',$this->ObjFrmSelect($sql_st, $aten3, true, true, array('val'=>'none','label'=>'Escoja una Opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmModifyAtention.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
		function ModificaAtencion($id,$nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs){
		global $codReporte;

		// Comprueba Valores	
		if(!$nopc){ $bPC = true; $this->errors .= 'El N�mero de la PC debe ser especificado<br>'; }
		if(!$obs){ $bObs = true; $this->errors .= 'La observaci�n debe ser especificado<br>'; }
		if(!$tipo1){ $bTip1 = true; $this->errors .= 'El Tipo de Soporte debe ser especificada<br>'; }
		if(!$aten1){ $bAt1 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo1>0&&$aten1<1){ $bAt11 = true; $this->errors .= 'La Atenci�n debe ser especificada<br>'; }
		if($tipo2>0&&$aten2<1){ $bAt2 = true; $this->errors .= 'La Segunda Atenci�n debe ser especificada<br>'; }
		if($tipo3>0&&$aten3<1){ $bAt3 = true; $this->errors .= 'La Tercera Atenci�n debe ser especificada<br>'; }

		if($bPC||$bObs||$bTip1||$bAt1||$bAt11||$bAt2||$bAt3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaAtencion($id,$nopc,$tipo1,$tipo2,$tipo3,$aten1,$aten2,$aten3,$stt,$obs,$tiempo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			$sql_SP = sprintf("EXECUTE sp_modAtencion3 %d,'%s','%s','%s',%d,%d,%d,%d,%d,%d,'%s'",
							  $id,
							  $this->PrepareParamSQL($stt),
							  strtoupper($this->PrepareParamSQL($obs)),
							  $_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($tipo1),
							  $this->PrepareParamSQL($aten1),
							  ($tipo2) ? $this->PrepareParamSQL($tipo2) : "NULL",
							  ($aten2) ? $this->PrepareParamSQL($aten2) : "NULL",
							  ($tipo3) ? $this->PrepareParamSQL($tipo3) : "NULL",
							  ($aten3) ? $this->PrepareParamSQL($aten3) : "NULL",
							  $this->PrepareParamSQL($nopc)
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			$destination .= "&codReporte={$id}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormAgregaInfTecnico($id,$asunto=NULL,$diagnostico=NULL,$sol=NULL,$errors=false){
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
/**/
		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));
										}
			}

/**/

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddInfTecnico';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('diagnostico',$diagnostico);
		$html->assign_by_ref('sol',$sol);
	
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddInfTecnico.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');	
	}
	
	function AgregaInfTecnico($id,$asunto,$diagnostico,$sol){

		// Comprueba Valores	
		if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaInfTecnico($id,$asunto,$diagnostico,$sol,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql_SP = sprintf("EXECUTE sp_insInfTecnico %d,'%s','%s','%s','%s','%s'",
							  $id,
							  "-".date('Y')."-CONVENIO_SITRADOC/".$this->userIntranet['SIGLA_DEP']."-".$_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($asunto),
							  $this->PrepareParamSQL($diagnostico),
							  $this->PrepareParamSQL($sol),
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			$destination .= "&id={$id}";
			header("Location: $destination");
			exit;
		}
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}
	/*
	function FormGeneraReporte($search2=false){
		global $adjunto;
		//$this->abreConnDB();
		//$this->conn->debug = true;

		//Manipulacion de las Fechas
		$fecAtencion = ($fecAtencion!='//'&&$fecAtencion) ? $fecAtencion : date('m/Y');
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('adjunto',$adjunto);		
		
		//Mes y anyo
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecAtencion,0,2), true));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAtencion,3,4), true));

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'reportes.tpl.php');
		if(!$search2) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	*/
	function EnviaMsgCourier($GrupoOpciones1,&$adjunto){
	

	//echo "holas";
		$this->abreConnDB();
		//$this->conn->debug = true;
			
		//echo "dest".$eDest;

		//$eFrom = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;
	  //echo "<br>desde".$eFrom;
		// Envia el correo de Texto plano con el Adjunto
		$mail = & new htmlMimeMail();
		
		$desTitulo = "DESPACHO VIRTUAL";
		//$desMensaje = $html->fetch('oad/tramite/sumarioDirTxt.tpl.php');
		$desMensaje = "DESPACHO VIRTUAL";
					
		$eFrom = "ventana_virtual@" . $this->emailDomain;
			$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
		//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
		// Envia el correo de Texto plano con el Adjunto
		$mail = & new htmlMimeMail();
		$mail->setSubject($desTitulo);
		$mail->setText($desMensaje);

		if(is_uploaded_file($adjunto['tmp_name'])){
			$attachment = & $mail->getFile($adjunto['tmp_name']);
			$mail->addAttachment($attachment, $adjunto['name'], $adjunto['type']);
		}
		
		$mail->setFrom($eFrom);
			$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe,jnoborikawa@CONVENIO_SITRADOC.gob.pe');
			//$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,mcedamanos@CONVENIO_SITRADOC.gob.pe,jnoborikawa@CONVENIO_SITRADOC.gob.pe');
		//$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,otd@CONVENIO_SITRADOC.gob.pe,cgiron@CONVENIO_SITRADOC.gob.pe,mdioses@CONVENIO_SITRADOC.gob.pe,hcruzado@CONVENIO_SITRADOC.gob.pe');
		$mail->setReturnPath($eFrom);
		$result = $mail->send(array($eDest));
		
		/*
		$mail->setFrom($eFrom);
		$mail->setBcc($eDest);
		$mail->setReturnPath($eFrom);
		$result = $mail->send(array());
		//echo "gg".$result;
		*/

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('result',$result);
		// Muestra el Resultado del Envio
		//$html->display('oad/tramite/correspondencia/statMensaje.tpl.php');
		$html->display('oad/tramite/showResultQuery.tpl.php');
	}	

	function GeneraReporte($tipReporte,$mes,$anyo,&$adjunto){
		echo "matrix".$adjunto;exit;
			$this->EnviaMsgCourier(&$adjunto);
		
		/*
		switch($tipReporte){
			case 1:
			$this->ReporteAte1($mes,$anyo);
			break;
			case 2:
			$this->ReporteAte2($mes,$anyo);
			break;
			case 3:
			$this->ReporteAte3($mes,$anyo);//Reporte Software
			break;
			case 4:
			$this->ReporteAte4($mes,$anyo);//Reporte Hardware
			break;
			case 7:
			$this->ReporteAte5($mes,$anyo);//Reporte Otros
			break;
			case 5:
			$this->ReporteAte6($mes,$anyo);//Reporte Redes
			break;
			case 6:
			$this->ReporteAte7($mes,$anyo);//Reporte Mantenimiento Correctivo
			break;
			case 8:
			$this->ReporteAte8($mes,$anyo);//Reporte Software
			break;
			case 9:
			$this->ReporteEspecial($mes,$anyo);//Reporte Especial
			break;
		}
		*/
	}

	function ReporteAte1($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		//$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
	
		$sql_st = sprintf("EXECUTE sp_reporteAte1 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate1',array('hw'=>$row[0],
										  'sw'=>$row[1],
										  'redes'=>$row[2],
										  'otros'=>$row[3],
										  'total'=>$row[4],
										  'mc'=>$row[5]));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;
		//$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$html->assign('fechaGen',ucfirst(strftime("%d/%m/%Y   %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate1'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte1.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte2($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$hoy=date('d');
		$ayer=$hoy-1;
		
		$mesinicial=$mes-1;
		if($mesinicial<10){$mesinicial='0'.$mesinicial;}
		$fecha1='17/'.$mesinicial.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}else{
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}
		$anyoinicial=$anyo-1;
		if($mes==1){$fecha1='17/12/'.$anyoinicial;$fecha2='19/01/'.$anyo;$fecha3='20/'.$mes.'/'.$anyo;}
		
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);
	
		$sql_st = sprintf("EXECUTE sp_reporteAte2 '%s','%s'",
							$fecha1,
							$fecha3);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate2',array('hw'=>$row[0],
										  'hw1'=>$row[1],
										  'hw2'=>$row[2],
										  'hw3'=>$row[3],
										  'sw'=>$row[4],
										  'sw1'=>$row[5],
										  'sw2'=>$row[6],
										  'sw3'=>$row[7],
										  're'=>$row[8],
										  're1'=>$row[9],
										  're2'=>$row[10],
										  're3'=>$row[11],
										  'ot'=>$row[12],
										  'ot1'=>$row[13],
										  'ot2'=>$row[14],
										  'ot3'=>$row[15],
										  'tot'=>$row[16],
										  'tot1'=>$row[17],
										  'tot2'=>$row[18],
										  'tot3'=>$row[19],
										  'mc'=>$row[20],
										  'mc1'=>$row[21],
										  'mc2'=>$row[22],
										  'mc3'=>$row[23],
										  'hw4'=>$row[24],
										  'sw4'=>$row[25],
										  're4'=>$row[26],
										  'mc4'=>$row[27],
										  'ot4'=>$row[28],
										  'tot4'=>$row[29],
										  'hw5'=>$row[30],
										  'sw5'=>$row[31],
										  're5'=>$row[32],
										  'mc5'=>$row[33],
										  'ot5'=>$row[34],
										  'tot5'=>$row[35],
										  'hw6'=>$row[36],
										  'sw6'=>$row[37],
										  're6'=>$row[38],
										  'mc6'=>$row[39],
										  'ot6'=>$row[40],
										  'tot6'=>$row[41]
										  ));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate2'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte2.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function num($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	
	function ReporteAte3($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte3 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function numm($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3  and r.usuario='obarja'
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function ReporteAte8($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) and r.usuario='obarja'
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte8 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->numm($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function num2($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num3($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num4($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function num5($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	

	function ReporteAte4($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte4 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate4',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num2($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate4'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte4.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte5($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte5 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate5',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num3($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate5'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte5.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte6($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte6 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate6',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num4($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate6'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte6.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	function ReporteAte7($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte7 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate7',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num5($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate7'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte7.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function cuentaAteToti($fecha1,$fecha2,$codTrab){
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)>=convert(datetime,'$fecha1',103) 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)<=convert(datetime,'$fecha2',103) 
			and r.usuario=tr.email and tr.codigo_trabajador=$codTrab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
		
	}
	
	function cuentaAte($mes,$dia,$anyo,$trab){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email and tr.codigo_trabajador=$trab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}
	
	function cuentaAteTotalDia($mes,$dia,$anyo){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email 
			and.r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}

	function ReporteEspecial($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
			$maxDias=31;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			$maxDias=28;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			$maxDias=30;
			}
		/**/
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)> convert(datetime,'$fecha1',103) 
			AND convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)<=convert(datetime,'$fecha2',103)
			and r.flag='O'  
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = "SELECT APELLIDOS_TECNICO+' '+NOMBRES_TECNICO,CODIGO_TRABAJADOR
					FROM dbo.h_tecnico 
					WHERE ESTADO='ACTIVO' --AND CONDICION='C' AND NIVEL=1
					AND CLAVE>0  --or codigo_trabajador=2 
					order by 1 
					";
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('trab',array('nomb' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant1' => $this->cuentaAte($mes,1,$anyo,$row[1]),
										   'cant2' => $this->cuentaAte($mes,2,$anyo,$row[1]),
										   'cant3' => $this->cuentaAte($mes,3,$anyo,$row[1]),
										   'cant4' => $this->cuentaAte($mes,4,$anyo,$row[1]),
										   'cant5' => $this->cuentaAte($mes,5,$anyo,$row[1]),
										   'cant6' => $this->cuentaAte($mes,6,$anyo,$row[1]),
										   'cant7' => $this->cuentaAte($mes,7,$anyo,$row[1]),
										   'cant8' => $this->cuentaAte($mes,8,$anyo,$row[1]),
										   'cant9' => $this->cuentaAte($mes,9,$anyo,$row[1]),
										   'cant10' => $this->cuentaAte($mes,10,$anyo,$row[1]),
										   'cant11' => $this->cuentaAte($mes,11,$anyo,$row[1]),
										   'cant12' => $this->cuentaAte($mes,12,$anyo,$row[1]),
										   'cant13' => $this->cuentaAte($mes,13,$anyo,$row[1]),
										   'cant14' => $this->cuentaAte($mes,14,$anyo,$row[1]),
										   'cant15' => $this->cuentaAte($mes,15,$anyo,$row[1]),
										   'cant16' => $this->cuentaAte($mes,16,$anyo,$row[1]),
										   'cant17' => $this->cuentaAte($mes,17,$anyo,$row[1]),
										   'cant18' => $this->cuentaAte($mes,18,$anyo,$row[1]),
										   'cant19' => $this->cuentaAte($mes,19,$anyo,$row[1]),
										   'cant20' => $this->cuentaAte($mes,20,$anyo,$row[1]),
										   'cant21' => $this->cuentaAte($mes,21,$anyo,$row[1]),
										   'cant22' => $this->cuentaAte($mes,22,$anyo,$row[1]),
										   'cant23' => $this->cuentaAte($mes,23,$anyo,$row[1]),
										   'cant24' => $this->cuentaAte($mes,24,$anyo,$row[1]),
										   'cant25' => $this->cuentaAte($mes,25,$anyo,$row[1]),
										   'cant26' => $this->cuentaAte($mes,26,$anyo,$row[1]),
										   'cant27' => $this->cuentaAte($mes,27,$anyo,$row[1]),
										   'cant28' => $this->cuentaAte($mes,28,$anyo,$row[1]),
										   'cant29' => $this->cuentaAte($mes,29,$anyo,$row[1]),
										   'cant30' => $this->cuentaAte($mes,30,$anyo,$row[1]),
										   'cant31' => $this->cuentaAte($mes,31,$anyo,$row[1]),
										   'toti' => $this->cuentaAteToti($fecha1,$fecha2,$row[1])
										   
										   ));
			$rs->Close();
		}
		unset($rs);
		
			$html->assign_by_ref('total1',$this->cuentaAteTotalDia($mes,1,$anyo));
			$html->assign_by_ref('total2',$this->cuentaAteTotalDia($mes,2,$anyo));
			$html->assign_by_ref('total3',$this->cuentaAteTotalDia($mes,3,$anyo));
			$html->assign_by_ref('total4',$this->cuentaAteTotalDia($mes,4,$anyo));
			$html->assign_by_ref('total5',$this->cuentaAteTotalDia($mes,5,$anyo));
			$html->assign_by_ref('total6',$this->cuentaAteTotalDia($mes,6,$anyo));
			$html->assign_by_ref('total7',$this->cuentaAteTotalDia($mes,7,$anyo));
			$html->assign_by_ref('total8',$this->cuentaAteTotalDia($mes,8,$anyo));
			$html->assign_by_ref('total9',$this->cuentaAteTotalDia($mes,9,$anyo));
			$html->assign_by_ref('total10',$this->cuentaAteTotalDia($mes,10,$anyo));
			$html->assign_by_ref('total11',$this->cuentaAteTotalDia($mes,11,$anyo));
			$html->assign_by_ref('total12',$this->cuentaAteTotalDia($mes,12,$anyo));
			$html->assign_by_ref('total13',$this->cuentaAteTotalDia($mes,13,$anyo));
			$html->assign_by_ref('total14',$this->cuentaAteTotalDia($mes,14,$anyo));
			$html->assign_by_ref('total15',$this->cuentaAteTotalDia($mes,15,$anyo));
			$html->assign_by_ref('total16',$this->cuentaAteTotalDia($mes,16,$anyo));
			$html->assign_by_ref('total17',$this->cuentaAteTotalDia($mes,17,$anyo));
			$html->assign_by_ref('total18',$this->cuentaAteTotalDia($mes,18,$anyo));
			$html->assign_by_ref('total19',$this->cuentaAteTotalDia($mes,19,$anyo));
			$html->assign_by_ref('total20',$this->cuentaAteTotalDia($mes,20,$anyo));
			$html->assign_by_ref('total21',$this->cuentaAteTotalDia($mes,21,$anyo));
			$html->assign_by_ref('total22',$this->cuentaAteTotalDia($mes,22,$anyo));
			$html->assign_by_ref('total23',$this->cuentaAteTotalDia($mes,23,$anyo));
			$html->assign_by_ref('total24',$this->cuentaAteTotalDia($mes,24,$anyo));
			$html->assign_by_ref('total25',$this->cuentaAteTotalDia($mes,25,$anyo));
			$html->assign_by_ref('total26',$this->cuentaAteTotalDia($mes,26,$anyo));
			$html->assign_by_ref('total27',$this->cuentaAteTotalDia($mes,27,$anyo));
			$html->assign_by_ref('total28',$this->cuentaAteTotalDia($mes,28,$anyo));
			if($maxDias==30||$maxDias==31){
				$html->assign_by_ref('total29',$this->cuentaAteTotalDia($mes,29,$anyo));
				$html->assign_by_ref('total30',$this->cuentaAteTotalDia($mes,30,$anyo));
			}
			if($maxDias==31){
				$html->assign_by_ref('total31',$this->cuentaAteTotalDia($mes,31,$anyo));
			}

		$html->assign_by_ref('maxDias',$maxDias);
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'trab'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte8.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function DetalleInformeTecnico($id){
		// Genera HTML de Muestra
		$html = new Smarty;
		$this->abreConnDB();
		
		$sql="select d.asunto,d.observaciones,d.indicativo_oficio
				from db_tramite_documentario.dbo.documento d,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
			";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$diagnostico=$rs->fields[0];
			$sol=$rs->fields[1];
			$indicativo=$rs->fields[2];
			$html->assign_by_ref('diagnostico',$diagnostico);
			$html->assign_by_ref('sol',$sol);
		}
		
				$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );;
		
		//echo $sql_SP;
		$rsData = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsData){
			print $this->conn->ErrorMsg();
		}else{
				//while($DocumentoData = $rsData->FetchRow())
				$usuario=$rsData->fields[0];
				$coddep=$rsData->fields[1];
				$descrip=$rsData->fields[2];
				$flag=$rsData->fields[3];
				$siglas=$rsData->fields[4];
				$marca=$rsData->fields[5];
				$modelo=$rsData->fields[6];
				$codPat=$rsData->fields[7];
				$serie=$rsData->fields[8];
				$dep=$rsData->fields[9];
				$codPC=$rsData->fields[10];
		$html->assign_by_ref('usuario',$usuario);
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('descrip',$descrip);
		$html->assign_by_ref('flag',$flag);
		$html->assign_by_ref('siglas',$siglas);
		$html->assign_by_ref('marca',$marca);
		$html->assign_by_ref('modelo',$modelo);
		$html->assign_by_ref('codPat',$codPat);
		$html->assign_by_ref('serie',$serie);
		$html->assign_by_ref('dep',$dep);
		$html->assign_by_ref('codPC',$codPC);
			//unset($row);
			$rsData->Close();
		}
		unset($rsData);
		
		$titulo="INFORME T�CNICO N� ".$indicativo;
		$html->assign_by_ref('titulo',$titulo);

		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oti/helpdesk/printDetalle.tpl.php') : $html->display('oti/helpdesk/showDetalle.tpl.php');
	}
	
	function FinalizaAccion(){
		global $ids,$idObs;
		
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
						
				if ($ids){
					$ss=count($ids);
							for($q=0;$q<$ss;$q++){
								$sql_SP = sprintf("EXECUTE sp_finalizaAccion %d,%d,'%s'",
													$ids[$q],
													$this->userIntranet['COD_DEP'],
													$_SESSION['cod_usuario']
												  );
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
									$rs->Close();
								}
								unset($rs);
							}
					//$idDoc = $ids[0];
				}		
							
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}";
			header("Location: $destination");
			//echo "hhh".$destination;
			exit;
	}
	
	function FormGeneraReportes($search2=false){
		global $tipReporte,$nrotramite,$claseDocInt;
		global $fecInicio,$fecFin;
		global $siglasMult;
		global $subDependencia;
		global $GrupoOpciones1,$a;
		global $tipoDoc;//Para el listado de Documentos Salientes de la Dependencia
		global $tipoDocc;//Para el Listado de Documentos Recibidos en la Dependencia
		global $numero;
		global $depInicial;
		global $status,$trabajador;
		global $tipTrat;
		global $tupa;
		global $anyo3;
		global $adjunto;
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('nrotramite',$nrotramite);
		$html->assign_by_ref('claseDocInt',$claseDocInt);
		$html->assign_by_ref('siglasMult',$siglasMult);
		$html->assign_by_ref('subDependencia',$subDependencia);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('a',$a);
		$html->assign_by_ref('tipoDocc',$tipoDocc);
		$html->assign_by_ref('numero',$numero);
		$html->assign_by_ref('tipoDoc',$tipoDoc);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('adjunto',$adjunto);

		$html->assign_by_ref('status',$status);		
		
		
		
		// Contenido Select Tipo de Tratamiento
		$sql_st = "SELECT id_tipo_tratamiento, lower(descripcion) ".
				  "FROM tipo_tratamiento ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipTrat',$this->ObjFrmSelect($sql_st, $tipTrat, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select TUPA
		$sql_st = "SELECT id_tupa, substring(descripcion,1,120) ".
				  "FROM db_general.dbo.tupa ".
				  "WHERE codigo_dependencia=".$this->userIntranet['COD_DEP']." ".
				  "ORDER BY 1";
		$html->assign_by_ref('selTupa',$this->ObjFrmSelect($sql_st, $tupa, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecInicio,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
		//$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));		

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display('altaDireccion/headerArm.tpl.php');
		//if($codigoTrabajador==646){
		$html->display('altaDireccion/reportes.tpl.php');
		//}else{
		//$html->display('oad/tramite/reportesDocDir.tpl.php');
		//}
		if(!$search2) $html->display('altaDireccion/footerArm.tpl.php');
	}
	
	/**/function busSiglasSITRADOC($dat){//haya las siglas(Dependencia) actual del Documento Externo o Expediente
		$this->abreConnDB();
		$this->conn->debug =false;
		$sql="select Upper(dep.siglas) 
		         from movimiento_documento md, db_general.dbo.h_dependencia dep
				 where md.id_documento=$dat and md.derivado=0
				  and md.id_dependencia_destino=dep.codigo_dependencia"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				//$depe1=0;
				}
				else{
				
					$i=-1;
					while(!$rs->EOF){
					$i++;
					$arregloP[$i]=$rs->fields[0];
					$rs->MoveNext();
					}

						$depe=$arregloP[0];
						$contador=count($arregloP);
						//echo "el contador".$depe;
						if($contador>0){
							$depe1=$depe;
							for($h=1;$h<$contador;$h++){
								$depe1=$depe1.'-'.$arregloP[$h];
							}
						}//fin del if($contador>0)
					}
			//unset($rs);
		return($depe1);
	}	
	
	function Reporte1DataMiningNivelIII($tipDoc,$sit,$coddep,$fecha1,$fecha2){
		global $RZ,$fecVenc,$asunto,$estado;
		global $checkboxx,$checkboxx2,$checkboxx3,$checkboxx4;
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);		
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));				
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		// Contenido Clase de Documento Interno
		/**/
		$sql_st = "SELECT id_tupa, substring(descripcion,1,80) ".
				  "FROM db_general.dbo.tupa ".
				  "ORDER BY 1";
		$html->assign_by_ref('selTupa',$this->ObjFrmSelect($sql_st, $tupa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
		/**/
		// Contenido Clase de Documento Interno
		$sql_st = "SELECT codigo_trabajador, apellidos_trabajador+' '+nombres_trabajador ".
				  "FROM db_general.dbo.h_trabajador ".
				  "WHERE estado='ACTIVO' and condicion<>'GENERAL' ".
				  "AND coddep=5 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
		
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('sit',$sit);
		$html->assign_by_ref('tipDoc',$tipDoc);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);
		$html->assign_by_ref('fecVenc',$fecVenc);
		$html->assign_by_ref('asunto',$asunto);	
		$html->assign_by_ref('estado',$estado);
		
		$html->assign_by_ref('checkboxx',$checkboxx);
		$html->assign_by_ref('checkboxx2',$checkboxx2);
		$html->assign_by_ref('checkboxx3',$checkboxx3);
		$html->assign_by_ref('checkboxx4',$checkboxx4);					
		
		$print ? $html->display('oad/tramite/printDetalle.tpl.php') : $html->display('altaDireccion/reportesDataMining/showSeleccionOrden.tpl.php');				
		
	}
	
	function Reporte1DataMiningNivelIV($tipDoc,$sit,$coddep,$tupa,$trabajador,$RZ,$print2,$fecha1,$fecha2,$print,$print3){
		global $fecVenc,$asunto,$estado;
		global $fechai,$fechaf;
		global $checkboxx,$checkboxx2,$checkboxx3,$checkboxx4;
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//echo "estado: ".$estado;
		$this->abreConnDB();
		//$this->conn->debug = true;
		
			$sql_st ="SELECT distinct a.id,a.fecha_recepcion,a.registro_documento,
			
							a.remitente,
			                 case when d.id_tipo_documento in (1,4) then d.asunto
							      when d.id_tipo_documento=2 then tup.descripcion end,
							 CONVERT(varchar,d.auditmod,103),flag_sec,flag_vp,flag_vi,flag_jg,a.registro_documento,r.fecha 
							            
							 from requerimiento r,accion a,documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
							 							
							 where d.id_documento=a.id_documento and r.id_accion=a.id
							 and r.fecha in (select min(fecha) from requerimiento group by id_accion)
			                
							";
			if($this->userIntranet['COD_DEP']==5){
				$sql_st.= " and a.flag_sec=0 ";
			}elseif($this->userIntranet['COD_DEP']==16){
				$sql_st.= " and a.flag_vp=0 ";
			}elseif($this->userIntranet['COD_DEP']==36){
				$sql_st.= " and a.flag_vi=0 ";
			}
					
			/*
			if($estado==1){$sql_st.=" and a.flag in (3,4) ";}
			elseif($estado==2){$sql_st.=" and a.flag in (3) ";}
			elseif($estado==3){$sql_st.=" and a.flag in (4) ";}
			elseif($estado==4){$sql_st.=" and a.flag in (3) and convert(datetime,r.fecha,103)<getDate() ";}			
			*/
			if(($checkboxx==1||$checkboxx2==2||$checkboxx3==3||$checkboxx4==4)){//DOCUMENTOS EXTERNOS
				
				$sql_st.=" and (";
				if($checkboxx==1){$nn[]="a.flag in (3,4)";}
				if($checkboxx2==2){$nn[]="a.flag in (3)";}
				if($checkboxx3==3){$nn[]="a.flag in (4)";}
				if($checkboxx4==4){$nn[]="(a.flag in (3) and convert(datetime,r.fecha,103)<getDate())";}
				$sql_st.= (count($nn)>0) ? ' '.implode(' or ',$nn) : '';
				
				$sql_st.=" ) ";
			}			
			
			if($tupa>0){$sql_st.=" and d.id_tup=$tupa ";}
			if($trabajador>0){$sql_st.=" and md.codigo_trabajador=$trabajador ";}
			if(!empty($RZ)&&!is_null($RZ)){
				$sql_st.=" and Upper(a.remitente) LIKE Upper('%{$RZ}%')  ";
			}
			if(!empty($asunto)&&!is_null($asunto)){
				$sql_st.=" and (Upper(d.asunto) LIKE Upper('%{$asunto}%') ) ";
			}
			if(!empty($fechai)&&!is_null($fechai)&&!empty($fechaf)&&!is_null($fechaf)){
				$sql_st.=" and a.fecha_despacho>convert(datetime,'$fechai',103) and a.fecha_despacho<dateadd(dd,1,convert(datetime,'$fechaf',103)) ";
			}
						
			if($fecVenc==4&&($tipDoc==1||$tipDoc==2)){$sql_st.=" and datediff(d,getDate(),convert(datetime,d.Fecha_Max_Plazo,103)) > 10 ";}
			if($fecVenc==3&&($tipDoc==1||$tipDoc==2)){$sql_st.=" and datediff(d,getDate(),convert(datetime,d.Fecha_Max_Plazo,103)) <= 10 and datediff(d,getDate(),convert(datetime,d.Fecha_Max_Plazo,103)) >= 0 ";}
			if($fecVenc==2&&($tipDoc==1||$tipDoc==2)){$sql_st.=" and convert(datetime,d.Fecha_Max_Plazo,103) < getDate() ";}
			if($fecVenc==4&&($tipDoc==4)){$sql_st.=" and datediff(d,getDate(),convert(datetime,md.fecha_plazo,103)) > 10 ";}
			if($fecVenc==3&&($tipDoc==4)){$sql_st.=" and datediff(d,getDate(),convert(datetime,md.fecha_plazo,103)) <= 10 and datediff(d,getDate(),convert(datetime,md.fecha_plazo,103)) >= 0 ";}
			if($fecVenc==2&&($tipDoc==4)){$sql_st.=" and convert(datetime,md.fecha_plazo,103) < getDate() ";}
			$sql_st.=" order by 1 desc";
							
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('id' => $row[0],
										   'tu' => $row[1],
										   'tup' => $row[2],
										   'razSoc' => $row[3],
										   'asunto' => $row[4],
										   'fecIngP' => $row[5],
										   'flag1' => $row[6],
										   'flag2' => $row[7],
										   'flag3' => $row[8],
										   'flag4' => $row[9],
										   'nroTD' => $row[10],
										   'fecVenc' => $row[11]
										   ));
			$rs->Close();
		}
		unset($rs);		

		
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('tipDoc',$tipDoc);
		$html->assign_by_ref('sit',$sit);
		$html->assign_by_ref('tupa',$tupa);
		$html->assign_by_ref('trabajador',$trabajador);									
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);			
		$html->assign_by_ref('fechai',$fechai);
		$html->assign_by_ref('fechaf',$fechaf);			
		
		if($print3==1){
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';		
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'hoja1'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('oad/tramite/DinamicReports/ReportToSGandVice/showListadoDocPDF.tpl.php'),true,$logo);
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;		
		
		}
		if($print2==1){
					// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('oad/tramite/DinamicReports/ReportToSGandVice/viewExtendListadpDoc.tpl.php');
			exit;
		}else{
			$print ? $html->display('oad/tramite/DinamicReports/ReportToSGandVice/printListadoDoc.tpl.php') : $html->display('altaDireccion/reportesDataMining/showListadoDoc.tpl.php');				
		}
	}
	
	function GeneraHtml($idRequerimiento){
		$html = new Smarty;
				
		$this->abreConnDB();
		$this->conn->debug =false;
		$sql="select id_respuesta,respuesta,fecha,flag,flag_email 
		         from requerimiento_respuesta
				 where id_requerimiento=$idRequerimiento"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				//$depe1=0;
				}
				else{
				
					$i=-1;
					while(!$rs->EOF){
					$i++;
					$arregloP[$i]=$rs->fields[0];
					$arregloP2[$i]=$rs->fields[1];
					$arregloP3[$i]=$rs->fields[2];
					$arregloP4[$i]=$rs->fields[3];
					$arregloP5[$i]=$rs->fields[4];
					$rs->MoveNext();
					}

						$depe=$arregloP[0];
						/**/
						$idRespuestaI=$arregloP[0];
						$respuestaI=$arregloP2[0];
						$fecRI=$arregloP3[0];
						$flagI=$arregloP4[0];
						$flagEmailI=$arregloP5[0];
						//echo $respuestaI;
						$htmlnew="
						  <tr>
							<td class=\"textoblack\" width=\"20%\">Respuesta 1:</td>
							<td width=\"60%\" colspan=\"2\" class=\"textoblack\"><textarea name=\"respuesta{$idRespuestaI}\" cols=\"60\" rows=\"4\" class=\"iptxt1\" id=\"observaciones\" >";
							if($respuestaI){$htmlnew.="{$respuestaI}";}
							else{$htmlnew.="Sin respuesta";}
						$htmlnew.="</textarea></td>
						  	<td class=\"textoblack\" width=\"20%\">";
							if ($flagEmailI==0&& ($coddepTrab==13||$coddepTrab==$coddepDestino)){
							   $htmlnew.="<input type=\"submit\" class=\"texto\" value=\"Actualizar Borrador\" onClick=\"cargar(document.{$frmName},3,{$idRespuestaI})\">";
							 }  
							   $htmlnew.="</td>
						  </tr>
						  <tr>
							<td class=\"textoblack\">Fecha de Respuesta 1:</td>
							<td colspan=\"2\" class=\"textoblack\">";
							if ($fecRI!=""){$htmlnew.="{$fecRI}";}
							else{$htmlnew.="No se ha respondido";}
							
							$htmlnew.="</td>
						  	<td class=\"textoblack\">";
							
							if ($flagEmailI==0&& ($coddepTrab==13||$coddepTrab==$coddepDestino)){
							$htmlnew.="<input type=\"submit\" class=\"texto\" value=\"Enviar\" onClick=\"cargar(document.{$frmName},4,{$idRespuestaI})\">";}
							
							$htmlnew.="</td>
						  </tr>
						  <tr>
							<td class=\"textoblack\">&nbsp;</td>
							<td class=\"textored\"><strong>";
							if ($flagI==1){$htmlnew.="ATENDIDO";}
							else{$htmlnew.="PENDIENTE";}
							if ($flagEmailI==1){$htmlnew.="-SE HA ENVIADO POR MAIL";}
							$htmlnew.="</strong></td>
							<td class=\"textored\"><div align=\"right\"><strong>";
							if ($flagEmailI==0){$htmlnew.="BORRADOR";}
							$htmlnew.="</strong></div></td>
							<td class=\"textoblack\">&nbsp;</td>
						  </tr>
						";/**/
						$contador=count($arregloP);
						//echo "el contador".$depe;
						if($contador>0){
							$htmlnewN=$htmlnew;
													
							for($h=1;$h<$contador;$h++){
							$hh=$h+1;
							$idRespuestaI=$arregloP[$h];
							$respuestaI=$arregloP2[$h];
							$fecRI=$arregloP3[$h];
							$flagI=$arregloP4[$h];
							$flagEmailI=$arregloP5[$h];
								$arregloP[$h]="
						  <tr>
							<td class=\"textoblack\" width=\"20%\">Respuesta 1:</td>
							<td width=\"60%\" colspan=\"2\" class=\"textoblack\"><textarea name=\"respuesta{$idRespuestaI}\" cols=\"60\" rows=\"4\" class=\"iptxt1\" id=\"observaciones\" >";
							if($respuestaI){$arregloP[$h].="{$respuestaI}";}
							else{$arregloP[$h].="Sin respuesta";}
						$arregloP[$h].="</textarea></td>
						  	<td class=\"textoblack\" width=\"20%\">";
							if ($flagEmailI==0&& ($coddepTrab==13||$coddepTrab==$coddepDestino)){
							   $arregloP[$h].="<input type=\"submit\" class=\"texto\" value=\"Actualizar Borrador\" onClick=\"cargar(document.{$frmName},3,{$idRespuestaI})\">";
							 }  
							   $arregloP[$h].="</td>
						  </tr>
						  <tr>
							<td class=\"textoblack\">Fecha de Respuesta 1:</td>
							<td colspan=\"2\" class=\"textoblack\">";
							if ($fecRI!=""){$arregloP[$h].="{$fecRI}";}
							else{$arregloP[$h].="No se ha respondido";}
							
							$arregloP[$h].="</td>
						  	<td class=\"textoblack\">";
							
							if ($flagEmailI==0&& ($coddepTrab==13||$coddepTrab==$coddepDestino)){
							$arregloP[$h].="<input type=\"submit\" class=\"texto\" value=\"Enviar\" onClick=\"cargar(document.{$frmName},4,{$idRespuestaI})\">";}
							
							$arregloP[$h].="</td>
						  </tr>
						  <tr>
							<td class=\"textoblack\">&nbsp;</td>
							<td class=\"textored\"><strong>";
							if ($flagI==1){$arregloP[$h].="ATENDIDO";}
							else{$arregloP[$h].="PENDIENTE";}
							if ($flagEmailI==1){$arregloP[$h].="-SE HA ENVIADO POR MAIL";}
							$arregloP[$h].="</strong></td>
							<td class=\"textored\"><div align=\"right\"><strong>";
							if ($flagEmailI==0){$arregloP[$h].="BORRADOR";}
							$arregloP[$h].="</strong></div></td>
							<td class=\"textoblack\">&nbsp;</td>
						  </tr>
								";					
								$htmlnewN=$htmlnewN.' '.$arregloP[$h];
							}
						}//fin del if($contador>0)
					}
			//unset($rs);
		return($htmlnewN);
	}
	
	function DetallesFlujoDirector($id, $a, $m, $respuesta, $respuesta2, $respuesta3, $respuesta4, $print=false){
		global $requerimiento1,$requerimiento2,$requerimiento3,$requerimiento4,$requerimiento5;
		global $fecha1,$fecha2,$fecha3,$fecha4,$fecha5;
		global $opcionX1,$opcionX2,$opcionX3,$opcionX4,$opcionX5;
		global $fechai,$fechaf;
		//global $respuesta2;	
	    //echo "el tipo de doc es: ".$tipDoc;
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		$sql=sprintf(" select a.id,a.registro_documento,a.resumen,a.nro_memorando,
		                     a.flag_sec,a.flag_vp,a.flag_vi,a.flag_jg,a.id_documento,a.derivacion
					  
					   from accion a where a.id=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$regDoc=$rs->fields[1];
			$resumen=$rs->fields[2];
			$nroMemo=$rs->fields[3];
			$flagSG=$rs->fields[4];
			$flagDVMPE=$rs->fields[5];
			$flagVMI=$rs->fields[6];
			$flagDMASES=$rs->fields[7];
			$idDocumento=$rs->fields[8];
			$derivado=$rs->fields[9];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('a',$a);
		$html->assign_by_ref('m',$m);
		$html->assign_by_ref('regDoc',$regDoc);				  
		$html->assign_by_ref('resumen',$resumen);
		$html->assign_by_ref('nroMemo',$nroMemo);
		$html->assign_by_ref('flagSG',$flagSG);
		$html->assign_by_ref('flagDVMPE',$flagDVMPE);
		$html->assign_by_ref('flagVMI',$flagVMI);
		$html->assign_by_ref('flagDMASES',$flagDMASES);
		$html->assign_by_ref('idDocumento',$idDocumento);
		$html->assign_by_ref('derivado',$derivado);
		$html->assign_by_ref('ubiActual',$this->busSiglasSITRADOC($idDocumento));
		$html->assign_by_ref('coddepTrab',$this->userIntranet['COD_DEP']);
		
		$html->assign_by_ref('fechai',$fechai);
		$html->assign_by_ref('fechaf',$fechaf);
		
		if(($this->userIntranet['DIRE']==1&& (in_array($this->userIntranet['COD_DEP'],array(1,13,5,16,36,50,2))))||(in_array($this->userIntranet['CODIGO'],array(646,185)))){
			$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
										convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
										convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
										dir.observaciones,mt.link,mt.observaciones,dir.fecha_plazo
									from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
								where  
								d.id_documento=md.id_documento
								and md.id_movimiento_documento=mt.id_movimiento
								and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
								and d.id_documento=%d order by dir.id_dirigido",$this->PrepareParamSQL($id));
			$control=1;
			$html->assign_by_ref('control',$control);					
		}else{
			$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
										convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
										convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
										dir.observaciones,mt.link,mt.observaciones,dir.fecha_plazo
									from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
								where md.id_dependencia_destino=%d 
								and d.id_documento=md.id_documento
								/*and mt.id_movimiento_tratamiento in (select max(id_movimiento_tratamiento) from movimiento_tratamiento group by id_movimiento)*/
								and md.id_movimiento_documento=mt.id_movimiento
								and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
								and d.id_documento=%d order by dir.id_dirigido",$this->userIntranet['COD_DEP'],$this->PrepareParamSQL($id));
		}								
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('doc', array(//'id'=>$id,
										   'nombre'=>$row[0],
										   'avance'=>$row[1],
										   'diaEnvio'=>$row[3],
										   'horaEnvio'=>$row[4],
										   'diaRec'=>$row[5],
										   'horaRec'=>$row[6],
										   'obs'=>$row[7],
										   'link'=>$row[8],
										   'obsSecre'=>$row[9],
										   'fPlazo'=>$row[10]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		//Procedimientos para las acciones de agregar respuesta, finalizar,actualizar borrador y enviar
		$this->abreConnDB();		
		/**/
		//Procedimiento para ingresar un nuevo requerimiento
		if($a==6){
		//$this->conn->debug=true;		
			//echo $rptaEspecial."holas";
		$depOrigen=1;
			if($opcionX1==1){//Requerimiento para todos
				//Para SG
				$depDestino=5;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento1),
								  $depDestino,								  
								  $depOrigen,
								  $fecha1,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
				$reqN=$requerimiento1;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento1."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));				
				

				$depDestino=16;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento1),
								  $depDestino,								  
								  $depOrigen,
								  $fecha1,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento1;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento1."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								

				$depDestino=36;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento1),
								  $depDestino,								  
								  $depOrigen,
								  $fecha1,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento1;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento1."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								

				$depDestino=50;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento1),
								  $depDestino,								  
								  $depOrigen,
								  $fecha1,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				//$desMensaje = $html->fetch('altaDireccion/detallesAccionTxt.tpl.php');
				$reqN=$requerimiento1;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento1."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								
			
			}
			
			if($opcionX2==1){
				$depDestino=5;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento2),
								  $depDestino,								  
								  $depOrigen,
								  $fecha2,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento2;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento2."
				               https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								
			}
				
			if($opcionX3==1){
				$depDestino=16;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento3),
								  $depDestino,								  
								  $depOrigen,
								  $fecha3,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento3;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento3."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								
			}
			
			if($opcionX4==1){
				$depDestino=36;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento4),
								  $depDestino,								  
								  $depOrigen,
								  $fecha4,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento4;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);																
				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento4."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								
			}		

			if($opcionX5==1){
				$depDestino=50;
				$sql_SP = sprintf("EXECUTE sp_insRequerimientoEsp %d,'%s',%d,%d,'%s','%s'",
								  $id,
								  $this->PrepareParamSQL($requerimiento5),
								  $depDestino,								  
								  $depOrigen,
								  $fecha5,
								  $_SESSION['cod_usuario']
								  );
				 //echo $sql_SP;exit;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs)
					$RETVAL=1;
				else{
					$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
					$rs->Close();
				}
				unset($rs);
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE GENERA REQUERIMIENTO";
				$reqN=$requerimiento5;
				$direccion="https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}";
				$html->assign_by_ref('reqN',$reqN);
				$html->assign_by_ref('direccion',$direccion);												

				$desMensaje = $html->fetch('altaDireccion/envioReqTxt.tpl.php');
				/*$desMensaje = "REGISTRO_DOCUMENTO: ".$regDoc."
							   RESUMEN: ".$resumen."
							   Nro. Memorando SG: ".$nroMemo."
							   Derivado a: ".$derivado."
							   REQUERIMIENTO:
							   ".$requerimiento5."
							   https:///institucional/aplicativos/altadireccion/index.php?accion=showDetallesAccion&id={$id}							   
				";*/
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				//$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));								
			}							
		}		
		/**/
		//Procedimiento para Actualizar Borrador, solo se actuliza el borrador mas no se env�a mail
		if($a==3){
		//$this->conn->debug=true;		
			$rptaEspecial=$respuesta[$m];
			//echo $rptaEspecial."holas"; 
			$sql=sprintf(" update requerimiento set respuesta='$rptaEspecial',fecha_respuesta=convert(varchar,getDate(),103)
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$exitoAccion=$rs->fields[1];
					/*
					$sql2="select count(*) from requerimiento_respuesta where id_requerimiento=$m and flag_email=0";
					$rs2 = & $this->conn->Execute($sql2);
					unset($sql2);
					if (!$rs2){
						print $this->conn->ErrorMsg();
					}else{
						$contt=$rs2->fields[0];
						if($contt==1){//Ya existe un requerimiento que no se ha enviado por mail
							$sql3="update requerimiento_respuesta set respuesta='$rptaEspecial',fecha=convert(varchar,getDate(),103)
							      	where id_requerimiento=$m and flag_email=0
								  ";
						}else{//Es la priemera vez que se va a crear una respuesta
							$sql3="insert into requerimiento_respuesta values($m,'$rptaEspecial',convert(varchar,getDate(),103),'PRUEBA',getDate(),0,0)
								  ";
						}
						
							$rs3 = & $this->conn->Execute($sql3);
							unset($sql3);
							if (!$rs3){
								print $this->conn->ErrorMsg();
							}else{
								$exitoAccion=$rs3->fields[1];
							}						
					}
					*/				
			}			
		}
		
		if($a==71||$a==81||$a==91){
		//$this->conn->debug=true;
			if($a==71){		
				$rptaEspecial2=$respuesta2[$m];
			}elseif($a==81){
				$rptaEspecial3=$respuesta3[$m];
			}elseif($a==91){
				$rptaEspecial4=$respuesta4[$m];
			}
			//echo $rptaEspecial2."holas";exit; 
			if($a==71){
				$sql=sprintf(" update requerimiento set respuesta2='$rptaEspecial2',fecha2=convert(varchar,getDate(),103)
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));
			}elseif($a==81){
				$sql=sprintf(" update requerimiento set respuesta3='$rptaEspecial3',fecha3=convert(varchar,getDate(),103)
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));			
			}elseif($a==91){
				$sql=sprintf(" update requerimiento set respuesta4='$rptaEspecial4',fecha4=convert(varchar,getDate(),103)
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));			
			}
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$exitoAccion=$rs->fields[1];
			}			
		}		
		
		//Procedimiento para enviar el Documento al Ministro y ya no se puede modificar el Borrador
		if($a==4){
		//$this->conn->debug=true;		
			$rptaEspecial=$respuesta[$m];
			//echo $rptaEspecial."holas"; 
			$sql=sprintf(" update requerimiento set respuesta='$rptaEspecial',fecha_respuesta=convert(varchar,getDate(),103),flag_email=1
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$exitoAccion=$rs->fields[1];
				
/**/
			$sqlM=sprintf("SELECT requerimiento,fecha,respuesta,fecha_respuesta,
			                 case when codigo_dependencia_destino=5 then 'SG'
							      when codigo_dependencia_destino=16 then 'DVM-PE'
								  when codigo_dependencia_destino=36 then 'VMI'
								  when codigo_dependencia_destino=50 then 'DM-ASES' end
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			$rsM = & $this->conn->Execute($sqlM);
			unset($sqlM);
			if (!$rsM){
				print $this->conn->ErrorMsg();
			}else{
					$requerimientoMail=$rsM->fields[0];
					$fechaMail=$rsM->fields[1];
					$respuestaMail=$rsM->fields[2];
					$fechaRespuestaMail=$rsM->fields[3];
					$depDestinoMail=$rsM->fields[4];
					$html->assign_by_ref('requerimientoMail',$requerimientoMail);
					$html->assign_by_ref('fechaMail',$fechaMail);
					$html->assign_by_ref('respuestaMail',$respuestaMail);
					$html->assign_by_ref('fechaRespuestaMail',$fechaRespuestaMail);
					$html->assign_by_ref('depDestinoMail',$depDestinoMail);
			}
/**/				
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE DA RESPUESTA A REQUERIMIENTO";
				$desMensaje = $html->fetch('altaDireccion/detallesAccionTxt.tpl.php');
				//$desMensaje = $rptaEspecial;
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));				
			}			
		}

		if($a==72||$a==82||$a==92){
		//$this->conn->debug=true;		
			if($a==72){
				$rptaEspecial2=$respuesta2[$m];
			}elseif($a==82){
				$rptaEspecial3=$respuesta3[$m];
			}elseif($a==92){
				$rptaEspecial4=$respuesta4[$m];
			}
			//echo $rptaEspecial."holas"; 
			if($a==72){
				$sql=sprintf(" update requerimiento set respuesta2='$rptaEspecial2',fecha2=convert(varchar,getDate(),103),flag_email2=1
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));
			}elseif($a==82){
				$sql=sprintf(" update requerimiento set respuesta3='$rptaEspecial3',fecha3=convert(varchar,getDate(),103),flag_email3=1
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));			
			}elseif($a==92){
				$sql=sprintf(" update requerimiento set respuesta4='$rptaEspecial4',fecha3=convert(varchar,getDate(),103),flag_email4=1
						   		from requerimiento where id=%d",$this->PrepareParamSQL($m));			
			}
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$exitoAccion=$rs->fields[1];
				
/**/		if($a==72){
				$sqlM=sprintf("SELECT requerimiento,fecha,respuesta2,fecha2,
			                 case when codigo_dependencia_destino=5 then 'SG'
							      when codigo_dependencia_destino=16 then 'DVM-PE'
								  when codigo_dependencia_destino=36 then 'VMI'
								  when codigo_dependencia_destino=50 then 'DM-ASES' end
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			}elseif($a==82){
				$sqlM=sprintf("SELECT requerimiento,fecha,respuesta3,fecha3,
			                 case when codigo_dependencia_destino=5 then 'SG'
							      when codigo_dependencia_destino=16 then 'DVM-PE'
								  when codigo_dependencia_destino=36 then 'VMI'
								  when codigo_dependencia_destino=50 then 'DM-ASES' end
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			}elseif($a==92){
				$sqlM=sprintf("SELECT requerimiento,fecha,respuesta4,fecha4,
			                 case when codigo_dependencia_destino=5 then 'SG'
							      when codigo_dependencia_destino=16 then 'DVM-PE'
								  when codigo_dependencia_destino=36 then 'VMI'
								  when codigo_dependencia_destino=50 then 'DM-ASES' end
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			}
			$rsM = & $this->conn->Execute($sqlM);
			unset($sqlM);
			if (!$rsM){
				print $this->conn->ErrorMsg();
			}else{
					$requerimientoMail=$rsM->fields[0];
					$fechaMail=$rsM->fields[1];
					$respuestaMail=$rsM->fields[2];
					$fechaRespuestaMail=$rsM->fields[3];
					$depDestinoMail=$rsM->fields[4];
					$html->assign_by_ref('requerimientoMail',$requerimientoMail);
					$html->assign_by_ref('fechaMail',$fechaMail);
					$html->assign_by_ref('respuestaMail',$respuestaMail);
					$html->assign_by_ref('fechaRespuestaMail',$fechaRespuestaMail);
					$html->assign_by_ref('depDestinoMail',$depDestinoMail);
			}
/**/				
				
				$mail = & new htmlMimeMail();
		
				$desTitulo = "SE DA RESPUESTA A REQUERIMIENTO";
				$desMensaje = $html->fetch('altaDireccion/detallesAccionTxt.tpl.php');
				//$desMensaje = $rptaEspecial;
					
				$eFrom = "ventana_virtual@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));				
			}			
		}
		
		//Procedimiento para finalizar el requerimiento, si ya est�n resueltos todos los requerimientos el documento se considera atendido
		if($a==2){
		//$this->conn->debug=true;		
			//echo $rptaEspecial."holas"; 
			$sql=sprintf("update requerimiento set flag=1
						   from requerimiento where id=%d",$this->PrepareParamSQL($m));
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$exitoAccion=$rs->fields[0];
			}
			
			$sql2= "select count(*) from requerimiento where flag=0";// Se revisa si se han finalizado todos los requerimientos
			$rs2 = & $this->conn->Execute($sql2);
			unset($sql2);
			if (!$rs2){
				print $this->conn->ErrorMsg();
			}else{
				$contFlag=$rs2->fields[0];
				if($contFlag==0){
					$sql3= "update accion set flag=4 where id=$id";// El documento pasa al estado Atendido si se han finalizado todos los requerimientos
					$rs3 = & $this->conn->Execute($sql3);
					unset($sql3);
					if (!$rs3){
						print $this->conn->ErrorMsg();
					}else{
						$exitoAccion=$rs3->fields[0];
					}						
				}
			}						
						
		}		
		$sql="select dep.siglas from finaldoc fd,db_general.dbo.h_dependencia dep where fd.coddep=dep.codigo_dependencia and fd.id_documento=$idDocumento";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			
			$sitActual=$rs->fields[0];
			$html->assign_by_ref('sitActual',$sitActual);
		}
		unset($rs);
		
			if($this->userIntranet['COD_DEP']==5){
				$sql_st = sprintf("select r.id,r.requerimiento,r.respuesta ,
									case when r.codigo_dependencia_origen=1 then 'DM'
										 when r.codigo_dependencia_origen=5 then 'SG'
										 when r.codigo_dependencia_origen=16 then 'DVM-PE'
										 when r.codigo_dependencia_origen=36 then 'VMI'
										 when r.codigo_dependencia_origen=50 then 'DM-ASES' end,
									case when r.codigo_dependencia_destino=1 then 'DM'
										 when r.codigo_dependencia_destino=5 then 'SG'
										 when r.codigo_dependencia_destino=16 then 'DVM-PE'
										 when r.codigo_dependencia_destino=36 then 'VMI'
										 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.fecha,r.FECHA_RESPUESTA,r.flag_email,r.flag,r.codigo_dependencia_destino,
										 r.respuesta2,r.fecha2,r.flag_email2,r.respuesta3,r.fecha3,r.flag_email3,
										 r.respuesta4,r.fecha4,r.flag_email4
									 from requerimiento r
								WHERE r.id_accion=%d and r.codigo_dependencia_destino=5
								  order by 1 desc
								",
								$id);
			}elseif($this->userIntranet['COD_DEP']==16){
				$sql_st = sprintf("select r.id,r.requerimiento,r.respuesta ,
									case when r.codigo_dependencia_origen=1 then 'DM'
										 when r.codigo_dependencia_origen=5 then 'SG'
										 when r.codigo_dependencia_origen=16 then 'DVM-PE'
										 when r.codigo_dependencia_origen=36 then 'VMI'
										 when r.codigo_dependencia_origen=50 then 'DM-ASES' end,
									case when r.codigo_dependencia_destino=1 then 'DM'
										 when r.codigo_dependencia_destino=5 then 'SG'
										 when r.codigo_dependencia_destino=16 then 'DVM-PE'
										 when r.codigo_dependencia_destino=36 then 'VMI'
										 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.fecha,r.FECHA_RESPUESTA,r.flag_email,r.flag,r.codigo_dependencia_destino,
										 r.respuesta2,r.fecha2,r.flag_email2,r.respuesta3,r.fecha3,r.flag_email3,
										 r.respuesta4,r.fecha4,r.flag_email4
									 from requerimiento r
								WHERE r.id_accion=%d and r.codigo_dependencia_destino=16 
								  order by 1 desc
								",
								$id);
			}elseif($this->userIntranet['COD_DEP']==36){
				$sql_st = sprintf("select r.id,r.requerimiento,r.respuesta ,
									case when r.codigo_dependencia_origen=1 then 'DM'
										 when r.codigo_dependencia_origen=5 then 'SG'
										 when r.codigo_dependencia_origen=16 then 'DVM-PE'
										 when r.codigo_dependencia_origen=36 then 'VMI'
										 when r.codigo_dependencia_origen=50 then 'DM-ASES' end,
									case when r.codigo_dependencia_destino=1 then 'DM'
										 when r.codigo_dependencia_destino=5 then 'SG'
										 when r.codigo_dependencia_destino=16 then 'DVM-PE'
										 when r.codigo_dependencia_destino=36 then 'VMI'
										 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.fecha,r.FECHA_RESPUESTA,r.flag_email,r.flag,r.codigo_dependencia_destino,
										 r.respuesta2,r.fecha2,r.flag_email2,r.respuesta3,r.fecha3,r.flag_email3,
										 r.respuesta4,r.fecha4,r.flag_email4
									 from requerimiento r
								WHERE r.id_accion=%d and r.codigo_dependencia_destino=36 
								  order by 1 desc
								",
								$id);
			}else{
				$sql_st = sprintf("select r.id,r.requerimiento,r.respuesta ,
									case when r.codigo_dependencia_origen=1 then 'DM'
										 when r.codigo_dependencia_origen=5 then 'SG'
										 when r.codigo_dependencia_origen=16 then 'DVM-PE'
										 when r.codigo_dependencia_origen=36 then 'VMI'
										 when r.codigo_dependencia_origen=50 then 'DM-ASES' end,
									case when r.codigo_dependencia_destino=1 then 'DM'
										 when r.codigo_dependencia_destino=5 then 'SG'
										 when r.codigo_dependencia_destino=16 then 'DVM-PE'
										 when r.codigo_dependencia_destino=36 then 'VMI'
										 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.fecha,r.FECHA_RESPUESTA,r.flag_email,r.flag,r.codigo_dependencia_destino,
										 r.respuesta2,r.fecha2,r.flag_email2,r.respuesta3,r.fecha3,r.flag_email3,
										 r.respuesta4,r.fecha4,r.flag_email4
									 from requerimiento r
								WHERE r.id_accion=%d 
								  order by 1 desc
								",
								$id);
			}
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('Exp',array('id' => $row[0],
										   'req' => ucfirst($row[1]),
										   'rpta' => ucfirst($row[2]),
										   'origen' => $row[3],
										   'destino' => $row[4],
										   'fecha1' => $row[5],
										   'fecR' => $row[6],
										   'flagEmail' => $row[7],
										   'flag' => $row[8],
										   'coddepDestino' => $row[9],
										   //'html' => $this->GeneraHtml($row[0])
										   'rpta2' => $row[10],
										   'fecR2' => $row[11],
										   'flagEmail2' => $row[12],
										   'rpta3' => $row[13],
										   'fecR3' => $row[14],
										   'flagEmail3' => $row[15],
										   'rpta4' => $row[16],
										   'fecR4' => $row[17],
										   'flagEmail4' => $row[18]										   										   
										   										   
										   ));
			$rs->Close();
		}
		unset($rs);
		
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('FechaActual',ucfirst(strftime("%A, %d de %B del %Y a las ")));
		
			//Para sacar la hora del Servidor SQL
			$sqlH="select substring(convert(varchar,getDate(),108),1,8)";
			$rsH = & $this->conn->Execute($sqlH);
			unset($sqlH);
			if (!$rsH)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rsH->fields[0];
				$html->assign_by_ref('HoraActual',$hora);
			}
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		//$html->display('oad/tramite/showDetallesDoc.tpl.php');
			$print ? $html->display('oad/tramite/showDetalles/printDetallesDoc.tpl.php') : $html->display('altaDireccion/reportesDataMining/showDetallesDoc.tpl.php');
	}
	
	function FechaActual(){
		setlocale (LC_TIME, $this->zonaHoraria);
		return ucfirst(strtolower(strftime("%d/%m/%Y")));
	}
	function HoraActual(){
		$this->abreConnDB();
//		$this->conn->debug = true;

		$sql="select convert(varchar,getDate(),108)";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rs->fields[0];
			}
		//setlocale (LC_TIME, $this->zonaHoraria);
		//return ucfirst(strtolower(strftime("%T")));	
		return($hora);
	}								
	
	function FormTransfiereDocAltaDir($trabajador=NULL,$asunto=NULL,$observaciones=NULL,$errors=false){
		global $list;
		global $tel;
		global $ids;//cuando vamos a delegar varios id's
		global $rr;
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $desFechaIni;
		global $anyo3;
		global $nroOTD,$RZ;
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft,$idObs;
		global $med;
		global $etapa;
		global $idProceso,$idDocumento,$Buscar;
		global $idCondNew,$idCond,$correlativo;
		global $codDistJud,$codSede;
		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $radio3;	
		global $inicio;
		global $GrupoOpciones1,$nroTD,$tipoDocc,$numero2,$anyo2,$siglasDepe2,$Buscar,$idDocumento;	
		/*
		if(empty($id)&&empty($ids)&&empty($idExpxResol)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		*/
		$idDoc = $ids[0];
		$a=count($ids);
		
		//echo "numero: ".$a;
				
		//$this->abreConnDB();
		//$this->conn->debug = true;
		/*
		for($i=0;$i<$a;$i++){
			
			$idA=$ids[$i];
			$sql="select id_documento, case when id_tipo_documento=1 then num_tram_documentario
		     								when id_tipo_documento=2 then num_tram_documentario
											when id_tipo_documento=4 then indicativo_oficio end,
						from documento
						where id_documento=$idA
			 ";
		}	 
			*/ 
		
		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmTransfiereDocAltaDir';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		if($GrupoOpciones1==1){
			if($Buscar==1&&$nroTD!=""){
				$this->abreConnDB();
				
					if($anyo3>0){
						$nroOTD=$nroTD."-".$anyo3;
					}else{
						$nroOTD=$nroTD."-";
					}

					$sql="select d.id_documento,d.num_tram_documentario,d.indicativo_oficio,
								 case when d.id_tipo_documento=1 then d.asunto
									  when d.id_tipo_documento=2 then tup.descripcion end,
								 d.observaciones,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
						  from movimiento_documento md,documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
						  where 
							   d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0 and
							   md.id_dependencia_destino=5 and 
							   d.num_tram_documentario like '%{$nroOTD}%'	 
						 ";
					 
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDocumento=$rs->fields[0];
					$numTram=$rs->fields[1];
					$ind=$rs->fields[2];
					$asunto2=$rs->fields[3];
					$obs=$rs->fields[4];
					$fecRec=$rs->fields[5];
					if($idDocumento>0){
						$exito=1;
					}else{
						$exito=0;
					}
				}
			}
		}elseif($GrupoOpciones1==2){
			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
					      and d.indicativo_oficio like '%{$var}'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 and
										      md.derivado=0 and md.finalizado=0 and md.id_dependencia_destino=5
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				
			}//fin del if($Buscar)
		}//fin del if($GrupoOpciones1)		
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('dev',$dev);
		$html->assign_by_ref('ids',$ids);
		
		
		$html->assign_by_ref('opciones1',$opciones1);
		
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('tipoDocc',$tipoDocc);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('siglasDepe2',$siglasDepe2);
		$html->assign_by_ref('idDocumento',$idDocumento);
		
		/*para que se retorne a la b�squeda anterior*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones2',$observaciones2);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo2',$indicativo2);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);		/**/
		$html->assign_by_ref('codTrabJ',$this->userIntranet['CODIGO']);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('idCond',$idCond);
		$html->assign_by_ref('idObs',$idObs);
		$html->assign_by_ref('radio3',$radio3);
		$html->assign_by_ref('inicio',$inicio);

		// Contenido Select Trabajador
		if ($dev=="T"){
			$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
					  "FROM db_general.dbo.h_trabajador ".
					  "WHERE coddep=" . $this->userIntranet['COD_DEP'] ." AND ESTADO='ACTIVO' ".
					 // "AND codigo_trabajador=". $trabajador ." ".
					 "and (director<>7 or director is null) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true));
			unset($sql_st);
		}else{
		$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
				  "FROM db_general.dbo.h_trabajador ".
				  "WHERE coddep=" . $this->userIntranet['COD_DEP'] ." AND ESTADO='ACTIVO' ".
				  "and condicion<>'GENERAL' AND codigo_trabajador<>".$this->userIntranet['CODIGO'] ." ".
				  "and (director<>7 or director is null) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT id_clase_documento_interno, substring(descripcion,1,23) ".
						  "FROM dbo.clase_documento_interno ".
						  "where procedencia='I' and categoria='D' ".
						  "and id_clase_documento_interno not in (55) ".
						  "ORDER BY 2";
		$html->assign_by_ref('selTipoDocc',$this->ObjFrmSelect($sql_st, $tipoDocc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT siglas, substring(siglas,1,9) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Seleccione')));				
		unset($sql_st);
		
		if($idDocumento>0){
		// Contenido Select del Asunto para DINSECOVI
		$sql_st = "SELECT id_documento, case when id_tipo_documento=1 then num_tram_documentario ".
				  "when id_tipo_documento=2 then num_tram_documentario ".
				  "when id_tipo_documento=4 then indicativo_oficio end ".
				  "FROM documento ".
				  "WHERE id_documento=$idDocumento ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RZ, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		if(!$inicio){
			
			$this->abreConnDB();
			$sql="select id from accion where flag=1";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
							$ii=-1;
							while(!$rs->EOF){
							$ii++;
							$ids[$ii]=$rs->fields[0];
							$rs->MoveNext();
							}
				}		
			//echo "xxx".count($ids);	
		}
		// Lista el Software ya Agregado
		//$idSoft = $ids;
		$ids = (is_array($ids)) ? $ids : array();
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		$idObs = (is_array($idObs)) ? $idObs : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoftNew) array_push($ids, $idSoftNew);
		
		//echo "x".count($ids)."x";
		unset($tramaIds);
		if($ids&&count($ids)>0){
			for($i=0;$i<count($ids);$i++){
				if(!empty($ids[$i])&&!is_null($ids[$i])){
					if($i==0){
						//echo "<br>-PP".$ids[$i];
						$tramaIds=$ids[$i];
					}else{
						if($tramaIds && $tramaIds!="")
							$tramaIds=$tramaIds.",".$ids[$i];
						else
							$tramaIds=$ids[$i];
					}	
				}
			}
		}
		if($ids&&count($ids)>0){
			if($tramaIds && $tramaIds!=""){
				//$sql_SP = "select id from accion where FLAG=1 and id not in ($tramaIds)";
				$sql_SP = "delete from accion where FLAG=1 and id not in ($tramaIds)";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					/*if($row = $rs->FetchRow()){
						echo "<br>Id eliminado: ".$row[0];
					}*/
				}
			}		
		}
		
		if($ids&&count($ids)>0){
			$this->abreConnDB();
			for($i=0;$i<count($ids);$i++){
				if(!empty($ids[$i])&&!is_null($ids[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,id_documento,case when id_tipo_documento=1 then 'EXTERNO'
					                                               when id_tipo_documento=2 then 'EXPEDIENTE'
																   when id_tipo_documento=4 then 'INTERNO' end,
											  fecha_recepcion,registro_documento,resumen,nro_memorando,DERIVACION,remitente
				  						FROM accion
										where id=%d",
									  $this->PrepareParamSQL($ids[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow()){
							$ruta="/var/www/intranet/institucional/aplicativos/oad/sitradocV2/archivos/".$row[1].".pdf";
							$valRuta=file_exists($ruta);
							$html->append('soft', array('id' => $ids[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'nroDoc' =>$row[2],
														'asu' =>$row[3],
														'fec' =>$row[4],
														'obs' => $row[5],
														'nroMemo' => $row[6],
														'der' => $row[7],
														'remi' => $row[8],
														'valRuta' => $valRuta
														));
						}								
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}
		

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);
//echo "xxx";
		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
//echo "yyy";				
		$html->display('altaDireccion/frmTransf.tpl.php');
//echo "xxx";		
		$html->display('oad/footerArm.tpl.php');	
	}			
	
	function TransfiereDocAltaDir(){
	
		global $ids,$idObs,$radio3;
		global $bSubmit,$bSubmit2;
		
		if($bSubmit){$opcion=2;}//Solo guardar y no enviar al SG
		if($bSubmit2){$opcion=1;}//Enviar al SG
		
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
						
				if ($ids){
					$ss=count($ids);
							for($q=0;$q<$ss;$q++){
								$sql_SP = sprintf("EXECUTE sp_guardaSG %d,'%s',%d,'%s',%d",
													$ids[$q],
													($idObs[$q]) ? $idObs[$q] : "NULL",
													$this->userIntranet['COD_DEP'],
													$_SESSION['cod_usuario'],
													$opcion
												  );
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
									$rs->Close();
								}
								unset($rs);
							}
					//$idDoc = $ids[0];
				}		
										
		if($radio3==1||$opcion==1){	
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}";
			for($q=0;$q<$ss;$q++){
				$destination .= "&ids[]={$ids[$q]}";
			}
			header("Location: $destination");
			exit;
		}else{
						$html = new Smarty;
						$html->assign_by_ref('msjAlerta',$msjAlerta);
						//echo "holas";
						$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['FRM_TRANSFIERE_DOC_ALTADIR'] . '&tipDocumento=3&tipBusqueda=1';
						//echo $destination;
						header('Location: ' . $destination);
		
		}	
	}
	
	function FormTransfiereDocAltaDir2($trabajador=NULL,$asunto=NULL,$observaciones=NULL,$errors=false){
		global $list;
		global $tel;
		global $ids;//cuando vamos a delegar varios id's
		global $rr;
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $desFechaIni;
		global $anyo3;
		global $nroOTD,$RZ;
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft,$idObs;
		global $med;
		global $etapa;
		global $idProceso,$idDocumento,$Buscar;
		global $idCondNew,$idCond,$correlativo;
		global $codDistJud,$codSede;
		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $radio3;	
		global $inicio;
		global $GrupoOpciones1,$nroTD,$tipoDocc,$numero2,$anyo2,$siglasDepe2,$Buscar,$idDocumento;	
		/*
		if(empty($id)&&empty($ids)&&empty($idExpxResol)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		*/
		$idDoc = $ids[0];
		$a=count($ids);
		
		//echo "numero: ".$a;
				
		//$this->abreConnDB();
		//$this->conn->debug = true;
		/*
		for($i=0;$i<$a;$i++){
			
			$idA=$ids[$i];
			$sql="select id_documento, case when id_tipo_documento=1 then num_tram_documentario
		     								when id_tipo_documento=2 then num_tram_documentario
											when id_tipo_documento=4 then indicativo_oficio end,
						from documento
						where id_documento=$idA
			 ";
		}	 
			*/ 
		
		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmTransfiereDocAltaDir2';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		if($GrupoOpciones1==1){
			if($Buscar==1&&$nroTD!=""){
				$this->abreConnDB();
				
					if($anyo3>0){
						$nroOTD=$nroTD."-".$anyo3;
					}else{
						$nroOTD=$nroTD."-";
					}

					$sql="select d.id_documento,d.num_tram_documentario,d.indicativo_oficio,
								 case when d.id_tipo_documento=1 then d.asunto
									  when d.id_tipo_documento=2 then tup.descripcion end,
								 d.observaciones,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
						  from movimiento_documento md,documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
						  where 
							   d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0 and
							   md.id_dependencia_destino=5 and 
							   d.num_tram_documentario like '%{$nroOTD}%'	 
						 ";
					 
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDocumento=$rs->fields[0];
					$numTram=$rs->fields[1];
					$ind=$rs->fields[2];
					$asunto2=$rs->fields[3];
					$obs=$rs->fields[4];
					$fecRec=$rs->fields[5];
					if($idDocumento>0){
						$exito=1;
					}else{
						$exito=0;
					}
				}
			}
		}elseif($GrupoOpciones1==2){
			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
					      and d.indicativo_oficio like '%{$var}'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 and
										      md.derivado=0 and md.finalizado=0 and md.id_dependencia_destino=5
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				
			}//fin del if($Buscar)
		}//fin del if($GrupoOpciones1)		
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('dev',$dev);
		$html->assign_by_ref('ids',$ids);
		
		
		$html->assign_by_ref('opciones1',$opciones1);
		
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('tipoDocc',$tipoDocc);
		$html->assign_by_ref('numero2',$numero2);
		$html->assign_by_ref('anyo2',$anyo2);
		$html->assign_by_ref('siglasDepe2',$siglasDepe2);
		$html->assign_by_ref('idDocumento',$idDocumento);
		
		/*para que se retorne a la b�squeda anterior*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones2',$observaciones2);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo2',$indicativo2);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);		/**/
		$html->assign_by_ref('codTrabJ',$this->userIntranet['CODIGO']);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('idCond',$idCond);
		$html->assign_by_ref('idObs',$idObs);
		$html->assign_by_ref('radio3',$radio3);
		$html->assign_by_ref('inicio',$inicio);

		// Contenido Select Trabajador
		if ($dev=="T"){
			$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
					  "FROM db_general.dbo.h_trabajador ".
					  "WHERE coddep=" . $this->userIntranet['COD_DEP'] ." AND ESTADO='ACTIVO' ".
					 // "AND codigo_trabajador=". $trabajador ." ".
					 "and (director<>7 or director is null) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true));
			unset($sql_st);
		}else{
		$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador)+' '+lower(nombres_trabajador) ".
				  "FROM db_general.dbo.h_trabajador ".
				  "WHERE coddep=" . $this->userIntranet['COD_DEP'] ." AND ESTADO='ACTIVO' ".
				  "and condicion<>'GENERAL' AND codigo_trabajador<>".$this->userIntranet['CODIGO'] ." ".
				  "and (director<>7 or director is null) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT id_clase_documento_interno, substring(descripcion,1,23) ".
						  "FROM dbo.clase_documento_interno ".
						  "where procedencia='I' and categoria='D' ".
						  "and id_clase_documento_interno not in (55) ".
						  "ORDER BY 2";
		$html->assign_by_ref('selTipoDocc',$this->ObjFrmSelect($sql_st, $tipoDocc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select del tipo de documento
		$sql_st = "SELECT siglas, substring(siglas,1,9) ".
						  "FROM db_general.dbo.h_dependencia ".
						  "where condicion='ACTIVO' ".
						  "ORDER BY 2";
		$html->assign_by_ref('selSiglasDep2',$this->ObjFrmSelect($sql_st, $siglasDepe2, true, true, array('val'=>'none','label'=>'Seleccione')));				
		unset($sql_st);
		
		if($idDocumento>0){
		// Contenido Select del Asunto para DINSECOVI
		$sql_st = "SELECT id_documento, case when id_tipo_documento=1 then num_tram_documentario ".
				  "when id_tipo_documento=2 then num_tram_documentario ".
				  "when id_tipo_documento=4 then indicativo_oficio end ".
				  "FROM documento ".
				  "WHERE id_documento=$idDocumento ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RZ, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		if(!$inicio){
			
			$this->abreConnDB();
			$sql="select id from accion where flag=3";
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
							$ii=-1;
							while(!$rs->EOF){
							$ii++;
							$ids[$ii]=$rs->fields[0];
							$rs->MoveNext();
							}
				}		
			//echo "xxx".count($ids);	
		}
		// Lista el Software ya Agregado
		//$idSoft = $ids;
		$ids = (is_array($ids)) ? $ids : array();
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		$idObs = (is_array($idObs)) ? $idObs : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoftNew) array_push($ids, $idSoftNew);
		if($ids&&count($ids)>0){
			$this->abreConnDB();
			for($i=0;$i<count($ids);$i++){
				if(!empty($ids[$i])&&!is_null($ids[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,id_documento,case when id_tipo_documento=1 then 'EXTERNO'
					                                               when id_tipo_documento=2 then 'EXPEDIENTE'
																   when id_tipo_documento=4 then 'INTERNO' end,
											  fecha_recepcion,registro_documento,resumen,nro_memorando,DERIVACION,remitente
				  						FROM accion
										where id=%d",
									  $this->PrepareParamSQL($ids[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow()){
							$ruta="/var/www/intranet/institucional/aplicativos/oad/sitradocV2/archivos/".$row[1].".pdf";
							$valRuta=file_exists($ruta);						
							$html->append('soft', array('id' => $ids[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'nroDoc' =>$row[2],
														'asu' =>$row[3],
														'fec' =>$row[4],
														'obs' => $row[5],
														'nroMemo' => $row[6],
														'der' => $row[7],
														'remi' => $row[8],
														'valRuta' => $valRuta
														));
						}								
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);
//echo "xxx";
		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
//echo "yyy";				
		$html->display('altaDireccion/frmTransf2.tpl.php');
//echo "xxx";		
		$html->display('oad/footerArm.tpl.php');	
	}
	
	function GeneraExcel($print2){
		global $ids;
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		//echo "los ids".count($ids);exit;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
			/*
			$sql_st = "select a.id,convert(varchar,d.auditmod,103),a.remitente,
				 registro_documento,resumen,nro_memorando,derivacion,case when a.id_tipo_documento=1 then 'EXTERNO'
				                                                          when a.id_tipo_documento=2 then 'EXPEDIENTE'
																		  when a.id_tipo_documento=4 then 'INTERNO' END	
				 from documento d left join db_general.dbo.persona sol on d.id_persona=sol.id, accion a
				 where a.flag=3 and a.id_documento=d.id_documento
				 ";
							
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('id' => $row[0],
										   'fecRec' => $row[1],
										   'remi' => $row[2],
										   'regDoc' => $row[3],
										   'resumen' => $row[4],
										   'nroMemo' => $row[5],
										   'der' => $row[6],
										   'tipDoc' => $row[7]
										   ));
			$rs->Close();
		}
		unset($rs);
		*/
		if($ids&&count($ids)>0){
			$this->abreConnDB();
			for($i=0;$i<count($ids);$i++){
				if(!empty($ids[$i])&&!is_null($ids[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("select a.id,convert(varchar,d.auditmod,103),a.remitente,
				 registro_documento,resumen,nro_memorando,derivacion,case when a.id_tipo_documento=1 then 'EXTERNO'
				                                                          when a.id_tipo_documento=2 then 'EXPEDIENTE'
																		  when a.id_tipo_documento=4 then 'INTERNO' END	
				 from documento d left join db_general.dbo.persona sol on d.id_persona=sol.id, accion a
				 where a.id_documento=d.id_documento and a.id=%d",$this->PrepareParamSQL($ids[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						while($row = $rs->FetchRow())
							$html->append('list',array('id' => $row[0],
													   'fecRec' => $row[1],
													   'remi' => $row[2],
													   'regDoc' => $row[3],
													   'resumen' => $row[4],
													   'nroMemo' => $row[5],
													   'der' => $row[6],
													   'tipDoc' => $row[7]
													   ));
						$rs->Close();
					}
					unset($rs);			
					/**/
				}
			}
		}						
				
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('tipDoc',$tipDoc);
		$html->assign_by_ref('dep',$dep);
		$html->assign_by_ref('tupa',$tupa);
		$html->assign_by_ref('trabajador',$trabajador);									
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);			
		
					// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			//header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
			header("Content-Disposition: attachment; filename=despachoVirtual".date('d').date('m').date('y').".csv");			
				$html->display('altaDireccion/viewExtend.tpl.php');
			exit;
	}
	
	function FormGeneraReporteNoti($search2=false){
		global $tipReporte,$notificacion;
		global $fecIniNoti,$fecFinNoti;
		global $GrupoOpciones1,$adjunto,$tipMensajeria;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		//Manipulacion de las Fechas
		$fecNotificacion = ($fecNotificacion!='//'&&$fecNotificacion) ? $fecNotificacion : date('m/Y');
		$fecIniNoti = ($fecIniNoti!='//'&&$fecIniNoti) ? $fecIniNoti : date('m/d/Y');
		$fecFinNoti = ($fecFinNoti!='//'&&$fecFinNoti) ? $fecFinNoti : date('m/d/Y');
		
		$horInicio = ($horInicio!=':'&&$horInicio) ? $horInicio : NULL;
		$horFin = ($horFin!=':'&&$horFin) ? $horFin : NULL;		
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(1));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte2';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('notificacion',$notificacion);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('adjunto',$adjunto);
		$html->assign_by_ref('coddepTrab',$this->userIntranet['COD_DEP']);

		//Mes y anyo
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecNotificacion,0,2), true, array('val'=>'none','label'=>'Todos')));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecNotificacion,3,4), true));
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniNoti,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniNoti,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecIniNoti,6,4), true, array('val'=>'none','label'=>'--------')));

		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinNoti,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinNoti,3,2), true, array('val'=>'none','label'=>'----')));
		//$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFinNoti,6,4), true, array('val'=>'none','label'=>'--------')));		
		
		//Hora de Inicio
		$html->assign('selHoraIni',$this->ObjFrmHora(0, 23, true, substr($horInicio,11,2), true,array('val'=>'none','label'=>'----')));
		$html->assign('selMinIni',$this->ObjFrmMinuto(30, true, substr($horInicio,14,2), true));		
		
		//Hora de Fin
		$html->assign('selHoraFin',$this->ObjFrmHora(0, 23, true, substr($horFin,11,2), true,array('val'=>'none','label'=>'----')));
		$html->assign('selMinFin',$this->ObjFrmMinuto(30, true, substr($horFin,14,2), true));		

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		$html->display('oad/headerArm.tpl.php');
		$html->display('altaDireccion/reportes.tpl.php');
		if(!$search2) $html->display('oad/footerArm.tpl.php');
	}	
	
	function GeneraReporteNoti($tipReporte,$notificacion,$mes,$anyo,$fecIniNoti,$fecFinNoti,$horInicio,$horFin,&$adjunto,$GrupoOpciones1,$tipMensajeria){
		//echo "archivoww ".$adjunto;
		$dependencia=$this->userIntranet['COD_DEP'];
		switch($tipReporte){
			case 1:
			$this->ReporteNotificacion($notificacion);
			break;
			case 2:
			$this->ListadoCorrespodencia($mes,$anyo,$fecIniNoti,$fecFinNoti);
			break;
			case 3:
			$this->Reporte3Notificacion($notificacion);
			break;
			case 4:
			$this->Reporte4Notificacion($notificacion);
			break;
			case 5:
			$this->Reporte5Notificacion($notificacion);
			break;
			case 6:
			$this->Reporte6Notificacion($mes,$anyo,$fecIniNoti,$fecFinNoti);
			break;
			case 7:
			$this->Reporte7Notificacion($mes,$anyo,$fecIniNoti,$fecFinNoti);
			break;
			case 8:
			$this->ListadoCorrespodenciaExcel($mes,$anyo,$fecIniNoti,$fecFinNoti,$tipReporte);
			break;
			case 9:
			$this->ListadoCorrespodenciaExcel($mes,$anyo,$fecIniNoti,$fecFinNoti,$tipReporte);
			break;
			case 10:
			$this->ListadoCorrespodenciaExcel($mes,$anyo,$fecIniNoti,$fecFinNoti,$tipReporte);
			break;
			case 11:
			$this->ListadoCorrespodenciaExcelNoti($mes,$anyo,$fecIniNoti,$fecFinNoti,$tipReporte,$horInicio,$horFin,$dependencia,$tipMensajeria);
			break;
			case 12:
			//echo "archivo ".$adjunto;
			$this->EnviaMsgCourier($GrupoOpciones1,&$adjunto);			
			break;
		}
	}				

	function FormRespondeRequerimiento($id,$observaciones=NULL,$errors=false){
		
		global $ids;//Para la finalizaci�n autom�tica
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $anyo3;
		/**/
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		/**/
		// Genera Objeto HTML
		$html = new Smarty;
//echo "holas";
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		/**/
		
				if($this->userIntranet['COD_DEP']==13||$this->userIntranet['COD_DEP']==50||$this->userIntranet['COD_DEP']==1){
									$sql_SP = "select r.id,r.id_accion,case when r.codigo_dependencia_origen=1 then 'DM'
											 when r.codigo_dependencia_origen=5 then 'SG'
											 when r.codigo_dependencia_origen=16 then 'DVM-PE'
											 when r.codigo_dependencia_origen=36 then 'VMI'
											 when r.codigo_dependencia_origen=50 then 'DM-ASES' END,
					  r.requerimiento,r.respuesta,r.flag,case when r.codigo_dependencia_destino=1 then 'DM'
											 when r.codigo_dependencia_destino=5 then 'SG'
											 when r.codigo_dependencia_destino=16 then 'DVM-PE'
											 when r.codigo_dependencia_destino=36 then 'VMI'
											 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.codigo_dependencia_destino,
										FLAG_EMAIL	 
											  from requerimiento r
					  where id_accion=$id";
				}else{
									$sql_SP = "select r.id,r.id_accion,case when r.codigo_dependencia_origen=1 then 'DM'
											 when r.codigo_dependencia_origen=5 then 'SG'
											 when r.codigo_dependencia_origen=16 then 'DVM-PE'
											 when r.codigo_dependencia_origen=36 then 'VMI'
											 when r.codigo_dependencia_origen=50 then 'DM-ASES' END,
					  r.requerimiento,r.respuesta,r.flag,case when r.codigo_dependencia_destino=1 then 'DM'
											 when r.codigo_dependencia_destino=5 then 'SG'
											 when r.codigo_dependencia_destino=16 then 'DVM-PE'
											 when r.codigo_dependencia_destino=36 then 'VMI'
											 when r.codigo_dependencia_destino=50 then 'DM-ASES' end,r.codigo_dependencia_destino,
										FLAG_EMAIL	 
											  from requerimiento r
					  where r.codigo_dependencia=".$this->userIntranet['COD_DEP']." and id_accion=$id";
	  
	  			}
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						while($row = $rs->FetchRow())
							$html->append('list', array('id' => $row[0],
														'idDoc' => ucwords($row[1]),
														'regDoc' =>ucwords($row[2]),
														'fecRec' => $row[3],
														'res' =>$row[4],
														'flag' =>$row[5],
														'siglas' =>$row[6],
														'codDest' =>$row[7],
														'flagMail' =>$row[8]
														));
						$rs->Close();
					}
					unset($rs);					
		/**/
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRespondeRequerimiento';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('ids',$ids);
		
		$coddep=$this->userIntranet['COD_DEP'];
		if($coddep==13){
			$coddep=5;
			$html->assign_by_ref('coddep',$coddep);
		}else{
			$html->assign_by_ref('coddep',$coddep);
		}
		
		/*para que se retorne a la b�squeda anterior*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones2',$observaciones2);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo2',$indicativo2);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);		/**/
		$html->assign_by_ref('anyo3',$anyo3);
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		
		$html->display('altaDireccion/headerArm.tpl.php');
		$html->display('altaDireccion/frmRespondeReq.tpl.php');
		$html->display('altaDireccion/footerArm.tpl.php');	
	}
	
	function RespondeRequerimiento($id,$observaciones){
	
		//echo "holas";
		global $ids;//Para la finalizaci�n autom�tica
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $fecIniDir2,$fecFinDir2;
		global $anyo3;
		// Comprueba Valores	
		if(!$observaciones){ $bobs = true; $this->errors .= 'Las observaciones deben ser especificadas<br>'; }

		if($bobs){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormRespondeRequerimiento($id,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			//echo "gggg";
			$this->abreConnDB();
			//$this->conn->debug = true;
				//Para la finalizaci�n autom�tica de documentos


						$sql_SP = sprintf("EXECUTE sp_RespondeRequerimiento %d,%s,%d,'%s'",
													$id,
												  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
												  $this->userIntranet['COD_DEP'],
												  $_SESSION['cod_usuario']
												  );
						 //echo $sql_SP; exit;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
							$rs->Close();
						}
						unset($rs);
						
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
			
			/*
						$msjAlerta=1;
						$html = new Smarty;
						$html->assign_by_ref('msjAlerta',$msjAlerta);
						//echo "holas";
						$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['BUSCA_DOCDIR'] . '&page=' . $page . '&tipDocumento=' . $tipDocumento . '&tipBusqueda=' . $tipBusqueda . '&msjAlerta=' . $msjAlerta . '&fecIniDir2=' . $fecIniDir . '&fecFinDir2=' . $fecFinDir. '&nroTD=' . $nroTD . '&asunto=' . $asunto2 . '&observaciones=' . $observaciones2 . '&siglasDep=' . $siglasDep . '&procedimiento=' . $procedimiento . '&ruc=' . $ruc . '&indicativo=' . $indicativo2 . '&RZ=' . $RZ . '&tipodDoc=' . $tipodDoc . '&menu=SumarioDir&subMenu=frmSearchDocDir' . '&anyo3=' . $anyo3;
						//echo $destination;
						header('Location: ' . $destination);

			/**/
			
		}
	}				

	function FormGeneraRespuesta($id,$observaciones=NULL,$errors=false){
		
		global $ids;//Para la finalizaci�n autom�tica
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $anyo3,$radio2;
		/*
		if(empty($idMemo)&&empty($ids)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		*/
		// Genera Objeto HTML
		$html = new Smarty;
//echo "holas";
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		if($id>0){
					$sql="select respuesta from requerimiento where id=$id";
					$rs = & $this->conn->Execute($sql);
					unset($sql);
					if (!$rs)
						$RETVAL=1;
					else{
						$observaciones=$rs->fields[0];
					}
		}	
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraRespuesta';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('ids',$ids);
		
		/*para que se retorne a la b�squeda anterior*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones2',$observaciones2);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo2',$indicativo2);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);		/**/
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('radio2',$radio2);
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		
		$html->display('oad/headerArm.tpl.php');
		$html->display('altaDireccion/frmRespondeRequerimiento.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}

	function GeneraRespuesta($id,$observaciones){
	
		//echo "holas";
		global $ids;//Para la finalizaci�n autom�tica
		
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $fecIniDir2,$fecFinDir2;
		global $anyo3,$radio2;
		// Comprueba Valores	
		if(!$observaciones){ $bobs = true; $this->errors .= 'Las observaciones deben ser especificadas<br>'; }

		if($bobs){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormGeneraRespuesta($id,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			//echo "gggg";
			$this->abreConnDB();
			//$this->conn->debug = true;
				//Para la finalizaci�n autom�tica de documentos
						$sql="select id_accion from requerimiento where id=$id";
						$rs = & $this->conn->Execute($sql);
						unset($sql);
						if (!$rs)
							$RETVAL=1;
						else{
							$idAccion=$rs->fields[0];
						}
						$sql_SP = sprintf("EXECUTE RESPONDEREQUERIMIENTO %d,%s,%d,'%s'",
													$id,
												  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
												  $this->userIntranet['COD_DEP'],
												  $_SESSION['cod_usuario']
												  );
						 //echo $sql_SP; exit;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
							$rs->Close();
						}
						unset($rs);
					if($radio2==1){	
						$sql2="update requerimiento set flag_email=1 where id=$id";
						$rs = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs)
							$RETVAL=1;
						else{
							$rs->Close();
						}
					}						
						
			if($radio2==1){
				$mail = & new htmlMimeMail();
		
				$desTitulo = "RESPUESTA A REQUERIMIENTO";
		//$desMensaje = $html->fetch('oad/tramite/sumarioDirTxt.tpl.php');
				$desMensaje = $observaciones;
					
				$eFrom = "sitradoc@" . $this->emailDomain;
					$eDest = "dbo@CONVENIO_SITRADOC.gob.pe";
				//$eDest = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;		
				// Envia el correo de Texto plano con el Adjunto
				$mail = & new htmlMimeMail();
				$mail->setSubject($desTitulo);
				$mail->setText($desMensaje);
		
				
				$mail->setFrom($eFrom);
					$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,lflores@CONVENIO_SITRADOC.gob.pe,jnoborikawa@CONVENIO_SITRADOC.gob.pe');
				//$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe,psolorzano@CONVENIO_SITRADOC.gob.pe,otd@CONVENIO_SITRADOC.gob.pe,cgiron@CONVENIO_SITRADOC.gob.pe,mdioses@CONVENIO_SITRADOC.gob.pe,hcruzado@CONVENIO_SITRADOC.gob.pe');
				$mail->setReturnPath($eFrom);
				$result = $mail->send(array($eDest));

			
			}			
						
			/*
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			header("Location: $destination");
			exit;
			*/
			/**/
						$msjAlerta=1;
						$html = new Smarty;
						$html->assign_by_ref('msjAlerta',$msjAlerta);
						//echo "holas";
						$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['FRM_RESPONDE_REQUERIMIENTO'] . '&id=' . $idAccion;
						//echo $destination;
						header('Location: ' . $destination);

			/**/
			
		}
	}
	
	function FormAgregaRequerimiento($id,$observaciones=NULL,$errors=false){
		
		global $ids;//Para la finalizaci�n autom�tica
		global $coddep;
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $anyo3,$radio2;
		global $dia_ini,$mes_ini,$anyo_ini;
		/*
		if(empty($idMemo)&&empty($ids)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		*/
		// Genera Objeto HTML
		$html = new Smarty;
//echo "holas";
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir) ? $fecIniDir : date('m/d/Y');		
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddRequerimiento';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('ids',$ids);
		
		/*para que se retorne a la b�squeda anterior*/
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('asunto2',$asunto2);
		$html->assign_by_ref('observaciones2',$observaciones2);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('fecIniDir',$fecIniDir);
		$html->assign_by_ref('fecFinDir',$fecFinDir);
		$html->assign_by_ref('indicativo2',$indicativo2);
		$html->assign_by_ref('siglasDep',$siglasDep);
		$html->assign_by_ref('tipodDoc',$tipodDoc);
		$html->assign_by_ref('page',$page);		/**/
		$html->assign_by_ref('anyo3',$anyo3);
		$html->assign_by_ref('radio2',$radio2);
		$html->assign_by_ref('coddep',$coddep);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-11, date('Y'), substr($fecIniDir,6,4), true, array('val'=>'none','label'=>'--------')));		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		
		$html->display('oad/headerArm.tpl.php');
		$html->display('altaDireccion/frmAddRequerimiento.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	function AgregaRequerimiento($id,$observaciones){
		
		//echo "holas";
		global $ids;//Para la finalizaci�n autom�tica
		global $coddep;
		global $tipDocumento,$tipBusqueda,$nroTD,$asunto2,$observaciones2,$procedimiento,$fecIniDir,$fecFinDir,$indicativo2,$siglasDep,$tipodDoc,$page;
		global $fecIniDir2,$fecFinDir2;
		global $anyo3,$radio2;
		global $dia_ini,$mes_ini,$anyo_ini;		
		// Comprueba Valores	
		if(!$observaciones){ $bobs = true; $this->errors .= 'Las observaciones deben ser especificadas<br>'; }
		
		if($bobs){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaRequerimiento($id,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			
			//echo "gggg";
			$this->abreConnDB();
			//$this->conn->Debug = true;
			
			$fecIniDir=$_POST['dia_ini']."/".$_POST['mes_ini']."/".$_POST['anyo_ini'];
			
			//$this->conn->debug = true;
				//Para la finalizaci�n autom�tica de documentos
						$sql_SP = sprintf("EXECUTE sp_insRequerimiento %d,%s,%d,%d,'%s','%s'",
													$id,
												  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
												  ($coddep>0) ? $coddep : "0",
												  $this->userIntranet['COD_DEP'],
												  $fecIniDir,
												  $_SESSION['cod_usuario']
												  );
						 //echo $sql_SP; exit;
						$rs = & $this->conn->Execute($sql_SP);
						//echo $sql_SP;
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
							$rs->Close();
						}
						unset($rs);
						//echo "holas";
						
						//exit;
			/*
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			header("Location: $destination");
			exit;
			*/
			/**/
						$msjAlerta=1;
						$html = new Smarty;
						$html->assign_by_ref('msjAlerta',$msjAlerta);
						//echo "holas";
						$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['FRM_RESPONDE_REQUERIMIENTO'] . '&id=' . $id;
						//echo $destination;
						header('Location: ' . $destination);

			/**/
			
		}
	}
	
	function MuestraCriteriosSeleccion($estado,$GrupoOpciones1,$RZ,$asunto,$fechai,$fechaf){
		global $checkbox,$checkbox2,$checkbox3,$checkbox4;
		global $checkboxx,$checkboxx2,$checkboxx3,$checkboxx4;		
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);		
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));				
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		// Contenido Clase de Documento Interno
		/**/
		$sql_st = "SELECT id_tupa, substring(descripcion,1,80) ".
				  "FROM db_general.dbo.tupa ".
				  "ORDER BY 1";
		$html->assign_by_ref('selTupa',$this->ObjFrmSelect($sql_st, $tupa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
		/**/
		// Contenido Clase de Documento Interno
		$sql_st = "SELECT codigo_trabajador, apellidos_trabajador+' '+nombres_trabajador ".
				  "FROM db_general.dbo.h_trabajador ".
				  "WHERE estado='ACTIVO' and condicion<>'GENERAL' ".
				  "AND coddep=5 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $trabajador, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);		
		
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('sit',$sit);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('fechai',$fechai);
		$html->assign_by_ref('fechaf',$fechaf);
		$html->assign_by_ref('fecVenc',$fecVenc);
		$html->assign_by_ref('asunto',$asunto);	
		$html->assign_by_ref('estado',$estado);					
		$html->assign_by_ref('checkbox',$checkbox);					
		$html->assign_by_ref('checkbox2',$checkbox2);					
		$html->assign_by_ref('checkbox3',$checkbox3);					
		$html->assign_by_ref('checkbox4',$checkbox4);					
		$html->assign_by_ref('checkboxx',$checkboxx);					
		$html->assign_by_ref('checkboxx2',$checkboxx2);					
		$html->assign_by_ref('checkboxx3',$checkboxx3);					
		$html->assign_by_ref('checkboxx4',$checkboxx4);					

		
		$print ? $html->display('oad/tramite/printDetalle.tpl.php') : $html->display('altaDireccion/reportesDataMining/showSeleccioneCriterios.tpl.php');				
		
	}	
	
	function ListadoDespachoVirtual($estado,$GrupoOpciones1,$RZ,$asunto,$fechai,$fechaf,$print){
		global $checkbox,$checkbox2,$checkbox3,$checkbox4;		
		global $checkboxx,$checkboxx2,$checkboxx3,$checkboxx4;
		// Genera HTML de Muestra
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//echo "estado: ".$estado;
		$this->abreConnDB();
		//$this->conn->debug = true;
		
			$sql_st ="select d.id_documento,case when a.id_tipo_documento=1 then 'EXTERNO'
												 when a.id_tipo_documento=2 then 'EXPEDIENTE'
												 when a.id_tipo_documento=4 then 'INTERNO' end,
									case when d.id_tipo_documento=1 then d.asunto
			                                                  when d.id_tipo_documento=2 then tup.descripcion
															  when d.id_tipo_documento=4 then d.asunto end,
					  a.registro_documento,a.remitente,a.fecha_recepcion,a.resumen,a.nro_memorando,a.derivacion,
					  convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
				 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),dep.siglas,
				 t.apellidos_trabajador+' '+t.nombres_trabajador,
				 case when md.finalizado=0 then 'ACTIVO'
				      when md.finalizado=1 then 'FINALIZADO' end,
				 flag_sec,flag_vp,flag_vi,flag_jg
				from documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa,
     					movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO',
						db_general.dbo.h_dependencia dep,accion a
				WHERE d.id_documento=md.id_documento and md.derivado=0 /*and md.finalizado=0*/
					and md.id_dependencia_destino=dep.codigo_dependencia
					and a.id_documento=d.id_documento 
								                
							";
			if($estado==1){$sql_st.="  ";}//CUALQUIER ESTADO EN SITRADOC
			elseif($estado==2){$sql_st.=" and md.finalizado=0 ";}//PENDIENTE EN SITRADOC
			elseif($estado==3){$sql_st.=" and md.finalizado=1 ";}//FINALIZADOS EN SITRADOC
			elseif($estado==4){$sql_st.=" and md.finalizado=0 and convert(datetime,d.fecha_max_plazo,103)<getDate() ";}//VENCIDOS Y PENDIENTES EN SITRADOC			
			
			if(!empty($RZ)&&!is_null($RZ)){//PARA EL REMITENTE
				$sql_st.=" and Upper(a.remitente) LIKE Upper('%{$RZ}%')  ";
			}
			if(!empty($asunto)&&!is_null($asunto)){
				$sql_st.=" and (Upper(d.asunto) LIKE Upper('%{$asunto}%') ) ";
			}
			if(!empty($fechai)&&!is_null($fechai)&&!empty($fechaf)&&!is_null($fechaf)){//FECHAS DE DESPACHO
				$sql_st.=" and a.fecha_despacho,103>convert(datetime,'$fechai',103) and a.fecha_despacho<dateadd(dd,1,convert(datetime,'$fechaf',103)) ";
			}
			
			if(($checkbox==1||$checkbox2==2||$checkbox4==4)&& !$checkbox3){//DOCUMENTOS EXTERNOS
				
				$sql_st.=" and (";
				if($checkbox==1){$mm[]="a.id_tipo_documento=1";}
				if($checkbox2==2){$mm[]="a.id_tipo_documento=2";}
				if($checkbox4==4){$mm[]="a.id_tipo_documento=4";}
				$sql_st.= (count($mm)>0) ? ' '.implode(' or ',$mm) : '';
				
				$sql_st.=" ) ";
			}
			
			if(($checkboxx==1||$checkboxx2==2||$checkboxx3==3)&& !$checkboxx4){//DOCUMENTOS EXTERNOS
				
				$sql_st.=" and (";
				if($checkboxx==1){$nn[]="md.finalizado=0";}
				if($checkboxx2==2){$nn[]="md.finalizado=1";}
				if($checkboxx3==3){$nn[]="(md.finalizado=0 and convert(datetime,d.fecha_max_plazo,103)<getDate())";}
				$sql_st.= (count($nn)>0) ? ' '.implode(' or ',$nn) : '';
				
				$sql_st.=" ) ";
			}			
							
			$sql_st.=" order by md.audit_mod desc";
														
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('list',array('id' => $row[0],
										   'tipDoc' => $row[1],
										   'asunto' => $row[2],
										   'regDoc' => $row[3],
										   'remi' => $row[4],
										   'fecRec' => $row[5],
										   'resumen' => $row[6],
										   'nroMemo' => $row[7],
										   'der' => $row[8],
										   'fecInfP' => $row[9],
										   'fecIngDep' => $row[10],
										   'siglas' => $row[11],
										   'trab' => $row[12],
										   'estado' => $row[13],
										   'flagSG' => $row[14],
										   'flagVP' => $row[15],
										   'flagVI' => $row[16],
										   'flagJG' => $row[17]
										   ));
			$rs->Close();
		}
		unset($rs);		

		
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('tipDoc',$tipDoc);
		$html->assign_by_ref('sit',$sit);
		$html->assign_by_ref('tupa',$tupa);
		$html->assign_by_ref('trabajador',$trabajador);									
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);			
		$html->assign_by_ref('estado',$estado);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('fechai',$fechai);
		$html->assign_by_ref('fechaf',$fechaf);
		$html->assign_by_ref('checkbox',$checkbox);					
		$html->assign_by_ref('checkbox2',$checkbox2);					
		$html->assign_by_ref('checkbox3',$checkbox3);					
		$html->assign_by_ref('checkbox4',$checkbox4);					
		$html->assign_by_ref('checkboxx',$checkboxx);					
		$html->assign_by_ref('checkboxx2',$checkboxx2);					
		$html->assign_by_ref('checkboxx3',$checkboxx3);					
		$html->assign_by_ref('checkboxx4',$checkboxx4);					
		
		if($print3==1){
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';		
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'hoja1'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('oad/tramite/DinamicReports/ReportToSGandVice/showListadoDocPDF.tpl.php'),true,$logo);
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;		
		
		}
		if($print==1){
					// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('altaDireccion/reportesDataMining/viewDetallesDespachoVirtual.tpl.php');
			exit;
		}else{
			$print ? $html->display('oad/tramite/DinamicReports/ReportToSGandVice/printListadoDoc.tpl.php') : $html->display('altaDireccion/reportesDataMining/showListadoDespachoVirtual.tpl.php');				
		}
	}							

}
?>
