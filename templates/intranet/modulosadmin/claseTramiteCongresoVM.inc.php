<?
include_once('claseTramiteCongreso.inc.php');

class TramiteCongresoVM extends TramiteCongreso{

	var $statDocumento = array(NUEVOSASIGNADOS => 'D',
							          VENCIDOS => 'D');
	
	function TramiteCongresoVM($menu){
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][0];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][9];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Saca los Datos del Usuario
		$this->datosUsuarioMSSQL();
		
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		if($this->userIntranet['COD_DEP']!=16&&$this->userIntranet['COD_DEP']!=36&&$this->userIntranet['COD_DEP']!=1){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							MUESTRA_SUMARIO => 'viewSumary',
							FRM_BUSCA_DOCUMENTO => 'frmSearchDoc',
							BUSCA_DOCUMENTO => 'searchDoc',
							FRM_RESPONDE_DOCUMENTO =>'frmRespondeDocumento',
							RESPONDE_DOCUMENTO => 'respondeDocumento',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							REPORTE_SUBDOC => 'ReporteSubDoc',
							GENERAREPORTE => 'GeneraReporte',							
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSumario', label => 'SUMARIO' ),
							1 => array ( 'val' => 'frmSearchDoc', label => 'BUSCAR' ),
		                    2 => array ( 'val' => 'frmRespondeDocumento', label => 'RESPONDER' ),
							3 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTES')
							);
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		$this->pathTemplate = 'dvm/tramite/';
		
    }
	
	function CantidadDocumentos($stat,$i){
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql_st = sprintf("SELECT COUNT(*) ".
						  //"FROM dbo.documento ".
						  "FROM dbo.documento d, dbo.documento_h_dependencia ddep ".
						  "WHERE d.flag='{$stat}' and d.id_documento=ddep.id_documento %s%s",
						  (($this->statDocumento[NUEVOSASIGNADOS]==$stat)&&($i==1)) ? "and d.fecha_max_resp >= getDate() and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'] : "",
						  (($this->statDocumento[VENCIDOS]==$stat)&&($i==2)) ? "and d.fecha_max_resp < getDate() and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'] : "");
							//$this->userIntranet['COD_DEP']==16 ? " AND ddep.codigo_dependencia=16" : " ",
							//$this->userIntranet['COD_DEP']==36 ? " AND ddep.codigo_dependencia=36" : " ");
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$row = $rs->FetchRow();
			$rs->Close();
		}
		
		return $row[0];	
	}
	
	function MuestraSumario(){
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('statDocumento',$this->statDocumento);
		
		$html->assign('nAsignados',$this->CantidadDocumentos($this->statDocumento[NUEVOSASIGNADOS],1));
		$html->assign('nVencidos',$this->CantidadDocumentos($this->statDocumento[VENCIDOS],2));
		
		$html->display($this->pathTemplate . 'index.tpl.php');
	}
	
	function MuestraIndex(){
		$this->MuestraSumario();
	}
		
	function FormRespondeDocumento($idDocumento,$doc_rpta=NULL,$fecRespuesta=NULL,$codDependencia=NULL,$documento=NULL,$fecha=NULL,$errors=false){
		
		if(!$idDocumento){
				$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
				return;
			}
		//Manipulacion de las Fechas
		$fecha = ($fecha!='//'&&$fecha) ? $fecha : date('m/d/Y');
		$fecRespuesta = ($fecRespuesta!='//'&&$fecRespuesta) ? $fecRespuesta : NULL;
		
		/*ESTA ES UNA PRUEBA PARA VER SI TENEMOS YA DATOS INGRESADOS EN LAS TABLAS SUB_DOCUMENTO_H_DEPENDENCIA Y C�DIGO_DEPENDENCIA*/
		// Obtiene Datos del Contacto
		//if(!$reLoad){
		
			$this->abreConnDB();
			//$this->conn->debug = true;
			//
			// Obtiene el Id del SubDocumento
			$sql_SP = sprintf("EXECUTE sp_listSubDocumento %d",$this->PrepareParamSQL($idDocumento));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$idSubDocumento = $row[0];
				}
				$rs->Close();
			}
			unset($rs);
			//
			// Obtiene los Datos del Documento Derivado si es que existen
			if($idSubDocumento&&$idSubDocumento>0){

					$sql_SP = sprintf("EXECUTE sp_listDocDerivado %d,%d",
											   $this->PrepareParamSQL($idDocumento),
											   $this->PrepareParamSQL($idSubDocumento));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow()){
							$codDependencia = $row[0];
							$documento = $row[1];
							$fecha = $row[2];
							
						}
						$rs->Close();
					}
					unset($rs);
				}//fin del idSubDocumento>0
			
		//}
		/*ESTA ES LA PARTE FINAL DE VERIFICACI�N DE LOS DTOS SI ES QUE HAN SIDO INGRESADOS ANTERIORMENTE*/
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRespondeDocumento';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$idDocumento);
		$html->assign_by_ref('doc_rpta',$doc_rpta);
		$html->assign_by_ref('documento',$documento);
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, lower(dependencia) ".
				  "FROM DB_GENERAL.dbo.h_dependencia ".
				  "WHERE (codigo_dependencia!=16 and codigo_dependencia!=36 and codigo_dependencia!=5 and codigo_dependencia!=34) ".
				  "ORDER BY 2";
		
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $codDependencia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecha,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDia',$this->ObjFrmDia(1, 31, substr($fecha,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecha,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha de Respuesta
		$html->assign_by_ref('selMesRpta',$this->ObjFrmMes(1, 12, true, substr($fecRespuesta,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaRpta',$this->ObjFrmDia(1, 31, substr($fecRespuesta,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoRpta',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecRespuesta,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Valor para los codigos de Contactos a Modificar
		$html->assign_by_ref('idDocumento',$idDocumento);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmRespondeDocumento.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}

	function RespondeDocumento($idDocumento,$doc_rpta,$fecRespuesta,$codDependencia,$documento,$fecha){
		//Manipulacion de las Fechas
		$fecRespuesta = ($fecRespuesta!='//'&&$fecRespuesta&&!strstr($fecRespuesta,'none')) ? $fecRespuesta : NULL;
		$fecha = ($fecha!='//'&&$fecha&&!strstr($fecha,'none')) ? $fecha : NULL;

		// Comprueba Valores	
		//if(!$doc_rpta){ $bDRpta = true; $this->errors .= 'El N�mero de Documento debe ser especificado<br>'; }
		//if($fecRespuesta){ $bFRpta = ($this->ValidaFechaDocumento($fecRespuesta,'Respuesta')); }
		//if(!$codDependencia){ $bDependencia = true; $this->errors .= 'La Dependencia debe ser especificada<br>'; }
		//if(!$documento){ $bDocumento = true; $this->errors .= 'El Documento debe ser especificado<br>'; }
		if($fecha){ $bFecha = ($this->ValidaFechaDocumento($fecha,'Fecha'));}
        if($fecRespuesta){
				if($fecRespuesta < $fecha){ $bFechas = true; $this->errors .= 'La fecha de Respuesta debe ser mayor';}
				if($fecRespuesta > date('m/d/Y')){ $bFechas2 =true; $this->errors .= 'La fecha de Respuesta es mayor al d�a de hoy.
																					 Vuelva a ingresar los Datos de Respuesta de la Oficina';}
		}

		if($bFecha||$bFechas||$bFechas2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Proyectos de Inversi�n',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormRespondeDocumento($idDocumento,$doc_rpta,$fecRespuesta,$codDependencia,$documento,$fecha,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			/*Obtenemos el id de documentodependencia para hacer el insert en la tabla SubDocumento*/
			$sql_st = "SELECT id_documento_h_dependencia FROM dbo.documento_h_dependencia WHERE id_documento=$idDocumento";
			//echo $sql_st;exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				exit;
			else{
				$docdep = $rs->FetchRow();
				$idDocumentoDependencia = $docdep[0];
				$rs->Close();
				}
			unset($rs);
			/**/
			if ($doc_rpta=="") {
					$sql_SP = sprintf("EXECUTE sp_insSubDocumento %d,'%s',%s,'%s'",
									  $idDocumentoDependencia,
									  ($doc_rpta) ? $doc_rpta : 'NULL',
									  ($fecRespuesta) ? "'".$this->PrepareParamSQL($fecRespuesta)."'" : "NULL",
									  $_SESSION['cod_usuario']);
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);
					
					///////////
			/*Obtenemos el Id del SubDocumento que hemos ingresado*/
			//echo $idDocumento;
			$sql_st = "SELECT sd.id_subdocumento FROM dbo.sub_documento sd, dbo.documento_h_dependencia ddep, dbo.documento d 
			           WHERE d.id_documento=$idDocumento and ddep.id_documento_h_dependencia=sd.id_documento_h_dependencia
					         and ddep.id_documento=d.id_documento";
			//echo $sql_st;exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				exit;
			else{
				$subdocdep = $rs->FetchRow();
				$idSubDocumento = $subdocdep[0];
				$rs->Close();
				}
			//
			unset($rs);
							
			/**/
			/*Asignamos el SubDocumento (Insertamos los datos en la tabla Sub_Documento_H_Dependencia)*/
			$sql_SP = sprintf("EXECUTE sp_asigSubDocumento %d,%d",
							  $idSubDocumento,
							  $this->PrepareParamSQL($codDependencia));
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			/**/
			
			/*Obtenemos el id de subdocumentodependencia para hacer el insert en la tabla SubDocumento*/
			//echo $idSubDocumento;
			$sql_st = "SELECT id_subdocumento_h_dependencia FROM dbo.sub_documento_h_dependencia WHERE id_subdocumento=$idSubDocumento";
			//echo $sql_st;exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				exit;
			else{
				$subdocdepe = $rs->FetchRow();
				$idSubDocumentoDependencia = $subdocdepe[0];
				$rs->Close();
				}
			unset($rs);
			/**/
			
			/**/
			/*Insertamos en la tabla DocDerivado*/
			$sql_SP = sprintf("EXECUTE sp_insDocDerivado %d,%s,%s,'%s'",
							  $idSubDocumentoDependencia,
							  ($documento) ? "'".$this->PrepareParamSQL($documento)."'" : "NULL",
							  ($fecha) ? "'".$this->PrepareParamSQL($fecha)."'" : "NULL",
							  $_SESSION['cod_usuario']);
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			/**/
					///////////
				}
				else{
				
					///////////
					/*Obtenemos el Id del SubDocumento que hemos ingresado*/
					//echo $idDocumento;
					$sql_st = "SELECT sd.id_subdocumento FROM dbo.sub_documento sd, dbo.documento_h_dependencia ddep, dbo.documento d 
							   WHERE d.id_documento=$idDocumento and ddep.id_documento_h_dependencia=sd.id_documento_h_dependencia
									 and ddep.id_documento=d.id_documento";
					//echo $sql_st;exit;
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if (!$rs)
						exit;
					else{
						$subdocdep = $rs->FetchRow();
						$idSubDocumento = $subdocdep[0];
						$rs->Close();
						}
					//
					unset($rs);
									
					/**/
					//////////
					$sql_SP = sprintf("EXECUTE sp_insSubDocumento2 %d,%d,'%s',%s,'%s'",
					                  $idSubDocumento,
									  $idDocumentoDependencia,
									  //($doc_rpta) ? $doc_rpta : 'NULL',
									  $this->PrepareParamSQL($doc_rpta),
									  ($fecRespuesta) ? "'".$this->PrepareParamSQL($fecRespuesta)."'" : "NULL",
									  $_SESSION['cod_usuario']);
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);
				
				}
			
			if ($doc_rpta!=""){
			
					/**/
					/*Finalmente actualizamos el flag del documento*/
					$sql_st = "UPDATE dbo.documento SET flag='R' WHERE id_documento=$idDocumento";
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if (!$rs)
						exit;
					else{
						//$docdep = $rs->FetchRow();
						$rs->Close();
						}
					unset($rs);
					/**/
			}//final del if del $doc_rpta		

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;

		}
	}
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage --size 'a4' --fontsize 6.0 %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}

	function FormGeneraReporte($tipReporte=NULL,$search2=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(2));
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'reportes.tpl.php');
		if(!$search2) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
		
	function GeneraReporte($tipReporte){
		switch($tipReporte){
			case 1:
			$this->ReporteSubDoc();
			break;
		}
	}

	function ReporteSubDoc(){
		// Genera HTML
		$html = new Smarty;
		//$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$condConsulta[] = "doc.flag='D' and ddep.codigo_dependencia=" . $this->userIntranet['COD_DEP'];
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		$sql_st = sprintf("EXEC sp_reporteSubDoc %s",
					($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('subdoc',array('tram'=>$row[0],
										  'asunto'=>$row[1],
										  'fMax'=>$row[2],
										  'doc'=>$row[3],
										  'dFec'=>ucwords($row[4]),
										  'dep'=>ucwords($row[5]),
										  'ddoc'=>$row[6],
										  'fDoc'=>$row[7]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		//$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.SITRADOC.jpg';
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.SITRADOC.jpg';

		
		//$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/dvm/tramite/reports';
		$filename = 'SubDoc'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dvm/tramite/reporteSubDoc.tpl.php'),true,$logo);

		$destination = 'https://' . $_SERVER['HTTP_HOST'] . '/institucional/aplicativos/dvm/tramite/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

}
?>