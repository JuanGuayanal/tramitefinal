<?
error_reporting(E_PARSE);
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class SeguimientoMerluza extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function SeguimientoMerluza($menu,$subMenu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		//$this->serverDB = $this->arr_serverDB['mssql'][4];
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][5];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_REGISTRA_DECJUR_MENSUAL => 'frmRegDecJur',
							REGISTRA_DECJUR_MENSUAL => 'regDecJur',
							FRM_AGREGA_DECJURMENSUAL =>'frmAddDecJurMensual',
							AGREGA_DECJURMENSUAL =>'addDecJurMensual',
							FRM_BUSCA_DECJURMENSUAL =>'frmSearchDecJurMensual',
							BUSCA_DECJURMENSUAL =>'searchDecJurMensual',
							ANULA_DECJURMENSUAL => 'anulaDecJurMensual',
							FRM_GENERA_REPORTE_MERLUZA => 'frmGeneraReporteMerluza',
							GENERA_REPORTE_MERLUZA => 'generaReporteMerluza',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();
		
		
		$this->menu_items[0] = array('val' => 'frmSearchDatos', label => 'EMBARCACIONES');
		$this->menu_items[1] = array('val' => 'frmSearchArm', label => 'ARMADORES');
		$this->menu_items[2] = array('val' => 'frmReportes', label => 'REPORTES');
		$this->menu_items[3] = array('val' => 'frmRegDecJur', label => 'DECLARACI�N JURADA');
		
		
		// Menu Seleccionado
		//$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		$this->menuPager = ($menu) ? $menu : $this->menu_items[3]['val'];
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchDatos', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddEmbarcacionNac', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyEmbarcacionNac', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchArm', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddArmador', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyArmador', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[2]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmReportes', label => 'Reportes' )
										 );
		elseif($this->menuPager==$this->menu_items[3]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmRegDecJur', label => 'MENSUAL' ),
										 1 => array ( 'val' => 'frmAddImporte', label => 'IMPORTE PAGADO' ),
										 2 => array ( 'val' => 'frmSearchImporte', label => 'BUSCAR IMPORTE' ),
										 3 => array ( 'val' => 'frmModifyImporte', label => 'MODIFICA IMPORTE' ),
										 4 => array ( 'val' => 'frmReportImporte', label => 'REPORTES' )
										 );								 
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];										 
		
		$this->pathTemplate = 'dnepp/embarcacionnac/';		
    }
	
	function MuestraStatTrans($accion){

		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_REGISTRA_DECJUR_MENSUAL]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('dnepp/seguimientoMerluza/headerArmn.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormRegistraDeclaracionJuradaMensual();			
	}
	
	function formateaTexto($cadena){
	
		$cadena= ereg_replace("          "," ",$cadena);
		$cadena= ereg_replace("         "," ",$cadena);
		$cadena= ereg_replace("        "," ",$cadena);
		$cadena= ereg_replace("       "," ",$cadena);
		$cadena= ereg_replace("      "," ",$cadena);
		$cadena= ereg_replace("     "," ",$cadena);
		$cadena= ereg_replace("    "," ",$cadena);
		$cadena= ereg_replace("   "," ",$cadena);
		$cadena= ereg_replace("  "," ",$cadena);
		
		return ($cadena);
	}
	
	function formateaAcentos($cadena){
	
		$cadena= ereg_replace("�","a",$cadena);
		$cadena= ereg_replace("�","e",$cadena);
		$cadena= ereg_replace("�","i",$cadena);
		$cadena= ereg_replace("�","o",$cadena);
		$cadena= ereg_replace("�","u",$cadena);
		$cadena= ereg_replace("�","A",$cadena);
		$cadena= ereg_replace("�","E",$cadena);
		$cadena= ereg_replace("�","I",$cadena);
		$cadena= ereg_replace("�","O",$cadena);
		$cadena= ereg_replace("�","U",$cadena);
		
		return ($cadena);
	}
	
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	}	
	
	function calculaInteres($monto,$fechaInicial,$fechaPago){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$interes=$monto;
		/*$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<dateadd(dd,1,getDate()) 
					ORDER BY convert(datetime,fecha_tasa,103)";*/
		if($fechaPago && $fechaPago!="")
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<convert(datetime,'$fechaPago',103) 
					ORDER BY convert(datetime,fecha_tasa,103)";
		else
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<getDate() 
					ORDER BY convert(datetime,fecha_tasa,103)";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row=$rs->FetchRow()){
				//$interes=$interes+$interes*($row[0]/36500);
				$interes=$interes+$interes*(pow((1+($row[0]/100)),(1/360))-1);
				//echo $interes."<br>";
			}	
			$rs->Close();
		}
		unset($rs);		
		//$interes=$monto*$tasa;
		$interesTotal=$interes-$monto;
		return($interesTotal);
	}	
	
function compara_fechas($fecha1,$fecha2)
            
 
{
            
 
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("/",$fecha1);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("-",$fecha1);
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("/",$fecha2);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("-",$fecha2);
        $dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
        return ($dif);                         
            
 
}	
	
	function FormRegistraDeclaracionJuradaMensual($anyo=NULL,$RZoRUC=NULL,$BuscaRZ=NULL,$RazonSocial=NULL,$razonSocial=NULL,$direccion=NULL,$RUC=NULL,$mail=NULL,$repLegal=NULL,$docRepLegal=NULL,$idEmb=NULL,$chi=NULL,$chdB=NULL,$chdC=NULL,$cMonto=NULL,$radiobutton=NULL,$errors=false){
		global $valorEnvio,$registro,$valorGrab,$recargar;
		
		$valorEnvio=($_POST['valorEnvio']) ? $_POST['valorEnvio'] : $_GET['valorEnvio'];
				
		//echo "x".$valorEnvio."x";
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		if($recargar==1){
			unset($registro);
			unset($chi);
			unset($chdB);
			unset($chdC);
		}elseif($recargar==2){
			unset($registro);
			unset($chi);
			unset($chdB);
			unset($chdC);
			unset($RazonSocial);
		}

		
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRegDecJur';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('anyo',$anyo);
		$html->assign_by_ref('radiobutton',$radiobutton);
		$html->assign_by_ref('valorEnvio',$valorEnvio);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		if((!$BuscaRZ&&$RZoRUC!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		/*$sql_st = "SELECT id_arm, substring(razonsocial_arm,1,50) ".
				  "FROM user_dnepp.armador ".
				  "WHERE (Upper(razonsocial_arm) like Upper('%$RZoRUC%') OR Upper(ruc_arm) like Upper('%$RZoRUC%') )".
				  "ORDER BY 2";*/
		$sql_st = "SELECT id, CASE WHEN id_tipo_persona=1 THEN substring(Upper(nombres+' '+apellidos),1,80) ELSE Upper(rtrim(razon_social)) END ".
				  "FROM user_dnepp.vpersona ar ".
				  "WHERE (Upper(razon_social) like Upper('%$RZoRUC%') OR Upper(NRO_DOCUMENTO) like Upper('%$RZoRUC%') or Upper(nombres) like Upper('%$RZoRUC%') or Upper(apellidos) like Upper('%$RZoRUC%')) ".
				  //"and nro_documento<>'00000000000' ".
				  "and id in (select id_pers from db_dnepp.user_dnepp.embxpersona where estado_embxpers=1) ".
				  "ORDER BY 2";		  
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		
		$html->assign_by_ref('chi',$chi);
		$html->assign_by_ref('chdB',$chdB);
		$html->assign_by_ref('chdC',$chdC);
		$html->assign_by_ref('registro',$registro);
		$html->assign_by_ref('idEmb',$idEmb);
		$html->assign_by_ref('valorGrab',$valorGrab);
		
		switch($radiobutton){
			case "01":
				$fechaSinInteres="06/02/2008";
				$fechaConInteres="29/02/2008";
				$fechaInicialInteres="01/03/2008";
				
				$fecha1SGS="01/01/2008";
				$fecha2SGS="31/01/2008";
				//echo $this->compara_fechas(date("d/m/Y"),$fechaSinInteres);
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=1;				
				break;
			case "02":
				$fechaSinInteres="06/03/2008";
				$fechaConInteres="31/03/2008";
				$fechaInicialInteres="01/04/2008";
				
				$fecha1SGS="01/02/2008";
				$fecha2SGS="29/02/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=2;				
				break;
			case "03":
				$fechaSinInteres="06/04/2008";
				$fechaConInteres="30/04/2008";
				$fechaInicialInteres="01/05/2008";
				
				$fecha1SGS="01/03/2008";
				$fecha2SGS="31/03/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=3;				
				break;
			case "04":
				$fechaSinInteres="06/05/2008";
				$fechaConInteres="31/05/2008";
				$fechaInicialInteres="01/06/2008";
				
				$fecha1SGS="01/04/2008";
				$fecha2SGS="30/04/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=4;				
				break;
			case "05":
				$fechaSinInteres="06/06/2008";
				$fechaConInteres="30/05/2008";
				$fechaInicialInteres="01/07/2008";
				
				$fecha1SGS="01/05/2008";
				$fecha2SGS="31/05/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=5;				
				break;
			case "06":
				$fechaSinInteres="06/07/2008";
				$fechaConInteres="31/07/2008";
				$fechaInicialInteres="01/08/2008";
				
				$fecha1SGS="01/06/2008";
				$fecha2SGS="30/06/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=6;				
				break;
			case "07":
				$fechaSinInteres="06/08/2008";
				$fechaConInteres="31/08/2008";
				$fechaInicialInteres="01/09/2008";
				

				$fecha1SGS="01/07/2008";
				$fecha2SGS="31/07/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=7;				
				break;
			case "08":
				$fechaSinInteres="06/09/2008";
				$fechaConInteres="30/09/2008";
				$fechaInicialInteres="01/10/2008";
				
				$fecha1SGS="01/08/2008";
				$fecha2SGS="31/08/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=8;				
				break;
			case "09":
				$fechaSinInteres="06/10/2008";
				$fechaConInteres="31/10/2008";
				$fechaInicialInteres="01/11/2008";
				
				$fecha1SGS="01/09/2008";
				$fecha2SGS="30/09/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=9;				
				break;
			case "10":
				$fechaSinInteres="06/11/2008";
				$fechaConInteres="30/11/2008";
				$fechaInicialInteres="01/12/2008";
				
				$fecha1SGS="01/10/2008";
				$fecha2SGS="31/10/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=10;				
				break;
			case "11":
				$fechaSinInteres="06/12/2008";
				$fechaConInteres="31/12/2008";
				$fechaInicialInteres="01/01/2009";
				
				$fecha1SGS="01/11/2008";
				$fecha2SGS="30/11/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=11;				
				break;
			case "12":
				/*$fechaSinInteres="05/01/2009";
				$fechaConInteres="31/01/2008";*/
				$fechaSinInteres="06/01/2008";
				$fechaConInteres="31/01/2007";
				$fechaInicialInteres="01/02/2007";
				
				$fecha1SGS="01/12/2008";
				$fecha2SGS="31/12/2008";
				//echo date("d/m/Y",$fechaSinInteres)."-".strtotime($fechaConInteres)."-".strtotime(date("d/m/Y"));
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					//echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConInteres)>0){
					//echo "CON inter�s";
					$conInteres=1;
				}else{
					//echo "SIN intereses";					
					$sinInteres=1;
				}
				$anyoFob=2008;
				$mesFob=12;				
				break;
			default:
				$anyoFob=2008;
				$mesFob=1;				
				break;								
		}
		
		//SP Para calcular el precio promedio FOB para cada mes	
					$sql_SP = "SELECT PRECIOPROMEDIOFOB,VALCHI,PORCENTAJE_CHI,VALCHD,PORCENTAJE_CHD
				  						FROM VALOR_FOB_PAGO_DERECHO 
										where id_ANYO=$anyoFob AND ID_MES=$mesFob";
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						
						$precioPromedioFOB=$rs->fields[0];
						$valCHI=$rs->fields[1];
						$porcentajeCHI=$rs->fields[2];
						$valCHD=$rs->fields[3];
						/*$repLegal=$rs->fields[5];
						$docRepLeg=$rs->fields[6];*/
						$rs->Close();
					}
					unset($rs);
					
		//SP Para calcular el tipo de cambio el d�a de hoy
					$hoy=date("d/m/Y");
					$sql_SP = "select compra_tcdappd,venta_tcdappd
								from user_dnepp.tipo_cambio_dappd 
								where convert(varchar,fech_tcdappd,103)='".$hoy."'";
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						
						$compraDolar=$rs->fields[0];
						$ventaDolar=$rs->fields[1];
						$rs->Close();
					}
					unset($rs);		
		
		if($RazonSocial>0){
			//echo $RazonSocial;
					/*$sql_SP = sprintf("SELECT id_arm,razonsocial_arm,domicilio_arm,ruc_arm,email1_arm,
											  repleg_arm,dociderl_arm
				  						FROM user_dnepp.armador 
										where id_arm=%d",
									  $this->PrepareParamSQL($RazonSocial));*/
					$sql_SP = sprintf("SELECT id,CASE WHEN id_tipo_persona=1 THEN Upper(nombres+' '+apellidos) ELSE Upper(rtrim(razon_social)) END,
											  dIRECCION,NRO_DOCUMENTO,email,
											  REPRESENTANTE_LEGAL,NRO_DOCUMENTO_REPRESENTANTE
				  						FROM user_dnepp.vpersona 
										where id=%d",
									  $this->PrepareParamSQL($RazonSocial));				  
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						
						$razonSocial=$rs->fields[1];
						$direccion=$rs->fields[2];
						$RUC=$rs->fields[3];
						$mail=$rs->fields[4];
						$repLegal=$rs->fields[5];
						$docRepLeg=$rs->fields[6];
						$rs->Close();
					}
					unset($rs);
					
					// Obtiene los Datos de c/Embarcaci�n 
					/*$sql_SP = sprintf("select emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
										from user_dnepp.embxarm embxarm,user_dnepp.embarcacionnac emb
										where embxarm.id_arm=%d
										and embxarm.id_emb=emb.id_emb
										and embxarm.estado_embxarm=1
										and emb.codpag_emb is not null
										order by 2",
									  $this->PrepareParamSQL($RazonSocial));*/
					$sql_SP = sprintf("select emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
										from user_dnepp.vpersona ar, user_dnepp.embxpersona ea, user_dnepp.embarcacionnac emb/*, user_dnepp.resolucion r*/
										where ea.id_pers=%d
										and ea.id_emb=emb.id_emb
										and ar.id=ea.id_pers
										and ea.estado_embxpers=1
										/*and ea.id_res=r.id_res*/
										and emb.codpag_emb is not null
										order by 2",
									  $this->PrepareParamSQL($RazonSocial));				  
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
										
						$m=0;
						$montoInicial=0;
						$deducionTotal=0;
						$interesTotal=0;
						$Total=0;
						while($row = $rs->FetchRow()){
						
						$valorGrabado[$m]=0;
						/*Consulta de la tabla declaracion_jurada_mensual*/
							$sql4="select COUNT(*)
									from WEBMINPRO.DECLARACION_MERLUZA
									where id_embarcacion=".$row[0]." 
										AND MES_DJM=".$radiobutton." 
										AND ESTADO_DJM=1";
							//djm.ESTADO_DJM=1		
							$rs4 = & $this->conn->Execute($sql4);
							unset($sql4);
							if (!$rs4){
								print $this->conn->ErrorMsg();
								return;
							}else{
								$contador[$m]=$rs4->fields[0];
							}						
						
							if($contador[$m]==1){
								$sql3="select djm.CAPTOT_MERLUZA_DJM
										from WEBMINPRO.DECLARACION_MERLUZA djm
										where djm.id_embarcacion=".$row[0]." 
											AND DJM.MES_DJM=".$radiobutton." 
											AND DJM.ESTADO_DJM=1";
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$chi[$m]=$rs3->fields[0];
									$valorGrabado[$m]=1;
								}
							}									

							$html->append('emb', array('idEmb' => $row[0],
														'nomEmb' => ucwords($row[1]),
														'matEmb' =>ucwords($row[2]),
														'codPago' =>ucwords($row[3]),
														'registro' => $registro[$m],
														'chi' => $chi[$m],
														'valorGrabado' => $valorGrabado[$m]
														));
							$m=$m+1;							
						}								
						$rs->Close();
					}
					unset($rs);					
			
		}
		$html->assign_by_ref('razonSocial',$razonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('RUC',$RUC);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('montoInicial',$montoInicial);
		$html->assign_by_ref('deducionTotal',$deducionTotal);
		$html->assign_by_ref('interesTotal',$interesTotal);
		$html->assign_by_ref('Total',$Total);
		
		$html->assign_by_ref('docRepLeg',$docRepLeg);
		$html->assign_by_ref('fechaSinInteres',$fechaSinInteres);
		$html->assign_by_ref('fechaConInteres',$fechaConInteres);
		$html->assign_by_ref('compraDolar',$compraDolar);
		$html->assign_by_ref('precioPromedioFOB',$precioPromedioFOB);
		$html->assign_by_ref('valCHI',$valCHI);
		$factorCHI=(0.25/100)*$precioPromedioFOB*$compraDolar;
		$html->assign_by_ref('factorCHI',$factorCHI);
		
		$html->assign_by_ref('estado',$idEstado);
		$html->assign_by_ref('mes',date('m'));
		
		$html->assign_by_ref('errors',$errors);
		$menu=0;
		$accionHeader=0;
		$html->assign_by_ref('menu',$menu);
		$html->assign_by_ref('accionHeader',$accionHeader);
		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		//echo $chi[3].count($chi)."xx".count($idEmb).$idEmb[3];
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display('dnepp/seguimientoMerluza/headerArmn.tpl.php');
		$html->display('dnepp/seguimientoMerluza/frmRegistroDecJurMensual_bk2.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function RegistraDeclaracionJuradaMensual($anyo,$RZoRUC,$BuscaRZ,$RazonSocial,$razonSocial,$direccion,$RUC,
											  $mail,$repLegal,$docRepLegal,$idEmb,$chi,$chdB,$chdC,$cMonto,$radiobutton){
		
		global $valorGrab;
		
		if($anyo<1){ $bC2 = true; $this->errors .= 'El A�o debe ser especificado<br>'; }
		if($RazonSocial<1){ $bC3 = true; $this->errors .= 'El Armador debe ser especificado<br>'; }
		//if(!$razonSocial){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		
		if($bC2||$bC3){
			//echo "matrix";
			$objIntranet = new Intranet();
			$objIntranet->Header('Dgepp - Seguimiento de la Merluza',false,array('suspEmb'));
			$objIntranet->Body('dgepp_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormRegistraDeclaracionJuradaMensual($anyo,$RZoRUC,$BuscaRZ,$RazonSocial,$razonSocial,$direccion,$RUC,
											  			$mail,$repLegal,$docRepLegal,$idEmb,$chi,$chdB,$chdC,$cMonto,$radiobutton,
														$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = true;
			
			//echo "justomatrix".count($idEmb);exit;
			/*for($i=0;$i<count($idEmb);$i++){
				echo "<br>".$idEmb[$i]."-".$chi[$i]."-".$valorGrab[$i];
			}exit;*/
			
			for($i=0;$i<count($idEmb);$i++){

				if(($chi[$i]!="" ) ){
					//echo "<br>matrix";
						$sql_SP = sprintf("EXECUTE %s %d,%d,%d,%d,%2f,'%s'",
										  ($valorGrab[$i]==0) ? "sp_ins_DECLARACION_MERLUZA" : "sp_mod_DECLARACION_MERLUZA" ,
										  ($anyo) ? $this->PrepareParamSQL($anyo) : "2008",
										  ($radiobutton) ? $this->PrepareParamSQL($radiobutton) : "NULL",
										  ($RazonSocial) ? $this->PrepareParamSQL($RazonSocial) : "NULL",
										  $idEmb[$i],
										  $chi[$i],
										  $_SESSION['cod_usuario']
										  );
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);
					
											
				}//fin del if()
				
								  
			}//fin del for($i=0;$i<count($idEmb);$i++)			  

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->menu_items[0]['val']}";
			header("Location: $destination");
			exit;
		}											  
	
	}
	
	function FormAgregaDeclaracionJurada($matriculaEmb=NULL,$nombreEmb=NULL,$RZoRUC=NULL,$BuscaRZ=NULL,$RazonSocial=NULL,$puerto=NULL,$sistPesca=NULL,$regimen=NULL,$tipPreservacion=NULL,$casco=NULL,
								$errors=false){
		global $bSoftware,$idSoftNew,$idSoft,$idSoftNew2,$idSoft2,$bSoftware2,$idCond2,$idCondNew2,$idSoft3,$idSoftNew3;
		global $nombreDeclarante,$nroDocDeclarante,$nroTramite,$origen,$mes,$emb,$BuscaEmb,$embarcacion,$chi,$chd1,$chd2;
		global $idChi,$idChd1,$idChd2,$idChiNew,$idChd1New,$idChd2New;
		$nombrePC=($_POST['nombrePC']) ? $_POST['nombrePC'] : $_GET['nombrePC'];		
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		*/

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddDecJurMensual';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('matriculaEmb',$matriculaEmb);
		$html->assign_by_ref('nombreEmb',$nombreEmb);
		$html->assign_by_ref('eslora',$eslora);
		$html->assign_by_ref('manga',$manga);
		$html->assign_by_ref('puntual',$puntual);
		$html->assign_by_ref('capbod',$capbod);
		$html->assign_by_ref('capbodTM',$capbodTM);
		$html->assign_by_ref('capbod3p',$capbod3p);
		$html->assign_by_ref('capbod15p',$capbod15p);
		$html->assign_by_ref('transmisor',$transmisor);
		$html->assign_by_ref('marcaMotor',$marcaMotor);
		$html->assign_by_ref('modeloMotor',$modeloMotor);
		$html->assign_by_ref('serieMotor',$serieMotor);
		$html->assign_by_ref('potenciaMotor',$potenciaMotor);
		$html->assign_by_ref('arqb_emb',$arqb_emb);
		$html->assign_by_ref('arqn_emb',$arqn_emb);
		$html->assign_by_ref('nroResol',$nroResol);
		
		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('idSoft2',$idSoft2);
		$html->assign_by_ref('idSoft3',$idSoft3);		
		$html->assign_by_ref('error',$error);
		$html->assign_by_ref('nroTramite',$nroTramite);
		$html->assign_by_ref('mes',$mes);
		$html->assign_by_ref('emb',$emb);
		//$html->assign_by_ref('chi',$chi);
		//$html->assign_by_ref('chd1',$chd1);
		//$html->assign_by_ref('chd2',$chd2);
		
		$html->assign_by_ref('mesActual',date('m'));
		
		// Contenido Select del Puerto
		$sql_st = "SELECT id_orgres, lower(desc_orgres) ".
				  "FROM user_dnepp.origenres ".
				  "ORDER BY 2";
		$html->assign_by_ref('selOrigen',$this->ObjFrmSelect($sql_st, $origen, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Sistema de Pesca (Aparejo)
		$sql_st = "SELECT id_apa, lower(nombre_apa) ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSistPesca',$this->ObjFrmSelect($sql_st, $sistPesca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Regimen
		$sql_st = "SELECT id_regimen, lower(desc_regimen) ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $regimen, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Tipo de preservaci�n
		$sql_st = "SELECT id_tpres, lower(desc_tpres) ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipPreservacion',$this->ObjFrmSelect($sql_st, $tipPreservacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Casco
		$sql_st = "SELECT id_casco, lower(desc_casco) ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $casco, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select de la especie x Literal
		$sql_st = sprintf("SELECT e.codigo, lower(e.descripcion) 
				  FROM db_general.dbo.especies e,user_dnepp.literalxespecie le 
				  WHERE e.codigo=le.id_esp 
				  AND le.id_literal=%d 
				  ORDER BY 2",($literal)? $literal : "xx");
		$html->assign_by_ref('selEspecie',$this->ObjFrmSelect($sql_st, $especie, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Destino
		$sql_st = "SELECT id_destino, lower(desc_destino) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $destino, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);				
		
		if((!$BuscaRZ&&$RZoRUC!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id, case when id_tipo_persona=1 then substring(apellidos,1,50)+' '+substring(nombres,1,50) else substring(razon_social,1,50) end ".
				  "FROM user_dnepp.vpersona ".
				  "WHERE (Upper(razon_social) like Upper('%$RZoRUC%') or Upper(apellidos) like Upper('%$RZoRUC%') or Upper(nombres) like Upper('%$RZoRUC%') )".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		/**/if($RazonSocial>0){
					//$this->abreConnDB();
					//$this->conn->debug = true;
					$sql_SP = sprintf("select id, case when id_tipo_persona=1 then substring(apellidos,1,50)+' '+substring(nombres,1,50) 
												else substring(razon_social,1,50) end,
										nro_documento,direccion 
										from user_dnepp.vpersona
										where id=%d",
									  $this->PrepareParamSQL($RazonSocial));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$nombreDeclarante=$rs->fields[1];
						$nroDocDeclarante=$rs->fields[2];
						$direccionDeclarante=$rs->fields[3];
					}			
			
			$html->assign_by_ref('nombreDeclarante',$nombreDeclarante);
			$html->assign_by_ref('nroDocDeclarante',$nroDocDeclarante);
		}/**/
		
		if((!$BuscaEmb&&$emb!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "select emb.id_emb,emb.nombre_emb+' '+emb.matricula_emb+' '+convert(varchar,emb.codpag_emb)
										from user_dnepp.embarcacionnac emb
										where 
										(emb.nombre_emb like '%{$emb}%' or emb.matricula_emb like '%{$emb}%' or emb.codpag_emb like '%{$emb}%')
										and emb.codpag_emb is not null
										order by 2";
		$html->assign_by_ref('selEmbarcacion',$this->ObjFrmSelect($sql_st, $embarcacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft2 = (is_array($idSoft2)) ? $idSoft2 : array();
		$idChi = (is_array($idChi)) ? $idChi : array();
		$idChd1 = (is_array($idChd1)) ? $idChd1 : array();
		$idChd2 = (is_array($idChd2)) ? $idChd2 : array();
		
		if($idSoftNew2) array_push($idSoft2, $idSoftNew2);
		if($idChiNew) array_push($idChi, $idChiNew);
		if($idChd1New) array_push($idChd1, $idChd1New);
		if($idChd2New) array_push($idChd2, $idChd2New);
		
		if($idSoft2&&count($idSoft2)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft2);$i++){
				if(!empty($idSoft2[$i])&&!is_null($idSoft2[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
				  						from user_dnepp.embarcacionnac emb
										where emb.id_emb=%d",
									  $this->PrepareParamSQL($idSoft2[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft2', array('id' => $idSoft2[$i],
														//'idD' => ucwords($row[0]),
														'emb' =>ucwords($row[1]),
														'chi' => $idChi[$i],
														'chd1' => $idChd1[$i],
														'chd2' => $idChd2[$i],
														'matri' =>$row[2],
														'codPago' =>$row[3]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$menu=2;
		$html->assign_by_ref('menu',$menu);
		$accionHeader=1;
		$html->assign_by_ref('accionHeader',$accionHeader);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/seguimientoMerluza/headerArmn.tpl.php');
		$html->display('dnepp/declaracionJurada/frmAddDecJurMensual.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function AgregaDeclaracionJurada($matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco){
		global $idSoft2;
		global $nombreDeclarante,$nroDocDeclarante,$nroTramite,$origen,$mes,$emb,$BuscaEmb,$embarcacion,$chi,$chd1,$chd2;
		global $idChi,$idChd1,$idChd2;
				
		//if($embarcacion<1){ $bC3 = true; $this->errors .= 'El Armador debe ser especificado<br>'; }
		//if($this->compara_fechas($desFechaIni,date('d/m/Y'))>0){ $bC4 = true; $this->errors .= 'La fecha de pago debe ser menor a la de hoy<br>';}
		//if(!$razonSocial){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
				//echo "prueba";
				$nroTramitex=$nroTramite."-2008";
				$this->abreConnDB();
				//$this->conn->debug = true;
				$sql_SP="SELECT COUNT(*) FROM DB_TRAMITE_DOCUMENTARIO.dbo.DOCUMENTO
				WHERE NUM_tram_documentario='$nroTramitex'";
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else{
							$contador=$rs->fields[0];
							if($contador==0){
								$bC4 = true; $this->errors .= 'El registro '.$nroTramitex.' no existe, ingrese un registro v�lido.<br>'; 
							}	
						}
						unset($rs);
					
				
				$contadorEmb=count($idSoft2);
							if($contadorEmb==0){
								$bC3 = true; $this->errors .= 'Debe ingresar al menos una embarcaci�n.<br>'; 
							}
							
				/**///Para verificar que no existe duplicados al momento de ingresar
				for($i=0;$i<count($idSoft2);$i++){
					$sql_SP="SELECT COUNT(*) FROM DECLARACION_JURADA_MENSUAL
					WHERE id_embarcacion=".$idSoft2[$i]." and mes_djm=".$mes." and estado_djm=1";
							$rs = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rs)
								$RETVAL=1;
							else{
								$contador2=$rs->fields[0];
								if($contador2>=1){
									$bC5 = true; $this->errors .= 'Ya existe una declaraci�n con la embarcaci�n y el mes seleccionado.<br>'; 
								}	
							}
							unset($rs);
				
				}/**/			
		
		
		if($bC4||$bC3||$bC5){
			//echo "matrix";
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaDeclaracionJurada($matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
				for($i=0;$i<count($idSoft2);$i++){						
						$sql_SP = sprintf("EXECUTE sp_ins_DECLARACION_JURADA_MENSUAL %d,%d,%d,%d,%d,'%s',%2f,%2f,%2f,%2f,'%s'",
										  ($anyo) ? $this->PrepareParamSQL($anyo) : "2008",
										  ($mes) ? $this->PrepareParamSQL($mes) : "NULL",
										  ($RazonSocial) ? $this->PrepareParamSQL($RazonSocial) : "NULL",
										  $idSoft2[$i],
										  ($origen) ? $this->PrepareParamSQL($origen) : "4",//TABALA USER_DNEPP.ENTIDADRECEPDOC
										  $nroTramite,
										  $idChi[$i],
										  $idChd1[$i],
										  $idChd2[$i],
										  NULL,
										  $_SESSION['cod_usuario']
										  );
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);						
				}//fin del for()
								  

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}&subMenu={$this->menu_items[0]['val']}";
			header("Location: $destination");
			exit;
		}											  
	
	}	
	
	function FormBuscaDeclaracionJuradaMensual($page=NULL,$nroTD=NULL,$checkTodos=NULL,$checkEmb=NULL,$checkCodPag=NULL,$checkMatri=NULL,$FechaIni=NULL,$FechaFin=NULL,$ctaCte=NULL,$checkRegistro=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDecJurMensual';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('checkTodos',$checkTodos);
		$html->assign_by_ref('checkEmb',$checkEmb);
		$html->assign_by_ref('checkCodPag',$checkCodPag);
		$html->assign_by_ref('checkMatri',$checkMatri);
		$html->assign_by_ref('FechaIni',$FechaIni);
		$html->assign_by_ref('FechaFin',$FechaFin);
		$html->assign_by_ref('ctaCte',$ctaCte);
		$html->assign_by_ref('checkRegistro',$checkRegistro);
		
		// Contenido Select de la cta corriente
		$sql_st = "SELECT id_ctacte, lower(banco_ctacte+' ('+des_ctacte+')') ".
				  "FROM user_dnepp.cuentacorriente ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCtaCte',$this->ObjFrmSelect($sql_st, $ctaCte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		$menu=2;
		$accionHeader=0;
		$html->assign_by_ref('menu',$menu);
		$html->assign_by_ref('accionHeader',$accionHeader);		
	
		// Muestra el Formulario
		$html->display('dnepp/seguimientoMerluza/headerArmn.tpl.php');
		$html->display('dnepp/declaracionJurada/search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function BuscaDeclaracionJuradaMensual($page,$nroTD,$checkTodos,$checkEmb,$checkCodPag,$checkMatri,$FechaIni,$FechaFin,$ctaCte,$checkRegistro){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscar';
		$frmName = 'frmBuscarDecJurMen';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
				
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDeclaracionJuradaMensual($page,$nroTD,$checkTodos,$checkEmb,$checkCodPag,$checkMatri,$FechaIni,$FechaFin,$ctaCte,$checkRegistro,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		
		$condTables[] = "user_dnepp.embarcacionnac emb";
		$condConsulta[] = "djm.id_embarcacion=emb.id_emb";
		$condConsulta[] = "djm.ESTADO_DJM=1";
		
		if(!empty($ctaCte)&&!is_null($ctaCte)&&$ctaCte!='none'){
			$condConsulta[] = "djm.mes_djm=$ctaCte";
		}
		
		if(!empty($nroTD)&&!is_null($nroTD)&&$nroTD!=""){
			if($checkTodos==1){
				$condConsulta[] = "(emb.nombre_emb like '%{$nroTD}%' OR emb.matricula_emb like '%{$nroTD}%' OR emb.codpag_emb like '%{$nroTD}%')";	
			}elseif($checkEmb==1){
				$condConsulta[] = "(emb.nombre_emb like '%{$nroTD}%')";	
			}elseif($checkCodPag==1){
				$condConsulta[] = "(emb.codpag_emb like '%{$nroTD}%')";	
			}elseif($checkMatri==1){
				$condConsulta[] = "(emb.matricula_emb like '%{$nroTD}%')";	
			}elseif($checkRegistro==1){
				$condTables[] = "db_tramite_documentario.dbo.documento d";
				$condConsulta[] = "(djm.id_documento=d.id_documento and d.num_tram_documentario like '%{$nroTD}%')";	
			}
		}
		
		/*if($FechaIni && $FechaIni!="" && $FechaFin && $FechaFin!=""){
			$condConsulta[] ="convert(datetime,'$FechaIni',103)<=CONVERT(DATETIME,ip.feccanc_ip,103)";
			$condConsulta[] ="convert(datetime,'$FechaFin',103)>=CONVERT(DATETIME,ip.feccanc_ip,103)";
		}*/
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' /*and*/ '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
				
		$sql_SP = sprintf("EXECUTE sp_busIdDecJurMensual '%s','%s',0,0",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where));

		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIdDecJurMensual '%s','%s',%d,%d",
								($tables) ? $this->PrepareParamSQL($tables): NULL,
								($where) ? $this->PrepareParamSQL($where): NULL,
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($embID = $rsId->FetchRow()){

					// Obtiene Todos los Datos de la Embarcacion
					$sql_SP = sprintf("EXECUTE sp_busca_datos_decJurMensual %d",$embID[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($embData = $rsData->FetchRow())
							$html->append('arrEmb', array('id' => $embData[0],
														  'mes' => $embData[2],
														  'declarante' => $embData[3],
														  'monto' => $embData[7],
														  'tipImporte' => ucwords(strtolower($embData[1])),
														  'recibo' => ucwords(strtolower($embData[5])),
														  'ctaCte' => ucwords(strtolower($embData[4])),
														  'fecPago' => $embData[6],
														  'codPago' => number_format($embData[10],3),
														  'nomEmb' => number_format($embData[8],3),
															'matriEmb' => number_format($embData[9],3)
														  ));
						$rsData->Close();
					}
					unset($rsData);
					unset($strEmbArm);
					unset($strEmbApa);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign('datos',array('desNombre'=>$desNombre,
									'tipBusqueda'=>$tipBusqueda,
									'arte'=>$idArte,
									'regimen'=>$idRegimen,
									'preserva'=>$idPreserva,
									'casco'=>$idCasco,
									'destino'=>$idDestino,
									'estado'=>$idEstado));
		
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscarDecJurMensual', $this->arr_accion[BUSCA_DECJURMENSUAL], true));

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/declaracionJurada/searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}	
	
	function AnulaDeclaracionJuradaMensual($id){
				
			$this->abreConnDB();
			//$this->conn->debug = true;
			
						
					$sql_SP = sprintf("EXECUTE sp_anula_decJurMensual %d",
									  $id
									  );
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);						
				
								  

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}&subMenu={$this->menu_items[0]['val']}";
			header("Location: $destination");
			exit;
	
	}
	
	function FormGeneraReporteMerluza($GrupoOpciones1=NULL,$armador=NULL,$embarcacion=NULL,$matricula_emb=NULL){
		global $idRegimen;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporteMerluza';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		
		$html->assign_by_ref('estado',$idEstado);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$menu=1;
		$html->assign_by_ref('menu',$menu);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('dnepp/seguimientoMerluza/headerArmn.tpl.php');
		$html->display('dnepp/seguimientoMerluza/reportes.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function GeneraReporteMerluza($GrupoOpciones1,$armador,$embarcacion,$matricula_emb){
		global $idRegimen;
		//if($matricula_emb>0)
			//$this->ReporteEstadoCuentaDerechoPescaFlorentino($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen);				
		//else
			$this->ReporteMerluza($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen);				
	}
	
	function ReporteMerluza($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen,$print=false){
	
		//echo "matrix";
		$html = new Smarty;
		
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		//$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistDuenos.tpl.php');
		$sql="select convert(varchar,getDate(),103)+' '+convert(varchar,getDate(),108)";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			$fechaGen=$rs->fields[0];
		}
		
		$sql_="SELECT user_dnepp.f_nombre_personaxembarcacion_vigente(EMB.ID_EMB),
        EMB.NOMBRE_EMB,SUM(CAPTOT_MERLUZA_DJM),emb.id_emb

FROM WEBMINPRO.DECLARACION_MERLUZA DM,
     USER_DNEPP.EMBARCACIONNAC EMB
WHERE EMB.ID_EMB=DM.ID_EMBARCACION";

if($armador && $armador!="")
	$sql_.=" and user_dnepp.f_nombre_personaxembarcacion_vigente(EMB.ID_EMB) like '%".$armador."%' ";
if($embarcacion && $embarcacion!="")
	$sql_.=" and EMB.nombre_EMB like '%".$embarcacion."%' ";
if($matricula_emb && $matricula_emb!="")
	$sql_.=" and EMB.matricula_EMB like '%".$matricula_emb."%' ";

$sql_.=" GROUP BY DM.ID_ARMADOR,EMB.NOMBRE_EMB,EMB.ID_EMB
order by DM.ID_ARMADOR,EMB.NOMBRE_EMB,EMB.ID_EMB";
		$rs_ = & $this->conn->Execute($sql_);
		unset($sql_);
		if(!$rs_)
			print $this->conn->ErrorMsg();
		else{
			$totalEnero=0;
			$totalFebrero=0;
			$totalMarzo=0;
			$totalAbril=0;
			$totalMayo=0;
			$totalJunio=0;
			$totalJulio=0;
			$totalAgosto=0;
			$totalSeptiembre=0;
			$totalOctubre=0;
			$totalNoviembre=0;
			$totalDiciembre=0;
			while($row_ = $rs_->FetchRow()){
				$sql="select * from declaracion_merluza where id_embarcacion=".$row_[3];
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if(!$rs)
					print $this->conn->ErrorMsg();
				else{
					unset($cantidadEne);
					unset($cantidadFeb);
					unset($cantidadMar);
					unset($cantidadAbr);
					unset($cantidadMay);
					unset($cantidadJun);
					unset($cantidadJul);
					unset($cantidadAgo);
					unset($cantidadSep);
					unset($cantidadOct);
					unset($cantidadNov);
					unset($cantidadDic);
					
					while($row = $rs->FetchRow()){
						if($row[2]==1){
							$cantidadEne=$row[5];
						}elseif($row[2]==2){
							$cantidadFeb=$row[5];
						}elseif($row[2]==3){
							$cantidadMar=$row[5];
						}elseif($row[2]==4){
							$cantidadAbr=$row[5];
						}elseif($row[2]==5){
							$cantidadMay=$row[5];
						}elseif($row[2]==6){
							$cantidadJun=$row[5];
						}elseif($row[2]==7){
							$cantidadJul=$row[5];
						}elseif($row[2]==8){
							$cantidadAgo=$row[5];
						}elseif($row[2]==9){
							$cantidadSep=$row[5];
						}elseif($row[2]==10){
							$cantidadOct=$row[5];
						}elseif($row[2]==11){
							$cantidadNov=$row[5];
						}elseif($row[2]==12){
							$cantidadDic=$row[5];
						}
						if(!$cantidadEne)
							$cantidadEne=0;
						if(!$cantidadFeb)
							$cantidadFeb=0;
						if(!$cantidadMar)
							$cantidadMar=0;
						if(!$cantidadAbr)
							$cantidadAbr=0;
						if(!$cantidadMay)
							$cantidadMay=0;
						if(!$cantidadJun)
							$cantidadJun=0;
						if(!$cantidadJul)
							$cantidadJul=0;
						if(!$cantidadAgo)
							$cantidadAgo=0;
						if(!$cantidadSep)
							$cantidadSep=0;
						if(!$cantidadOct)
							$cantidadOct=0;
						if(!$cantidadNov)
							$cantidadNov=0;
						if(!$cantidadDic)
							$cantidadDic=0;
					}
				}
				//$suma_interes_chi_chd=$row_[2]+$row_[3];
				//$suma_total_interes_chi_chd=$suma_total_interes_chi_chd+$suma_interes_chi_chd;
				$html->append('list',array('armador'=>$row_[0],
										   'emb'=>$row_[1],
										   'sumaTotal'=>number_format($row_[2],2),
										   'ene'=>number_format($cantidadEne,2),
										   'feb'=>number_format($cantidadFeb,2),
										   'mar'=>number_format($cantidadMar,2),
										   'abr'=>number_format($cantidadAbr,2),
										   'may'=>number_format($cantidadMay,2),
										   'jun'=>number_format($cantidadJun,2),
										   'jul'=>number_format($cantidadJul,2),
										   'ago'=>number_format($cantidadAgo,2),
										   'sep'=>number_format($cantidadSep,2),
										   'oct'=>number_format($cantidadOct,2),
										   'nov'=>number_format($cantidadNov,2),
										   'dic'=>number_format($cantidadDic,2)
										   ));
				$totalEnero=$totalEnero+$cantidadEne;
				$totalFebrero=$totalFebrero+$cantidadFeb;
				$totalMarzo=$totalMarzo+$cantidadMar;
				$totalAbril=$totalAbril+$cantidadAbr;
				$totalMayo=$totalMayo+$cantidadMay;
				$totalJunio=$totalJunio+$cantidadJun;
				$totalJulio=$totalJulio+$cantidadJul;
				$totalAgosto=$totalAgosto+$cantidadAgo;
				$totalSeptiembre=$totalSeptiembre+$cantidadSep;
				$totalOctubre=$totalOctubre+$cantidadOct;
				$totalNoviembre=$totalNoviembre+$cantidadNov;
				$totalDiciembre=$totalDiciembre+$cantidadDic;
			}							   
			$rs->Close();		
		}

		$total=$totalEnero+$totalFebrero+$totalMarzo+$totalAbril+$totalMayo+$totalJunio+$totalJulio+$totalAgosto+$totalSeptiembre+$totalOctubre+$totalNoviembre+$totalDiciembre;
				
		$html->assign_by_ref('totalEnero',number_format($totalEnero,2));
		$html->assign_by_ref('totalFebrero',number_format($totalFebrero,2));
		$html->assign_by_ref('totalMarzo',number_format($totalMarzo,2));
		$html->assign_by_ref('totalAbril',number_format($totalAbril,2));
		$html->assign_by_ref('totalMayo',number_format($totalMayo,2));
		$html->assign_by_ref('totalJunio',number_format($totalJunio,2));
		$html->assign_by_ref('totalJulio',number_format($totalJulio,2));
		$html->assign_by_ref('totalAgosto',number_format($totalAgosto,2));
		$html->assign_by_ref('totalSeptiembre',number_format($totalSeptiembre,2));
		$html->assign_by_ref('totalOctubre',number_format($totalOctubre,2));
		$html->assign_by_ref('totalNoviembre',number_format($totalNoviembre,2));
		$html->assign_by_ref('totalDiciembre',number_format($totalDiciembre,2));
		$html->assign_by_ref('total',number_format($total,2));
		
		$html->assign_by_ref('fechaGen',$fechaGen);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		$html->assign_by_ref('idRegimen',$idRegimen);
		$html->assign_by_ref('suma_total_interes_chi_chd',$suma_total_interes_chi_chd);
		$html->assign_by_ref('importe_tolva_2004',$importe_tolva_2004);
		$html->assign_by_ref('importe_tolva_2005',$importe_tolva_2005);
		$html->assign_by_ref('importe_tolva_2006',$importe_tolva_2006);
		$html->assign_by_ref('importe_tolva_2007',$importe_tolva_2007);
		$html->assign_by_ref('suma_importes_tolva',$suma_importes_tolva);
		$html->assign_by_ref('fechaActual',date('d/m/Y'));
		$html->assign_by_ref('nombreEmb',$nombreEmb);
		$html->assign_by_ref('matriculaEmb',$matriculaEmb);
		$html->assign_by_ref('armadorEmb',$armadorEmb);
		$html->assign_by_ref('telefonoEmb',$telefonoEmb);
		$html->assign_by_ref('mailEmb',$mailEmb);
		$html->assign_by_ref('cascoEmb',$cascoEmb);
		$html->assign_by_ref('captura2004Emb',$captura2004Emb);
		$html->assign_by_ref('captura2005Emb',$captura2005Emb);
		$html->assign_by_ref('captura2006Emb',$captura2006Emb);
		$html->assign_by_ref('captura2007Emb',$captura2007Emb);
		$html->assign_by_ref('domicilioEmb',$domicilioEmb);
		$html->assign_by_ref('repLegalEmb',$repLegalEmb);
		$html->assign_by_ref('print',$print);
		
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->display('dnepp/seguimientoMerluza/showDetalle.tpl.php');
	}
		
}
?>
