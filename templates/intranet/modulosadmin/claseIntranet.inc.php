<?php
include_once('claseObjetosForm.inc.php');
include_once('claseModulos.inc.php');


class Intranet extends ObjetosForm{
	// Define parametros de la aplicacion
	var $indexPage = "intranet.php";
	var $loginPage = "index.php";
	var $exitPage = "exit.php";
	var $sugerenciaPage = "sugerencias.php";
	var $gruposMailPage = "msggrupos.php";
	var $_mailDomain = 'cunamas.gob.pe';
	var $mods = null;
	
	
	function Intranet(){
		$this->BeginSession();
		//$this->EntryRigth();
		$this->mods = & new Modulos();
	}

	function Header($titulo=NULL,$indIndex=false,$styles=false,$onLoad=false,$prueba=false){
		// Crea el Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('titulo',$titulo);
		$html->assign_by_ref('indIndex',$indIndex);
		$html->assign_by_ref('styles',$styles);
		$html->assign_by_ref('onLoad',$onLoad);
		if($prueba)
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,2,1,true));
		else
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,1,1,true));
			$html->assign_by_ref('modPers',$this->mods->muestraIndexSubmodulosHTML(NULL,1,2,true));
			$html->assign_by_ref('fecHoy',$this->CurrentDate());
			$html->assign_by_ref('userIntra',$_SESSION['cod_usuario']);
			$html->assign_by_ref('domiIntra',$this->_mailDomain);

                $html->assign_by_ref('groupPage',$this->mods->mensajesGrupos());
//		$html->assign_by_ref('groupPage',$this->gruposMailPage);
		$html->assign_by_ref('sugePage',$this->sugerenciaPage);
		$html->assign_by_ref('indexPage',$this->indexPage);
		$html->assign_by_ref('exitPage',$this->exitPage);
		
		//$html->display('intranet/header.tpl.php');
		$html->assign_by_ref('dolarCompra',$_SESSION['dolarCompra']);
		$html->assign_by_ref('dolarVenta',$_SESSION['dolarVenta']);
		$html->assign_by_ref('fob',$_SESSION['fob']);
		$html->assign_by_ref('uit',$_SESSION['uit']);
		$html->assign_by_ref('tasaDiaria',$_SESSION['tasaDiaria']);
		$html->display('intranet/header.tpl.php');
	}
	
	function Header2($titulo=NULL,$indIndex=false,$styles=false,$onLoad=false,$prueba=false){
		// Crea el Objeto HTML
	
		$html = new Smarty;
		
		
		// Setea Caracteristicas en el Formulario
		$html->assign_by_ref('titulo',$titulo);
		$html->assign_by_ref('indIndex',$indIndex);
		$html->assign_by_ref('styles',$styles);
		$html->assign_by_ref('onLoad',$onLoad);
		if($prueba)
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,2,1,true));
		else
			$html->assign_by_ref('modInsti',$this->mods->muestraIndexSubmodulosHTML(NULL,1,1,true));
			$html->assign_by_ref('modPers',$this->mods->muestraIndexSubmodulosHTML(NULL,1,2,true));
			$html->assign_by_ref('fecHoy',$this->CurrentDate());
			$html->assign_by_ref('userIntra',$_SESSION['cod_usuario']);
			$html->assign_by_ref('domiIntra',$this->_mailDomain);

                $html->assign_by_ref('groupPage',$this->mods->mensajesGrupos());
//		$html->assign_by_ref('groupPage',$this->gruposMailPage);
		$html->assign_by_ref('sugePage',$this->sugerenciaPage);
		$html->assign_by_ref('indexPage',$this->indexPage);
		$html->assign_by_ref('exitPage',$this->exitPage);
		
		//$html->display('intranet/header.tpl.php');
		$html->assign_by_ref('dolarCompra',$_SESSION['dolarCompra']);
		$html->assign_by_ref('dolarVenta',$_SESSION['dolarVenta']);
		$html->assign_by_ref('fob',$_SESSION['fob']);
		$html->assign_by_ref('uit',$_SESSION['uit']);
		$html->assign_by_ref('tasaDiaria',$_SESSION['tasaDiaria']);
		
		$html->display('intranet/header2.tpl.php');

	}	
	
	function Body($imgTitulo=NULL,$menuLeft=NULL,$blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
//		$html->assign_by_ref('blockLeft',$blockLeft);
		//$html->assign_by_ref('menuLeft',$menuLeft);
		//$html->assign_by_ref('imgTitulo',$imgTitulo);
		
		$html->assign_by_ref('institucional',$this->mods->muestraModulos(NULL,1));
		$html->assign_by_ref('admin',$this->mods->muestraModulos(NULL,1,3));
		//$html->assign_by_ref('personal',$this->mods->muestraModulos(NULL,1,2));
		$html->display('intranet/body.tpl.php');		
	}
	
	function Body2($imgTitulo=NULL,$menuLeft=NULL,$blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->assign_by_ref('menuLeft',$menuLeft);
		$html->assign_by_ref('imgTitulo',$imgTitulo);
		$html->display('intranet/body2.tpl.php');		
	}
	
	function Bodyn($imgTitulo=NULL,$menuLeft=NULL,$blockLeft=true){
	
	if(!($con=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS)))
		{
			echo "Error en la conexion de la base de datos";
		}else{ 
			//echo "conectado a la conexion de la base de datos";
			$AA=0;
		}
	if(!(mssql_select_db("DB_TRAMITE_DOCUMENTARIO",$con)))
		{
			echo "Error al elegir la base de datos";
		}else{
			//echo "exite al conectarme ala base de datos";
			$AA=0;
		}
		
		$email=$_SESSION['cod_usuario'];
		
			$sqlC="SELECT TR.nombres_TRABAJADOR+' '+TR.apellidos_TRABAJADOR,tr.coddep
				FROM DB_GENERAL.dbo.H_TRABAJADOR TR,db_general.dbo.h_trabajador tr2
				WHERE 
				TR2.EMAIL='$email'
				and tr2.coddep=tr.coddep
				and tr.estado='ACTIVO'
				and tr.director=1";
			if(!mssql_query($sqlC,$con)){
				echo "error en la consulta";
			}else{
				$resultadoC=mssql_query($sqlC,$con);
				$num_camposC=mssql_num_fields($resultadoC);
				$num_registrosC=mssql_num_rows($resultadoC);
				
					while($datosC=mssql_fetch_array($resultadoC)){
							$director=$datosC[0];
							$coddep=$datosC[1];
					}		
			}
			
			//echo $director."-".$coddep;		
		
			/*Listado de los que han sido procesados por el robot pero no est�n aceptados por OADA*/
				
				$sql="select MD.ID_DEPENDENCIA_DESTINO AS DEPENDENCIA,d.id_tipo_documento AS TIPO_DOCUMENTO,count(*) AS CONTADOR 
		        			from movimiento_documento md,documento d 
							where md.derivado=0 and md.finalizado=0
							and md.id_dependencia_destino=$coddep 
							and d.id_documento=md.id_documento group by MD.ID_DEPENDENCIA_DESTINO,d.id_tipo_documento";
							  
				if(!mssql_query($sql,$con)){
					echo "error en la consulta";
				}else{
					$resultado=mssql_query($sql,$con);
					$num_campos=mssql_num_fields($resultado);
					$num_registros=mssql_num_rows($resultado);
					$j=$num_registros-1;
					$jj=-1;
						while($datos=mssql_fetch_array($resultado)){
								$jj++;
								for($i=0;$i<$num_campos;$i++){
									$arreglo[$jj][$i]=$datos[$i];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$Ext = array();
				//$Exp = array();
				//$Int = array();
				
				for($a=0;$a<$num_registros;$a++){
					$idDepDestino = $arreglo[$a][0];
					$idTipoDoc = $arreglo[$a][1];
					$cont = $arreglo[$a][2];
					
						if($idTipoDoc==1){
							$Ext[$idDepDestino]=$cont;
						}elseif($idTipoDoc==2){
							$Exp[$idDepDestino]=$cont;
						}elseif($idTipoDoc==4){
							$Int[$idDepDestino]=$cont;
						}

				
				}

/**********************************/
/********Expedientes Vencidos******/
				$sql3="select md.id_documento,(dbo.cuentadiasutiles(convert(varchar,d.auditmod,103),convert(varchar,getDate(),103))-tup.numero_dias)
		        			from movimiento_documento md,documento d,DB_GENERAL.DBO.TUPA TUP 
							where
							d.id_documento=md.id_documento
							and md.derivado=0 and md.finalizado=0
							and md.id_dependencia_destino=$coddep
							and d.id_tipo_documento=2
							AND D.ID_TUP=TUP.ID_TUPA
							order by 1 desc";
							  
				if(!mssql_query($sql3,$con)){
					echo "error en la consulta";
				}else{
					$resultado3=mssql_query($sql3,$con);
					$num_campos3=mssql_num_fields($resultado3);
					$num_registros3=mssql_num_rows($resultado3);
					$j3=$num_registros3-1;
					$jj3=-1;
						while($datos3=mssql_fetch_array($resultado3)){
								$jj3++;
								for($i3=0;$i3<$num_campos3;$i3++){
									$arreglo3[$jj3][$i3]=$datos3[$i3];
								}//arreglo final que se entrega pero este caso es para 
						}
				}
				
				//$Ext = array();
				//$Exp = array();
				//$Int = array();
				$valorI=0;
				for($a3=0;$a3<$num_registros3;$a3++){
					$idDocumentoPadre = $arreglo3[$a3][0];
					$cont3 = $arreglo3[$a3][1];
									
					if($cont3>0){
						$valorI=$valorI+1;
					}
				}	//fin del for($a=0...)
				
				
								$ceros=0;
								$ExternosCurso=($Ext[$coddep] && $Ext[$coddep]!="") ? $Ext[$coddep] : $ceros;
								$ExternosDia=($Ext2[$coddep] && $Ext2[$coddep]!="") ? $Ext2[$coddep] : $ceros;
								$ExpedientesCurso=($Exp[$coddep] && $Exp[$coddep]!="") ? $Exp[$coddep] : $ceros;
								$ExpedientesDia=($Exp2[$coddep] && $Exp2[$coddep]!="") ? $Exp2[$coddep] : $ceros;
								//$ExpedientesVencido=($Exp3[$coddep] && $Exp3[$coddep]!="") ? $Exp3[$coddep] : $ceros;
								$ExpedientesVencido=($valorI && $valorI!="") ? $valorI : $ceros;
								$ExpedientesNoVencido=$ExpedientesCurso-$ExpedientesVencido;
								$InternosCurso=($Int[$coddep] && $Int[$coddep]!="") ? $Int[$coddep] : $ceros;
								$InternosDia=($Int2[$coddep] && $Int2[$coddep]!="") ? $Int2[$coddep] : $ceros;
								
								$TotalEnCurso=$ExternosCurso+$ExpedientesCurso+$InternosCurso;
	
		// Crea el Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->assign_by_ref('menuLeft',$menuLeft);
		$html->assign_by_ref('imgTitulo',$imgTitulo);
		$html->assign_by_ref('TotalEnCurso',$TotalEnCurso);
		$html->assign_by_ref('ExternosCurso',$ExternosCurso);
		$html->assign_by_ref('ExpedientesCurso',$ExpedientesCurso);
		$html->assign_by_ref('InternosCurso',$InternosCurso);
		$html->assign_by_ref('ExpedientesVencido',$ExpedientesVencido);
		$html->assign_by_ref('director',$director);
		
		$html->display('intranet/bodyn.tpl.php');		
	}		

	
	function Footer($blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
//		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->display('intranet/footer.tpl.php');
	}

	function Footer2($blockLeft=true){
		// Crea el Objeto HTML
		$html = new Smarty;
//		$html->assign_by_ref('blockLeft',$blockLeft);
		$html->display('intranet/footer2.tpl.php');
	}

        function director(){

                if($this->mods->mensajesGrupos()){
                        return true;
                }else{return false;}

        }

}
?>