<?
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');
include_once 'mimemail/htmlMimeMail.php';
require_once('nusoap/lib/nusoap.php');

class DirectorioTramite extends Modulos{

	var $folderXML = '../tramite/xml';
	var $emailDomain = "midis.gob.pe";

	function DirectorioTramite($menu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans',
							FRM_BUSCA_PERSONA =>'frmSearchPersona',
							BUSCA_PERSONA =>'searchPersona',
							BUSCA_PERSONA_EXTENDIDO => 'frmSearchExtendedPer',
							FRM_AGREGA_PERSONA => 'frmAddPersona',
							FRM_AGREGA_PERSONA_WEBSERVICE => 'frmAddPersonaWebService',
							AGREGA_PERSONA => 'addPersona',
							FRM_MODIFICA_PERSONA => 'frmModifyPersona',
							MODIFICA_PERSONA => 'modifyPersona',
							FRM_AGREGA_CONTACTO => 'frmAddContacto',
							AGREGA_CONTACTO => 'addContacto',
							IMPRIMIR_CLAVE => 'printClave',
							MUESTRA_DETALLE_PERSONA => 'showDetailPer',
							IMPRIME_DETALLE =>'ImprimeDetalle',
							FRM_AGREGA_PERSONA2 => 'frmAddPersona2'
							);

		$this->datosUsuarioMSSQL();

		// Items del Menu Principal
		if($this->userIntranet['COD_DEP']==12||$this->userIntranet['COD_DEP']==32||$this->userIntranet['COD_DEP']==33
||$this->userIntranet['COD_DEP']==34 ||$this->userIntranet['COD_DEP']==35 ||$this->userIntranet['COD_DEP']==44 ||$this->userIntranet['COD_DEP']>0
){
/*			$this->menu_items = array(0 => array ( 'val' => 'frmSearchPersona', label => 'BUSCAR' ),
											 1 => array ( 'val' => 'frmAddPersona', label => 'AGREGAR'),
											 2 => array ( 'val' => 'frmModifyPersona', label => 'MODIFICAR'),
											 3 => array ( 'val' => 'frmAddContacto', label => 'CONTACTO')
											);*/
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchPersona', label => 'BUSCAR' ),
											 1 => array ( 'val' => 'frmAddPersona', label => 'AGREGAR')
											);
		}else{
			$this->menu_items = array(0 => array ( 'val' => 'frmSearchPersona', label => 'BUSCAR' )
											);
		}
		
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
    }

	function MuestraStatTrans($accion){
		global $id,$clave;//Id de la Persona
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. DEBE REALIZAR PREVIAMENTE UNA B�SQUEDA. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);

		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		if($id){//�Existe id de la Persona?
		
					$imgStat = 'stat_true.gif';
					$imgOpcion = 'img_printer2.gif';
					$altOpcion = 'Obtener Clave';
					$lnkOpcion = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_CLAVE]}&id={$id}&clave={$clave}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

					$html->assign_by_ref('id',$id);
					$html->assign_by_ref('imgOpcion',$imgOpcion);
					$html->assign_by_ref('altOpcion',$altOpcion);
					$html->assign_by_ref('lnkOpcion',$lnkOpcion);
		}
				
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_PERSONA]);
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/showStatTrans.inc.php');
		$html->display('oad/footerArm.tpl.php');
	}

	function MuestraIndex(){
		if($this->userIntranet['COD_DEP']==48){
			$this->FormAgregaPersona();
		}else{
			$this->FormBuscaPersona();
		}	
	}

	function FechaActual(){
		setlocale (LC_TIME, $this->zonaHoraria);
		return ucfirst(strtolower(strftime("%d/%m/%Y")));
	}
	function HoraActual(){
		//setlocale (LC_TIME, $this->zonaHoraria);
		//return ucfirst(strtolower(strftime("%T")));	
		$this->abreConnDB();
//		$this->conn->debug = true;

		$sql="select convert(varchar,getDate(),108)";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				$hora=$rs->fields[0];
			}
		return($hora);
	}
	/* Funcion para validar Email */
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	} 	
	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	function FormBuscaPersona($page=NULL,$desNombre=NULL,$sector=NULL,$codDepa=NULL,$tipPersona=NULL,$search=false){
		global $ruc,$codDepa2,$codProv,$codDist;
		
		/*$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select distinct id,case when id_tipo_persona=1 then apellidos+' '+nombres
									 else razon_social end,len(case when id_tipo_persona=1 then apellidos+' '+nombres
									 else razon_social end),nro_documento,case when id_tipo_identificacion=1 then 'DNI'
												                   when id_tipo_identificacion=8 then 'RUC'
														ELSE 'OTROS' END,
											direccion,representante_legal,LEN(NRO_DOCUMENTO) AS LONGITUD								
									from db_general.dbo.persona
									where id in (
									select embxper.id_pers from db_dnepp.user_dnepp.embxpersona embxper,db_dnepp.user_dnepp.pd2007 pd
									 where embxper.estado_embxpers=1 and embxper.id_emb=pd.id_emb and pd.entra=1
									)
					and len(nro_documento) in (8,11)
					and nro_documento<>'00000000'
					and nro_documento<>'00000000000'
					ORDER BY 1			 
		
			 ";
			$sql="select distinct p.id,case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
									 else p.razon_social end,len(case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
									 else p.razon_social end),p.nro_documento,case when p.id_tipo_identificacion=1 then 'DNI'
												                   when p.id_tipo_identificacion=8 then 'RUC'
														ELSE 'OTROS' END,
											p.direccion,p.representante_legal,LEN(p.NRO_DOCUMENTO) AS LONGITUD,
											depa.departamento,prov.provincia,dist.distrito,p.telefono,p.email,
											case when p.id_tipo_identificacion_rep_leg=1 then 'DNI'
												                   when p.id_tipo_identificacion_rep_leg=8 then 'RUC'
														ELSE 'OTROS' END,
											p.nro_documento_representante
									from db_general.dbo.persona p,db_general.dbo.departamento depa,
										db_general.dbo.provincia prov,
										db_general.dbo.distrito dist
									where p.id in (
									select embxper.id_pers from db_dnepp.user_dnepp.embxpersona embxper,db_dnepp.user_dnepp.pd2007 pd
									 where embxper.estado_embxpers=1 and embxper.id_emb=pd.id_emb and pd.entra=1
									)
					and len(p.nro_documento) in (8,11)
					and p.nro_documento<>'00000000'
					and p.nro_documento<>'00000000000'
					and p.codigo_departamento=depa.codigo_departamento
					and p.codigo_provincia=prov.codigo_provincia and prov.codigo_departamento=depa.codigo_departamento
					and p.codigo_distrito=dist.codigo_distrito and dist.codigo_provincia=prov.codigo_provincia and dist.codigo_departamento=depa.codigo_departamento
					ORDER BY 1";  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					$i=0;
					while ($row = $rs->FetchRow()){
						$i=$i+1;
						$id=$row[0];
						$nombre=$row[1];
						$longitud=$row[2];
						$ruc=$row[3];
						$tipoIdentificacion=$row[4];
						$direccion=$row[5];
						$representante=$row[6];
						$departamento=$row[8];
						$provincia=$row[9];
						$distrito=$row[10];
						$telefono=$row[11];
						$mail=$row[12];
						$tipoIdentificacionRepLeg=$row[13];
						$nroDocRepLeg=$row[14];
						//echo $id.",".$nombre."<br>";
						
						$clave = $this->GeneraPassword(6);
						$valores=$clave-$longitud*12;
						
						if($valores<100000){
							$valores="3".$valores;
						}
						
						echo $id.",".$nombre.",".$tipoIdentificacion.",".$ruc.",".$this->formateaComas($direccion).",".$this->formateaComas($representante).",".$valores.",".$departamento.",".$provincia.",".$distrito.",".$telefono.",".$mail.",".$tipoIdentificacionRepLeg.",".$nroDocRepLeg."<br>";
						
						$nombres="PRUEBA";
						$apellidos="PRUEBA";
						$direccion="PRUEBA";
						$telefono="PRUEBA";
						$fax="PRUEBA";
						$mail="PRUEBA@prueba.com";
						
						//echo "justomatrix";
						
						$sql_SP2 = sprintf("EXECUTE sp_insContacto %d,%s,%s,%s,%s,%s,%s,'%s','%s','%s'",
										  $id,
										  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
										  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
										  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
										  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
										  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
										  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
										  $_SESSION['cod_usuario'],
										  md5($valores),
										  ($radio) ? $radio : "P"
										  );
						 //echo $sql_SP; exit;
						$rs2 = & $this->conn->Execute($sql_SP2);
						unset($sql_SP2);
						if (!$rs2)
							$RETVAL2=1;
						unset($rs2);
					}//fin del while
					//echo "el valor del contador es: ".$contador;
			}		
			*/
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscaPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('desNombre',$desNombre);
		$html->assign_by_ref('ruc',$ruc);

		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector where id=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Departamento
		$sql_st = "SELECT codigo_departamento, lower(departamento) ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 2";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa2',$this->ObjFrmSelect($sql_st, $codDepa2, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa2)||!empty($codDepa2)) ? $codDepa2 : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/searchPersona.tpl.php');
		if(!$search) $html->display('oad/footerArm.tpl.php');
	}
	
	function busTipPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select id_tipo_persona 
		         from db_general.dbo.tipo_persona 
				 where ID_TIPO_PERSONA=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
					$tipPersona=$row[0];
					//echo "el valor del contador es: ".$contador;
			}
			//unset($rs);
		return($tipPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}

	function busPrivPersona($dat){//Se busca si es se ha numerado la resoluci�n 
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="select rubro 
		         from db_general.dbo.persona 
				 where id=$dat "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					//while ($row = $rs->FetchRow())
					$privPersona=$rs->fields[0];
					//echo "el valor del contador es: ".$privPersona;
			}
			//unset($rs);
		return($privPersona);
		//Los Trabajadores normales no pueden delegar documentos o expedientes, s�lo los jefes de oficina y directores.
	}
	
	function BuscaDirectorio($page,$desNombre,$sector,$codDepa,$tipPersona){
		global $ruc,$codDepa2,$codProv,$codDist;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmBuscaPersona';
		$html->assign('frmName','frmSearchExtendedPer');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array(
									
									'desNombre'=>$desNombre,
									'sector'=>$sector,
									'codDepa'=>$codDepa,
									'tipPersona'=>$tipPersona,
									'ruc'=>$ruc,
									'codDepa2'=>$codDepa2,
									'codProv'=>$codProv,
									'codDist'=>$codDist
									));
		
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaPersona($page,$desNombre,$sector,$codDepa,$tipPersona,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
				
		// Condicionales Segun ingreso del Usuario
		$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";
		
		if(!empty($codDepa2)&&!is_null($codDepa2)&&$codDepa2!='none'){
			$condConsulta[] = "p.codigo_departamento='$codDepa2' ";
			if(!empty($codProv)&&!is_null($codProv)&&$codProv!='none'){
				$condConsulta[] = "p.codigo_provincia='$codProv' ";
				if(!empty($codDist)&&!is_null($codDist)&&$codDist!='none'){
					$condConsulta[] = "p.codigo_distrito='$codDist' ";
				}
			}
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		//echo "<br>la tabla".$table."<br>el wherte".$where."<br>tipo de cor".$TipoCor."<br>";

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($desNombre);
	

			$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,0,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL'
								);
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
				$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,%s,%s",
									($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
									($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
									($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
									$start,
									$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosPersona %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($NotiData = $rs->FetchRow())
							$html->append('arrPer', array('id' => $id[0],
														  'desNom' => ucwords($NotiData[1]),
														  'tipPer' => $NotiData[2],
														  'nro' => $NotiData[3],
														  'dire' => ucfirst($NotiData[4]),
														  'tel' => strtoupper($NotiData[5]),
														  'mail' => $NotiData[6],
														  'tipPersona' => $this->busTipPersona($id[0]),
														  'acui' => $this->busPrivPersona($id[0]),
														  'flag' => $NotiData[7]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);
		
		$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('userDirectorio',$_SESSION['cod_usuario']);

		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_PERSONA], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscaPersona', $this->arr_accion[BUSCA_PERSONA], true));

		// Muestra el Resultado de la Busqueda
		$html->display('oad/tramite/searchResultPersona.tpl.php');
		$html->display('oad/footerArm.tpl.php');
		
	}
	
	function VistaExtendidaDocPersona($desNombre,$sector,$codDepa,$tipPersona,$ruc){
		$html = new Smarty;

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$condTable[] = "db_general.dbo.tipo_persona tp";
		$condTable[] = "db_general.dbo.sector s";
		$condTable[] = "db_general.dbo.departamento dep";
		$condConsulta[] = "tp.id_tipo_persona=p.id_tipo_persona";
		$condConsulta[] = "s.id=p.id_sector";
		$condConsulta[] = "dep.codigo_departamento=p.codigo_departamento ";
		
		if(!empty($tipPersona)&&!is_null($tipPersona)&&$tipPersona!='none')
			$condConsulta[] = "tp.id_tipo_persona=$tipPersona";
		if(!empty($sector)&&!is_null($sector)&&$sector!='none')
			$condConsulta[] = "s.id=$sector";
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none')
			$condConsulta[] = "dep.codigo_departamento='$codDepa' ";
		if(!empty($ruc)&&!is_null($ruc))
			$condConsulta[] = "Upper(p.nro_documento) LIKE Upper('%{$ruc}%')";

		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busIDPersona %s,%s,%s,1,0",
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			while($id = $rsId->FetchRow()){
				// Obtiene Todos los Datos de la Parte
				$sql_SP = sprintf("EXECUTE sp_busDatosPersona %d",$id[0]);
				//echo $sql_SP;
				$rsData = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if(!$rsData)
					print $this->conn->ErrorMsg();
				else{
					if($PerData = $rsData->FetchRow())
							$html->append('arrPer', array('id' => $id[0],
														  'desNom' => ucwords($PerData[1]),
														  'tipPer' => $PerData[2],
														  'nro' => $PerData[3],
														  'dire' => ucfirst($PerData[4]),
														  'tel' => strtoupper($PerData[5]),
														  'mail' => $PerData[6],
														  'tipPersona' => $this->busTipPersona($id[0])
														  //'acui' => $this->busPrivPersona($id[0])
														  ));
					$rsData->Close();
				}
				unset($rsData);
			}
			$rsId->Close();
		}
		unset($rsId);

		// Muestra el Resultado de la Busqueda
		header("Pragma: ");
		header("Cache-Control: ");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
		$html->display('oad/tramite/viewExtendedPer.tpl.php');
		exit;
	}
	
	function FormAgregaPersona($sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$errors=false){
		//echo "En mantenimiento";exit;
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $bBusca;
		global $rubro10;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/**/
		if($bBusca==1){
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql="select count(*)
			      from db_general.dbo.persona where nro_documento='$ruc'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$cuenta=$rs->fields[0];
				if($cuenta>=1){
					$msj="El n�mero ingresado ya existe en la Base de Datos<br>Ingrese otro n�mero. Gracias";
				}
			}

			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi
			      from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";*/
			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi,s.telef1
			      from bd_sunat.dbo.dato_principal P left join BD_SUNAT.DBO.DATO_SECUNDARIO s on p.numruc=s.numruc and s.telef1<>'-' 	
				  where p.NUMRUC='$ruc'	";	  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					//$razonsocial = $row[0];
					$codDepa = $row[1];
					$codProv = $row[2];
					$codDist = $row[3];
					$direccion = $row[4];
					$identi= $row[5];
					$telefono= $row[6];
					if($identi=="02"){
						$razonsocial = $row[0];
						$tipPersona=2;
						$tipIdent=8;
						if($razonsocial!=""){
							$valorReadonly=1;
						}						
					}else{
						$nombres= $row[0];
						$tipPersona=1;
						$tipIdent=8;}
					
				}
				$rs->Close();
			}
			*/
			unset($rs);
			
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);
		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector where id=4 ".
				  "ORDER BY 2 DESC";
		//$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		if(!isset($_GET['popup']))$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddPersona.tpl.php');
		if(!isset($_GET['popup']))$html->display('oad/footerArm.tpl.php');
	}
	function FormAgregaPersona2($sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$errors=false){
		//echo "En mantenimiento";exit;
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $bBusca;
		global $rubro10;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/**/
		if($bBusca==1){
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql="select count(*)
			      from db_general.dbo.persona where nro_documento='$ruc'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$cuenta=$rs->fields[0];
				if($cuenta>=1){
					$msj="El n�mero ingresado ya existe en la Base de Datos<br>Ingrese otro n�mero. Gracias";
				}
			}

			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi
			      from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";*/
			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi,s.telef1
			      from bd_sunat.dbo.dato_principal P left join BD_SUNAT.DBO.DATO_SECUNDARIO s on p.numruc=s.numruc and s.telef1<>'-' 	
				  where p.NUMRUC='$ruc'	";	  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					//$razonsocial = $row[0];
					$codDepa = $row[1];
					$codProv = $row[2];
					$codDist = $row[3];
					$direccion = $row[4];
					$identi= $row[5];
					$telefono= $row[6];
					if($identi=="02"){
						$razonsocial = $row[0];
						$tipPersona=2;
						$tipIdent=8;
						if($razonsocial!=""){
							$valorReadonly=1;
						}						
					}else{
						$nombres= $row[0];
						$tipPersona=1;
						$tipIdent=8;}
					
				}
				$rs->Close();
			}
			*/
			unset($rs);
			
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersona2';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);
		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector where id=4 ".
				  "ORDER BY 2 DESC";
		//$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		if(!isset($_GET['popup']))$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddPersona2.tpl.php');
		if(!isset($_GET['popup']))$html->display('oad/footerArm.tpl.php');
	}
	function FormAgregaPersonaWebService($sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$errors=false){
		//echo "En mantenimiento";exit;
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $bBusca;
		global $rubro10;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		/*
		if($this->userIntranet['COD_DEP']!=7){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}*/
		/**/
		if($bBusca==1){
		
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql="select count(*)
			      from db_general.dbo.persona where nro_documento='$ruc'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$cuenta=$rs->fields[0];
				if($cuenta>=1){
					$msj="El n�mero ingresado ya existe en la Base de Datos<br>Ingrese otro n�mero. Gracias";
				}
			}
			
			$soapclient  =  new nusoap_client( 'http://wwss.sunat.gob.pe/jboss-net/services/ConsultaRuc?wsdl'); 
			$soapclient -> setEndpoint($wsdl);
			
			$rs = $soapclient->call('getDatosPrincipales',array( 'trama'=>$_POST['ruc']));
			
			$rs2 = $soapclient->call('getDatosSecundarios',array( 'trama'=>$_POST['ruc']));
			$rs3 = $soapclient->call('getDatosT1144',array( 'trama'=>$_POST['ruc']));
			$rs4 = $soapclient->call('getRepLegales',array( 'trama'=>$_POST['ruc']));
			//$rs5 = $soapclient->call('getEstablecimientosAnexos',array( 'trama'=>$_POST['ruc']));
			//$rs6 = $soapclient->call('getEstAnexosT1150',array( 'trama'=>$_POST['ruc']));
			
			if ($soapclient -> fault) { // si
				  echo 'No se pudo completar la operaci�n....<br>'; 
				  die(); 
			}else{ 
				$error = $soapclient->getError(); 
				if ($error) { // Hubo algun error 
					echo '<br>Error:' . $error; 
				} 
			}
			
			//print_r($rs);
			//echo "<br/>";
			//print_r($rs2);
			//echo "<br/>";
			//print_r($rs3);
			//echo "<br/>RepLegales<br/>";
			//print_r($rs4);
			//print_r($rs5);
			//print_r($rs6);

					//$razonsocial = $row[0];
					$codDepa = $rs['cod_dep'];
					$codProv = substr($rs['cod_prov'],2,2);
					$codDist = substr($rs['cod_dist'],4,2);
					$direccion = rtrim($rs['desc_tipzon']);
					if(rtrim($rs['ddp_nomzon'])!="" && rtrim($rs['ddp_nomzon'])!="-"){
						$direccion.=" ".rtrim($rs['ddp_nomzon']);
					}
					if(rtrim($rs['desc_tipvia'])!="" && rtrim($rs['desc_tipvia'])!="-"){
						$direccion.=" ".rtrim($rs['desc_tipvia']);
					}
					if(rtrim($rs['ddp_nomvia'])!="" && rtrim($rs['ddp_nomvia'])!="-"){
						$direccion.=" ".rtrim($rs['ddp_nomvia']);
					}
					if(rtrim($rs['ddp_numer1'])!="" && rtrim($rs['ddp_numer1'])!="-"){
						$direccion.=" ".rtrim($rs['ddp_numer1']);
					}
					if(rtrim($rs['num_depar'])!="" && rtrim($rs['num_depar'])!="-"){
						$direccion.=" ".rtrim($rs['num_depar']);
					}
					if(rtrim($rs['ddp_refer1'])!="" && rtrim($rs['ddp_refer1'])!="-"){
						$direccion.=" ".rtrim($rs['ddp_refer1']);
					}
					
					
					$identi= $row[5];
					$telefono= rtrim($rs2['dds_telef1']);
					if(rtrim($rs2['dds_telef2'])!="" && rtrim($rs2['dds_telef2'])!="-"){
						$telefono.="-".rtrim($rs2['dds_telef2']);
					}
					if(rtrim($rs2['dds_telef3'])!="" && rtrim($rs2['dds_telef3'])!="-"){
						$telefono.="-".rtrim($rs2['dds_telef3']);
					}
					
					$fax= rtrim($rs2['dds_numfax']);
					$mail=rtrim($rs3['cod_correo1'])." ".rtrim($rs3['cod_correo2']);
					if($rs['ddp_identi']=="02"){
						$razonsocial = rtrim($rs['ddp_nombre']);
						$tipPersona=2;
						$tipIdent=8;
						if($razonsocial!=""){
							$valorReadonly=1;
						}						
					}else{
						$nombres= rtrim($rs['ddp_nombre']);
						$tipPersona=1;
						$tipIdent=8;
					}
					$tipIdentRepLegal=rtrim($rs4[0]['rso_docide']);
					$nroRepLegal=rtrim($rs4[0]['rso_nrodoc']);
					$repLegal=rtrim($rs4[0]['rso_nombre']);
				
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersonaWebService';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);
		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector where id=4 ".
				  "ORDER BY 2 DESC";
		//$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddPersonaWebService.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}	
	
	function formateaComas($cadena){
	
		$cadena= ereg_replace(","," ",$cadena);
		
		return ($cadena);
	}	
	
	function formateaTexto($cadena){
	
		$cadena= ereg_replace("          "," ",$cadena);
		$cadena= ereg_replace("         "," ",$cadena);
		$cadena= ereg_replace("        "," ",$cadena);
		$cadena= ereg_replace("       "," ",$cadena);
		$cadena= ereg_replace("      "," ",$cadena);
		$cadena= ereg_replace("     "," ",$cadena);
		$cadena= ereg_replace("    "," ",$cadena);
		$cadena= ereg_replace("   "," ",$cadena);
		$cadena= ereg_replace("  "," ",$cadena);
		
		return ($cadena);
	}
	
	function formateaAcentos($cadena){
	
		$cadena= ereg_replace("�","a",$cadena);
		$cadena= ereg_replace("�","e",$cadena);
		$cadena= ereg_replace("�","i",$cadena);
		$cadena= ereg_replace("�","o",$cadena);
		$cadena= ereg_replace("�","u",$cadena);
		$cadena= ereg_replace("�","A",$cadena);
		$cadena= ereg_replace("�","E",$cadena);
		$cadena= ereg_replace("�","I",$cadena);
		$cadena= ereg_replace("�","O",$cadena);
		$cadena= ereg_replace("�","U",$cadena);
		
		return ($cadena);
	}		
	
	function AgregaPersona($sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones){
		global $rubro1,$rubro2,$rubro3,$rubro4,$rubro5,$rubro6;
		global $rubro10;
		global $bBusca;
		
			if ($razonsocial && $razonsocial!="")
				$razonsocial=$this->formateaTexto($razonsocial);
			if ($nombres && $nombres!=""){
				$nombres=$this->formateaTexto($nombres);
				$nombres=$this->formateaAcentos($nombres);
			}	
			if ($apellidos && $apellidos!=""){
				$apellidos=$this->formateaTexto($apellidos);
				$apellidos=$this->formateaAcentos($apellidos);
			}	
			if ($repLegal && $repLegal!="")
				$repLegal=$this->formateaTexto($repLegal);				
				
		// Comprueba Valores
		$tamano=strlen($ruc);
		$tamanoDireccion=strlen($direccion);
		$tamanoNombre=strlen($nombres);
		$tamanoApellido=strlen($apellidos);
		$tamanoRazonSocial=strlen($razonsocial);
		$tamanoRepLegal=strlen($nroRepLegal);
		$tamanoNombreRepLegal=strlen($repLegal);
		if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if($tipIdent==8&& $tamano!=11){ $bC10 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC<br>'; }
		if($tipIdent==1&& $tamano!=8){ $bC11 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI<br>'; }
		//if(!$telefono){ $bC9 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		//if($codDepa<1){ $bC10 = true; $this->errors .= 'El Departamento debe ser especificado<br>'; }
		//if($codProv<1){ $bC11 = true; $this->errors .= 'La Provincia debe ser especificada<br>'; }
		//if($codDist<1){ $bC12 = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC13 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }
		if($tamanoDireccion<10){ $bC14 = true; $this->errors .= 'Al menos 10 caracteres que debe contener la direcci�n<br>'; }
		if($nombres&&$nombres!=""&&$tipPersona==1&& $tamanoNombre<2){ $bC15 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre<br>'; }
		if($apellidos&&$apellidos!=""&&$tipPersona==1&& $tamanoApellido<2){ $bC16 = true; $this->errors .= 'Al menos 2 caracteres debe contener el apellido<br>'; }
		if($razonsocial&&$razonsocial!=""&&$tipPersona==2&& $tamanoRazonSocial<2){ $bC17 = true; $this->errors .= 'Al menos 2 caracteres debe contener la Raz�n Social<br>'; }
		if($tipIdentRepLegal>0&&!$nroRepLegal){ $bC18 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser espcificado<br>'; }
		if($tipIdentRepLegal==8&& $tamanoRepLegal!=11){ $bC19 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC del Representante Legal<br>'; }
		if($tipIdentRepLegal==1&& $tamanoRepLegal!=8){ $bC20 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI del Representante Legal<br>'; }
		if($tipIdentRepLegal>0&&!$repLegal){ $bC21 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tipIdentRepLegal<1){ $bC22 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&!$nroRepLegal){ $bC23 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tamanoNombreRepLegal<2){ $bC24 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre del Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&!$repLegal){ $bC25 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&$tipIdentRepLegal<1){ $bC26 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
	/*	if($tipPersona==2&&$tipIdent==8&&$bBusca!=1&&$ruc!="00000000000"){ $bC27 = true; $this->errors .= 'Debe consultar la data de SUNAT, hacer click en el checkbox.<br>'; }*/
		/**/
			$this->abreConnDB();
			//$this->conn->debug = true;
			if((!$apellidos&& !$nombres)&& $tipPersona==2){
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and razon_social='$razonsocial' /*and nro_documento='$ruc'*/";
			}else{
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and nombres='$nombres' and apellidos='$apellidos' /*and nro_documento='$ruc'*/";
			}
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe la persona en el Directorio de CONVENIO_SITRADOC<br>'; }
				}
				
			$sql2="select count(*) from db_general.dbo.persona where nro_documento='$ruc' AND NRO_DOCUMENTO<>'00000000000' and NRO_DOCUMENTO<>'00000000'";
				$rs2 = & $this->conn->Execute($sql2);
				unset($sql2);
				if (!$rs2)
					$RETVAL=1;
				else{
					$contador2=$rs2->fields[0];
					if($contador2>=1){ $bC12 = true; $this->errors .= 'Ya existe el Nro de documento en el Directorio de CONVENIO_SITRADOC<br>'; }
				}
				
			/*if($tipPersona==2&&$tipIdent==8&&$bBusca==1&&$ruc!="00000000000"){
				$sql3="select count(*) from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";
				$rs3 = & $this->conn->Execute($sql3);
				unset($sql3);
				if (!$rs3)
					$RETVAL=1;
				else{
					$contador3=$rs3->fields[0];
					if($contador3==0){ $bC28 = true; $this->errors .= 'No existe el Nro de ruc en el Padr�n RUC.<br>'; }
				}				
			}*/				
			
		/**/
		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC9||$bC10||$bC11||$bC12||$bC13||$bC14||$bC15||$bC16||$bC17||$bC18||$bC19||$bC20||$bC21||$bC22||$bC23||$bC24||$bC25||$bC26||$bC27||$bC28){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaPersona($sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{

			if($rubro1!=1) $rubro1=0;
			if($rubro2!=1) $rubro2=0;
			if($rubro3!=1) $rubro3=0;
			if($rubro4!=1) $rubro4=0;
			if($rubro5!=1) $rubro5=0;
			if($rubro6!=1) $rubro6=0;
			if($rubro10!=1) $rubro10=0;
			
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
						
			$sql_SP = sprintf("EXECUTE sp_insPersona %d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,'%s',%s",
							  ($sector) ? $this->PrepareParamSQL($sector) : "NULL",
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "'SIN DIRECCI�N'",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa&&$codDepa!='none') ? "'".$this->PrepareParamSQL($codDepa)."'" : "'00'",
							  ($codProv&&$codProv!='none') ? "'".$this->PrepareParamSQL($codProv)."'" : "'00'",
							  ($codDist&&$codDist!='none') ? "'".$this->PrepareParamSQL($codDist)."'" : "'00'",
							  $_SESSION['cod_usuario'],
							  //"'".$rubro1.$rubro2.$rubro3.$rubro4.$rubro5.$rubro6."000".$rubro10."'"
							  "'1111111111'"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Para q se actualice el XML
				//$this->GeneraXMLRazonSocial($tipPersona);
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			if($this->userIntranet['COD_DEP']==48){
				$destination .= "&menu={$this->menu_items[0]['val']}";
			}else{
				$destination .= "&menu={$this->menu_items[1]['val']}";
			}
			header("Location: $destination");
			exit;
		}
	}

	function FormModificaPersona($id,$sector=NULL,$tipPersona=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$tipIdent=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,$ruc=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$nroRepLegal=NULL,$tipIdentRepLegal=NULL,$observaciones=NULL,$reLoad=false,$errors=false){
		//echo "En mantenimiento";exit;
		global $bBusca;
		/*Esto es para que solo las Direcciones y Oficinas Generales que le compete tengan acceso a Notificaciones*/
		if($this->userIntranet['COD_DEP']!=12 && $this->userIntranet['COD_DEP']!=32 && $this->userIntranet['COD_DEP']!=33
&&$this->userIntranet['COD_DEP']!=34 && $this->userIntranet['COD_DEP']!=35 && $this->userIntranet['COD_DEP']!=44
){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		/**/
			if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos de la Persona
			$sql_SP = sprintf("EXECUTE sp_listPersona %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					/**/			
					$id = $row[0];
					$sector = $row[1];
					$tipPersona = $row[2];
					$codDepa = $row[3];
					$codProv = $row[4];
					$codDist = $row[5];
					$tipIdent = $row[6];
					$razonsocial = $row[8];
					$nombres = $row[9];
					$apellidos = $row[10];
					$ruc = $row[11];
					$direccion = $row[12];
					$telefono = $row[13];
					$fax = $row[14];
					$mail = $row[15];
					$repLegal = $row[17];
					$nroRepLegal = $row[18];
					$tipIdentRepLegal = $row[19];
					$observaciones = $row[23];
					/**/
				}
				$rs->Close();
			}
			unset($rs);
			}//fin del if($reload)

		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddPersona';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('bBusca',$bBusca);

		// Contenido Select Sector
		$sql_st = "SELECT id, lower(descripcion) ".
				  "FROM db_general.dbo.sector where id=4 ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSector',$this->ObjFrmSelect($sql_st, $sector, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmModifyPersona.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}

	function ModificaPersona($id,$sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones){
		// Comprueba Valores	
		if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		//if(!$telefono){ $bC9 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		//if($codDepa<1){ $bC10 = true; $this->errors .= 'El Departamento debe ser especificado<br>'; }
		//if($codProv<1){ $bC11 = true; $this->errors .= 'La Provincia debe ser especificada<br>'; }
		//if($codDist<1){ $bC12 = true; $this->errors .= 'El Distrito debe ser especificado<br>'; }

		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaPersona($id,$sector,$tipPersona,$codDepa,$codProv,$codDist,$tipIdent,$razonsocial,$nombres,$apellidos,$ruc,$direccion,$telefono,$fax,$mail,$repLegal,$nroRepLegal,$tipIdentRepLegal,$observaciones,true,$errors);
			
			$objIntranet->Footer();
		}else{
			
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			
			$sql_SP = sprintf("EXECUTE sp_modPersona %d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,'%s'",
								$id,
							  ($sector) ? $this->PrepareParamSQL($sector) : "NULL",
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "NULL",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa) ? "'".$this->PrepareParamSQL($codDepa)."'" : "NULL",
							  ($codProv) ? "'".$this->PrepareParamSQL($codProv)."'" : "NULL",
							  ($codDist) ? "'".$this->PrepareParamSQL($codDist)."'" : "NULL",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				//Para q se actualice el XML
				//$this->GeneraXMLRazonSocial($tipPersona);
				//Fin de Para q se actualice el XML
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function GeneraXMLRazonSocial($tipo=1) {
		$this->abreConnDB();
		//$this->conn->debug = true;

		// Obtiene los Datos de la Persona
		$sql_SP = sprintf("SELECT id, CASE WHEN id_tipo_persona=1 THEN Upper(apellidos)+' '+Upper(nombres) ELSE substring(Upper(razon_social),1,100) END, FLAG ".
						  "FROM db_general.dbo.persona ".
						  "WHERE id_tipo_persona=%d ".
						  "and FLAG='A' ".
						  "ORDER BY 2", $tipo);
		// echo $sql_SP;
		
				
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			$ii=0;
			while($row = $rs->FetchRow()){
			
				//echo $row[1]."-".$row[2]."<br />";
			
				$valor=$row[0];
				$cadena=strtoupper($row[1]);
				//$cadena=htmlentities($cadena);
				if($cadena!=""){
					if(ord($cadena[0])>64&&ord($cadena[0])<96)					
						$x=ord($cadena[0])-65;
					if(ord($cadena[1])>64&&ord($cadena[1])<96)					
						$y=ord($cadena[1])-65;
					if((ord($cadena[0])>=48&&ord($cadena[0])<=57)||ord($cadena[0])==38)
						$x=ord($cadena[0]);
					if((ord($cadena[1])>=48&&ord($cadena[1])<=57)||ord($cadena[1])==38)					
						$y=ord($cadena[1]);
					// $arreglo[$x][$y]["Total"]++;
					// $ii=$arreglo[$x][$y]["Total"];
					$arreglo[$x][$y][] = array($valor, $cadena);
				}
				$cadena="";
			}
			
			//exit();
			$rs->Close();
		}
		unset($rs);

		
		

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('elements',$arreglo);

		
		// Genera el archivo XML en la carpeta xml
		// header("Content-type: text/xml");
		$filename = $tipo==1 ? 'natural.xml' : 'juridica.xml';
		$fd = fopen($this->folderXML . '/' . $filename, 'w+');
		echo $this->folderXML . '/' . $filename.'<br>';
		
		fputs($fd, $html->fetch('oad/tramite/XMLRazonSocial.tpl.php'));
		
		fclose($fd);	
		//exit();

	}
	
	function FormAgregaContacto($id,$nombres=NULL,$apellidos=NULL,$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$radio=NULL,$errors=false){
		if($this->userIntranet['COD_DEP']!=12 && $this->userIntranet['COD_DEP']!=32 && $this->userIntranet['COD_DEP']!=33&& $this->userIntranet['COD_DEP']!=34 && $this->userIntranet['COD_DEP']!=35 && $this->userIntranet['COD_DEP']!=44){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		
		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		$html->assign_by_ref('FechaActual',$this->FechaActual());
		$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddContacto';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('radio',$radio);
		$html->assign_by_ref('id',$id);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('oad/headerArm.tpl.php');
		$html->display('oad/tramite/frmAddContacto.tpl.php');
		$html->display('oad/footerArm.tpl.php');	
	}
	
	// Genera una Cadena Aleatoria que sera tomada como Password de Usuario
	function GeneraPassword($length){
		//$vowels = 'aeiouyAEIOUY';//5x2
		$vowels = '123456789012';
		//$consonants = 'bdghjlmnpqrstvwxzBDGHJLMNPQRSTVWXZ';//17x2
		$consonants = '1234567890123456712345678901234567';
		$password = '';
		
		$alt = time() % 2;
		srand(time());
	
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % 34)];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % 12)];
				$alt = 1;
			}
		}
		return $password;
	}
	
	function AgregaContacto($id,$nombres,$apellidos,$direccion,$telefono,$fax,$mail,$radio){
	global $id;//el id dela persona
	$tamanoNombre=strlen($nombres);
	$tamanoApellido=strlen($apellidos);
	$tamanoDireccion=strlen($direccion);
	$tamanoTelefono=strlen($telefono);
		if(!$nombres){ $bC1 = true; $this->errors .= 'El nombre debe ser especificado<br>'; }
		if(!$apellidos){ $bC2 = true; $this->errors .= 'Los Apellidos deben ser especificados<br>'; }
		if(!$direccion){ $bC3 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if(!$telefono){ $bC4 = true; $this->errors .= 'El Tel�fono debe ser especificado<br>'; }
		if($tamanoNombre<3){ $bC6= true; $this->errors .= 'El nombre debe tener al menos 3 caracteres<br>'; }
		if($tamanoApellido<2){ $bC7= true; $this->errors .= 'El apellido debe tener al menos 2 caracteres<br>'; }
		if($tamanoDireccion<10){ $bC8= true; $this->errors .= 'La direcci�n debe tener al menos 10 caracteres<br>'; }
		if($tamanoTelefono<7){ $bC9= true; $this->errors .= 'El tel�fono debe tener al menos 7 caracteres<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC5 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }

		if($bC1||$bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC9){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('tramite_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaContacto($id,$nombres,$apellidos,$direccion,$telefono,$fax,$mail,$radio,$errors);
			
			$objIntranet->Footer();
		}else{

			$this->abreConnDB();
			//$this->conn->debug = true;
			//echo "holas".$id."holas";exit;
			$clave = $this->GeneraPassword(6);
			
			$sql_SP = sprintf("EXECUTE sp_insContacto %d,%s,%s,%s,%s,%s,%s,'%s','%s','%s'",
							  $id,
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "NULL",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "' '",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "' '",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "' '",
							  $_SESSION['cod_usuario'],
							  md5($clave),
							  ($radio) ? $radio : "P"
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				/*Ya no se se va a enviar por mail la clave
				//Para q se actualice el XML
				//$this->GeneraXMLRazonSocial($tipPersona);
						$desTitulo = "CLAVE PARA ACCESO A LA INFORMACI�N CONFIDENCIAL";
						$desMensaje = "Sr.".$apellidos." su clave es: ".$clave." ";
									
						$eFrom = "CONVENIO_SITRADOC@" . $this->emailDomain;
						$eDest = $mail;		
						// Envia el correo de Texto plano con el Adjunto
						$mail = & new htmlMimeMail();
						$mail->setSubject($desTitulo);
						$mail->setText($desMensaje);
						
						$mail->setFrom($eFrom);
						$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe');
						$mail->setReturnPath($eFrom);
						$result = $mail->send(array($eDest));
				//Fin de Para q se actualice el XML								
				*/
				
						$desTitulo = "CLAVE PARA ACCESO A LA INFORMACI�N CONFIDENCIAL";
						$desMensaje = "Sr.".$apellidos." su clave es: ".$clave." ";
									
						$eFrom = "CONVENIO_SITRADOC@" . $this->emailDomain;
						$eDest = $mail;		
						// Envia el correo de Texto plano con el Adjunto
						$mail = & new htmlMimeMail();
						$mail->setSubject($desTitulo);
						$mail->setText($desMensaje);
						
						$mail->setFrom($eFrom);
						$mail->setBcc('jtumay@CONVENIO_SITRADOC.gob.pe');
						$mail->setReturnPath($eFrom);
						$result = $mail->send(array($eDest));
				
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
				
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}&id={$id}&clave={$clave}";//Se manda el id de la Persona
			header("Location: $destination");
			exit;
		}
	}
	
	function ImprimeClave($id,$clave){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_SP = sprintf("Select CLAVE from contacto where id_persona=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				// Setea Campos del Formulario
				$html->assign('claveEncriptada',$row[0]);
				$html->assign('clave',$clave);
				//$html->assign('num',$row[1]);
				//$html->assign('rem',strtoupper($row[2]));
				//$html->assign('asunto',$row[3]);
				//$html->assign('folio',$row[4]);
				//$html->assign('fecRec',$row[5]);
				//$clave=$row[6].$row[7];
				//$html->assign('clave',$clave);
				//$html->assign('user',$row[8]);
				
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign_by_ref('FechaActual',$this->FechaActual());
				$html->assign_by_ref('HoraActual',$this->HoraActual());
				
				$html->display('oad/tramite/printClave.tpl.php');
			}
			$rs->Close();
		}
	}
	
	
	function DetallePersona($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql=sprintf("select s.descripcion,tp.descripcion,depa.departamento,prov.provincia,dist.distrito,tti.descripcion,
						case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
							 when p.id_tipo_persona=2 then p.razon_social end as RAZON_SOCIAL,p.nro_documento,p.direccion,p.telefono,p.fax,p.email,p.representante_legal,
						tti2.descripcion,p.nro_documento_representante
						from db_general.dbo.sector s,db_general.dbo.tipo_persona tp,
							 db_general.dbo.departamento depa,db_general.dbo.provincia prov,db_general.dbo.distrito dist,
							 db_general.dbo.t_tipo_identificacion tti,
							 db_general.dbo.persona p left join db_general.dbo.t_tipo_identificacion tti2 on p.id_tipo_identificacion_rep_leg=tti2.codigo_t_identificacion
						where p.id_sector=s.id and p.id_tipo_persona=tp.id_tipo_persona
						  and p.codigo_departamento=depa.codigo_departamento
						  and p.codigo_provincia=prov.codigo_provincia and prov.codigo_departamento=depa.codigo_departamento
						  and p.codigo_distrito=dist.codigo_distrito and dist.codigo_provincia=prov.codigo_provincia and dist.codigo_departamento=depa.codigo_departamento
						  and p.id_tipo_identificacion=tti.codigo_t_identificacion
						  and 
					      p.id=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$sector=$rs->fields[0];
			$tipPersona=$rs->fields[1];
			$departamento=$rs->fields[2];
			$provincia=$rs->fields[3];
			$distrito=$rs->fields[4];
			$tipIdent=$rs->fields[5];
			$RazonSocial=$rs->fields[6];
			$nroDoc=$rs->fields[7];
			$direccion=$rs->fields[8];
			$telefono=$rs->fields[9];
			$fax=$rs->fields[10];
			$email=$rs->fields[11];
			$RepLegal=$rs->fields[12];
			$tipIdentRep=$rs->fields[13];
			$nroDocRep=$rs->fields[14];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('sector',$sector);				  
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('departamento',$departamento);
		$html->assign_by_ref('provincia',$provincia);
		$html->assign_by_ref('distrito',$distrito);			
		$html->assign_by_ref('tipIdent',$tipIdent);	  
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('nroDoc',$nroDoc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('RepLegal',$RepLegal);
		$html->assign_by_ref('tipIdentRep',$tipIdentRep);
		$html->assign_by_ref('nroDocRep',$nroDocRep);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oad/tramite/directorio/printDetallesPersona.tpl.php') : $html->display('oad/tramite/directorio/showDetallesPersona.tpl.php');
	}
	
 function ImprimeDetalle($id){
  $this->DetallePersona($id, true);
 }
	
}
?>
