<?
require_once('claseModulos.inc.php');

class RecepcionVUSE extends Modulos{
	
	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function RecepcionVUSE(){
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	}
	
	/* ------------------------------------------------------ */
	/* Funcion para Mostrar los Modulos a los cuales		  */
	/* un determinado Grupo tendra derecho (Checkboxs)		  */
	/* ------------------------------------------------------ */
	
	function muestraModulosDerechosTR($idmod=NULL,$tip_integrante=0,$idusr=NULL,$idgrp=NULL){
		$sql_st_usr = "SELECT id_moduloderecho, cod_usuario, CASE WHEN ind_leer IS true THEN ' CHECKED' ELSE '' END,
									   CASE WHEN ind_insertar IS true THEN ' CHECKED' ELSE '' END,
									   CASE WHEN ind_modificar IS true THEN ' CHECKED' ELSE '' END ";
		$sql_st_usr .= (is_null($idusr)||empty($idusr)) ? "" : ", CASE WHEN cod_usuario='${idusr}' THEN '#EAEAEA' ELSE '#F6F6F6' END ";
		$sql_st_usr .= "FROM modulo_derecho ".
					   "WHERE cod_usuario IS NOT NULL ";
		$sql_st_usr .= (is_null($idmod)||empty($idmod)) ? "" : "and id_modulo=${idmod} ";
		$sql_st_usr .= "ORDER BY 2";

		$sql_st_grp = "SELECT id_moduloderecho, g.cod_grupo, CASE WHEN md.ind_leer IS true THEN ' CHECKED' ELSE '' END,
													   CASE WHEN md.ind_insertar IS true THEN ' CHECKED' ELSE '' END,
													   CASE WHEN md.ind_modificar IS true THEN ' CHECKED' ELSE '' END ";
		$sql_st_grp .= (is_null($idgrp)||empty($idgrp)) ? "" : ", CASE WHEN md.id_grupo=${idgrp} THEN '#EAEAEA' ELSE '#F6F6F6' END ";
		$sql_st_grp .= "FROM grupo g, modulo_derecho md ".
					   "WHERE g.id_grupo=md.id_grupo and md.id_grupo IS NOT NULL and g.ind_activo IS true ";
		$sql_st_grp .= (is_null($idmod)||empty($idmod)) ? "" : "and md.id_modulo=${idmod} ";
		$sql_st_grp .= "ORDER BY 2";

		$this->abreConnDB();
		
		if($tip_integrante==1){
			$sql_st = $sql_st_usr;
			$imagen = "/img/800x600/ico-usuario.gif";
		}elseif($tip_integrante==2){
			$sql_st = $sql_st_grp;
			$imagen = "/img/800x600/ico-grupo.gif";
		}

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$html_tr = NULL;
			while ($row = $rs->FetchRow()) {
				$color_td = (isset($row[5])) ? $row[5] : '#F6F6F6';
				$html_tr .= "
	  <tr>
	  <td class=\"texto\" bgcolor=\"${color_td}\" valign=\"middle\">
	    &nbsp;&nbsp;<img src=\"${imagen}\" border=\"0\" align=\"absmiddle\" hspace=\"1\">&nbsp;&nbsp;&nbsp;{$row[1]}
	  </td>
		<td align=\"center\" bgcolor=\"${color_td}\"> 
		  <input type=\"checkbox\" name=\"der[{$row[0]}][0]\" value=\"1\"{$row[2]}>
		</td>
		<td align=\"center\" bgcolor=\"${color_td}\"> 
		  <input type=\"checkbox\" name=\"der[{$row[0]}][1]\" value=\"1\"{$row[3]}>
		</td>
		<td align=\"center\" bgcolor=\"${color_td}\"> 
		  <input type=\"checkbox\" name=\"der[{$row[0]}][2]\" value=\"1\"{$row[4]}>
		</td>
	  </tr>";
			}
			unset($row);
			$rs->Close(); # optional
		}
		if(is_null($html_tr))
			return array(false);
		else
			return array(true,$html_tr);
	}
		
	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar el Formulario HTML para asignar			 */
	/* Derechos a un Grupo sobre los Modulos Existentes				 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function insertaModuloDerechoHTML($accion=ACCION_BUSCAR_FALSE, $id_modulo=NULL,$id_modulotipo=1,$idusr=NULL,$idgrp=NULL,$tip_integrante=1){
		$frmName = "frmModDer";
		$frmName2 = "frmModDer2";
		$jscript_st = "document.${frmName}.accion.value=" . ACCION_BUSCAR_FALSE . "\r\n";
		$this->insertaScriptSubmitForm($frmName,$jscript_st);
?>
  
<table width="540" border="0" cellspacing="1" cellpadding="2">
  <form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
    <input type="hidden" name="accion" value="<?=ACCION_BUSCAR_TRUE?>">
    <tr> 
      <td colspan="4" class="texto" width="540"> <hr width="100%" size="1"> </td>
    </tr>
    <tr> 
      <td colspan="4" width="540"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="300" align="center" bgcolor="#CCE7F9" class="texto"><b>PROCEDIMIENTO PESCA </b></td>
      <td width="240" align="center" bgcolor="#CCE7F9" class="texto"><b>DECLARACI&Oacute;N JURADA Diqpf </b></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#CCE7F9" class="texto"><a href="printCargo.php?a=1" target="_blank" onClick="window.open(this.href, this.target, 'width=300, height=200, scrollbars=yes, resizable=no, top=100'); return false;"><img src="/img/gerencia.jpg" width="120" height="110" border="0"></a></td>
      <td width="240" align="center" bgcolor="#CCE7F9" class="texto"><a href="printCargo.php?a=2" target="_blank" onClick="window.open(this.href, this.target, 'width=300, height=200, scrollbars=yes, resizable=no, top=100'); return false;"><img src="/img/gtupa-chico.jpg" width="120" height="110" border="0"></a></td>
    </tr>
    <tr> 
      <td width="300" align="center" bgcolor="#CCE7F9" class="texto"><b>&nbsp;</b></td>
      <td width="240" align="center" bgcolor="#CCE7F9" class="texto"><b></b></td>
    </tr>	
    <tr> 
      <td width="300" align="center" bgcolor="#CCE7F9" class="texto"><b>CONGRESO DE LA REP&Uacute;BLICA </b></td>
      <td width="240" align="center" bgcolor="#CCE7F9" class="texto"><b>OTROS EXTERNOS </b></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#CCE7F9" class="texto"><a href="printCargo.php?a=3" target="_blank" onClick="window.open(this.href, this.target, 'width=300, height=200, scrollbars=yes, resizable=no, top=100'); return false;"><img src="/img/administracion.gif" width="160" height="140" border="0"></a></td>
      <td width="240" align="center" bgcolor="#CCE7F9" class="texto"><a href="printCargo.php?a=4" target="_blank" onClick="window.open(this.href, this.target, 'width=300, height=200, scrollbars=yes, resizable=no, top=100'); return false;"><img src="/img/gestion1.gif" width="120" height="110" border="0"></a></td>
    </tr>    
		  
        </table></td>
    </tr>
    <tr> 
      <td colspan="4" class="texto" width="540"> <hr width="100%" size="1"> </td>
    </tr>
    <tr> 
      <td colspan="4" align="right" class="texto">
<input type="submit" class="but-login" value="Desplegar">
        &nbsp;&nbsp;</td>
    </tr>
  </form>
</table>
    <?
		if($accion==ACCION_BUSCAR_TRUE){
			$perms = $this->muestraModulosDerechosTR($id_modulo,$tip_integrante,$idusr,$idgrp);
			if($perms[0]){
    ?>
  <table width="540" border="0" cellspacing="1" cellpadding="2">
  <form name="<?=$frmName2?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
    <input type="hidden" name="tip_integrante" value="<?=$tip_integrante;?>">
    <input type="hidden" name="id_modulo" value="<?=$id_modulo;?>">
    <input type="hidden" name="accion" value="<?=ACCION_INSERTAR_TRUE?>">
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
    <tr> 
      <td width="300" rowspan="2" align="center" bgcolor="#CCE7F9" class="texto"><b>NOMBRE</b></td>
      <td colspan="3" align="center" width="240" class="texto" bgcolor="#CCE7F9"><b>DERECHOS</b></td>
    </tr>
    <tr> 
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Leer</td>
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Insertar</td>
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Modificar</td>
    </tr>
    <?
				echo $perms[1];
    ?>
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
    <tr align="center"> 
      <td colspan="4" width="540"> <input type="submit" value="INGRESAR DERECHOS" class="but-login" onClick="document.<?=$frmName;?>.accion.value=<?=ACCION_INSERTAR_TRUE?>;"> 
        &nbsp;&nbsp;&nbsp; <input type="reset" value="LIMPIAR DERECHOS" class="but-login" name="reset"> 
      </td>
    </tr>
  </form>
  </table>
<?
			}else{
				$msg = sprintf("No se ha encontrado ningun %s que tenga derechos sobre este M�dulo", ($tip_integrante==1) ? 'Usuario ' : 'Grupo ');
				$this->muestraMensajeInfo($msg);
			}
		}
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Insertar a la Base de Datos los derechos		 */
	/* asignados a un Grupo sobre determinados Modulos				 */
	/* ------------------------------------------------------------- */
	/* MODULO MODULO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function insertaModuloDerechosDB($id_modulo, $derechos, $tip_integrante){
		if(is_array($derechos)){
			while (list ($id_moduloderecho) = each ($derechos)){
				$upd_st[] = sprintf("UPDATE modulo_derecho ".
									"SET ind_leer='%s', ind_insertar='%s', ind_modificar='%s' ".
									"WHERE id_moduloderecho=%s",
									(isset($derechos[$id_moduloderecho][0])) ? '1' : '0',
									(isset($derechos[$id_moduloderecho][1])) ? '1' : '0',
									(isset($derechos[$id_moduloderecho][2])) ? '1' : '0',
									$id_moduloderecho);
				$ids_modder[] = $id_moduloderecho;
			}
		}
		$upd_st_clean = "UPDATE modulo_derecho ".
						"SET ind_leer='0', ind_insertar='0', ind_modificar='0' ".
						"WHERE id_modulo=${id_modulo}";
		if(is_array($ids_modder))
			$upd_st_clean .= " and id_moduloderecho NOT IN (". implode(",", $ids_modder) . ")";

		if($tip_integrante==1)
			$upd_st_clean .= " and cod_usuario IS NOT NULL";
		elseif($tip_integrante==2)
			$upd_st_clean .= " and id_grupo IS NOT NULL";
			
		// Abre la Conexion y comienza la Transacci�n
		$this->abreConnDB();
		$this->conn->BeginTrans();
		
		// Si hay derechos que cambiar
		if(is_array($upd_st)){
			for($i=0;$i<count($upd_st);$i++){
				if ($this->conn->Execute($upd_st[$i]) === false){
					print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
					$this->conn->RollBackTrans();
					return 0;
				}
			}
		}
		
		// Pone en nulo los derechos que no hayan sido declarados
		if ($this->conn->Execute($upd_st_clean) === false){
			print 'error actualizando: '.$this->conn->ErrorMsg().'<BR>';
			$this->conn->RollBackTrans();
			return 0;
		}
		$this->conn->CommitTrans();
		$msg = sprintf("El Proceso de Asignacion de Derechos de los %ss al Modulo se ha realizado correctamente.", ($tip_integrante==1) ? 'Usuario ' : 'Grupo ');
		$this->muestraMensajeInfo($msg);
	}

}
?>