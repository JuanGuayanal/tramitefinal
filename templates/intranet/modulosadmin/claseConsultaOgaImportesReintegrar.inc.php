<?
error_reporting(E_PARSE);
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

require_once "class.graphic.php";


class ConsultaOgaImportesReintegrar extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function ConsultaOgaImportesReintegrar($menu,$subMenu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		//$this->serverDB = $this->arr_serverDB['mssql'][4];
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][5];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_AGREGA_EMBARCACIONNAC =>'frmAddEmbarcacionNac',
							AGREGA_EMBARCACIONNAC => 'addEmbarcacionNac',
							FRM_BUSCA_EMBARCACIONNAC =>'frmSearchDatos',
							BUSCA_EMBARCACIONNAC =>'searchDatos',
							CREA_CSV => 'createCSV',							
							FRM_MODIFICA_EMBARCACIONNAC =>'frmModifyEmbarcacionNac',
							MODIFICA_EMBARCACIONNAC => 'modifyEmbarcacionNac',							
							MUESTRA_DETALLE => 'showDetail',
							IMPRIME_DETALLE => 'printDetail',
							FRM_BUSCA_ARMADOR =>'frmSearchArm',
							BUSCA_ARMADOR =>'searchArm',
							CREA_CSV2 => 'createCSV2',
							FRM_AGREGA_ARMADOR =>'frmAddArmador',
							AGREGA_ARMADOR => 'addArmador',
							FRM_MODIFICA_ARMADOR =>'frmModifyArmador',
							MODIFICA_ARMADOR => 'modifyArmador',
							MUESTRA_HIST_SUSP => 'showHistSusp',
							IMPRIME_HIST_SUSP => 'printHistSusp',
							MUESTRA_HIST_SUSP_PP => 'showHistSuspPP',
							IMPRIME_HIST_SUSP_PP => 'printHistSuspPP',
							MUESTRA_HIST_DUENO => 'showHistDueno',
							MUESTRA_DETALLE_RESOLUCION => 'showDetalleResol',
							MUESTRA_DETALLE_PERSONA => 'showDetailPer',
							FRM_GENERA_REPORTE => 'frmReportes',//Para sacar la matriz de incumplimiento
							GENERA_REPORTE => 'Reportes',
							FRM_REGISTRA_DECJUR_MENSUAL => 'frmRegDecJur',
							REGISTRA_DECJUR_MENSUAL => 'regDecJur',
							LISTADO_REPORTES => 'listadoReportes',
							LISTADO_DIGSECOVI_DGEPP => 'listadoDigsecoviDgepp',
							LISTADO_DIFERENCIAS_DESCARGAS_DJ => 'listadoDifDescargasDj',
							FRM_AGREGA_IMPORTE_PAGADO => 'frmAddImporte',
							AGREGA_IMPORTE_PAGADO => 'addImporte',
							FRM_MODIFICA_IMPORTE_PAGADO => 'frmModifyImporte',
							MODIFICA_IMPORTE_PAGADO => 'modifyImporte',
							FRM_BUSCA_IMPORTE =>'frmSearchImporte',
							BUSCA_IMPORTE =>'searchImporte',
							FRM_GENERA_REPORTE_IMPORTE => 'frmReportImporte',
							GENERA_REPORTE_IMPORTE => 'reportImporte',
							FRM_AGREGA_DECJURMENSUAL =>'frmAddDecJurMensual',
							AGREGA_DECJURMENSUAL =>'addDecJurMensual',
							DA_ACCESO_MOD_DJM =>'daAccesoModDjm',
							FRM_LISTADO_X_AUTORIZAR =>'frmListadoxAutorizar',
							LISTADO_X_AUTORIZAR =>'ListadoxAutorizar',
							DA_AUTORIZACION =>'daAutorizacion',
							AUTORIZA_CAMBIO => 'autorizaRechazaAutorizacion',
							FRM_MODIFICA_DECJURMENSUAL =>'frmModifyDecJurMensual',
							MODIFICA_DECJURMENSUAL =>'modifyDecJurMensual',
							FRM_BUSCA_DECJURMENSUAL =>'frmSearchDecJurMensual',
							BUSCA_DECJURMENSUAL =>'searchDecJurMensual',
							ANULA_DECJURMENSUAL => 'anulaDecJurMensual',
							FRM_BUSCA_DJM_CAPBOD =>'frmSearchDjmCapBod',
							BUSCA_DJM_CAPBOD =>'searchDjmCapBod',
							CREA_CSV_CAPBOD =>'creaCsvCapBod',
							FRM_MODIFICA_CAPBOD => 'frmModifyEmbCapBod',
							MODIFICA_CAPBOD => 'modifyEmbCapBod',
							FRM_AGREGA_NEW_CAPBOD => 'frmAddNewEmbCapBod',
							AGREGA_NEW_CAPBOD => 'addNewEmbCapBod',
							FRM_BUSCA_RPTA_LMC =>'frmSearchRptaLMC',
							BUSCA_RPTA_LMC =>'searchRptaLMC',
							MUESTRA_DETALLE_RPTA_LMCE => 'showDetailRptaLMCE',
							IMPRIME_DETALLE_RPTA_LMCE => 'printDetailRptaLMCE',
							MUESTRA_DETALLE_RPTA_LMCE2 => 'showDetailRptaLMCE2',
							IMPRIME_DETALLE_RPTA_LMCE2 => 'printDetailRptaLMCE2',
							FRM_BUSCA_HOJA_VIDA =>'frmSearchHojaVida',
							BUSCA_HOJA_VIDA =>'searchHojaVida',
							CREA_CSV_HOJA_VIDA =>'creaCsvHojaVida',
							FRM_GENERA_ESTADO_CUENTA_FLORENTINO => 'frmGeneraEstadoCuentaFlorentino',
							GENERA_ESTADO_CUENTA_FLORENTINO => 'generaEstadoCuentaFlorentino',
							FRM_GENERA_REPORTE_EVALUACION => 'frmGeneraReporteEvaluacion',
							CONSULTA_DECLARACIONJURADA_ANUAL => 'frmConsultaDjAnual',
							FRM_GENERA_REPORTE_DJ_ANUAL => 'frmReportDJAnual',
							GENERA_REPORTE_DJ_ANUAL => 'reportDJAnual',
							REPORTE_DECLARACIONJURADA_ANUAL => 'reporteDjAnual',
							FRM_GENERA_REPORTE_DJM => 'frmReportDjm',
							GENERA_REPORTE_DJM => 'reportDjm',
							LISTADO_DECLARACIONES_JURADAS_MENSUALES => 'listadoDJMensuales',
							REPORTE_CAPTURA_TOTAL_DECLARADA_EMB => 'reporteCapturaTotalDeclaradaxEmb',
							REPORTE_CAPTURA_REGIMEN => 'reporteCapturaxRegimen',
							FRM_BUSCA_REGISTRO_DETALLE_EMBARCACION =>'frmSearchRegDetalleEmb',
							BUSCA_REGISTRO_DETALLE_EMBARCACION =>'searchRegDetalleEmb',
							FRM_AGREGA_DETALLE_EMBARCACION =>'frmAddRegDetalleEmb',
							AGREGA_DETALLE_EMBARCACION =>'addRegDetalleEmb',
							FRM_BUSCA_IMPORTE_DIF_DJ_TOLVAS => 'frmSearchImpPagDJyTolvas',
							BUSCA_IMPORTE_DIF_DJ_TOLVAS => 'searchImpPagDJyTolvas',
							FRM_AGREGA_IMPORTE_DIF_DJ_TOLVAS=>'frmAddImporteDJvsTolvas',
							FRM_MODIFICA_IMPORTE_DIF_DJ_TOLVAS => 'frmModifyImporteDJvsTolvas',
							MODIFICA_IMPORTE_DIF_DJ_TOLVAS => 'modifyImporteDJvsTolvas',
							FRM_GENERA_REPORTE_DIF_DJ_TOLVAS => 'frmReportesDJvsTolvas',
							GENERA_REPORTE_DIF_DJ_TOLVAS => 'ReportesDJvsTolvas',
							AGREGA_IMPORTE_DIF_DJ_TOLVAS=>'addImporteDJvsTolvas',
							FRM_BUSCA_REGISTRO_TRANSFERENCIA_PROPIEDAD =>'frmSearchRegistroTransferenciaPropiedad',
							BUSCA_REGISTRO_TRANSFERENCIA_PROPIEDAD =>'searchRegistroTransferenciaPropiedad',
							CREA_CSV_REGISTRO_TRANSFERENCIA_PROPIEDAD =>'creaCsvRegistroTransferenciaPropiedad',
							FRM_AGREGA_REGISTRO_TRANSFERENCIA_PROPIEDAD => 'frmAddRegistroTransferenciaPropiedad',
							AGREGA_REGISTRO_TRANSFERENCIA_PROPIEDAD => 'addRegistroTransferenciaPropiedad',
							ANULA_REGISTRO_TRANSFERENCIA_PROPIEDAD => 'anulaRegistroTransferenciaPropiedad',
							FRM_GENERA_REPORTE_EVALUACION_DECJUR => 'frmGeneraReporteEvaluacionDJ',
							GENERA_REPORTE_EVALUACION_DECJUR => 'GeneraReporteEvaluacionDJ',
							GENERA_REPORTE_EVALUACION_DECJUR2007 => 'GeneraReporteEvaluacionDJ2007',
							GENERA_REPORTE_EVALUACION_DECJUR2009 => 'GeneraReporteEvaluacionDJ2009',
							GENERA_REPORTE_EVALUACION_DECJUR2009_2 => 'GeneraReporteEvaluacionDJ2009_2',
							LISTADO_REPORTES_INCUMPLIMIENTO => 'listadoReportesIncumplimiento',
							LISTADO_MONTOS_ADEUDADOS_2008 => 'listadoMontosAdeudados2008',
							LISTADO_MONTOS_ADEUDADOS_2009 => 'listadoMontosAdeudados2009',
							LISTADO_DIFERENCIAS_ADEUDOS_2004_2007 => 'listadoDiferenciasAdeudados2004_2007',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();
		
		
		$this->menu_items[0] = array('val' => 'frmSearchDatos', label => 'EMBARCACIONES');
		$this->menu_items[1] = array('val' => 'frmSearchArm', label => 'ARMADORES');
		$this->menu_items[2] = array('val' => 'frmReportes', label => 'REPORTES');
		$this->menu_items[3] = array('val' => 'frmRegDecJur', label => 'DECLARACI�N JURADA');
		
		
		// Menu Seleccionado
		//$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		$this->menuPager = ($menu) ? $menu : $this->menu_items[3]['val'];
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchDatos', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddEmbarcacionNac', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyEmbarcacionNac', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchArm', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddArmador', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyArmador', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[2]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmReportes', label => 'Reportes' )
										 );
		elseif($this->menuPager==$this->menu_items[3]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmRegDecJur', label => 'MENSUAL' ),
										 1 => array ( 'val' => 'frmAddImporte', label => 'IMPORTE PAGADO' ),
										 2 => array ( 'val' => 'frmSearchImporte', label => 'BUSCAR IMPORTE' ),
										 3 => array ( 'val' => 'frmModifyImporte', label => 'MODIFICA IMPORTE' ),
										 4 => array ( 'val' => 'frmReportImporte', label => 'REPORTES' )
										 );								 
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];										 
		
		$this->pathTemplate = 'dnepp/embarcacionnac/';		
    }
	
	function MuestraStatTrans($accion){
		global $otroModulo;

		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_EMBARCACIONNAC]);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_ARMADOR]);			
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		if($otroModulo==1){
			// Setea accion para el resultado Accion
			$html->assign_by_ref('accion',$this->arr_accion);
			$html->display('dnepp/diferenciasDjVSTolvas/headerArm.tpl.php');
			$html->display('dnepp/diferenciasDjVSTolvas/showStatTrans.inc.php');
		}else{	
			$html->display('dnepp/headerArmn.tpl.php');
			$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		}	
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormGeneraReporte();			
	}
	
	function XMLEmbarcacion(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$a=htmlspecialchars("<")."embarcaciones".htmlspecialchars(">");
				
		$sql_st = "SELECT id_emb, nombre_emb, substring(nombre_emb,1,1), matricula_emb
  FROM db_dnepp.user_dnepp.embarcacionnac
  ORDER BY 2";
	
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						/*while($AtencionData = $rs->FetchRow())
							$a.="<".$AtencionData[2]."><puerto><id>".$AtencionData[0]."</id><nombre>".$AtencionData[1]."</nombre></puerto></A>";
						$rs->Close();*/
						
						$ii=-1;
						while(!$rs->EOF){
							$ii++;
							$codigoPuerto[$ii]=$rs->fields[0];
							$descripcionPuerto[$ii]=$rs->fields[1];
							$firstCharPuerto[$ii]=$rs->fields[2];
							$matricula[$ii]=$rs->fields[3];
							
							if($ii==0){
								//$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[0].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
								$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[0]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[0]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
							}else{
								if($firstCharPuerto[$ii]!=$firstCharPuerto[$ii-1]){
									$a.=htmlspecialchars("</").$firstCharPuerto[$ii-1].htmlspecialchars(">");
									//$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}else{
									//$a.=htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}		
							}
							
							$rs->MoveNext();
						}
						$rs->Close();						
						
					}
		$contador=count($firstCharPuerto);			
		$a.=htmlspecialchars("</").$firstCharPuerto[$contador-1].htmlspecialchars(">");
		//$a.=htmlspecialchars("</")."puertos".htmlspecialchars(">");
		$a.=htmlspecialchars("</")."embarcaciones".htmlspecialchars(">");
		echo "<br>".$a;													
	
	
		/*$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('emb',array('id'=>$row[0],
										  'nomb'=>$row[1],
										  'matr'=>$row[2]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEmbarcaciones.tpl.php');*/
	
	}	
	
	function formateaTexto($cadena){
	
		$cadena= ereg_replace("          "," ",$cadena);
		$cadena= ereg_replace("         "," ",$cadena);
		$cadena= ereg_replace("        "," ",$cadena);
		$cadena= ereg_replace("       "," ",$cadena);
		$cadena= ereg_replace("      "," ",$cadena);
		$cadena= ereg_replace("     "," ",$cadena);
		$cadena= ereg_replace("    "," ",$cadena);
		$cadena= ereg_replace("   "," ",$cadena);
		$cadena= ereg_replace("  "," ",$cadena);
		
		return ($cadena);
	}
	
	function formateaAcentos($cadena){
	
		$cadena= ereg_replace("�","a",$cadena);
		$cadena= ereg_replace("�","e",$cadena);
		$cadena= ereg_replace("�","i",$cadena);
		$cadena= ereg_replace("�","o",$cadena);
		$cadena= ereg_replace("�","u",$cadena);
		$cadena= ereg_replace("�","A",$cadena);
		$cadena= ereg_replace("�","E",$cadena);
		$cadena= ereg_replace("�","I",$cadena);
		$cadena= ereg_replace("�","O",$cadena);
		$cadena= ereg_replace("�","U",$cadena);
		
		return ($cadena);
	}
	
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	}	
	
	
	function FormGeneraReporte($GrupoOpciones1=NULL,$armador=NULL,$embarcacion=NULL,$matricula_emb=NULL){
		global $idRegimen,$idDestino,$porcentaje,$nroDeclaraciones,$anyo,$servidor;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		/*$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);*/
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReportes';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		$html->assign_by_ref('porcentaje',$porcentaje);
		$html->assign_by_ref('nroDeclaraciones',$nroDeclaraciones);
		$html->assign_by_ref('anyo',$anyo);
		$html->assign_by_ref('servidor',$servidor);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		
		$html->assign_by_ref('estado',$idEstado);
		$html->assign_by_ref('codUsuario',$_SESSION['cod_usuario']);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		$menu=3;
		$html->assign_by_ref('menu',$menu);
		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display('dnepp/reportes/oga/headerArmn.tpl.php');
		$html->display('dnepp/reportes/oga/reportes.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function GeneraReporte($GrupoOpciones1,$armador,$embarcacion,$matricula_emb){
		global $idRegimen,$idDestino,$porcentaje,$nroDeclaraciones,$anyo,$servidor;
			$this->ReporteMontosAdeudados2008($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen,$anyo);
	}
	
	function calculaInteres($monto,$fechaInicial,$fechaPago){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$interes=$monto;
		/*$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<dateadd(dd,1,getDate()) 
					ORDER BY convert(datetime,fecha_tasa,103)";*/
		if($fechaPago && $fechaPago!="")
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<convert(datetime,'$fechaPago',103) 
					ORDER BY convert(datetime,fecha_tasa,103)";
		else
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<getDate() 
					ORDER BY convert(datetime,fecha_tasa,103)";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row=$rs->FetchRow()){
				//$interes=$interes+$interes*($row[0]/36500);
				$interes=$interes+$interes*(pow((1+($row[0]/100)),(1/360))-1);
				//echo $interes."<br>";
			}	
			$rs->Close();
		}
		unset($rs);		
		//$interes=$monto*$tasa;
		$interesTotal=$interes-$monto;
		return($interesTotal);
	}	
	
function compara_fechas($fecha1,$fecha2)
            
 
{
            
 
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("/",$fecha1);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("-",$fecha1);
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("/",$fecha2);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("-",$fecha2);
        $dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
        return ($dif);                         
            
 
}	
	
	function formateaNumero($cadena){
		//$cadena= ereg_replace(",",".",$cadena);
		$cadena= ereg_replace(",","",$cadena);
		
		return ($cadena);
	}

	function ReporteMontosAdeudados2008($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen,$anyo,$print=false){
		//echo "DEstino:".$idDestino."-Porcentaje:".$porcentaje;exit;
		//echo "matrix".$idDestino;
		$html = new Smarty;
		
		$this->abreConnDB();
		//$this->conn->debug=true;
		
	if($anyo==2009){
			$sql_SP="SELECT emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb,emb.capbod_emb,S.TOTAL_A_PAGAR2009,
				USER_DNEPP.f_Nombre_PersonaxEmbarcacion_Vigente(eMB.id_emb),S.FECHA
from user_dnepp.embarcacionnac emb,webminpro.SALDOS_FINALES_2009 S
where emb.codpag_emb is not null
and emb.id_emb in (select id_embarcacion from dbo.declaracion_jurada_mensual where anyo_djm=2009 and estado_djm=1)
AND EMB.ID_EMB=S.ID_EMB
AND CONVERT(DATETIME,S.FECHA,103) in (select max(CONVERT(DATETIME,FECHA,103)) 
								   	  		  from webminpro.SALDOS_FINALES_2009
									  		  where CONVERT(DATETIME,FECHA,103)<=convert(datetime,'".date('d/m/Y')."',103)
									 )
/*and S.TOTAL_A_PAGAR2008>0*/
						 ";
	}else{
			$sql_SP="SELECT emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb,emb.capbod_emb,S.TOTAL_A_PAGAR2008,
				USER_DNEPP.f_Nombre_PersonaxEmbarcacion_Vigente(eMB.id_emb),S.FECHA
from user_dnepp.embarcacionnac emb,webminpro.SALDOS_A_PAGAR_2008 S
where emb.codpag_emb is not null
and emb.id_emb in (select id_embarcacion from dbo.declaracion_jurada_mensual where anyo_djm=2008 and estado_djm=1)
AND EMB.ID_EMB=S.ID_EMB
AND CONVERT(DATETIME,S.FECHA,103) in (select max(CONVERT(DATETIME,FECHA,103)) 
								   	  		  from webminpro.SALDOS_A_PAGAR_2008
									  		  where CONVERT(DATETIME,FECHA,103)<=convert(datetime,'".date('d/m/Y')."',103)
									 )
/*and S.TOTAL_A_PAGAR2008>0*/
						 ";		
		}			
		if($idRegimen>0)
			$sql_SP.=" and emb.id_regimen=$idRegimen ";
		if($embarcacion && $embarcacion!="")
			$sql_SP.=" and emb.nombre_emb like '%{$embarcacion}%' ";
		if($matricula_emb && $matricula_emb!="")
			$sql_SP.=" and emb.matricula_emb like '%{$matricula_emb}%' ";
		if($armador && $armador!="")
			$sql_SP.=" and user_dnepp.f_nombre_personaxembarcacion_vigente(emb.id_emb) like '%".$armador."%'";
		/*if($anyo>0)	
			$sql_SP.=" and EMB.ID_EMB IN (SELECT ID_EMBARCACION FROM DECLARACION_JURADA_MENSUAL WHERE ESTADO_DJM=1 AND ANYO_DJM=".$anyo.") ";
		*/
		$sql_SP .= " ORDER BY emb.nombre_emb";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
			
			//Primero se consulta si existe un saldo a favor del 2007 de la embarcaci�n si es que debiese
				
					unset($saldoFavor2007_2008);
					if($anyo==2008){//
					$sql__S="select SALDO from SALDOS2007PARAEL2008 where ID_EMB=".$row[0];
			
					$rs__S = & $this->conn->Execute($sql__S);
								unset($sql__S);
								if (!$rs__S){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$saldoFavor2007_2008=$rs__S->fields[0];
								}
								if(!$saldoFavor2007_2008){
									$saldoFavor2007_2008=0;
								}
					}//
					unset($nuevoMontoaPagar14082009);
					if($row[5]>0 && $saldoFavor2007_2008>0){
						$nuevoMontoaPagar14082009=$row[5]-$saldoFavor2007_2008;
						if($nuevoMontoaPagar14082009<0){//Puede ser con el saldo ya cubri� toda la deuda por lo que el importe es cero
							$nuevoMontoaPagar14082009=0;
						}
					}else{
						$nuevoMontoaPagar14082009=$row[5];
					}
								
						$html->append('list',array('idEmb'=>$row[0],
												   'emb'=>$row[1],
												   'matri'=>$row[2],
												   'codPago'=>$row[3],
												   'importe'=>number_format($row[5],2),
												   'armador'=>$row[6],
												   'saldo2007'=>number_format($saldoFavor2007_2008,2),
												   'nuevMontoPagar2008'=>number_format($nuevoMontoaPagar14082009,2)
												   ));
						$fechaUltimaCorrida=$row[7];
			}							   
			$rs->Close();
		}
		unset($rs);
		
		
		//$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistDuenos.tpl.php');
		$sql="select convert(varchar,getDate(),103)+' '+convert(varchar,getDate(),108)";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			$fechaGen=$rs->fields[0];
		}
		$html->assign_by_ref('fechaUltimaCorrida',$fechaUltimaCorrida);
		$html->assign_by_ref('fechaGen',$fechaGen);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		$html->assign_by_ref('idRegimen',$idRegimen);
		$html->assign_by_ref('idDestino',$idDestino);
		$html->assign_by_ref('porcentaje',$porcentaje);
		$html->assign_by_ref('anyo',$anyo);
		$html->assign_by_ref('print',$print);
		
		$html->assign_by_ref('accion',$this->arr_accion);
		
		if($print==1){
			$html->display('dnepp/reportes/printReportIncumplimiento.tpl.php');
		}elseif($print==2)	{
					$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/pie-logo.gif';		
					$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
					$filename = 'hoja1'.mktime();
					$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/reportes/oga/reportMontosReintegrar2008OgaPdf.tpl.php'),true,$logo);
			
					$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
					header("Content-type: application/pdf");
					header("Location: {$destination}");
					//echo $destination;
					exit;		
		}elseif($print==3){
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('dnepp/reportes/oga/viewExtendMontosReintegrar2008.tpl.php');
			exit;		
		}else{
			$html->display('dnepp/reportes/oga/reportMontosReintegrar2008Oga.tpl.php');
		}
	}
	
	
}
?>
