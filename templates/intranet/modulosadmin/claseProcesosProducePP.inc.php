<?php
error_reporting(E_PARSE);
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class claseProcesosProducePP extends Modulos{

    function claseProcesosProducePP($menu){
	
		//echo "Pagina no disponible";exit;
	
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][11];
		$this->userDB = $this->arr_userDB['mssql'][1];
		$this->passDB = $this->arr_passDB['mssql'][1];
				
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCA_EXP => 'frmSearchNew',
							BUSCA_EXP => 'searchNew',
							FRM_COMPLETA_DOC => 'frmUpdateNew',
							COMPLETA_DOC => 'updateNew',
							FRM_AGREGA_PROCESO => 'frmAddProc',
							AGREGA_PROCESO => 'addProc',							
							FRM_ADDOBS_DOCDIR =>'frmAddObsDocDir',
							ADDOBS_DOCDIR =>'addObsDocDir',
							MUESTRA_DETALLE_DOCDIR => 'showDetailDocDir',
							IMPRIME_FLUJO =>'ImprimeFlujo',
							FRM_GENERAREPORTE=>'frmGeneraReporte',
							GENERAREPORTE=>'GeneraReporte',
							REPORTE1=>'Reporte1',
							FRM_FINALIZA_PROCESO => 'frmFinalizaProceso',
							FINALIZA_PROCESO => 'finalizaProceso',
							MUESTRA_EXPEDIENTE => 'showExpediente',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		// Obtiene los Datos del Usuario en la DB MSSQL
		$this->datosUsuarioMSSQL();
		//$_SESSION['coddep'] = $this->userIntranet['COD_DEP'];


		/*
		if(!$this->userIntranet){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}
		*/
		/* Si el Usuario no pertenece a una Dependencia
		   Permitida, no se instancia la Clase */ 
		/*
		if($this->userIntranet['COD_DEP']!=18){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no pertenece a una Dependencia permitida para la Operaci�n de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}
		*/

		/* Si el Usuario no tiene los Derechos m�nimos
		   sobre el Modulo, no se instancia la Clase */ 
		/*if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Noticias del Portal Produce',false,array('noticias'));
			$objIntranet->Body('noticiasportal_tit.gif',false,false);
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer(false);

			return $this = false;
		}*/
		

				
				 // || $this->userIntranet['COD_DEP']==13
		
		
		// Items del Menu Principal
		/*
		if($_SESSION['mod_ind_leer']) $this->menu_items[0] = array ( 'val' => 'frmSearchNew', label => 'BUSCAR' );
		if($_SESSION['mod_ind_insertar']) $this->menu_items[1] = array ( 'val' => 'frmAddNew', label => 'AGREGAR' );
		if($_SESSION['mod_ind_modificar']) $this->menu_items[2] = array ( 'val' => 'frmModifyNew', label => 'MODIFICAR' );
		*/
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchNew', label => 'BUSCAR' ),
		                    1 => array ( 'val' => 'frmAddProc', label => 'AGREGAR'),
							//2 => array ( 'val' => 'frmUpdateNew', label => 'COMPLETAR' ),
							2 => array ( 'val' => 'frmGeneraReporte', label => 'REPORTE')
							);
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
    }
	
	function ValidaFechaNoticia($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager(4));

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display('pp/headerArmn.tpl.php');
		$html->display('pp/showStatTrans.inc.php');
		$html->display('pp/footerArm.tpl.php');
	}
	
	function FormBuscaDocumento($page=NULL,$tipDocumento=NULL,$tipBusqueda=NULL,$nroTD=NULL,$asunto=NULL,$observaciones=NULL,$fecIniDir=NULL,$fecFinDir=NULL,$search=false){
		global $materia2,$correlativo2,$exp,$exp2,$exp3;
		global $a;
		global $desFechaIni;
		global $nroTD,$checkTodos,$checkCorrelativo,$checkJuzgado,$checkSalaSup,$checkCorteSup,$checkRemi,$FechaIni,$FechaFin,
			   $checkNroExpediente,$checkAbogado,$checkDistrito,$checkPartesProc,$checkEmb,$checkResol,$checkJuzgadoPaz,$estado,$etapaProcesal,$naturaleza2,$checkMedCau;
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir) ? $fecIniDir : "08/01/2005";
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir) ? $fecFinDir : date('m/d/Y');

		// Genera HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipDocumento',$tipDocumento);
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('procedimiento',$procedimiento);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('fecIniDir',$fecIniDir);		
		$html->assign_by_ref('materia2',$materia2);		
		$html->assign_by_ref('correlativo2',$correlativo2);		
		$html->assign_by_ref('a',$a);		
		$html->assign_by_ref('exp',$exp);		
		$html->assign_by_ref('exp2',$exp2);		
		$html->assign_by_ref('exp3',$exp3);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		
		$html->assign_by_ref('nroTD',$nroTD);
		$html->assign_by_ref('checkTodos',$checkTodos);
		$html->assign_by_ref('checkCorrelativo',$checkCorrelativo);
		$html->assign_by_ref('checkJuzgado',$checkJuzgado);
		$html->assign_by_ref('checkSalaSup',$checkSalaSup);
		$html->assign_by_ref('checkCorteSup',$checkCorteSup);
		$html->assign_by_ref('checkRemi',$checkRemi);
		$html->assign_by_ref('checkNroExpediente',$checkNroExpediente);
		$html->assign_by_ref('checkAbogado',$checkAbogado);
		$html->assign_by_ref('checkDistrito',$checkDistrito);
		$html->assign_by_ref('checkPartesProc',$checkPartesProc);
		$html->assign_by_ref('checkEmb',$checkEmb);
		$html->assign_by_ref('checkResol',$checkResol);
		$html->assign_by_ref('checkJuzgadoPaz',$checkJuzgadoPaz);
		$html->assign_by_ref('estado',$estado);
		$html->assign_by_ref('checkMedCau',$checkMedCau);
		
		$html->assign_by_ref('FechaIni',$FechaIni);
		$html->assign_by_ref('FechaFin',$FechaFin);
		
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.ETAPA_PROCESAL ".
					  "ORDER BY 1");
		$html->assign_by_ref('selEtapaProcesal',$this->ObjFrmSelect($sql_st, $etapaProcesal, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.naturaleza ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia2)||!empty($materia2)) ? $materia2 : 0);
		$html->assign_by_ref('selNaturaleza2',$this->ObjFrmSelect($sql_st, $naturaleza2, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$accionHeader=0;
		$html->assign_by_ref('accionHeader',$accionHeader);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecIniDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecIniDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecIniDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFinDir,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFinDir,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y'), date('Y'), substr($fecFinDir,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
		
		// Muestra el Formulario

		
		
		
		
				if($this->userIntranet['COD_DEP']==15)
				{
					$html->display('pp/headerArmn.tpl.php');
					$html->display('pp/procesosProduce/search.tpl.php');
					if(!$search) $html->display('pp/footerArm.tpl.php');
				}else{
				
					//$html->display('pp/headerArmn.tpl.php');
					echo '<center>El usuario no tiene autorizaci�n para ingresar a la Aplicaci�n.</center><br>';
					//$html->display('pp/procesosProduce/search.tpl.php');
					/*echo 	"<script>alert('El usuario no tiene autorizaci�n para ingresar a la Aplicaci�n.');</script>";
					*/
					//$html->display('pp/footerArm.tpl.php');
					exit;
				}
		
	}
	
	function MuestraIndex(){
		//echo "justomatrix is here";
		
		$this->FormBuscaDocumento();
	}

	function busper($dat,$j){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$codigoDependencia=15;
		$sql="select Upper(apellidos_trabajador)+' '+Upper(nombres_trabajador)
				from db_general.dbo.h_trabajador t,movimiento_documento md,movimiento_tratamiento mt,dirigido d
				where md.id_documento=$dat 
				and md.derivado=0 and md.id_dependencia_destino=". $codigoDependencia ."  
				and md.id_movimiento_documento=mt.id_movimiento
				and mt.derivado=0  
     			and d.id_movimiento_tratamiento=mt.id_movimiento_tratamiento and d.codigo_persona=t.codigo_trabajador
		        and d.id_dirigido=0
				and t.coddep=". $codigoDependencia ." "; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$persona=$rs->fields[0];
						$persona=strtoupper($persona);
						$rs->Close();
			}
			unset($rs);
		switch($j){
			case 1:
				return($persona);
				break;
		}
	}	

	function buscaOtrasRZ($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
		                  when sol.id_tipo_persona=2 then sol.razon_social end
				from db_general.dbo.persona sol,db_procuraduria.dbo.RZ_procuraduria rz
				where rz.id_proceso=$dat 
		        and rz.id_persona=sol.id
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function buscaMedCautelar($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,auditmod,103)+': '+descripcion
				from db_procuraduria.dbo.medida
				where id_proceso=$dat 
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function buscaEtapas($dat){//haya la ubicaci�n (Trabajador o trabajadores) actual del Documento Externo o Expediente
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,auditmod,103)+': '+descripcion
				from db_procuraduria.dbo.etapas
				where id_proceso=$dat 
				"; 
		//Esta es una prueba
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[0];
						$rs->MoveNext();
						}
	
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
			}
			unset($rs);
			
			return($depe1);

	}	

	function BuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir){
		global $fecIniDir2,$fecFinDir2,$page,$materia2,$correlativo2,$exp,$exp2,$exp3;
		global $desFechaIni;
		global $nroTD,$checkTodos,$checkCorrelativo,$checkJuzgado,$checkSalaSup,$checkCorteSup,$checkRemi,$FechaIni,$FechaFin,
			   $checkNroExpediente,$checkAbogado,$checkDistrito,$checkPartesProc,$checkEmb,$checkResol,$checkJuzgadoPaz,$estado,$etapaProcesal,$naturaleza2,$checkMedCau;
		//Manipulacion de las Fechas
		$fecIniDir = ($fecIniDir!='//'&&$fecIniDir&&!strstr($fecIniDir,'none')) ? $fecIniDir : NULL;
		$fecFinDir = ($fecFinDir!='//'&&$fecFinDir&&!strstr($fecFinDir,'none')) ? $fecFinDir : NULL;
		if($fecIniDir2&&$fecFinDir2){
			$fecIniDir=$fecIniDir2;
			$fecFinDir=$fecFinDir2;
		}
		/*
		if($fecIniDir){$bFIng = $this->ValidaFechaProyecto($fecIniDir,'Inicio');}
		if($fecFinDir){ $bFSal = ($this->ValidaFechaProyecto($fecFinDir,'Fin')); }
		*/
		if ($fecIniDir&&$fecFinDir){

		$mes_ini=substr($fecIniDir,0,2);
		$mes_fin=substr($fecFinDir,0,2);
		$dia_ini=substr($fecIniDir,3,2);
		$dia_fin=substr($fecFinDir,3,2);
		$anyo_ini=substr($fecIniDir,6,4);
		$anyo_fin=substr($fecFinDir,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecIniDir, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFinDir, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "error en los meses";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "error en los dias";$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);exit;}
					}
			}
		}		

		}
		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
			//exit;
			$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$errors);
			
		}else{
		
		//$this->abreConnDB();
		//$this->conn->debug = true;
		/*Inicio de la parte Original: No se tiene en cuenta que se va a realizar una consulta sin listado de documentos
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarDocDir';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocDir($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,true);
		
		Fin de la parte Original*/
		/*Inicio de la prueba del Listado de Documentos*/
		// Setea datos del Formulario
		//Esta parte lo pongo para que se muestre un resultado de a lo m�s 20 resultados.
		//$this->numMaxResultsSearch = 20;
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDocumento($page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,true);

		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign('tipDocumento',$tipDocumento);
		$html->assign('tipBusqueda',$tipBusqueda);

		// Setea datos del Formulario
		$html->assign('frmName','frmSearchExtended');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		$html->assign('datos',array('tipDocumento'=>$tipDocumento,
									'tipBusqueda'=>$tipBusqueda,
									'nroTD'=>$nroTD,
									'asunto'=>$asunto,
									'observaciones'=>$observaciones,
									'procedimiento'=>$procedimiento,
									'fecIniDir'=>$fecIniDir,
									'fecFinDir'=>$fecFinDir,
									'indicativo'=>$indicativo,
									'siglasDep'=>$siglasDep,
									'tipodDoc'=>$tipodDoc,
									'page'=>$page,
									'materia2'=>$materia2,
									'correlativo2'=>$correlativo2,
									'exp'=>$exp,
									'exp2'=>$exp2,
									'exp3'=>$exp3,
									'desFechaIni'=>$desFechaIni,
									'nroTD'=>$nroTD,
									'checkTodos'=>$checkTodos,
									'checkCorrelativo'=>$checkCorrelativo,
									'checkJuzgado'=>$checkJuzgado,
									'checkSalaSup'=>$checkSalaSup,
									'checkCorteSup'=>$checkCorteSup,
									'checkRemi'=>$checkRemi,
									'FechaIni'=>$FechaIni,
									'FechaFin'=>$FechaFin,
									'checkNroExpediente'=>$checkNroExpediente,
									'checkAbogado'=>$checkAbogado,
									'checkDistrito'=>$checkDistrito,
									'checkPartesProc'=>$checkPartesProc,
									'checkEmb'=>$checkEmb,
									'checkResol'=>$checkResol,
									'checkJuzgadoPaz'=>$checkJuzgadoPaz,
									'estado'=>$estado,
									'naturaleza2'=>$naturaleza2,
									'checkMedCau'=>$checkMedCau
									));

		/*Fin de la prueba del Listado de Documentos*/
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		$codigoDependencia=15;
				
		// Condicionales Segun ingreso del Usuario
		
		if(!empty($nroTD)&&!is_null($nroTD)){
			if($checkTodos==1){
				$condConsulta[] = "(Upper(p.correlativo) LIKE Upper('%{$nroTD}%') OR Upper(p.exp1) LIKE Upper('%{$nroTD}%') OR Upper(p.exp2) LIKE Upper('%{$nroTD}%') OR Upper(p.exp3) LIKE Upper('%{$nroTD}%') )";
			}elseif($checkCorrelativo==1/*&&!$checkTodos&&!$checkJuzgado&&!$checkSalaSup&&!$checkCorteSup*/){
				$condConsulta[] = "Upper(p.correlativo) LIKE Upper('%{$nroTD}%')";
			}elseif($checkJuzgado==1/*&&!$checkTodos&&!$checkCorrelativo&&!$checkSalaSup&&!$checkCorteSup*/){
				$condConsulta[] = "Upper(p.juzgado) LIKE Upper('%{$nroTD}%')";
			}elseif($checkSalaSup==1/*&&!$checkTodos&&!$checkCorrelativo&&!$checkJuzgado&&!$checkCorteSup*/){
				$condConsulta[] = "Upper(p.sala) LIKE Upper('%{$nroTD}%')";
			}elseif($checkCorteSup==1/*&&!$checkTodos&&!$checkCorrelativo&&!$checkJuzgado&&!$checkSalaSup*/){
				$condConsulta[] = "Upper(p.corte) LIKE Upper('%{$nroTD}%')";
			}elseif($checkJuzgadoPaz==1){
				$condConsulta[] = "Upper(p.juzgado_paz) LIKE Upper('%{$nroTD}%')";
			}elseif($checkNroExpediente==1){
				$condConsulta[] = "(Upper(p.exp1) LIKE Upper('%{$nroTD}%') OR Upper(p.exp2) LIKE Upper('%{$nroTD}%') OR Upper(p.exp3) LIKE Upper('%{$nroTD}%') OR Upper(p.EXPEDIENTE_JUZGADO_PAZ) LIKE Upper('%{$nroTD}%'))";
			}elseif($checkAbogado==1){
				$condTable[]="DB_GENERAL.dbo.H_TRABAJADOR tr";
				$condConsulta[] = "tr.codigo_trabajador=p.id_abogado and (Upper(tr.apellidos_trabajador) LIKE Upper('%{$nroTD}%') OR Upper(tr.nombres_trabajador) LIKE Upper('%{$nroTD}%'))";
			}elseif($checkDistrito==1){
				$condTable[]="distritojudicial dist";
				$condConsulta[] = "dist.codigo_distritojudicial=p.codigo_distritojudicial and Upper(dist.descripcion) LIKE Upper('%{$nroTD}%') ";
			}elseif($checkPartesProc==1){
				$condConsulta[] = "p.id in (select distinct p.id_proceso from rz_procuraduria p,db_general.dbo.persona sol where p.id_persona=sol.id and (Upper(sol.razon_social) LIKE Upper('%{$nroTD}%') OR Upper(sol.apellidos) LIKE Upper('%{$nroTD}%')) )";
			}elseif($checkEmb==1){
				//$condConsulta[] = "p.id in (select distinct embxproc.id_proceso from embxproceso embxproc,db_dnepp.user_dnepp.embarcacionnac emb where embxproc.id_emb=emb.id_Emb and Upper(emb.nombre_emb) LIKE Upper('%{$nroTD}%')) ";
				$condConsulta[] = "(p.id in (select distinct embxproc.id_proceso from embxproceso embxproc,db_dnepp.user_dnepp.embarcacionnac emb where embxproc.id_emb=emb.id_Emb and Upper(emb.nombre_emb) LIKE Upper('%{$nroTD}%')) 
									OR
									p.embclonada1 LIKE Upper('%{$nroTD}%') or p.embclonada2 LIKE Upper('%{$nroTD}%') or p.embclonada3 LIKE Upper('%{$nroTD}%')
									)
									";
			}elseif($checkResol==1){
				$condConsulta[] = "(p.id in (select distinct resolxproc.id_proceso from resolimpugnadaxproceso resolxproc,db_tramite_documentario.dbo.resolucion r where resolxproc.id_resolucion=r.id and Upper(r.nro_resol) LIKE Upper('%{$nroTD}%') and r.coddep in (25,47,48,24,16,36)) 
									or p.id in (select p.id from proceso p,db_tramite_documentario.dbo.resolucion r where p.id_resolucion_autoritativa=r.id and Upper(r.nro_resol) LIKE Upper('%{$nroTD}%') and r.coddep=5)
									)";
			}
		}
		if(!empty($FechaIni)&&!is_null($FechaIni)&&!empty($FechaFin)&&!is_null($FechaFin)){
				//$condConsulta[] = "p.fecVenc='$FechaIni'";
				$condConsulta[] = "p.fecvenc<>'NULL'";
				$condConsulta[] = "convert(datetime,p.fecVenc,103)>=convert(datetime,'$FechaIni',103)";
				$condConsulta[] = "convert(datetime,p.fecVenc,103)<=convert(datetime,'$FechaFin',103)";
		}
		
		if($estado==1)
			$condConsulta[]="p.estado_proceso is null";
		elseif($estado==2)
			$condConsulta[]="p.estado_proceso=1";

			if(($materia2>=1&& $materia2<=4) || $materia2==7){
				$condConsulta[]="p.materia=$materia2";
			}
			if($naturaleza2>0){
				$condConsulta[]="p.id_naturaleza=$naturaleza2";
			}
			if($checkMedCau==1){
				$condConsulta[]="p.id in (select id_proceso from db_procuraduria.dbo.medida)";
			}
			if(!empty($correlativo2)&&!is_null($correlativo2)){
				$condConsulta[] = "Upper(p.correlativo) LIKE Upper('%{$correlativo2}%')";
			}
			if(!empty($exp)&&!is_null($exp))
				$condConsulta[] = "Upper(p.exp1) LIKE Upper('%{$exp}%')";
			if(!empty($exp2)&&!is_null($exp2))
				$condConsulta[] = "Upper(p.exp2) LIKE Upper('%{$exp2}%')";
			if(!empty($exp3)&&!is_null($exp3))
				$condConsulta[] = "Upper(p.exp3) LIKE Upper('%{$exp3}%')";
			if(!empty($desFechaIni)&&!is_null($desFechaIni))
				$condConsulta[] = "p.fecVenc='$desFechaIni'";
			
			if($etapaProcesal>0 && $etapaProcesal!="none")
				$condConsulta[] = "p.id_etapa_procesal=$etapaProcesal";

		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;

			$sql_SP = sprintf("EXECUTE DB_PROCURADURIA.dbo.sp_busIdProc %s,%s,0,0",
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');

		//echo $sql_SP;
		//exit;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
//			echo "edwfw  :".$start;exit;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE  DB_PROCURADURIA.dbo.sp_busIdProc %s,%s,%s,%s",
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);

			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){	
					// Obtiene Todos los Datos del documento
						$sql_SP = sprintf("EXECUTE  DB_PROCURADURIA.dbo.sp_busDatosProc %d",$id[0]);

					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);

					if(!$rs)
						print $this->conn->ErrorMsg();
					else{						   
						while($DocumentoData = $rs->FetchRow()){
							$ruta="/var/www/intranet/institucional/aplicativos/pp/procesosProduce/archivos/".$id[0].".doc";
							$valRuta=file_exists($ruta);
							
							$html->append('arrDocDir', array('id' => $id[0],
														  'sol' => ucwords($DocumentoData[0]),
														  'nroTD' => $DocumentoData[1],
														  'natu' => $DocumentoData[2],
														  'monto' => ucfirst($DocumentoData[3]),
														  'tipProceso' => strtoupper($DocumentoData[4]),
														  'tipResponsabilidad' => $DocumentoData[5],
														  'distJud' => $DocumentoData[6],
														  'materia' => $DocumentoData[7],
														  'tipDoc' => $tipDocumento,
														  'juzgado' => $DocumentoData[8],
														  'exp1' => $DocumentoData[9],
														  'obsfinal' => $DocumentoData[10],
														  'obsRev' => $DocumentoData[11],
														  'nivel1' => $DocumentoData[7],
														  'nivel2' => $DocumentoData[8],
														  'nivel3' => $DocumentoData[9],
														  'sePractCor' => $DocumentoData[15],
														  'seCreaNoti' => $DocumentoData[14],
														  'proc' => $DocumentoData[16],
														  'cor' => $DocumentoData[14],
														  'fecVenc' => $DocumentoData[15],
														  'estado' => $DocumentoData[16],
														  'trabajador' => $DocumentoData[17],
														  'valRuta' => $valRuta,
														  'otrasRZ' => $this->buscaOtrasRZ($id[0]),
														  'usuario_reg' => $DocumentoData[18],
														  'usuario_actual' => $_SESSION['cod_usuario']
														  ));

						}
														  
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		$html->assign_by_ref('dependenciaJ',$codigoDependencia);
		//$html->assign_by_ref('dire',$this->SearchPrivilegios($this->userIntranet['CODIGO']));
		//$html->assign_by_ref('nomDire',$this->SearchDirector($codigoDependencia));
		$trabajador=$this->userIntranet['APELLIDO']." ".$this->userIntranet['NOMBRE'];
		$html->assign_by_ref('trabajador',$trabajador);
		$siglaDep="PP";
		$html->assign_by_ref('siglaDep',$siglaDep);
		$var="&page={$page}&tipDocumento={$tipDocumento}&tipBusqueda={$tipBusqueda}&nroTD={$nroTD}&asunto={$asunto}&observaciones={$observaciones}&fecIniDir={$fecIniDir}&fecFinDir={$fecFinDir}&materia2={$materia2}&correlativo2={$correlativo2}";
		$html->assign_by_ref('var',$var);
		
		// Setea datos del Formulario CSV
		//$html->assign('frmName','frmCSV');
		//$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		/*Prueba Justomatrix is back*/
		$html->assign_by_ref('codTrabajador',$this->userIntranet['CODIGO']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		//$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DOCDIR], true));
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, 'frmBuscar', $this->arr_accion[BUSCA_EXP], true));
		
		// Muestra el Resultado de la Busqueda
		$html->display('pp/procesosProduce/searchResult.tpl.php');
		$html->display('pp/footerArm.tpl.php');
		}//fin del if($fecInicio&&$fecFin)
	}
	
	function FormCompletaDocumento($id,$naturaleza=NULL,$monto=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,
	                               $materia=NULL,$natu=NULL,$tipProc=NULL,$tipoResp=NULL,$juzgado=NULL,$exp=NULL,
								   $sala=NULL,$exp2=NULL,$corte=NULL,$exp3=NULL,$medida=NULL,$opcion2=NULL,$nombre=NULL,
								   $Busca=NULL,$RZ=NULL,$reLoad=false,$errors=NULL){
		global $nroOTD,$RZ;
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $med;
		global $etapa;
		global $idProceso;
		global $idCondNew,$idCond,$correlativo;
		global $a,$page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $codDistJud,$codSede;
		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $resolAutoritativa,$embarcacion,$abogado,$especialista1,$especialista2,$especialista3,$resolImpugnada,
			   $juzgadoPaz,$exp4,$especialista4,$idSoft2,$bSoftware2,$idSoftNew2,$BuscaEmb,$emb,$idSoft3,$bSoftware3,$idSoftNew3,$plant,$planta,
			   $tribunal,$exp5,$especialista5,$resol,$bSoftware4,$idSoft4,$idSoftNew4,$sub,$tipExcepcion,$excepcion,$anotacion1,$anotacion2;
		global $embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3,$tipoMonto,$etapaProcesal;	   

		$sub=($_POST['sub']) ? $_POST['sub'] : $_GET['sub'];
		if($id&&!$reLoad){
			$this->abreConnDB();
			
			$sql_st = sprintf("SELECT id,id_documento,id_naturaleza,monto,id_tipo_proceso,id_tipo_responsabilidad".
			   						 ",codigo_departamento,codigo_provincia,codigo_distrito".
									 ",materia,delito,juzgado,exp1,sala,exp2".
									 ",corte,exp3,codigo_distritojudicial,codigo_sede,fecVenc,correlativo, ".
									 "juzgado_paz,expediente_juzgado_paz,especialista_juzgado_paz,id_resolucion_autoritativa, ".
									 "id_abogado,TRIBUNAL,EXPEDIENTE_TRIBUNAL,ESPECIALISTA_TRIBUNAL,ESPECIALISTA_JUZGADO_FISCALIA,ESPECIALISTA_SALA_SUPERIOR, ".
									 "ESPECIALISTA_CORTE_SUPREMA,embclonada1,matricolnada1,embclonada2,matriclonada2,embclonada3,matriclonada3, ".
									 "id_tipo_monto,id_etapa_procesal ".
							  "FROM db_procuraduria.dbo.proceso ".
							  "WHERE id=%d",$this->PrepareParamSQL($id));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($idProceso,$idDocumento,$naturaleza,$monto,$tipProc,
						 $tipoResp,$codDepa,$codProv,$codDist,$materia,$delito,
						 $juzgado,$exp,$sala,$exp2,$corte,$exp3,$codDistJud,$codSede,$desFechaIni,$correlativoBD,
						 $juzgadoPaz,$exp4,$especialista4,$resolAutoritativa,$abogado,$tribunal,$exp5,$especialista5,
						 $especialista1,$especialista2,$especialista3,$embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3,
						 $tipoMonto,$etapaProcesal) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
		}
				
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		//$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		//$frmName = 'frmAddNew';
		$frmName = 'frmUpdateNew';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		if($idProceso>0){
		    //echo "holas justomatrix".$idProceso;
			$this->abreConnDB();
			//$this->conn->Debug=true;
			
			/*$sql="select id,descripcion,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108) 
			      from db_procuraduria.dbo.medida
						where id_proceso=$idProceso
				  ";
			//echo $sql;
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('soft', array( 'id'=>$row[0],
												   'medida'=>$row[1],
												   'fecha'=>$row[2]
												   ));
												   
					unset($row);
					$rs->Close();
			}	*/		
			
			$sql="select id,descripcion,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108),anotacion 
			      from db_procuraduria.dbo.medida
						where id_proceso=$idProceso
				order by 1		
				  ";
			//echo $sql;
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('med', array( 'id'=>$row[0],
												   'medida'=>$row[1],
												   'fecha'=>$row[2],
												   'anotacion'=>$row[3]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
			$sql="select id,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108),descripcion,anotacion
				from db_procuraduria.dbo.etapas
				where id_proceso=$idProceso
				order by 1
				";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('eta', array( 'id'=>$row[0],
												   'etapa'=>$row[2],
												   'fecha'=>$row[1],
												   'anotacion'=>$row[3]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
			$sql="select ep.id,convert(varchar,ep.auditmod,103)+' '+convert(varchar,ep.auditmod,108),ep.EXCEPCION,te.descripcion
				from db_procuraduria.dbo.EXCEPCIONXPROCESO ep,db_procuraduria.dbo.tipo_excepcion te
				where ep.id_proceso=$idProceso and ep.id_tipo_excepcion=te.id 
				order by 1
				";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('exc', array( 'id'=>$row[0],
												   'excepcion'=>$row[2],
												   'fecha'=>$row[1],
												   'tipExcepcion'=>$row[3]
												   ));
												   
					unset($row);
					$rs->Close();
			}			
			
			/**/
			if(!$reLoad){
				$sql="select id,id_persona,condicion 
					  from db_procuraduria.dbo.RZ_procuraduria
							where id_proceso=$idProceso
							and id_persona is not null
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$idSoft[]=$row[1];
							$idCond[]=$row[2];
						}							   
						unset($row);
						$rs->Close();
				}
				
				$sql="select id,id_emb
					  from db_procuraduria.dbo.embxproceso
							where id_proceso=$idProceso
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$idSoft2[]=$row[1];
						}							   
						unset($row);
						$rs->Close();
				}
				
				$sql="select id,id_persona
					  from db_procuraduria.dbo.plantaxproceso
							where id_proceso=$idProceso
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$idSoft3[]=$row[1];
						}							   
						unset($row);
						$rs->Close();
				}
				
				$sql="select id,id_resolucion
					  from db_procuraduria.dbo.resolimpugnadaxproceso
							where id_proceso=$idProceso
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$idSoft4[]=$row[1];
						}							   
						unset($row);
						$rs->Close();
				}				
			}/**/
			
			
		}
		
		if($id>0){
			$idProceso=$id;
			$sql_st = "select correlativo from db_procuraduria.dbo.proceso where id=".$idProceso;
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				$correlativoBD=$rs->fields[0];
			}			
		}		

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idProceso',$idProceso);
		$html->assign_by_ref('nroOTD',$nroOTD);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('naturaleza',$naturaleza);
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('natu',$natu);
		$html->assign_by_ref('juzgado',$juzgado);
		$html->assign_by_ref('exp',$exp);
		$html->assign_by_ref('sala',$sala);
		$html->assign_by_ref('exp2',$exp2);
		$html->assign_by_ref('corte',$corte);
		$html->assign_by_ref('exp3',$exp3);
		$html->assign_by_ref('medida',$medida);
		$html->assign_by_ref('excepcion',$excepcion);
		$html->assign_by_ref('opcion2',$opcion2);
		$html->assign_by_ref('nombre',$nombre);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('list',$list);
		$html->assign_by_ref('tel',$tel);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('reLoad',$reLoad);
		$html->assign_by_ref('idProceso',$idProceso);
		$html->assign_by_ref('etapa',$etapa);
		
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('especialista1',$especialista1);
		$html->assign_by_ref('especialista2',$especialista2);
		$html->assign_by_ref('especialista3',$especialista3);
		$html->assign_by_ref('resol',$resol);
		$html->assign_by_ref('juzgadoPaz',$juzgadoPaz);
		$html->assign_by_ref('exp4',$exp4);
		$html->assign_by_ref('especialista4',$especialista4);
		$html->assign_by_ref('tribunal',$tribunal);
		$html->assign_by_ref('exp5',$exp5);
		$html->assign_by_ref('especialista5',$especialista5);
		$html->assign_by_ref('emb',$emb);
		$html->assign_by_ref('plant',$plant);
		$html->assign_by_ref('anotacion1',$anotacion1);
		$html->assign_by_ref('anotacion2',$anotacion2);
		
		$html->assign_by_ref('idCond',$idCond);
		
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('opcion3',$opcion3);
		$html->assign_by_ref('correlativoBD',$correlativoBD);
		$html->assign_by_ref('sub',$sub);
		
		$html->assign_by_ref('embClonada1',$embClonada1);
		$html->assign_by_ref('embClonada2',$embClonada2);
		$html->assign_by_ref('embClonada3',$embClonada3);
		$html->assign_by_ref('matriEmbClonada1',$matriEmbClonada1);
		$html->assign_by_ref('matriEmbClonada2',$matriEmbClonada2);
		$html->assign_by_ref('matriEmbClonada3',$matriEmbClonada3);		
		
		/*Para q retorne a la b�squeda*/
		/*$page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2*/
		$html->assign('datos',array('tipDocumento'=>$tipDocumento,
									'tipBusqueda'=>$tipBusqueda,
									'nroTD'=>$nroTD,
									'asunto'=>$asunto,
									'observaciones'=>$observaciones,
									'procedimiento'=>$procedimiento,
									'fecIniDir'=>$fecIniDir,
									'fecFinDir'=>$fecFinDir,
									'indicativo'=>$indicativo,
									'siglasDep'=>$siglasDep,
									'tipodDoc'=>$tipodDoc,
									'page'=>$page,
									'materia2'=>$materia2,
									'correlativo2'=>$correlativo2
									));
		/**/
				
		$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "ORDER BY 2";
		$html->assign_by_ref('selMateria',$this->ObjFrmSelect($sql_st, $materia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		if($materia>0){
			$sql="select ISNULL(MAX(contador),0)+1 from db_procuraduria.dbo.contador where id_materia=$materia
				  ";
			//echo $sql;
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
				$correlativo=$rs->fields[0];
				if($correlativo<10)
				   $correlativo="00".$correlativo;
				elseif($correlativo<100)
					$correlativo="0".$correlativo;
			}
		}
		
		if($materia==1){
			$correlativo="P-".$correlativo."-".date('Y');
		}elseif($materia==2){
			$correlativo="C-".$correlativo."-".date('Y');
		}elseif($materia==3){
			$correlativo="Const-".$correlativo."-".date('Y');
		}elseif($materia==4){
			$correlativo="Lab-".$correlativo."-".date('Y');
		}elseif($materia==5){
			$correlativo="Fon-".$correlativo."-".date('Y');
		}elseif($materia==6){
			$correlativo="Ind-".$correlativo."-".date('Y');
		}elseif($materia==7){
			$correlativo="Mipes-".$correlativo."-".date('Y');
		}
		$html->assign_by_ref('correlativo',$correlativo);
		$html->assign_by_ref('materia',$materia);
			
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.naturaleza ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		$html->assign_by_ref('selNaturaleza',$this->ObjFrmSelect($sql_st, $naturaleza, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
			
		if($materia==2)
		{	
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_monto where ID IN (3)  ".
					  "ORDER BY 2");
		}else{
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_monto where ID IN (1,2) ".
					  "ORDER BY 2");		
		}
		$html->assign_by_ref('selTipoMonto',$this->ObjFrmSelect($sql_st, $tipoMonto, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.ETAPA_PROCESAL ".
					  "ORDER BY 1");
		$html->assign_by_ref('selEtapaProcesal',$this->ObjFrmSelect($sql_st, $etapaProcesal, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
			
		if($materia==7){//Minpes
			//20: Civil
			//21: Penal
			//22: Laboral
			//23: Constitucional
			if($naturaleza==20)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=2 
						  ORDER BY 2";
			elseif($naturaleza==21)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=1
						  ORDER BY 2";
			elseif($naturaleza==22)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia in (4,7)
						  ORDER BY 2";
			elseif($naturaleza==23)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=3
						  ORDER BY 2";
			else
				$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_proceso ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);			  
		}else{//			
			$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_proceso ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		}			  
		$html->assign_by_ref('selTipoProceso',$this->ObjFrmSelect($sql_st, $tipProc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador+' '+nombres_trabajador) ".
					  "FROM db_general.dbo.h_trabajador ".
					  "where coddep=15 and estado='ACTIVO' ".
					  "and condicion<>'GENERAL' ".
					  "ORDER BY 2";
		$html->assign_by_ref('selAbogado',$this->ObjFrmSelect($sql_st, $abogado, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		if($materia==1){
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (2,4,5) ".
						  "ORDER BY 2";
		}else{
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (1,3) ".
						  "ORDER BY 2";
		}
		$html->assign_by_ref('selTipoResp',$this->ObjFrmSelect($sql_st, $tipoResp, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		if($tipoResp>0){
			$html->assign_by_ref('tipoResp',$tipoResp);
		}
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT id,'RM '+nro_resol ".
				  "FROM resolucion ".
				  "where coddep=5 and id_tipo_resolucion=2 and year(auditmod)>=2005 ".
				  "ORDER BY 1 DESC";
		$html->assign('selResolAutoritativa',$this->ObjFrmSelect($sql_st, $resolAutoritativa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		if((!$Busca&&$nombre!="")){
		// Contenido Select del Asunto para DINSECOVI
		$sql_st = "SELECT id, case when id_tipo_persona=1 then substring(apellidos,1,40)+' '+substring(nombres,1,40) ".
				  "when id_tipo_persona=2 then lower(substring(razon_social,1,60)) end ".
				  "FROM db_general.dbo.persona ".
				  "WHERE (Upper(razon_social) like Upper('%$nombre%')) or (Upper(nombres) like Upper('%$nombre%')) or (Upper(apellidos) like Upper('%$nombre%'))".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RZ, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.DBO.TIPO_EXCEPCION ".
					  "ORDER BY 2";
		$html->assign_by_ref('selTipoExcepcion',$this->ObjFrmSelect($sql_st, $tipExcepcion, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		/**/
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_distritojudicial, descripcion ".
				  "FROM db_procuraduria.dbo.distritojudicial ".
				  "ORDER BY 2";
		$html->assign('selDistJud',$this->ObjFrmSelect($sql_st, $codDistJud, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		/**/
		$sql_st = sprintf("SELECT codigo_sede, Lower(descripcion) ".
				  		  "FROM db_procuraduria.dbo.sede ".
				  		  "WHERE codigo_distritojudicial='%s' ".
				  		  "ORDER BY 2",(!is_null($codDistJud)||!empty($codDistJud)) ? $codDistJud : 'xx');
		$html->assign('selSede',$this->ObjFrmSelect($sql_st, $codSede, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		
		//Juzgado de Paz: Busca si hay correlativos ingresados anteriormente
		if($exp4!="" && $exp4!="NULL" && $exp4!=" "){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXPEDIENTE_JUZGADO_PAZ='".$exp4."' ";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpJuzgadoPazRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosExpJuzgadoPazRepetidos==""){
									$correlativosExpJuzgadoPazRepetidos="Este exp. del Juzgado de Paz ya se registr� en el(los): ".$row_[0].",";
								}else{
									$correlativosExpJuzgadoPazRepetidos.=" ".$row_[0].",";
								}
							}
						}
						$html->assign_by_ref('correlativosExpJuzgadoPazRepetidos',$correlativosExpJuzgadoPazRepetidos);
					}		
		}//fin del if($exp4
		
		//Juzgado/Fiscal�a: Busca si hay correlativos ingresados anteriormente
		if($exp!="" && $exp!="NULL" && $exp!=" "){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP1='".$exp."' ";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpJuzgadoFiscaliaRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosExpJuzgadoFiscaliaRepetidos==""){
									$correlativosExpJuzgadoFiscaliaRepetidos="Este exp. del Juzgado/Fiscal�a ya se registr� en el(los): ".$row_[0].",";
								}else{
									$correlativosExpJuzgadoFiscaliaRepetidos.=" ".$row_[0].",";
								}
							}
						}
						$html->assign_by_ref('correlativosExpJuzgadoFiscaliaRepetidos',$correlativosExpJuzgadoFiscaliaRepetidos);
					}		
		}//fin del if($exp4
		
		//Sala Superior: Busca si hay correlativos ingresados anteriormente
		if($exp2!="" && $exp2!="NULL" && $exp2!=" "){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP2='".$exp2."' ";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpSalaRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosExpSalaRepetidos==""){
									$correlativosExpSalaRepetidos="Este exp. del Sala Superior ya se registr� en el(los): ".$row_[0].",";
								}else{
									$correlativosExpSalaRepetidos.=" ".$row_[0].",";
								}
							}
						}
						$html->assign_by_ref('correlativosExpSalaRepetidos',$correlativosExpSalaRepetidos);
					}		
		}//fin del if($exp4
		
		//Corte Suprema: Busca si hay correlativos ingresados anteriormente
		if($exp3!="" && $exp3!="NULL" && $exp3!=" "){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP3='".$exp3."' ";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpCorteRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosExpCorteRepetidos==""){
									$correlativosExpCorteRepetidos="Este exp. del Corte Suprema ya se registr� en el(los): ".$row_[0].",";
								}else{
									$correlativosExpCorteRepetidos.=" ".$row_[0].",";
								}
							}
						}
						$html->assign_by_ref('correlativosExpCorteRepetidos',$correlativosExpCorteRepetidos);
					}		
		}//fin del if($exp4
		
		//tRIBUNAL: Busca si hay correlativos ingresados anteriormente
		if($exp5!="" && $exp5!="NULL" && $exp5!=" "){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXPEDIENTE_TRIBUNAL='".$exp5."' ";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpTribunalRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosExpTribunalRepetidos==""){
									$correlativosExpTribunalRepetidos="Este exp. del Tribunal ya se registr� en el(los): ".$row_[0].",";
								}else{
									$correlativosExpTribunalRepetidos.=" ".$row_[0].",";
								}
							}
						}
						$html->assign_by_ref('correlativosExpTribunalRepetidos',$correlativosExpTribunalRepetidos);
					}		
		}//fin del if($exp4
		
		// Lista el Software ya Agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		
		//echo "x".count($idSoft)."x";
		//echo "<br>xxxx";
		if($idSoft&&count($idSoft)>0){
			//echo "matrix";
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,case when id_tipo_persona=1 then apellidos+' '+nombres
										when id_tipo_persona=2 then lower(razon_social) end ,nro_documento
				  						FROM db_general.dbo.persona 
										where id=%d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'nroDoc' =>$row[2]
														));
						$rs->Close();
						//echo "justomatrix is here";
					}
					unset($rs);
					/**/
				}
			}
		}
		
		if((!$BuscaEmb&&$emb!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "select emb.id_emb,emb.nombre_emb+' '+emb.matricula_emb+' '+convert(varchar,emb.codpag_emb)
										from db_dnepp.user_dnepp.embarcacionnac emb
										where 
										(emb.nombre_emb like '%{$emb}%' or emb.matricula_emb like '%{$emb}%' or emb.codpag_emb like '%{$emb}%')
										and emb.codpag_emb is not null
										order by 2";
		$html->assign_by_ref('selEmbarcacion',$this->ObjFrmSelect($sql_st, $embarcacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft2 = (is_array($idSoft2)) ? $idSoft2 : array();
		
		if($idSoftNew2) array_push($idSoft2, $idSoftNew2);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft2&&count($idSoft2)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft2);$i++){
				if(!empty($idSoft2[$i])&&!is_null($idSoft2[$i])){
				
					$sql_="SELECT p.CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO p,db_procuraduria.dbo.embxproceso ep
							WHERE p.ID=ep.id_proceso
							and ep.id_emb=".$idSoft2[$i];
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosEmbRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativoBD!=$row_[0]){
								if($correlativosEmbRepetidos=="")
									$correlativosEmbRepetidos="Esta embarcaci�n ya se registr� en el(los): ".$row_[0].",";
								else
									$correlativosEmbRepetidos.=" ".$row_[0].",";
							}	
						}
					}				
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
				  						from db_dnepp.user_dnepp.embarcacionnac emb
										where emb.id_emb=%d",
									  $this->PrepareParamSQL($idSoft2[$i]));
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft2', array('id' => $idSoft2[$i],
														//'idD' => ucwords($row[0]),
														'emb' =>ucwords($row[1]),
														'matri' =>$row[2],
														'codPago' =>$row[3],
														'correlativosEmbRepetidos' =>$correlativosEmbRepetidos
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}		
		/**/

		if((!$BuscaPlanta&&$plant!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT ID,CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
	   		   		ELSE RAZON_SOCIAL END
FROM DB_GENERAL.DBO.PERSONA
WHERE ID IN (
SELECT ID FROM db_dnepp.user_dnepp.VPERSONA VP INNER JOIN db_dnepp.user_dnepp.PLANTPROCXPERSONA PPXP                                                                                                                                                                                                                
			ON VP.ID = PPXP.ID_PERS                                                                                                                                                                                                                                     
			WHERE PPXP.ESTADO_PLAPROXPERS = 1
)
and (APELLIDOS like '%{$plant}%' or nombres like '%{$plant}%' or razon_social like '%{$plant}%' )		
ORDER BY 2";
		$html->assign_by_ref('selPlanta',$this->ObjFrmSelect($sql_st, $planta, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft3 = (is_array($idSoft3)) ? $idSoft3 : array();
		
		if($idSoftNew3) array_push($idSoft3, $idSoftNew3);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft3&&count($idSoft3)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft3);$i++){
				if(!empty($idSoft3[$i])&&!is_null($idSoft3[$i])){
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
	   		   		ELSE RAZON_SOCIAL END
				  						FROM DB_GENERAL.DBO.PERSONA
										where id=%d",
									  $this->PrepareParamSQL($idSoft3[$i]));
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft3', array('id' => $idSoft3[$i],
														//'idD' => ucwords($row[0]),
														'planta' =>ucwords($row[1])
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}

		if((!$BuscaResolucion&&$resol!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT ID,case when id_tipo_resolucion=4 then 'RD'
								  when id_tipo_resolucion=7 then 'RCAS' END+' '+nro_resol
					from resolucion
WHERE nro_resol like '%{$resol}%'
AND CODDEP IN (25,47,48,24,16,36) 	
AND year(auditmod)>=2005
ORDER BY 2";
		$html->assign_by_ref('selResolImpugnada',$this->ObjFrmSelect($sql_st, $resolImpugnada, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft4 = (is_array($idSoft4)) ? $idSoft4 : array();
		
		if($idSoftNew4) array_push($idSoft4, $idSoftNew4);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft4&&count($idSoft4)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft4);$i++){
				if(!empty($idSoft4[$i])&&!is_null($idSoft4[$i])){
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,nro_resol
				  						FROM resolucion
										where id=%d",
									  $this->PrepareParamSQL($idSoft4[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft4', array('id' => $idSoft4[$i],
														//'idD' => ucwords($row[0]),
														'resol' =>ucwords($row[1])
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}		
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArmn.tpl.php');
		$html->display('pp/procesosProduce/frmCompletaDoc.tpl.php');
		$html->display('pp/footerArm.tpl.php');
	
	}

	function CompletaDocumento($id,$naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ){
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $idCond,$correlativo;
		global $idProceso;
		global $etapa;
		
		global $page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $fecIniDir2,$fecFinDir2;
		global $codDistJud,$codSede;

		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $resolAutoritativa,$embarcacion,$abogado,$especialista1,$especialista2,$especialista3,$resolImpugnada,
			   $juzgadoPaz,$exp4,$especialista4,$idSoft2,$emb,$idSoft3,$plant,$planta,
			   $tribunal,$exp5,$especialista5,$resol,$idSoft4,$tipExcepcion,$excepcion,$anotacion1,$anotacion2;
		global $embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3,$tipoMonto,$etapaProcesal;
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		// Comprueba Valores	
		//if(!$juzgado){ $bTit = true; $this->errors .= 'El nombre del Juzgado debe ser especificado<br>'; }

		if($bTit){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('tramite_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormCompletaDocumento($id,$naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ,true,$errors);
			
			$objIntranet->Footer(false);
		}else{
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			//$this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXEC  %s %d,%d,%d,'%s',%s,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,%s,%s,%s,%s,%s,%s,%d,%d",
							  ($idProceso>0) ? "db_procuraduria.dbo.modProceso" : "db_procuraduria.dbo.insProceso",
							  $id,
							  $this->PrepareParamSQL($materia),
							  $this->PrepareParamSQL($naturaleza),
							  ($this->PrepareParamSQL($natu)) ? $this->PrepareParamSQL($natu) : "NULL",
							  ($this->PrepareParamSQL($monto)) ? $this->PrepareParamSQL($monto) : "NULL",
							  $this->PrepareParamSQL($tipProc),
							  $this->PrepareParamSQL($tipoResp),
							  $this->PrepareParamSQL($codDepa),
							  $this->PrepareParamSQL($codProv),
							  $this->PrepareParamSQL($codDist),
							  $this->PrepareParamSQL($juzgado),
							  $this->PrepareParamSQL($exp),
							  ($this->PrepareParamSQL($sala)) ? $this->PrepareParamSQL($sala) : " ",
							  ($this->PrepareParamSQL($exp2)) ? $this->PrepareParamSQL($exp2) : " ",
							  ($this->PrepareParamSQL($corte)) ? $this->PrepareParamSQL($corte) : " ",
							  ($this->PrepareParamSQL($exp3)) ? $this->PrepareParamSQL($exp3) : " ",
							  $_SESSION['cod_usuario'],
							  $correlativo,
							  $this->PrepareParamSQL($codDistJud),
							  $this->PrepareParamSQL($codSede),
							  ($this->PrepareParamSQL($desFechaIni)) ? $this->PrepareParamSQL($desFechaIni) : "NULL",
							  $opcion3,
							  ($this->PrepareParamSQL($juzgadoPaz)) ? $this->PrepareParamSQL($juzgadoPaz) : "NULL",
							  ($this->PrepareParamSQL($exp4)) ? $this->PrepareParamSQL($exp4) : "NULL",
							  ($this->PrepareParamSQL($especialista4)) ? $this->PrepareParamSQL($especialista4) : "NULL",
							  ($this->PrepareParamSQL($tribunal)) ? $this->PrepareParamSQL($tribunal) : "NULL",
							  ($this->PrepareParamSQL($exp5)) ? $this->PrepareParamSQL($exp5) : "NULL",
							  ($this->PrepareParamSQL($especialista5)) ? $this->PrepareParamSQL($especialista5) : "NULL",
							  ($this->PrepareParamSQL($especialista1)) ? $this->PrepareParamSQL($especialista1) : "NULL",
							  ($this->PrepareParamSQL($especialista2)) ? $this->PrepareParamSQL($especialista2) : "NULL",
							  ($this->PrepareParamSQL($especialista3)) ? $this->PrepareParamSQL($especialista3) : "NULL",
							  ($resolAutoritativa) ? $resolAutoritativa : "NULL",
							  ($abogado) ? $abogado : "NULL",
							  ($embClonada1) ? "'".$this->PrepareParamSQL($embClonada1)."'" : "NULL",
							  ($matriEmbClonada1) ? "'".$this->PrepareParamSQL($matriEmbClonada1)."'" : "NULL",
							  ($embClonada2) ? "'".$this->PrepareParamSQL($embClonada2)."'" : "NULL",
							  ($matriEmbClonada2) ? "'".$this->PrepareParamSQL($matriEmbClonada2)."'" : "NULL",
							  ($embClonada3) ? "'".$this->PrepareParamSQL($embClonada3)."'" : "NULL",
							  ($matriEmbClonada3) ? "'".$this->PrepareParamSQL($matriEmbClonada3)."'" : "NULL",
							  $tipoMonto,
							  $etapaProcesal
							  );
			// echo $sql_FT;
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									if($row = $rs->FetchRow()){
										$RETVAL = $row[0];
										$idProc = $row[1];
									}else
										$RETVAL = 1;
									$rs->Close();
								}
								unset($rs);
								
						
				if(!$RETVAL/*&& !$idProceso*/){
				
					//Primero borramos los registros anteriores
					$sql_SP = "delete from db_procuraduria.dbo.RZ_procuraduria where ID_PROCESO=".$idProc;
					$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs){
							$RETVAL=1;
							//break;
						}
					
					// Inserta los Software's correspondientes al CPU
					for($i=0;$i<count($idSoft);$i++){
						//$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insProcesoRZ %d,%d,'%s',%d",
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insProcesoRZ %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft[$i]),
										  $_SESSION['cod_usuario'],
										  $this->PrepareParamSQL($idCond[$i])										  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}

				if(!$RETVAL&& $medida!=""){
					// Inserta las Medidas Cautelares correspondientes al proceso
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insMedida %d,'%s','%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($medida),
										  $this->PrepareParamSQL($anotacion2),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}

				if(!$RETVAL&& $etapa!=""){
					// Inserta los Software's correspondientes al CPU
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insEtapa %d,'%s','%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($etapa),
										  $this->PrepareParamSQL($anotacion1),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}
				
				if(!$RETVAL&& $excepcion!="" && $tipExcepcion>0){
					// Inserta los Software's correspondientes al CPU
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insExcepcion %d,%d,'%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($tipExcepcion),
										  $this->PrepareParamSQL($excepcion),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}				
				
				/**/if(!$RETVAL){
					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft2);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insEmbxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft2[$i]),
										  $_SESSION['cod_usuario'],
										  ($i==0) ?"2" :"1"
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}/**/				

				/**/if(!$RETVAL){
					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft3);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insPlantaxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft3[$i]),
										  $_SESSION['cod_usuario'],
										  ($i==0) ?"2" :"1"							  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}/**/
				
				/**/if(!$RETVAL){
					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft4);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insResolImpugnadaxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft4[$i]),
										  $_SESSION['cod_usuario'],
										  ($i==0) ?"2" :"1"
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}		/**/				

						
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();
				
			//exit;	
			/*
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
			*/
			/**/
				if (!$RETVAL) {
					$a=1;
					$html = new Smarty;
					$html->assign_by_ref('a',$a);
					//echo "holas";
					$destination = $_SERVER['PHP_SELF'] . '?accion=' . $this->arr_accion['BUSCA_EXP'] . '&page=' . $page . '&tipDocumento=' . $tipDocumento . '&tipBusqueda=' . $tipBusqueda . '&a=' . $a . '&fecIniDir2=' . $fecIniDir . '&fecFinDir2=' . $fecFinDir. '&materia2=' . $materia2 . '&correlativo2=' . $correlativo2;
					//echo $destination;
					header('Location: ' . $destination);
				}else{ 
						$destination = $_SERVER['PHP_SELF'] . '?accion=';
						$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
						$destination .= "&menu={$this->menu_items[2]['val']}";
						//echo $destination;exit;
						header("Location: $destination");
						exit;
				}
			/**/
		}
	}
	
	function FormAgregaProceso($naturaleza=NULL,$monto=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,
	                               $materia=NULL,$natu=NULL,$tipProc=NULL,$tipoResp=NULL,$juzgado=NULL,$exp=NULL,
								   $sala=NULL,$exp2=NULL,$corte=NULL,$exp3=NULL,$medida=NULL,$opcion2=NULL,$nombre=NULL,
								   $Busca=NULL,$RZ=NULL,$reLoad=false,$errors=NULL){
		global $nroOTD,$RZ;
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $med;
		global $etapa;
		global $idProceso;
		global $idCondNew,$idCond,$correlativo;
		global $a,$page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $codDistJud,$codSede;
		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $adjunto,$resolAutoritativa,$embarcacion,$abogado,$especialista1,$especialista2,$especialista3,$resolImpugnada,
			   $juzgadoPaz,$exp4,$especialista4,$idSoft2,$bSoftware2,$idSoftNew2,$BuscaEmb,$emb,$idSoft3,$bSoftware3,$idSoftNew3,$plant,$planta,
			   $tribunal,$exp5,$especialista5,$resol,$bSoftware4,$idSoft4,$idSoftNew4,$anotacion1,$anotacion2;
		global $otroCorrelativo,$anyoOtro;
		global $opcionAgregaResolImpug,$tipNewTipResolImpugnada,$newResolImpugnada,$newanyoResolImpugnada,$newDepeResolImpugnada,$desFechaFin,$sumillaNewResolImpugnada;
		global $embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3,$tipoMonto,$etapaProcesal;
		// Manipulaci�n de Datos
		$desImg = $indImg!='N' ? $desImg : NULL;
		$desImgExp = $indImgExp!='N' ? $desImgExp : NULL;
		$numAnImgExp = $indImgExp!='N' ? $numAnImgExp : NULL;
		$numAlImgExp = $indImgExp!='N' ? $numAlImgExp : NULL;

		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager(3));
		//$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddProc';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nroOTD',$nroOTD);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('naturaleza',$naturaleza);
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('natu',$natu);
		$html->assign_by_ref('juzgado',$juzgado);
		$html->assign_by_ref('exp',$exp);
		$html->assign_by_ref('sala',$sala);
		$html->assign_by_ref('exp2',$exp2);
		$html->assign_by_ref('corte',$corte);
		$html->assign_by_ref('exp3',$exp3);
		$html->assign_by_ref('medida',$medida);
		$html->assign_by_ref('opcion2',$opcion2);
		$html->assign_by_ref('nombre',$nombre);
		$html->assign_by_ref('Busca',$Busca);
		$html->assign_by_ref('RZ',$RZ);
		$html->assign_by_ref('list',$list);
		$html->assign_by_ref('tel',$tel);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('reLoad',$reLoad);
		$html->assign_by_ref('idProceso',$idProceso);
		$html->assign_by_ref('etapa',$etapa);
		//$html->assign_by_ref('resolAutoritativa',$resolAutoritativa);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('especialista1',$especialista1);
		$html->assign_by_ref('especialista2',$especialista2);
		$html->assign_by_ref('especialista3',$especialista3);
		$html->assign_by_ref('resol',$resol);
		$html->assign_by_ref('juzgadoPaz',$juzgadoPaz);
		$html->assign_by_ref('exp4',$exp4);
		$html->assign_by_ref('especialista4',$especialista4);
		$html->assign_by_ref('tribunal',$tribunal);
		$html->assign_by_ref('exp5',$exp5);
		$html->assign_by_ref('especialista5',$especialista5);
		$html->assign_by_ref('emb',$emb);
		$html->assign_by_ref('plant',$plant);
		$html->assign_by_ref('anotacion1',$anotacion1);
		$html->assign_by_ref('anotacion2',$anotacion2);
		
		$html->assign_by_ref('idCond',$idCond);
		
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('opcion3',$opcion3);
		$html->assign_by_ref('correlativoBD',$correlativoBD);
		$html->assign_by_ref('adjunto',$adjunto);
		
		$html->assign_by_ref('otroCorrelativo',$otroCorrelativo);
		$html->assign_by_ref('anyoOtro',$anyoOtro);
		$html->assign_by_ref('opcionAgregaResolImpug',$opcionAgregaResolImpug);
		$html->assign_by_ref('tipNewTipResolImpugnada',$tipNewTipResolImpugnada);
		$html->assign_by_ref('newResolImpugnada',$newResolImpugnada);
		$html->assign_by_ref('newanyoResolImpugnada',$newanyoResolImpugnada);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('sumillaNewResolImpugnada',$sumillaNewResolImpugnada);
		
		//$embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3
		$html->assign_by_ref('embClonada1',$embClonada1);
		$html->assign_by_ref('embClonada2',$embClonada2);
		$html->assign_by_ref('embClonada3',$embClonada3);
		$html->assign_by_ref('matriEmbClonada1',$matriEmbClonada1);
		$html->assign_by_ref('matriEmbClonada2',$matriEmbClonada2);
		$html->assign_by_ref('matriEmbClonada3',$matriEmbClonada3);
		
		
		//$_SESSION['cod_usuario']=="mespinoza"||
		if(
		$_SESSION['cod_usuario']=="eayllon"||
		$_SESSION['cod_usuario']=="caguilar"||
		$_SESSION['cod_usuario']=="obarja"
		//$_SESSION['cod_usuario']=="aserpa"||
		//$_SESSION['cod_usuario']=="jtumay"||
		//||$_SESSION['cod_usuario']=="cherrera"
		)
			$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "ORDER BY 2";
		else
			$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "where id not in (7)".
					  "ORDER BY 2";
		$html->assign_by_ref('selMateria',$this->ObjFrmSelect($sql_st, $materia, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		if($materia>0){
			$sql="select ISNULL(MAX(contador),0)+1 from db_procuraduria.dbo.contador where id_materia=$materia
				  ";
			//echo $sql;
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
				$correlativo=$rs->fields[0];
				if($correlativo<10)
				   $correlativo="00".$correlativo;
				elseif($correlativo<100)
					$correlativo="0".$correlativo;
			}
		}
		
		if($materia==1){
			$correlativo="P-".$correlativo."-".date('Y');
		}elseif($materia==2){
			$correlativo="C-".$correlativo."-".date('Y');
		}elseif($materia==3){
			$correlativo="Const-".$correlativo."-".date('Y');
		}elseif($materia==4){
			$correlativo="Lab-".$correlativo."-".date('Y');
		}elseif($materia==5){
			$correlativo="Fon-".$correlativo."-".date('Y');
		}elseif($materia==6){
			$correlativo="Ind-".$correlativo."-".date('Y');
		}elseif($materia==7){
			$correlativo="Mipes-".$correlativo."-".date('Y');
		}elseif($materia==8){
			$correlativo="Concil-".$correlativo."-".date('Y');
		}elseif($materia==9){
			$correlativo="Arbi-".$correlativo."-".date('Y');			
		}
		
		$html->assign_by_ref('correlativo',$correlativo);
		$html->assign_by_ref('materia',$materia);
			
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_tramite_documentario.dbo.tipo_resolucion ".
					  "WHERE id in (3,4,7,8) ".
					  "ORDER BY 2");
		$html->assign_by_ref('selTipNewTipResolImpugnada',$this->ObjFrmSelect($sql_st, $tipNewTipResolImpugnada, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_dependencia, lower(siglas) ".
					  "FROM db_general.dbo.h_dependencia ".
					  "WHERE codigo_dependencia in (24,25,47,48,16,36) ".
					  "ORDER BY 2");
		$html->assign_by_ref('selNewDepeResolImpugnada',$this->ObjFrmSelect($sql_st, $newDepeResolImpugnada, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.naturaleza ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		$html->assign_by_ref('selNaturaleza',$this->ObjFrmSelect($sql_st, $naturaleza, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);


		if($materia==2)
		{	
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_monto where ID IN (3)  ".
					  "ORDER BY 2");
		}else{
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_monto where ID IN (1,2) ".
					  "ORDER BY 2");		
		}
			

		$html->assign_by_ref('selTipoMonto',$this->ObjFrmSelect($sql_st, $tipoMonto, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.ETAPA_PROCESAL ".
					  "ORDER BY 1");
		$html->assign_by_ref('selEtapaProcesal',$this->ObjFrmSelect($sql_st, $etapaProcesal, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
			
		if($materia==7){//Minpes
			//20: Civil
			//21: Penal
			//22: Laboral
			//23: Constitucional
			if($naturaleza==20)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=2 
						  ORDER BY 2";
			elseif($naturaleza==21)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=1
						  ORDER BY 2";
			elseif($naturaleza==22)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia in (4,7)
						  ORDER BY 2";
			elseif($naturaleza==23)			
				$sql_st = "SELECT ID, lower(descripcion) 
						  FROM db_procuraduria.dbo.tipo_proceso 
						  WHERE id_materia=3
						  ORDER BY 2";
			else
				$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_proceso ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);			  
		}else{//
			$sql_st = sprintf("SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.tipo_proceso ".
					  "WHERE id_materia=%d ".
					  "ORDER BY 2",(!is_null($materia)||!empty($materia)) ? $materia : 0);
		}
		//echo $sql_st;
		$html->assign_by_ref('selTipoProceso',$this->ObjFrmSelect($sql_st, $tipProc, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = "SELECT codigo_trabajador, lower(apellidos_trabajador+' '+nombres_trabajador) ".
					  "FROM db_general.dbo.h_trabajador ".
					  "where coddep=15 and estado='ACTIVO' ".
					  "and condicion<>'GENERAL' ".
					  "ORDER BY 2";
		$html->assign_by_ref('selAbogado',$this->ObjFrmSelect($sql_st, $abogado, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		if($materia==1){
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (2,4,5) ".
						  "ORDER BY 2";
		}else{
			$sql_st = "SELECT ID, lower(descripcion) ".
						  "FROM db_procuraduria.dbo.tipo_responsabilidad ".
						  "WHERE id in (1,3) ".
						  "ORDER BY 2";
		}
		$html->assign_by_ref('selTipoResp',$this->ObjFrmSelect($sql_st, $tipoResp, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		if($tipoResp>0){
			$html->assign_by_ref('tipoResp',$tipoResp);
		}
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT id,'RM '+nro_resol ".
				  "FROM resolucion ".
				  "where coddep=5 and id_tipo_resolucion=2 and year(auditmod)>=2005 ".
				  "ORDER BY 1 DESC";
		$html->assign('selResolAutoritativa',$this->ObjFrmSelect($sql_st, $resolAutoritativa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);		
		
		if((!$Busca&&$nombre!="")){
		// Contenido Select del Asunto para DINSECOVI
		$sql_st = "SELECT id, case when id_tipo_persona=1 then substring(apellidos,1,40)+' '+substring(nombres,1,40) ".
				  "when id_tipo_persona=2 then lower(substring(razon_social,1,50)) end ".
				  "FROM db_general.dbo.persona ".
				  "WHERE (Upper(razon_social) like Upper('%$nombre%')) or (Upper(nombres) like Upper('%$nombre%')) or (Upper(apellidos) like Upper('%$nombre%'))".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RZ, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);

		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		/**/
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_distritojudicial, descripcion ".
				  "FROM db_procuraduria.dbo.distritojudicial ".
				  "ORDER BY 2";
		$html->assign('selDistJud',$this->ObjFrmSelect($sql_st, $codDistJud, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		/**/
		$sql_st = sprintf("SELECT codigo_sede, Lower(descripcion) ".
				  		  "FROM db_procuraduria.dbo.sede ".
				  		  "WHERE codigo_distritojudicial='%s' ".
				  		  "ORDER BY 2",(!is_null($codDistJud)||!empty($codDistJud)) ? $codDistJud : 'xx');
		$html->assign('selSede',$this->ObjFrmSelect($sql_st, $codSede, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		/**/
		//Juzgado de Paz: Busca si hay correlativos ingresados anteriormente
		if($exp4!=""){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXPEDIENTE_JUZGADO_PAZ='".$exp4."'";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpJuzgadoPazRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosExpJuzgadoPazRepetidos=="")
								$correlativosExpJuzgadoPazRepetidos="Este exp. del Juzgado de Paz ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosExpJuzgadoPazRepetidos.=" ".$row_[0].",";
						}
						$html->assign_by_ref('correlativosExpJuzgadoPazRepetidos',$correlativosExpJuzgadoPazRepetidos);
					}		
		}//fin del if($exp4
		
		//Juzgado/Fiscal�a: Busca si hay correlativos ingresados anteriormente
		if($exp!=""){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP1='".$exp."'";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpJuzgadoFiscaliaRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosExpJuzgadoFiscaliaRepetidos=="")
								$correlativosExpJuzgadoFiscaliaRepetidos="Este exp. del Juzgado/Fiscal�a ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosExpJuzgadoFiscaliaRepetidos.=" ".$row_[0].",";
						}
						$html->assign_by_ref('correlativosExpJuzgadoFiscaliaRepetidos',$correlativosExpJuzgadoFiscaliaRepetidos);
					}		
		}//fin del if($exp4
		
		//Sala Superior: Busca si hay correlativos ingresados anteriormente
		if($exp2!=""){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP2='".$exp2."'";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpSalaRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosExpSalaRepetidos=="")
								$correlativosExpSalaRepetidos="Este exp. de la Sala Superior ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosExpSalaRepetidos.=" ".$row_[0].",";
						}
						$html->assign_by_ref('correlativosExpSalaRepetidos',$correlativosExpSalaRepetidos);
					}		
		}//fin del if($exp4
		
		//Corte Suprema: Busca si hay correlativos ingresados anteriormente
		if($exp3!=""){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXP3='".$exp3."'";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpCorteRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosExpCorteRepetidos=="")
								$correlativosExpCorteRepetidos="Este exp. de la Corte Suprema ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosExpCorteRepetidos.=" ".$row_[0].",";
						}
						$html->assign_by_ref('correlativosExpCorteRepetidos',$correlativosExpCorteRepetidos);
					}		
		}//fin del if($exp4
		
		//Corte Suprema: Busca si hay correlativos ingresados anteriormente
		if($exp5!=""){
					$sql_="SELECT CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO
							WHERE EXPEDIENTE_TRIBUNAL='".$exp5."'";
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosExpTribunalRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosExpTribunalRepetidos=="")
								$correlativosExpTribunalRepetidos="Este exp. del Tribunal ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosExpTribunalRepetidos.=" ".$row_[0].",";
						}
						$html->assign_by_ref('correlativosExpTribunalRepetidos',$correlativosExpTribunalRepetidos);
					}		
		}//fin del if($exp4		
		
		// Lista el Software ya Agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,case when id_tipo_persona=1 then apellidos+' '+nombres
										when id_tipo_persona=2 then lower(razon_social) end ,nro_documento
				  						FROM db_general.dbo.persona 
										where id=%d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'nroDoc' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}
		
		
		if((!$BuscaEmb&&$emb!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "select emb.id_emb,emb.nombre_emb+' '+emb.matricula_emb+' '+convert(varchar,emb.codpag_emb)
										from db_dnepp.user_dnepp.embarcacionnac emb
										where 
										(emb.nombre_emb like '%{$emb}%' or emb.matricula_emb like '%{$emb}%' or emb.codpag_emb like '%{$emb}%')
										and emb.codpag_emb is not null
										order by 2";
		$html->assign_by_ref('selEmbarcacion',$this->ObjFrmSelect($sql_st, $embarcacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft2 = (is_array($idSoft2)) ? $idSoft2 : array();
		
		if($idSoftNew2) array_push($idSoft2, $idSoftNew2);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft2&&count($idSoft2)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft2);$i++){
				if(!empty($idSoft2[$i])&&!is_null($idSoft2[$i])){
				
					$sql_="SELECT p.CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO p,db_procuraduria.dbo.embxproceso ep
							WHERE p.ID=ep.id_proceso
							and ep.id_emb=".$idSoft2[$i];
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosEmbRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosEmbRepetidos=="")
								$correlativosEmbRepetidos="Esta embarcaci�n ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosEmbRepetidos.=" ".$row_[0].",";
						}
					}
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
				  						from db_dnepp.user_dnepp.embarcacionnac emb
										where emb.id_emb=%d",
									  $this->PrepareParamSQL($idSoft2[$i]));
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft2', array('id' => $idSoft2[$i],
														//'idD' => ucwords($row[0]),
														'emb' =>ucwords($row[1]),
														'matri' =>$row[2],
														'codPago' =>$row[3],
														'correlativoEmbRepetidos' =>$correlativosEmbRepetidos
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}		
		/**/

		if((!$BuscaPlanta&&$plant!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT ID,CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
	   		   		ELSE RAZON_SOCIAL END
FROM DB_GENERAL.DBO.PERSONA
WHERE ID IN (
SELECT ID FROM db_dnepp.user_dnepp.VPERSONA VP INNER JOIN db_dnepp.user_dnepp.PLANTPROCXPERSONA PPXP                                                                                                                                                                                                                
			ON VP.ID = PPXP.ID_PERS                                                                                                                                                                                                                                     
			WHERE PPXP.ESTADO_PLAPROXPERS = 1
)
and (APELLIDOS like '%{$plant}%' or nombres like '%{$plant}%' or razon_social like '%{$plant}%' )		
ORDER BY 2";
		$html->assign_by_ref('selPlanta',$this->ObjFrmSelect($sql_st, $planta, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft3 = (is_array($idSoft3)) ? $idSoft3 : array();
		
		if($idSoftNew3) array_push($idSoft3, $idSoftNew3);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft3&&count($idSoft3)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft3);$i++){
				if(!empty($idSoft3[$i])&&!is_null($idSoft3[$i])){
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,CASE WHEN ID_TIPO_PERSONA=1 THEN APELLIDOS+' '+NOMBRES
	   		   		ELSE RAZON_SOCIAL END
				  						FROM DB_GENERAL.DBO.PERSONA
										where id=%d",
									  $this->PrepareParamSQL($idSoft3[$i]));
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft3', array('id' => $idSoft3[$i],
														//'idD' => ucwords($row[0]),
														'planta' =>ucwords($row[1])
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}

		if((!$BuscaResolucion&&$resol!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT ID,case when id_tipo_resolucion=4 then 'RD'
								  when id_tipo_resolucion=7 then 'RCAS'
								  when id_tipo_resolucion=3 then 'RVM' END+' '+nro_resol
					from resolucion
WHERE nro_resol like '%{$resol}%'
AND CODDEP IN (25,47,48,24,16,36) 	
AND year(auditmod)>=2005
ORDER BY 2";
		$html->assign_by_ref('selResolImpugnada',$this->ObjFrmSelect($sql_st, $resolImpugnada, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		}		
		
		// Lista la especie x literal ya agregado
		$idSoft4 = (is_array($idSoft4)) ? $idSoft4 : array();
		
		if($idSoftNew4) array_push($idSoft4, $idSoftNew4);
		/**/
		//echo "x".count($idSoftNew2);
		if($idSoft4&&count($idSoft4)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft4);$i++){
				if(!empty($idSoft4[$i])&&!is_null($idSoft4[$i])){
				
					$sql_="SELECT p.CORRELATIVO 
							FROM DB_PROCURADURIA.dbo.PROCESO p,db_procuraduria.dbo.RESOLIMPUGNADAXPROCESO rip
							WHERE p.ID=rip.id_proceso
							and rip.id_resolucion=".$idSoft4[$i];
					//echo $sql_;
					$rs_ = & $this->conn->Execute($sql_);
					unset($sql_);
					if (!$rs_){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$correlativosResolRepetidos="";
						while($row_ = $rs_->FetchRow()){
							if($correlativosResolRepetidos=="")
								$correlativosResolRepetidos="Esta resoluci�n ya se registr� en el(los): ".$row_[0].",";
							else
								$correlativosResolRepetidos.=" ".$row_[0].",";
						}
					}				
					
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id,nro_resol
				  						FROM resolucion
										where id=%d",
									  $this->PrepareParamSQL($idSoft4[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft4', array('id' => $idSoft4[$i],
														//'idD' => ucwords($row[0]),
														'resol' =>ucwords($row[1]),
														'correlativosResolRepetidos'=>$correlativosResolRepetidos
														));
						$rs->Close();
					}
					unset($rs);
					
				}
			}
		}
		
		$accionHeader=1;
		$html->assign_by_ref('accionHeader',$accionHeader);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArmn.tpl.php');
		$html->display('pp/procesosProduce/frmAddProceso.tpl.php');
		$html->display('pp/footerArm.tpl.php');
	
	}
	
	function AgregaProceso($naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ,&$adjunto){
		global $list,$tel;
		global $bSoftware,$idSoftNew,$idSoft;
		global $idCond,$correlativo;
		global $idProceso;
		global $etapa;
		
		global $page,$tipDocumento,$tipBusqueda,$nroTD,$asunto,$observaciones,$fecIniDir,$fecFinDir,$materia2,$correlativo2;
		global $fecIniDir2,$fecFinDir2;
		global $codDistJud,$codSede;

		global $desFechaIni;//Fecha en la que se vence el expediente
		global $opcion3;//Para confirmar el n�mero de correlativo
		global $correlativoBD;//correlativo existente en la Base de Datos
		global $resolAutoritativa,$embarcacion,$abogado,$especialista1,$especialista2,$especialista3,$resolImpugnada,
			   $juzgadoPaz,$exp4,$especialista4,$idSoft2,$emb,$idSoft3,$plant,$planta,
			   $tribunal,$exp5,$especialista5,$resol,$idSoft4,$anotacion1,$anotacion2;
		global $otroCorrelativo,$anyoOtro;
		global $opcionAgregaResolImpug,$tipNewTipResolImpugnada,$newResolImpugnada,$newanyoResolImpugnada,$newDepeResolImpugnada,$desFechaFin,$sumillaNewResolImpugnada;
		global $embClonada1,$matriEmbClonada1,$embClonada2,$matriEmbClonada2,$embClonada3,$matriEmbClonada3,$tipoMonto,$etapaProcesal;
		//Manipulacion de las Fechas
		$fecNoticia = ($fecNoticia!='// :'&&$fecNoticia&&!strstr($fecNoticia,'none')) ? $fecNoticia : NULL;
		
		//echo "t".$_FILES['adjunto']['tmp_name']."t<br>".$RZ;
		//echo "x".$_FILES['adjunto']['name']."x";exit;
		
		// Comprueba Valores	
		//if(!$juzgado && !$juzgadoPaz){ $bTit = true; $this->errors .= 'Al menos un Juzgado debe ser especificado<br>'; }
		if(!empty($_FILES['adjunto']['tmp_name']) AND is_uploaded_file($_FILES['adjunto']['tmp_name']) AND $_FILES['adjunto']['type']!="application/msword"){
			//echo $_FILES['adjunto']['type'];exit;//application/msword
			$b2 = true; $this->errors .= 'El Tipo de archivo es diferente de word<br>';
		}
		
		/*B�squeda del mismo correlativo anteriormente*/
		
			if($otroCorrelativo!="" && $anyoOtro>0 && $otroCorrelativo){
				if($materia==1){
					$correlativo="P-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==2){
					$correlativo="C-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==3){
					$correlativo="Const-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==4){
					$correlativo="Lab-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==5){
					$correlativo="Fon-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==6){
					$correlativo="Ind-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==7){
					$correlativo="Mipes-".$otroCorrelativo."-".$anyoOtro;
				}elseif($materia==8){
					$correlativo="Concil-".$otroCorrelativo."-".$anyoOtro;					
				}elseif($materia==9){
					$correlativo="Arbi-".$otroCorrelativo."-".$anyoOtro;					
				}
				$opcion3=1;
			}		
			
			if($opcion3==1){
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = "select count(*) from db_procuraduria.dbo.proceso where correlativo='$correlativo'";
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$contadorR=$rs->fields[0];
						if($contadorR>=1){
							$b3 = true; $this->errors .= 'El Correlativo ya existe<br>';
						}
					}
					unset($rs);
			}				
		/**/
		//$opcionAgregaResolImpug,$tipNewTipResolImpugnada,$newResolImpugnada,$newanyoResolImpugnada,$newDepeResolImpugnada,$desFechaFin,$sumillaNewResolImpugnada
		if($opcionAgregaResolImpug==1){
			//echo "xxx".$tipNewTipResolImpugnada."xxx";
			if($tipNewTipResolImpugnada<0 || !$tipNewTipResolImpugnada || $tipNewTipResolImpugnada=="none"){
				$b4 = true; $this->errors .= 'El Tipo de Resoluci�n debe ser especificado<br>';
			}	
			if($newResolImpugnada=="" || !$newResolImpugnada){
				$b5 = true; $this->errors .= 'El N�mero de Resoluci�n debe ser especificado<br>';
			}	
			if($newanyoResolImpugnada<0 || !$newanyoResolImpugnada){
				$b6 = true; $this->errors .= 'El A�o de Resoluci�n debe ser especificado<br>';
			}	
			if($newDepeResolImpugnada<0 || !$newDepeResolImpugnada || $newDepeResolImpugnada=="none"){
				$b7 = true; $this->errors .= 'La sigla de Resoluci�n debe ser especificado<br>';
			}	
			if($desFechaFin=="" || !$desFechaFin){
				$b8 = true; $this->errors .= 'La Fecha de resoluci�n debe ser especificado<br>';
			}	
			if($sumillaNewResolImpugnada=="" || !$sumillaNewResolImpugnada){
				$b9 = true; $this->errors .= 'La Sumilla de resoluci�n debe ser especificado<br>';
			}
			if(($tipNewTipResolImpugnada==7 && $newDepeResolImpugnada!=47)||
				($tipNewTipResolImpugnada==4 && ($newDepeResolImpugnada==47||$newDepeResolImpugnada==48||$newDepeResolImpugnada==36||$newDepeResolImpugnada==16))||
				($tipNewTipResolImpugnada==3 && ($newDepeResolImpugnada==25||$newDepeResolImpugnada==24||$newDepeResolImpugnada==47||$newDepeResolImpugnada==48))
				){
				$b10 = true; $this->errors .= 'La dependencia no coincide con el tipo de resoluci�n<br>';
			}
			if($tipNewTipResolImpugnada>0 && $newResolImpugnada!="" && $newDepeResolImpugnada>0 && $newanyoResolImpugnada>0){
					$sql_SP = "select count(*)
								from db_tramite_documentario.dbo.resolucion
								where id_tipo_resolucion=".$tipNewTipResolImpugnada."
								and nro_resol like '".$newResolImpugnada."'
								and coddep=".$newDepeResolImpugnada."
								and year(auditmod)=".$newanyoResolImpugnada."
								";
					 //echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						$contadorT=$rs->fields[0];
						if($contadorT>=1){
							$b11 = true; $this->errors .= 'La resoluci�n ya existe<br>';
						}
					}
					unset($rs);	
			}			
			
		}
		

		if($bTit||$b2||$b3||$b4||$b5||$b6||$b7||$b8||$b9||$b10||$b11){
			$objIntranet = new Intranet();
			$objIntranet->Header2('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body2('tramite_tit.gif',false,false);
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaProceso($naturaleza,$monto,$codDepa,$codProv,$codDist,$materia,$natu,$tipProc,$tipoResp,$juzgado,$exp,$sala,$exp2,$corte,$exp3,$medida,$opcion2,$nombre,$Busca,$RZ,true,$errors);
			
			$objIntranet->Footer2(false);
		}else{
						
			// Obtiene un identificador de conexi�n
			$this->abreConnDB();
			//$this->conn->debug = true;
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXEC  %s %d,%d,%d,'%s',%s,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,%s,%s,%s,%s,%s,%s,%d,%d",
							  ($idProceso>0) ? "db_procuraduria.dbo.modProceso" : "db_procuraduria.dbo.insProceso",
							  $id,
							  $this->PrepareParamSQL($materia),
							  $this->PrepareParamSQL($naturaleza),
							  ($this->PrepareParamSQL($natu)) ? $this->PrepareParamSQL($natu) : "NULL",
							  ($this->PrepareParamSQL($monto)) ? $this->PrepareParamSQL($monto) : "NULL",
							  $this->PrepareParamSQL($tipProc),
							  $this->PrepareParamSQL($tipoResp),
							  $this->PrepareParamSQL($codDepa),
							  $this->PrepareParamSQL($codProv),
							  $this->PrepareParamSQL($codDist),
							  $this->PrepareParamSQL($juzgado),
							  $this->PrepareParamSQL($exp),
							  ($this->PrepareParamSQL($sala)) ? $this->PrepareParamSQL($sala) : " ",
							  ($this->PrepareParamSQL($exp2)) ? $this->PrepareParamSQL($exp2) : " ",
							  ($this->PrepareParamSQL($corte)) ? $this->PrepareParamSQL($corte) : " ",
							  ($this->PrepareParamSQL($exp3)) ? $this->PrepareParamSQL($exp3) : " ",
							  $_SESSION['cod_usuario'],
							  $correlativo,
							  $this->PrepareParamSQL($codDistJud),
							  $this->PrepareParamSQL($codSede),
							  ($this->PrepareParamSQL($desFechaIni)) ? $this->PrepareParamSQL($desFechaIni) : "NULL",
							  $opcion3,
							  ($this->PrepareParamSQL($juzgadoPaz)) ? $this->PrepareParamSQL($juzgadoPaz) : "NULL",
							  ($this->PrepareParamSQL($exp4)) ? $this->PrepareParamSQL($exp4) : "NULL",
							  ($this->PrepareParamSQL($especialista4)) ? $this->PrepareParamSQL($especialista4) : "NULL",
							  ($this->PrepareParamSQL($tribunal)) ? $this->PrepareParamSQL($tribunal) : "NULL",
							  ($this->PrepareParamSQL($exp5)) ? $this->PrepareParamSQL($exp5) : "NULL",
							  ($this->PrepareParamSQL($especialista5)) ? $this->PrepareParamSQL($especialista5) : "NULL",
							  ($this->PrepareParamSQL($especialista1)) ? $this->PrepareParamSQL($especialista1) : "NULL",
							  ($this->PrepareParamSQL($especialista2)) ? $this->PrepareParamSQL($especialista2) : "NULL",
							  ($this->PrepareParamSQL($especialista3)) ? $this->PrepareParamSQL($especialista3) : "NULL",
							  ($resolAutoritativa) ? $resolAutoritativa : "NULL",
							  ($abogado) ? $abogado : "NULL",
							  ($embClonada1) ? "'".$this->PrepareParamSQL($embClonada1)."'" : "NULL",
							  ($matriEmbClonada1) ? "'".$this->PrepareParamSQL($matriEmbClonada1)."'" : "NULL",
							  ($embClonada2) ? "'".$this->PrepareParamSQL($embClonada2)."'" : "NULL",
							  ($matriEmbClonada2) ? "'".$this->PrepareParamSQL($matriEmbClonada2)."'" : "NULL",
							  ($embClonada3) ? "'".$this->PrepareParamSQL($embClonada3)."'" : "NULL",
							  ($matriEmbClonada3) ? "'".$this->PrepareParamSQL($matriEmbClonada3)."'" : "NULL",
							  $tipoMonto,
							  $etapaProcesal
							  );
			//echo $sql_SP;exit;
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									if($row = $rs->FetchRow()){
										$RETVAL = $row[0];
										$idProc = $row[1];
									}else
										$RETVAL = 1;
									$rs->Close();
								}
								unset($rs);
								
						
				if(!$RETVAL&& !$idProceso){
					// Inserta los Software's correspondientes al CPU
					for($i=0;$i<count($idSoft);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insProcesoRZ %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft[$i]),
										  $_SESSION['cod_usuario'],
										  $this->PrepareParamSQL($idCond[$i])										  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}

				if(!$RETVAL&& $medida!=""){
					// Inserta las Medidas Cautelares correspondientes al proceso
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insMedida %d,'%s','%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($medida),
										  $this->PrepareParamSQL($anotacion2),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}

				if(!$RETVAL&& $etapa!=""){
					// Inserta los Software's correspondientes al CPU
					
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insEtapa %d,'%s','%s','%s'",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($etapa),
										  $this->PrepareParamSQL($anotacion1),
										  $_SESSION['cod_usuario']);
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					
				}
				
				/**/if(!$RETVAL){
					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft2);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insEmbxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft2[$i]),
										  $_SESSION['cod_usuario'],
										  (!$idProceso)? 1 : 2									  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}/**/				

				/**/if(!$RETVAL){
					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft3);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insPlantaxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft3[$i]),
										  $_SESSION['cod_usuario'],
										  (!$idProceso)? 1 : 2									  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}/**/
				
				/**/if(!$RETVAL){
					if($opcionAgregaResolImpug==1){
						//Se registra la resoluci�n
								$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_RegistraResolucionPasada %d,%d,'%s',%d,'%s',%d,'%s'",
										  $this->PrepareParamSQL($tipNewTipResolImpugnada),
										  $this->PrepareParamSQL($newResolImpugnada),
										  $sumillaNewResolImpugnada,
										  $newanyoResolImpugnada,
										  $desFechaFin,
										  $newDepeResolImpugnada,
										  $_SESSION['cod_usuario']				  
										  );
								//echo 		$sql_SP;  
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else{
									if($row2 = $rs->FetchRow()){
										$RETVAL = $row2[0];
										$idResolucionAntigua = $row2[1];
									}else
										$RETVAL = 1;
									$rs->Close();
								}
								unset($rs);
								
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insResolImpugnadaxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idResolucionAntigua),
										  $_SESSION['cod_usuario'],
										  (!$idProceso)? 1 : 2									  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);
						//exit;
						
					}

					// Inserta las embarcaciones x cada proceso
					for($i=0;$i<count($idSoft4);$i++){
						$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_insResolImpugnadaxProceso %d,%d,'%s',%d",
										  $this->PrepareParamSQL($idProc),
										  $this->PrepareParamSQL($idSoft4[$i]),
										  $_SESSION['cod_usuario'],
										  (!$idProceso)? 1 : 2									  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}
				}		/**/		
				
			if(!empty($_FILES['adjunto']['tmp_name']) AND is_uploaded_file($_FILES['adjunto']['tmp_name'])){ 
				if(filesize($_FILES['adjunto']['tmp_name'])<512000){ 
					//$destinoX='/var/www/intranet/institucional/aplicativos/pp/procesosProduce/archivos/'.$idProc.'/'.$_FILES['adjunto']['name'];
					/*$ruta="/var/www/intranet/institucional/aplicativos/pp/procesosProduce/archivos/".$idProc;
					mkdir ($ruta, 0700);*/
					
					//$destinoX='/var/www/intranet/institucional/aplicativos/pp/procesosProduce/archivos/'.$_FILES['adjunto']['name'];
					$destinoX='/var/www/intranet/institucional/aplicativos/pp/procesosProduce/archivos/'.$idProc.'.doc';
					//$destinoX=$ruta.'/'.$_FILES['adjunto']['name'];
					if(move_uploaded_file($_FILES['adjunto']['tmp_name'],$destinoX)){ 
						//echo 'Exito en la exportaci�n';
					}else{ 
						echo 'Erreur lors de la copie du fichier'; 
					} 
				}else{
					echo "El archivo supera el tama�o permitido";
				}			
			}		
		    //echo "matrix is here";				
						
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();
				
			//exit;	
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
			
		}
	}		
	
	function FormAgregaAvancesDir($idObsDir,$observaciones=NULL,$errors=false){
		if(empty($idObsDir)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);
		//$html->assign_by_ref('FechaActual',$this->FechaActual());
		//$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddObsDocDir';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('idObsDir',$idObsDir);
		$html->assign_by_ref('observaciones',$observaciones);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArm.tpl.php');
		$html->display('pp/procesoConst/frmAddObsDir.tpl.php');
		$html->display('oad/footerArm.tpl.php');			
	}
	
	function AgregaAvancesDir($idObsDir,$observaciones){
		// Comprueba Valores	
		if(!$observaciones){ $b1 = true; $this->errors .= 'El avance debe ser especificado<br>'; }

		if($b1){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaAvancesDir($idObsDir,$observaciones,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_procJudicial %d,'%s',%d,'%s'",
								$idObsDir,
							  $this->PrepareParamSQL($observaciones),
							  $this->userIntranet['COD_DEP'],
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
		}
	}
	
	function buscaAceptacion($dat){
		$this->abreConnDB();
		//$this->conn->debug = true;
		$sql="select convert(varchar,audit_rec,103)+' '+convert(varchar,audit_rec,108)
		         from movimiento_documento
				 where id_movimiento_documento in (select max(id_movimiento_documento) from movimiento_documento where id_documento=$dat and id_dependencia_destino=15 ) "; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					$rec=$rs->fields[0];
					$rs->Close();//Agregado el 22/03/2005
				}
				unset($rs);//Agregado el 22/03/2005
		if($rec){$acepta=$rec;}
		else{$acepta="";}
		return($acepta);
	}

	function DetalleDocumentoDir($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql=sprintf("select d.id_documento,d.id_tipo_documento,d.num_tram_documentario,
		                     d.indicativo_oficio,case when d.id_tipo_documento=1 then d.asunto
							                          when d.id_tipo_documento=2 then tup.descripcion 
													  when d.id_tipo_documento=4 then d.asunto end,
							 convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
							 cdi.descripcion,dep1.siglas,dep2.siglas,
							 a.nivel1,a.nivel2,
							 a.nivel3,convert(varchar,a.auditmod,103),convert(varchar,a.auditmod,108),
							 fd.observaciones,convert(varchar,fd.auditmod,103),convert(varchar,fd.auditmod,108),a.usuario,fd.usuario,
							 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),md.avance,
							 d.observaciones,
							 case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	                              when sol.id_tipo_persona=2 then sol.razon_social end,sol.direccion,sol.email,sol.telefono
					from dbo.clase_documento_interno cdi,
					      documento d  left join db_general.dbo.persona sol on d.id_persona=sol.id
						               left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
									   left join almacen_documento a on d.id_documento=a.id_documento and a.coddep=".$this->userIntranet['COD_DEP']." 					
									   left join finaldoc fd on d.id_documento=fd.id_documento and fd.coddep=".$this->userIntranet['COD_DEP']." ,
						 movimiento_documento md INNER JOIN db_general.dbo.h_dependencia dep1 on md.id_dependencia_origen=dep1.codigo_dependencia
													 LEFT JOIN db_general.dbo.h_dependencia dep2 on md.id_dependencia_destino=dep2.codigo_dependencia
					  where d.id_clase_documento_interno=cdi.id_clase_documento_interno and
					  		d.id_documento=md.id_documento and 
					      d.id_documento=%d",$this->PrepareParamSQL($id));
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$idTipoDoc=$rs->fields[1];
			$numTram=$rs->fields[2];
			$indicativo=$rs->fields[3];
			$asunto=$rs->fields[4];
			$fecRec=$rs->fields[5];
			$claseDoc=$rs->fields[6];
			$depOrigen=$rs->fields[7];
			//$depDestino=$this->busSiglasDocMult($id);
			$depDestino2=$rs->fields[8];
			$auditRecibido=$this->buscaAceptacion($id);
			$nivel1=$rs->fields[9];
			$nivel2=$rs->fields[10];
			$nivel3=$rs->fields[11];
			$fec2=$rs->fields[12];
			$hora2=$rs->fields[13];
			$fecha2=$fec2." ".$hora2;
			$observaciones=$rs->fields[14];
			$fec1=$rs->fields[15];
			$hora1=$rs->fields[16];
			$fecha1=$fec1." ".$hora1;
			$user=$rs->fields[17];
			$user2=$rs->fields[18];
			$fecDer=$rs->fields[19];
			$avance=$rs->fields[20];
			$obs=$rs->fields[21];
			$RazonSocial=$rs->fields[22];
			$direccion=$rs->fields[23];
			$email=$rs->fields[24];
			$telefono=$rs->fields[25];
		}	
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idTipoDoc',$idTipoDoc);				  
		$html->assign_by_ref('numTram',$numTram);
		$html->assign_by_ref('indicativo',$indicativo);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('fecRec',$fecRec);			
		$html->assign_by_ref('claseDoc',$claseDoc);	  
		$html->assign_by_ref('depOrigen',$depOrigen);
		$html->assign_by_ref('depDestino',$depDestino);
		$html->assign_by_ref('depDestino2',$depDestino2);
		$html->assign_by_ref('auditRecibido',$auditRecibido);
		$html->assign_by_ref('nivel1',$nivel1);
		$html->assign_by_ref('nivel2',$nivel2);
		$html->assign_by_ref('nivel3',$nivel3);
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('fecha2',$fecha2);
		$html->assign_by_ref('user',$user);
		$html->assign_by_ref('user2',$user2);	
		$html->assign_by_ref('fecDer',$fecDer);
		$html->assign_by_ref('avance',$avance);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('telefono',$telefono);
		$sql_SP = sprintf("select tr.apellidos_trabajador+' '+tr.nombres_trabajador,dir.avance,dir.id_oficio,
									convert(varchar,dir.auditmod,103),convert(varchar,dir.auditmod,108),
									convert(varchar,dir.audit_rec,103),convert(varchar,dir.audit_rec,108),
									dir.observaciones,mt.link,mt.observaciones
		                        from documento d,movimiento_documento md,movimiento_tratamiento mt,dirigido dir,DB_GENERAL.dbo.H_TRABAJaDOR tr
							where md.derivado=0 and md.id_dependencia_destino=%d 
							and d.id_documento=md.id_documento
							and mt.derivado=0
							and md.id_movimiento_documento=mt.id_movimiento
							and dir.codigo_persona=tr.codigo_trabajador and dir.id_movimiento_tratamiento=mt.id_movimiento_tratamiento
							and d.id_documento=%d",$this->userIntranet['COD_DEP'],$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('doc', array(//'id'=>$id,
										   'nombre'=>$row[0],
										   'avance'=>$row[1],
										   //'idOficio' => $this->bucaDetalleDocumentoDir($row[2]),
										   'diaEnvio'=>$row[3],
										   'horaEnvio'=>$row[4],
										   'diaRec'=>$row[5],
										   'horaRec'=>$row[6],
										   'obs'=>$row[7],
										   'link'=>$row[8],
										   'obsSecre'=>$row[9]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		
		
		if($idTipoDoc==4){
			$sql_st = sprintf("SELECT d.indicativo_oficio,d.asunto,d.observaciones,Upper(dep1.siglas),Upper(dep2.siglas),
		                          Upper(c.descripcion),convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  md.avance 
							FROM db_tramite_documentario.dbo.movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                                     left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                                 db_tramite_documentario.dbo.documento d, dbo.clase_documento_interno c
							WHERE md.id_documento=%d and md.id_oficio>0 and d.id_documento=md.id_oficio and d.id_clase_documento_interno=c.id_clase_documento_interno
						      order by d.id_documento  
							",
							$id);
		}else{
			$sql_st = sprintf("SELECT d.indicativo_oficio,d.asunto,d.observaciones,Upper(dep1.siglas),Upper(dep2.siglas),
		                          Upper(c.descripcion),convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),
								  md.avance 
							FROM db_tramite_documentario.dbo.movimiento_documento md inner join db_general.dbo.h_dependencia dep1 on dep1.codigo_dependencia=md.id_dependencia_origen
	                                                     left join db_general.dbo.h_dependencia dep2 on dep2.codigo_dependencia=md.id_dependencia_destino,
	                                 db_tramite_documentario.dbo.documento d, dbo.clase_documento_interno c
							WHERE md.id_documento=%d and md.id_oficio>0 and d.id_documento=md.id_oficio and d.id_clase_documento_interno=c.id_clase_documento_interno
						      order by d.id_documento  
							",
							$id);
		}
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('dir2',array('ind' => $row[0],
										   'cla' => $row[5],
										   'depo' => $row[3],
										   'depd' => $row[4],
										   'asu' => ucfirst($row[1]),
										   'obs' => ucfirst($row[2]),
										   'fDer' => $row[6],
										   'avance' => $row[7]
										   ));
			$rs->Close();
		}
		unset($rs);
		//Obtenemos los datos del Proceso Constitucianal asociado al Documento
		$sql_SP = sprintf("SELECT natu.descripcion,pro.monto,tiproc.descripcion,tr.descripcion,dist.descripcion,mat.descripcion
							FROM db_procuraduria.dbo.proceso pro,db_tramite_documentario.dbo.documento d,
							     db_procuraduria.dbo.sede sed,db_procuraduria.dbo.distritojudicial dist,
								 db_procuraduria.dbo.naturaleza natu,db_procuraduria.dbo.tipo_proceso tiproc,
								 db_procuraduria.dbo.tipo_responsabilidad tr,db_procuraduria.dbo.materia mat
							WHERE
							     d.id_documento=%d and d.id_documento=pro.id_documento and 
								 pro.codigo_distritojudicial=sed.codigo_distritojudicial and sed.codigo_sede=pro.codigo_sede and 
								 pro.codigo_distritojudicial=dist.codigo_distritojudicial and natu.id=pro.id_naturaleza and
								 pro.id_tipo_responsabilidad=tr.id and
								 pro.id_tipo_proceso=tiproc.id and mat.id=pro.materia
							",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{

				$naturaleza=$rs->fields[0];
				$monto=$rs->fields[1];										   
				$tipProceso=$rs->fields[2];
				$tipResponsabilidad=$rs->fields[3];
				$distrito=$rs->fields[4];
				$materia=$rs->fields[5];
				
				$html->assign_by_ref('naturaleza',$naturaleza);
				$html->assign_by_ref('monto',$monto);
				$html->assign_by_ref('tipProceso',$tipProceso);
				$html->assign_by_ref('tipResponsabilidad',$tipResponsabilidad);
				$html->assign_by_ref('distrito',$distrito);
				$html->assign_by_ref('materia',$materia);
		}
		unset($rs);
		
		$sql_SP = sprintf("SELECT med.descripcion,med.auditmod
					from db_procuraduria.dbo.proceso pro,db_procuraduria.dbo.medida med
					where pro.id_documento=%d and pro.id=med.id_proceso",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('med', array(//'id'=>$id,
										   'descripcion'=>$row[0],
										   'fecha'=>$row[1]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("SELECT et.descripcion,et.auditmod
					from db_procuraduria.dbo.proceso pro,db_procuraduria.dbo.etapas et
					where pro.id_documento=%d and pro.id=et.id_proceso",$this->PrepareParamSQL($id));
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow())
				$html->append('eta', array(//'id'=>$id,
										   'descripcion'=>$row[0],
										   'fecha'=>$row[1]
										   ));
										   
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('pp/procesoConst/printDetallesDoc.tpl.php') : $html->display('pp/procesoConst/showDetallesDoc.tpl.php');
	}
	
 function ImprimeFlujo($id){
  $this->DetalleDocumentoDir($id, true);
 }
 
	function FormGeneraReporte($search2=false){
		global $tipReporte,$nrotramite;
		global $fecInicio,$fecFin;
		global $GrupoOpciones1;
		global $codSede,$codDistJud,$materia;
		global $mes_ini,$dia_ini,$anyo_ini;
		global $mes_fin,$dia_fin,$anyo_fin;
		
		$fecInicio = $_POST['mes_ini'].'/'.$_POST['dia_ini'].'/'.$_POST['anyo_ini'];
		$fecFin = $_POST['mes_fin'].'/'.$_POST['dia_fin'].'/'.$_POST['anyo_fin'];
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='//'&&$fecFin) ? $fecFin : date('m/d/Y');
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		//$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('nrotramite',$nrotramite);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		
		$sql_st = "SELECT ID, lower(descripcion) ".
					  "FROM db_procuraduria.dbo.materia ".
					  "ORDER BY 2";
		$html->assign_by_ref('selMateria',$this->ObjFrmSelect($sql_st, $materia, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_distritojudicial, descripcion ".
				  "FROM db_procuraduria.dbo.distritojudicial ".
				  "ORDER BY 2";
		$html->assign('selDistJud',$this->ObjFrmSelect($sql_st, $codDistJud, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_sede, Lower(descripcion) ".
				  		  "FROM db_procuraduria.dbo.sede ".
				  		  "WHERE codigo_distritojudicial='%s' ".
				  		  "ORDER BY 2",(!is_null($codDistJud)||!empty($codDistJud)) ? $codDistJud : 'xx');
		$html->assign('selSede',$this->ObjFrmSelect($sql_st, $codSede, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Fecha Ingreso
		$html->assign_by_ref('selMesIng',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaIng',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true, array('val'=>'none','label'=>'----')));
		$html->assign_by_ref('selAnyoIng',$this->ObjFrmAnyo(date('Y')-32, date('Y'), substr($fecInicio,6,4), true, array('val'=>'none','label'=>'--------')));
		
		// Fecha Salida
		$html->assign_by_ref('selMesSal',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true, array('val'=>'none','label'=>'------------------')));
		$html->assign_by_ref('selDiaSal',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true, array('val'=>'none','label'=>'----')));
		//$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));
		$html->assign_by_ref('selAnyoSal',$this->ObjFrmAnyo(date('Y')-32, date('Y'), substr($fecFin,6,4), true, array('val'=>'none','label'=>'--------')));		

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$accionHeader=2;
		$html->assign_by_ref('accionHeader',$accionHeader);		
		
		// Muestra el Formulario
		$html->display('pp/headerArmn.tpl.php');
		$html->display('pp/procesoConst/reportes.tpl.php');
		if(!$search2) $html->display('pp/footerArm.tpl.php');
	}
	
	function GeneraReporte($tipReporte,$nrotramite,$fecInicio,$fecFin,$GrupoOpciones1,$codSede,$codDistJud,$materia){
	//$fecInicio= ($_POST['fecInicio']) ? $_POST['fecInicio'] : $_GET['fecInicio'];
		switch($GrupoOpciones1){
			case 1:
			$this->Reporte1($fecInicio,$fecFin,$materia);
			break;
			case 2:
			$this->Reporte2($fecInicio,$fecFin,$codSede,$codDistJud);
			break;
			case 3:
			$this->ListaRelacionExpedientes($codDistJud);
			break;
		}
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0"){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s  ",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename
									));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}
	
	function ValidaFechaProyecto($fecha,$error){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if( !checkdate(sprintf('%d',substr($fecha,0,2)),sprintf('%d',substr($fecha,3,2)),substr($fecha,6,4)) )
			$errors .= "- La Fecha {$error} no es Valida.<br>";
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}

	function Reporte1($fecInicio,$fecFin,$materia){

		/**/$anio=date('Y');
		//Manipulacion de las Fechas
		$fecInicio = ($fecInicio!='//'&&$fecInicio&&!strstr($fecInicio,'none')) ? $fecInicio : NULL;
		$fecFin = ($fecFin!='//'&&$fecFin&&!strstr($fecFin,'none')) ? $fecFin : NULL;

		if($fecInicio){$bFIng = $this->ValidaFechaProyecto($fecInicio,'Inicio');}
		

		if($fecFin){ $bFSal = ($this->ValidaFechaProyecto($fecFin,'Fin')); }
		if ($fecInicio&&$fecFin){

		//echo $fecInicio."<br>";
		//echo $fecFin."<br>";
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecInicio, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFin, $regs2);
		
		

		
		if ($regs[3]>$regs2[3]){echo "<strong>ERROR EN LOS ANYOS</strong>";exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "<strong>ERROR EN LOS MESES</strong>";exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "<strong>ERROR EN LOS D�AS</strong>";exit;}
					}
			}
		}		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
		}else{
		
			// Genera HTML
			$html = new Smarty;
			
			$this->abreConnDB();
			//$this->conn->debug = true;
			$html->assign_by_ref('fecInicio',$fecInicio);
			$html->assign_by_ref('fecFin',$fecFin);
			$fecInicio2=$regs[2].'/'.$regs[1].'/'.$regs[3];
			$fecFin2=$regs2[2].'/'.$regs2[1].'/'.$regs2[3];
			$html->assign_by_ref('fecFin2',$fecFin2);
			$html->assign_by_ref('fecInicio2',$fecInicio2);

			$sql_st=sprintf("select p.id,p.correlativo,nat.descripcion,p.monto,tp.descripcion,tr.descripcion,
			                       mat.descripcion, p.juzgado, p.exp1, p.sala, p.exp2, 
								   p.corte, p.exp3,dist.descripcion,
								   case when p.id_naturaleza in (11,12,13,14) then p.delito end
							from db_procuraduria.dbo.proceso p, db_procuraduria.dbo.naturaleza nat,
								 db_procuraduria.dbo.materia mat, db_procuraduria.dbo.tipo_proceso tp,
								 db_procuraduria.dbo.tipo_responsabilidad tr,
								 db_procuraduria.dbo.distritojudicial dist
							where
							   p.id_naturaleza=nat.id and p.id_tipo_proceso=tp.id and p.id_tipo_responsabilidad=tr.id 
							   and p.materia=mat.id
							   and p.codigo_distritojudicial=dist.codigo_distritojudicial %s
							   and p.auditmod>convert(varchar,'$fecInicio2',103)
							   and p.auditmod<dateadd(dd,1,convert(datetime,'$fecFin2',103))
							order by p.id desc 
					  ",($materia>0) ? " and p.materia=$materia " : " ");
					  
			//echo $sql_st; exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow())
					$html->append('list',array('id' => $row[0],
												'natu' => $row[2],
												'monto' => $row[3],
												'tipProc' => $row[4],
												'tipResp' => $row[5],
												'dist' => $row[13],
												'mat' => $row[6],
												'juz' => $row[7],
												'exp1' => $row[8],
												'sala' => $row[9],
												'exp2' => $row[10],
												'corte' => $row[11],
												'exp3' => $row[12],
												'otRazSoc' => $this->buscaOtrasRZ($row[0]),
												'medCautelar' => $this->buscaMedCautelar($row[0]),
												'etapas' => $this->buscaEtapas($row[0]),
												'cor' => $row[1],
												'delito' => $row[14]
												));
				$rs->Close();
			}
			
			unset($rs);
			
			//setlocale(LC_TIME,$this->_zonaHoraria);
			setlocale(LC_TIME,"spanish");
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$dependencia=($dependencia>0)? $this->buscaSiglasDependencia($dependencia) : $this->userIntranet['DEPENDENCIA'];
			//$html->assign_by_ref('dependencia',$dependencia);
			//$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
			//$html->assign_by_ref('a',$a);
			
			$html->assign_by_ref('materia',$materia);
			
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
			
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'list'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/ListadoProcesos.tpl.php'),true,$logo);//true para horizontal y false para vertical
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;
		}//fin del if($fecInicio,$fecFin)
		}
		/**/		
	}

	function Reporte2($fecInicio,$fecFin,$codSede,$codDistJud){

		/**/$anio=date('Y');
		//Manipulacion de las Fechas
		$fecInicio = ($fecInicio!='//'&&$fecInicio&&!strstr($fecInicio,'none')) ? $fecInicio : NULL;
		$fecFin = ($fecFin!='//'&&$fecFin&&!strstr($fecFin,'none')) ? $fecFin : NULL;

		if($fecInicio){$bFIng = $this->ValidaFechaProyecto($fecInicio,'Inicio');}
		

		if($fecFin){ $bFSal = ($this->ValidaFechaProyecto($fecFin,'Fin')); }
		if ($fecInicio&&$fecFin){

		//echo $fecInicio."<br>";
		//echo $fecFin."<br>";
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecInicio, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $fecFin, $regs2);
		
		

		
		if ($regs[3]>$regs2[3]){echo "<strong>ERROR EN LOS ANYOS</strong>";exit;}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[1]>$regs2[1])
				{
					echo "<strong>ERROR EN LOS MESES</strong>";exit;
				}
			else
			{
				if($regs[1]==$regs2[1])
					{
						if ($regs[2]>$regs2[2]){echo "<strong>ERROR EN LOS D�AS</strong>";exit;}
					}
			}
		}		
		if($bFIng||$bFSal){
			echo "<strong>INGRESO DE FECHAS INCORRECTAS. VUELVA A INTENTARLO</strong>";
		}else{
		
			// Genera HTML
			$html = new Smarty;
			
			$this->abreConnDB();
			$this->conn->debug = false;
			$html->assign_by_ref('fecInicio',$fecInicio);
			$html->assign_by_ref('fecFin',$fecFin);
			$fecInicio2=$regs[2].'/'.$regs[1].'/'.$regs[3];
			$fecFin2=$regs2[2].'/'.$regs2[1].'/'.$regs2[3];
			$html->assign_by_ref('fecFin2',$fecFin2);
			$html->assign_by_ref('fecInicio2',$fecInicio2);

			$sql_st="select p.id,p.correlativo,nat.descripcion,p.monto,tp.descripcion,tr.descripcion,
			                       mat.descripcion, p.juzgado, p.exp1, p.sala, p.exp2, 
								   p.corte, p.exp3,dist.descripcion,
								   case when p.id_naturaleza in (11,12,13,14) then p.delito end
							from db_procuraduria.dbo.proceso p, db_procuraduria.dbo.naturaleza nat,
								 db_procuraduria.dbo.materia mat, db_procuraduria.dbo.tipo_proceso tp,
								 db_procuraduria.dbo.tipo_responsabilidad tr,
								 db_procuraduria.dbo.distritojudicial dist,
								 db_procuraduria.dbo.sede sed
							where
							   p.id_naturaleza=nat.id and p.id_tipo_proceso=tp.id and p.id_tipo_responsabilidad=tr.id 
							   and p.materia=mat.id
							   and p.codigo_distritojudicial=dist.codigo_distritojudicial
							   and p.codigo_sede=sed.codigo_sede
					  and p.codigo_distritojudicial=sed.codigo_distritojudicial
					  and p.codigo_sede='$codSede' and p.codigo_distritojudicial='$codDistJud'
					  and p.auditmod>convert(varchar,'$fecInicio2',103)
					  and p.auditmod<dateadd(dd,1,convert(datetime,'$fecFin2',103))
					  order by p.id desc 
					  ";
			//echo $sql_st; exit;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				print $this->conn->ErrorMsg();
			else{
				while ($row = $rs->FetchRow())
					$html->append('list',array('id' => $row[0],
												'natu' => $row[2],
												'monto' => $row[3],
												'tipProc' => $row[4],
												'tipResp' => $row[5],
												'dist' => $row[13],
												'mat' => $row[6],
												'juz' => $row[7],
												'exp1' => $row[8],
												'sala' => $row[9],
												'exp2' => $row[10],
												'corte' => $row[11],
												'exp3' => $row[12],
												'otRazSoc' => $this->buscaOtrasRZ($row[0]),
												'medCautelar' => $this->buscaMedCautelar($row[0]),
												'etapas' => $this->buscaEtapas($row[0]),
												'cor' => $row[1],
												'delito' => $row[14]
												));
				$rs->Close();
			}
			
			unset($rs);
			
			//setlocale(LC_TIME,$this->_zonaHoraria);
			setlocale(LC_TIME,"spanish");
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$dependencia=($dependencia>0)? $this->buscaSiglasDependencia($dependencia) : $this->userIntranet['DEPENDENCIA'];
			//$html->assign_by_ref('dependencia',$dependencia);
			//$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
			//$html->assign_by_ref('a',$a);
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
			
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			$filename = 'list'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/ListadoProcesos.tpl.php'),true,$logo);//true para horizontal y false para vertical
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;
		}//fin del if($fecInicio,$fecFin)
		}
		/**/		
	}

	
	function ListaRelacionExpedientes($codDistJud){
		$this->abreConnDB();
		//$this->conn->debug = true;

		// Obtiene los Datos del Inventariado
		//$sql_SP = "select descripcion,codigo_distritojudicial+' '+descripcion from db_procuraduria.dbo.sede where codigo_distritojudicial='$codDistJud'";
		//$sql_SP = "select descripcion,codigo_sede+' '+descripcion from db_procuraduria.dbo.sede where codigo_distritojudicial='$codDistJud' ";
		/**/$sql_SP = "select sed.descripcion,sed.codigo_sede+' '+sed.descripcion
					  from db_procuraduria.dbo.sede sed
					  where sed.codigo_distritojudicial='$codDistJud' and sed.codigo_sede in (select codigo_sede from db_procuraduria.dbo.proceso)";
		/**/
		/*
		$sql_SP = "select sed.descripcion,sed.codigo_sede+' '+sed.descripcion 
		              from db_procuraduria.dbo.sede sed,db_procuraduria.dbo.proceso p 
					  where p.codigo_distritojudicial='$codDistJud' and p.codigo_distritojudicial=sed.codigo_distritojudicial ";
		*/
		// echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs){
			print $this->conn->ErrorMsg();
			return;
		}else{
			
			if($row = $rs->FetchRow()){
				$desDependencia = $row[0];
				$desTrabajador = $row[1];
			}
			/**/
						$i=-1;
						while(!$rs->EOF){
						$i++;
						$arregloP[$i]=$rs->fields[1];
						$rs->MoveNext();
						}
						/*
							$depe=$arregloP[0];
							$contador=count($arregloP);
							//echo "el contador".$depe;
							if($contador==1){
								$depe1=$depe;
							}
							if($contador>1){
								$depe1=$depe;
								for($h=1;$h<$contador;$h++){
									$depe1=$depe1.' - '.$arregloP[$h];
								}
							}//fin del if($contador>0)
						*/
			
			/**/
			// list(null,$codTrabajador,$codInventariado,$desNombre,$codPatrimonial,$desSerial,
			//	 $idTipo,$idMarca,$idModelo,$desTipo,$fecIngreso,$codEmp,$codEntrada,$numGaran) = $rs->FetchRow();
			$rs->Close();
		}
		unset($rs);
		
		//echo "holas".$row[1];

		// Obtiene las Atenciones sobre el inventariado
		$sql_st = "select distinct sed.descripcion,p.id,p.correlativo,case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
		                                     when sol.id_tipo_persona=2 then sol.razon_social end,p.juzgado,p.exp1
						from db_procuraduria.dbo.proceso p,db_general.dbo.persona sol,
						      db_procuraduria.dbo.rz_procuraduria rz,db_procuraduria.dbo.sede sed
							  where p.id=rz.id_proceso and rz.id_persona=sol.id
							  and p.codigo_distritojudicial='$codDistJud' and p.codigo_sede=sed.codigo_sede 
							  and sed.codigo_distritojudicial='$codDistJud'
									 ";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			// Crea el Objeto HTML
			$html = & new Smarty;
			$cUsr = $cDep = $loop = 0;
			while ($row = $rs->FetchRow()){
				if($loop==0){
					$userTemp = $row[0];
					//$userTemp = $arregloP[0];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				if($userTemp!=$row[0]){
					$cUsr++;
					$userTemp = $row[0];
					//$userTemp = $arregloP[$cUsr];
					$ate['users'][$cUsr]['name'] = $userTemp;
				}
				$ate['users'][$cUsr]['ates'][] = array(	'moti'=>ucfirst($row[2]),
														// 'trab'=>ucwords($row[2]),
														// 'depe'=>$row[3],
														'obse'=>ucfirst($row[3]),
														'fech'=>$row[4],
														'tota'=>$row[5]);
				$loop++;
			}
			$rs->Close();
						
			$html->assign_by_ref('ate',$ate);
			
			$html->assign_by_ref('pcDep',$desDependencia);
			$html->assign_by_ref('pcTrab',$desTrabajador);
			$html->assign_by_ref('pcName',$desNombre);
			$html->assign_by_ref('pcCod',$codInventariado);
			
			setlocale(LC_TIME, $this->zonaHoraria);
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$header = "holas";

			$logo = '/var/www/intranet/img/800x600/img.rep.logo.13.gif';			
			$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];			
			
			//$options = $options. " --footer D D D ";
			//$path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['PHP_SELF']) . '/reports';
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			
			$filename = sprintf("ate%s%s", '-'.mktime(), '-'.$idCPU);
			
			// Genera Archivo PDF			
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesoConst/reportAten1.tpl.php'),false,$logo,$options);

			//$destination = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/reports/' . $filename . '.pdf';
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Location: $destination");
			exit;
			// $html->display('oti/inventario/pcs/reportes/reportAten1.tpl.php');
		}
	}
	
	function FormFinalizaProceso($id,$GrupoOpciones1=NULL,$resol=NULL,$desFechaIni=NULL,$GrupoOpciones2=NULL,$monto=NULL,$motivo=NULL,$errors=false){
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;

		/*$html->assign_by_ref('menu',$this->GeneraMenuPager(4));
		$html->assign_by_ref('menuPager',$this->menuPager);*/
		//$html->assign_by_ref('FechaActual',$this->FechaActual());
		//$html->assign_by_ref('HoraActual',$this->HoraActual());

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmFinalizaProceso';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('resol',$resol);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('GrupoOpciones2',$GrupoOpciones2);
		$html->assign_by_ref('monto',$monto);
		$html->assign_by_ref('motivo',$motivo);

		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display('pp/headerArmn.tpl.php');
		$html->display('pp/procesosProduce/frmFinalizaProceso.tpl.php');
		$html->display('oad/footerArm.tpl.php');			
	}
	
	function FinalizaProceso($id,$GrupoOpciones1,$resol,$desFechaIni,$GrupoOpciones2,$monto,$motivo){
		// Comprueba Valores	
		if(!$resol){ $b1 = true; $this->errors .= 'La resoluci�n debe ser especificado<br>'; }
		if(!$desFechaIni){ $b2 = true; $this->errors .= 'La fecha de la resoluci�n debe ser especificado<br>'; }

		if($b1||$b2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Tr�mite Documentario',false,array('suspEmb'));
			$objIntranet->Body('proyectosinversion_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaAvancesDir($id,$GrupoOpciones1,$resol,$desFechaIni,$GrupoOpciones2,$monto,$motivo,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
						
			$sql_SP = sprintf("EXECUTE db_procuraduria.dbo.sp_finalizaProceso %d,%d,'%s','%s','%s',%d,'%s','%s'",
								$id,
							  $this->PrepareParamSQL($GrupoOpciones1),
							  $this->PrepareParamSQL($resol),
							  $this->PrepareParamSQL($motivo),
							  $this->PrepareParamSQL($desFechaIni),
							  $this->PrepareParamSQL($GrupoOpciones2),
							  $this->PrepareParamSQL($monto),
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP; exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			//echo $destination;exit;
			header("Location: $destination");
			exit;
		}
	}
	
	function ReporteExpediente($id){
		global $a,$b,$c;
		$a=($_POST['a']) ? $_POST['a'] : $_GET['a'];
		$b=($_POST['b']) ? $_POST['b'] : $_GET['b'];
		$c=($_POST['c']) ? $_POST['c'] : $_GET['c'];
		//echo $a;
 		/**/$anio=date('Y');
		//Manipulacion de las Fechas

		
			// Genera HTML
			$html = new Smarty;
			
			$this->abreConnDB();
			//$this->conn->debug = true;

			$sql_st = sprintf("SELECT p.id,p.id_documento,nat.descripcion,p.monto,tp.descripcion,p.id_tipo_responsabilidad,
			   						 p.codigo_departamento,p.codigo_provincia,p.codigo_distrito,
									 mat.descripcion,p.delito,p.juzgado,p.exp1,p.sala,p.exp2,
									 p.corte,p.exp3,dist.descripcion,p.codigo_sede,p.fecVenc,p.correlativo, 
									 p.juzgado_paz,p.expediente_juzgado_paz,p.especialista_juzgado_paz,p.id_resolucion_autoritativa, 
									 tr.apellidos_trabajador+' '+tr.nombres_trabajador,p.TRIBUNAL,EXPEDIENTE_TRIBUNAL,p.ESPECIALISTA_TRIBUNAL,p.ESPECIALISTA_JUZGADO_FISCALIA,p.ESPECIALISTA_SALA_SUPERIOR, 
									 p.ESPECIALISTA_CORTE_SUPREMA,p.estado_proceso,convert(varchar,p.audit_finalizacion,103),
									 p.resolucion_finalizacion,p.fecha_finalizacion,p.monto_finalizacion,p.motivo_finalizacion,
									 case when p.tipo_finalizacion=1 then 'Definitivo'
									 	  when p.tipo_finalizacion=2 then 'Provisional' end,
									 case when p.estado_archivamiento=1 then 'A favor'
									 	  when p.estado_archivamiento=2 then 'En contra' end,
									 r.nro_resol,r.sumilla
							  FROM db_procuraduria.dbo.proceso p left join db_procuraduria.dbo.tipo_proceso tp on p.id_tipo_proceso=tp.id
							  											  left join db_general.dbo.h_trabajador tr on p.id_abogado=tr.codigo_trabajador
																		  left join db_tramite_documentario.dbo.resolucion r on p.ID_RESOLUCION_AUTORITATIVA=r.id,
							  	   db_procuraduria.dbo.distritojudicial dist,db_procuraduria.dbo.naturaleza nat,
								   db_procuraduria.dbo.materia mat
							  WHERE p.id=%d and p.codigo_distritojudicial=dist.codigo_distritojudicial
							  and p.id_naturaleza=nat.id and p.materia=mat.id",$this->PrepareParamSQL($id));
			// echo $sql_st;
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs)
				$RETVAL=0;
			else{
				if($row = $rs->FetchRow())
					list($idProceso,$idDocumento,$naturaleza,$monto,$tipProc,
						 $tipoResp,$codDepa,$codProv,$codDist,$materia,$delito,
						 $juzgado,$exp,$sala,$exp2,$corte,$exp3,$DistJud,$codSede,$desFechaIni,$correlativoBD,
						 $juzgadoPaz,$exp4,$especialista4,$resolAutoritativa,$abogado,$tribunal,$exp5,$especialista5,
						 $especialista1,$especialista2,$especialista3,$estado,$fecFin,$resolFinalizacion,$fechaFinalizacion,
						 $montoFinalizacion,$motivoFinalizacion,$tipoFinalizacion,$estadoArchivamiento,
						 $nroResolucion,$sumillaResolucion) = $row;
				unset($row);
				$rs->Close();
			}
			unset($rs);
			
			//echo $nroResolucion."xx";
			
				$sql="select embxproc.id,embxproc.id_emb,emb.nombre_emb
					  from db_procuraduria.dbo.embxproceso embxproc,db_dnepp.user_dnepp.embarcacionnac emb
							where embxproc.id_proceso=$idProceso and embxproc.id_emb=emb.id_emb
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$html->append('emb', array('id' => $row[0],
														'idEmb' => ucwords($row[1]),
														'emb' => ucwords($row[2])
														));
						}							   
						unset($row);
						$rs->Close();
				}
				
				$sql="select plantaxproc.id,plantaxproc.id_persona,case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
																		else sol.razon_social end
					  from db_procuraduria.dbo.plantaxproceso plantaxproc,db_general.dbo.persona sol
							where plantaxproc.id_proceso=$idProceso and sol.id=plantaxproc.id_persona
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$html->append('planta', array('id' => $row[0],
														'idPlanta' => ucwords($row[1]),
														'planta' => ucwords($row[2])
														));
						}							   
						unset($row);
						$rs->Close();
				}
				
				$sql="select rzproc.id,rzproc.id_persona,rzproc.condicion, case when sol.id_tipo_persona=1 then sol.apellidos+' '+sol.nombres
																		else sol.razon_social end
					  from db_procuraduria.dbo.RZ_procuraduria rzproc,db_general.dbo.persona sol
							where rzproc.id_proceso=$idProceso and sol.id=rzproc.id_persona
							and rzproc.id_persona is not null 
					  ";
				//echo $sql;
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
							return;
				}else{
						while ($row = $rs->FetchRow()){
							$html->append('partesProc', array('id' => $row[0],
														'idPersona' => ucwords($row[1]),
														'condicion' => ucwords($row[2]),
														'nombres' => ucwords($row[3])
														));
						}							   
						unset($row);
						$rs->Close();
				}
				
			$sql="select id,descripcion,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108) 
			      from db_procuraduria.dbo.medida
						where id_proceso=$idProceso
				order by 1 desc
				  ";
			//echo $sql;
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('med', array( 'id'=>$row[0],
												   'medida'=>$row[1],
												   'fecha'=>$row[2]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
			$sql="select id,convert(varchar,auditmod,103)+' '+convert(varchar,auditmod,108),descripcion,anotacion
				from db_procuraduria.dbo.etapas
				where id_proceso=$idProceso
				order by 1 desc
				";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('eta', array( 'id'=>$row[0],
												   'etapa'=>$row[2],
												   'fecha'=>$row[1],
												   'anotacion'=>$row[3]
												   ));
												   
					unset($row);
					$rs->Close();
			}
			
			$sql="select ep.id,convert(varchar,ep.auditmod,103)+' '+convert(varchar,ep.auditmod,108),ep.EXCEPCION,te.descripcion
				from db_procuraduria.dbo.EXCEPCIONXPROCESO ep,db_procuraduria.dbo.tipo_excepcion te
				where ep.id_proceso=$idProceso and ep.id_tipo_excepcion=te.id 
				order by 1 desc
				";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
						return;
			}else{
					while ($row = $rs->FetchRow())
						$html->append('exc', array( 'id'=>$row[0],
												   'excepcion'=>$row[2],
												   'fecha'=>$row[1],
												   'tipExcepcion'=>$row[3]
												   ));
												   
					unset($row);
					$rs->Close();
			}			

			
			//setlocale(LC_TIME,$this->_zonaHoraria);
			setlocale(LC_TIME,"spanish");
			$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
			//$dependencia=($dependencia>0)? $this->buscaSiglasDependencia($dependencia) : $this->userIntranet['DEPENDENCIA'];
			//$html->assign_by_ref('dependencia',$dependencia);
			//$html->assign_by_ref('coddep',$this->userIntranet['COD_DEP']);
			//$html->assign_by_ref('a',$a);
			
			$html->assign_by_ref('id',$id);
			$html->assign_by_ref('a',$a);
			$html->assign_by_ref('b',$b);
			$html->assign_by_ref('c',$c);
			$html->assign_by_ref('naturaleza',$naturaleza);
			$html->assign_by_ref('monto',$monto);
			$html->assign_by_ref('materia',$materia);
			$html->assign_by_ref('delito',$delito);
			$html->assign_by_ref('juzgadoPaz',$juzgadoPaz);
			$html->assign_by_ref('exp4',$exp4);
			$html->assign_by_ref('juzgado',$juzgado);
			$html->assign_by_ref('exp',$exp);
			$html->assign_by_ref('especialista1',$especialista1);
			$html->assign_by_ref('sala',$sala);
			$html->assign_by_ref('exp2',$exp2);
			$html->assign_by_ref('corte',$corte);
			$html->assign_by_ref('exp3',$exp3);
			$html->assign_by_ref('tribunal',$tribunal);
			$html->assign_by_ref('exp5',$exp5);
			$html->assign_by_ref('especialista2',$especialista2);
			$html->assign_by_ref('DistJud',$DistJud);
			$html->assign_by_ref('naturaleza',$naturaleza);
			$html->assign_by_ref('tipProc',$tipProc);
			$html->assign_by_ref('abogado',$abogado);
			$html->assign_by_ref('estado',$estado);
			$html->assign_by_ref('fecFin',$fecFin);
			$html->assign_by_ref('resolFinalizacion',$resolFinalizacion);
			$html->assign_by_ref('fechaFinalizacion',$fechaFinalizacion);
			$html->assign_by_ref('correlativoBD',$correlativoBD);
			$html->assign_by_ref('montoFinalizacion',$montoFinalizacion);
			$html->assign_by_ref('motivoFinalizacion',$motivoFinalizacion);
			$html->assign_by_ref('tipoFinalizacion',$tipoFinalizacion);
			$html->assign_by_ref('estadoArchivamiento',$estadoArchivamiento);
			$html->assign_by_ref('nroResolucion',$nroResolucion);
			$html->assign_by_ref('sumillaResolucion',$sumillaResolucion);
			
			$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
			
			$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
			/*$filename = 'list'.mktime();
			$this->CreaArchivoPDF($filename,$path,$html->fetch('pp/procesosProduce/reportes/showExpediente.tpl.php'),false,$logo);//true para horizontal y false para vertical
	
			$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
			header("Content-type: application/pdf");
			header("Location: {$destination}");
			//echo $destination;
			exit;*/
			
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('pp/procesosProduce/reportes/showExpediente.tpl.php') : $html->display('pp/procesosProduce/reportes/showExpediente.tpl.php');			
			
		/**/		
	}	
	
}
?>