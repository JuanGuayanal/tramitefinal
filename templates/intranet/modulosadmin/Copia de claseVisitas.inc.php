<?
error_reporting(E_ALL);
include_once 'claseModulos.inc.php';
include_once 'claseIntranet.inc.php';

class Visitas extends Modulos{

	function Visitas() {
		
				// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		// Obtiene los Datos del Modulo Actual
		$this->ModuloInfo($_SESSION['modulo_id']);

		if(!$_SESSION['mod_ind_leer']){
			$objIntranet = new Intranet();
			$objIntranet->Header('Suspension de Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('suspembarcacion_tit.gif');
			
			$this->errors .= 'Ud no tiene los permisos suficientes para el manejo de este m�dulo';
			$this->muestraMensajeInfo($this->errors);
			
			$objIntranet->Footer();

			return $this = false;
		}

		
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][14];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];

		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							'FRM_BUSCA_VISITA' => 'frmSearchVisit',
							'BUSCA_VISITA' => 'searchVisit',
							'FRM_AGREGA_VISITA' => 'frmAddVisit',
							'AGREGA_VISITA' => 'addVisit',
							'REGISTRA_ENTRADA' => 'registraEntrada',
							'REGISTRA_SALIDA' => 'registraSalida',
							'REGISTRA_ENTRADA_CONVENIO_SITRADOC' => 'registraEntradaProduce',
							'REGISTRA_SALIDA_CONVENIO_SITRADOC' => 'registraSalidaProduce',
							'NUEVO_VISITA' => 'addNewVisit',
							'IMPRIME_VISITA' => 'printVisit',
							'CREA_CSV_VISITA' => 'CreaCsvVisit',
							'CANCELA_VISITA' => 'CancelVisit',
							);
		$this->datosUsuarioMSSQL();
	}
	

	function MuestraIndex(){
		$this->FormBuscaVisita();
	}
	
	function FormBuscaVisita($tipo){
		$tipo=($_POST['tipo']) ? $_POST['tipo'] : $_GET['tipo'];
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio
			,$horFin,$ora_ini,$ora_fin,$min_ini,$min_fin,$tipo2,$tipo3;
		
		if(!$campo_fecha){$campo_fecha=date('d/m/Y');}
		if(!$campo_fecha2){$campo_fecha2=date('d/m/Y');}
		
		$horInicio = ($horInicio!=':'&&$horInicio) ? $horInicio : "00:00";
		$horFin = ($horFin!=':'&&$horFin) ? $horFin : "23:59";
		
		// Crea el Objeto HTML
		$html = & new Smarty;
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		/*$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);*/
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
				
		$html->assign_by_ref('tipo',$tipo);
		$llaveInicial="{";
		$llaveFinal="}";
		$html->assign_by_ref('llaveInicial',$llaveInicial);
		$html->assign_by_ref('llaveFinal',$llaveFinal);
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		
		//Hora de Inicio
		$html->assign('selHoraIni',$this->ObjFrmHora(0, 23, true, substr($horInicio,0,2), true,array('val'=>'none','label'=>'----')));
		$html->assign('selMinIni',$this->ObjFrmMinuto(15, true, substr($horInicio,3,2), true));		
		
		//Hora de Fin
		$html->assign('selHoraFin',$this->ObjFrmHora(0, 23, true, substr($horFin,0,2), true,array('val'=>'none','label'=>'----')));
		$html->assign('selMinFin',$this->ObjFrmMinuto(15, true, substr($horFin,3,2), true));


		if($this->userIntranet['DIRE']!=1 && $this->userIntranet['DIRE']!=6 && $this->userIntranet['DIRE']!=5  && $this->userIntranet['DIRE']!=7 && $this->userIntranet['CODIGO']!=239 && $this->userIntranet['CODIGO']!=1366 && $this->userIntranet['CODIGO']!=2001 && $this->userIntranet['CODIGO']!=185 && $this->userIntranet['CODIGO']!=2294 && $this->userIntranet['CODIGO']!=916 && $this->userIntranet['CODIGO']!=2308 && $this->userIntranet['CODIGO']!=334 && $this->userIntranet['CODIGO']!=869 && $this->userIntranet['CODIGO']!=2135){
			$soloNombre=1;
			$html->assign_by_ref('soloNombre',$soloNombre);
		}
		
	
		// Contenido Select Dependencia
		if($this->userIntranet['CODIGO']==239 || $this->userIntranet['CODIGO']==1366 || $this->userIntranet['CODIGO']==2001 || $this->userIntranet['CODIGO']==185 || $this->userIntranet['CODIGO']==2294){
				$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "AND CODIGO_DEPENDENCIA not in (49,63,66,68, 90,104,106,107,108,109,110,111,112,113,114,136,64,134,77,138,132,116,135,127,133,80,140,139,117,118,141,142,143,144,145,146,147,148,149,150,131,152, 159, 158,161,162,164,165,44,72,65,47,115,62,84) ".
				  " ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Todas')));
		}else{
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "AND CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP']." ".
				  " ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true));
		$tipo2=$this->userIntranet['COD_DEP'];
		}
		unset($sql_st);
		$html->assign_by_ref('tipo2',$tipo2);
		
		// Contenido Select Usuario
		if($tipo2>0 && $this->userIntranet['CODIGO']==239 || $this->userIntranet['CODIGO']==1366 || $this->userIntranet['CODIGO']==2001 || $this->userIntranet['CODIGO']==185 || $this->userIntranet['CODIGO']==2294){
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,1297) ".
						  //"AND CODIGO_TRABAJADOR=%d".
						  //"ORDER BY 2",$tipo2);
						  "ORDER BY 2",$tipo2);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		
		}elseif($this->userIntranet['DIRE']==1||$this->userIntranet['DIRE']==6 || $this->userIntranet['CODIGO']==2308 || $this->userIntranet['CODIGO']==916 || $this->userIntranet['CODIGO']==334 || $this->userIntranet['CODIGO']==2135 || $this->userIntranet['CODIGO']==869){
		
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,898,897,1129)".
						  //"AND CODIGO_TRABAJADOR=%d".
						  //"ORDER BY 2",$tipo2);
						  "ORDER BY 2",$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		}elseif($this->userIntranet['DIRE']==5||$this->userIntranet['DIRE']==7 || $this->userIntranet['CODIGO']==2308 || $this->userIntranet['CODIGO']==916 || $this->userIntranet['CODIGO']==334 || $this->userIntranet['CODIGO']==2135 || $this->userIntranet['CODIGO']==869){
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,898,897,1129,1297) and estado='ACTIVO' and idsubdependencia=".$this->userIntranet['SUB_DEP']
						  //"AND CODIGO_TRABAJADOR=%d".
						  ." ORDER BY 2",$tipo2);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		
		}elseif($this->userIntranet['CODIGO']==239 && $this->userIntranet['CODIGO']==1366 && $this->userIntranet['CODIGO']==2001 && $this->userIntranet['CODIGO']==185 && $this->userIntranet['CODIGO']==2294){
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,898,897,1129,1297) and estado='ACTIVO' /*and publicado_intranet='1' and condicion='CONTRATADO' */".
						  //"AND CODIGO_TRABAJADOR=%d".
						  "ORDER BY 2",$tipo2);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		}else{
			if($this->userIntranet['CODIGO']==239 || $this->userIntranet['CODIGO']==1366 || $this->userIntranet['CODIGO']==2001 || $this->userIntranet['CODIGO']==185 || $this->userIntranet['CODIGO']==2294){
			$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
							  "FROM db_general.dbo.h_trabajador ".
							  "WHERE estado='ACTIVO' and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,898,897,1129,1297)".
							  "ORDER BY 2",$this->userIntranet['COD_DEP'],$this->userIntranet['CODIGO']);
			}
			else {
				$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
								  "FROM db_general.dbo.h_trabajador ".
								  "WHERE coddep=%d and estado='ACTIVO' and PUBLICADO_CONTACTO<>0 AND CODIGO_TRABAJADOR NOT IN (1092,265,405,581,1979,407,408,406,583,409,410,269,271,268,1382,254,629,257,276,862,252,255,261,264,582,253,260,256,275,1088,1240,262,251,402,1545,2085,1544,1121,716,717,718,898,897,1129,1297)".
								  "AND CODIGO_TRABAJADOR=%d".
								  " ORDER BY 2",$this->userIntranet['COD_DEP'],$this->userIntranet['CODIGO']);			
			}
						  
		//$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'No especificado')));
		if ($this->userIntranet['CODIGO']==239 || $this->userIntranet['CODIGO']==1366 || $this->userIntranet['CODIGO']==2001 || $this->userIntranet['CODIGO']==185 || $this->userIntranet['CODIGO']==2294 ) {
			$tipo3="";
		}else{
			$tipo3=$this->userIntranet['CODIGO'];
		}
		
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'Todas')));
		$tipo3=$this->userIntranet['CODIGO'];
		}

		$html->assign_by_ref('tipo3',$tipo3);
		
		$sql_SP="SELECT COUNT(*) FROM DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION 
			/*WHERE convert(datetime,FECHA,103)>=CONVERT(datetime,'01/01/2011',103)*/";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$totalVisitas=$rs->fields[0];
				$rs->Close();
			}
			//echo $sql_SP;
			$html->assign_by_ref('totalVisitas',$totalVisitas);
			unset($rs);

		$sql_SP="SELECT COUNT(*) FROM DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION where YEAR(CONVERT(DATETIME, CONVERT(VARCHAR(10),FECHA,103),103))=YEAR(GETDATE()) AND FLAG='X'";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$totalAnno=$rs->fields[0];
				$rs->Close();
			}
			//echo $sql_SP;
			$html->assign_by_ref('totalAnno',$totalAnno);
			unset($rs);	
			
		$sql_SP="SELECT COUNT(*) FROM DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION where   YEAR(CONVERT(DATETIME, CONVERT(VARCHAR(10),FECHA,103),103))=YEAR(GETDATE())
AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(10),FECHA,103),103))=MONTH(GETDATE()) AND FLAG='X'";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$totalMes=$rs->fields[0];
				$rs->Close();
			}
			//echo $sql_SP;
			$html->assign_by_ref('totalMes',$totalMes);
			unset($rs);				
		
		$sql_SP="SELECT COUNT(*) FROM DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION where FECHA=CONVERT(VARCHAR(10),GETDATE(),103) and FLAG='X'";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$totalDia=$rs->fields[0];
				$rs->Close();
			}
			//echo $sql_SP;
			$html->assign_by_ref('totalDia',$totalDia);
			unset($rs);			
		
		

		
		/*
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  //"AND CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP']." ".
				  " ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true, array('val'=>'none','label'=>'Todas')));
		}else{
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "AND CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP']." ".
				  " ORDER BY 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true));
		}
		*/
		
		
		unset($sql_st);
		
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('oti/visitas/frmSearch.tpl.php');		
	}
	
	function BuscaVisita($tipo,$mensaje){
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin,$ora_ini,$ora_fin,$min_ini,$min_fin,$tipo2,$tipo3;
		global $print;
		global $codfunci,$coddepen,$codvisita;
		
		$print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
		$tipo3=($_POST['tipo3']) ? $_POST['tipo3'] : $_GET['tipo3'];
		
		if($mensaje==1){
			echo '<b><font color="\CC0000\" size="\-1	\">Ingreso registrado  al CONVENIO_SITRADOC.</b></font>';
		}
		
		if($mensaje==2){
			echo '<b><font color="\CC0000\" size="\-1	\">Ingreso registrado  a la  Dependencia.</b></font>';
		}
		
		if($mensaje==3){
			echo '<b><font color="\CC0000\" size="\-1	\">Salida registrada de la Dependencia.</b></font>';
		}
		
		if($mensaje==4){
			echo '<b><font color="\CC0000\" size="\-1	\">Salida registrada del CONVENIO_SITRADOC.</b></font>';
		}
		if($mensaje==5){
			echo '<b><font color="\CC0000\" size="\-1	\">Visita Cancelada del CONVENIO_SITRADOC.</b></font>';
		}		
		if($mensaje==6){
			echo '<b><font color="\CC0000\" size="\-1	\">Visita registrada satisfactoriamente.</b></font>';
		}		

		
		if ($campo_fecha&&$campo_fecha2&& $campo_fecha!=""&& $campo_fecha2!=""){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los a�os no es correcta.</b></font>';}
		elseif($regs[3]==$regs2[3]){
			if ($regs[2]>$regs2[2]){
					echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los meses no es correcto.</b></font>';
				}else{
					if($regs[2]==$regs2[2]){
						if ($regs[1]>$regs2[1]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los d�as no es correcta.</b></font>';}
					}
				}
			}		
		}

		$html = & new Smarty;
		
		$horInicio=$ora_ini.":".$min_ini;
		$horFin=$ora_fin.":".$min_fin;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		//,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		
		//$html->assign_by_ref('codfunci',$codfunci);
		//$html->assign_by_ref('coddepen',$coddepen);
		
		$html->assign_by_ref('codvisita',$codvisita);
		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
		
				
		if($print!=1)
		$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		if(!empty($horInicio)&&!is_null($horInicio)&&$horInicio!='' && $campo_fecha!="" ){
			//$condConsulta[] = "vr.audit_mod>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			if($tipo=="P"){
				//$condConsulta[] = "CONVERT(VARCHAR,vr.FECHA,103)+vr.hora_programada>=convert(VARCHAR,'{$campo_fecha}',103)+'$horInicio'";
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada>=convert(DATETIME,'{$campo_fecha}',103)+'$horInicio'";
			}else{
				//$condConsulta[] = "CONVERT(VARCHAR,vr.FECHA,103)+vr.hora_ingreso>=convert(VARCHAR,'{$campo_fecha}',103)+'$horInicio'";
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso>=convert(DATETIME,'{$campo_fecha}',103)+'$horInicio'";
			}
		}
		if(!empty($horFin)&&!is_null($horFin)&&$horFin!='' && $campo_fecha2!=""){
			//$condConsulta[] = "vr.audit_mod<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			if($tipo=="P"){
				//$condConsulta[] = "CONVERT(VARCHAR,vr.FECHA,103)+vr.hora_programada<=convert(VARCHAR,'{$campo_fecha2}',103)+'$horFin'";
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada<=convert(DATETIME,'{$campo_fecha2}',103)+'$horFin'";
			}else{
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso<=convert(DATETIME,'{$campo_fecha2}',103)+'$horFin'";
				//$condConsulta[] = "CONVERT(VARCHAR,vr.FECHA,103)+vr.hora_ingreso<=convert(VARCHAR,'{$campo_fecha2}',103)+'$horFin'";
			}
		}
		
		/*
		if($nombreVisitante!=""){
			$condConsulta[] = "(v.nombres like '%{$nombreVisitante}%' or v.apellidos like '%{$nombreVisitante}%')";
		}
		*/
		if($dniVisitante!=''){
			$condConsulta[] = "v.numero_documento like '%{$dniVisitante}%'";
		}
		
		/*if($funcionario!=''){
			$condConsulta[] = "(ht.nombres_trabajador like '%{$funcionario}%' or ht.apellidos_trabajador like '%{$funcionario}%')";
		}*/
		
		/*
		if($tipo2>0){ 
				$condConsulta[] = "(ht.CODDEP={$tipo2})"; 
		}
		if($tipo3>0){ 
			if( $this->userIntranet['CODIGO']!=239){
				
			}else{
				$condConsulta[] = "(ht.codigo_trabajador={$tipo3})"; 
			}
		}		
		*/
		
		if( $this->userIntranet['CODIGO']==239){
			//if($tipo2>0){ $condConsulta[] = "(ht.CODDEP={". $this->userIntranet['CODIGO'] ."})"; }
			//if($tipo3>0){ $condConsulta[] = "(ht.codigo_trabajador={". $this->userIntranet['COD_DEP'] ."})"; }		
		}else{
			if($tipo2>0){ $condConsulta[] = "(ht.CODDEP={$tipo2})"; }
			if($tipo3>0){ $condConsulta[] = "(ht.codigo_trabajador={$tipo3})"; }		
		}

		
		//if($this->userIntranet['COD_DEP']!=1 && $this->userIntranet['COD_DEP']!=2 && $this->userIntranet['COD_DEP']!=13){
		if($this->userIntranet['CODIGO']!=239 && $this->userIntranet['CODIGO']!=1366 && $this->userIntranet['CODIGO']!=2001 && $this->userIntranet['CODIGO']!=185 && $this->userIntranet['CODIGO']!=2294){
			//AQU� HICE EL CAMBIO EL 06/04/2011
			if($this->userIntranet['DIRE']==1||$this->userIntranet['DIRE']==6 ||$this->userIntranet['DIRE']==5 ||$this->userIntranet['DIRE']==7 || $this->userIntranet['CODIGO']==2308 || $this->userIntranet['CODIGO']==916 || $this->userIntranet['CODIGO']==334 || $this->userIntranet['CODIGO']==2135 || $this->userIntranet['CODIGO']==869){
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO IN (SELECT CODIGO_TRABAJADOR FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODDEP=".$this->userIntranet['COD_DEP'].")";
			}else{
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO=".$this->userIntranet['CODIGO']." ";
			}
		}
		
		if($tipo2>0 && ($this->userIntranet['CODIGO']==239 && $this->userIntranet['CODIGO']==1366 && $this->userIntranet['CODIGO']==2001 && $this->userIntranet['CODIGO']==185  && $this->userIntranet['CODIGO']==2294)){
			$condConsulta[] = " VR.CODIGO_FUNCIONARIO IN (SELECT CODIGO_TRABAJADOR FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODDEP=".$tipo2.")";
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		
		if ($tipo2=="none") {
		}else{
			$condConsulta[] = " ht.coddep=". $tipo2 ." ";
		}
		
		if (($tipo3=="239" || $tipo3=="2001" || $tipo3=="1366" || $tipo3=="2001" || $tipo3=="2294") || $tipo3=="none") {
		}else{
			$condConsulta[] = " vr.CODIGO_FUNCIONARIO=". $tipo3 ." ";
		}		
		
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';
		// Obtiene los datos de Cada Modulo
		//$sql_SP =  sprintf("EXEC sp_busVisita '%s'",$this->PrepareParamSQL($tipo));
		//$sql_SP =  sprintf("EXEC sp_busVisitaFechas '%s','%s','%s'",$this->PrepareParamSQL($tipo),$campo_fecha,$campo_fecha2);
		
		//echo "x1: ". $tipo ."<br>";
		//echo "x2: ". $where ."<br>";
		
		/*,'%s','%s'
		,
		$this->PrepareParamSQL($nombreVisitante),
		$this->PrepareParamSQL($nombreVisitante)	*/	
		$sql_SP =  sprintf("EXEC webminpro.[sp_busVisitaFechas_31052011] '%s','%s','%s','%s'",
		$this->PrepareParamSQL($tipo),
		$this->PrepareParamSQL($where),
		$this->PrepareParamSQL($nombreVisitante),
		$this->PrepareParamSQL($nombreVisitante)
		);
		//echo "SQL: ". $sql_SP ."<br>";
		//echo "xx: ". "EXEC sp_busVisitaFechas '%s','%s'",$this->PrepareParamSQL($tipo),$this->PrepareParamSQL($where);
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()) {
				$html->append('visi',array('nomb' => ucwords($row[0]),
										   'docu' => $row[1],
										   'ndoc' => $row[2],
										   'asun' => ucwords($row[3]),
										   'func' => ucwords($row[4]),
										   'depe' => ucwords($row[5]),
										   'fecE' => $row[6],
										   'horaE' => $row[7],
										   'fecS' => $row[8],
										   'horaS' => $row[9],
										   'id' => $row[10],
										   'horaIngreso' => $row[11],
										   'horaSalida' => $row[12],
										   'horaProg' => $row[13],
										   'siglas' => $row[14],
										   'tipo3' => $row[15],
										   'tipo2' => $row[16],
										   'codvisita'=> $row[17]
										   ));
							//$totalVisitas= $row[15];
							//echo "xxx".$total-visitas."yyy";
			}
			$rs->Close();
		}
		unset($rs);				
		
		
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		
		$html->assign_by_ref('tipo',$tipo);
		$html->assign_by_ref('totalVisitas',$totalVisitas);
		
		$html->assign_by_ref('tipo2',$tipo2);
		$html->assign_by_ref('tipo3',$tipo3);
		
		
		$html->display('oti/visitas/searchResult.tpl.php');
	}
	
	function FormAgregaVisitante($tipo,$dniVisitante,$nombresVisitante,$apellidosVisitante,$tipo2,$tipo3,$tipo4,$obs, $cmbProcedencia){
		
		global $bBusca,$bAgregaPersona,$horInicio,$ora_ini,$min_ini,$campo_fecha,$campo_fechafin, $hora_sist_apa;
		
		$ora_ini=($_POST['ora_ini']) ?  $_POST['ora_ini'] : $_GET['ora_ini'];
		$min_ini=($_POST['min_ini']) ?  $_POST['min_ini'] : $_GET['min_ini'];
		
		
		$xchk_varios=$_REQUEST['chk_varios'];
		$campo_fechafin=$_REQUEST['campo_fechafin'];

		if($xchk_varios==""){ $xchk_varios=0;}
//		echo '-------'.$_REQUEST['chk_varios'];
		
		if($ora_ini!="" && $min_ini!=""){
			$horInicio=$ora_ini.":".$min_ini;
		}
		
		/*
		$hora_local  = mktime(date("H")); 
		$hora=getdate($hora_local); 
		print $hora[hours].":".$hora[minutes];
		*/
		$hora_sist_apa=date('H');
		$min_sist_apa=date('i');		
		$tamd=strlen('00'.$hora_sist_apa);
		$tame=strlen('00'.$min_sist_apa);		
		$cadena_hora_sis=substr('00'.$hora_sist_apa, $tamd-2,2);
		$cadena_min_sis=substr('00'.$min_sist_apa, $tame-2,2);		
//		echo $cadena_hora_sis.'<br>';
		$horInicio = ($horInicio!=':'&&$horInicio) ? $horInicio : $cadena_hora_sis.":".$cadena_min_sis."";
		
		// Crea el Objeto HTML
		$html = & new Smarty;
		
		$frmName = 'frmAddVisit';
		
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('chk_varios',$xchk_varios);
		$html->assign_by_ref('campo_fechafin',$campo_fechafin);		
		
		//$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		
		$this->abreConnDB();
		//$this->connDebug=true;
		
		if($bBusca && $dniVisitante!=""){
			$this->abreConnDB();
			//$this->connDebug=true;
			
/*
			$sql_SP="SELECT APELLIDO_PATERNO,APELLIDO_MATERNO,NOMBRES,DNI
					 FROM DB_RENIEC.DBO.PERSONA 
					 WHERE DNI='$dniVisitante'";
					 
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$apellidosVisitante=$rs->fields[0]." ".$rs->fields[1];
				$nombresVisitante=$rs->fields[2];
				$rs->Close();
			}
			unset($rs);
*/					 
			
			$sql_SP="SELECT APELLIDOS, NOMBRES, NUMERO_DOCUMENTO
							FROM DB_VISITA_RECEPCION.dbo.visitante
							WHERE NUMERO_DOCUMENTO='$dniVisitante'";
					 					 
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$apellidosVisitante=$rs->fields[0];
				$nombresVisitante=$rs->fields[1];
				$rs->Close();
			}
			unset($rs);
			
			
			//if($apellidosVisitante==" " ||! $apellidosVisitante){
			if($apellidosVisitante==" " || !$apellidosVisitante){
				//echo "Se debe habilitar el bot�n click aqu�";
				//Se debe habilitar el bot�n click aqu�
				$permiteRegistroPersona2=1;
				$html->assign_by_ref('permiteRegistroPersona2',$permiteRegistroPersona2);
			}
		}
		
		if($bAgregaPersona){
			//Se debe permitir registrar la persona
			$permiteRegistroPersona=1;
			$html->assign_by_ref('permiteRegistroPersona',$permiteRegistroPersona);
		}

		$html->assign_by_ref('apellidosVisitante',$apellidosVisitante);
		$html->assign_by_ref('nombresVisitante',$nombresVisitante);
		$html->assign_by_ref('obs',$obs);
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);
		$html->assign_by_ref('cmbProcedencia',$cmbProcedencia);
		
		
		// Contenido Select Tipo Documento		
		if ($cmbProcedencia=="N") {
			$sql_st = "SELECT codigo_t_identificacion,descripcion ".
					  " FROM DB_GENERAL.dbo.T_TIPO_IDENTIFICACION ".
					  " WHERE CODIGO_T_IDENTIFICACION IN (1) ".
					  " ORDER BY 1";			
		}
		if ($cmbProcedencia=="E") {
			$sql_st = "SELECT codigo_t_identificacion,descripcion ".
					  " FROM DB_GENERAL.dbo.T_TIPO_IDENTIFICACION ".
					  " WHERE CODIGO_T_IDENTIFICACION IN (3,9) ".
					  " ORDER BY 1";
		}
		//echo "x: ". $tipo ."<br>";
		$html->assign_by_ref('selTipoDoc',$this->ObjFrmSelect($sql_st, $tipo, true, true));
		unset($sql_st);
		//echo "ss".$cmbProcedencia."ee";
		/*
		$sql_st = "SELECT codigo_t_identificacion,descripcion ".
				 				  "FROM DB_GENERAL.dbo.T_TIPO_IDENTIFICACION ".
								  "WHERE CODIGO_T_IDENTIFICACION IN (1,3,9) ".
				  				"ORDER BY 1";
		$html->assign_by_ref('selTipoDoc',$this->ObjFrmSelect($sql_st, $tipo, true, true));
		unset($sql_st);
		*/
		
		// Contenido Select Dependencia
		$sql_st = "SELECT codigo_dependencia, Lower(dependencia) ".
				  "FROM db_general.dbo.h_dependencia ".
				  "WHERE condicion='ACTIVO' ".
				  "AND CODIGO_DEPENDENCIA=".$this->userIntranet['COD_DEP']." ".
				  " ORDER BY 2";
		//$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $desDependencia, true, true, array('val'=>'none','label'=>'No especificada')));
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $tipo2, true, true));
		unset($sql_st);
		
		// Contenido Select Usuario
		$sql_st = sprintf("SELECT codigo_trabajador, Lower(apellidos_trabajador+' '+nombres_trabajador) ".
						  "FROM db_general.dbo.h_trabajador ".
						  "WHERE coddep=%d and estado='ACTIVO' /*and publicado_intranet='1'*/ and condicion<>'GENERAL' ".
						  "ORDER BY 2",$this->userIntranet['COD_DEP']);
		$html->assign_by_ref('selTrabajador',$this->ObjFrmSelect($sql_st, $tipo3, true, true, array('val'=>'none','label'=>'No especificado')));
		unset($sql_st);	
		
		// Contenido Select Tipo Documento
		$sql_st = "SELECT id,descripcion ".
				 				  "FROM dbo.asunto_recepcion ".
				  				"ORDER BY 1";
		$html->assign_by_ref('selMotivo',$this->ObjFrmSelect($sql_st, $tipo4, true, true, array('val'=>'none','label'=>'Seleccione Uno')));
		//echo $sql_st;
		unset($sql_st);
		
		
		
		$llaveInicial="{";
		$llaveFinal="}";
		$html->assign_by_ref('llaveInicial',$llaveInicial);
		$html->assign_by_ref('llaveFinal',$llaveFinal);
		
		//Hora de Inicio
		$html->assign('selHoraIni',$this->ObjFrmHora(7, 23, true, substr($horInicio,0,2), true,array('val'=>'none','label'=>'----')));
		$html->assign('selMinIni',$this->ObjFrmMinuto(1, true, substr($horInicio,3,2), true));	
		//echo "z".$ora_ini."z";
	
		$html->assign_by_ref('ora_ini',$ora_ini);
		$html->assign_by_ref('min_ini',$min_ini);	
		
		$this->abreConnDB();
			$this->connDebug=true;
		
		if($dniVisitante!=""){
			
			$sql="SELECT CODIGO FROM DB_VISITA_RECEPCION.dbo.visitante WHERE NUMERO_DOCUMENTO='$dniVisitante'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs)
				$RETVAL=1;
			else{
				$codigoVistanteX=$rs->fields[0];
			}
		}
		
		if($codigoVistanteX>0){
			$sql_SP2="select count(*) from DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION 
										where HORA_PROGRAMADA='".$horInicio."' AND FECHA='".$campo_fecha."' AND CODIGO_visitante=".$codigoVistanteX;
			$rs2 = & $this->conn->Execute($sql_SP2);
				unset($sql_SP2);
				if (!$rs2)
					$RETVAL=1;
				else{
					if($row2 = $rs2->FetchRow()){
						//$RETVAL = $row2[0];
						$contadorVisitante = $row2[0];
					}else
						$RETVAL = 1;
					$rs2->Close();
				}
				unset($rs2);
			
			if($contadorVisitante==1){
				$alertaX=1;
				$html->assign_by_ref('alertaX',$alertaX);
			}
		}
		
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->display('oti/visitas/RegisterVisited.tpl.php');		
	}
	
function compara_fechas($fecha1,$fecha2)
            
 
{
            
 
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("/",$fecha1);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("-",$fecha1);
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("/",$fecha2);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("-",$fecha2);
        $dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
        return ($dif);                         
            
 
}	

	function AgregaVisitante($tipo,$dniVisitante,$nombresVisitante,$apellidosVisitante,$tipo2,$tipo3,$tipo4,$obs){
		$this->abreConnDB();
		//$this->conn->debug = true;	

		global $horInicio,$ora_ini,$min_ini,$campo_fecha,$campo_fecha2,$chk_varios;
		$horInicio=$ora_ini.":".$min_ini;
		
		// Comprueba Valores	
		if(!$tipo){ $tipo = true; $this->errors .= 'El Tipo de documento debe ser especificado<br>'; }
		//if($dniVisitante&&!is_numeric($dniVisitante)){ $dniVisitante = true; $this->errors .= 'El N�mero de Documento debe ser un valor num�rico<br>'; }
		if($dniVisitante==""){ ; $this->errors .= 'El N�mero de Documento debe ser un valor num�rico<br>'; }
		if(!$nombresVisitante){ $nombresVisitante = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidosVisitante){ $apellidosVisitante = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$tipo2){ $tipo2 = true; $this->errors .= 'La Oficina debe ser especificada<br>'; }
		if(!$tipo3){ $tipo3 = true; $this->errors .= 'El funcionario a visistar debe ser especificado<br>'; }
		if(!$tipo4){ $tipo4 = true; $this->errors .= 'El Motivo de la visita debe ser especificado<br>'; }

		if($mensaje==6){
			echo '<b><font color="\CC0000\" size="\-1	\">Visita registrada satisfactoriamente.</b></font>';
		}		
		
		$fechaHoy=date("d/m/Y");
		$diferencia=$this->compara_fechas($fechaHoy,$campo_fecha);
		
		//echo "<br/>x".$fechaHoy."x".$campo_fecha."x";  
		//echo "<br/>x".$diferencia."x";
		if($diferencia>0){   
			echo '<b><font color="\CC0000\" size="\-1	\">No puede ingresar una fecha anterior al d�a de hoy</b></font>';
			$this->FormAgregaVisitante($tipo,$dniVisitante,$nombresVisitante,$apellidosVisitante,$tipo2,$tipo3,$tipo4,$obs);
			exit;
					}  

		
		
		$this->abreConnDB();
		//$this->conn->debug = true;	
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE WEBMINPRO.CONSULTA_REGISTRA_ID_VISITANTE '%s','%s','%s',%d,'%s'",
							  $this->PrepareParamSQL($nombresVisitante),
							  $this->PrepareParamSQL($apellidosVisitante),
							  $this->PrepareParamSQL($dniVisitante),
							  $this->PrepareParamSQL($tipo),
							  $_SESSION['cod_usuario']);
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
					$codigoVistante = $row[1];
					
					
					$sql_SP2="select count(*) from DB_VISITA_RECEPCION.dbo.VISITA_RECEPCION 
									where HORA_PROGRAMADA='".$horInicio."' AND FECHA='".$campo_fecha."' AND CODIGO_visitante=".$codigoVistante;
						$rs2 = & $this->conn->Execute($sql_SP2);
							unset($sql_SP2);
							if (!$rs2)
								$RETVAL=1;
							else{
								if($row2 = $rs2->FetchRow()){
									//$RETVAL = $row2[0];
									$contadorVisitante = $row2[0];
								}else
									$RETVAL = 1;
								$rs2->Close();
							}
							unset($rs2);
						
						if($contadorVisitante==1){
							$RETVAL = 1;
							$this->conn->RollbackTrans();
							echo '<b><font color="\CC0000\" size="\-1	\">El visitante ya se encuentra registrado en esta fecha y hora</b></font>';
							$this->FormAgregaVisitante($tipo,$dniVisitante,$nombresVisitante,$apellidosVisitante,$tipo2,$tipo3,$tipo4,$obs);
							exit;
						}
		
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
			if(!$RETVAL){
			
			$campo_fechafin=$_REQUEST['campo_fechafin'];
			$chkVarios=$_REQUEST['chk_varios'];		
				
						$sql_SP = sprintf("EXECUTE WEBMINPRO.REGISTRA_VISITA_31052011 %d,%d,%d,%d,'%s','%s','%s','%s','%s','%s'",
										  $codigoVistante,
										  $this->PrepareParamSQL($tipo),
										  $this->PrepareParamSQL($tipo3),
										  $this->PrepareParamSQL($tipo4),
										  $_SESSION['cod_usuario'],
										  $this->PrepareParamSQL($obs),
										  $campo_fecha,
										  $horInicio,
										   $campo_fechafin,
										   $chkVarios
										  );
						//************
						//echo $sql_SP;exit;
						
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){ 	
								$RETVAL=1; 	
								break;
							}else{
							if($softData = $rsSoft->FetchRow()){ 
								$RETVAL = $softData[0];
								//$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();
		//exit;
		//$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		//$html->assign_by_ref('accion',$this->arr_accion);
		
		//echo "1";
		//$html->display('oti/visitas/RegisterVisited.tpl.php');
		//$this->BuscaVisita($tipo);exit;
		$this->FormAgregaVisitante();
		//exit;
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de error
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de �xito
		}
	}

	function AgregaNuevoVisitante($desNombres,$desApellidos,$idTipo,$nroDocumento,$mensaje){
		/*
		echo "Nombre: ". $desNombres;
		echo "Apellidos: ". $desApellidos;
		echo "Tipo: ". $idTipo;
		echo "Nrodoc: ". $nroDocumento;
		return false;
		*/
		//echo md5("10165999709412356");
		
		// Comprueba Valores	
		if(!$desNombres){ $desNombres = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$desApellidos){ $desApellidos = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$idTipo){ $idTipo = true; $this->errors .= 'El Tipo de documento debe ser especificado<br>'; }
		if($nroDocumento&&!is_numeric($nroDocumento)){ $nroDocumento = true; $this->errors .= 'El N�mero de Documento debe ser un valor num�rico<br>'; }				
		
		
		$this->abreConnDB();
			// $this->conn->debug = true;	
			$sql_SP = sprintf("EXECUTE sp_addNuevoVisitante '%s','%s',%d,%d'",
							  $this->PrepareParamSQL($desNombres),
							  $this->PrepareParamSQL($desApellidos),
							  $this->PrepareParamSQL($idTipo),
							  $this->PrepareParamSQL($nroDocumento),
							  $_SESSION['cod_usuario']);
			//************
			//echo $sql_SP;return false;
			
			
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$mensaje=6;
		$this->FormAgregaVisitante();
		exit;
		//$html->display('oti/visitas/RegisterNewVisited.tpl.php');		
	}
	
	function RegistraEntrada($id){				
		
		global $campo_fecha,$campo_fecha2;
		$campo_fecha = ($_POST['campo_fecha']) ? $_POST['campo_fecha'] : $_GET['campo_fecha'];

		$this->abreConnDB();
		//$this->conn->debug = true;	
			
			$sql_SP = sprintf("EXECUTE WEBMINPRO.REGISTRA_ENTRADA_VISITANTE %d",
							  $this->PrepareParamSQL($id));
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
							
		//exit;
		$mensaje=2;
		$this->BuscaVisita($tipo,$mensaje);exit;
		
		/*$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//$html->display('oti/visitas/RegisterVisited.tpl.php');
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de error
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de �xito
		}*/
	}	

	function RegistraEntradaProduce($id){				
		global $campo_fecha,$campo_fecha2;
		$campo_fecha = ($_POST['campo_fecha']) ? $_POST['campo_fecha'] : $_GET['campo_fecha'];
		$this->abreConnDB();
		//$this->conn->debug = true;	
			
			$sql_SP = sprintf("EXECUTE WEBMINPRO.REGISTRA_ENTRADA_CONVENIO_SITRADOC %d",
							  $this->PrepareParamSQL($id));
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
							
		//exit;
		$mensaje=1;
		
		//$campo_fecha2=
		$this->BuscaVisita($tipo,$mensaje);exit;
		
		/*$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//$html->display('oti/visitas/RegisterVisited.tpl.php');
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de error
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de �xito
		}*/
	}	

	
	function RegistraSalida($id){
		global $campo_fecha,$campo_fecha2;
		$campo_fecha = ($_POST['campo_fecha']) ? $_POST['campo_fecha'] : $_GET['campo_fecha'];
				
		
		$this->abreConnDB();
		//$this->conn->debug = true;	
			
			$sql_SP = sprintf("EXECUTE WEBMINPRO.REGISTRA_SALIDA_VISITANTE %d",
							  $this->PrepareParamSQL($id));
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
							
		//exit;
		$mensaje=3;
		$this->BuscaVisita($tipo,$mensaje);exit;
		
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//$html->display('oti/visitas/RegisterVisited.tpl.php');
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de error
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de �xito
		}
	}	

	function RegistraSalidaProduce($id){				
		global $campo_fecha,$campo_fecha2;
		$campo_fecha = ($_POST['campo_fecha']) ? $_POST['campo_fecha'] : $_GET['campo_fecha'];
		
		$this->abreConnDB();
		//$this->conn->debug = true;	
			
			$sql_SP = sprintf("EXECUTE WEBMINPRO.REGISTRA_SALIDA_CONVENIO_SITRADOC %d",
							  $this->PrepareParamSQL($id));
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
							
		//exit;
		$mensaje=4;
		$this->BuscaVisita($tipo,$mensaje);exit;
		
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		//$html->display('oti/visitas/RegisterVisited.tpl.php');
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de error
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
			//plantilla de �xito
		}
	}	


//************
	function fncCancelavisitaxId($id){
		global $flag;
		//$id = ($_POST['id']) ? $_POST['id'] : $_GET['id'];		
		//echo "xxx:  ". $id ."<br>"; return false;
		
		$this->abreConnDB();
		//$this->conn->debug = true;	
			
		$sql_SP = sprintf("EXECUTE WEBMINPRO.CANCELA_VISITA_CONVENIO_SITRADOC %d", $this->PrepareParamSQL($id) );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			$RETVAL=1;
		else{
			if($row = $rs->FetchRow()){
				$RETVAL = $row[0];
			}else
				$RETVAL = 1;
			$rs->Close();
		}
		unset($rs);
			
		$mensaje=5;
		$this->BuscaVisita($tipo,$mensaje);exit;
		
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		if($RETVAL){
			$html->display('oti/visitas/frmSearch.tpl.php');
		}else{
			$html->display('oti/visitas/frmSearch.tpl.php');
		}
	}	
//************



	function ImprimeVisita($tipo,$campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin,$codfuncio,$coddepen,$codvisita, $print){
		//global $print;
		//global $tipo2,$print;
		//$print = ($_POST['print']) ? $_POST['print'] : $_GET['print'];
		
		$mode 		= ($_POST['mode']) ? $_POST['mode'] : $_GET['mode'];
		$codfuncio 	= ($_POST['tipo3']) ? $_POST['tipo3'] : $_GET['tipo3'];
		//$coddepen	= ($_POST['tipo2']) ? $_POST['tipo2'] : $_GET['tipo2'];
		$coddepen	= $_REQUEST['tipo2'];
		$xls 		= ($_POST['xls']) ? $_POST['xls'] : $_GET['xls'];
		
		
		//echo "::: ". $coddepen ." :::";return false;
		
		if ($tipo2=="") {$tipo2=$coddepen;}
		if ($tipo3=="") {$tipo3=$codfuncio;}		
		
		if ($campo_fecha&&$campo_fecha2){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo "error en los anyos";}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo "error en los meses";
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo "error en los dias";}
					}
			}
		}		

		}


		$html = & new Smarty;
		
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		//,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		
		$html->assign_by_ref('codfuncio',$codfuncio);
		$html->assign_by_ref('coddepen',$coddepen);
		$html->assign_by_ref('tipo2',$tipo2);
		$html->assign_by_ref('codvisita',$codvisita);

		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);		
		//echo "z".$horInicio."x";
		
		//$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		if(!empty($horInicio)&&!is_null($horInicio)&&$horInicio!='' && $campo_fecha!="" ){
			//$condConsulta[] = "vr.audit_mod>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			if($tipo=="P"){
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			}else{
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			}
		}
		if(!empty($horFin)&&!is_null($horFin)&&$horFin!='' && $campo_fecha2!=""){
			//$condConsulta[] = "vr.audit_mod<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			if($tipo=="P"){
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			}else{
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			}
		}
		if($nombreVisitante!=""){
			$condConsulta[] = "(v.nombres like '%{$nombreVisitante}%' or v.apellidos like '%{$nombreVisitante}%')";
		}
		if($dniVisitante!=''){
			$condConsulta[] = "v.numero_documento like '%{$dniVisitante}%'";
		}
		if($funcionario!=''){
			$condConsulta[] = "(ht.nombres_trabajador like '%{$funcionario}%' or ht.apellidos_trabajador like '%{$funcionario}%')";
		}

		//---
		if ($coddepen=="none") { $coddepen=0;}
		if ($codfuncio=="none") { $codfuncio=0;}
		//if($mode=="btn"){ 
			if($coddepen>0){ 
				$condConsulta[] = " ht.coddep={$coddepen} ";
			}
		//}
		//if($mode=="btn"){
			if($codfuncio>0 && $codfuncio!=239) { 
				$condConsulta[] = " vr.codigo_funcionario={$codfuncio}";  
			}
		//}
		
		//if($this->userIntranet['COD_DEP']!=1 && $this->userIntranet['COD_DEP']!=2 && $this->userIntranet['COD_DEP']!=13){
		if($this->userIntranet['CODIGO']!=239 && $this->userIntranet['CODIGO']!=1366 && $this->userIntranet['CODIGO']!=2001 && $this->userIntranet['CODIGO']!=185  && $this->userIntranet['CODIGO']!=2294){
			if($this->userIntranet['DIRE']==1||$this->userIntranet['DIRE']==6 ||$this->userIntranet['DIRE']==5 ||$this->userIntranet['DIRE']==7 || $this->userIntranet['CODIGO']==2308 || $this->userIntranet['CODIGO']==916 || $this->userIntranet['CODIGO']==334 || $this->userIntranet['CODIGO']==2135 || $this->userIntranet['CODIGO']==869){
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO IN (SELECT CODIGO_TRABAJADOR FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODDEP=".$this->userIntranet['COD_DEP'].")";
			}else{
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO=".$this->userIntranet['CODIGO']." ";
			}
		}

		if($tipo2>0 && ($this->userIntranet['CODIGO']==239 && $this->userIntranet['CODIGO']==1366 && $this->userIntranet['CODIGO']==2001 && $this->userIntranet['CODIGO']==2294)){
			$condConsulta[] = " VR.CODIGO_FUNCIONARIO IN (SELECT CODIGO_TRABAJADOR FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODDEP=".$tipo2.")";
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' AND '.implode(' AND ',$condConsulta) : '';

		// Obtiene los datos de Cada Modulo
		$sql_SP =  sprintf("EXEC sp_busVisitaFechas '%s','%s'",$this->PrepareParamSQL($tipo),$this->PrepareParamSQL($where));
		//echo "sql_SP: ". $sql_SP ."<br><br>";
		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow())
				$html->append('visi',array('nomb' => ucwords($row[0]),
										   'docu' => $row[1],
										   'ndoc' => $row[2],
										   'asun' => ucwords($row[3]),
										   'func' => ucwords($row[4]),
										   'depe' => ucwords($row[5]),
										   'fecE' => $row[6],
										   'horaE' => $row[7],
										   'fecS' => $row[8],
										   'horaS' => $row[9],
										   'id' => $row[10],
										   'horaIngreso' => $row[11],
										   'horaSalida' => $row[12],
										   'horaProg' => $row[13],
										   'siglas' => $row[14],
										   'codfunci' => $row[15],
										   'coddepen' => $row[16],
										   'codvisita'=> $row[17]
										   ));
			$codfunci= $row[15];
			$coddepen= $row[16];
			$codvisita= $row[17];
													   
			$rs->Close();
		}
		unset($rs);
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('tipo',$tipo);
		$html->assign_by_ref('print',$print);
		
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horFin',$horFin);
		
		$html->assign_by_ref('codfunci',$codfunci);
		$html->assign_by_ref('coddepen',$coddepen);
		$html->assign_by_ref('codvisita',$codvisita);
		
		//echo "::: ". $coddepen ." :::";return false;
		
		if ($xls==1) {$print=3;}
		if($print==3) {
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=consulta-".mktime().".csv");
			
			$html->display('oti/visitas/CreaCSVVisitantes.tpl.php');
			exit;
		}else{
			$html->display('oti/visitas/printVisit.tpl.php');
		}
	}
	
	
	function CreaCsvVisitas($id){
		global $campo_fecha,$campo_fecha2,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horInicioP,$horFin,$horFinP,$ora_ini,$ora_fin,$min_ini,$min_fin;
		global $print;
		
		$print=($_POST['print']) ? $_POST['print'] : $_GET['print'];
		if ($campo_fecha&&$campo_fecha2){

		$dia_ini=substr($campo_fecha,0,2);
		$dia_fin=substr($campo_fecha2,0,2);
		$mes_ini=substr($campo_fecha,3,2);
		$mes_fin=substr($campo_fecha2,3,2);
		$anyo_ini=substr($campo_fecha,6,4);
		$anyo_fin=substr($campo_fecha2,6,4);
		
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha, $regs);
		ereg( "([0-9]{2})/([0-9]{2})/([0-9]{4})", $campo_fecha2, $regs2);
		
		if ($regs[3]>$regs2[3]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los a�os no es correcta.</b></font>';}
		elseif($regs[3]==$regs2[3])
		{
			if ($regs[2]>$regs2[2])
				{
					echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los meses no es correcta.</b></font>';
				}
			else
			{
				if($regs[2]==$regs2[2])
					{
						if ($regs[1]>$regs2[1]){echo '<b><font color="\CC0000\" size="\-1	\">La selecci�n de los d�as no es correcta.</b></font>';}
					}
			}
		}		

		}


		$html = & new Smarty;
		
		$horInicio=$ora_ini.":".$min_ini;
		$horFin=$ora_fin.":".$min_fin;
		
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$html->assign_by_ref('campo_fecha',$campo_fecha);
		$html->assign_by_ref('campo_fecha2',$campo_fecha2);		
		//,$nombreVisitante,$dniVisitante,$funcionario,$horInicio,$horFin
		$html->assign_by_ref('nombreVisitante',$nombreVisitante);
		$html->assign_by_ref('dniVisitante',$dniVisitante);
		$html->assign_by_ref('funcionario',$funcionario);
		$html->assign_by_ref('horInicio',$horInicio);
		$html->assign_by_ref('horInicioP',$horInicioP);
		$html->assign_by_ref('horFin',$horFin);
		$html->assign_by_ref('horFinP',$horFinP);		
		$html->assign_by_ref('codigoTrabajador',$this->userIntranet['CODIGO']);
		
				
		if($print!=1)
		$this->FormBuscaVisita($tipo);
		// Crea el Objeto HTML
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		if(!empty($horInicio)&&!is_null($horInicio)&&$horInicio!=''){
			//$condConsulta[] = "vr.audit_mod>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			if($tipo=="P"){
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			}else{
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso>=convert(datetime,'{$campo_fecha}',103)+'$horInicio'";
			}
		}
		if(!empty($horFin)&&!is_null($horFin)&&$horFin!=''){
			//$condConsulta[] = "vr.audit_mod<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			if($tipo=="P"){
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_programada<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			}else{
				$condConsulta[] = "CONVERT(DATETIME,vr.FECHA,103)+vr.hora_ingreso<=convert(datetime,'{$campo_fecha2}',103)+'$horFin'";
			}
		}
		if($nombreVisitante!=""){
			$condConsulta[] = "(v.nombres like '%{$nombreVisitante}%' or v.apellidos like '%{$nombreVisitante}%')";
		}
		if($dniVisitante!=''){
			$condConsulta[] = "v.numero_documento like '%{$dniVisitante}%'";
		}
		if($funcionario!=''){
			$condConsulta[] = "(ht.nombres_trabajador like '%{$funcionario}%' or ht.apellidos_trabajador like '%{$funcionario}%')";
		}
		
		
		//if($this->userIntranet['COD_DEP']!=1 && $this->userIntranet['COD_DEP']!=2 && $this->userIntranet['COD_DEP']!=13){
		if($this->userIntranet['CODIGO']!=239 && $this->userIntranet['CODIGO']!=1366 && $this->userIntranet['CODIGO']!=2001 && $this->userIntranet['CODIGO']!=185 && $this->userIntranet['CODIGO']!=2294){
			if($this->userIntranet['DIRE']==1||$this->userIntranet['DIRE']==6 ||$this->userIntranet['DIRE']==5 ||$this->userIntranet['DIRE']==7 || $this->userIntranet['CODIGO']==2308 || $this->userIntranet['CODIGO']==916 || $this->userIntranet['CODIGO']==334 || $this->userIntranet['CODIGO']==2135 || $this->userIntranet['CODIGO']==869){
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO IN (SELECT CODIGO_TRABAJADOR FROM DB_GENERAL.dbo.H_TRABAJADOR WHERE CODDEP=".$this->userIntranet['COD_DEP'].")";
			}else{
				$condConsulta[] = " VR.CODIGO_FUNCIONARIO=".$this->userIntranet['CODIGO']." ";
			}
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		//echo $where;
		// Obtiene los datos de Cada Modulo
		//$sql_SP =  sprintf("EXEC sp_busVisita '%s'",$this->PrepareParamSQL($tipo));
		//$sql_SP =  sprintf("EXEC sp_busVisitaFechas '%s','%s','%s'",$this->PrepareParamSQL($tipo),$campo_fecha,$campo_fecha2);
		if ($tipo=="") {$tipo="Y";}
		$sql_SP =  sprintf("EXEC sp_busVisitaFechas '%s','%s'",$this->PrepareParamSQL($tipo),$this->PrepareParamSQL($where));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow())
				$html->append('visi',array('nomb' => ucwords($row[0]),
										   'docu' => $row[1],
										   'ndoc' => $row[2],
										   'asun' => ucwords($row[3]),
										   'func' => ucwords($row[4]),
										   'depe' => ucwords($row[5]),
										   'fecE' => $row[6],
										   'horaE' => $row[7],
										   'fecS' => $row[8],
										   'horaS' => $row[9],
										   'id' => $row[10],
										   'horaIngreso' => $row[11],
										   'horaSalida' => $row[12],
										   'horaProg' => $row[13],
										   'siglas' => $row[14]
										   ));
			$rs->Close();
		}
		unset($rs);
		
		//$html->assign_by_ref('frmName','frmBuscaVisita');
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$html->assign_by_ref('tipo',$tipo);
		
		$html->display('oti/visitas/searchResult.tpl.php');
	}
}
?>