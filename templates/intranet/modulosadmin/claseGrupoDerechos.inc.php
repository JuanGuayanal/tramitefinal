<?
require_once('claseModulos.inc.php');

class GrupoDerechos extends Modulos{
	
	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function GrupoDerechos(){
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	}
	
	function getIDModuloPadre($idmod){
		$this->abreConnDB();
		
		$sql_st = "SELECT id_modulopadre, UPPER(des_titulocorto) FROM modulo WHERE id_modulo=$idmod";

		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$datosMod = $rs->FetchRow();
			$rs->Close();
		}
		return $datosMod;
	}
	
	/* ------------------------------------------------------ */
	/* Funcion para Mostrar los Modulos a los cuales		  */
	/* un determinado Grupo tendra derecho (Checkboxs)		  */
	/* ------------------------------------------------------ */
	
	function muestraGrupoModulosDerechos($idgrp,$tip_modulo,$idmodpad=NULL){
		$this->abreConnDB();
		
		$sql_st = "SELECT id_modulo, des_titulocorto FROM modulo WHERE id_modulopadre".
		$sql_st .= (empty($idmodpad)) ? " IS NULL" : "=${idmodpad}";
		$sql_st .= " and id_modulotipo=${tip_modulo} and ind_activo IS true ORDER BY 2";
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow()) {
				// Consulta si el Modulo $idmodpad tiene SubModulos
				$sql_aux_st = "SELECT count(*) FROM modulo where id_modulopadre={$row[0]}";
				$rs_aux = & $this->conn->Execute($sql_aux_st);
				unset($sql_aux_st);
				if (!$rs_aux)
					print $this->conn->ErrorMsg();
				else{
					$num_submod = $rs_aux->fields[0];
					$ind_link = ($num_submod > 0) ? true : false;
					unset($num_submod);
					$rs_aux->Close(); # optional
				}
				
				// Cosulta los derechos del Grupo sobre el Modulo $idmodpad
				$sql_aux_st = "SELECT ind_leer, ind_insertar, ind_modificar FROM modulo_derecho where id_modulo={$row[0]} and id_grupo=${idgrp}";
				$rs_aux = & $this->conn->Execute($sql_aux_st);
				unset($sql_aux_st);
				if (!$rs_aux)
					print $this->conn->ErrorMsg();
				else{
					if($row_aux = $rs_aux->FetchRow()){
						if($row_aux[0] == 't')
							$ck_leer = " checked";
						if($row_aux[1] == 't')
							$ck_insertar = " checked";
						if($row_aux[2] == 't')
							$ck_modificar = " checked";
					}
					unset($row_aux);
					$rs_aux->Close(); # optional
				}
?>
	  <tr>
	  <td class="texto" bgcolor="#F6F6F6" valign="middle">
	    <? echo ($ind_link) ? "&nbsp;<a href=\"{$_SERVER['PHP_SELF']}?idgrp=${idgrp}&idmodpad={$row[0]}&tip_modulo=${tip_modulo}&accion=".ACCION_INSERTAR_FALSE."\"><img src=\"/img/800x600/ico-mas.gif\" border=\"0\" align=\"absmiddle\" hspace=\"1\">{$row[1]}</a>" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$row[1]}";?>
	  </td>
		<td align="center" bgcolor="#F6F6F6"> 
		  <input type="checkbox" name="der[<?=$row[0];?>][ind_leer]" value="1"<?echo $ck_leer;?>>
		</td>
		<td align="center" bgcolor="#F6F6F6"> 
		  <input type="checkbox" name="der[<?=$row[0];?>][ind_insertar]" value="1"<?echo $ck_insertar;?>>
		</td>
		<td align="center" bgcolor="#F6F6F6"> 
		  <input type="checkbox" name="der[<?=$row[0];?>][ind_modificar]" value="1"<?echo $ck_modificar;?>>
		</td>
	  </tr>
<?
				unset($ind_link);
				unset($ck_leer);
				unset($ck_insertar);
				unset($ck_modificar);
			}
			unset($row);
			$rs->Close(); # optional
		}
	}
		
	/* ------------------------------------------------------------- */
	/* Funcion para Mostrar el Formulario HTML para asignar			 */
	/* Derechos a un Grupo sobre los Modulos Existentes				 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function insertaGrupoDerechoHTML($idgrp,$tip_modulo=NULL,$idmodpad=NULL){
		$frmName = "frmGrpDer";
		$this->insertaScriptSubmitForm($frmName);
?>
	<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	
  <table width="540" border="0" cellspacing="1" cellpadding="2">
    <tr> 
      <td colspan="4" class="texto" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="4" width="540"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="td-lb-login" width="140"><b>TIPO DE MODULO</b></td>
            <td width="400"> 
              <?
		$sql_st = "SELECT id_modulotipo, des_modulotipo FROM modulo_tipo WHERE ind_activo IS true ORDER BY 2";
		echo $this->muestraSelectUniversal('tip_modulo', $sql_st, $tip_modulo);
?>
              &nbsp;&nbsp;&nbsp; 
              <input type="submit" value="Desplegar" class="but-login" onClick="document.<?=$frmName;?>.accion.value=<?=ACCION_BUSCAR_TRUE?>;">
              <input type="hidden" name="idgrp" value="<?=$idgrp;?>">
              <input type="hidden" name="idmodpad" value="<?=$idmodpad;?>">
              <input type="hidden" name="accion">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td colspan="4" class="texto" width="540"> 
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="4" width="540">&nbsp;</td>
    </tr>
<?
		if(!is_null($idmodpad)&&!empty($idmodpad)){
			$datosMod = $this->getIDModuloPadre($idmodpad);
			$url_regresa  = "{$_SERVER['PHP_SELF']}?idgrp={$idgrp}";
			$url_regresa .= (!is_null($datosMod[0])) ? "&idmodpad={$datosMod[0]}" : "";
			$url_regresa .= "&tip_modulo=${tip_modulo}&accion=".ACCION_INSERTAR_FALSE;
?>
    <tr>
      <td colspan="4" width="540">
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="4" width="540">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="td-lb-login" width="170"><b>SUBMODULOS DEL MODULO</b></td>
            <td class="texto" width="270"><b> 
              <? echo $datosMod[1]; ?>
              </b></td>
            <td width="100" align="center"> 
              <img src="/img/lb_mover.gif" width="66" height="20" name="lb_mover"> 
              <a href="<?=$url_regresa?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('lb_mover','','/img/lb_regresar.gif',0)"><img src="/img/b_regresar.gif" width="20" height="20" border="0"></a> 
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td colspan="4" width="540">
        <hr width="100%" size="1">
      </td>
    </tr>
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
<?
		}
		if(!is_null($tip_modulo)&&!empty($tip_modulo)){
?>
    <tr> 
      <td rowspan="2" align="center" width="300" class="texto" bgcolor="#CCE7F9"><b>NOMBRE</b></td>
      <td colspan="3" align="center" width="240" class="texto" bgcolor="#CCE7F9"><b>DERECHOS</b></td>
    </tr>
    <tr> 
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Leer</td>
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Insertar</td>
      <td align="center" width="80" class="texto" bgcolor="#DFEDF6">Modificar</td>
    </tr>
    <?
			$this->muestraGrupoModulosDerechos($idgrp,$tip_modulo,$idmodpad);
?>
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
    <tr align="center"> 
      <td colspan="4" width="540"> 
        <input type="submit" value="INGRESAR DERECHOS" class="but-login" onClick="document.<?=$frmName;?>.accion.value=<?=ACCION_INSERTAR_TRUE?>;">
        &nbsp;&nbsp;&nbsp; 
        <input type="reset" value="LIMPIAR DERECHOS" class="but-login" name="reset">
      </td>
    </tr>
    <?
		}
?>
  </table>
	</form>
<?
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Insertar a la Base de Datos los derechos		 */
	/* asignados a un Grupo sobre determinados Modulos				 */
	/* ------------------------------------------------------------- */
	/* MODULO GRUPO-DERECHOS										 */
	/* ------------------------------------------------------------- */
	
	function insertaGrupoDerechosDB($id_grupo, $derechos, $id_modulopadre, $tip_modulo){
		$error = false;
		if(count($derechos)>0){
			$cont = 0;
			while (list ($id_modulo) = each ($derechos)) {
				if(count($derechos[$id_modulo])>0){
					$ins_st[$cont] = "INSERT INTO modulo_derecho(id_modulo, id_grupo";
					$val_st[$cont] = "VALUES (${id_modulo}, ${id_grupo}";
					while(list ($tip_derecho, $val) = each ($derechos[$id_modulo])){
						$ins_st[$cont] .= ",${tip_derecho}";
						$val_st[$cont] .= ",'$val'";
					}
					$ins_st[$cont] .= ") " . $val_st[$cont] . ")";
					unset($val_st);
					$cont++;		
				}
			}
	
			$this->abreConnDB();
			
			// Borra los Anteriores Derechos del Grupo
			$sql_mod_st = "SELECT id_modulo from modulo where id_modulopadre";
			$sql_mod_st .= (!empty($id_modulopadre)) ? "=${id_modulopadre}" : ' IS NULL';
			$sql_mod_st .= " and id_modulotipo=${tip_modulo} and ind_activo IS true";

			$rs = & $this->conn->Execute($sql_mod_st);
			unset($sql_mod_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				while($row = $rs->FetchRow())
					$del_st[]="DELETE FROM modulo_derecho WHERE id_grupo=${id_grupo} and id_modulo={$row[0]}";
				unset($row);
				$rs->Close();
			}			
			for($i=0; $i<count($del_st); $i++){
				if ($this->conn->Execute($del_st[$i]) === false){
					print 'error borrando: '.$this->conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($del_st);
			
			for($i=0; $i<count($ins_st); $i++){
				if ($this->conn->Execute($ins_st[$i]) === false){
					print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($ins_st); 
				
			// Muestra el Mensaje Status de la Operacion
			if($error)
				$this->muestraTablaStatusDB("Ha ocurrido un error al asignar los derechos.<br>Vuelva a intentarlo por favor.","Modulo Derechos de Grupo");
			else
				$this->muestraTablaStatusDB("Asignación de Derechos de manera correcta.","Modulo Derechos de Grupo");
		}else{

			$this->abreConnDB();
				
			// Borra los Anteriores Derechos del Grupo
			$sql_mod_st = "SELECT id_modulo from modulo where id_modulopadre";
			$sql_mod_st .= (!empty($id_modulopadre)) ? "=${id_modulopadre}" : ' IS NULL';
			$sql_mod_st .= " and id_modulotipo=${tip_modulo} and ind_activo IS true";
			$rs = & $this->conn->Execute($sql_mod_st);
			unset($sql_mod_st);

			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				while($row = $rs->FetchRow())
					$del_st[]="DELETE FROM modulo_derecho WHERE id_grupo=${id_grupo} and id_modulo={$row[0]}";
				unset($row);
				$rs->Close();
			}			
			for($i=0; $i<count($del_st); $i++){
				if ($this->conn->Execute($del_st[$i]) === false){
					print 'error borrando: '.$this->conn->ErrorMsg().'<BR>';
					$error = true;
				}
			}
			unset($del_st);
			$this->conn->Close();
	
			// Muestra el Mensaje Status de la Operacion
			if($error)
				$this->muestraTablaStatusDB("Ha ocurrido un error al asignar los derechos.<br>Vuelva a intentarlo por favor.","Modulo Derechos de Grupo");
			else
				$this->muestraTablaStatusDB("Asignación de Derechos de manera correcta.<br>El grupo no tendra ningun derecho sobre los modulos.","Modulo Derechos de Grupo");
		}
	}

}
?>