<?
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class EmbarcacionesNac extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function EmbarcacionesNac($menu,$subMenu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][5];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							//SUMARIO => 'frmSumario',
							FRM_AGREGA_EMBARCACIONNAC =>'frmAddEmbarcacionNac',
							AGREGA_EMBARCACIONNAC => 'addEmbarcacionNac',
							FRM_BUSCA_EMBARCACIONNAC =>'frmSearchDatos',
							BUSCA_EMBARCACIONNAC =>'searchDatos',
							CREA_CSV => 'createCSV',							
							FRM_MODIFICA_EMBARCACIONNAC =>'frmModifyEmbarcacionNac',
							MODIFICA_EMBARCACIONNAC => 'modifyEmbarcacionNac',							
							MUESTRA_DETALLE => 'showDetail',
							IMPRIME_DETALLE => 'printDetail',
							FRM_BUSCA_ARMADOR =>'frmSearchArm',
							BUSCA_ARMADOR =>'searchArm',
							CREA_CSV2 => 'createCSV2',
							FRM_AGREGA_ARMADOR =>'frmAddArmador',
							AGREGA_ARMADOR => 'addArmador',
							FRM_MODIFICA_ARMADOR =>'frmModifyArmador',
							MODIFICA_ARMADOR => 'modifyArmador',
							MUESTRA_HIST_SUSP => 'showHistSusp',
							IMPRIME_HIST_SUSP => 'printHistSusp',
							MUESTRA_HIST_SUSP_PP => 'showHistSuspPP',
							IMPRIME_HIST_SUSP_PP => 'printHistSuspPP',
							MUESTRA_HIST_DUENO => 'showHistDueno',
							MUESTRA_DETALLE_RESOLUCION => 'showDetalleResol',
							MUESTRA_DETALLE_PERSONA => 'showDetailPer',
							FRM_GENERA_REPORTE => 'frmReportes',//Para sacar la matriz de incumplimiento
							GENERA_REPORTE => 'Reportes',
							FRM_REGISTRA_DECJUR_MENSUAL => 'frmRegDecJur',
							REGISTRA_DECJUR_MENSUAL => 'regDecJur',
							LISTADO_REPORTES => 'listadoReportes',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();
		
		
		//$this->menu_items[0] = array('val' => 'frmSearchDatos', label => 'EMBARCACIONES');
		//$this->menu_items[1] = array('val' => 'frmSearchArm', label => 'ARMADORES');
		$this->menu_items[2] = array('val' => 'frmReportes', label => 'REPORTES');
		//$this->menu_items[3] = array('val' => 'frmRegDecJur', label => 'DECLARACI�N JURADA');
		
		
		// Menu Seleccionado
		//$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		$this->menuPager = ($menu) ? $menu : $this->menu_items[2]['val'];
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchDatos', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddEmbarcacionNac', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyEmbarcacionNac', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmSearchArm', label => 'BUSCAR' ),
										 1 => array ( 'val' => 'frmAddArmador', label => 'AGREGAR' ),
										 2 => array ( 'val' => 'frmModifyArmador', label => 'MODIFICAR' )
										 );
		elseif($this->menuPager==$this->menu_items[2]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmReportes', label => 'Reportes' )
										 );
		elseif($this->menuPager==$this->menu_items[3]['val'])
			$this->subMenu_items = array(0 => array ( 'val' => 'frmRegDecJur', label => 'MENSUAL' )
										 );								 
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];										 
		
		$this->pathTemplate = 'dnepp/embarcacionnac/';		
    }
	
	function MuestraStatTrans($accion){

		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_EMBARCACIONNAC]);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_ARMADOR]);			
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		//$this->FormBuscaEmbarcacionNac();
		$this->FormGeneraReporte();
		//$this->XMLEmbarcacion();
	}
	
	function XMLEmbarcacion(){
		$html = & new Smarty;
		$this->abreConnDB();
		
		$a=htmlspecialchars("<")."embarcaciones".htmlspecialchars(">");
				
		$sql_st = "SELECT id_emb, nombre_emb, substring(nombre_emb,1,1), matricula_emb
  FROM db_dnepp.user_dnepp.embarcacionnac
  ORDER BY 2";
	
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						/*while($AtencionData = $rs->FetchRow())
							$a.="<".$AtencionData[2]."><puerto><id>".$AtencionData[0]."</id><nombre>".$AtencionData[1]."</nombre></puerto></A>";
						$rs->Close();*/
						
						$ii=-1;
						while(!$rs->EOF){
							$ii++;
							$codigoPuerto[$ii]=$rs->fields[0];
							$descripcionPuerto[$ii]=$rs->fields[1];
							$firstCharPuerto[$ii]=$rs->fields[2];
							$matricula[$ii]=$rs->fields[3];
							
							if($ii==0){
								//$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[0].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
								$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[0]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[0]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
							}else{
								if($firstCharPuerto[$ii]!=$firstCharPuerto[$ii-1]){
									$a.=htmlspecialchars("</").$firstCharPuerto[$ii-1].htmlspecialchars(">");
									//$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}else{
									//$a.=htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<")."embarcacion".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("<")."matricula".htmlspecialchars(">").htmlentities($matricula[$ii]).htmlspecialchars("</")."matricula".htmlspecialchars(">")."<br>".htmlspecialchars("</")."embarcacion".htmlspecialchars(">");
								}		
							}
							
							$rs->MoveNext();
						}
						$rs->Close();						
						
					}
		$contador=count($firstCharPuerto);			
		$a.=htmlspecialchars("</").$firstCharPuerto[$contador-1].htmlspecialchars(">");
		//$a.=htmlspecialchars("</")."puertos".htmlspecialchars(">");
		$a.=htmlspecialchars("</")."embarcaciones".htmlspecialchars(">");
		echo "<br>".$a;													
	
	
		/*$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow()){
				$html->append('emb',array('id'=>$row[0],
										  'nomb'=>$row[1],
										  'matr'=>$row[2]));
			}
			$rs->Close();
		}
		unset($rs);
		
		$html->display('dnpa/desembarque/XMLEmbarcaciones.tpl.php');*/
	
	}	
	
	function FormBuscaEmbarcacionNac($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$idArte=NULL,$idRegimen=NULL,$idPreserva=NULL,$idCasco=NULL,$idDestino=NULL,$idEspecie=NULL,$idEstado='none',$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		
		$html->assign_by_ref('estado',$idEstado);
		
		//$a=htmlspecialchars("<")."puertos".htmlspecialchars(">");
		$a=htmlspecialchars("<")."establecimientos".htmlspecialchars(">");
		/*$sql_st = "SELECT CODIGO,DESCRIPCION,substring(descripcion,1,1) ".
					"FROM db_generaL.DBO.puertoS ".
					"order by 2";/**/
		/**/$sql_st = "sELECT ID,RAZON_SOCIAL,substring(RAZON_SOCIAL,1,1) 
					FROM DB_GENERAL.DBO.EIPS
					order by 2";					/**/
					$rs = & $this->conn->Execute($sql_st);
					unset($sql_st);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						/*while($AtencionData = $rs->FetchRow())
							$a.="<".$AtencionData[2]."><puerto><id>".$AtencionData[0]."</id><nombre>".$AtencionData[1]."</nombre></puerto></A>";
						$rs->Close();*/
						
						$ii=-1;
						while(!$rs->EOF){
							$ii++;
							$codigoPuerto[$ii]=$rs->fields[0];
							$descripcionPuerto[$ii]=$rs->fields[1];
							$firstCharPuerto[$ii]=$rs->fields[2];
							
							if($ii==0){
								//$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[0].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
								$a.=htmlspecialchars("<").$firstCharPuerto[0].htmlspecialchars(">").htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[0].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[0]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
							}else{
								if($firstCharPuerto[$ii]!=$firstCharPuerto[$ii-1]){
									$a.=htmlspecialchars("</").$firstCharPuerto[$ii-1].htmlspecialchars(">");
									//$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<").$firstCharPuerto[$ii].htmlspecialchars(">").htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
								}else{
									//$a.=htmlspecialchars("<")."puerto".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").$descripcionPuerto[$ii].htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."puerto".htmlspecialchars(">");
									$a.=htmlspecialchars("<")."establecimiento".htmlspecialchars(">")."<br>".htmlspecialchars("<")."id".htmlspecialchars(">").$codigoPuerto[$ii].htmlspecialchars("</")."id".htmlspecialchars(">")."<br>".htmlspecialchars("<")."nombre".htmlspecialchars(">").htmlentities($descripcionPuerto[$ii]).htmlspecialchars("</")."nombre".htmlspecialchars(">")."<br>".htmlspecialchars("</")."establecimiento".htmlspecialchars(">");
								}		
							}
							
							$rs->MoveNext();
						}
						$rs->Close();						
						
					}
		$contador=count($firstCharPuerto);			
		$a.=htmlspecialchars("</").$firstCharPuerto[$contador-1].htmlspecialchars(">");
		//$a.=htmlspecialchars("</")."puertos".htmlspecialchars(">");
		$a.=htmlspecialchars("</")."establecimientos".htmlspecialchars(">");
		//echo "<br>".$a;												

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function ObtieneArmadoresEmbarcacion($id,$sep=' - '){
		// Obtiene los Armadores de la Embarcacion
		$sql_SP = sprintf("EXECUTE sp_busca_armador_embarcacion %d",$id);
		$rsArm = & $this->conn->Execute($sql_SP);
		// echo $sql_SP;
		unset($sql_SP);
		if(!$rsArm)
			print $this->conn->ErrorMsg();
		else{
			while($embArm = $rsArm->FetchRow())
				$strEmbArm[] = $embArm[0];
			$rsArm->Close();
		}
		unset($rsArm);
		
		return (isset($strEmbArm)) ? implode($sep,$strEmbArm) : false;
	}
	
	function ObtieneAparejosEmbarcacion($id,$sep=' - '){
		// Obtiene los Aparejos de la Embarcacion
		$sql_SP = sprintf("EXECUTE sp_busca_aparejo_embarcacion %d",$id);
		$rsApa = & $this->conn->Execute($sql_SP);
		//echo $sql_SP;
		unset($sql_SP);
		if(!$rsApa)
			print $this->conn->ErrorMsg();
		else{
			while($embApa = $rsApa->FetchRow())
				$strEmbApa[] = $embApa[0];
			$rsApa->Close();
		}
		unset($rsApa);
		
		return (isset($strEmbApa)) ? implode($sep,$strEmbApa) : false;
	}	
	
	function BuscaEmbarcacionNac($page,$tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		//Contador de caracteres
		$cont=strlen($desNombre);
		$cont2=strlen($tipBusqueda);
		//echo "el contador es".$cont;
		/**/
		if($cont>10||($cont2>5)){
		 $this->MuestraIndex();
		 exit;
		}/**/		
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaEmbarcacionNac($page,$tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($idArte)&&!is_null($idArte)&&$idArte!='none'){
			$condConsulta[] = "emb.id_emb=eap.id_emb and eap.id_apa={$idArte}";
			$condTables[] = "user_dnepp.embxapar eap";
		}
		if(!empty($idDestino)&&!is_null($idDestino)&&$idDestino!='none'){
			$condConsulta[] = "emb.id_emb=eed.id_emb and eed.id_destino='{$idDestino}'";
			$condTables[] = "user_dnepp.embxespxdest eed";
		}
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = sprintf("(emb.id_estper=%1\$d or emb.estadozarpe_emb=%1\$d)",$idEstado);
		if(!empty($idPreserva)&&!is_null($idPreserva)&&$idPreserva!='none')
			$condConsulta[] = "emb.id_tpres={$idPreserva}";
		if(!empty($idRegimen)&&!is_null($idRegimen)&&$idRegimen!='none')
			$condConsulta[] = "emb.id_regimen={$idRegimen}";
		if(!empty($idCasco)&&!is_null($idCasco)&&$idCasco!='none')
			$condConsulta[] = "emb.id_casco={$idCasco}";
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
				
		$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where));

		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
								$this->PrepareParamSQL($tables),
								$this->PrepareParamSQL($where),
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($embID = $rsId->FetchRow()){
					// Obtiene los Armadores de la Embarcacion
					$strEmbArm = $this->ObtieneArmadoresEmbarcacion($embID[0]);
					$strEmbArm = (strlen($strEmbArm)>60) ? substr($strEmbArm,0,57).'...' : $strEmbArm;
					// Obtiene los Aparejos de la Embarcacion
					$strEmbApa = $this->ObtieneAparejosEmbarcacion($embID[0]);
					$strEmbApa = (strlen($strEmbApa)>60) ? substr($strEmbApa,0,57).'...' : $strEmbApa;
					// Obtiene Todos los Datos de la Embarcacion
					$sql_SP = sprintf("EXECUTE sp_busca_datos_embarcacion %d",$embID[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($embData = $rsData->FetchRow())
							$html->append('arrEmb', array('id' => $embData[0],
														  'desc' => $embData[1],
														  'matri' => $embData[2],
														  'armador' => ($strEmbArm) ? ucwords(strtolower($strEmbArm)) : 'No tiene',
														  'aparejo' => ($strEmbApa) ? ucwords(strtolower($strEmbApa)) : 'No tiene',
														  'regimen' => ucwords(strtolower($embData[3])),
														  'preserva' => ucwords(strtolower($embData[4])),
														  'casco' => ucwords(strtolower($embData[5])),
														  'pesca' => ucfirst($embData[15]),
														  'zarpe' => ucfirst($embData[16]),
															'constancia' => ucfirst($embData[17])
														  ));
						$rsData->Close();
					}
					unset($rsData);
					unset($strEmbArm);
					unset($strEmbApa);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign('datos',array('desNombre'=>$desNombre,
									'tipBusqueda'=>$tipBusqueda,
									'arte'=>$idArte,
									'regimen'=>$idRegimen,
									'preserva'=>$idPreserva,
									'casco'=>$idCasco,
									'destino'=>$idDestino,
									'estado'=>$idEstado));
		
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_EMBARCACIONNAC], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function ObtieneDestinoEspecieEmbarcacion($idEmb,$idDest,$sep='<br>- '){
		// Lista las Especies de la Embarcacion por el Destino especificado
		$sql_SP = sprintf("EXECUTE  sp_lista_espxdestxemb %d,'%s'",
						   $this->PrepareParamSQL($idEmb),
						   $this->PrepareParamSQL($idDest));
		// echo $sql_SP;
		$rsEsp = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsEsp){
			print $this->conn->ErrorMsg();
		}else{
			while($esp = $rsEsp->FetchRow())
				$embEsp[] = $esp[0];
			unset($esp);
			$rsEsp->Close();
		}
		unset($rsEsp);
		
		return (is_array($embEsp)) ? implode($sep,$embEsp) : false;
	}

	
	function DetalleEmbarcacion($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;

		$id=urldecode($id);
		$this->abreConnDB();
		//$this->conn->debug=true;
			
		$sql_SP = sprintf("EXECUTE sp_lista_embarcacion %d,'<br>'",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rsData = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsData){
			print $this->conn->ErrorMsg();
		}else{
			if($embData = $rsData->FetchRow())
				/* Sequencia de Datos
				   0. nombre_emb
				   1. matricula_emb
				   2. desc_casco
				   3. capbod_emb
				   4. capbod1_emb
				   5. desc_regimen
				   6. especies **
				   7. armador *
				   8. Patron **
				   9. Doc. Identidad **
				   10. desc_estper
				   11. numero_res (Res. Suspencion) *
				   12. Cod. Baliza **
				   13. Ind. Pesca Jurel **
				   14. Matri. alto/bajo relieve **
				   15. numero_res (Res. permiso)
				   16. capbod23porc (inc. 3%)
				   17. capbod215porc (inc. 15%)
				   18. sist. pesca
				   19. tip. preserv.
				   20. numero_res (Res. Cancelacion)
				   21. Estado de Zarpe
				   22. Motivo Suspension Zarpe
				   23. Documento de Susp. de Zarpe
				   24. Fecha de Suspension de Zarpe
				   25. Transmisor
				   26. Motor Caracteristicas
			
				   * Datos sacados de Sub Store Procedures
				   ** Datos faltantes
				*/
				$html->assign('emb', array('id' => $id,
										  'desc' => $embData[0],
										  'matri' => $embData[1],
										  'casco' => $embData[2],
										  'capbod' => $embData[3],
										  'capbodTM' => $embData[4],
										  'regimen' => $embData[5],
										  'armador' => $embData[7],
										  'estado' => $embData[10],
										  'resSusp' => $embData[11],
										  'resPerm' => $embData[15],
										  'capbod3p' => ($embData[3]>=32.6 && $embData[3]<=50) ? round($embData[4]*1.06,2) : round($embData[4]*1.03,2),
										  'capbod15p' => round($embData[4]*1.15,2),
										  'aparejo' => $embData[18],
										  'preserva' => $embData[19],
										  'resCan' => $embData[20],
										  'zarpe' => $embData[21],
										  'motZarpe' => $embData[22],
										  'docZarpe' => $embData[23],
										  'fecZarpe' => $embData[24],
										  'trans' => $embData[25],
										  'motor' => $embData[26]
										  ));
			unset($row);
			$rsData->Close();
		}
		unset($rsData);
		
		// Agrega los Destinos y las Especies al Template HTML
		$sql_SP = "EXECUTE sp_lista_destino";
		$rsDest = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsDest){
			print $this->conn->ErrorMsg();
		}else{
			// $cont = 0;
			while($dest = $rsDest->FetchRow()){
				$html->append("embDest",$dest[1]);
				$html->append("embEsp",$this->ObtieneDestinoEspecieEmbarcacion($id,$dest[0]));
			}
			unset($dest);
			$rsDest->Close();
		}
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('dnepp/embarcacionnac/printDetalle.tpl.php') : $html->display('dnepp/embarcacionnac/showDetalle.tpl.php');
	}
	
	function ImprimeDetalleEmbarcacion($id){
		$this->DetalleEmbarcacion($id, true);
	}
	
	function HistorialSuspensionZarpe($id,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow())
				$html->assign(array('id'=>$row[0],'desc'=>$row[1],'matri'=>$row[2]));
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("EXECUTE sp_listHistSuspEmb %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp',array('oDis'=>$row[0],
										   'fIni'=>$row[1],
										   'moti'=>$row[2],
										   'fFin'=>$row[3],
										   'obse'=>$row[4],
										   'esta'=>$row[5]));
			$rs->Close();
		}
		unset($rs);

		$print ? $html->display('dnepp/embarcacionnac/printHistSusp.tpl.php') : $html->display('dnepp/embarcacionnac/showHistSusp.tpl.php');
	}
	
	function ImprimeHistorialSuspensionZarpe($id){
		$this->HistorialSuspensionZarpe($id,true);
	}
	
	function HistorialSuspensionPesca($id,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow())
				$html->assign(array('id'=>$row[0],'desc'=>$row[1],'matri'=>$row[2]));
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("EXECUTE sp_listHistSuspPPEmb %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp',array('resu'=>$row[0],
										   'fFin'=>$row[1],
										   'obse'=>$row[2],
										   'esta'=>$row[3]));
			$rs->Close();
		}
		unset($rs);
		
		/*$sql_SP = sprintf("SELECT     emb.NOMBRE_EMB AS NOMBRE, 
		   emb.MATRICULA_EMB AS MATRICULA, 
                      EMBXSUSP.FECHALEVANTE_EMBXSUSP AS FECHA_LEVANTE, 
RESOLUCION.NUMERO_RES AS RESOLUCION, 
                      EMBXSUSP.OBS_EMBXSUSP EMBXSUSP 
FROM         DB_DNEPP.user_dnepp.EMBARCACIONNAC emb INNER JOIN DB_DNEPP.user_dnepp.EMBXSUSP EMBXSUSP ON EMB.ID_EMB =EMBXSUSP.ID_EMB 
			 										INNER JOIN DB_DNEPP.user_dnepp.RESOLUCION resolucion ON EMBXSUSP.ID_RES = RESOLUCION.ID_RES 

WHERE  emb.id_emb=%d and (NOT (EMBXSUSP.FECHALEVANTE_EMBXSUSP IS NULL)) 
ORDER BY EMBXSUSP.FECHALEVANTE_EMBXSUSP DESC",$this->PrepareParamSQL($id));*/
		$sql_SP=sprintf("SELECT Upper(numero_res),  Convert(varchar(10),fechalevante_embxsusp,103), Upper(obs_embxsusp), CASE WHEN fechalevante_embxsusp is NULL THEN 1 ELSE 0 END,
						fechinisusp_embxsusp, fechfinsusp_embxsusp, exs.fechultmod,
					  fechaincursion_embxsusp, infraccion_embxsusp, zonadepesca_embxsusp,
					  oficioaarmador_embxsusp,r.fechapub_res,r.resgen_res,r.fechultmod
  FROM user_dnepp.embxsusp exs INNER JOIN user_dnepp.resolucion r ON exs.id_res=r.id_res
  WHERE id_motembxsusp is NULL and id_emb=%d",$id);
		
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp2',array('nombre'=>$row[0],
										   'esta'=>$row[3],
										   'fecLev'=>$row[1],
										   'resol'=>$row[0],
										   'obs'=>$row[2],
										   'fIni'=>$row[4],
										   'fFin'=>$row[5],
										   'fMod'=>$row[6],
										   'fInc'=>$row[7],
										   'inf'=>$row[8],
										   'zonaPesca'=>$row[9],
										   'ofaArma'=>$row[10],
										   'fecResol'=>$row[11],
										   'obsResol'=>$row[12],
										   'auditResol'=>$row[13]										   
										   ));
			$rs->Close();
		}
		unset($rs);		

		$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistSuspPP.tpl.php');
	}
	
	function ImprimeHistorialSuspensionPesca($id){
		$this->HistorialSuspensionPesca($id,true);
	}

	function HistorialDuenos($id,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow())
				$html->assign(array('id'=>$row[0],'desc'=>$row[1],'matri'=>$row[2]));
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("EXECUTE sp_listHistDuenos %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp',array('rz'=>$row[2],
										   'resol'=>$row[3],
										   'status'=>$row[4],
										   'idResol'=>$row[5],
										   'idPersona'=>$row[6]));
			$rs->Close();
		}
		unset($rs);

		$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistDuenos.tpl.php');
	}	

	function MuestraResolucion($idResol,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		//$this->conn->debug=true;
		if($idResol==653){
			$html->display('dnepp/embarcacionnac/resoluciones/rd/2001/323-2001-PE-DNEPP.tpl.php');
		}elseif($idResol==433){
			$html->display('dnepp/embarcacionnac/resoluciones/rd/2002/056-2002-CONVENIO_SITRADOC-DNEPP.tpl.php');
		}elseif($idResol==4266){
			$html->display('dnepp/embarcacionnac/resoluciones/rd/2006/446-2006-CONVENIO_SITRADOC-DGEPP.tpl.php');
		}

		//$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistDuenos.tpl.php');
	}
	
	function DetallePersona($idPersona, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
				
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql=sprintf("select s.descripcion,tp.descripcion,depa.departamento,prov.provincia,dist.distrito,tti.descripcion,
						case when p.id_tipo_persona=1 then p.apellidos+' '+p.nombres
							 when p.id_tipo_persona=2 then p.razon_social end as RAZON_SOCIAL,p.nro_documento,p.direccion,p.telefono,p.fax,p.email,p.representante_legal,
						tti2.descripcion,p.nro_documento_representante
						from db_general.dbo.sector s,db_general.dbo.tipo_persona tp,
							 db_general.dbo.departamento depa,db_general.dbo.provincia prov,db_general.dbo.distrito dist,
							 db_general.dbo.t_tipo_identificacion tti,
							 db_general.dbo.persona p left join db_general.dbo.t_tipo_identificacion tti2 on p.id_tipo_identificacion_rep_leg=tti2.codigo_t_identificacion
						where p.id_sector=s.id and p.id_tipo_persona=tp.id_tipo_persona
						  and p.codigo_departamento=depa.codigo_departamento
						  and p.codigo_provincia=prov.codigo_provincia and prov.codigo_departamento=depa.codigo_departamento
						  and p.codigo_distrito=dist.codigo_distrito and dist.codigo_provincia=prov.codigo_provincia and dist.codigo_departamento=depa.codigo_departamento
						  and p.id_tipo_identificacion=tti.codigo_t_identificacion
						  and 
					      p.id=%d",$idPersona);
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
		
			$sector=$rs->fields[0];
			$tipPersona=$rs->fields[1];
			$departamento=$rs->fields[2];
			$provincia=$rs->fields[3];
			$distrito=$rs->fields[4];
			$tipIdent=$rs->fields[5];
			$RazonSocial=$rs->fields[6];
			$nroDoc=$rs->fields[7];
			$direccion=$rs->fields[8];
			$telefono=$rs->fields[9];
			$fax=$rs->fields[10];
			$email=$rs->fields[11];
			$RepLegal=$rs->fields[12];
			$tipIdentRep=$rs->fields[13];
			$nroDocRep=$rs->fields[14];
		}	
		$html->assign_by_ref('idPersona',$idPersona);
		$html->assign_by_ref('sector',$sector);				  
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('departamento',$departamento);
		$html->assign_by_ref('provincia',$provincia);
		$html->assign_by_ref('distrito',$distrito);			
		$html->assign_by_ref('tipIdent',$tipIdent);	  
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		$html->assign_by_ref('nroDoc',$nroDoc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('RepLegal',$RepLegal);
		$html->assign_by_ref('tipIdentRep',$tipIdentRep);
		$html->assign_by_ref('nroDocRep',$nroDocRep);
		
		$html->assign_by_ref('dependencia',$this->userIntranet['DEPENDENCIA']);
		
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oad/tramite/directorio/printDetallesPersona.tpl.php') : $html->display('dnepp/embarcacionnac/showDetallePersona.tpl.php');
	}	
	
	function CreaCSVBuscaEmbarcacion($tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado){
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($idArte)&&!is_null($idArte)&&$idArte!='none'){
			$condConsulta[] = "emb.id_emb=eap.id_emb and eap.id_apa={$idArte}";
			$condTables[] = "user_dnepp.embxapar eap";
		}
		if(!empty($idDestino)&&!is_null($idDestino)&&$idDestino!='none'){
			$condConsulta[] = "emb.id_emb=eed.id_emb and eed.id_destino='{$idDestino}'";
			$condTables[] = "user_dnepp.embxespxdest eed";
		}
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = sprintf("(emb.id_estper=%1\$d or emb.estadozarpe_emb=%1\$d)",$idEstado);
		if(!empty($idPreserva)&&!is_null($idPreserva)&&$idPreserva!='none')
			$condConsulta[] = "emb.id_tpres={$idPreserva}";
		if(!empty($idRegimen)&&!is_null($idRegimen)&&$idRegimen!='none')
			$condConsulta[] = "emb.id_regimen={$idRegimen}";
		if(!empty($idCasco)&&!is_null($idCasco)&&$idCasco!='none')
			$condConsulta[] = "emb.id_casco={$idCasco}";
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el Store Procedure que lista todos los registros de la Busqueda
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',%d,%d",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where),
							1,
							0);
		//echo $sql_SP;

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			// Genera HTML
			$html = new Smarty;
			
			// Consulta y Agrega al Template HTML los Destinos Validos para toda embarcacion
			$sql_SP = "EXECUTE sp_lista_destino";
			$rsDest = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsDest){
				print $this->conn->ErrorMsg();
			}else{
				while($dest = $rsDest->FetchRow()){
					$html->append("embDest",$dest[1]);
					$arrIdDest[] = $dest[0];
				}
				unset($dest);
				$rsDest->Close();
			}
			unset($rsDest);

			while($embID = $rsId->FetchRow()){
				// Obtiene las Especie por Destino de Cada Embarcacion
				for($i=0;$i<count($arrIdDest);$i++)
					$embEsp[] = $this->PrepareParamSQL($this->ObtieneDestinoEspecieEmbarcacion($embID[0],$arrIdDest[$i],"\n"),'"');

				// Consulta y Agrega al Template HTML los Destinos Validos para toda embarcacion
				$sql_SP = sprintf("EXECUTE sp_lista_embarcacion %d,'%s'",
									$embID[0],"\n");
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					if($row = $rs->FetchRow())
						/* Sequencia de Datos
						   0. nombre_emb
						   1. matricula_emb
						   2. desc_casco
						   3. capbod_emb
						   4. capbod1_emb
						   5. desc_regimen
						   6. especies **
						   7. armador *
						   8. Patron **
						   9. Doc. Identidad **
						   10. desc_estper
						   11. numero_res (Res. Suspencion) *
						   12. Cod. Baliza **
						   13. Ind. Pesca Jurel **
						   14. Matri. alto/bajo relieve **
						   15. numero_res (Res. permiso)
						   16. capbod23porc (inc. 3%)
						   17. capbod215porc (inc. 15%)
						   18. sist. pesca
						   19. tip. preserv.
						   20. numero_res (Res. Cancelacion)
						   21. Estado de Zarpe
						   22. Documento de Susp. de Zarpe
						   23. Fecha de Suspension de Zarpe
						   24. Transmisor
					
						   * Datos sacados de Sub Store Procedures
						   ** Datos faltantes
						*/		
						// Agraga los datos de la Embarcacion al Template HTML
						$html->append('arrEmb',array('desc'=>$this->PrepareParamSQL($row[0],'"'),
													 'matr'=>$this->PrepareParamSQL($row[1],'"'),
													 'casc'=>$this->PrepareParamSQL($row[2],'"'),
													 'capbod'=>$this->PrepareParamSQL($row[3],'"'),
													 'capbodTM'=>($row[4]) ? $this->PrepareParamSQL($row[4],'"') : 'NO TIENE',
													 'regi'=>($row[5]) ? $this->PrepareParamSQL($row[5],'"') : 'NO ESPECIFICADO',
													 'espe'=>$embEsp,
													 'arma'=>$this->PrepareParamSQL($row[7],'"'),
													 'esta'=>$this->PrepareParamSQL($row[10],'"'),
													 'resSusp'=>($row[11]) ? $this->PrepareParamSQL($row[11],'"') : 'NO TIENE',
													 'resPerm'=>($row[15]) ? $this->PrepareParamSQL($row[15],'"') : 'NO TIENE',
													 'cb3p'=>($row[3]>=32.6 && $row[3]<=50) ? $this->PrepareParamSQL(round($row[4]*1.06,2),'"') : $this->PrepareParamSQL(round($row[4]*1.03,2),'"'),
													 'cb15p'=>($row[4]) ? $this->PrepareParamSQL(round($row[4]*1.15,2),'"') : 'NO TIENE',
													 'sist'=>$this->PrepareParamSQL($row[18],'"'),
													 'pres'=>$this->PrepareParamSQL($row[19],'"'),
													 'resCan'=>($row[20]) ? $this->PrepareParamSQL($row[20],'"') : 'NO TIENE',
													 'zarpe' => $this->PrepareParamSQL($row[21],'"'),
													 'motZarpe' => ($row[22]) ? $this->PrepareParamSQL($row[22],'"') : 'NO TIENE',
													 'docZarpe' => ($row[23]) ? $this->PrepareParamSQL($row[23],'"') : 'NO TIENE',
													 'fecZarpe' => ($row[24]) ? $this->PrepareParamSQL($row[24],'"') : 'NO TIENE',
													 'trans' => ($row[25]) ? $this->PrepareParamSQL($row[25],'"') : 'NO TIENE',
													 'motor'=>($row[26]) ? $this->PrepareParamSQL($row[26],'"') : 'NO ESPECIFICADO'));
					unset($embEsp);
					$rs->Close();
				}
				unset($rs);

			}
			$rsId->Close();
			
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=consulta-".mktime().".csv");
			$html->display('dnepp/embarcacionnac/createCSV.tpl.php');
			exit;
			
		}
		unset($rsId);		
	}	

	function FormAgregaEmbarcacionNac($matriculaEmb=NULL,$nombreEmb=NULL,$RZoRUC=NULL,$BuscaRZ=NULL,$RazonSocial=NULL,$puerto=NULL,$sistPesca=NULL,$regimen=NULL,$tipPreservacion=NULL,$casco=NULL,
								$eslora=NULL,$manga=NULL,$puntual=NULL,$capbod=NULL,$capbodTM=NULL,$capbod3p=NULL,$capbod15p=NULL,$transmisor=NULL,$marcaMotor=NULL,$modeloMotor=NULL,
								$serieMotor=NULL,$potenciaMotor=NULL,$tipCombustible=NULL,$statusPerPesca=NULL,$statusPerZarpe=NULL,$literal=NULL,$arqb_emb=NULL,$arqn_emb=NULL,
								$errors=false){
		global $especie,$destino;
		global $bSoftware,$idSoftNew,$idSoft,$idSoftNew2,$idSoft2,$bSoftware2,$idCond2,$idCondNew2,$idSoft3,$idSoftNew3;
		global $tipResol,$nroResol,$BuscaResol,$resolucion;
		$nombrePC=($_POST['nombrePC']) ? $_POST['nombrePC'] : $_GET['nombrePC'];		
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('matriculaEmb',$matriculaEmb);
		$html->assign_by_ref('nombreEmb',$nombreEmb);
		$html->assign_by_ref('eslora',$eslora);
		$html->assign_by_ref('manga',$manga);
		$html->assign_by_ref('puntual',$puntual);
		$html->assign_by_ref('capbod',$capbod);
		$html->assign_by_ref('capbodTM',$capbodTM);
		$html->assign_by_ref('capbod3p',$capbod3p);
		$html->assign_by_ref('capbod15p',$capbod15p);
		$html->assign_by_ref('transmisor',$transmisor);
		$html->assign_by_ref('marcaMotor',$marcaMotor);
		$html->assign_by_ref('modeloMotor',$modeloMotor);
		$html->assign_by_ref('serieMotor',$serieMotor);
		$html->assign_by_ref('potenciaMotor',$potenciaMotor);
		$html->assign_by_ref('arqb_emb',$arqb_emb);
		$html->assign_by_ref('arqn_emb',$arqn_emb);
		$html->assign_by_ref('nroResol',$nroResol);
		
		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('idSoft2',$idSoft2);
		$html->assign_by_ref('idSoft3',$idSoft3);		
		$html->assign_by_ref('error',$error);
		
		if((!$BuscaResol&&$nroResol!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id, substring(nro_resol,1,50) ".
				  "FROM db_tramite_documentario.dbo.resolucion ".
				  "WHERE (Upper(nro_resol) like Upper('%$nroResol%') )".
				  "AND id_tipo_resolucion=$tipResol ".
				  "ORDER BY 2";
		$html->assign_by_ref('selResolucion',$this->ObjFrmSelect($sql_st, $resolucion, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		

		// Contenido Select del Tipo de Resoluci�n
		$sql_st = "SELECT id, substring(lower(descrip_completa),1,18) ".
				  "FROM DB_TRAMITE_DOCUMENTARIO.dbo.TIPO_RESOLUCION ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResol',$this->ObjFrmSelect($sql_st, $tipResol, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);		
		
		// Contenido Select del Puerto
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM DB_GENERAL.DBO.PUERTOS ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPuerto',$this->ObjFrmSelect($sql_st, $puerto, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Sistema de Pesca (Aparejo)
		$sql_st = "SELECT id_apa, lower(nombre_apa) ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSistPesca',$this->ObjFrmSelect($sql_st, $sistPesca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Regimen
		$sql_st = "SELECT id_regimen, lower(desc_regimen) ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $regimen, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Tipo de preservaci�n
		$sql_st = "SELECT id_tpres, lower(desc_tpres) ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipPreservacion',$this->ObjFrmSelect($sql_st, $tipPreservacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Casco
		$sql_st = "SELECT id_casco, lower(desc_casco) ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $casco, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Tipo de combustible
		$sql_st = "SELECT id_tcomb, lower(desc_tcomb) ".
				  "FROM user_dnepp.tipocombustible ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipCombustible',$this->ObjFrmSelect($sql_st, $tipCombustible, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Estado de Permiso de pesca
		$sql_st = "SELECT id_estper, lower(desc_estper) ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selStatusPerPesca',$this->ObjFrmSelect($sql_st, $statusPerPesca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Estado de Permiso de zarpe
		$sql_st = "SELECT estadozarpe_emb, lower(desc_estzarp) ".
				  "FROM user_dnepp.estadozarpe ".
				  "ORDER BY 2";
		$html->assign_by_ref('selStatusPerZarpe',$this->ObjFrmSelect($sql_st, $statusPerZarpe, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Literal
		$sql_st = "SELECT id_literal, lower(desc_literal+' '+detalle_literal) ".
				  "FROM user_dnepp.literal ".
				  "ORDER BY 2";
		$html->assign_by_ref('selLiteral',$this->ObjFrmSelect($sql_st, $literal, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select de la especie x Literal
		$sql_st = sprintf("SELECT e.codigo, lower(e.descripcion) 
				  FROM db_general.dbo.especies e,user_dnepp.literalxespecie le 
				  WHERE e.codigo=le.id_esp 
				  AND le.id_literal=%d 
				  ORDER BY 2",($literal)? $literal : "xx");
		$html->assign_by_ref('selEspecie',$this->ObjFrmSelect($sql_st, $especie, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Destino
		$sql_st = "SELECT id_destino, lower(desc_destino) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $destino, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);				
		
		if((!$BuscaRZ&&$RZoRUC!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id_arm, substring(razonsocial_arm,1,50) ".
				  "FROM user_dnepp.armador ".
				  "WHERE (Upper(razonsocial_arm) like Upper('%$RZoRUC%') )".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		// Lista el Armador ya agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){

					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id_arm,razonsocial_arm,domicilio_arm
				  						FROM user_dnepp.armador 
										where id_arm=%d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'asunto' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Lista la especie x literal ya agregado
		$idSoft2 = (is_array($idSoft2)) ? $idSoft2 : array();
		$idCond2 = (is_array($idCond2)) ? $idCond2 : array();
		if($idSoftNew2) array_push($idSoft2, $idSoftNew2);
		if($idCondNew2) array_push($idCond2, $idCondNew2);
		if($idSoft2&&count($idSoft2)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft2);$i++){
				if(!empty($idSoft2[$i])&&!is_null($idSoft2[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT codigo,descripcion
				  						FROM db_general.dbo.especies 
										where codigo=%d",
									  $this->PrepareParamSQL($idSoft2[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft2', array('id' => $idSoft2[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond2[$i],
														'asunto' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Lista el sistema de pesca (aparejo) x embarcacion ya agregado
		$idSoft3 = (is_array($idSoft3)) ? $idSoft3 : array();
		if($idSoftNew3) array_push($idSoft3, $idSoftNew3);
		if($idSoft3&&count($idSoft3)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft3);$i++){
				if(!empty($idSoft3[$i])&&!is_null($idSoft3[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id_apa, nombre_apa
				  						FROM user_dnepp.aparejo 
										where id_apa=%d",
									  $this->PrepareParamSQL($idSoft3[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft3', array('id' => $idSoft3[$i],
														'desc' =>ucwords($row[1])
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddEmbarcacionNac.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function AgregaEmbarcacionNac($matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco,
								$eslora,$manga,$puntual,$capbod,$capbodTM,$capbod3p,$capbod15p,$transmisor,$marcaMotor,$modeloMotor,
								$serieMotor,$potenciaMotor,$tipCombustible,$statusPerPesca,$statusPerZarpe,$literal,$arqb_emb,$arqn_emb){

		global $especie,$destino;
		global $bSoftware,$idSoftNew,$idSoft,$idSoftNew2,$idSoft2,$bSoftware2,$idCond2,$idCondNew2,$idSoft3,$idSoftNew3,$resolucion;		
		// Comprueba Valores	
		//if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		//if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		//if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaEmbarcacionNac($matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco,
								$eslora,$manga,$puntual,$capbod,$capbodTM,$capbod3p,$capbod15p,$transmisor,$marcaMotor,$modeloMotor,
								$serieMotor,$potenciaMotor,$tipCombustible,$statusPerPesca,$statusPerZarpe,$literal,$arqb_emb,$arqn_emb,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE sp_insEmb_intra '%s','%s','%s',%s,%s,%s,%s,%d,%d,%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
							  $this->PrepareParamSQL($matriculaEmb),
							  $this->PrepareParamSQL($nombreEmb),
							  $this->PrepareParamSQL($puerto),
							  $this->PrepareParamSQL($regimen),
							  $this->PrepareParamSQL($tipPreservacion),
							  $this->PrepareParamSQL($casco),
							  $this->PrepareParamSQL($eslora),
							  $this->PrepareParamSQL($manga),
							  $this->PrepareParamSQL($puntual),
							  $this->PrepareParamSQL($transmisor),
							  $this->PrepareParamSQL($arqb_emb),
							  $this->PrepareParamSQL($arqn_emb),
							  $this->PrepareParamSQL($capbod),
							  $this->PrepareParamSQL($capbodTM),
							  $marcaMotor,
							  $modeloMotor,
							  $serieMotor,
							  $potenciaMotor,
							  $tipCombustible,
							  $literal,
							  $statusPerPesca,
							  $statusPerZarpe,
							  $_SESSION['cod_usuario']);
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
					$idEmb = $row[1];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
			if(!$RETVAL){
				//if($indTrans=='Y'){

					for($i=0;$i<count($idSoft2);$i++){
					//$idCond2
						$sql_SP = sprintf("EXECUTE sp_asignaEmbxEspxDest_intra %d,%d,%d",
										  $idEmb,
										  $this->PrepareParamSQL($idSoft2[$i]),
										  $this->PrepareParamSQL($idCond2[$i])										  
										  );
						// echo $sql_SP;
						$rsSoft = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rsSoft){
							$RETVAL=1;
							break;
						}else{
							if($softData = $rsSoft->FetchRow()){
								$RETVAL = $softData[0];
								//$idParte = $softData[1];
							}else{
								$RETVAL = 1;
								break;
							}
							$rsSoft->Close();
						}
						unset($rsSoft);					
					}



                    // Si la transferencia es por cambio, retira las partes correspondientes            
					if(!$RETVAL){
						//if($indCambio){
						for($i=0;$i<count($idSoft3);$i++){
						//$idCond2
							$sql_SP = sprintf("EXECUTE sp_asignaEmbxApar_intra %d,%d",
											  $idEmb,
											  $this->PrepareParamSQL($idSoft3[$i])
											  );
							// echo $sql_SP;
							$rsSoft = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rsSoft){
								$RETVAL=1;
								break;
							}else{
								if($softData = $rsSoft->FetchRow()){
									$RETVAL = $softData[0];
									//$idParte = $softData[1];
								}else{
									$RETVAL = 1;
									break;
								}
								$rsSoft->Close();
							}
							unset($rsSoft);					
						}
						//}
					}
					// Registra la Transferencia en el HelpDesk
					if(!$RETVAL){
						//if($indCambio){
						for($i=0;$i<count($idSoft);$i++){
						//$idCond2
							$sql_SP = sprintf("EXECUTE sp_asignaEmbxArm_intra %d,%d,%d",
											  $idEmb,
											  $this->PrepareParamSQL($idSoft[$i]),
											  $resolucion
											  );
							// echo $sql_SP;
							$rsSoft = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rsSoft){
								$RETVAL=1;
								break;
							}else{
								if($softData = $rsSoft->FetchRow()){
									$RETVAL = $softData[0];
									//$idParte = $softData[1];
								}else{
									$RETVAL = 1;
									break;
								}
								$rsSoft->Close();
							}
							unset($rsSoft);					
						}
						//}
					}
				//}
			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaEmbarcacionNac($id,$opcion,$matriculaEmb=NULL,$nombreEmb=NULL,$RZoRUC=NULL,$BuscaRZ=NULL,$RazonSocial=NULL,$puerto=NULL,$sistPesca=NULL,$regimen=NULL,$tipPreservacion=NULL,$casco=NULL,
								$eslora=NULL,$manga=NULL,$puntual=NULL,$capbod=NULL,$capbodTM=NULL,$capbod3p=NULL,$capbod15p=NULL,$transmisor=NULL,$marcaMotor=NULL,$modeloMotor=NULL,
								$serieMotor=NULL,$potenciaMotor=NULL,$tipCombustible=NULL,$statusPerPesca=NULL,$statusPerZarpe=NULL,$literal=NULL,$arqb_emb=NULL,$arqn_emb=NULL,$reLoad=NULL,
								$errors=false){
		global $especie,$destino;
		global $bSoftware,$idSoftNew,$idSoft,$idSoftNew2,$idSoft2,$bSoftware2,$idCond2,$idCondNew2,$idSoft3,$idSoftNew3;
		global $tipResolCambNombre,$nroResolCambNombre,$BuscaResolCambNombre,$resolucionCambNombre,$tipResolCambTit,$nroResolCambTit,$BuscaResolCambTit,$resolucionCambTit;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}		
		
		// Genera Objeto HTML
		$html = new Smarty;		

		//Trae los datos de la embarcaci�n
		if(!$reLoad){		
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql=sprintf("EXEC sp_lista_datos_embarcacion %d",$id);	  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
					$nombreEmb=$rs->fields[1];
					$tipPreservacion=$rs->fields[2];
					$matriculaEmb=$rs->fields[3];
					$literal=$rs->fields[4];
					$capbod=$rs->fields[6];
					$regimen=$rs->fields[7];
					$eslora=$rs->fields[10];
					$manga=$rs->fields[14];
					$puntual=$rs->fields[15];
					$arqb_emb=$rs->fields[17];
					$arqn_emb=$rs->fields[18];
					$casco=$rs->fields[21];
					$marcaMotor=$rs->fields[22];
					$modeloMotor=$rs->fields[23];
					$serieMotor=$rs->fields[24];
					$potenciaMotor=$rs->fields[25];
					$tipCombustible=$rs->fields[27];
					$statusPerPesca=$rs->fields[28];
					$transmisor=$rs->fields[30];
					$statusPerZarpe=$rs->fields[31];

				$rs->Close();
			}
			unset($rs);
			
			// Obtiene los Armadores de la Embarcaci�n
			$sql_SP = sprintf("EXECUTE sp_listArmadoresEmbarcacion_intra %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$html->append('soft', array('id' => $row[0],
												'desc' => ucwords($row[1]),
												'asunto' => ucwords($row[2])
												));
				$rs->Close();
			}
			unset($rs);
			
			// Obtiene los Aparejos (Sistemas de Pesca) de la Embarcaci�n
			$sql_SP = sprintf("EXECUTE sp_listAparejoEmbarcacion_intra %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$html->append('soft3', array('id' => $row[0],
												'desc' => ucwords($row[1])
												));
				$rs->Close();
			}
			unset($rs);
			
			// Obtiene los DestinoxEspeciexLiteral de la Embarcaci�n
			$sql_SP = sprintf("EXECUTE sp_listDestinoxEspeciexLiteral_intra %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				while($row = $rs->FetchRow())
					$html->append('soft2', array('id' => $row[0],
												'desc' => ucwords($row[1]),
												'cond' => $row[2]
												));
				$rs->Close();
			}
			unset($rs);			
			
		}				

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyAtention';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('matriculaEmb',$matriculaEmb);
		$html->assign_by_ref('nombreEmb',$nombreEmb);
		$html->assign_by_ref('eslora',$eslora);
		$html->assign_by_ref('manga',$manga);
		$html->assign_by_ref('puntual',$puntual);
		$html->assign_by_ref('capbod',$capbod);
		$html->assign_by_ref('capbodTM',$capbodTM);
		$html->assign_by_ref('capbod3p',$capbod3p);
		$html->assign_by_ref('capbod15p',$capbod15p);
		$html->assign_by_ref('transmisor',$transmisor);
		$html->assign_by_ref('marcaMotor',$marcaMotor);
		$html->assign_by_ref('modeloMotor',$modeloMotor);
		$html->assign_by_ref('serieMotor',$serieMotor);
		$html->assign_by_ref('potenciaMotor',$potenciaMotor);
		$html->assign_by_ref('arqb_emb',$arqb_emb);
		$html->assign_by_ref('arqn_emb',$arqn_emb);
		
		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('idSoft',$idSoft);
		$html->assign_by_ref('idSoft2',$idSoft2);
		$html->assign_by_ref('idSoft3',$idSoft3);
		$html->assign_by_ref('error',$error);
		
		$html->assign_by_ref('reLoad',$reLoad);
		$html->assign_by_ref('opcion',$opcion);
		
		$html->assign_by_ref('nroResolCambNombre',$nroResolCambNombre);
		$html->assign_by_ref('nroResolCambTit',$nroResolCambTit);
		
		if((!$BuscaResolCambNombre&&$nroResolCambNombre!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id, substring(nro_resol,1,50) ".
				  "FROM db_tramite_documentario.dbo.resolucion ".
				  "WHERE (Upper(nro_resol) like Upper('%$nroResolCambNombre%') ) ".
				  "AND id_tipo_resolucion=$tipResolCambNombre ".
				  "AND coddep not in (25,8,22,23,41,38,26) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selResolucionCambNombre',$this->ObjFrmSelect($sql_st, $resolucionCambNombre, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		

		// Contenido Select del Tipo de Resoluci�n
		$sql_st = "SELECT id, Upper(descripCION) ".
				  "FROM DB_TRAMITE_DOCUMENTARIO.dbo.TIPO_RESOLUCION ".
				  "WHERE ID IN (1,2,3,4) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResolCambNombre',$this->ObjFrmSelect($sql_st, $tipResolCambNombre, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		if((!$BuscaResolCambTit&&$nroResolCambTit!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id, substring(nro_resol,1,50) ".
				  "FROM db_tramite_documentario.dbo.resolucion ".
				  "WHERE (Upper(nro_resol) like Upper('%$nroResolCambTit%') ) ".
				  "AND id_tipo_resolucion=$tipResolCambTit ".
				  "AND coddep not in (25,8,22,23,41,38,26) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selResolucionCambTit',$this->ObjFrmSelect($sql_st, $resolucionCambTit, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		

		// Contenido Select del Tipo de Resoluci�n
		$sql_st = "SELECT id, Upper(descripCION) ".
				  "FROM DB_TRAMITE_DOCUMENTARIO.dbo.TIPO_RESOLUCION ".
				  "WHERE ID IN (1,2,3,4) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoResolCambTit',$this->ObjFrmSelect($sql_st, $tipResolCambTit, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);		

		// Contenido Select del Puerto
		$sql_st = "SELECT codigo, lower(descripcion) ".
				  "FROM DB_GENERAL.DBO.PUERTOS ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPuerto',$this->ObjFrmSelect($sql_st, $puerto, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Sistema de Pesca (Aparejo)
		$sql_st = "SELECT id_apa, lower(nombre_apa) ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selSistPesca',$this->ObjFrmSelect($sql_st, $sistPesca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Regimen
		$sql_st = "SELECT id_regimen, lower(desc_regimen) ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $regimen, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Tipo de preservaci�n
		$sql_st = "SELECT id_tpres, lower(desc_tpres) ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipPreservacion',$this->ObjFrmSelect($sql_st, $tipPreservacion, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Casco
		$sql_st = "SELECT id_casco, lower(desc_casco) ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $casco, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);

		// Contenido Select del Tipo de combustible
		$sql_st = "SELECT id_tcomb, lower(desc_tcomb) ".
				  "FROM user_dnepp.tipocombustible ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipCombustible',$this->ObjFrmSelect($sql_st, $tipCombustible, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Estado de Permiso de pesca
		$sql_st = "SELECT id_estper, lower(desc_estper) ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selStatusPerPesca',$this->ObjFrmSelect($sql_st, $statusPerPesca, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Estado de Permiso de zarpe
		$sql_st = "SELECT estadozarpe_emb, lower(desc_estzarp) ".
				  "FROM user_dnepp.estadozarpe ".
				  "ORDER BY 2";
		$html->assign_by_ref('selStatusPerZarpe',$this->ObjFrmSelect($sql_st, $statusPerZarpe, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Literal
		$sql_st = "SELECT id_literal, lower(desc_literal+' '+detalle_literal) ".
				  "FROM user_dnepp.literal ".
				  "ORDER BY 2";
		$html->assign_by_ref('selLiteral',$this->ObjFrmSelect($sql_st, $literal, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select de la especie x Literal
		$sql_st = sprintf("SELECT e.codigo, lower(e.descripcion) 
				  FROM db_general.dbo.especies e,user_dnepp.literalxespecie le 
				  WHERE e.codigo=le.id_esp 
				  AND le.id_literal=%d 
				  ORDER BY 2",($literal)? $literal : "xx");
		$html->assign_by_ref('selEspecie',$this->ObjFrmSelect($sql_st, $especie, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);
		
		// Contenido Select del Destino
		$sql_st = "SELECT id_destino, lower(desc_destino) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $destino, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);				
		
		if((!$BuscaRZ&&$RZoRUC!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id_arm, substring(razonsocial_arm,1,50) ".
				  "FROM user_dnepp.armador ".
				  "WHERE (Upper(razonsocial_arm) like Upper('%$RZoRUC%') )".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		// Lista el Armador ya agregado
		$idSoft = (is_array($idSoft)) ? $idSoft : array();
		$idCond = (is_array($idCond)) ? $idCond : array();
		if($idSoftNew) array_push($idSoft, $idSoftNew);
		if($idCondNew) array_push($idCond, $idCondNew);
		if($idSoft&&count($idSoft)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft);$i++){
				if(!empty($idSoft[$i])&&!is_null($idSoft[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id_arm,razonsocial_arm,domicilio_arm
				  						FROM user_dnepp.armador 
										where id_arm=%d",
									  $this->PrepareParamSQL($idSoft[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft', array('id' => $idSoft[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond[$i],
														'asunto' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Lista la especie x literal ya agregado
		$idSoft2 = (is_array($idSoft2)) ? $idSoft2 : array();
		$idCond2 = (is_array($idCond2)) ? $idCond2 : array();
		if($idSoftNew2) array_push($idSoft2, $idSoftNew2);
		if($idCondNew2) array_push($idCond2, $idCondNew2);
		if($idSoft2&&count($idSoft2)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft2);$i++){
				if(!empty($idSoft2[$i])&&!is_null($idSoft2[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT codigo,descripcion
				  						FROM db_general.dbo.especies 
										where codigo=%d",
									  $this->PrepareParamSQL($idSoft2[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft2', array('id' => $idSoft2[$i],
														'idD' => ucwords($row[0]),
														'desc' =>ucwords($row[1]),
														'cond' => $idCond2[$i],
														'asunto' =>$row[2]
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}		

		// Lista el sistema de pesca (aparejo) x embarcacion ya agregado
		$idSoft3 = (is_array($idSoft3)) ? $idSoft3 : array();
		if($idSoftNew3) array_push($idSoft3, $idSoftNew3);
		if($idSoft3&&count($idSoft3)>0){
			$this->abreConnDB();
			for($i=0;$i<count($idSoft3);$i++){
				if(!empty($idSoft3[$i])&&!is_null($idSoft3[$i])){
					/**/
					// Obtiene los Datos de c/Dispositivo Agregado
					$sql_SP = sprintf("SELECT id_apa, nombre_apa
				  						FROM user_dnepp.aparejo 
										where id_apa=%d",
									  $this->PrepareParamSQL($idSoft3[$i]));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						if($row = $rs->FetchRow())
							$html->append('soft3', array('id' => $idSoft3[$i],
														'desc' =>ucwords($row[1])
														));
						$rs->Close();
					}
					unset($rs);
					/**/
				}
			}
		}
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmModifyEmbarcacionNac.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function ModificaEmbarcacionNac($id,$opcion,$matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco,
									$eslora,$manga,$puntual,$capbod,$capbodTM,$capbod3p,$capbod15p,$transmisor,$marcaMotor,$modeloMotor,
									$serieMotor,$potenciaMotor,$tipCombustible,$statusPerPesca,$statusPerZarpe,$literal,$arqb_emb,$arqn_emb,
									$especie,$destino,$bSoftware,$idSoftNew,$idSoft,$idSoftNew2,$idSoft2,$bSoftware2,$idCond2,$idCondNew2,
									$idSoft3,$idSoftNew3,$tipResolCambNombre,$nroResolCambNombre,$BuscaResolCambNombre,$resolucionCambNombre,
									$tipResolCambTit,$nroResolCambTit,$BuscaResolCambTit,$resolucionCambTit
								){

		// Comprueba Valores	
		//if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		//if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		//if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaEmbarcacionNac($id,$opcion,$matriculaEmb,$nombreEmb,$RZoRUC,$BuscaRZ,$RazonSocial,$puerto,$sistPesca,$regimen,$tipPreservacion,$casco,
												$eslora,$manga,$puntual,$capbod,$capbodTM,$capbod3p,$capbod15p,$transmisor,$marcaMotor,$modeloMotor,
												$serieMotor,$potenciaMotor,$tipCombustible,$statusPerPesca,$statusPerZarpe,$literal,$arqb_emb,$arqn_emb,1,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$sql_SP = sprintf("EXECUTE sp_modEmb_intra %d,%d,'%s','%s','%s',%s,%s,%s,%s,%d,%d,%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'",
							  $id,
							  $opcion,
							  $this->PrepareParamSQL($matriculaEmb),
							  $this->PrepareParamSQL($nombreEmb),
							  $this->PrepareParamSQL($puerto),
							  $this->PrepareParamSQL($regimen),
							  $this->PrepareParamSQL($tipPreservacion),
							  $this->PrepareParamSQL($casco),
							  $this->PrepareParamSQL($eslora),
							  $this->PrepareParamSQL($manga),
							  $this->PrepareParamSQL($puntual),
							  $this->PrepareParamSQL($transmisor),
							  $this->PrepareParamSQL($arqb_emb),
							  $this->PrepareParamSQL($arqn_emb),
							  $this->PrepareParamSQL($capbod),
							  $this->PrepareParamSQL($capbodTM),
							  $marcaMotor,
							  $modeloMotor,
							  $serieMotor,
							  $potenciaMotor,
							  $tipCombustible,
							  $literal,
							  $statusPerPesca,
							  $statusPerZarpe,
							  $_SESSION['cod_usuario']);
							  
			 //echo $sql_SP."xx";
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
					//$idEmb = $row[1];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);
			
			if(!$RETVAL){
				//if($indTrans=='Y'){

					if($opcion==1){//Se puede modificar cualquier dato de la embarcaci�n
						// Borra los Especie x Destino x Embarcaci�n si los hubiese
						$sql_SP = sprintf("EXECUTE sp_delEspxDestEmb_intra %d",
										  $this->PrepareParamSQL($id));
						// echo $sql_SP;
						$rs = & $this->conn->Execute($sql_SP);
						unset($sql_SP);
						if (!$rs)
							$RETVAL=1;
						else
							$rs->Close();
						unset($rs);					
	
						for($i=0;$i<count($idSoft2);$i++){
						//$idCond2
							$sql_SP = sprintf("EXECUTE sp_asignaEmbxEspxDest_intra %d,%d,%d",
											  $id,
											  $this->PrepareParamSQL($idSoft2[$i]),
											  $this->PrepareParamSQL($idCond2[$i])										  
											  );
							// echo $sql_SP;
							$rsSoft = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rsSoft){
								$RETVAL=1;
								break;
							}else{
								if($softData = $rsSoft->FetchRow()){
									$RETVAL = $softData[0];
									//$idParte = $softData[1];
								}else{
									$RETVAL = 1;
									break;
								}
								$rsSoft->Close();
							}
							unset($rsSoft);					
						}//fin del for($i=0;$i<count($idSoft2);$i++)
						
						
						// Si la transferencia es por cambio, retira las partes correspondientes            
						if(!$RETVAL){
							$sql_SP = sprintf("EXECUTE sp_delAparEmb_intra %d",
											  $this->PrepareParamSQL($id));
							// echo $sql_SP;
							$rs = & $this->conn->Execute($sql_SP);
							unset($sql_SP);
							if (!$rs)
								$RETVAL=1;
							else
								$rs->Close();
							unset($rs);					
						
							//if($indCambio){
							for($i=0;$i<count($idSoft3);$i++){
							//$idCond2
								$sql_SP = sprintf("EXECUTE sp_asignaEmbxApar_intra %d,%d",
												  $id,
												  $this->PrepareParamSQL($idSoft3[$i])
												  );
								// echo $sql_SP;
								$rsSoft = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rsSoft){
									$RETVAL=1;
									break;
								}else{
									if($softData = $rsSoft->FetchRow()){
										$RETVAL = $softData[0];
										//$idParte = $softData[1];
									}else{
										$RETVAL = 1;
										break;
									}
									$rsSoft->Close();
								}
								unset($rsSoft);					
							}//fin del for($i=0;$i<count($idSoft3);$i++)

						}//fin del if(!$RETVAL)						
						
					}//fin del if($opcion==1)

					
					if($opcion==2){//Cambio de titular de permiso de pesca
					// Registra la Transferencia en el HelpDesk
						if(!$RETVAL){
						
								$sql_SP = sprintf("EXECUTE sp_deshabilitaEmbxArm_intra %d",
												  $this->PrepareParamSQL($id));
								// echo $sql_SP;
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else
									$rs->Close();
								unset($rs);
						
							//if($indCambio){
							for($i=0;$i<count($idSoft);$i++){
							//$idCond2
								$sql_SP = sprintf("EXECUTE sp_asignaEmbxArm_intra2 %d,%d,%d",
												  $id,
												  $this->PrepareParamSQL($idSoft[$i]),
												  $resolucionCambTit
												  );
								// echo $sql_SP;
								$rsSoft = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rsSoft){
									$RETVAL=1;
									break;
								}else{
									if($softData = $rsSoft->FetchRow()){
										$RETVAL = $softData[0];
										//$idParte = $softData[1];
									}else{
										$RETVAL = 1;
										break;
									}
									$rsSoft->Close();
								}
								unset($rsSoft);					
							}
							
						}//fin del if(!$RETVAL)
					}//fin del if($opcion==2)
				//}


					if($opcion==1){//Elimina los registros que est�n activos para posteriormente eliminarlos
					// Registra la Transferencia en el HelpDesk
						if(!$RETVAL){
						
								$sql_SP = sprintf("EXECUTE sp_delActivosEmbxArm_intra %d",
												  $this->PrepareParamSQL($id));
								// echo $sql_SP;
								$rs = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rs)
									$RETVAL=1;
								else
									$rs->Close();
								unset($rs);
						
							//if($indCambio){
							for($i=0;$i<count($idSoft);$i++){
							//$idCond2
								$sql_SP = sprintf("EXECUTE sp_asignaEmbxArm_intra2 %d,%d,%d",
												  $id,
												  $this->PrepareParamSQL($idSoft[$i]),
												  $resolucionCambTit
												  );
								// echo $sql_SP;
								$rsSoft = & $this->conn->Execute($sql_SP);
								unset($sql_SP);
								if (!$rsSoft){
									$RETVAL=1;
									break;
								}else{
									if($softData = $rsSoft->FetchRow()){
										$RETVAL = $softData[0];
										//$idParte = $softData[1];
									}else{
										$RETVAL = 1;
										break;
									}
									$rsSoft->Close();
								}
								unset($rsSoft);					
							}
							
						}//fin del if(!$RETVAL)
					}//fin del if($opcion==1)


			}
			
			if($RETVAL)
				$this->conn->RollbackTrans(); 				
			else
				$this->conn->CommitTrans();

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}	
	
	function FormBuscaArmador($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarArm';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display('dnepp/armadores/search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function ObtieneEmbarcacionesArmador($id,$sep=' - '){
		// Obtiene los Armadores de la Embarcacion
		$sql_SP = sprintf("EXECUTE sp_busca_embarcacion_armador %d",$id);
		$rsArm = & $this->conn->Execute($sql_SP);
		// echo $sql_SP;
		unset($sql_SP);
		if(!$rsArm)
			print $this->conn->ErrorMsg();
		else{
			while($embArm = $rsArm->FetchRow())
				$strEmbArm[] = $embArm[0];
			$rsArm->Close();
		}
		unset($rsArm);
		
		return (isset($strEmbArm)) ? implode($sep,$strEmbArm) : false;
	}	
	
	function BuscaArmador($page,$tipBusqueda,$desNombre,$codDepa,$codProv,$codDist){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscarArm';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		//Contador de caracteres
		$cont=strlen($desNombre);
		$cont2=strlen($tipBusqueda);
		//echo "el contador es".$cont;
		/**/
		if($cont>10||($cont2>5)){
		 $this->MuestraIndex();
		 exit;
		}/**/		
		
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaArmador($page,$tipBusqueda,$desNombre,$codDepa,$codProv,$codDist,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none'){
			$condConsulta[] = "ar.id_dep={$codDepa}";
		}
		if(!empty($codProv)&&!is_null($codProv)&&$codProv!='none'){
			$condConsulta[] = "ar.id_prov={$codProv}";
		}
		if(!empty($codDist)&&!is_null($codDist)&&$codDist!='none'){
			$condConsulta[] = "ar.id_dist={$codDist}";
		}
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
				
		$sql_SP = sprintf("EXECUTE sp_busca_id_armador %d,%s,'%s','%s',0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where));

		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busca_id_armador %d,%s,'%s','%s',%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
								$this->PrepareParamSQL($tables),
								$this->PrepareParamSQL($where),
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($embID = $rsId->FetchRow()){
					// Obtiene los Armadores de la Embarcacion
					$strEmbArm = $this->ObtieneEmbarcacionesArmador($embID[0]);
					$strEmbArm = (strlen($strEmbArm)>60) ? substr($strEmbArm,0,57).'...' : $strEmbArm;
					/*// Obtiene los Aparejos de la Embarcacion
					$strEmbApa = $this->ObtieneAparejosEmbarcacion($embID[0]);
					$strEmbApa = (strlen($strEmbApa)>60) ? substr($strEmbApa,0,57).'...' : $strEmbApa;*/
					// Obtiene Todos los Datos de la Embarcacion
					$sql_SP = sprintf("EXECUTE sp_busca_datos_armador %d",$embID[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($embData = $rsData->FetchRow())
							$html->append('arrArm', array('id' => $embData[0],
														  'depa' => $embData[1],
														  'prov' => $embData[2],
														  'embarcacion' => ($strEmbArm) ? ucwords(strtolower($strEmbArm)) : 'No tiene',
														  //'aparejo' => ($strEmbApa) ? ucwords(strtolower($strEmbApa)) : 'No tiene',
														  'dist' => ucwords($embData[3]),
														  'ruc' => $embData[4],
														  'desc' => $embData[5],
														  'domi' => $embData[6],
														  'telef1' => $embData[8],
														  'telef2' => $embData[9],
														  'email1' => $embData[12],
														  'repLegal' => ucfirst($embData[13]),
														  'zarpe' => ucfirst($embData[16]),
															'constancia' => ucfirst($embData[17])
														  ));
						$rsData->Close();
					}
					unset($rsData);
					unset($strEmbArm);
					unset($strEmbApa);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV2');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign('datos',array('desNombre'=>$desNombre,
									'tipBusqueda'=>$tipBusqueda,
									'codDepa'=>$codDepa,
									'codProv'=>$codProv,
									'codDist'=>$codDist));
		
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_ARMADOR], true));

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/armadores/searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function CreaCSVBuscaArmador($tipBusqueda,$desNombre,$codDepa,$codProv,$codDist){
		
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($codDepa)&&!is_null($codDepa)&&$codDepa!='none'){
			$condConsulta[] = "ar.id_dep={$codDepa}";
		}
		if(!empty($codProv)&&!is_null($codProv)&&$codProv!='none'){
			$condConsulta[] = "ar.id_prov={$codProv}";
		}
		if(!empty($codDist)&&!is_null($codDist)&&$codDist!='none'){
			$condConsulta[] = "ar.id_dist={$codDist}";
		}
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el Store Procedure que lista todos los registros de la Busqueda
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busca_id_armador %d,%s,'%s','%s',%d,%d",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where),
							1,
							0);
		//echo $sql_SP;

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			// Genera HTML
			$html = new Smarty;
			
			while($armID = $rsId->FetchRow()){

					//$embEsp[] = $this->PrepareParamSQL($this->ObtieneDestinoEspecieEmbarcacion($embID[0],$arrIdDest[$i],"\n"),'"');
					$strEmbArm = $this->ObtieneEmbarcacionesArmador($armID[0]);
					$strEmbArm = (strlen($strEmbArm)>60) ? substr($strEmbArm,0,57).'...' : $strEmbArm;					

				// Consulta y Agrega al Template HTML los Destinos Validos para toda embarcacion
				$sql_SP = sprintf("EXECUTE sp_busca_datos_armador %d",
									$armID[0]);
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					if($row = $rs->FetchRow())
						// Agraga los datos de la Embarcacion al Template HTML
						$html->append('arrArm',array('id' => $row[0],
														  'depa' => ($row[1]) ? $row[1] : 'No especificado',
														  'prov' => ($row[2]) ? $row[2] : 'No especificado',
														  'embarcacion' => ($strEmbArm) ? ucwords(strtolower($strEmbArm)) : 'No tiene',
														  'dist' => ($row[3]) ? ucwords($row[3]) : 'No especificado',
														  'ruc' => ($row[4]) ? $row[4] : 'No tiene',
														  'desc' => $row[5],
														  'domi' => ($row[6]) ? $row[6] : 'No especificado',
														  'telef1' => $row[8],
														  'telef2' => $row[9],
														  'email1' => $row[12],
														  'repLegal' => ucfirst($row[13]),
														  'zarpe' => ucfirst($row[16]),
														  'constancia' => ucfirst($row[17])
															));
					unset($embEsp);
					$rs->Close();
				}
				unset($rs);

			}
			$rsId->Close();
			
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=consulta2-".mktime().".csv");
			$html->display('dnepp/embarcacionnac/createCSV2.tpl.php');
			exit;
			
		}
		unset($rsId);		
	}	
	
	function FormAgregaArmador($tipPersona=NULL,$tipIdent=NULL,$ruc=NULL,$bBusca=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,
								$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$tipIdentRepLegal=NULL,$nroRepLegal=NULL,$observaciones=NULL,$telefono2=NULL,
								$fax2=NULL,$mail2=NULL,$errors=false){

		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		if($bBusca==1){
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql="select count(*)
			      from db_general.dbo.persona where nro_documento='$ruc'";
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				$cuenta=$rs->fields[0];
				if($cuenta>=1){
					$msj="El n�mero ingresado ya existe en la Base de Datos<br>Ingrese otro n�mero. Gracias";
				}
			}

			/*$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi
			      from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";*/
			$sql="select nombre,substring(ubigeo,1,2),substring(ubigeo,3,2),substring(ubigeo,5,2),nomvia+' '+NUMER1+' '+NOMZON+' '+REFER1,identi,s.telef1
			      from bd_sunat.dbo.dato_principal P left join BD_SUNAT.DBO.DATO_SECUNDARIO s on p.numruc=s.numruc and s.telef1<>'-' 	
				  where p.NUMRUC='$ruc'	";	  
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					//$razonsocial = $row[0];
					$codDepa = $row[1];
					$codProv = $row[2];
					$codDist = $row[3];
					$direccion = $row[4];
					$identi= $row[5];
					$telefono= $row[6];
					if($identi=="02"){
						$razonsocial = $row[0];
						$tipPersona=2;
						$tipIdent=8;
						if($razonsocial!=""){
							$valorReadonly=1;
						}						
					}else{
						$nombres= $row[0];
						$tipPersona=1;
						$tipIdent=8;}
					
				}
				$rs->Close();
			}
			unset($rs);
		}		

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddArmador';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('telefono2',$telefono2);
		$html->assign_by_ref('fax2',$fax2);
		$html->assign_by_ref('mail2',$mail2);		
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);

		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display('dnepp/armadores/frmAddArmador.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function formateaTexto($cadena){
	
		$cadena= ereg_replace("          "," ",$cadena);
		$cadena= ereg_replace("         "," ",$cadena);
		$cadena= ereg_replace("        "," ",$cadena);
		$cadena= ereg_replace("       "," ",$cadena);
		$cadena= ereg_replace("      "," ",$cadena);
		$cadena= ereg_replace("     "," ",$cadena);
		$cadena= ereg_replace("    "," ",$cadena);
		$cadena= ereg_replace("   "," ",$cadena);
		$cadena= ereg_replace("  "," ",$cadena);
		
		return ($cadena);
	}
	
	function formateaAcentos($cadena){
	
		$cadena= ereg_replace("�","a",$cadena);
		$cadena= ereg_replace("�","e",$cadena);
		$cadena= ereg_replace("�","i",$cadena);
		$cadena= ereg_replace("�","o",$cadena);
		$cadena= ereg_replace("�","u",$cadena);
		$cadena= ereg_replace("�","A",$cadena);
		$cadena= ereg_replace("�","E",$cadena);
		$cadena= ereg_replace("�","I",$cadena);
		$cadena= ereg_replace("�","O",$cadena);
		$cadena= ereg_replace("�","U",$cadena);
		
		return ($cadena);
	}
	
	function ComprobarEmail($email){ 
		$mail_correcto = 0;//compruebo unas cosas primeras 
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
		   if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
			  //miro si tiene caracter . 
			  if (substr_count($email,".")>= 1){//obtengo la terminacion del dominio 
				 $term_dom = substr(strrchr ($email, '.'),1);//compruebo que la terminaci�n del dominio sea correcta 
				 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
								//compruebo que lo de antes del dominio sea correcto 
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
					if ($caracter_ult != "@" && $caracter_ult != "."){ 
					   $mail_correcto = 1; 
					} 
				 } 
			  } 
		   } 
		} 
		if ($mail_correcto) 
		   return true; 
		else 
		   return false; 
	}	
	
	function AgregaArmador($tipPersona,$tipIdent,$ruc,$bBusca,$codDepa,$codProv,$codDist,$razonsocial,$nombres,$apellidos,
								$direccion,$telefono,$fax,$mail,$repLegal,$tipIdentRepLegal,$nroRepLegal,$observaciones,$telefono2,
								$fax2,$mail2){
								

			if ($razonsocial && $razonsocial!="")
				$razonsocial=$this->formateaTexto($razonsocial);
			if ($nombres && $nombres!=""){
				$nombres=$this->formateaTexto($nombres);
				$nombres=$this->formateaAcentos($nombres);
			}	
			if ($apellidos && $apellidos!=""){
				$apellidos=$this->formateaTexto($apellidos);
				$apellidos=$this->formateaAcentos($apellidos);
			}	
			if ($repLegal && $repLegal!="")
				$repLegal=$this->formateaTexto($repLegal);				
				
		// Comprueba Valores
		$tamano=strlen($ruc);
		$tamanoDireccion=strlen($direccion);
		$tamanoNombre=strlen($nombres);
		$tamanoApellido=strlen($apellidos);
		$tamanoRazonSocial=strlen($razonsocial);
		$tamanoRepLegal=strlen($nroRepLegal);
		$tamanoNombreRepLegal=strlen($repLegal);
		
		
		//if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if($tipIdent==8&& $tamano!=11){ $bC10 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC<br>'; }
		if($tipIdent==1&& $tamano!=8){ $bC11 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC13 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }
		if($tamanoDireccion<10){ $bC14 = true; $this->errors .= 'Al menos 10 caracteres que debe contener la direcci�n<br>'; }
		if($nombres&&$nombres!=""&&$tipPersona==1&& $tamanoNombre<2){ $bC15 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre<br>'; }
		if($apellidos&&$apellidos!=""&&$tipPersona==1&& $tamanoApellido<2){ $bC16 = true; $this->errors .= 'Al menos 2 caracteres debe contener el apellido<br>'; }
		if($razonsocial&&$razonsocial!=""&&$tipPersona==2&& $tamanoRazonSocial<2){ $bC17 = true; $this->errors .= 'Al menos 2 caracteres debe contener la Raz�n Social<br>'; }
		if($tipIdentRepLegal>0&&!$nroRepLegal){ $bC18 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser espcificado<br>'; }
		if($tipIdentRepLegal==8&& $tamanoRepLegal!=11){ $bC19 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC del Representante Legal<br>'; }
		if($tipIdentRepLegal==1&& $tamanoRepLegal!=8){ $bC20 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI del Representante Legal<br>'; }
		if($tipIdentRepLegal>0&&!$repLegal){ $bC21 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tipIdentRepLegal<1){ $bC22 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&!$nroRepLegal){ $bC23 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tamanoNombreRepLegal<2){ $bC24 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre del Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&!$repLegal){ $bC25 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&$tipIdentRepLegal<1){ $bC26 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($tipPersona==2&&$tipIdent==8&&$bBusca!=1&&$ruc!="00000000000"){ $bC27 = true; $this->errors .= 'Debe consultar la data de SUNAT, hacer click en el checkbox.<br>'; }
		
		
		/**/
			$this->abreConnDB();
			//$this->conn->debug = true;
			if((!$apellidos&& !$nombres)&& $tipPersona==2){
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and razon_social='$razonsocial' /*and nro_documento='$ruc'*/";
			}else{
				$sql="select count(*) from db_general.dbo.persona 
					   where id_tipo_persona=$tipPersona
					   and nombres='$nombres' and apellidos='$apellidos' /*and nro_documento='$ruc'*/";
			}
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs)
					$RETVAL=1;
				else{
					$contador=$rs->fields[0];
					if($contador>=1){ $bC9 = true; $this->errors .= 'Ya existe la persona en el Directorio de CONVENIO_SITRADOC<br>'; }
				}
				
			$sql2="select count(*) from db_general.dbo.persona where nro_documento='$ruc' AND NRO_DOCUMENTO<>'00000000000' and NRO_DOCUMENTO<>'00000000'";
				$rs2 = & $this->conn->Execute($sql2);
				unset($sql2);
				if (!$rs2)
					$RETVAL=1;
				else{
					$contador2=$rs2->fields[0];
					if($contador2>=1){ $bC12 = true; $this->errors .= 'Ya existe el Nro de documento en el Directorio de CONVENIO_SITRADOC<br>'; }
				}
				
			if($tipPersona==2&&$tipIdent==8&&$bBusca==1&&$ruc!="00000000000"){
				$sql3="select count(*) from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";
				$rs3 = & $this->conn->Execute($sql3);
				unset($sql3);
				if (!$rs3)
					$RETVAL=1;
				else{
					$contador3=$rs3->fields[0];
					if($contador3==0){ $bC28 = true; $this->errors .= 'No existe el Nro de ruc en el Padr�n RUC.<br>'; }
				}				
			}	
			
						

		if($bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC9||$bC10||$bC11||$bC12||$bC13||$bC14||$bC15||$bC16||$bC17||$bC18||$bC19||$bC20||$bC21||$bC22||$bC23||$bC24||$bC25||$bC26||$bC27||$bC28){
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaArmador($tipPersona,$tipIdent,$ruc,$bBusca,$codDepa,$codProv,$codDist,$razonsocial,$nombres,$apellidos,
								$direccion,$telefono,$fax,$mail,$repLegal,$tipIdentRepLegal,$nroRepLegal,$observaciones,$telefono2,
								$fax2,$mail2,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql_SP = sprintf("EXECUTE sp_insArmador %d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,'%s',%s",
							  ($sector) ? $this->PrepareParamSQL($sector) : "1",//idSector=1: Sector Pesquer�a
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "'SIN DIRECCI�N'",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "NULL",
							  ($telefono2) ? "'".$this->PrepareParamSQL($telefono2)."'" : "NULL",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "NULL",
							  ($fax2) ? "'".$this->PrepareParamSQL($fax2)."'" : "NULL",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "NULL",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
							  ($mail2) ? "'".$this->PrepareParamSQL($mail2)."'" : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa&&$codDepa!='none') ? "'".$this->PrepareParamSQL($codDepa)."'" : "'00'",
							  ($codProv&&$codProv!='none') ? "'".$this->PrepareParamSQL($codProv)."'" : "'00'",
							  ($codDist&&$codDist!='none') ? "'".$this->PrepareParamSQL($codDist)."'" : "'00'",
							  $_SESSION['cod_usuario'],
							  "'1111111111'"
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}	
	
	function FormModificaArmador($id,$tipPersona=NULL,$tipIdent=NULL,$ruc=NULL,$codDepa=NULL,$codProv=NULL,$codDist=NULL,$razonsocial=NULL,$nombres=NULL,$apellidos=NULL,
								$direccion=NULL,$telefono=NULL,$fax=NULL,$mail=NULL,$repLegal=NULL,$tipIdentRepLegal=NULL,$nroRepLegal=NULL,$observaciones=NULL,$telefono2=NULL,
								$fax2=NULL,$mail2=NULL,$reLoad=NULL,$errors=false){
		
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}		
		
		if(!$reLoad){
		//Trae los datos del armador
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			$sql=sprintf("EXEC sp_lista_armador %d",$id);
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					//$razonsocial = $row[0];
					$codDepa = $row[1];
					$codProv = $row[2];
					$codDist = $row[3];
					$ruc = $row[4];
					$razonsocial = $row[5];
					$direccion = $row[6];
					$telefono = $row[8];
					$telefono2 = $row[9];
					$fax = $row[10];
					$fax2 = $row[11];
					$mail = $row[12];
					$repLegal = $row[13];
					$mail2 = $row[14];
					$nroRepLegal = $row[15];
					//$tipPersona=2;
					if($nroRepLegal&&$nroRepLegal!=""){
						$tipIdentRepLegal=1;
					}
					/*Se consulta la tabla persona en DB_GENERAL para sacar el id y saber si es una persona natural*/
					/*Se parte de la premisa que todo registro en la tabla armador est� contenido en la tabla persona en DB_GENERAL*/
					if($razonsocial&&$razonsocial!=""){
						$sql2=sprintf("EXEC sp_lista_persona '%s'",$razonsocial);
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$idPersona=$rs2->fields[0];
							$tipPersona=$rs2->fields[1];
							$tipIdent=$rs2->fields[2];
							if($tipPersona==1){
								$nombres=$rs2->fields[4];
								$apellidos=$rs2->fields[5];
								$ruc=$rs2->fields[6];
							}	
						}						
					}
					
				}
				$rs->Close();
			}
			unset($rs);
			}//fin del if(!$reLoad)

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyArmador';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('idPersona',$idPersona);
		$html->assign_by_ref('razonsocial',$razonsocial);
		$html->assign_by_ref('nombres',$nombres);
		$html->assign_by_ref('apellidos',$apellidos);
		$html->assign_by_ref('ruc',$ruc);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('fax',$fax);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('nroRepLegal',$nroRepLegal);
		$html->assign_by_ref('observaciones',$observaciones);
		$html->assign_by_ref('tipPersona',$tipPersona);
		$html->assign_by_ref('telefono2',$telefono2);
		$html->assign_by_ref('fax2',$fax2);
		$html->assign_by_ref('mail2',$mail2);		
		$html->assign_by_ref('rubro1',$rubro1);
		$html->assign_by_ref('rubro2',$rubro2);
		$html->assign_by_ref('rubro3',$rubro3);
		$html->assign_by_ref('rubro4',$rubro4);
		$html->assign_by_ref('rubro5',$rubro5);
		$html->assign_by_ref('rubro6',$rubro6);		
		$html->assign_by_ref('bBusca',$bBusca);
		$html->assign_by_ref('tipIdent',$tipIdent);
		$html->assign_by_ref('tipIdentRepLegal',$tipIdentRepLegal);
		$html->assign_by_ref('rubro10',$rubro10);
		$html->assign_by_ref('msj',$msj);
		$html->assign_by_ref('valorReadonly',$valorReadonly);
		$html->assign_by_ref('reLoad',$reLoad);

		// Contenido Select Tipo de Persona
		$sql_st = "SELECT id_tipo_persona, lower(descripcion) ".
				  "FROM db_general.dbo.tipo_persona ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoPersona',$this->ObjFrmSelect($sql_st, $tipPersona, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		
		// Setea Muestra de Campos del Ubigeo
		$sql_st = "SELECT codigo_departamento, departamento ".
				  "FROM db_general.dbo.departamento ".
				  "ORDER BY 1";
		$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDepa',$this->ObjFrmSelect($sql_st, $codDepa, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_provincia, Lower(provincia) ".
				  		  "FROM db_general.dbo.provincia ".
				  		  "WHERE codigo_departamento='%s' ".
				  		  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx');
		$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selProv',$this->ObjFrmSelect($sql_st, $codProv, true, true));
		unset($sql_st);
		
		$sql_st = sprintf("SELECT codigo_distrito, Lower(distrito) ".
				  		  "FROM db_general.dbo.distrito ".
				 		  "WHERE codigo_departamento='%s' and codigo_provincia='%s' ".
						  "ORDER BY 2",(!is_null($codDepa)||!empty($codDepa)) ? $codDepa : 'xx',(!is_null($codProv)||!empty($codProv)) ? $codProv : 'xx');
		$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true, array('val'=>'none','label'=>'No especificado')));
		//$html->assign('selDist',$this->ObjFrmSelect($sql_st, $codDist, true, true));
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n
		if($tipPersona==1){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}elseif($tipPersona==2){
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true));					  
		}else{
			$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
					  "FROM db_general.dbo.t_tipo_identificacion ".
					  "WHERE codigo_t_identificacion in (1,8) ".
					  "ORDER BY 2";
			$html->assign_by_ref('selTipoIdentificacion',$this->ObjFrmSelect($sql_st, $tipIdent, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		}
		unset($sql_st);
		
		// Contenido Select Tipo de Identificaci�n del Representante Legal
		$sql_st = "SELECT codigo_t_identificacion, lower(descripcion) ".
				  "FROM db_general.dbo.t_tipo_identificacion ".
				  "WHERE codigo_t_identificacion in (1,3,8) ".
				  "ORDER BY 2";
		$html->assign_by_ref('selTipoIdentRepLegal',$this->ObjFrmSelect($sql_st, $tipIdentRepLegal, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
				
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display('dnepp/armadores/frmModifyArmador.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function ModificaArmador($id,$idPersona,$tipPersona,$tipIdent,$ruc,$codDepa,$codProv,$codDist,$razonsocial,$nombres,$apellidos,
								$direccion,$telefono,$fax,$mail,$repLegal,$tipIdentRepLegal,$nroRepLegal,$observaciones,$telefono2,
								$fax2,$mail2){
								
			if ($razonsocial && $razonsocial!="")
				$razonsocial=$this->formateaTexto($razonsocial);
			if ($nombres && $nombres!=""){
				$nombres=$this->formateaTexto($nombres);
				$nombres=$this->formateaAcentos($nombres);
			}	
			if ($apellidos && $apellidos!=""){
				$apellidos=$this->formateaTexto($apellidos);
				$apellidos=$this->formateaAcentos($apellidos);
			}	
			if ($repLegal && $repLegal!="")
				$repLegal=$this->formateaTexto($repLegal);				
				
		// Comprueba Valores
		$tamano=strlen($ruc);
		$tamanoDireccion=strlen($direccion);
		$tamanoNombre=strlen($nombres);
		$tamanoApellido=strlen($apellidos);
		$tamanoRazonSocial=strlen($razonsocial);
		$tamanoRepLegal=strlen($nroRepLegal);
		$tamanoNombreRepLegal=strlen($repLegal);
		
		
		//if($sector<1){ $bC1 = true; $this->errors .= 'El Sector debe ser especificado<br>'; }
		if($tipPersona<1){ $bC2 = true; $this->errors .= 'El Tipo de Persona debe ser especificado<br>'; }
		if($tipIdent<1){ $bC3 = true; $this->errors .= 'El Tipo de Identificaci�n debe ser especificado<br>'; }
		if(!$razonsocial&&$tipPersona==2){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		if(!$nombres&&$tipPersona==1){ $bC5 = true; $this->errors .= 'El Nombre debe ser especificado<br>'; }
		if(!$apellidos&&$tipPersona==1){ $bC6 = true; $this->errors .= 'El Apellido debe ser especificado<br>'; }
		if(!$ruc){ $bC7 = true; $this->errors .= 'El n�mero de documento debe ser mayor que cero<br>'; }
		if(!$direccion){ $bC8 = true; $this->errors .= 'La Direcci�n debe ser especificada<br>'; }
		if($tipIdent==8&& $tamano!=11){ $bC10 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC<br>'; }
		if($tipIdent==1&& $tamano!=8){ $bC11 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI<br>'; }
		if((!$this->ComprobarEmail($mail))&&($mail!="")){ $bC13 = true; $this->errors .= 'El E-mail debe ser v�lido<br>'; }
		if($tamanoDireccion<10){ $bC14 = true; $this->errors .= 'Al menos 10 caracteres que debe contener la direcci�n<br>'; }
		if($nombres&&$nombres!=""&&$tipPersona==1&& $tamanoNombre<2){ $bC15 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre<br>'; }
		if($apellidos&&$apellidos!=""&&$tipPersona==1&& $tamanoApellido<2){ $bC16 = true; $this->errors .= 'Al menos 2 caracteres debe contener el apellido<br>'; }
		if($razonsocial&&$razonsocial!=""&&$tipPersona==2&& $tamanoRazonSocial<2){ $bC17 = true; $this->errors .= 'Al menos 2 caracteres debe contener la Raz�n Social<br>'; }
		if($tipIdentRepLegal>0&&!$nroRepLegal){ $bC18 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser espcificado<br>'; }
		if($tipIdentRepLegal==8&& $tamanoRepLegal!=11){ $bC19 = true; $this->errors .= 'Son 11 d�gitos que debe contener el RUC del Representante Legal<br>'; }
		if($tipIdentRepLegal==1&& $tamanoRepLegal!=8){ $bC20 = true; $this->errors .= 'Son 8 d�gitos que debe contener el DNI del Representante Legal<br>'; }
		if($tipIdentRepLegal>0&&!$repLegal){ $bC21 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tipIdentRepLegal<1){ $bC22 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&!$nroRepLegal){ $bC23 = true; $this->errors .= 'El n�mero de documento del Representante Legal debe ser especificado<br>'; }
		if($repLegal&&$tamanoNombreRepLegal<2){ $bC24 = true; $this->errors .= 'Al menos 2 caracteres debe contener el nombre del Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&!$repLegal){ $bC25 = true; $this->errors .= 'El Representante Legal debe ser especificado<br>'; }
		if($nroRepLegal&&$tipIdentRepLegal<1){ $bC26 = true; $this->errors .= 'El tipo de documento del Representante Legal debe ser especificado<br>'; }
		if($tipPersona==2&&$tipIdent==8&&$bBusca!=1&&$ruc!="00000000000"){ $bC27 = true; $this->errors .= 'Debe consultar la data de SUNAT, hacer click en el checkbox.<br>'; }
		
			$this->abreConnDB();
			//$this->conn->debug = true;
				
			if($tipPersona==2&&$tipIdent==8&&$bBusca==1&&$ruc!="00000000000"){
				$sql3="select count(*) from bd_sunat.dbo.dato_principal where NUMRUC='$ruc'";
				$rs3 = & $this->conn->Execute($sql3);
				unset($sql3);
				if (!$rs3)
					$RETVAL=1;
				else{
					$contador3=$rs3->fields[0];
					if($contador3==0){ $bC28 = true; $this->errors .= 'No existe el Nro de ruc en el Padr�n RUC.<br>'; }
				}				
			}	
			
						

		if($bC2||$bC3||$bC4||$bC5||$bC6||$bC7||$bC8||$bC10||$bC11||$bC13||$bC14||$bC15||$bC16||$bC17||$bC18||$bC19||$bC20||$bC21||$bC22||$bC23||$bC24||$bC25||$bC26||$bC27||$bC28){
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);

			$this->FormModificaArmador($id,$tipPersona,$tipIdent,$ruc,$codDepa,$codProv,$codDist,$razonsocial,$nombres,$apellidos,
								$direccion,$telefono,$fax,$mail,$repLegal,$tipIdentRepLegal,$nroRepLegal,$observaciones,$telefono2,
								$fax2,$mail2,1,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql_SP = sprintf("EXECUTE sp_modArmador %d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,'%s',%s",
							  $id,
							  $idPersona,
							  ($tipPersona) ? $this->PrepareParamSQL($tipPersona) : "NULL",
							  ($tipIdent) ? $this->PrepareParamSQL($tipIdent) : "NULL",
							  ($razonsocial) ? "'".$this->PrepareParamSQL($razonsocial)."'" : "' '",
							  ($nombres) ? "'".$this->PrepareParamSQL($nombres)."'" : "' '",
							  ($apellidos) ? "'".$this->PrepareParamSQL($apellidos)."'" : "' '",
							  ($ruc) ? "'".$this->PrepareParamSQL($ruc)."'" : "' '",
							  ($direccion) ? "'".$this->PrepareParamSQL($direccion)."'" : "'SIN DIRECCI�N'",
							  ($telefono) ? "'".$this->PrepareParamSQL($telefono)."'" : "NULL",
							  ($telefono2) ? "'".$this->PrepareParamSQL($telefono2)."'" : "NULL",							  
							  ($fax) ? "'".$this->PrepareParamSQL($fax)."'" : "NULL",
							  ($fax2) ? "'".$this->PrepareParamSQL($fax2)."'" : "NULL",							  
							  ($mail) ? "'".$this->PrepareParamSQL($mail)."'" : "NULL",
							  ($repLegal) ? "'".$this->PrepareParamSQL($repLegal)."'" : "NULL",							  
							  ($nroRepLegal) ? "'".$this->PrepareParamSQL($nroRepLegal)."'" : "NULL",
							  ($tipIdentRepLegal) ? $this->PrepareParamSQL($tipIdentRepLegal) : "NULL",
							  ($mail2) ? "'".$this->PrepareParamSQL($mail2)."'" : "NULL",
  							  ($observaciones) ? "'".$this->PrepareParamSQL($observaciones)."'" : "NULL",
							  ($codDepa&&$codDepa!='none') ? "'".$this->PrepareParamSQL($codDepa)."'" : "'00'",
							  ($codProv&&$codProv!='none') ? "'".$this->PrepareParamSQL($codProv)."'" : "'00'",
							  ($codDist&&$codDist!='none') ? "'".$this->PrepareParamSQL($codDist)."'" : "'00'",
							  $_SESSION['cod_usuario'],
							  "'1111111111'"
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->menu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormGeneraReporte($GrupoOpciones1=NULL,$armador=NULL,$embarcacion=NULL,$matricula_emb=NULL){
		global $idRegimen;
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReportes';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		
		$html->assign_by_ref('estado',$idEstado);
		
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display('dnepp/reportes/reportes.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function GeneraReporte($GrupoOpciones1,$armador,$embarcacion,$matricula_emb){
		global $idRegimen;
		$this->ListadoReportes($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen);				
	}
	
	function ListadoReportes($GrupoOpciones1,$armador,$embarcacion,$matricula_emb,$idRegimen,$print=false){
	
		//echo "matrix";
		$html = new Smarty;
		
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql_SP = "select count(dj2007.id_emb),emb.nombre_emb,EMB.MATRICULA_EMB,arm.razonsocial_arm,
					count(case when dj2007.id_mpd2007=1 then 1 end) as ENERO
					,count(case when dj2007.id_mpd2007=2 then 1 end) as FEBRERO
					,count(case when dj2007.id_mpd2007=3 then 1 end) as MARZO
					,count(case when dj2007.id_mpd2007=4 then 1 end) as ABRIL
					,count(case when dj2007.id_mpd2007=5 then 1 end) as MAYO
					,count(case when dj2007.id_mpd2007=6 then 1 end) as JUNIO
					,count(case when dj2007.id_mpd2007=7 then 1 end) as JULIO
					,count(case when dj2007.id_mpd2007=8 then 1 end) as AGOSTO
					,count(case when dj2007.id_mpd2007=9 then 1 end) as SEPTIEMBRE
					,count(case when dj2007.id_mpd2007=10 then 1 end) as OCTUBRE
					,count(case when dj2007.id_mpd2007=11 then 1 end) as NOVIEMBRE
					,count(case when dj2007.id_mpd2007=12 then 1 end) as DICIEMBRE 
					from user_dnepp.DECJURMES2007 dj2007,USER_DNEPP.embarcacionnac emb,
						USER_DNEPP.embxarm embxarm,user_dnepp.armador arm
					where
					dj2007.id_emb=emb.id_emb
					and emb.id_emb=embxarm.id_emb
					and embxarm.estado_embxarm=1
					and embxarm.id_arm=arm.id_arm ";
					
		if($idRegimen>0)
			$sql_SP.=" and emb.id_regimen=$idRegimen ";
		if($embarcacion && $embarcacion!="")
			$sql_SP.=" and emb.nombre_emb like '%{$embarcacion}%' ";
		if($matricula_emb && $matricula_emb!="")
			$sql_SP.=" and emb.matricula_emb like '%{$matricula_emb}%' ";
		if($armador && $armador!="")
			$sql_SP.=" and arm.razonsocial_arm like '%{$armador}%' ";		
					
		$sql_SP .= " group by emb.nombre_emb,EMB.MATRICULA_EMB,arm.razonsocial_arm"; 
		
		if($GrupoOpciones1==1)
			$sql_SP .= " HAVING COUNT(dj2007.ID_EMB)<12";
		else if($GrupoOpciones1==2)
			$sql_SP .= " HAVING COUNT(dj2007.ID_EMB)=12";	
					
		$sql_SP .= " order by 1 desc";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('list',array('nroMeses'=>$row[0],
										   'emb'=>$row[1],
										   'matri'=>$row[2],
										   'armador'=>$row[3],
										   'enero'=>$row[4],
										   'febrero'=>$row[5],
										   'marzo'=>$row[6],
										   'abril'=>$row[7],
										   'mayo'=>$row[8],
										   'junio'=>$row[9],
										   'julio'=>$row[10],
										   'agosto'=>$row[11],
										   'septiembre'=>$row[12],
										   'octubre'=>$row[13],
										   'noviembre'=>$row[14],
										   'diciembre'=>$row[15]
										   ));
			$rs->Close();
		}
		unset($rs);

		//$print ? $html->display('dnepp/embarcacionnac/printHistSuspPP.tpl.php') : $html->display('dnepp/embarcacionnac/showHistDuenos.tpl.php');
		$sql="select convert(varchar,getDate(),103)+' '+convert(varchar,getDate(),108)";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			$fechaGen=$rs->fields[0];
		}
		$html->assign_by_ref('fechaGen',$fechaGen);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		$html->assign_by_ref('armador',$armador);
		$html->assign_by_ref('embarcacion',$embarcacion);
		$html->assign_by_ref('matricula_emb',$matricula_emb);
		$html->assign_by_ref('idRegimen',$idRegimen);
		$html->assign_by_ref('print',$print);
		
		$html->assign_by_ref('accion',$this->arr_accion);
		
		if($print==1){
			$html->display('dnepp/reportes/printReportIncumplimiento.tpl.php');
		}elseif($print==2)	{
					$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';		
					$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/aplicativos/oad/reports';
					$filename = 'hoja1'.mktime();
					$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/reportes/reportIncumplimientoPdf.tpl.php'),true,$logo);
			
					$destination = '/institucional/aplicativos/oad/reports/' . $filename . '.pdf';
					header("Content-type: application/pdf");
					header("Location: {$destination}");
					//echo $destination;
					exit;		
		}elseif($print==3){
			// Muestra el Resultado de la Busqueda
			header("Pragma: ");
			header("Cache-Control: ");
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=documento-".mktime().".csv");
				$html->display('dnepp/reportes/viewExtendReportIncumplimiento.tpl.php');
			exit;		
		}else{
			$html->display('dnepp/reportes/reportIncumplimiento.tpl.php');
		}
	}
	
	function calculaInteres($monto,$fechaInicial,$fechaPago){
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$interes=$monto;
		/*$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<dateadd(dd,1,getDate()) 
					ORDER BY convert(datetime,fecha_tasa,103)";*/
		if($fechaPago && $fechaPago!="")
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<convert(datetime,'$fechaPago',103) 
					ORDER BY convert(datetime,fecha_tasa,103)";
		else
			$sql_SP = "SELECT TASA_ANUAL FROM db_sancion.dbo.tasa_diaria_legal
					WHERE convert(datetime,fecha_tasa,103)>=CONVERT(datetime,'$fechaInicial',103)
					and convert(datetime,fecha_tasa,103)<getDate() 
					ORDER BY convert(datetime,fecha_tasa,103)";
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row=$rs->FetchRow()){
				//$interes=$interes+$interes*($row[0]/36500);
				$interes=$interes+$interes*(pow((1+($row[0]/100)),(1/360))-1);
				//echo $interes."<br>";
			}	
			$rs->Close();
		}
		unset($rs);		
		//$interes=$monto*$tasa;
		return($interes);
	}	
	
function compara_fechas($fecha1,$fecha2)
            
 
{
            
 
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("/",$fecha1);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
            
 
              list($dia1,$mes1,$a�o1)=split("-",$fecha1);
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("/",$fecha2);
            
 
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
            
 
              list($dia2,$mes2,$a�o2)=split("-",$fecha2);
        $dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
        return ($dif);                         
            
 
}	
	
	function FormRegistraDeclaracionJuradaMensual($anyo=NULL,$RZoRUC=NULL,$BuscaRZ=NULL,$RazonSocial=NULL,$razonSocial=NULL,$direccion=NULL,$RUC=NULL,$mail=NULL,$repLegal=NULL,$docRepLegal=NULL,$idEmb=NULL,$chi=NULL,$chdB=NULL,$chdC=NULL,$cMonto=NULL,$radiobutton=NULL,$search=false){
		global $valorEnvio,$registro,$valorGrab;
		
		$valorEnvio=($_POST['valorEnvio']) ? $_POST['valorEnvio'] : $_GET['valorEnvio'];
		
		echo "x".$valorEnvio."x";
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmRegDecJur';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('RZoRUC',$RZoRUC);
		$html->assign_by_ref('anyo',$anyo);
		$html->assign_by_ref('radiobutton',$radiobutton);
		$html->assign_by_ref('valorEnvio',$valorEnvio);
		
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		if((!$BuscaRZ&&$RZoRUC!="")){
		// Contenido Select del Asunto para DINSECOVI
		$this->abreConnDB();
		//$this->conn->debug = true;		
		$sql_st = "SELECT id_arm, substring(razonsocial_arm,1,50) ".
				  "FROM user_dnepp.armador ".
				  "WHERE (Upper(razonsocial_arm) like Upper('%$RZoRUC%') OR Upper(ruc_arm) like Upper('%$RZoRUC%') )".
				  "ORDER BY 2";
		$html->assign_by_ref('selRazonSocial',$this->ObjFrmSelect($sql_st, $RazonSocial, true, true, array('val'=>'none','label'=>'Seleccione una Opci�n')));
		unset($sql_st);
		}		
		
		$html->assign_by_ref('RazonSocial',$RazonSocial);
		
		$html->assign_by_ref('chi',$chi);
		$html->assign_by_ref('chdB',$chdB);
		$html->assign_by_ref('chdC',$chdC);
		$html->assign_by_ref('registro',$registro);
		$html->assign_by_ref('idEmb',$idEmb);
		$html->assign_by_ref('valorGrab',$valorGrab);
		
		switch($radiobutton){
			case "01":
				$fechaSinInteres="06/02/2008";
				$fechaConInteres="29/02/2008";
				$fechaInicialInteres="01/03/2008";
				
				$fecha1SGS="01/01/2008";
				$fecha2SGS="31/01/2008";
				//echo $this->compara_fechas(date("d/m/Y"),$fechaSinInteres);
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "02":
				$fechaSinInteres="06/03/2008";
				$fechaConInteres="31/03/2008";
				$fechaInicialInteres="01/04/2008";
				
				$fecha1SGS="01/02/2008";
				$fecha2SGS="29/02/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "03":
				$fechaSinInteres="06/04/2008";
				$fechaConInteres="30/04/2008";
				$fechaInicialInteres="01/05/2008";
				
				$fecha1SGS="01/03/2008";
				$fecha2SGS="31/03/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "04":
				$fechaSinInteres="06/05/2008";
				$fechaConInteres="31/05/2008";
				$fechaInicialInteres="01/06/2008";
				
				$fecha1SGS="01/04/2008";
				$fecha2SGS="30/04/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "05":
				$fechaSinInteres="06/06/2008";
				$fechaConInteres="30/05/2008";
				$fechaInicialInteres="01/07/2008";
				
				$fecha1SGS="01/05/2008";
				$fecha2SGS="31/05/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "06":
				$fechaSinInteres="06/07/2008";
				$fechaConInteres="31/07/2008";
				$fechaInicialInteres="01/08/2008";
				
				$fecha1SGS="01/06/2008";
				$fecha2SGS="30/06/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "07":
				$fechaSinInteres="06/08/2008";
				$fechaConInteres="31/08/2008";
				$fechaInicialInteres="01/09/2008";
				
				$fecha1SGS="01/07/2008";
				$fecha2SGS="31/07/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "08":
				$fechaSinInteres="06/09/2008";
				$fechaConInteres="30/09/2008";
				$fechaInicialInteres="01/10/2008";
				
				$fecha1SGS="01/08/2008";
				$fecha2SGS="31/08/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "09":
				$fechaSinInteres="06/10/2008";
				$fechaConInteres="31/10/2008";
				$fechaInicialInteres="01/11/2008";
				
				$fecha1SGS="01/09/2008";
				$fecha2SGS="30/09/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "10":
				$fechaSinInteres="06/11/2008";
				$fechaConInteres="30/11/2008";
				$fechaInicialInteres="01/12/2008";
				
				$fecha1SGS="01/10/2008";
				$fecha2SGS="31/10/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "11":
				$fechaSinInteres="06/12/2008";
				$fechaConInteres="31/12/2008";
				$fechaInicialInteres="01/01/2009";
				
				$fecha1SGS="01/11/2008";
				$fecha2SGS="30/11/2008";
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;
			case "12":
				/*$fechaSinInteres="05/01/2009";
				$fechaConInteres="31/01/2008";*/
				$fechaSinInteres="06/01/2008";
				$fechaConInteres="31/01/2007";
				$fechaInicialInteres="01/02/2007";
				
				$fecha1SGS="01/12/2008";
				$fecha2SGS="31/12/2008";
				//echo date("d/m/Y",$fechaSinInteres)."-".strtotime($fechaConInteres)."-".strtotime(date("d/m/Y"));
				if($this->compara_fechas(date("d/m/Y"),$fechaSinInteres)<0){
					echo "con deducci�n";
					$deduccion=1;
				}elseif($this->compara_fechas(date("d/m/Y"),$fechaConnInteres)>0){
					echo "CON inter�s";
					$conInteres=1;
				}else{
					echo "SIN intereses";					
					$sinInteres=1;
				}
				break;				
		}
		
		if($RazonSocial>0){
			//echo $RazonSocial;
					$sql_SP = sprintf("SELECT id_arm,razonsocial_arm,domicilio_arm,ruc_arm,email1_arm,
											  repleg_arm,dociderl_arm
				  						FROM user_dnepp.armador 
										where id_arm=%d",
									  $this->PrepareParamSQL($RazonSocial));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
						
						$razonSocial=$rs->fields[1];
						$direccion=$rs->fields[2];
						$RUC=$rs->fields[3];
						$mail=$rs->fields[4];
						$repLegal=$rs->fields[5];
						$docRepLeg=$rs->fields[6];
						$rs->Close();
					}
					unset($rs);
					
					// Obtiene los Datos de c/Embarcaci�n 
					$sql_SP = sprintf("select emb.id_emb,emb.nombre_emb,emb.matricula_emb,emb.codpag_emb
										from user_dnepp.embxarm embxarm,user_dnepp.embarcacionnac emb
										where embxarm.id_arm=%d
										and embxarm.id_emb=emb.id_emb
										and embxarm.estado_embxarm=1
										and emb.codpag_emb is not null
										order by 2",
									  $this->PrepareParamSQL($RazonSocial));
					// echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs){
						print $this->conn->ErrorMsg();
						return;
					}else{
										
						$m=0;
						$montoInicial=0;
						$deducionTotal=0;
						$interesTotal=0;
						$Total=0;
						while($row = $rs->FetchRow()){
						
						/**/
						if(!$valorEnvio){
							$sql2="SELECT COD_EMB,MONTH(CONVERT(DATETIME,FECHA,103)) AS MES,SUM(CANTIDAD) AS SUMA
										FROM db_general.dbo.datos_sgs
										WHERE CONVERT(DATETIME,'$fecha1SGS',103)<=CONVERT(DATETIME,FECHA,103)
										and CONVERT(DATETIME,FECHA,103)<dateadd(dd,1,CONVERT(DATETIME,'$fecha2SGS',103))
										AND cod_emb=".$row[0]."
										GROUP BY COD_EMB,MONTH(CONVERT(DATETIME,FECHA,103))";
							$rs2 = & $this->conn->Execute($sql2);
							unset($sql2);
							if (!$rs2){
								print $this->conn->ErrorMsg();
								return;
							}else{
								$dataSgs=$rs2->fields[2];
								$chi[$m]=$dataSgs;
							}
						}							
						/**/
						
						$valorGrabado[$m]=0;
						/*Consulta de la tabla declaracion_jurada_mensual*/
							$sql4="select COUNT(*)
									from dbo.declaracion_jurada_mensual 
									where id_embarcacion=".$row[0]." ";
							$rs4 = & $this->conn->Execute($sql4);
							unset($sql4);
							if (!$rs4){
								print $this->conn->ErrorMsg();
								return;
							}else{
								$contador[$m]=$rs4->fields[0];
							}						
						
							if($contador[$m]==1){
								$sql3="select djm.captotchi_djm,djm.captotchd_jurcab_djm,djm.captotchd_otro_djm,d.num_tram_documentario
										from dbo.declaracion_jurada_mensual djm,db_tramite_documentario.dbo.documento d
										where djm.id_embarcacion=".$row[0]." and d.id_documento=djm.id_documento ";
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$chi[$m]=$rs3->fields[0];
									$chdB[$m]=$rs3->fields[1];
									$chdC[$m]=$rs3->fields[2];
									$registro[$m]=$rs3->fields[3];
									$valorGrabado[$m]=1;
								}
							}									
						/**/
							$monto=$chi[$m]*(0.25/100)*2.04+$chdB[$m]*(0.058/100)*3500+$chdC[$m]*(0.058/100)*3500;
							if($deduccion==1){
								$deducido=$monto*0.05;
								$deducionTotal=$deducionTotal+$deducido;
							}elseif($conInteres==1){
								$interes=$this->calculaInteres($monto,$fechaInicialInteres);
								$interesTotal=$interesTotal+$interes;
							}else{
								$deducido=0;
							}
							$subTotal=$monto-$deducido+$interes;
							$Total=$Total+$subTotal;
							
							$montoInicial=$montoInicial+$monto;
							$html->append('emb', array('idEmb' => $row[0],
														'nomEmb' => ucwords($row[1]),
														'matEmb' =>ucwords($row[2]),
														'codPago' =>ucwords($row[3]),
														'registro' => $registro[$m],
														'chi' => $chi[$m],
														//'chi'=> $dataSgs,
														'chdB' => $chdB[$m],
														'chdC' => $chdC[$m],
														'monto' => $monto,
														'deducido' => $deducido,
														'interes' => $interes,
														'subTotal' => $subTotal,
														'valorGrabado' => $valorGrabado[$m]
														));
							$m=$m+1;							
						}								
						$rs->Close();
					}
					unset($rs);					
			
		}
		$html->assign_by_ref('razonSocial',$razonSocial);
		$html->assign_by_ref('direccion',$direccion);
		$html->assign_by_ref('RUC',$RUC);
		$html->assign_by_ref('mail',$mail);
		$html->assign_by_ref('repLegal',$repLegal);
		$html->assign_by_ref('montoInicial',$montoInicial);
		$html->assign_by_ref('deducionTotal',$deducionTotal);
		$html->assign_by_ref('interesTotal',$interesTotal);
		$html->assign_by_ref('Total',$Total);
		
		$html->assign_by_ref('docRepLeg',$docRepLeg);
		$html->assign_by_ref('fechaSinInteres',$fechaSinInteres);
		$html->assign_by_ref('fechaConInteres',$fechaConInteres);
		
		$html->assign_by_ref('estado',$idEstado);
		
		
		//echo $chi[3].count($chi)."xx".count($idEmb).$idEmb[3];
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display('dnepp/declaracionJurada/frmRegistroDecJurMensual.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function RegistraDeclaracionJuradaMensual($anyo,$RZoRUC,$BuscaRZ,$RazonSocial,$razonSocial,$direccion,$RUC,
											  $mail,$repLegal,$docRepLegal,$idEmb,$chi,$chdB,$chdC,$cMonto,$radiobutton){
		
		global $registro,$valorGrab;
				
		if($anyo<1){ $bC2 = true; $this->errors .= 'El A�o debe ser especificado<br>'; }
		if($RazonSocial<1){ $bC3 = true; $this->errors .= 'El Armador debe ser especificado<br>'; }
		if(!$razonSocial){ $bC4 = true; $this->errors .= 'La Raz�n Social debe ser especificada<br>'; }
		
		if($bC2||$bC3||$bC4){
			$objIntranet = new Intranet();
			$objIntranet->Header('Embarcaciones Pesqueras',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormRegistraDeclaracionJuradaMensual($anyo,$RZoRUC,$BuscaRZ,$RazonSocial,$razonSocial,$direccion,$RUC,
											  			$mail,$repLegal,$docRepLegal,$idEmb,$chi,$chdB,$chdC,$cMonto,$radiobutton,
														$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			for($i=0;$i<count($idEmb);$i++){
				
				if(($chi[$i]!="" || $chdB[$i]!="" || $chdC[$i]!="") && $valorGrab[$i]==0){
					if($chi[$i]=="")
						$chi[$i]="0.00";
					if($chdB[$i]=="")
						$chdB[$i]="0.00";
					if($chdC[$i]=="")
						$chdC[$i]="0.00";
						
					$sql_SP = sprintf("EXECUTE sp_ins_DECLARACION_JURADA_MENSUAL %d,%d,%d,%d,%d,'%s',%2f,%2f,%2f,%2f,'%s'",
									  ($anyo) ? $this->PrepareParamSQL($anyo) : "2008",
									  ($radiobutton) ? $this->PrepareParamSQL($radiobutton) : "NULL",
									  ($RazonSocial) ? $this->PrepareParamSQL($RazonSocial) : "NULL",
									  $idEmb[$i],
									  ($idErd) ? $this->PrepareParamSQL($idErd) : "4",//TABALA USER_DNEPP.ENTIDADRECEPDOC
									  $registro[$i],
									  $chi[$i],
									  $chdB[$i],
									  $chdC[$i],
									  $cMonto[$i],
									  $_SESSION['cod_usuario']
									  );
					 //echo $sql_SP;exit;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if (!$rs)
						$RETVAL=1;
					else{
						$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
						$rs->Close();
					}
					unset($rs);						
				}//fin del if()
				
								  
			}//fin del for($i=0;$i<count($idEmb);$i++)			  

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}&subMenu={$this->menu_items[0]['val']}";
			header("Location: $destination");
			exit;
		}											  
	
	}
	
}
?>
