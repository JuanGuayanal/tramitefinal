<?php
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class EmbarcacionesPesqueras extends Modulos {

	function EmbarcacionesPesqueras($menu) {
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][5];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 5;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_BUSCAR => 'frmSearch',
							BUSCAR => 'search',
							MUESTRA_DETALLE => 'showDetail',
							IMPRIME_DETALLE => 'printDetail',
							MUESTRA_HIST_SUSP => 'showHistSusp',
							IMPRIME_HIST_SUSP => 'printHistSusp',
							MUESTRA_HIST_SUSP_PP => 'showHistSuspPP',
							IMPRIME_HIST_SUSP_PP => 'printHistSuspPP',
							VISTA_EXTENDIDA => 'viewEntended',
							CREA_CSV => 'createCSV',
							REPLICA_SGS => 'replicaSGS',
							REPLICA_SISESAT => 'replicaSISESAT',
							REPLICA_SGS_PRUEBA => 'replicaSGSProbe',
							REPORTE_DINSECOVI => 'reportDINSECOVI',
							REPORTE_ZARPE => 'reportZarpe',
							REPORTE_REGESP => 'reportRegEsp',
							REPORTE_NOSENYAL => 'reportNoSignal',
							REPORTE_SUSP135 => 'reportSusp135',
							REPORTE_SUSP135_ZARPE => 'report135Zarpe',
							REPORTE_POR_RESOLUCION => 'reportByRes'
							);
	}
	
	function FormBuscaEmbarcacion($page=NULL,$tipBusqueda=NULL,$desNombre=NULL,$idArte=NULL,$idRegimen=NULL,$idPreserva=NULL,$idCasco=NULL,$idDestino=NULL,$idEspecie=NULL,$idEstado='none'){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion')."\r\n".$this->insertaScriptValidaForm());

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipBusqueda',$tipBusqueda);
		$html->assign_by_ref('desNombre',$desNombre);
		// Contenido Select Arte
		$sql_st = "SELECT id_apa, nombre_apa ".
				  "FROM user_dnepp.aparejo ".
				  "ORDER BY 2";
		$html->assign_by_ref('selArte',$this->ObjFrmSelect($sql_st, $idArte, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Regimen
		$sql_st = "SELECT id_regimen, desc_regimen ".
				  "FROM user_dnepp.regimen ".
				  "ORDER BY 2";
		$html->assign_by_ref('selRegimen',$this->ObjFrmSelect($sql_st, $idRegimen, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Preserva
		$sql_st = "SELECT id_tpres, desc_tpres ".
				  "FROM user_dnepp.tipopreservacion ".
				  "ORDER BY 2";
		$html->assign_by_ref('selPreserva',$this->ObjFrmSelect($sql_st, $idPreserva, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Casco
		$sql_st = "SELECT id_casco, desc_casco ".
				  "FROM user_dnepp.casco ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCasco',$this->ObjFrmSelect($sql_st, $idCasco, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Estado Permiso
		$sql_st = "SELECT id_estper, desc_estper ".
				  "FROM user_dnepp.estadopermiso ".
				  "ORDER BY 2";
		$html->assign_by_ref('selEstado',$this->ObjFrmSelect($sql_st, $idEstado, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		// Contenido Select Tipo Destino
		$sql_st = "SELECT id_destino, rtrim(ltrim(desc_destino)) ".
				  "FROM user_dnepp.destino ".
				  "ORDER BY 2";
		$html->assign_by_ref('selDestino',$this->ObjFrmSelect($sql_st, $idDestino, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);

		
		$html->assign_by_ref('estado',$idEstado);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);

		// Muestra el Formulario		
		$html->display('dnepp/consultas/search.tpl.php');
	}
	
	function MuestraInstrucciones(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		$html->display('dnepp/consultas/showInstrucciones.tpl.php');
	}
	
	function MuestraIndex(){
		$this->FormBuscaEmbarcacion();
		$this->MuestraInstrucciones();
	}

	function ObtieneArmadoresEmbarcacion($id,$sep=' - '){
		// Obtiene los Armadores de la Embarcacion
		$sql_SP = sprintf("EXECUTE sp_busca_armador_embarcacion %d",$id);
		$rsArm = & $this->conn->Execute($sql_SP);
		// echo $sql_SP;
		unset($sql_SP);
		if(!$rsArm)
			print $this->conn->ErrorMsg();
		else{
			while($embArm = $rsArm->FetchRow())
				$strEmbArm[] = $embArm[0];
			$rsArm->Close();
		}
		unset($rsArm);
		
		return (isset($strEmbArm)) ? implode($sep,$strEmbArm) : false;
	}
	
	function ObtieneAparejosEmbarcacion($id,$sep=' - '){
		// Obtiene los Aparejos de la Embarcacion
		$sql_SP = sprintf("EXECUTE sp_busca_aparejo_embarcacion %d",$id);
		$rsApa = & $this->conn->Execute($sql_SP);
		//echo $sql_SP;
		unset($sql_SP);
		if(!$rsApa)
			print $this->conn->ErrorMsg();
		else{
			while($embApa = $rsApa->FetchRow())
				$strEmbApa[] = $embApa[0];
			$rsApa->Close();
		}
		unset($rsApa);
		
		return (isset($strEmbApa)) ? implode($sep,$strEmbApa) : false;
	}
	
	function BuscaEmbarcacion($page,$tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado){
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
		
		//Contador de caracteres
		$cont=strlen($desNombre);
		$cont2=strlen($tipBusqueda);
		//echo "el contador es".$cont;
		/**/
		if($cont>10||($cont2>5)){
		 $this->MuestraIndex();
		 exit;
		}/**/
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaEmbarcacion($page,$tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado);
		
		

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($idArte)&&!is_null($idArte)&&$idArte!='none'){
			$condConsulta[] = "emb.id_emb=eap.id_emb and eap.id_apa={$idArte}";
			$condTables[] = "user_dnepp.embxapar eap";
		}
		if(!empty($idDestino)&&!is_null($idDestino)&&$idDestino!='none'){
			$condConsulta[] = "emb.id_emb=eed.id_emb and eed.id_destino='{$idDestino}'";
			$condTables[] = "user_dnepp.embxespxdest eed";
		}
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = sprintf("(emb.id_estper=%1\$d or emb.estadozarpe_emb=%1\$d)",$idEstado);
		if(!empty($idPreserva)&&!is_null($idPreserva)&&$idPreserva!='none')
			$condConsulta[] = "emb.id_tpres={$idPreserva}";
		if(!empty($idRegimen)&&!is_null($idRegimen)&&$idRegimen!='none')
			$condConsulta[] = "emb.id_regimen={$idRegimen}";
		if(!empty($idCasco)&&!is_null($idCasco)&&$idCasco!='none')
			$condConsulta[] = "emb.id_casco={$idCasco}";
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		//C�digo para detectar intrusos
		$prueba1=strpos(strtoupper($desNombre), "SELECT ");		
		if(!($prueba1===false)) exit;		
		$prueba2=strpos(strtoupper($desNombre), "DELETE ");		
		if(!($prueba2===false)) exit;		
		$prueba3=strpos(strtoupper($desNombre), "INSERT ");		
		if(!($prueba3===false)) exit;		
		$prueba4=strpos(strtoupper($desNombre), "DROP ");		
		if(!($prueba4===false)) exit;		
		$prueba5=strpos(strtoupper($desNombre), "UPDATE ");		
		if(!($prueba5===false)) exit;
		$prueba6=strpos(strtoupper($desNombre), "ALTER ");
		if(!($prueba6===false)) exit;
		
		$m="%";
				
		$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',0,0",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where));
		//echo $sql_SP;		
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
			unset($rs);
		}

		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',%d,%d",
								$this->PrepareParamSQL($tipBusqueda),
								($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
								$this->PrepareParamSQL($tables),
								$this->PrepareParamSQL($where),
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($embID = $rsId->FetchRow()){
					// Obtiene los Armadores de la Embarcacion
					$strEmbArm = $this->ObtieneArmadoresEmbarcacion($embID[0]);
					$strEmbArm = (strlen($strEmbArm)>60) ? substr($strEmbArm,0,57).'...' : $strEmbArm;
					// Obtiene los Aparejos de la Embarcacion
					$strEmbApa = $this->ObtieneAparejosEmbarcacion($embID[0]);
					$strEmbApa = (strlen($strEmbApa)>60) ? substr($strEmbApa,0,57).'...' : $strEmbApa;
					// Obtiene Todos los Datos de la Embarcacion
					$sql_SP = sprintf("EXECUTE sp_busca_datos_embarcacion %d",$embID[0]);
					//echo $sql_SP;
					$rsData = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rsData)
						print $this->conn->ErrorMsg();
					else{
						if($embData = $rsData->FetchRow())
							$html->append('arrEmb', array('id' => $embData[0],
														  'desc' => $embData[1],
														  'matri' => $embData[2],
														  'armador' => ($strEmbArm) ? ucwords(strtolower($strEmbArm)) : 'No tiene',
														  'aparejo' => ($strEmbApa) ? ucwords(strtolower($strEmbApa)) : 'No tiene',
														  'regimen' => ucwords(strtolower($embData[3])),
														  'preserva' => ucwords(strtolower($embData[4])),
														  'casco' => ucwords(strtolower($embData[5])),
														  'pesca' => ucfirst($embData[15]),
														  'zarpe' => ucfirst($embData[16]),
														  'constancia' => ucfirst($embData[17])
														  ));
						$rsData->Close();
					}
					unset($rsData);
					unset($strEmbArm);
					unset($strEmbApa);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;
		
		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign('datos',array('desNombre'=>$desNombre,
									'tipBusqueda'=>$tipBusqueda,
									'arte'=>$idArte,
									'regimen'=>$idRegimen,
									'preserva'=>$idPreserva,
									'casco'=>$idCasco,
									'destino'=>$idDestino,
									'estado'=>$idEstado));
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->MenuSearchPaginable($numRegs, $page, $frmName, $this->arr_accion[BUSCAR], true));

		// Muestra el Resultado de la Busqueda
		$html->display('dnepp/consultas/searchResult.tpl.php');
	}
	
	function ObtieneDestinoEspecieEmbarcacion($idEmb,$idDest,$sep='<br>- '){
		// Lista las Especies de la Embarcacion por el Destino especificado
		$sql_SP = sprintf("EXECUTE  sp_lista_espxdestxemb %d,'%s'",
						   $this->PrepareParamSQL($idEmb),
						   $this->PrepareParamSQL($idDest));
		// echo $sql_SP;
		$rsEsp = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsEsp){
			print $this->conn->ErrorMsg();
		}else{
			while($esp = $rsEsp->FetchRow())
				$embEsp[] = $esp[0];
			unset($esp);
			$rsEsp->Close();
		}
		unset($rsEsp);
		
		return (is_array($embEsp)) ? implode($sep,$embEsp) : false;
	}

	
	function DetalleEmbarcacion($id, $print=false){
		// Genera HTML de Muestra
		$html = new Smarty;
		$this->SetDirTemplate($html);	

		$id=urldecode($id);
		$this->abreConnDB();
		//$this->conn->debug=true;
			
		$sql_SP = sprintf("EXECUTE sp_lista_embarcacion %d,'<br>'",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rsData = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsData){
			print $this->conn->ErrorMsg();
		}else{
			if($embData = $rsData->FetchRow())
				/* Sequencia de Datos
				   0. nombre_emb
				   1. matricula_emb
				   2. desc_casco
				   3. capbod_emb
				   4. capbod1_emb
				   5. desc_regimen
				   6. especies **
				   7. armador *
				   8. Patron **
				   9. Doc. Identidad **
				   10. desc_estper
				   11. numero_res (Res. Suspencion) *
				   12. Cod. Baliza **
				   13. Ind. Pesca Jurel **
				   14. Matri. alto/bajo relieve **
				   15. numero_res (Res. permiso)
				   16. capbod23porc (inc. 3%)
				   17. capbod215porc (inc. 15%)
				   18. sist. pesca
				   19. tip. preserv.
				   20. numero_res (Res. Cancelacion)
				   21. Estado de Zarpe
				   22. Motivo Suspension Zarpe
				   23. Documento de Susp. de Zarpe
				   24. Fecha de Suspension de Zarpe
				   25. Transmisor
				   26. Motor Caracteristicas
			
				   * Datos sacados de Sub Store Procedures
				   ** Datos faltantes
				*/
				$html->assign('emb', array('id' => $id,
										  'desc' => $embData[0],
										  'matri' => $embData[1],
										  'casco' => $embData[2],
										  'capbod' => $embData[3],
										  'capbodTM' => $embData[4],
										  'regimen' => $embData[5],
										  'armador' => $embData[7],
										  'estado' => $embData[10],
										  'resSusp' => $embData[11],
										  'resPerm' => $embData[15],
										  'capbod3p' => ($embData[3]>=32.6 && $embData[3]<=50) ? round($embData[4]*1.06,2) : round($embData[4]*1.03,2),
										  'capbod15p' => round($embData[4]*1.15,2),
										  'aparejo' => $embData[18],
										  'preserva' => $embData[19],
										  'resCan' => $embData[20],
										  'zarpe' => $embData[21],
										  'motZarpe' => $embData[22],
										  'docZarpe' => $embData[23],
										  'fecZarpe' => $embData[24],
										  'trans' => $embData[25],
										  'motor' => $embData[26]
										  ));
			unset($row);
			$rsData->Close();
		}
		unset($rsData);
		
		// Agrega los Destinos y las Especies al Template HTML
		$sql_SP = "EXECUTE sp_lista_destino";
		$rsDest = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsDest){
			print $this->conn->ErrorMsg();
		}else{
			// $cont = 0;
			while($dest = $rsDest->FetchRow()){
				$html->append("embDest",$dest[1]);
				$html->append("embEsp",$this->ObtieneDestinoEspecieEmbarcacion($id,$dest[0]));
			}
			unset($dest);
			$rsDest->Close();
		}
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('dnepp/consultas/printDetalle.tpl.php') : $html->display('dnepp/consultas/showDetalle.tpl.php');
	}
	
	function ImprimeDetalleEmbarcacion($id){
		$this->DetalleEmbarcacion($id, true);
	}
	
	function CreaCSVBuscaEmbarcacion($tipBusqueda,$desNombre,$idArte,$idRegimen,$idPreserva,$idCasco,$idDestino,$idEspecie,$idEstado){
		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTables = array();
		if(!empty($idArte)&&!is_null($idArte)&&$idArte!='none'){
			$condConsulta[] = "emb.id_emb=eap.id_emb and eap.id_apa={$idArte}";
			$condTables[] = "user_dnepp.embxapar eap";
		}
		if(!empty($idDestino)&&!is_null($idDestino)&&$idDestino!='none'){
			$condConsulta[] = "emb.id_emb=eed.id_emb and eed.id_destino='{$idDestino}'";
			$condTables[] = "user_dnepp.embxespxdest eed";
		}
		if(!empty($idEstado)&&!is_null($idEstado)&&$idEstado!='none')
			$condConsulta[] = sprintf("(emb.id_estper=%1\$d or emb.estadozarpe_emb=%1\$d)",$idEstado);
		if(!empty($idPreserva)&&!is_null($idPreserva)&&$idPreserva!='none')
			$condConsulta[] = "emb.id_tpres={$idPreserva}";
		if(!empty($idRegimen)&&!is_null($idRegimen)&&$idRegimen!='none')
			$condConsulta[] = "emb.id_regimen={$idRegimen}";
		if(!empty($idCasco)&&!is_null($idCasco)&&$idCasco!='none')
			$condConsulta[] = "emb.id_casco={$idCasco}";
		
		// Armma las Tablas Adicionales para la Consulta con los Condicionales
		$tables = (count($condTables)>0) ? ', '.implode(', ',$condTables) : '';
		
		// Arma el Condicional que sera paramero del Store Procedure
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el Store Procedure que lista todos los registros de la Busqueda
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$m="%";
		
		// Prepara el SP y agrega parametros
		$sql_SP = sprintf("EXECUTE sp_busca_id_embarcacion %d,%s,'%s','%s',%d,%d",
							$this->PrepareParamSQL($tipBusqueda),
							($desNombre) ? "'".$this->PrepareParamSQL($desNombre)."'" : "'".$m."'",
							$this->PrepareParamSQL($tables),
							$this->PrepareParamSQL($where),
							1,
							0);
		//echo $sql_SP;

		// Ejecuta la Consulta Paginable a trav�s del Store Procedure
		$rsId = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsId)
			print $this->conn->ErrorMsg();
		else{
			// Genera HTML
			$html = new Smarty;
			$this->SetDirTemplate($html);
			
			// Consulta y Agrega al Template HTML los Destinos Validos para toda embarcacion
			$sql_SP = "EXECUTE sp_lista_destino";
			$rsDest = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsDest){
				print $this->conn->ErrorMsg();
			}else{
				while($dest = $rsDest->FetchRow()){
					$html->append("embDest",$dest[1]);
					$arrIdDest[] = $dest[0];
				}
				unset($dest);
				$rsDest->Close();
			}
			unset($rsDest);

			while($embID = $rsId->FetchRow()){
				// Obtiene las Especie por Destino de Cada Embarcacion
				for($i=0;$i<count($arrIdDest);$i++)
					$embEsp[] = $this->PrepareParamSQL($this->ObtieneDestinoEspecieEmbarcacion($embID[0],$arrIdDest[$i],"\n"),'"');

				// Consulta y Agrega al Template HTML los Destinos Validos para toda embarcacion
				$sql_SP = sprintf("EXECUTE sp_lista_embarcacion %d,'%s'",
									$embID[0],"\n");
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					if($row = $rs->FetchRow())
						/* Sequencia de Datos
						   0. nombre_emb
						   1. matricula_emb
						   2. desc_casco
						   3. capbod_emb
						   4. capbod1_emb
						   5. desc_regimen
						   6. especies **
						   7. armador *
						   8. Patron **
						   9. Doc. Identidad **
						   10. desc_estper
						   11. numero_res (Res. Suspencion) *
						   12. Cod. Baliza **
						   13. Ind. Pesca Jurel **
						   14. Matri. alto/bajo relieve **
						   15. numero_res (Res. permiso)
						   16. capbod23porc (inc. 3%)
						   17. capbod215porc (inc. 15%)
						   18. sist. pesca
						   19. tip. preserv.
						   20. numero_res (Res. Cancelacion)
						   21. Estado de Zarpe
						   22. Documento de Susp. de Zarpe
						   23. Fecha de Suspension de Zarpe
						   24. Transmisor
					
						   * Datos sacados de Sub Store Procedures
						   ** Datos faltantes
						*/		
						// Agraga los datos de la Embarcacion al Template HTML
						$html->append('arrEmb',array('desc'=>$this->PrepareParamSQL($row[0],'"'),
													 'matr'=>$this->PrepareParamSQL($row[1],'"'),
													 'casc'=>$this->PrepareParamSQL($row[2],'"'),
													 'capbod'=>$this->PrepareParamSQL($row[3],'"'),
													 'capbodTM'=>($row[4]) ? $this->PrepareParamSQL($row[4],'"') : 'NO TIENE',
													 'regi'=>($row[5]) ? $this->PrepareParamSQL($row[5],'"') : 'NO ESPECIFICADO',
													 'espe'=>$embEsp,
													 'arma'=>$this->PrepareParamSQL($row[7],'"'),
													 'esta'=>$this->PrepareParamSQL($row[10],'"'),
													 'resSusp'=>($row[11]) ? $this->PrepareParamSQL($row[11],'"') : 'NO TIENE',
													 'resPerm'=>($row[15]) ? $this->PrepareParamSQL($row[15],'"') : 'NO TIENE',
													 'cb3p'=>($row[3]>=32.6 && $row[3]<=50) ? $this->PrepareParamSQL(round($row[4]*1.06,2),'"') : $this->PrepareParamSQL(round($row[4]*1.03,2),'"'),
													 'cb15p'=>($row[4]) ? $this->PrepareParamSQL(round($row[4]*1.15,2),'"') : 'NO TIENE',
													 'sist'=>$this->PrepareParamSQL($row[18],'"'),
													 'pres'=>$this->PrepareParamSQL($row[19],'"'),
													 'resCan'=>($row[20]) ? $this->PrepareParamSQL($row[20],'"') : 'NO TIENE',
													 'zarpe' => $this->PrepareParamSQL($row[21],'"'),
													 'motZarpe' => ($row[22]) ? $this->PrepareParamSQL($row[22],'"') : 'NO TIENE',
													 'docZarpe' => ($row[23]) ? $this->PrepareParamSQL($row[23],'"') : 'NO TIENE',
													 'fecZarpe' => ($row[24]) ? $this->PrepareParamSQL($row[24],'"') : 'NO TIENE',
													 'trans' => ($row[25]) ? $this->PrepareParamSQL($row[25],'"') : 'NO TIENE',
													 'motor'=>($row[26]) ? $this->PrepareParamSQL($row[26],'"') : 'NO ESPECIFICADO'));
					unset($embEsp);
					$rs->Close();
				}
				unset($rs);

			}
			$rsId->Close();
			header("Content-type: application/csv");
			header("Content-Disposition: attachment; filename=consulta-".mktime().".csv");
			$html->display('dnepp/embarcaciones/createCSV.tpl.php');
		}
		unset($rsId);		
	}
	
	function ReplicaDBDneppToSGS($probe=false){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		// Abre Conexion DB
		$this->abreConnDB();		
		$sql_SP = "SELECT id_emb ".
				  "FROM user_dnepp.embarcacionnac ".
				  "WHERE id_regimen not in (3) and id_emb in ( ".
				    "SELECT distinct id_emb ".
					"FROM user_dnepp.embxespxdest ".
					"WHERE id_esp in ( ".
					  "SELECT id_esp ".
					  "FROM user_dnepp.especie ".
					  "WHERE Upper(nombre_esp) like '%ANCHOV%' ".
					") ".
				  ") ".
				  "ORDER BY nombre_emb";
		$rsID = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsID){
			print $this->conn->ErrorMsg();
		}else{
			while($embID = $rsID->FetchRow()){
				$sql_SP = sprintf("EXECUTE sp_lista_embarcacion_SGS %d",$embID[0]);
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrEmb',array('desc'=>$row[0],
													 'matr'=>$row[1],
													 'casc'=>$row[2],
													 'cbM3'=>$row[3],
													 'cbTM'=>$row[4],
													 'regi'=>$row[5],
													 'espe'=>$row[6],
													 'arma'=>$row[7],
													 'patr'=>$row[8],
													 'docu'=>$row[9],
													 'esta'=>$row[10],
													 'reSu'=>$row[11],
													 'bali'=>$row[12],
													 'jure'=>$row[13],
													 'reli'=>$row[14],
													 'rePe'=>$row[15],
													 'cb3p'=>($row[3]>=32.6 && $row[3]<=50) ? round($row[4]*1.06,2) : round($row[4]*1.03,2),
													 'cb15'=>$row[17],
													 'id'=>$row[18],
													 'eZar'=>$row[19],
													 'mZar'=>$row[20],
													 'rArm'=>$row[21],
													 'moto'=>$row[22],
													 'insp'=>$row[23],
													 'sate'=>$row[24]));

					unset($row);
					$rs->Close();
				}
				unset($rs);
			}
			unset($embID);
			$rsID->Close();
		}
		unset($rsID);
		header("Content-type: application/text");
		header("Content-Disposition: attachment; filename=replicaEmbarcaciones" . strftime('%d-%I-%Y-%H_%M'). ".txt");
		$html->display((!$probe) ? 'dnepp/consultas/createTxt-SGS.tpl.php' : 'dnepp/consultas/createTxt-SGS-probe.tpl.php');
	}
	
	function ReplicaDBDneppToSISESAT($tipo=false){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		// Abre Conexion DB
		$this->abreConnDB();		
		switch($tipo){
			case 'embarcacionnac':
				$sql_SP = "EXECUTE sp_EmbarcacionnacToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrEmb',array('id'=>$row[0],
													 'desc'=>$row[1],
													 'tpre'=>$row[2],
													 'matr'=>$row[3],
													 'regi'=>$row[4],
													 'casc'=>$row[5],
													 'tras'=>$row[6],
													 'cbod'=>$row[7],
													 'prov'=>$row[8],
													 'equi'=>$row[9]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createEmb-SISESAT.tpl.php');
				break;
			case 'armador':
				$sql_SP = "EXECUTE sp_ArmadorToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrArm',array('id'=>$row[0],
													 'desc'=>$row[1],
													 'ruc'=>$row[2],
													 'dire'=>$row[3],
													 'tele'=>$row[4],
													 'fax'=>$row[5]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createArm-SISESAT.tpl.php');
				break;
			case 'aparejo':
				$sql_SP = "EXECUTE sp_AparejoToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrApa',array('id'=>$row[0],
													 'desc'=>$row[1]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createApa-SISESAT.tpl.php');
				break;
			case 'casco':
				$sql_SP = "EXECUTE sp_CascoToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrCas',array('id'=>$row[0],
													 'desc'=>$row[1]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createCas-SISESAT.tpl.php');
				break;
			case 'embxarm':
				$sql_SP = "EXECUTE sp_EmbxArmToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrExA',array('id'=>$row[0],
													 'idEmb'=>$row[1],
													 'idArm'=>$row[2]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createEmbxArm-SISESAT.tpl.php');
				break;
			case 'embxapar':
				$sql_SP = "EXECUTE sp_EmbxApaToSISESAT";
				//echo $sql_SP;
				$rs = & $this->conn->Execute($sql_SP);
				unset($sql_SP);
				if (!$rs){
					print $this->conn->ErrorMsg();
				}else{
					while($row = $rs->FetchRow())
						$html->append('arrExA',array('idApa'=>$row[0],
													 'idEmb'=>$row[1]));
		
					unset($row);
					$rs->Close();
				}
				unset($rs);

				header('Content-type: application/text');
				$html->display('dnepp/consultas/createEmbxApa-SISESAT.tpl.php');
				break;
		}
	}
	
	function HistorialSuspensionZarpe($id,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow())
				$html->assign(array('id'=>$row[0],'desc'=>$row[1],'matri'=>$row[2]));
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("EXECUTE sp_listHistSuspEmb %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp',array('oDis'=>$row[0],
										   'fIni'=>$row[1],
										   'moti'=>$row[2],
										   'fFin'=>$row[3],
										   'obse'=>$row[4],
										   'esta'=>$row[5]));
			$rs->Close();
		}
		unset($rs);

		$print ? $html->display('dnepp/consultas/printHistSusp.tpl.php') : $html->display('dnepp/consultas/showHistSusp.tpl.php');
	}
	
	function ImprimeHistorialSuspensionZarpe($id){
		$this->HistorialSuspensionZarpe($id,true);
	}
	
	function HistorialSuspensionPesca($id,$print=false){
		// Genera Objeto HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);	

		$this->abreConnDB();
		
		$sql_SP = sprintf("EXECUTE sp_buscaDatosEmb_intra %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			if($row = $rs->FetchRow())
				$html->assign(array('id'=>$row[0],'desc'=>$row[1],'matri'=>$row[2]));
			$rs->Close();
		}
		unset($rs);
		
		$sql_SP = sprintf("EXECUTE sp_listHistSuspPPEmb %d",$this->PrepareParamSQL($id));
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if(!$rs)
			print $this->conn->ErrorMsg();
		else{
			while($row = $rs->FetchRow())
				$html->append('susp',array('resu'=>$row[0],
										   'fFin'=>$row[1],
										   'obse'=>$row[2],
										   'esta'=>$row[3]));
			$rs->Close();
		}
		unset($rs);

		$print ? $html->display('dnepp/consultas/printHistSuspPP.tpl.php') : $html->display('dnepp/consultas/showHistSuspPP.tpl.php');
	}
	
	function ImprimeHistorialSuspensionPesca($id){
		$this->HistorialSuspensionPesca($id,true);
	}

	function ReporteSuspensionDINSECOVI(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporteSuspDINSECOVI";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6],
										  'dCum'=>$row[7],
										  'fSus'=>$row[8]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'suspToday'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportDinsecovi.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;		
	}
	
	function ReporteSuspRegEsp(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporteSuspRegEsp";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'suspRegEsp'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportRegEsp.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;		
	}
	
	function ReporteSuspZarpe(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporteSuspZarpe";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6],
										  'infr'=>$row[7]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'suspZarpe'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportZarpe.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;
	}
	
	function ReporteSuspNoSenyal(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporteSuspNoSenyal";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'casc'=>$row[3],
										  'tran'=>$row[4],
										  'fIng'=>$row[5]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$html->assign('fecha',strftime("%d-%m-%Y"));
		$html->assign('hora',(date('H')>17&&date('H')<9) ? '06:00 PM' : '09:00 AM');
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'suspNoSenyal'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportNoSenyal.tpl.php'),false,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;		
	}
	
	function ReporteSusp135(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporteSusp135";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6],
										  'infr'=>$row[7]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'suspDef135'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportSusp135.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;
	}
	
	function Reporte135Zarpe(){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = "EXEC sp_reporte135Zarpe";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6],
										  'infr'=>$row[7]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = 'susp135Zarpe'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/report135Zarpe.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;
	}
	
	function SeteaTituloReporte($idMotivo){
		switch($idMotivo){
			case 15:
				$titulo = array('SUSPENSI�N DEFINITIVA DEL R�GIMEN DE PESCA EXPLORATORIA DE LOS RECURSOS ANCHOVETA Y ANCHOVETA BLANCA (RESOLUCI�N MINISTERIAL N� 185-2004-CONVENIO_SITRADOC)',
								'RELACION DE EMBARCACIONES PESQUERAS DETECTADAS SIN LA EMISI�N DE SE�ALES DE POSICIONAMIENTO GPS POR M�S DE TRES (03) HORAS.');
				break;
			case 16:
				$titulo = array('SUSPENSI�N DE ZARPE POR TRES (03) D�AS CONSECUTIVOS (RESOLUCI�N MINISTERIAL N� 185-2004-CONVENIO_SITRADOC)',
								'RELACI�N DE EMBARCACIONES PESQUERAS DETECTADAS CON VELOCIDADES DE PESCA MENORES A DOS (02) NUDOS, '.
								'RUMBO NO CONSTANTE Y NO EMISION DE SE�AL DE POSICIONAMIENTO POR UN INTERVALO DE TRES HORAS DENTRO '.
								'DE LAS CINCO MILLAS MARINAS.');
				break;
			case 17:
				$titulo = array('SUSPENSI�N DEFINITIVA DEL R�GIMEN DE PESCA EXPLORATORIA DE LOS RECURSOS ANCHOVETA Y ANCHOVETA BLANCA (RESOLUCI�N MINISTERIAL N� 309-2003-CONVENIO_SITRADOC)');
				break;
			case 18:
				$titulo = array('SUSPENSI�N DE ZARPE POR VEINTE (20) D�AS CONSECUTIVOS (RESOLUCI�N MINISTERIAL N� 309-2003-CONVENIO_SITRADOC)');
				break;
		}
		return $titulo;
	}
	
	function ReportePorResolucion($idMotivo){
		// Genera HTML
		$html = new Smarty;
		$this->SetDirTemplate($html);
		
		$html->assign_by_ref('titulo',$this->SeteaTituloReporte($idMotivo));
		
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql_st = sprintf("EXEC sp_reportePorResolucion %d",$this->PrepareParamSQL($idMotivo));
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('emb',array('desc'=>$row[0],
										  'matr'=>$row[1],
										  'arma'=>$row[2],
										  'fInc'=>$row[3],
										  'zona'=>$row[4],
										  'dArm'=>$row[5],
										  'dDis'=>$row[6],
										  'infr'=>$row[7]));
			$rs->Close();
		}
		unset($rs);
		setlocale(LC_TIME,$this->_zonaHoraria);
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/apps/mmb.logo.CONVENIO_SITRADOC.jpg';
		
		$path = $_SERVER['DOCUMENT_ROOT'] . '/mipe/dinsecovi/suspensiones';
		$filename = sprintf('suspM%d-%s',$idMotivo,mktime());
		$this->CreaArchivoPDF($filename,$path,$html->fetch('dnepp/consultas/reportByRes.tpl.php'),true,$logo);

		$destination = 'http://' . $_SERVER['HTTP_HOST'] . '/mipe/dinsecovi/suspensiones/' . $filename . '.pdf';
		header("Content-type: application/pdf");
		header("Location: {$destination}");
		exit;
	}
}
?>