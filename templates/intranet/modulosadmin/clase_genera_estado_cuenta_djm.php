<?
error_reporting (E_ALL ^ E_NOTICE);

class genera_estado_cuenta_djm
{

var $saldo_mes_anterior;
var $dab;	
var $total_a_rep=0;
var $total_b_rep=0;
var $total_c_rep=0;
var $saldo_mes_anterior_final=0;

var $deuda_mes1=0;
var $deuda_mes2=0;
var $deuda_mes3=0;
var $deuda_mes4=0;
var $deuda_mes5=0;
var $deuda_mes6=0;
var $deuda_mes7=0;
var $deuda_mes8=0;
var $deuda_mes9=0;
var $deuda_mes10=0;
var $deuda_mes11=0;
var $deuda_mes12=0;

var $apli_doble=0;
var $apli_triple=0;



function conectar()
	{
		if (!($this->$dab=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))) 
		   { 
			  echo "Error conectando a la base de datos."; 
			  exit(); 
		   } 
		   if (!mssql_select_db("DB_DNEPP",$this->$dab)) 
		   { 
			  echo "Error seleccionando la base de datos."; 
			  exit(); 
		   }		
	}   


	  
	function desconectar($links)
	{
		mssql_close($links); 
		unset($links);
	}
	
	
function trae_meses_djm($link, $year_djm, $id_embarca, $gen) 
{

	$year_sis=date('Y');
	$mes_sis=date('m');	
	
	if($year_sis>$year_djm){ $mes_sis=13; }
	$mes_sis=number_format($mes_sis,0,'','');
	
	$this->total_a_rep=0;
	$this->total_b_rep=0;
	$this->total_c_rep=0;
	$this->saldo_mes_anterior_final=0;
	$this->deuda_mes1=0;
	$this->deuda_mes2=0;
	$this->deuda_mes3=0;
	$this->deuda_mes4=0;
	$this->deuda_mes5=0;
	$this->deuda_mes6=0;
	$this->deuda_mes7=0;
	$this->deuda_mes8=0;
	$this->deuda_mes9=0;			
	$this->deuda_mes10=0;
	$this->deuda_mes11=0;
	$this->deuda_mes12=0;			
	$this->apli_doble=0;
	$this->apli_triple=0;
	echo $this->cabecera_table_report();

	for ($i = 1; $i < $mes_sis; $i++)
	{
		$nom_mes='';
		if($i==1){ $nom_mes='Enero'; }
		if($i==2){ $nom_mes='Febrero'; }
		if($i==3){ $nom_mes='Marzo'; }
		if($i==4){ $nom_mes='Abril'; }
		if($i==5){ $nom_mes='Mayo'; }
		if($i==6){ $nom_mes='Junio'; }
		if($i==7){ $nom_mes='Julio'; }
		if($i==8){ $nom_mes='Agosto'; }
		if($i==9){ $nom_mes='Septiembre'; }
		if($i==10){ $nom_mes='Octubre'; }
		if($i==11){ $nom_mes='Noviembre'; }
		if($i==12){ $nom_mes='Diciembre'; }				
		//echo $nom_mes;

		$this->trae_fila_djm_embarcacion($link, $id_embarca, $year_djm, $i, $nom_mes); 
	}
	
	$total_a=$this->total_a_rep;
	$total_b=$this->total_b_rep;
	$total_c=$this->total_c_rep;
	
		echo $this->muestra_total_reporte($total_a, $total_b, $total_c);
		echo $this->pie_table_report();
		echo '<br><br>';
		if($this->saldo_mes_anterior_final<=0){	
			if($total_c>0)
			{
			$monto_b=0;
			}else{
			$monto_b=$this->saldo_mes_anterior_final*(-1);
			}
		}else{  
		$monto_b=0;
		}
		if($total_c>0){		$monto_a=$total_c;}else{  $monto_a=0;}
				
				
		echo $this->muestra_saldos_favor($monto_a, $monto_b, $year_djm);

		$mes1=$this->deuda_mes1;
		$mes2=$this->deuda_mes2;
		$mes3=$this->deuda_mes3;
		$mes4=$this->deuda_mes4;
		$mes5=$this->deuda_mes5;
		$mes6=$this->deuda_mes6;
		$mes7=$this->deuda_mes7;
		$mes8=$this->deuda_mes8;		
		$mes9=$this->deuda_mes9;
		$mes10=$this->deuda_mes10;
		$mes11=$this->deuda_mes11;
		$mes12=$this->deuda_mes12;
				

		$ap_doble=$this->apli_doble;
		$ap_triple=$this->apli_triple;
		
		$this->grabar_detalle_saldo($link, $id_embarca, $year_djm, 
							$mes1, $mes2, $mes3, 
							$mes4, $mes5, $mes6,
							$mes7, $mes8, $mes9,
							$mes10, $mes11, $mes12, $monto_b, $ap_doble, $ap_triple, $gen);


	$this->deuda_mes1=0;
	$this->deuda_mes2=0;
	$this->deuda_mes3=0;
	$this->deuda_mes4=0;
	$this->deuda_mes5=0;
	$this->deuda_mes6=0;
	$this->deuda_mes7=0;
	$this->deuda_mes8=0;
	$this->deuda_mes9=0;			
	$this->deuda_mes10=0;
	$this->deuda_mes11=0;
	$this->deuda_mes12=0;		
	$this->apli_doble=0;
	$this->apli_triple=0;
		$this->saldo_mes_anterior_final=0;
}

function muestra_total_reporte($total_a, $total_b, $total_c)
{

$script_total='
<tr bgcolor="#ECF5FF" height="20">
            <td colspan="7" align="center" class="textoblack" ><font size="2"><strong>TOTAL</strong></font></td>
            <td class="textoblack" align="right"><font size="2"><b>'.number_format($total_a,2,'.',',').'</b></font></td>
			<td class="textoblack" align="right"><font size="2"><b>'.number_format($total_b,2,'.',',').'</b></font></td>
			<td colspan="2" align="right" class="texto">&nbsp;</td>			
            <td class="textoblack" align="right"><font size="2"><b>'.number_format($total_c,2,'.',',').'</b></font></td>
          </tr>
';
return $script_total; 
}


function cabecera_table_report()
{

	$script_cabe='
			<table width="100%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
			<tr bgcolor="#A6D2EA">
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><div align="center"><strong>MES</strong></div></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>FECHA DE<br />VTO.<br />DEDUC. 5% </strong></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>CAPTURA<br />TM</strong></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>DERECHOS<br />DE PESCA</strong></td>
            <td colspan="3" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>DEDUCCIONES</strong></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><p><strong>TOTAL A <br />PAGAR</strong></p></td>
			<td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>TOTAL<br />PAGADO</strong></td>
			<td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>TOTAL<br />RESULTANTE</strong></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>INTERESES</strong></td>
            <td rowspan="2" align="center" bgcolor="#CCCCCC" class="textowhite"><strong>TOTAL</strong></td>            
          </tr>
          <tr>
            <td align="center" bgcolor="#CCCCCC" class="textowhite"><strong>5%</strong></td>
            <td align="center" bgcolor="#CCCCCC" class="textowhite"><strong>SALDO MES ANTERIOR </strong></td>
            <td align="center" bgcolor="#CCCCCC" class="textowhite"><strong>TOTAL</strong></td>
          </tr>';

return $script_cabe;

}


function pie_table_report()
{
	$script_cabe='</table>';
	return $script_cabe;
}




function trae_fila_djm_embarcacion($link, $id_embarca, $year_djm, $mes_djm, $nom_mes) 
{

	$xtipo_cambio=$this->trae_tipo_cambio_mes($link, $id_embarca, $year_djm, $mes_djm);
	$xuit=3600; //2011
	/*echo '1.-tipo de cambio'.$xtipo_cambio;*/
	
	$tm_chiA=$this->trae_valor_embarcacion_djm($link, $id_embarca, $year_djm, $mes_djm,1);
	$tm_chdB=$this->trae_valor_embarcacion_djm($link, $id_embarca, $year_djm, $mes_djm,2);
	$tm_chdC=$this->trae_valor_embarcacion_djm($link, $id_embarca, $year_djm, $mes_djm,3);	
	/*echo '<br>2.-Valores CHI y CHD.:<br>';
	echo 'CHI-A:'.$tm_chiA.'<br>';
	echo 'CHD-B:'.$tm_chdB.'<br>';
	echo 'CHD-C:'.$tm_chdC.'<br>';		*/
	$precio_prom=$this->trae_valores_derechos($link, $year_djm, $mes_djm,1); //precio prom fob
	$valCHI=$this->trae_valores_derechos($link, $year_djm, $mes_djm,2);  //valchi
	$porcCHI=$this->trae_valores_derechos($link, $year_djm, $mes_djm,3); //porcentaje chi
	$valCHD=$this->trae_valores_derechos($link, $year_djm, $mes_djm,4);  //valchd
	$porcCHD=$this->trae_valores_derechos($link, $year_djm, $mes_djm,5); //porcentaje chd
	$tm_total_captura=$tm_chiA+$tm_chdB+$tm_chdC;
	/*echo '3.-Valores Derechos.:<br>';
	echo 'Precio Prom.:'.$precio_prom.'<br>';
	echo 'Valor CHI:'.$valCHI.'<br>';
	echo 'Porcent. CHI:'.$porcCHI.'<br>';
	echo 'Valor CHD:'.$valCHD.'<br>';
	echo 'Porcent. CHD:'.$porcCHD.'<br>';	
	echo 'Total Captura:'.$tm_total_captura.'<br>';			*/
	
		
	$monto_chiA=$tm_chiA*$xtipo_cambio*$valCHI;
	$monto_chdB=$tm_chdB*($porcCHD/100)*$xuit;
	$monto_chdC=$tm_chdC*($porcCHD/100)*$xuit;		
	$total_derecho=$monto_chiA+$monto_chdB+$monto_chdC;

	$saldo_mes_anterior=0;
	if($mes_djm==1){ $saldo_mes_anterior=0;	}else{	$saldo_mes_anterior=$this->saldo_mes_anterior*(-1);	} //saldo mes anterior
	$total_deduccion=$saldo_mes_anterior; //total deduccion
	$total_a_pagar=$total_derecho-$total_deduccion; //total a pagar
	
	$exite_djm=0;
	
	$exite_djm=$this->verifica_si_hay_djm($link, $id_embarca, $year_djm, $mes_djm);
	if($exite_djm==0)
	{ 
		$str_existe='<br><span style="color:#FF0000;">(No Declarado)</span>';  
	}else{ 
		$str_existe='';  
	}
	echo '<tr bgcolor="#F5F5F5">';
	echo '<td align="center" class="texto">'.$nom_mes.'</td>';
	echo '<td align="center" class="texto">No hay Deducciones'.$str_existe.'</td>';	
	echo '<td align="right" valign="top" class="texto">'.number_format($tm_total_captura,3,'.','').'</td>';		
	echo '<td align="right" valign="top" class="texto">'.number_format($total_derecho,2,'.','').'</td>';			
	echo '<td align="right" valign="top" class="texto">'.number_format(0,2,'.','').'</td>';	
	echo '<td align="right" valign="top" class="texto">'.number_format($saldo_mes_anterior,2,'.','').'</td>';		
	echo '<td align="right" valign="top" class="texto">'.number_format($total_deduccion,2,'.','').'</td>';	
	
	echo '<td align="right" valign="top" class="texto">'.number_format($total_a_pagar,2,'.','').'</td>';				
	$total_importes_pagados=$this->trae_valor_total_importes_pagados($link, $id_embarca, $year_djm, $mes_djm);
	echo '<td align="right" valign="top" class="texto">'.number_format($total_importes_pagados,2,'.','');
			$this->trae_detalle_importes_pagados($link, $id_embarca, $year_djm, $mes_djm);
	echo '</td>';			

		$this->evaluacion_importes_pagados($link, $id_embarca, $year_djm, $mes_djm, $total_a_pagar);
		$this->total_a_rep=$this->total_a_rep+$total_a_pagar;
		$this->total_b_rep=$this->total_b_rep+$total_importes_pagados;
		
		

		
	echo '</tr>';	
	
	
}

function trae_tipo_cambio_mes($link, $id_embarca, $year_djm, $mes_djm) 
{
	$valor_tc=0;
	
	$sql="select  convert(numeric(18,3),compra_tcdappd), convert(numeric(18,3),venta_tcdappd), convert(varchar,fech_tcdappd,103)
		from user_dnepp.tipo_cambio_dappd 	where fech_tcdappd in (select max(fech_tcdappd) from user_dnepp.tipo_cambio_dappd
		where fech_tcdappd<convert(datetime,convert(varchar(10),getdate(),103),103) )";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$valor_tc=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		

	$fecha_cancelacion=$this->trae_fecha_cancelacion_mes($link, $id_embarca, $year_djm, $mes_djm);
	
	if($fecha_cancelacion!='')
	{
	$sql_tc2="sp_trae_tc_cancelacion '".$fecha_cancelacion."'";
				$resultw=mssql_query($sql_tc2,$link);		
				if (!$resultw){	
				//

				  }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$valor_tc=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}			
	}
	
	return  number_format($valor_tc,3,'.','');	 
}

function trae_fecha_cancelacion_mes($link, $id_embarca, $year_djm, $mes_djm) 
{
	$fecha_tc='';
	
	$sql="	SELECT feccanc_ip  from IMPORTE_PAGADO
			where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND FLAG_EXPORT=1
			and id_embdjm=".$id_embarca." and 
			CONVERT(DATETIME,feccanc_ip,103) in 
				(select min(convert(datetime,feccanc_ip,103)) from importe_pagado 
					where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca.")
			";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$fecha_tc=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		
	
	
	return  $fecha_tc;	 
}

function trae_valor_embarcacion_djm($link, $id_embarca, $year_djm, $mes_djm,$tipo)
{

	$valor_res=0;
	
	$sql="	select  djm.captotchi_djm, djm.captotchd_jurcab_djm, djm.captotchd_otro_djm, mes_djm 
			from dbo.declaracion_jurada_mensual djm
			where djm.id_embarcacion=".$id_embarca." AND DJM.ANYO_DJM=".$year_djm."  AND DJM.MES_DJM = ".$mes_djm." and estado_djm=1";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												if($tipo==1){ $valor_res=$row5[0]; }
												if($tipo==2){ $valor_res=$row5[1]; }
												if($tipo==3){ $valor_res=$row5[2]; }																								
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		

	return  $valor_res;	 
}

function trae_valores_derechos($link, $year_djm, $mes_djm,$tipo)
{

	$valor_res=0;
	
	$sql="	SELECT 
			CONVERT(NUMERIC(18,3),PRECIOPROMEDIOFOB), 
			CONVERT(NUMERIC(18,3),VALCHI), 
			CONVERT(NUMERIC(18,3),PORCENTAJE_CHI), 
			CONVERT(NUMERIC(18,3),VALCHD),
			CONVERT(NUMERIC(18,3),PORCENTAJE_CHD)
			FROM 
			VALOR_FOB_PAGO_DERECHO 
			where id_ANYO=".$year_djm." AND ID_MES=".$mes_djm."";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				  }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												if($tipo==1){ $valor_res=$row5[0]; } //precio prom
												if($tipo==2){ $valor_res=$row5[1]; } //valor chi
												if($tipo==3){ $valor_res=$row5[2]; } //porcentaje Chi
												if($tipo==4){ $valor_res=$row5[3]; } //valor chd
												if($tipo==5){ $valor_res=$row5[4]; } //porcentaje Chd
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		

	return  $valor_res;	 
}

function trae_valor_total_importes_pagados($link, $id_embarca, $year_djm, $mes_djm)
{
	$valor_impo=0;

		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." ";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$valor_impo=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

	return  $valor_impo;	
}

function trae_detalle_importes_pagados($link, $id_embarca, $year_djm, $mes_djm)
{

		$sql="	SELECT feccanc_ip, IMPORTE_IP  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				";

				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				 }else{  
											echo '<table width="70%">';
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												echo '<tr  bgcolor="#F5F5F5">';
												echo '<td align="left" class="texto"  width="50%">'.$row5[0].'</td>';
												echo '<td align="right" class="texto"  width="50%">'.number_format($row5[1],2,'.','').'</td>';
												echo '</tr>';
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
											echo '</table>';
											
								}	


}




function evaluacion_importes_pagados($link, $id_embarca, $year_djm, $mes_djm, $derecho)
{

	$dif_meses=$this->trae_dif_meses_actuales($link, $year_djm, $mes_djm);
	$monto_derecho=$derecho;

	$imp_pagados_a_tiempo_djm=$this->importes_pagados_a_tiempo($link, $id_embarca, $year_djm, $mes_djm);
	$saldo_a_tiempo_djm=number_format($monto_derecho,2,'.','')-number_format($imp_pagados_a_tiempo_djm,2,'.','');	
	/*
	echo '<br>----1.-'.$monto_derecho;
	echo '<br>----2.-'.$imp_pagados_a_tiempo_djm;
	echo '<br>----3.-'.$saldo_a_tiempo_djm;
	*/
	$saldo_a_tiempo_djm_para_doble=0;
	//echo '1er saldo '.number_format($saldo_a_tiempo_djm,2,'.','');
	if($saldo_a_tiempo_djm_para_doble>0)
	{
		$deuda_djm=$saldo_a_tiempo_djm_para_doble;
	}
	
	if($saldo_a_tiempo_djm<=0)
	{
		$saldo_mes=$saldo_a_tiempo_djm	;
	}else{
			$saldo_a_tiempo_djm_para_doble=$saldo_a_tiempo_djm;

			if($dif_meses>1){ $doble=1; }else{  $doble=0; }
			//cambioa estado
			//if($dif_meses>2){ $triple=1; }else{  $triple=0; }	

			if($doble==1)
			{ 
				$pagaron_doble=0;
				$monto_doble=$this->aplica_doble($link, $id_embarca, $year_djm, $mes_djm, $saldo_a_tiempo_djm_para_doble);  
				$pagaron_doble=$this->cuenta_imp_doble($link, $id_embarca, $year_djm, $mes_djm);
				$saldo_doble=$this->evaluar_doble($link, $id_embarca, $year_djm, $mes_djm, $monto_doble);
				
				//echo '<br>1er Pago Doble:  '.$monto_doble;
				//echo '<br>1er saldo Doble:  '.$saldo_doble;				
						$tiene_doble=0;
					if($saldo_doble>0)
					{
						$deuda_djm=$saldo_doble;
						$tiene_doble=1;
						$this->apli_doble=1;

					}
				
			}
			
			$aplico_nuevo_saldo=0;
			
			$cuenta_gen_triple=0;
			
			
			//cambioa estado
			//$cuenta_gen_triple=$this->cuenta_gen_triple($link, $year_djm);
			$cuenta_gen_triple=0;
			
			if($cuenta_gen_triple>0)
			{
					$cuenta_eva_triple=0;
					if($mes_djm==1 || $mes_djm==2 || $mes_djm==3 || $mes_djm==4 || $mes_djm==5 || $mes_djm==6 )
					{
						$ipa_trip=1;
						$cuenta_eva_triple=$this->cuenta_gen_nro_triple($link, $year_djm,1);
					}
					
					if($mes_djm==7 || $mes_djm==8 || $mes_djm==9 || $mes_djm==10 || $mes_djm==11 || $mes_djm==12)
					{
						$ipa_trip=2;
						$cuenta_eva_triple=$this->cuenta_gen_nro_triple($link, $year_djm,2);
					}							
					
					if($cuenta_eva_triple==1)	
					{	
						//echo '<br> i:'.$ipa_trip.'<br>';
						$saldo_pago_ant_triple=$this->evalua_pago_doble_antes_triple($link, $id_embarca, $year_djm, $mes_djm, $saldo_doble, $ipa_trip);
						if($saldo_doble>$saldo_pago_ant_triple)
						{
							$aplico_nuevo_saldo=1;
							$ipa_trip=$cuenta_gen_triple;
						}
						//echo '<br>Aplica:'.$ipa_trip.'<BR> 
								//SALDO 1:'.$saldo_doble.'<BR> 
								//SALDO 2:'.$saldo_pago_ant_triple.'<br>New Saldo: '.$aplico_nuevo_saldo;
						
					}else{
						if($ipa_trip==2)
						{

								$saldo_pago_ant_triple=$this->evalua_pago_doble_antes_2dotriple($link, $id_embarca, $year_djm, $mes_djm, $saldo_doble, $ipa_trip);
								if($saldo_doble>$saldo_pago_ant_triple)
								{
									$aplico_nuevo_saldo=1;
									$ipa_trip=$cuenta_gen_triple;
								}
						
						
						}
						$triple=0;
					}
			}
			
			if($aplico_nuevo_saldo==1)
			{
			//echo '<br>saldo anr1:'.$saldo_doble;
			$saldo_doble=$saldo_pago_ant_triple;
			//echo '<br>saldo anr2:'.$saldo_doble;
						
				if($saldo_doble>0.99){ $deuda_djm=$saldo_doble; }else{ $deuda_djm=0; }
				if($saldo_doble<=0){ $triple=0;	}
				if($saldo_doble<0.09){ $saldo_doble=0;  $triple=0; }
			//echo '<br>saldo anr3:'.$saldo_doble;
							
			}

			if($triple==1 && $tiene_doble==1 && $saldo_doble>0)
			{ 
			//echo '<br>aplica: triple ';
				$monto_triple=$this->aplica_triple($link, $id_embarca, $year_djm, $mes_djm, $saldo_a_tiempo_djm_para_doble, $saldo_doble, $pagaron_doble);  
				$saldo_triple=$this->evaluar_triple($link, $id_embarca, $year_djm, $mes_djm, $monto_triple);

				//echo '<br>1er Pago Triple:  '.$monto_triple;
				//echo '<br>1er saldo Triple:  '.$saldo_triple;	
					$tiene_triple=0;
					if($saldo_triple>0)
					{
						$deuda_djm=$saldo_triple;
						$tiene_triple=1;
						$this->apli_triple=1;												
					}else{
						$deuda_djm=0;
						$tiene_triple=1;
						$this->apli_triple=1;												
					
					}
				
			}

			if($doble==1 && $triple==0){ if($saldo_doble<0){	$saldo_mes=$saldo_doble; }	}
			if($doble==1 && $triple==1){ if($saldo_triple<0){	$saldo_mes=$saldo_triple; }	}			
			if($doble==0 && $triple==0){ if($saldo_a_tiempo_djm<0){	$saldo_mes=$saldo_a_tiempo_djm; }	}
	}	
	
	echo '<td align="right" valign="top" class="texto">';
	if($saldo_mes<=0){ echo number_format($saldo_mes,2,'.','');}else{  echo number_format($saldo_mes,2,'.',''); }
	echo '</td>';				

	echo '<td align="right" valign="top" class="texto">';
	if($deuda_djm>0){ echo number_format($deuda_djm,2,'.','');}else{  echo number_format($deuda_djm,2,'.',''); }
	if($tiene_doble==1){
		echo '<br><span style="color:#009966;">(**)</span>';
	}	
	if($tiene_triple==1){
		echo '<span style="color:#FF0000;">(***)</span>';
	}
	echo '</td>';				

	echo '<td align="right" valign="top" class="texto">';
	if($deuda_djm>0){ echo number_format($deuda_djm,2,'.','');}else{  echo number_format($deuda_djm,2,'.',''); }
	echo '</td>';				

	if($mes_djm==1){ $this->deuda_mes1=$deuda_djm; }
	if($mes_djm==2){ $this->deuda_mes2=$deuda_djm; }
	if($mes_djm==3){ $this->deuda_mes3=$deuda_djm; }
	if($mes_djm==4){ $this->deuda_mes4=$deuda_djm; }
	if($mes_djm==5){ $this->deuda_mes5=$deuda_djm; }
	if($mes_djm==6){ $this->deuda_mes6=$deuda_djm; }
	if($mes_djm==7){ $this->deuda_mes7=$deuda_djm; }
	if($mes_djm==8){ $this->deuda_mes8=$deuda_djm; }
	if($mes_djm==9){ $this->deuda_mes9=$deuda_djm; }
	if($mes_djm==10){ $this->deuda_mes10=$deuda_djm; }
	if($mes_djm==11){ $this->deuda_mes11=$deuda_djm; }
	if($mes_djm==12){ $this->deuda_mes12=$deuda_djm; }						
	
	
	$this->total_c_rep=$this->total_c_rep+$deuda_djm;

	$this->saldo_mes_anterior=$saldo_mes;
	if($saldo_mes<0)
	{
	$this->saldo_mes_anterior_final=$saldo_mes;
	}
}



function aplica_triple($link, $id_embarca, $year_djm, $mes_djm, $saldo_a_tiempo_djm_para_doble, $saldo_doble, $pagaron_doble)
{
	$monto_triple=0;
	
	//echo '<br>----'.$saldo_doble;
	//echo '<br>----'.$pagaron_doble;
	if($pagaron_doble>0)
	{
	$monto_triple=$saldo_doble*3;	
	}else{
	$monto_triple=$saldo_a_tiempo_djm_para_doble*3;	
	}
	//echo '<br>triple: '.$monto_triple;
	return $monto_triple;	
} 



function evaluar_triple($link, $id_embarca, $year_djm, $mes_djm, $monto_triple)
{
	$saldo_triple=0;


	$mes_djm_eva=$mes_djm+3;
	$ano_djm_eva=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	
	if($mes_djm>9)
	{ 
		if($mes_djm==10){ $mes_djm_eva='01';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==11){ $mes_djm_eva='02';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==12){ $mes_djm_eva='03';  $ano_djm_eva=$ano_djm_eva+1;}
	}
		
	$fecha_ini_triple= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;

	$imp_pagados_triple=0;
	
		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)>=convert(datetime,CONVERT(VARCHAR(10),'".$fecha_ini_triple."',103),103)
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
//echo $sql;
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				}else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$imp_pagados_triple=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

	//echo '<br>Evaluar triple:<br>'.$monto_triple.'----'.$imp_pagados_triple;
	$saldo_triple= $monto_triple-$imp_pagados_triple;
	return $saldo_triple;	
} 

function aplica_doble($link, $id_embarca, $year_djm, $mes_djm, $saldo_a_tiempo_djm_para_doble)
{
	$monto_doble=0;
	$monto_doble=$saldo_a_tiempo_djm_para_doble*2;	
	
	return $monto_doble;	
} 

function cuenta_imp_doble($link, $id_embarca, $year_djm, $mes_djm, $monto_doble)
{
$cuenta_doble=0;
	$mes_djm_eva=$mes_djm+2;
	$mes_djm_eva2=$mes_djm+3;
	$ano_djm_eva=$year_djm;
	$ano_djm_eva2=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	if($mes_djm_eva2<10){ $mes_djm_eva2='0'.$mes_djm_eva2; }	
	if($mes_djm>9)
				{ 
					if($mes_djm==10){ 						$mes_djm_eva2='01';  	$ano_djm_eva2=$ano_djm_eva2+1;}  
					if($mes_djm==11){ $mes_djm_eva='01';	$mes_djm_eva2='02';  	$ano_djm_eva=$ano_djm_eva+1; 	$ano_djm_eva2=$ano_djm_eva2+1;} 
					if($mes_djm==12){ $mes_djm_eva='02'; 	$mes_djm_eva2='03';  	$ano_djm_eva=$ano_djm_eva+1; 	$ano_djm_eva2=$ano_djm_eva2+1;} 
				}		
	
	$fecha_ini_doble= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;
	$fecha_fin_doble= '01/'.$mes_djm_eva2.'/'.$ano_djm_eva2;
		

	
	$sql="	SELECT count(*)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)>=
				convert(datetime,CONVERT(VARCHAR(10),'".$fecha_ini_doble."',103),103)
				and
				convert(datetime,convert(int,convert(datetime,CONVERT(VARCHAR(10),'".$fecha_fin_doble."',103),103))-1,103)
				>=
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
				//echo '<br>'.$sql.'<br>';
				$resultw=mssql_query($sql,$link);		
				$cuenta_doble=0;
				if (!$resultw){	
				//
				 }else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$cuenta_doble=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

	return $cuenta_doble;	
}
function evaluar_doble($link, $id_embarca, $year_djm, $mes_djm, $monto_doble)
{
	$saldo_doble=0;
	$mes_djm_eva=$mes_djm+2;
	$mes_djm_eva2=$mes_djm+3;
	$ano_djm_eva=$year_djm;
	$ano_djm_eva2=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	if($mes_djm_eva2<10){ $mes_djm_eva2='0'.$mes_djm_eva2; }	
	if($mes_djm>9)
				{ 
					if($mes_djm==10){ 						$mes_djm_eva2='01';  	$ano_djm_eva2=$ano_djm_eva2+1;}  
					if($mes_djm==11){ $mes_djm_eva='01';	$mes_djm_eva2='02';  	$ano_djm_eva=$ano_djm_eva+1; 	$ano_djm_eva2=$ano_djm_eva2+1;} 
					if($mes_djm==12){ $mes_djm_eva='02'; 	$mes_djm_eva2='03';  	$ano_djm_eva=$ano_djm_eva+1; 	$ano_djm_eva2=$ano_djm_eva2+1;} 
				}		
	
	$fecha_ini_doble= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;
	$fecha_fin_doble= '01/'.$mes_djm_eva2.'/'.$ano_djm_eva2;

	$imp_pagados_doble=0;
	
		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)>=
				convert(datetime,CONVERT(VARCHAR(10),'".$fecha_ini_doble."',103),103)
				and
				convert(datetime,convert(int,convert(datetime,CONVERT(VARCHAR(10),'".$fecha_fin_doble."',103),103))-1,103)>=
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
//echo '<br>'.$sql.'<br>';
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				 }else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$imp_pagados_doble=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	
	//echo 'saldo_doble : '.$monto_doble.'  -  '.$imp_pagados_doble;	
	$saldo_doble= $monto_doble-$imp_pagados_doble;
	//echo 'saldo_doble : '.$saldo_doble;
	return $saldo_doble;	
} 

function importes_pagados_a_tiempo($link, $id_embarca, $year_djm, $mes_djm)
{
	$mes_djm_eva=$mes_djm+1;
	$mes_djm_eva2=$mes_djm+2;
	$ano_djm_eva=$year_djm;
	$ano_djm_eva2=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	if($mes_djm_eva2<10){ $mes_djm_eva2='0'.$mes_djm_eva2; }	
	if($mes_djm>10)
				{ 
					if($mes_djm==11){ 						$mes_djm_eva2='01';  	$ano_djm_eva2=$ano_djm_eva2+1;} 
					if($mes_djm==12){ $mes_djm_eva='01'; 	$mes_djm_eva2='02';  	$ano_djm_eva=$ano_djm_eva+1; 	$ano_djm_eva2=$ano_djm_eva2+1;} 
				}	
	
	$fecha_ini_pagos= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;
	$fecha_fin_pagos= '01/'.$mes_djm_eva2.'/'.$ano_djm_eva2;
	
	$imp_pagados_a_tiempo=0;
	
		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				
				convert(datetime,
				CONVERT(VARCHAR(10),feccanc_ip,103)
				,103)
				>=
				convert(datetime,
				CONVERT(VARCHAR(10),'".$fecha_ini_pagos."',103)
				,103)
				and
				convert(datetime,
				convert(int,convert(datetime,CONVERT(VARCHAR(10),'".$fecha_fin_pagos."',103),103))-1
				,103)
				>=
				convert(datetime,
				CONVERT(VARCHAR(10),feccanc_ip,103)
				,103)
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
		//echo $sql;
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				// 

				  }else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$imp_pagados_a_tiempo=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

	//echo '<br>--'.$imp_pagados_a_tiempo.'--<br>';
	return $imp_pagados_a_tiempo;
}






function trae_dif_meses_actuales($link, $year_djm, $mes_djm) 
{
	$valor_difmese=0;
	if($mes_djm<10){ $mes_djm='0'.$mes_djm; }

	$sql="select  webminpro.fn_diferencia_meses('".$mes_djm."','".$year_djm."')";

				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$valor_difmese=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		

	
	
	return  $valor_difmese;	 
}



function trae_datos_generales($link, $year_djm, $idEmb, $nomEmb,$matEmb,$codPagoEmb, $capBodEmb, $capBodEmbTM, $razon_social, $domicilio)		
{
$script_res0='
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::: Intranet Institucional del Ministerio de la Producci�n :::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Language" content="es" />
 <link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<link rel="stylesheet" href="/styles/theme.css" type="text/css" />
<link rel="stylesheet" href="/styles/portada.css" type="text/css" />
 
<link rel="stylesheet" href="/styles/apps.search.css" type="text/css" />
<link rel="stylesheet" href="/styles/saltopagina.css" type="text/css" />
</head>
<body>
';

$script_res01='
<table border=0 cellpadding=0 cellspacing=0 width=780 align="center">
  <tr> 
    <td valign=top bgcolor="#FFFFFF"> ';

$script_res1='
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF">
		<tr>
		<td colspan="4" class="texto" align="center"><div align="left"><strong>MINISTERIO DE LA PRODUCCI&Oacute;N <BR />
		</strong>Direcci�n General de Extracci&oacute;n Y Procesimiento Pesquero </div></td>
		</tr>
		<tr>
		<td height="18" colspan="4" class="textogray">
		<p align=center><b>EVALUACI&Oacute;N DE LAS DECLARACIONES JURADAS DEL PAGO DE DERECHOS DE PESCA<br>'.$year_djm.'<BR>(S/.)</b><br></p></td>
		</tr>
		<tr>
		<td class="textogray">&nbsp;</td>
		<td height="18">&nbsp;</td>
		<td colspan="2"  >&nbsp;</td>
		</tr>
		<tr>
		<td class="textoblack"><strong></strong></td>
		<td height="18">&nbsp;</td>
		<td colspan="2" valign="top" >&nbsp;</td>
		</tr><tr>
		<td height="18" colspan="4" class="texto"><div align="center"><strong>'.$razon_social.'</strong></div></td></tr>
          <tr>
            <td height="18" colspan="4" class="textoblack"><div align="center"><span class="texto"><b></b></span></div></td>
          </tr>
          <tr>
            <td class="textoblack">&nbsp;</td><td height="18" class="textogray">&nbsp;</td><td colspan="2" valign="top" >&nbsp;</td>
          </tr>
        </table>		
';

$script_res2='
<table width="95%"  border="0" align="center" cellspacing="1" bgcolor="#FFFFFF" class="texto"  >
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr>
	  <td class="textoblack" width="20%"><strong>EMBARCACI&Oacute;N: </strong></td>
	  <td class="texto"><span class="textoblack"><strong>'.$nomEmb.'</strong></span></td>
	</tr>
	<tr>
	  <td class="textoblack"><strong>MATRICULA: </strong></td>
	  <td class="texto"><span class="textoblack"><strong>'.$matEmb.'</strong></span></td>
	</tr>
	<tr>
	  <td class="textoblack"><strong>CAPACIDAD DE BODEGA:</strong></td>
	  <td class="texto"><span class="textoblack"><strong>'.number_format($capBodEmb,3,'.','').' m3 - '.number_format($capBodEmbTM,3,'.','').' TM</strong></span></td>
	</tr>
	<tr>
	  <td class="textoblack"><strong>C&Oacute;DIGO DE PAGO : </strong></td>
	  <td class="texto"><span class="textoblack"><strong>'.$codPagoEmb.'</strong></span></td>
	</tr>	
  </table>

';
return $script_res0.$script_res01.$script_res1.$script_res2;

}

function muestra_saldos_favor($monto_a, $monto_b, $year_djm)
{
$ano_djm_ant=$year_djm-1;
 $script_res0='
 		<table border=0 cellpadding=0 cellspacing=0 width="100%" align="center">
			<tr bgcolor="#F5F5F5">
			<td width="70%">
				<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
					<tr bgcolor="#F5F5F5">
						<td width="100%" class="texto">Le comunicamos que su representada deber&aacute; cancelar la deuda incluyendo el inter&eacute;s moratorio que correspondan al pago a la fecha de pago. Al momento de realizar el reintegro correspondiente, deber&aacute; comunicarse al tel&eacute;fono 616-2222 anexo 1413, a fin de indicarle intereses moratorios al d&iacute;a de cancelaci&oacute;n, en cumplimiento con la normatividad vigente. </td>
					</tr>
				</table>
			</td>
			<td width="5%">&nbsp;</td>
			<td width="25%">
						<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
						<tr bgcolor="#F5F5F5">
						<td width="100%">
							<table border=0 cellpadding=0 cellspacing=0 width="100%" align="center">
								<tr>
								<td width="60%" class="texto"><b>IMPORTE A PAGAR : </b></td>
								<td width="40%" align="right" class="texto"><b>'.number_format($monto_a,2,'.',',').'</b></td>
								</tr>
							</table>
						</td>
						</tr>
						<tr bgcolor="#F5F5F5">
						<td width="100%">
							<table border=0 cellpadding=0 cellspacing=0 width="100%" align="center">
								<tr>
									<td width="60%" class="texto"><b>SALDO A FAVOR &nbsp;&nbsp;&nbsp;&nbsp;: </b></td>
									<td width="40%" align="right" class="texto"><b>'.number_format($monto_b,2,'.',',').'</b></td>
								</tr>
							</table>
						</td>
						</tr>
						</table> 
				</td>
				</tr>
				</table>
				
				<br><br>
				
				<table border=0 cellpadding=0 cellspacing=0 width="100%" align="center">
				<tr bgcolor="#F5F5F5">
				<td width="70%">
						<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
						<tr bgcolor="#F5F5F5">
						<td width="100%" class="texto">
						<b>* Saldo proveniente del Ejercicio '.$ano_djm_ant.'.</b>
						<br>
						<span style="color:#009966;">(**)&nbsp;&nbsp;&nbsp;Se Aplic� Doble.</span>
						<br>
						<span style="color:#FF0000;">(***)&nbsp;&nbsp;Se Aplic� Triple.</span>
						</td>
						</tr>
						</table>
				</td>
				<td width="5%">&nbsp;</td>
				<td width="25%">&nbsp;</td>
				</tr>
				</table>
				
				';
return $script_res0;

}

function terminar_djm($link, $id_emb, $year_djm, $fecha_imp)
{
		$cont=0;
	for ($i = 1; $i < 13; $i++)
	{
		$nom_mes='';
		if($i==1){ $nom_mes='Enero'; }
		if($i==2){ $nom_mes='Febrero'; }
		if($i==3){ $nom_mes='Marzo'; }
		if($i==4){ $nom_mes='Abril'; }
		if($i==5){ $nom_mes='Mayo'; }
		if($i==6){ $nom_mes='Junio'; }
		if($i==7){ $nom_mes='Julio'; }
		if($i==8){ $nom_mes='Agosto'; }
		if($i==9){ $nom_mes='Septiembre'; }
		if($i==10){ $nom_mes='Octubre'; }
		if($i==11){ $nom_mes='Noviembre'; }
		if($i==12){ $nom_mes='Diciembre'; }				
		//echo $nom_mes;
		$existe_djm_c=0;
		$existe_djm_c=$this->verifica_si_hay_djm($link, $id_emb, $year_djm, $i);
		
		if($existe_djm_c==0)
		{
				$cont=$cont+1;
				if($cont==1)
				{
				$str_meses.=$nom_mes;
				}else{
				$str_meses.='-'.$nom_mes;
				}
			
		}
	}


$script_res02='
		<table>
		<tr> 
		<td class="textoblack"> 
		<br><br><br><p><b><u>DECLARACION(ES) JURADA(S) FALTANTE(S):</u></b>
		<br>'.$str_meses.'</p><p><b><u>INFORMES:</u></b><br>

		Direcci&oacute;n General de Extracci&oacute;n y Procesamiento Pesquero DGEPP - Ministerio de la Producci&oacute;n<!--<br>-->
		<b>Telf. </b><b>616-2222 Anex. 1413 </b><br>
		Email: <b>dgepp@CONVENIO_SITRADOC.gob.pe</b><br>
		</p>
		<p><b><u>Fecha de Impresi&oacute;n:</u></b><br>
		'.$fecha_imp.'</p>
		
		</td>
		</tr>		
		</table>
</td></tr></table>

<H1 class=SaltoDePagina> </H1>
';

return $script_res02;
}


function verifica_si_hay_djm($link, $id_embarca, $year_djm, $mes_djm)
{
$existe_djm=0;

$sql="	select  COUNT(*)
			from dbo.declaracion_jurada_mensual djm
			where 
				djm.id_embarcacion=".$id_embarca." AND 
				DJM.ANYO_DJM=".$year_djm."  AND 
				DJM.MES_DJM = ".$mes_djm." and 
				estado_djm=1";

//echo $sql;
				$resultw=mssql_query($sql,$link);		
				if (!$resultw){	
				//
				 }else{  while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$existe_djm=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}		



return $existe_djm;
}




function trae_empresas_djm($link, $idpersona,$id_embarcacion, $year_djm, $gen)
{

							  
					$sql = "db_dnepp.webminpro.sp_trae_emb_djm_datos_gen_1 ".$year_djm."";

					if($id_embarcacion>0)
					{
					$sql = "db_dnepp.webminpro.sp_trae_emb_djm_datos_gen_2 ".$id_embarcacion."";
					}else{
						if($idpersona>0)
						{
						$sql = "db_dnepp.webminpro.sp_trae_emb_djm_datos_gen_3 ".$idpersona."";						
						}else{
						$sql = "exec db_dnepp.webminpro.sp_trae_emb_djm_datos_gen_4_2011 ".$year_djm."";						
						}
					}
				//echo $sql;


				$resultw_new=mssql_query($sql,$link);		
				if (!$resultw_new){	
				//
				 }else{  
				 while( $row5_new = mssql_fetch_array( $resultw_new ) )
											{ 
											
												
														$idEmb=$row5_new[0];
														$nomEmb=$row5_new[1];
														$matEmb=$row5_new[2];
														$codPagoEmb=$row5_new[3];
														$capBodEmb=$row5_new[4];
														$capBodEmbTM=$row5_new[4]*1.026;
														$razon_social=$row5_new[5];
														
													echo $this->trae_datos_generales($link, $year_djm, $idEmb, $nomEmb,$matEmb,$codPagoEmb, $capBodEmb, $capBodEmbTM, $razon_social, '');									
													$this->trae_meses_djm($link, $year_djm, $idEmb, $gen); 
													$fecha_imp=date('d/m/Y');
													echo $this->terminar_djm($link, $idEmb, $year_djm, $fecha_imp);
													//exit;
														
											} 
											mssql_free_result($resultw_new); 	
											unset($resultw_new);
								}		


				/*
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				}else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
													 echo '--error2--';
														$idEmb=$row5[0];
														$nomEmb=$row5[1];
														$matEmb=$row5[2];
														$codPagoEmb=$row5[3];
														$capBodEmb=$row5[4];
														$capBodEmbTM=$row5[4]*1.026;
														$razon_social=$row5[5];
														//$domicilio=$row5[6];

													echo $this->trae_datos_generales($link, $year_djm, $idEmb, $nomEmb,$matEmb,$codPagoEmb, $capBodEmb, $capBodEmbTM, $razon_social, '');									
													
													$this->trae_meses_djm($link, $year_djm, $idEmb); 
													$fecha_imp=date('d/m/Y');
													echo $this->terminar_djm($link, $id_emb, $year_djm, $fecha_imp);
													exit;
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	
								*/

	

}






function grabar_detalle_saldo($link, $id_embarca, 
								$year_djm, 
								$mes1, $mes2, $mes3,
								$mes4, $mes5, $mes6,
								$mes7, $mes8, $mes9,
								$mes10, $mes11, $mes12, $saldoFavor, $ap_doble, $ap_triple, $gen)
{

$TotalMonto=	number_format($mes1,2,'.','')+
				number_format($mes2,2,'.','')+
				number_format($mes3,2,'.','')+
				number_format($mes4,2,'.','')+
				number_format($mes5,2,'.','')+
				number_format($mes6,2,'.','')+
				number_format($mes7,2,'.','')+
				number_format($mes8,2,'.','')+
				number_format($mes9,2,'.','')+
				number_format($mes10,2,'.','')+
				number_format($mes11,2,'.','')+
				number_format($mes12,2,'.','');


				$sql_det="UPDATE webminpro.TBL_APLICACION_INTERES SET
				ESTADO=0 
				WHERE ANO_DJM=".$year_djm."  AND ID_EMB=".$id_embarca."  ";
				$resultw=mssql_query($sql_det,$link);

			if($year_djm==2009)
			{
			$sql_det="INSERT INTO webminpro.SALDOS_FINALES_DETALLADO_".$year_djm."_bk 
			VALUES(".$id_embarca.",
			".number_format($mes1,2,'.','').",".number_format($mes2,2,'.','').",".number_format($mes3,2,'.','').",".number_format($mes4,2,'.','').",
			".number_format($mes5,2,'.','').",".number_format($mes6,2,'.','').",".number_format($mes7,2,'.','').",
			".number_format($TotalMonto,2,'.','').",'".date('d/m/Y')."',
			".number_format($mes8,2,'.','').",".number_format($mes9,2,'.','').",".number_format($mes10,2,'.','').",".number_format($mes11,2,'.','').",".number_format($mes12,2,'.','').")";
			$resultw=mssql_query($sql_det,$link);		
			//echo '<br><br>'.$sql_det;
			
			$sql_anual="INSERT INTO webminpro.SALDOS_FINALES_".$year_djm."_bk 
			VALUES(".$id_embarca.",".number_format($TotalMonto,2,'.','').",0,
			".number_format($TotalMonto,2,'.','').",
			".number_format($saldoFavor,2,'.','').",'".date('d/m/Y')."')";
			$resultw=mssql_query($sql_anual,$link);
			
			}


			if($year_djm==2010)
			{
			
			$sql_del="delete from webminpro.SALDOS_FINALES_".$year_djm."_bk where fecha='".date('d/m/Y')."' and id_emb=".$id_embarca." ";
			$resultw=mssql_query($sql_del,$link);					
			//echo '<br><br>'.$sql_del;
			
			$sql_det_del="delete from webminpro.SALDOS_FINALES_DETALLADO_".$year_djm."_bk where fecha='".date('d/m/Y')."' and id_emb=".$id_embarca." ";
			$resultw=mssql_query($sql_det_del,$link);	
						
			$sql_det="INSERT INTO webminpro.SALDOS_FINALES_DETALLADO_".$year_djm."_bk 
			VALUES(".$id_embarca.",
			".number_format($mes1,2,'.','').",".number_format($mes2,2,'.','').",".number_format($mes3,2,'.','').",".number_format($mes4,2,'.','').",
			".number_format($mes5,2,'.','').",".number_format($mes6,2,'.','').",".number_format($mes7,2,'.','').",
			".number_format($TotalMonto,2,'.','').",'".date('d/m/Y')."',
			".number_format($mes8,2,'.','').",".number_format($mes9,2,'.','').",".number_format($mes10,2,'.','').",".number_format($mes11,2,'.','').",".number_format($mes12,2,'.','').")";
			$resultw=mssql_query($sql_det,$link);		
			//echo '<br><br>'.$sql_det;
			
			$sql_anual="INSERT INTO webminpro.SALDOS_FINALES_".$year_djm."_bk 
			VALUES(".$id_embarca.",".number_format($TotalMonto,2,'.','').",0,
			".number_format($TotalMonto,2,'.','').",
			".number_format($saldoFavor,2,'.','').",'".date('d/m/Y')."')";
			$resultw=mssql_query($sql_anual,$link);
			
			}
			
			
		if($gen==1)
		{
			$sql_del="delete from webminpro.SALDOS_FINALES_".$year_djm." where fecha='".date('d/m/Y')."' and id_emb=".$id_embarca." ";
			$resultw=mssql_query($sql_del,$link);					
			//echo '<br><br>'.$sql_del;
			
			$sql_det_del="delete from webminpro.SALDOS_FINALES_DETALLADO_".$year_djm." where fecha='".date('d/m/Y')."' and id_emb=".$id_embarca." ";
			$resultw=mssql_query($sql_det_del,$link);	
			//echo '<br><br>'.$sql_det_del;
			
			$sql_det="INSERT INTO webminpro.SALDOS_FINALES_DETALLADO_".$year_djm." 
			VALUES(".$id_embarca.",
			".number_format($mes1,2,'.','').",".number_format($mes2,2,'.','').",".number_format($mes3,2,'.','').",".number_format($mes4,2,'.','').",
			".number_format($mes5,2,'.','').",".number_format($mes6,2,'.','').",".number_format($mes7,2,'.','').",
			".number_format($TotalMonto,2,'.','').",'".date('d/m/Y')."',
			".number_format($mes8,2,'.','').",".number_format($mes9,2,'.','').",".number_format($mes10,2,'.','').",".number_format($mes11,2,'.','').",".number_format($mes12,2,'.','').")";
			$resultw=mssql_query($sql_det,$link);		
			//echo '<br><br>'.$sql_det;
			
			$sql_anual="INSERT INTO webminpro.SALDOS_FINALES_".$year_djm." 
			VALUES(".$id_embarca.",".number_format($TotalMonto,2,'.','').",0,
			".number_format($TotalMonto,2,'.','').",
			".number_format($saldoFavor,2,'.','').",'".date('d/m/Y')."')";
			$resultw=mssql_query($sql_anual,$link);
			//echo '<br><br>'.$sql_anual;
		}

			$sql_anual_aplia="INSERT INTO webminpro.TBL_APLICACION_INTERES 
			(ANO_DJM, ID_EMB, DOBLE, TRIPLE, ESTADO)
			VALUES(".$year_djm.",".$id_embarca.",
			".number_format($ap_doble,0,'.','').",
			".number_format($ap_triple,0,'.','').",1 )";
			$resultw=mssql_query($sql_anual_aplia,$link);
			//echo '<br><br>'.$sql_anual_aplia;
		

		
}



function cuenta_gen_triple($link, $year_djm)
{
$nro_gen_triple=0;

				$sql = "SELECT COUNT(*) FROM DB_DNEPP.webminpro.TBL_APLICA_TRIPLE WHERE ANO_DJM= ".$year_djm;
				$resultw_new=mssql_query($sql,$link);		
				if (!$resultw_new){	
				//
				 }else{  
				 while( $row5_new = mssql_fetch_array( $resultw_new ) )
											{ 
														$nro_gen_triple=$row5_new[0];
											} 
											mssql_free_result($resultw_new); 	
											unset($resultw_new);
								}		

return $nro_gen_triple;
}



function cuenta_gen_nro_triple($link, $year_djm, $er)
{
$nro_gen_triple=0;

if($er==1){ 	$str_cuenta=" 	and  year(convert(datetime, fecha,103))= ".$year_djm." "; }
$year_djmd=$year_djm+1;
if($er==2){ 	$str_cuenta=" 	and  year(convert(datetime, fecha,103))= ".$year_djmd." "; }


				$sql = "SELECT COUNT(*) FROM DB_DNEPP.webminpro.TBL_APLICA_TRIPLE WHERE ANO_DJM= ".$year_djm. " ".$str_cuenta." ";
				$resultw_new=mssql_query($sql,$link);		
				if (!$resultw_new){	
				//
				 }else{  
				 while( $row5_new = mssql_fetch_array( $resultw_new ) )
											{ 
														$nro_gen_triple=$row5_new[0];
											} 
											mssql_free_result($resultw_new); 	
											unset($resultw_new);
								}		

return $nro_gen_triple;
}




function evalua_pago_doble_antes_2dotriple($link, $id_embarca, $year_djm, $mes_djm, $saldo_doble, $ipa_trip)
{

	$mes_djm_eva=$mes_djm+3;
	$ano_djm_eva=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	
	if($mes_djm>9)
	{ 
		if($mes_djm==10){ $mes_djm_eva='01';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==11){ $mes_djm_eva='02';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==12){ $mes_djm_eva='03';  $ano_djm_eva=$ano_djm_eva+1;}
	}
		
	$fecha_ini_triple= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;

	$imp_pagados_ant_triple=0;
	
		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)>=convert(datetime,CONVERT(VARCHAR(10),'".$fecha_ini_triple."',103),103)	
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
				//echo '<BR>'.$sql;
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				}else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$imp_pagados_ant_triple=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

//echo '<br>'.$saldo_doble.'  -   '.$imp_pagados_ant_triple;

$saldo_doble_ant_triple=$saldo_doble-$imp_pagados_ant_triple;
//echo '<br>'.$saldo_doble_ant_triple.'';
return $saldo_doble_ant_triple;
}



function evalua_pago_doble_antes_triple($link, $id_embarca, $year_djm, $mes_djm, $saldo_doble, $ipa_trip)
{
				$cont_fe=0;
				$FECHA_TRIPLE='';
				$sql_fech = "SELECT FECHA FROM DB_DNEPP.webminpro.TBL_APLICA_TRIPLE WHERE ANO_DJM= ".$year_djm." ORDER BY CONVERT(DATETIME, FECHA,103) ASC " ;
				$resultw_new=mssql_query($sql_fech,$link);		
				if (!$resultw_new){	
				//
				 			}else{  
								while( $row5_new = mssql_fetch_array( $resultw_new ) )
											{ 
												$cont_fe=$cont_fe+1;
												if($cont_fe==$ipa_trip){ $FECHA_TRIPLE=$row5_new[0]; }
												if($cont_fe==$ipa_trip){ $FECHA_TRIPLE=$row5_new[0]; }
												if($cont_fe==$ipa_trip){ $FECHA_TRIPLE=$row5_new[0]; }
												if($cont_fe==$ipa_trip){ $FECHA_TRIPLE=$row5_new[0]; }												
											} 
											mssql_free_result($resultw_new); 	
											unset($resultw_new);
								}		
								

	$mes_djm_eva=$mes_djm+3;
	$ano_djm_eva=$year_djm;
		
	if($mes_djm_eva<10){ $mes_djm_eva='0'.$mes_djm_eva; }
	
	if($mes_djm>9)
	{ 
		if($mes_djm==10){ $mes_djm_eva='01';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==11){ $mes_djm_eva='02';  $ano_djm_eva=$ano_djm_eva+1;}
		if($mes_djm==12){ $mes_djm_eva='03';  $ano_djm_eva=$ano_djm_eva+1;}
	}
		
	$fecha_ini_triple= '01/'.$mes_djm_eva.'/'.$ano_djm_eva;

	$imp_pagados_ant_triple=0;
	
		$sql="	SELECT sum(IMPORTE_IP)  
				from IMPORTE_PAGADO
				where ID_ANYOPD=".$year_djm." AND id_mpd=".$mes_djm." AND 
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)>=
				convert(datetime,CONVERT(VARCHAR(10),'".$fecha_ini_triple."',103),103)
				AND
				convert(datetime,CONVERT(VARCHAR(10),'".$FECHA_TRIPLE."',103),103)>
				convert(datetime,CONVERT(VARCHAR(10),feccanc_ip,103),103)				
				AND FLAG_EXPORT=1	and id_embdjm=".$id_embarca." 
				group by id_mpd
				";
				//echo '<BR>'.$sql;
				$resultw=mssql_query($sql,$link);		
				
				if (!$resultw){	
				//
				}else{  
											while( $row5 = mssql_fetch_array( $resultw ) )
											{ 
												$imp_pagados_ant_triple=$row5[0];
											} 
											mssql_free_result($resultw); 	
											unset($resultw);
								}	

//echo '<br>'.$saldo_doble.'  -   '.$imp_pagados_ant_triple;

$saldo_doble_ant_triple=$saldo_doble-$imp_pagados_ant_triple;
//echo '<br>'.$saldo_doble_ant_triple.'';
return $saldo_doble_ant_triple;
}


}





?>
