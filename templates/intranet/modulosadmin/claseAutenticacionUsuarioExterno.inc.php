<?php
//require('auth.php');
require_once('claseModulos.inc.php');

class AutenticacionUsuarioExterno extends Modulos{

	function AutenticacionUsuarioExterno(){
		// Asigna Parametros de Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][0];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
	}

	function checkIP(){
		$ip = $_SERVER['REMOTE_ADDR'];
		if ($ip == "192.168.100.7" || $ip == "172.16.210.135" || $ip == "172.16.210.139" ) {
			return true;
		}else{
			return false;
		}
	}

	function validaIntento(){
		if(!(isset($_REQUEST["numero_intento"]))){
			$_REQUEST["numero_intento"]=0;
			return false;
		}
		if ($this->autentica()){
			return true;
		}
		return false;		
	}
	
	function autentica(){
		if (isset($_REQUEST["codigo"],$_REQUEST["numero_intento"],$_POST['idquest'], $_POST['answer'])){
			$idusr= $_REQUEST['uname'];
			$codigo=$_REQUEST['codigo'];
		}else{	
			$_REQUEST['mensaje_error']="Error, vuelva a intentar";	
			return false;
		}	
		$conexion = pg_pconnect("host=localhost port=5432 dbname=intranet user=postgres password=gonta_patan");
		if (!$conexion) {
			 $_REQUEST['mensaje_error']="<CENTER>Problemas de conexion, favor intente despu�s de unos minutos.</CENTER>";
			 return false;
		}
		$sql_st="SELECT * FROM autenticacion_vpn WHERE id='$idusr'";
		$rs = pg_Exec ($conexion, $sql_st);
		unset($sql_st);
		$rows = pg_NumRows($rs);
		
		if ($rows !=0 ){	
			$id=pg_result($rs, 0, 0);
			$cod=pg_result($rs, 0, 5);
			$bloqueado=pg_result($rs, 0, 1);
			$n_intento=pg_result($rs, 0, 3);
			$permitido=pg_result($rs, 0, 4);
			$n_intento++;
			$_REQUEST["numero_intento"]=$n_intento;
		}else{
			$_REQUEST['mensaje_error']="Error en autenticacion, intente nuevamente";
			return false;
		}
		if ($n_intento >= 100){
			$sql_st="UPDATE autenticacion_vpn SET bloqueado=true WHERE id='$idusr'";
			$rs = pg_Exec ($conexion, $sql_st);
			unset($sql_st);
			unset($rs);
			if ($bloqueado=='t'){
				$_REQUEST['mensaje_error']="El usuario se encuentra bloqueado";
				return false;
			}
			$_REQUEST['mensaje_error']="El usuario se encuentra bloqueado";
			return false;
		}
		$codigoMd5=md5("$idusr"."$codigo");
		if($idusr == $id && $codigoMd5 == $cod ){
			if ($permitido == 'f'){
				$_REQUEST['mensaje_error']="<CENTER>Ud. no tiene permiso para iniciar sesion,".
                                           "p�ngase en contacto con el <a href=\"mailto:intranet@CONVENIO_SITRADOC.gob.pe?subject=Prblema de usuario - contrase�a\">administrador</a> .</CENTER>";
				return false;
			}
			$statQuest = $this->getSecureAnswer($_POST['uname'], $_POST['idquest'], $_POST['answer']);
			if (!$statQuest){
				$_REQUEST['mensaje_error']="Error: No es una respuesta v�lida";
				return false;
			}
			if (!empty($_POST["code_val"]) && !empty($_POST["code"])){
				$captcha = $_POST["code_val"];
				// Valido el codigo CAPTCHA ingresado y lo comparo contra el almacenado en la sesion.
				$captcha = sha1($captcha);
				if($captcha != $_POST["code"]) {
					$_REQUEST['mensaje_error']="Error: C&oacute;digo de validaci&oacute;n incorrecto ";
					return false;
				}
			}else {
				$_REQUEST['mensaje_error']="Error: Ingrese su codigo de validacion ";
				return false; 
			}
			$sql_st="UPDATE autenticacion_vpn SET bloqueado=false WHERE id='$idusr'";
			$rs = pg_Exec ($conexion, $sql_st);
			$sql_st="UPDATE autenticacion_vpn SET n_intento=0 WHERE id='$idusr'";
			$rs = pg_Exec ($conexion, $sql_st);
			return true;
		}else{
			$_REQUEST['mensaje_error']="Error en autenticacion, favor verifique sus datos";
			$sql_st="UPDATE autenticacion_vpn SET n_intento=$n_intento WHERE id='$idusr'";
			$rs = pg_Exec ($conexion, $sql_st);
			unset($sql_st);
			unset($rs);
			return false;
		}
		pg_close($conexion);
		unset($conexion);        
	} 

	/* ----------------------------------------------------- */
	/* Funcion que entrega una Pregunta para el registro	 */
	/* de forma Aleatoria									 */
	/* ----------------------------------------------------- */
	/* CLASE REGISTRO PERSONAL								 */
	/* ----------------------------------------------------- */

	function getSecureQuestion(){
		if(is_null($idquest)){
			$quest['id'] = mt_rand(1,9);
		}else
			$quest['id'] = $idquest;

		switch($quest['id']){
    		Case 1: 
				$quest['text'] = "�Suma de d�gitos de tu a�o de Nacimiento?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="2" class="ip-login">';
				break;
    		Case 2:
				$quest['text'] = "�Suma de d�gitos de tu mes de Nacimiento?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="1" class="ip-login">';
				break;
    		Case 3: 
				$quest['text'] = "�Suma de d�gitos de tu d�a de Nacimiento?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="2" class="ip-login">';
				break;
    		Case 4: 
				$quest['text'] = "�Cu�l es el mes de tu Nacimiento?";
				$quest['html'] = '<select name="answer" class="ip-login">'.$this->ObjFrmMes(1, 12, true, NULL, true).'</select>';				
				break;
     		Case 5: 
				$quest['text'] = "�Cu�l es el d�a de tu Nacimiento?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="2" class="ip-login">';
				break;
     		Case 6: 
				$quest['text'] = "�Cu�les son los primeros 3 d�gitos de DNI?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="3" class="ip-login">';
				break;
     		Case 7: 
				$quest['text'] = "�Cu�les son los �ltimos 3 d�gitos de DNI?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="3" class="ip-login">';
				break;
 	    	Case 8: 
				$quest['text'] = "�Cu�l es la suma de los 3 �ltimos d�gitos de tu tel�fono?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="2" class="ip-login">';
				break;
     		Case 9: 
				$quest['text'] = "�Cu�l es la suma de los 3 primeros d�gitos de tu tel�fono?";
				$quest['html'] = '<input type="text" name="answer" size="10" maxlength="2" class="ip-login">';
				break;
		}
		return $quest;
	}
	
	/* ----------------------------------------------------- */
	/* Funcion que obtiene y califica la respuesta de la	 */
	/* pregunta de Registro									 */
	/* ----------------------------------------------------- */
	/* CLASE REGISTRO PERSONAL								 */
	/* ----------------------------------------------------- */

	function getSecureAnswer($codusr, $idquest, $usr_answer){
		$this->abreConnDB();

		$sql_st="SELECT CONVERT(char(10),fecha_nacimiento,103), dni, telefono ".
				"FROM dbo.h_trabajador ".
				"WHERE email='$codusr'";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$t_fecha = explode("/",$row[0]);
				$t_dni = $row[1];
				$t_telefono = $row[2];
			}
			unset($row);
			$rs->Close();
		}

		switch($idquest){
    		Case 1:
				$answer = 0;
				for($i=0;$i<strlen($t_fecha[2]);$i++)
					$answer += (integer) $t_fecha[2]{$i};
				$usr_answer = (integer) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
    		Case 2:
				$answer = 0;
				for($i=0;$i<strlen($t_fecha[1]);$i++)
					$answer += $t_fecha[1]{$i};
				$usr_answer = (integer) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
    		Case 3: 
				$answer = 0;
				for($i=0;$i<strlen($t_fecha[0]);$i++)
					$answer += (integer) $t_fecha[0]{$i};
				$usr_answer = (integer) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
    		Case 4: 
				$answer = $t_fecha[1];
				$usr_answer = sprintf("%02d",$usr_answer);
				$result = ($answer == $usr_answer) ? TRUE : FALSE;
				break;
     		Case 5: 
				$answer = $t_fecha[0];
				$usr_answer = sprintf("%02d",$usr_answer);
				$result = ($answer == $usr_answer) ? TRUE : FALSE;
				break;
     		Case 6:
				$answer = substr($t_dni, 0, 3);
				$usr_answer = (string) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
     		Case 7: 
				$answer = substr($t_dni, -3);
				$usr_answer = (string) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
 	    	Case 8: 
				$answer = 0;
				$tmp_answer = substr($t_telefono, -3);
				for($i=0;$i<strlen($tmp_answer);$i++)
					$answer += (integer) $tmp_answer{$i};
				$usr_answer = (integer) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
     		Case 9: 
				$answer = 0;
				$tmp_answer = substr($t_telefono, 0, 3);
				for($i=0;$i<strlen($tmp_answer);$i++)
					$answer += (integer) $tmp_answer{$i};
				$usr_answer = (integer) $usr_answer;
				$result = ($usr_answer == $answer) ? TRUE : FALSE;
				break;
		}
		return $result;
	}	

	/* ----------------------------------------------------- */
	/* Funcion que muestra la Interfaz de Registro		 	 */
	/* Entrada o Salida
	/* ----------------------------------------------------- */
	/* CLASE REGISTRO PERSONAL								 */
	/* ----------------------------------------------------- */
	function muestraRegistroHTML(){
		$quest = $this->getSecureQuestion();
?>
    	<input type="hidden" name="numAnswers" value="<?echo $numAnswers;?>">
    	<input type="hidden" name="idquest" value="<?=$quest['id'];?>">
		<td class="td-lb-login"><b><?=$quest['text'];?></b></td>
        <td ><?=$quest['html']; ?>
        </td>
<?
	}
}	
?>
