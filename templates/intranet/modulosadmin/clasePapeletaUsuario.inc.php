<?
require_once('clasePapeletas.inc.php');

class PapeletasUsuario extends Papeletas{

	/* ------------------------------------------------------------- */
	/* Constructor													 */
	/* ------------------------------------------------------------- */
	/* CLASE PAPELETAS USUARIO										 */
	/* ------------------------------------------------------------- */
	function PapeletasUsuario($menu){
		// Asigna Parametros de Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][13];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Asigna Valores al Array de Datos de Usuario
		$this->datosUsuarioMSSQL();

		// Acciones Permitidas en la Aplicacion
		$this->arr_accion = array(
							MUESTRA_SUMARIO => 'viewSumary',
							FRM_CREA_PAPELETA => 'newPapeleta',
							CREA_PAPELETA => 'sendPapeleta',
							FRM_BUSCA_PAPELETA => 'frmSearch',
							BUSCA_PAPELETA => 'searchPapeleta',
							FRM_GENERA_REPORTE => 'frmReport',
							GENERA_REPORTE => 'newReport',
							IMPRIMIR_PAPELETA => 'printPapeleta',
							MUESTRA_OBSERVACION => 'showObservacion',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'							
							);
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'viewSumary', label => 'SUMARIO' ),
		                    1 => array ( 'val' => 'newPapeleta', label => 'CREAR' ),
							2 => array ( 'val' => 'frmSearch', label => 'BUSCAR' ),
							3 => array ( 'val' => 'frmReport', label => 'REPORTE' )
							);
		
		// Menu Seleccionado
		$this->menuPager = $menu;
	}
	
	function ValidaCamposPapeleta($desDestino,$fecInicio,$fecFin,$indDiaEntero, $codMotivo){
		// Clasifica los Mensajes de Error si los hubiera
		$errors = '';
		if(!$desDestino)
			$errors .= "- Destino es un dato Obligatorio.<br>";
		if( !checkdate(sprintf('%d',substr($fecInicio,0,2)),sprintf('%d',substr($fecInicio,3,2)),substr($fecInicio,6,4)) )
			$errors .= "- La Fecha de Inicio no es Valida.<br>";
		if( !checkdate(sprintf('%d',substr($fecFin,0,2)),sprintf('%d',substr($fecFin,3,2)),substr($fecFin,6,4)) )
			$errors .= "- La Fecha de Fin no es Valida.<br>";
		if(!($indDiaEntero) && strtotime($fecInicio)>=strtotime($fecFin) )
			$errors .= "- El Inicio de la Actividad debe ser menor al Fin.<br>";

		$this->abreConnDB();
		// $this->conn->debug = true;
		$codigo_trabajador= $this->userIntranet['CODIGO'];
		//echo '<br>'; 
		//echo 'CONDICION'.$_SESSION['cod_usuario'];exit;

		$sql_st = sprintf("EXECUTE validaFechasPapeleta2011 '%s','%s',%d,%d,%d,%d,'%s','%s','%s','%s'",
						  ($indDiaEntero) ? substr($fecInicio,0,10) : $fecInicio,
						  ($indDiaEntero) ? substr($fecFin,0,10) : $fecFin,
						  ($indDiaEntero) ? 1 : 0,
						  $this->minTimeBeforeSent,
						  $this->maxTimeBeforeSent,
						  $this->maxTimePapeleta,
						  sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMinu),
						  sprintf('%02d:%02d',$this->endWorkHour,$this->endWorkMinu),
						  $codigo_trabajador,
						  $codMotivo
						  );
						//echo $sql_st;
						//exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
			
			/*echo '<br>';
			echo $row[1].'-<br>';
			echo $row[2].'-<br>';
			echo $row[3].'-<br>';
			echo $row[4].'-<br>';
			echo $row[5].'-<br>';
			echo $row[6].'-<br>';
			echo $row[7].'-<br>';
			echo $row[8].'-<br>';
			echo $row[9].'-<br>';
			echo $row[10].'-<br>';
			echo $row[11].'-<br>';
			echo $row[12].'-<br>';*/
		
								
				if($row[1]=='1')
				{
				$errors .= "- Ud. esta solicitando una Papeleta fuera del rango de tiempo permitido(Seg�n la Condici�n del Personal).<br>";
				}
				if($row[2]=='1')
				{
				$errors .= "- El Tiempo de Permiso de Papeleta pedido es muy largo (M�ximo {$this->maxTimePapeleta} horas)<br>";
				}				
			
				if($row[3]=='1')
				{
				$errors .= "- La Fecha de Inicio no puede ser mayor a la Fecha de Fin<br>";
				}		
				if($row[4]=='1')
				{
				$errors .= "- La Hora de la Fecha de Inicio es Menor a la Hora de Entrada (" . sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMinu) . ")<br>";
				}		


				if($row[8]=='1')
				{
				$errors .= "- El CAS no puede registrar un Permiso por Onom�stico.<br>";
				}		


				if($row[9]=='1')
				{
				$errors .= "- La(s) fecha(s) seleccionada(s) es(son) d�a(s) S�bado o Domingo o feriado.<br>";
				}		

				if($row[10]=='1')
				{
				$errors .= "- Los permisos solo son para m�ximo 3 d�as.<br>";
				}		


				if($row[11]=='1')
				{
				$errors .= "- Los permisos no pueden ser de una semana a otra(generar una papeleta por semana).<br>";
				}		
				
				
				if($row[12]=='1')
				{
				$errors .= "- El primer o �ltimo d�a no debe ser feriado.<br>";
				}						
							
							
							
							
				/*
				foreach(explode(',',$row[0]) as $error){
					switch($error){
						case 1:
							$errors .= "- Ud. esta solicitando una Papeleta fuera del rango de tiempo permitido (M�nimo {$this->minTimeBeforeSent} Minuto, M�ximo " . $this->maxTimeBeforeSent/(24*60) . " Dias antes de la Fecha Actual).<br>";
							break;
						case 2:
							$errors .= "- El Tiempo de Permiso de Papeleta pedido es muy largo (M�ximo {$this->maxTimePapeleta} horas)<br>";
							break;
						case 3:
							$errors .= "- La Fecha de Inicio no puede ser mayor a la Fecha de Fin<br>";
							break;
						case 4:
							$errors .= "- La Hora de la Fecha de Inicio es Menor a la Hora de Entrada (" . sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMinu) . ")<br>";
							break;
						case 5:
							$errors .= "- La Hora de la Fecha de Inicio no puede ser mayor o igual la la Hora de Salida (" . sprintf('%02d:%02d',$this->endWorkHour,$this->endWorkMinu) . ") <br>";
							break;
						case 6:
							$errors .= "- La Hora de la Fecha de Termino no puede ser mayor a la Hora de Salida (" . sprintf('%02d:%02d',$this->endWorkHour,$this->endWorkMinu) . ")<br>";
							break;
						case 7:
							$errors .= "- La Hora de la Fecha de Termino no puede ser menor o igual la la Hora de Salida (" . sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMinu) . ") <br>";
							break;
						}
					}*/
					
					//echo $errors;exit;
			}
			$rs->Close();
		}
			
			

		$ID_REGIMEN_TRABAJADOR=0;
		$sql_st = "SELECT ISNULL(ID_REGIMEN,0) FROM DB_GENERAL.dbo.H_TRABAJADOR
					WHERE CODIGO_TRABAJADOR=".$codigo_trabajador." ";
						//echo $sql_st;
						//exit;
		$rs2 = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs2)
			print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row2 = $rs2->FetchRow()) {
					$ID_REGIMEN_TRABAJADOR=$row2[0];
			}
			$rs2->Close();
		}
			if($ID_REGIMEN_TRABAJADOR==0 || $ID_REGIMEN_TRABAJADOR==4)
			{
				$errors .= "- El R�gimen del trabajador no esta contemplado para el registro de Papeletas.<br>";
			}
		// Si hay errores
		$this->errors .= $errors;
		return ($errors) ? true : false;
	}
	
	function MuestraStatTrans($accion){
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());

		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		//$html->display('papeletas/headerArm.tpl.php');
		$html->display('papeletas/showStatTrans.inc.php');
		$html->display('papeletas/footerArm.tpl.php');
	}

	function SumarioPapeleta(){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmStatus';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		$html->assign_by_ref('statPapeleta',$this->statPapeleta);
		
		$html->assign('nCurso',$this->obtienePapeletasEnCurso($this->statPapeleta[EN_CURSO]));
		$html->assign('nAprobado',$this->obtienePapeletasEnCurso($this->statPapeleta[APROBADO]));
		$html->assign('nDesaprobado',$this->obtienePapeletasEnCurso($this->statPapeleta[DESAPROBADO]));
		
		$html->display('papeletas/usuario/sumario.tpl.php');
	}
	
	function MuestraIndex(){
		$this->SumarioPapeleta();
	}

	function FrmCreaPapeleta($codMotivo=NULL,$fecInicio=NULL,$fecFin=NULL,$indDiaEntero=NULL,$desDestino=NULL,$desAsunto=NULL,$errors=NULL){
		//Manipulacion de las Fechas de Ingreso de la Actividad
		$fecInicio = ($fecInicio!='// :'&&$fecInicio) ? $fecInicio : date('m/d/Y');
		$fecFin = ($fecFin!='// :'&&$fecFin) ? $fecFin : date('m/d/Y');

		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmCreaPapeleta';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('errors',$errors);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		
		$id_regimen_entrada=$this->userIntranet['REGIMEN_ENTRADA'];
		
		//echo $id_regimen_entrada;

		if($id_regimen_entrada==1){ $str_motivos="'0007','0003','0038','0037','0041'"; }		
		if($id_regimen_entrada==2){ $str_motivos="'0007','0003','0037','0041'"; }
		if($id_regimen_entrada==3){ $str_motivos="'0007','0003','0038','0037','0041'"; }				
		
		if($id_regimen_entrada==0){ $str_motivos="('----')"; }
		if($id_regimen_entrada==4){ $str_motivos="('----')"; }		
		
		
		// Setea el Campo de los Motivos de Ausencia
		

		$sql_st = "SELECT codigo_motivo_ausencia, desc_motivo_ausencia ".
				  "FROM integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia ".
				  "WHERE 
				  codigo_motivo_ausencia in (".$str_motivos.") ".
				  "ORDER BY 2";


/* -- codigo_motivo_ausencia in (" . $this->motPapPerm . ") ".				  */


				  
		$html->assign('codMotivo',$this->ObjFrmSelect($sql_st, $codMotivo, true, true, array('val'=>'NULL','label'=>'Seleccione un Motivo')));
		unset($sql_st);
			
		// Setea los Campos de la Fecha/Hora de Inicio
		$html->assign('selMesIni',$this->ObjFrmMes(1, 12, true, substr($fecInicio,0,2), true));
		$html->assign('selDiaIni',$this->ObjFrmDia(1, 31, substr($fecInicio,3,2), true));
		$anyoIni = substr($fecInicio,6,4);
		$html->assign('selAnyoIni',$this->ObjFrmAnyo($anyoIni, $anyoIni+1, $anyoIni, true));
		$html->assign('selHoraIni',$this->ObjFrmHora($this->startWorkHour, $this->endWorkHour, true, (substr($fecInicio,11,2)) ? substr($fecInicio,11,2) : $this->horaIni, true));
		$html->assign('selMinIni',$this->ObjFrmMinuto($this->intervalMinu, true, (substr($fecInicio,14,2)) ? substr($fecInicio,11,2) : NULL, true));
		// Setea Campos Fecha de Fin
		$html->assign('selMesFin',$this->ObjFrmMes(1, 12, true, substr($fecFin,0,2), true));
		$html->assign('selDiaFin',$this->ObjFrmDia(1, 31, substr($fecFin,3,2), true));
		$anyoFin = substr($fecFin,6,4);
		$html->assign('selAnyoFin',$this->ObjFrmAnyo($anyoFin, $anyoFin+1, $anyoFin, true));
		$html->assign('selHoraFin',$this->ObjFrmHora($this->startWorkHour, $this->endWorkHour, true, (substr($fecFin,11,2)) ? substr($fecFin,11,2) : $this->horaIni, true));
		$html->assign('selMinFin',$this->ObjFrmMinuto($this->intervalMinu, true, (substr($fecFin,14,2)) ? substr($fecFin,14,2) : NULL, true));

		$html->assign_by_ref('indDiaEntero',$indDiaEntero);
		$html->assign_by_ref('desDestino',$desDestino);
		$html->assign_by_ref('desAsunto',$desAsunto);
		
		
		$fecInicio_text=$_REQUEST['xfecha_inicio_pape'];
		if($fecInicio_text=="")
		{
			$fecInicio_text=date('d/m/Y');
		}
		
		$fectermino_text=$_REQUEST['xfecha_termino_pape'];
		if($fectermino_text=="")
		{
			$fectermino_text=date('d/m/Y');
		}		
		
		$html->assign_by_ref('fecha_inicial_new',$fecInicio_text);		
		
		$html->assign_by_ref('fecha_dd_inicial_new',substr($fecInicio_text,0,2));		
		$html->assign_by_ref('fecha_mm_inicial_new',substr($fecInicio_text,3,2));		
		$html->assign_by_ref('fecha_yy_inicial_new',substr($fecInicio_text,6,4));		
		
		$html->assign_by_ref('fecha_termino_new',$fectermino_text);				
		
		$html->assign_by_ref('fecha_dd_termino_new',substr($fectermino_text,0,2));		
		$html->assign_by_ref('fecha_mm_termino_new',substr($fectermino_text,3,2));		
		$html->assign_by_ref('fecha_yy_termino_new',substr($fectermino_text,6,4));		
		
		
		$html->display('papeletas/usuario/create.tpl.php');

	}
			
	function CreaPapeleta($codMotivo,$fecInicio,$fecFin,$indDiaEntero,$desDestino,$desAsunto){

	

		if($this->ValidaCamposPapeleta($desDestino,$fecInicio,$fecFin,$indDiaEntero, $codMotivo)){
			$objIntranet = new Intranet();
			$objIntranet->Header('Papeletas de Asistencia');
			$objIntranet->Body('papusuario_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors.$errors,true);
			$this->FrmCreaPapeleta($codMotivo,$fecInicio,$fecFin,$indDiaEntero,$desDestino,$desAsunto,$errors);
			$objIntranet->Footer();
		}else{
			// Recorta el Campo de Hora si es que indDiaEntero esta Activo
			$fecInicio = ($indDiaEntero) ? substr($fecInicio,0,10) : $fecInicio;
			$fecFin = ($indDiaEntero) ? substr($fecFin,0,10) : $fecFin;
			
			$this->abreConnDB();
			// $this->conn->debug = true;
			
			// Inicia la Transaccion
			$this->conn->BeginTrans(); 
			
			$ins_st = sprintf("EXECUTE insPapeletaAsistencia %d,'%s','%s','%s',%d,'%s','%s',%d,%d,%d,'%s','%s'",
							  $this->userIntranet['CODIGO'],
							  $codMotivo,
							  ($indDiaEntero) ? substr($fecInicio,0,10) : $fecInicio,
							  ($indDiaEntero) ? substr($fecFin,0,10) : $fecFin,
							  ($indDiaEntero) ? 1 : 0,
							  $this->PrepareParamSQL($desDestino),
							  $this->PrepareParamSQL($desAsunto),
							  $this->minTimeBeforeSent,
							  $this->maxTimeBeforeSent,
							  $this->maxTimePapeleta,
							  sprintf('%02d:%02d',$this->startWorkHour,$this->startWorkMinu),
							  sprintf('%02d:%02d',$this->endWorkHour,$this->endWorkMinu));
							
							//echo $ins_st;
							  
			$rs = & $this->conn->Execute($ins_st);
			unset($ins_st);
			if (!$rs)
				$RETVAL=1;
			else{
				if($row = $rs->FetchRow()){
					$RETVAL = $row[0];
				}else
					$RETVAL = 1;
				$rs->Close();
			}
			unset($rs);

			if($RETVAL)
				$this->conn->RollbackTrans();
			else
				$this->conn->CommitTrans();

			$destination = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function ObtienePapeletasEnCurso($status){
		$this->abreConnDB();
		// $this->conn->debug = true;
		$sql_st = "SELECT COUNT(*) ".
				  "FROM dbo.papeleta ".
				  "WHERE id_estado=$status and codigo_trabajador={$this->userIntranet[CODIGO]}".
				  		" --and fecha_inicio > getdate() ";
//		echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$row = $rs->FetchRow();
			$rs->Close();
		}
		return $row[0];	
	}
	
	function FrmBuscaPapeleta($mesPap=NULL,$statPap=NULL,$pap=NULL){
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea Campos del Formulario
		$html->assign('selMesPap',$this->ObjFrmMes(1, date('n'), true, ($mesPap) ? $mesPap : date('n'), true, array('val'=>'none','label'=>'Todos')));
		
		$sql_st = 'SELECT id_estado, des_estado FROM papeleta_estado WHERE ind_activo=1';
		$html->assign('selStatPap',$this->ObjFrmSelect($sql_st, $statPap, true, true,array('val'=>'none','label'=>'Cualquiera')));
		$html->assign('nPap',(is_array($pap)) ? count($pap) : 0);
		$html->assign_by_ref('pap',$pap);
		$html->display('papeletas/usuario/frmBuscar.tpl.php');

	}
	
	function BuscaPapeleta($mesPap,$statPap,$actual=false){
		$this->abreConnDB();
		// $this->conn->debug = true;
		
		$sql_st = sprintf("SELECT p.id_papeleta, UPPER(m.desc_motivo_ausencia), ".
						 "CASE WHEN (p.ind_diaentero=1 and fecha_inicio<>fecha_termino) THEN 'Desde el '+CONVERT(varchar(10),p.fecha_inicio,103)+' hasta el '+CONVERT(varchar(10),p.fecha_termino,103) ".
							  "WHEN (p.ind_diaentero=1 and fecha_inicio=fecha_termino) THEN 'El '+CONVERT(varchar(10),p.fecha_inicio,103) ".
							  "ELSE 'Del '+CONVERT(varchar(10),p.fecha_inicio,103)+' desde las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_inicio,100),13,7))+', al '+CONVERT(varchar(10),p.fecha_termino,103)+' hasta las '+LOWER(SUBSTRING(CONVERT(varchar,fecha_termino,100),13,7)) ".
						 "END, ".
						 "p.id_estado, e.des_estado ".
				  "FROM dbo.papeleta p, dbo.papeleta_estado e, integrix_CONVENIO_SITRADOC.dbo.motivo_ausencia m ".
				  "WHERE p.codigo_motivo_ausencia=m.codigo_motivo_ausencia COLLATE database_default and p.codigo_trabajador={$this->userIntranet['CODIGO']} and ".
						"p.id_estado=e.id_estado and DATEPART(year,p.fecha_inicio) = DATEPART(year,getdate())".
						"%s%s%s ".
						" ORDER BY p.fecha_inicio DESC",						
						($statPap!='none') ? " and p.id_estado={$statPap}" : '',
						($mesPap!='none'&&$mesPap) ? sprintf(" and DATEPART(month,p.fecha_inicio)=%d",$mesPap) : "",
						($actual) ? ' and p.fecha_inicio > getdate()' : '');
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$cont=0;
			while ($row = $rs->FetchRow()){
				$pap[$cont] = array('motivo'=>$row[1],
							        'fecha'=>$row[2],
							        'estado'=>$row[4]
							);
							
				if($row[3]==$this->statPapeleta[EN_CURSO]){
					$pap[$cont]['imgStat'] = 'stat_none.gif';
					$pap[$cont]['imgOpcion'] = 'ico_blank.gif';
					$pap[$cont]['altOpcion'] = NULL;
					$pap[$cont]['lnkOpcion'] = NULL;
				}elseif($row[3]==2 || $row[3]==5){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}&prev=".$str_pre."','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

				}elseif($row[3]==4){
					$pap[$cont]['imgStat'] = 'stat_true.gif';
					$pap[$cont]['imgOpcion'] = 'ico_print.gif';
					$pap[$cont]['altOpcion'] = 'Imprimir';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[IMPRIMIR_PAPELETA]}&id={$row[0]}&prev=0','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=560,height=400'); void('')";

				}elseif($row[3]==$this->statPapeleta[DESAPROBADO]){
					$pap[$cont]['imgStat'] = 'stat_false.gif';
					$pap[$cont]['imgOpcion'] = 'ico_doc.gif';
					$pap[$cont]['altOpcion'] = 'Ver Observaciones';
					$pap[$cont]['lnkOpcion'] = "javascript: window.open('{$_SERVER['PHP_SELF']}?accion={$this->arr_accion[MUESTRA_OBSERVACION]}&id={$row[0]}&prev=1','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=220,height=150'); void('')";;
				}
				$cont++;
			}
			unset($row);
			$rs->Close();
			unset($rs);			
		}
		
		$this->FrmBuscaPapeleta($mesPap,$statPap,$pap);
	}

	function FrmReportePapeleta($fecInicio=NULL){
		// Limpia el Valor de los Parametros
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/n/Y');
		$mesRep = explode('/',$fecInicio);
		$mesRep = $mesRep[0];
		
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea Campos del Formulario
		$html->assign_by_ref('selMesRep',$this->ObjFrmMes(1, date('n')+1, true, $mesRep, true));

		$this->abreConnDB();
		// $this->conn->debug = true;
		

		//echo $this->userIntranet[CODIGO];
		$sql_st = sprintf("EXEC listRepGenXUsr2 %d, %s, %s",
						  $this->userIntranet[CODIGO],  
						  ($mesRep!='none') ? "'".$this->PrepareParamSQL(date('d/m/Y',mktime(0,0,0,$mesRep,1,date('Y'))))."'" : 'NULL',
						  ($mesRep!='none') ? "'".$this->PrepareParamSQL(date('d/m/Y',mktime(0,0,0,$mesRep+1,0,date('Y'))))."'" : 'NULL'
						  );
						  
			  
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$html->assign('nRep',$rs->RecordCount());
			while ($row = $rs->FetchRow())
				$html->append('rep', array('fecha'=> sprintf("Reporte %s del %s",(is_null($row[1])||empty($row[1])) ? '' : " de ".strftime("%B", mktime(0,0,0,$row[1],1,$row[2])),$row[2]),
										   'file'=> $row[0],
										   'fecCreacion'=> strftime("%A, %d de %B del %Y a las %H:%M %Ss",strtotime($row[3])),
										   'trabajador' => ($dep) ? $row[4] : ''));
			$rs->Close();
		}
		//echo "matrix";
		$html->display('papeletas/usuario/frmReporte.tpl.php');
		//echo "matrix2";
	}




	function FrmReportePapeletaxUsuario($fecInicio){
		// Limpia el Valor de los Parametros
		$ano_eje=$_REQUEST['ano_eje'];
		if($ano_eje==""){  $ano_eje=date('Y');	}
		$fecInicio = ($fecInicio!='//'&&$fecInicio) ? $fecInicio : date('m/n/Y');
		$mesRep = explode('/',$fecInicio);
		$mesRep = $mesRep[0];
		
		// Crea el Objeto HTML
		$html = new Smarty;
		//$html->assign_by_ref('menu',$this->GeneraMenuPager());
		//$html->assign_by_ref('menuPap',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea Campos del Formulario
		$html->assign_by_ref('selMesRep',$this->ObjFrmMes(1, date('n')+1, true, $mesRep, true));

		$this->abreConnDB();
		// $this->conn->debug = true;
		

		//echo $this->userIntranet[CODIGO];
		/*
		$sql_st = sprintf("EXEC listRepGenXUsr2 %d, %s, %s",
						  $this->userIntranet[CODIGO],  
						  ($mesRep!='none') ? "'".$this->PrepareParamSQL(date('d/m/Y',mktime(0,0,0,$mesRep,1,date('Y'))))."'" : 'NULL',
						  ($mesRep!='none') ? "'".$this->PrepareParamSQL(date('d/m/Y',mktime(0,0,0,$mesRep+1,0,date('Y'))))."'" : 'NULL'
						  );*/
		$cod_user=$this->userIntranet[CODIGO] ; 
		$sql_st = sprintf("EXEC listRepGenXUsr %d, %s, %s",
						  $cod_user,  
						  $ano_eje,
						  $mesRep
						  );

		
		//echo $sql_st;
								  
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			$html->assign('nRep',$rs->RecordCount());
			while ($row = $rs->FetchRow())
				$html->append('rep', array('fecha'=> sprintf("Reporte del a�o %s del mes de %s",$row[1], $row[2]),
										   'file'=> $row[0],
										   'fecCreacion'=> strftime("%A, %d de %B del %Y a las %H:%M %Ss",strtotime($row[3])),
										   'trabajador' => ($dep) ? $row[4] : ''));
			$rs->Close();
		}
		//echo "matrix";
		$html->display('papeletas/usuario/frmReporte.tpl.php');
		//echo "matrix2";
	}


		
	function GeneraReporteUsuario($fecInicio,$fecFin){
		//echo "hola";exit;
				$ano_eje=$_REQUEST['ano_eje'];
				if($ano_eje==""){  $ano_eje=date('Y');	}
		
		/*if($this->userIntranet[CODIGO]==710)
		{*/
			
			//echo '..'.$_POST['accion'].'..';
				$this->GeneraReportePapeletaUsuario($this->userIntranet[CODIGO],$fecInicio,1, $ano_eje);
				$this->FrmReportePapeletaxUsuario($fecInicio);
					
		/*}else{
				if($this->GeneraReportePapeleta($this->userIntranet[CODIGO],false,$fecInicio,$fecFin,1))
					$this->FrmReportePapeleta($fecInicio);		
		}*/
		

	}	
}
?>