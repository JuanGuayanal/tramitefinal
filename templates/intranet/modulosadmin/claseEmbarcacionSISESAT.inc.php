<?php
//putenv("ORACLE_SID=PROD");
//putenv("ORACLE_HOME=/u01/app/oracle/product/10.1.0/client_1/");
//putenv("TNS_ADMIN=/u01/app/oracle/product/10.1.0/client_1/network/admin/");
//putenv("LD_LIBRARY_PATH=/u01/app/oracle/product/10.1.0/client_1/lib");

include_once 'claseModulos.inc.php';

class EmbarcacionSISESAT extends Modulos {

    function EmbarcacionSISESAT(){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['oracle'];
		$this->DB = $this->arr_DB['oracle'][1];
		$this->userDB = $this->arr_userDB['oracle'][1];
		$this->passDB = $this->arr_passDB['oracle'][1];
    }

 	function ModificaTransmisor($idEmb,$numTrans){
		$error = -1;

		$this->abreConnDB();
		 //$this->conn->debug = true;
		
	    // For oracle, Prepare and PrepareSP are identical 
    	$sql_SP = $this->conn->PrepareSP('BEGIN upd_id_equipo(:error, :id_emb, :id_equipo_new); END;'); 
		$this->conn->Parameter($sql_SP,$error,'error',true);
		$this->conn->Parameter($sql_SP,$idEmb,'id_emb');
		$this->conn->Parameter($sql_SP,$numTrans,'id_equipo_new');
		$this->conn->Execute($sql_SP); 

		return $error;
	}

 	function RetiraTransmisor($idEmb){
		$error = -1;

		$this->abreConnDB();
		 //$this->conn->debug = true;
		
	    // For oracle, Prepare and PrepareSP are identical 
    	$sql_SP = $this->conn->PrepareSP('BEGIN ret_id_equipo(:error, :id_emb); END;'); 
		$this->conn->Parameter($sql_SP,$error,'error',true);
		$this->conn->Parameter($sql_SP,$idEmb,'id_emb');
		$this->conn->Execute($sql_SP); 
		
		return $error;
	}
}
?>
