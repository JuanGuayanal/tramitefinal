<?
require_once('claseModulos.inc.php');

class Exposiciones extends Modulos{

	/* ----------------------------------------------------- */
	/* Constructor											 */
	/* ----------------------------------------------------- */
	/* MODULO EXPOSICIONES									 */
	/* ----------------------------------------------------- */
	
	function Exposiciones(){
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Listar todas las Exposiciones Existentes (Index) */
	/* ------------------------------------------------------------- */
	/* MODULO EXPOSICIONES											 */
	/* ------------------------------------------------------------- */
	
	function muestraGrupoIndexHTML($cod_grupo=NULL){
	
	}	



	/*****************************************************************/
	/* Funcion que Lista las Exposiciones Existentes                 */
	/* Modulo Exposiciones  	 								     */
	/*****************************************************************/

	function muestraExposicionIndexHTML($txt_busca=NULL){
		$frmName = "frmBusca";
?>
<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
	<table width="540" border="0" cellspacing="0" cellpadding="2">
	  <tr> 
		<td> 
		  <hr width="100%" size="1">
		</td>
	  </tr>
	  <tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>				
            <td class="td-lb-login" width="360">
<?
		if($_SESSION['mod_ind_insertar']){
?>
              <img src="/img/800x600/ico-mas.gif" width="16" height="22" align="absmiddle"><a href="<? echo "{$_SERVER['PHP_SELF']}?accion=".ACCION_INSERTAR_FALSE; ?>"><b>AGREGAR EXPOSICI&Oacute;N</b></a> 
<?
		}else
			echo "&nbsp";
?>
              <input type="hidden" name="accion" value="<?echo ACCION_BUSCAR_TRUE;?>">
                </td>
				<td align="right" width="100">				  
                  <input type="text" name="txt_busca" class="ip-login contenido" value="<?echo $txt_busca?>">
				</td>
				<td align="center" width="80"> 
				  <input type="submit" value="Buscar" class="but-login">
				</td>
			  <td>&nbsp;</td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr> 
		<td> 
		  <hr width="100%" size="1">
		</td>
	  </tr>
	  <tr> 
		
      <td>&nbsp; </td>
	  </tr>
<?
		$txt_busca = strtoupper($txt_busca);
	
                // Evalua que no contenga sentencias SQL
                $this->evaluaNoSql($txt_busca);
	
		$sql_st =  "SELECT e.id_exposicion, e.tit_exposicion, d.des_dependencia, sd.des_subdependencia, sd.ord_subdependencia, e.des_autor, e.des_lugar, to_char(e.fec_exposicion,'DD-MM-YYYY'), e.fln_exposicion, e.ind_activo ";
		$sql_st .= "FROM exposicion e, dependencia d, subdependencia sd ";
		$sql_st .= "WHERE e.id_subdependencia=sd.id_subdependencia and d.id_dependencia=sd.id_dependencia ";
		$sql_st .= (!empty($txt_busca)||!is_null($txt_busca)) ? "and (UPPER(e.tit_exposicion) like '%${txt_busca}%' or UPPER(e.des_autor) like '%${txt_busca}%' or UPPER(e.des_lugar) like '%${txt_busca}%') " : "";
		$sql_st .= (!$_SESSION['mod_ind_insertar']&&!$_SESSION['mod_ind_modificar']) ? "and e.ind_activo IS true " : "";
		$sql_st .= "ORDER BY 2";
		
		$this->abreConnDB();
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()){

?>
	  <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="15" valign="top" class="contenido" align="center"> 
              <div align="center"><img src="/img/800x600/bullet.gif" width="10" height="8" vspace="3"> 
              </div>
            </td>
            <td class="contenido" valign="top" width="400"> 
              <table width="100%" border="0" cellspacing="1" cellpadding="0">
                <tr> 
                  <td colspan="2" width="441" class="item"><a href="<? echo "{$_SERVER['PHP_SELF']}?accion=" . ACCION_VER_FALSE . "&id={$row[0]}"; ?>"><b> 
                    <? echo $row[1]; ?>
                    </b></a></td>
                </tr>
                <tr> 
                  <td class="sub-item" width="80" valign="top">Dependencia:</td>
                  <td class="contenido" width="361"> 
                    <? echo ((int)$row[4]==1) ? $row[2] : $row[3]; ?>
                  </td>
                </tr>
                <?
				if( !empty($row[5]) || !is_null($row[5]) ){
?>
                <tr> 
                  <td class="sub-item" valign="top">Autor:</td>
                  <td class="contenido"> 
                    <? echo $row[5]; ?>
                  </td>
                </tr>
                <?
				}
				if( !empty($row[6]) || !is_null($row[6]) ){
?>
                <tr> 
                  <td class="sub-item" valign="top">Lugar:</td>
                  <td class="contenido"> 
                    <? echo $row[6]; ?>
                  </td>
                </tr>
                <?
				}
				if( !empty($row[7]) || !is_null($row[7]) ){
?>
                <tr> 
                  <td class="sub-item" valign="top">Fecha:</td>
                  <td class="contenido"> 
                    <? echo $row[7]; ?>
                  </td>
                </tr>
<?
				}
				if($_SESSION['mod_ind_insertar']||$_SESSION['mod_ind_modificar']){
?>
                <tr bgcolor="#F6F6F6"> 
                  <td class="sub-item" valign="top">Estado:</td>
                  <td class="textored"> 
                    <?echo ($row[9]=='t') ? 'Activo' : 'Inactivo';?>
                  </td>
                </tr>
<?
				}
				if($_SESSION['mod_ind_modificar']){
?>
                <tr bgcolor="#F6F6F6"> 
                  <td class="sub-item" valign="top">Acci&oacute;n:</td>
                  <td class="contenido"><a href="<? echo "{$_SERVER['PHP_SELF']}?id={$row[0]}&accion=".ACCION_MODIFICAR_FALSE; ?>">Editar</a></td>
                </tr>
<?
				}
?>
              </table>
            </td>
            <td class="contenido" width="80" valign="top"> 
              <div align="center"><a href="zip/<? echo $row[8]; ?>"><img src="/img/800x600/b_descargar.gif" width="80" height="20" border="0" vspace="2" alt="Descargar en archivo"></a> 
              </div>
            </td>
          </tr>
        </table>
      </td>
	</tr>
	<tr> 
	  <td>
        <hr width="100%" size="1">
      </td>
	</tr>
<?
				
			}
			$rs->Close();
			unset($rs);
		}		
?>
	<tr> 
	  <td>&nbsp;</td>
	</tr>
  </table>
</form>
<?
	}


	function muestraViewerExposicion($ptr, $ptrmax, $path, $ext){
?>
<a name="diap"></a> 
<table width="550" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="3" background="/img/800x600/viewerdiap/f_ar.gif"> 
      <table width="550" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/img/800x600/viewerdiap/esq_ari.gif" width="14" height="14"></td>
          <td align="right"><img src="/img/800x600/viewerdiap/esq_ard.gif" width="37" height="14"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="5" background="/img/800x600/viewerdiap/f_i.gif"></td>
    <td width="508" align="center" valign="middle"><img src="img/<? echo "${path}/diapositiva${ptr}.${ext}"; ?>" vspace="2" border="0"></td>
    <td width="37" background="/img/800x600/viewerdiap/f_d.gif">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><? if ( $ptr > 1 ) { ?><a href="<?echo $_SERVER['PHP_SELF'] . "?accion=" . ACCION_VER_TRUE . "&ptr=1&max=$ptrmax&pth=" . urlencode($path) . "&ext=" . urlencode($ext) . "#diap";?>"><? } ?><img src="/img/800x600/viewerdiap/bot_begin.gif" width="24" height="25" vspace="2" border="0"><? if ( $ptr > 1 ) { ?></a><? } ?></td>
        </tr>
        <tr>
          <td align="center"><? if ( $ptr > 1 ) { ?><a href="<?echo $_SERVER['PHP_SELF'] . "?accion=" . ACCION_VER_TRUE . "&ptr=" . ($ptr-1) . "&max=$ptrmax&pth=" . urlencode($path) . "&ext=" . urlencode($ext) . "#diap";?>"><? } ?><img src="/img/800x600/viewerdiap/bot_prev.gif" width="24" height="25" vspace="2" border="0"><? if ( $ptr > 1 ) { ?></a><? } ?></td>
        </tr>
        <tr>
          <td align="center"><? if ( $ptr < $ptrmax ) { ?><a href="<?echo $_SERVER['PHP_SELF'] . "?accion=" . ACCION_VER_TRUE . "&ptr=" . ($ptr+1) . "&max=$ptrmax&pth=" . urlencode($path) . "&ext=" . urlencode($ext) . "#diap";?>"><? } ?><img src="/img/800x600/viewerdiap/bot_next.gif" width="24" height="25" vspace="2" border="0"><? if ( $ptr > 1 ) { ?></a><? } ?></td>
        </tr>
        <tr>
          <td align="center"><? if ( $ptr < $ptrmax ) { ?><a href="<?echo $_SERVER['PHP_SELF'] . "?accion=" . ACCION_VER_TRUE . "&ptr=" . $ptrmax . "&max=$ptrmax&pth=" . urlencode($path) . "&ext=" . urlencode($ext) . "#diap";?>"><? } ?><img src="/img/800x600/viewerdiap/bot_last.gif" width="24" height="25" vspace="2" border="0"><? if ( $ptr > 1 ) { ?></a><? } ?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3" background="/img/800x600/viewerdiap/f_ab.gif"> 
      <table width="550" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="/img/800x600/viewerdiap/esq_abi.gif" width="14" height="14"></td>
          <td align="right"><img src="/img/800x600/viewerdiap/esq_abd.gif" width="37" height="14"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?
	}

	function muestraViewerExposicionInicio($id_exp, $ptr=1){
		global $serverdb, $type_db, $userdb, $passdb, $db;
	
		$conn = & ADONewConnection($type_db['postgres']);
		$conn->debug = false;
		if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
			print $conn->ErrorMsg();
		else{
			$sql_st="SELECT num_diapositivas, des_diapositivas ".
					"FROM exposicion ".
					"WHERE ind_activo IS true and id_exposicion=$id_exp";
			$rs = & $conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $conn->ErrorMsg();
			}else{
				if ($row = $rs->FetchRow()) {
					$path_ext = explode('.',$row[1]);
					$this->muestraViewerExposicion(1,$row[0],$path_ext[0],$path_ext[1]);
				}
				unset($row);
				$rs->Close(); # optional
			}
			$conn->Close();
		}
	}



	
	/* ---------------------------------------------------- */
	/* Funcion para mostrar el Formulario HTML	  			*/
	/* para Insertar o Modificar una Exposicion   			*/
	/* ---------------------------------------------------- */
	/* MODULO EXPOSICIONES				  		  			*/
	/* ---------------------------------------------------- */
	
	function insertaExposicionHTML($id=NULL,$accion=ACCION_INSERTAR_FALSE){
		$frmName = "frmExpo";
		if(!is_null($id)){
			$this->abreConnDB();
			
			$sql_st="SELECT tit_exposicion, des_exposicion, des_autor, e.id_subdependencia, date_part('day', fec_exposicion), ".
					"date_part('month', fec_exposicion), date_part('year', fec_exposicion), num_diapositivas, ".
					"des_diapositivas, fln_exposicion, e.ind_activo, to_char(e.fec_creacion,'DD/MM/YYYY HH24:MI:SS'), ".
					"to_char(e.fec_modificacion,'DD/MM/YYYY HH24:MI:SS'), e.usr_audit, des_lugar, id_dependencia ".
					"FROM exposicion e, subdependencia sd ".
					"WHERE id_exposicion=$id and e.id_subdependencia=sd.id_subdependencia";
			
			$rs = & $this->conn->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $this->conn->ErrorMsg();
			}else{
				$row = $rs->FetchRow();
				$rs->Close();
			}
		}
		$this->insertaFechaScript();
?>
	<form name="<?=$frmName?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
	  <table width="540" border="0" cellspacing="0" cellpadding="2">
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  
      <td colspan="5" class="td-lb-login" width="540"><b>DE LA EXPOSICI&Oacute;N
        <input type="hidden" name="accion" value="<? echo ($accion == ACCION_INSERTAR_FALSE) ? ACCION_INSERTAR_TRUE : ACCION_MODIFICAR_TRUE;?>">
        </b></td>
		</tr>
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>T&iacute;tulo</b></td>
		  <td width="130"> 
			
        <input type="text" name="tit_exposicion" class="ip-login contenido" size="32" value="<?=$row[0];?>">
		  </td>
		  <td width="5">&nbsp;</td>
		  <td class="texto" width="115">Fecha Realizaci&oacute;n</td>
		  <td width="120"> 
<?
			$row[4]= ($row[4]!=NULL) ? $row[4] : 'NULL';
			$row[5]= ($row[5]!=NULL) ? $row[5] : 'NULL';
			$row[6]= ($row[6]!=NULL) ? $row[6] : 'NULL';
			$this->insertaFechaForm('exp_dia','exp_mes','exp_anyo',$frmName,1990,date('Y'),$row[4],$row[5],$row[6]);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170">Descripci&oacute;n</td>
		  <td width="130"> 
			<textarea name="des_exposicion" class="ip-login contenido" cols="30"><?=$row[1];?></textarea>
		  </td>
		  <td width="5">&nbsp;</td>
		  <td class="texto" width="115">Lugar</td>
		  <td width="120"> 
			<input type="text" name="des_lugar" class="ip-login contenido" size="20" value="<?=$row[14];?>">
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>Publicaci&oacute;n Activa</b></td>
		  <td width="130"> 
<?
		$this->insertaIndActivoFrm($row[10]);
?>
		  </td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td colspan="5" class="td-lb-login" width="540"><b>DEL PROPIETARIO</b></td>
		</tr>
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170">Autor</td>
		  <td width="130"> 
			<input type="text" name="des_autor" class="ip-login contenido" size="32" value="<?=$row[2];?>">
		  </td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>Dependencia</b></td>
		  <td colspan="4"> 
<?
		$this->insertaDependenciaFrm($row[15]);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>Sub Dependencia</b></td>
		  <td colspan="4"> 
			<select name="id_subdependencia" class="ip-login contenido">
			</select>
<?
		$this->insertaSubdependenciaScript("document.${frmName}.id_subdependencia","document.${frmName}.id_dependencia",$row[3]);
?>
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td colspan="5" class="td-lb-login" width="540"><b>DEL CONTENIDO</b></td>
		</tr>
		<tr> 
		  <td colspan="5" width="540"> 
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>N&uacute;mero de Diapositivas</b></td>
		  <td width="130"> 
			<input type="text" name="num_diapositivas" class="ip-login contenido" size="4" value="<?=$row[7];?>">
		  </td>
		  <td width="5">&nbsp;</td>
		  <td class="texto" width="115"><b>Directorio Extensi&oacute;n</b></td>
		  <td width="120"> 
			<input type="text" name="des_diapositivas" class="ip-login contenido" size="20" value="<?=$row[8];?>">
		  </td>
		</tr>
		<tr> 
		  <td width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>Archivo Download</b></td>
		  <td width="130"> 
			<input type="text" name="fln_exposicion" class="ip-login contenido" size="32" value="<?=$row[9];?>">
		  </td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
<?
		if(!is_null($id)){
?>
		<tr> 
		  <td class="texto" colspan="5">
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td class="td-lb-login" colspan="5"><b>DE LA MODIFICACI&Oacute;N</b></td>
		</tr>
		<tr> 
		  <td class="texto" colspan="5">
			<hr width="100%" size="1">
		  </td>
		</tr>
		<tr> 
		  <td class="texto" width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="texto" width="170"><b>Usuario</b></td>
		  <td width="130" class="texto"><?=$row[13];?></td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr>
		  <td class="texto" width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
		<tr>
		  <td class="texto" width="170"><b>Fecha Creaci&oacute;n</b></td>
		  <td width="130" class="texto"><?=$row[11];?></td>
		  <td width="5">&nbsp;</td>
		  <td width="115" class="texto"><b>Fecha Modificaci&oacute;n</b></td>
		  <td width="120" class="texto"><?=$row[12];?></td>
		</tr>
		<tr>
		  <td class="texto" width="170">
			<input type="hidden" name="id" value="<?=$id;?>">
		  </td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
<?
		}
?>
		<tr> 
		  <td class="texto" colspan="5" align="center"> 
			<input type="submit" value="INGRESAR DATOS" class="but-login" onClick="MM_validateForm('tit_exposicion','T�tulo','R','num_diapositivas','N�mero de Diapositivas','RisNum','des_diapositivas','Directorio/Extensi�n','R','fln_exposicion','Archivo Download','R');return document.MM_returnValue">
			&nbsp;&nbsp;&nbsp; 
			<input type="reset" value="BORRAR DATOS" class="but-login">
		  </td>
		</tr>
		<tr> 
		  <td class="texto" width="170">&nbsp;</td>
		  <td width="130">&nbsp;</td>
		  <td width="5">&nbsp;</td>
		  <td width="115">&nbsp;</td>
		  <td width="120">&nbsp;</td>
		</tr>
	  </table>
	</form>
<?
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Insertar una Exposicion a la Base de Datos		 */
	/* ------------------------------------------------------------- */
	/* MODULO EXPOSICIONES											 */
	/* ------------------------------------------------------------- */
	
	function insertaExposicionDB($tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $fec_exposicion, $num_diapositivas, 
								 $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar){
	
		$ins_st = "INSERT INTO exposicion(tit_exposicion, ";
		$ins_st .= (!is_null($des_exposicion)) ? "des_exposicion, " : "";
		$ins_st .= (!is_null($des_autor))? "des_autor, " : "";
		$ins_st .= "id_subdependencia, ";
		$ins_st .= (!is_null($fec_exposicion))? "fec_exposicion, " : "";
		$ins_st .= "num_diapositivas, des_diapositivas, fln_exposicion, ind_activo, usr_audit";
		$ins_st .= (!is_null($des_lugar))? ", des_lugar" : "";
		$ins_st .= ") ";
						
		$ins_st .= "VALUES('${tit_exposicion}', ";
		$ins_st .= (!is_null($des_exposicion)) ? "'${des_exposicion}', " : "";
		$ins_st .= (!is_null($des_autor))? "'${des_autor}', " : "";
		$ins_st .= "$id_subdependencia, ";
		$ins_st .= (!is_null($fec_exposicion))? "to_date('${fec_exposicion}','DD/MM/YYYY'), " : "";
		$ins_st .= "${num_diapositivas}, '${des_diapositivas}', '${fln_exposicion}', '$ind_activo', '{$_SESSION['cod_usuario']}'";
		$ins_st .= (!is_null($des_lugar))? ", '${des_lugar}'" : "";
		$ins_st .= ")";   

		$this->abreConnDB();
		
		if ($this->conn->Execute($ins_st) === false) {
			print 'error insertando: '.$this->conn->ErrorMsg().'<BR>';
		}else{
			$this->muestraTablaStatusDB("La Exposici&oacute;n se ha insertado correctamente.","Modulo Exposiciones","Ingresar otra Exposici&oacute;n");
		}
		unset($ins_st); 
	}
	
	/* ------------------------------------------------------------- */
	/* Funcion para Modificar una Exposicion en la Base de Datos	 */
	/* ------------------------------------------------------------- */
	/* MODULO EXPOSICIONES											 */
	/* ------------------------------------------------------------- */
	
	function modificaExposicionDB($id,$tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $fec_exposicion, $num_diapositivas, 
								 $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar){

		$upd_st = "UPDATE exposicion set tit_exposicion='${tit_exposicion}', ".
		
		$upd_st .= (!is_null($des_exposicion)) ? "des_exposicion='${des_exposicion}', " : "des_exposicion=NULL, ";
		
		$upd_st .= (!is_null($des_autor))? "des_autor='${des_autor}', " : "des_autor=NULL, ";
		
		$upd_st .= "id_subdependencia=$id_subdependencia, ";

		$upd_st .= (!is_null($fec_exposicion))? "fec_exposicion=to_date('${fec_exposicion}','DD/MM/YYYY'), " : "fec_exposicion=NULL, ";

		$upd_st .= "num_diapositivas=${num_diapositivas}, des_diapositivas='${des_diapositivas}', fln_exposicion='${fln_exposicion}', ".
				   "ind_activo='$ind_activo', fec_modificacion=now(), usr_audit='{$_SESSION['cod_usuario']}'";
		
		$upd_st .= (!is_null($des_lugar))? ", des_lugar='${des_lugar}'" : ", des_lugar=NULL";
		
		$upd_st .= " WHERE id_exposicion=${id}";

		$this->abreConnDB();
				
		if ($this->conn->Execute($upd_st) === false) {
			print 'error modificando: '.$this->conn->ErrorMsg().'<BR>';
		}else{
			$this->muestraTablaStatusDB("La Exposici&oacute;n se ha modificado correctamente","Modulo Exposiciones","Ingresar otra Exposici&oacute;n");
		}
		unset($upd_st); 
	}
	
	/* -------------------------------------------------------------- */
	/* Funcion para Limpiar Datos que van desde el Formulario HTML	  */
	/* hacia las Funciones Insertar y Modificar Exposicion 			  */
	/* -------------------------------------------------------------- */
	/* MODULO EXPOSICIONES											  */
	/* -------------------------------------------------------------- */
	
	function enviaDatosExposicion($tit_exposicion, $des_exposicion, $des_autor, $id_subdependencia, $exp_dia, $exp_mes, $exp_anyo, $num_diapositivas, $des_diapositivas, $fln_exposicion, $ind_activo, $des_lugar, $id=NULL){
		
		$des_exposicion = (empty($des_exposicion)) ? NULL : trim($des_exposicion);
		$des_autor = (empty($des_autor)) ? NULL : trim($des_autor);
		$fec_exposicion = ($exp_dia=='NULL'||$exp_mes=='NULL'||$exp_anyo=='NULL') ? NULL : "${exp_dia}/${exp_mes}/${exp_anyo}";
		$des_lugar = (empty($des_lugar)) ? NULL : trim($des_lugar);
		
		if(is_null($id)){
			$this->insertaExposicionDB($tit_exposicion,$des_exposicion,$des_autor,$id_subdependencia,
								$fec_exposicion,$num_diapositivas,$des_diapositivas,$fln_exposicion,$ind_activo,$des_lugar);
		}else{
			$this->modificaExposicionDB($id,$tit_exposicion,$des_exposicion,$des_autor,$id_subdependencia,
								$fec_exposicion,$num_diapositivas,$des_diapositivas,$fln_exposicion,$ind_activo,$des_lugar);
		}
	}

}
?>
