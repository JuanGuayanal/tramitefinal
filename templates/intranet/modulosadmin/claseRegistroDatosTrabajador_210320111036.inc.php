<?
include_once 'mimemail/htmlMimeMail.php';
include_once('claseModulos.inc.php');
include_once('claseIntranet.inc.php');

class RegistroDatosTrabajador extends Modulos{

	var $emailDomain = "CONVENIO_SITRADOC.gob.pe";

	function RegistroDatosTrabajador($menu,$subMenu){
		// Para la Consulta de los Datos del M�dulo
		$this->statConn = false;
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
/*	
		$this->ModuloInfo($_SESSION['modulo_id']);
		
		if(!$_SESSION['mod_ind_leer'])
			return $this = false;
*/
		// Setea Parametros para la Conexion a la Base de Datos
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['mssql'][2];
		$this->typeDB = $this->arr_typeDB['mssql'];
		$this->DB = $this->arr_DB['mssql'][1];
		$this->userDB = $this->arr_userDB['mssql'][0];
		$this->passDB = $this->arr_passDB['mssql'][0];
		
		// Redefine Parametros para la Busqueda Paginable
		$this->numMaxResultsSearch = 10;
		$this->numMaxPagesSearch = 10;
		
		// Setea Array de Acciones de la Aplicacion
		$this->arr_accion = array(
							FRM_AGREGA_TRABAJADOR =>'frmAddTrab',
							AGREGA_TRABAJADOR => 'addTrab',
							FRM_BUSCA_TRABAJADOR =>'frmSearchAte',
							BUSCA_TRABAJADOR =>'searchAte',
							FRM_MODIFICA_TRABAJADOR => 'frmModifyTrab',
							MODIFICA_TRABAJADOR => 'modifyTrab',
							FRM_AGREGA_INFTECNICO =>'frmAddInfTecnico',
							AGREGA_INFTECNICO =>'addInfTecnico',
							MUESTRA_DETALLE => 'showDetail',
							IMPRIMIR_DOCUMENTO => 'printDocumento',
							IMPRIME_INF_TECNICO => 'printInfTecnico',
							FRM_GENERAREPORTE => 'frmGeneraReporte',
							REPORTE_ATE1 => 'ReporteAte1',
							REPORTE_ATE2 => 'ReporteAte2',
							REPORTE_ATE3 => 'ReporteAte3',
							GENERAREPORTE => 'GeneraReporte',
							FRM_BUSCA_DEPENDENCIA =>'frmSearchDepe',
							BUSCA_DEPENDENCIA =>'searchDepe',
							FRM_AGREGA_DEPENDENCIA =>'frmAddDepe',
							AGREGA_DEPENDENCIA =>'addDepe',
							FRM_MODIFICA_DEPENDENCIA => 'frmModifyDepe',
							MODIFICA_DEPENDENCIA => 'modifyDepe',
							BUENA_TRANS => 'goodTrans',
							MALA_TRANS => 'badTrans'
							);

		$this->datosUsuarioMSSQL();
		
		// Items del Menu Principal
		$this->menu_items = array(
							0 => array ( 'val' => 'frmSearchAte', 'label' => "TRABAJADOR" ),
							1 => array ( 'val' => 'frmSearchDepe', 'label' => 'DEPENDENCIA' )
							);
							
		// Menu Seleccionado
		$this->menuPager = ($menu) ? $menu : $this->menu_items[0]['val'];
		
		// Items del Sub Menu para cada Menu
		if($this->menuPager==$this->menu_items[0]['val']){
			$this->subMenu_items = array(
								0 => array ( 'val' => 'frmSearchAte', label => 'BUSCAR' ),
								1 => array ( 'val' => 'frmAddTrab', label => 'AGREGAR' ),
								2 => array ( 'val' => 'frmModifyTrab', label => 'MODIFICAR' )
								);
		}else{
			$this->subMenu_items = array(
								0 => array ( 'val' => 'frmSearchDepe', label => 'BUSCAR' ),
								1 => array ( 'val' => 'frmAddDepe', label => 'AGREGAR' ),
								2 => array ( 'val' => 'frmModifyDepe', label => 'MODIFICAR' )
								);
		}
		// Sub Menu Seleccionado
		$this->subMenuPager = ($subMenu) ? $subMenu : $this->subMenu_items[0]['val'];
		
		$this->pathTemplate = 'oti/registroDatosTrab/';
		$this->pathTemplate2 = 'oti/registroDatosDepe/';		
    }
	
	function MuestraStatTrans($accion){
	global $codReporte;
	global $id;
		if($accion==$this->arr_accion['BUENA_TRANS'])
			$stat = '<strong>LA EJECUCI�N DE LA TAREA HA SIDO LLEVADA A CABO CON �XITO</strong>';
		elseif($accion==$this->arr_accion['MALA_TRANS'])
			$stat = '<strong>HUBO UN ERROR AL EJECUTAR LA TAREA. VUELVA A INTENTARLO EN BREVES INSTANTES. GRACIAS.</strong>';
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		if($this->menuPager==$this->menu_items[0]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_TRABAJADOR]);
		elseif($this->menuPager==$this->menu_items[1]['val'])
			$html->assign_by_ref('accion',$this->arr_accion[FRM_BUSCA_DEPENDENCIA]);			
	
		// Setea el Estado de la Transaccion para poder mostrar el mensaje.
		$html->assign_by_ref('stat',$this->muestraMensajeInfo($stat,true));
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'showStatTrans.inc.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function MuestraIndex(){
		$this->FormBuscaTrabajador();
	}
	
	function ImprimeDocumento($codReporte){
	
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$sql_SP = sprintf("EXECUTE sp_busDatosAte3 %d",
								  $codReporte
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				// Crea el Objeto HTML
				$html = new Smarty;
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('diagnostico',ucfirst($row[7]));
														  $html->assign('indicativo',$row[8]);
														  $html->assign('oficina',$row[9]);
														  $html->assign('liquidacion',$row[4]);
														  $html->assign('tecnico',ucwords($row[11]));
														  $html->assign('tipAte1',ucfirst(strtolower($row[12])));
														  $html->assign('tipAte2',ucfirst(strtolower($row[13])));
														  $html->assign('tipAte3',ucfirst(strtolower($row[14])));
														  $html->assign('ate1',ucfirst(strtolower($row[4])));
														  $html->assign('ate2',ucfirst(strtolower($row[5])));
														  $html->assign('ate3',ucfirst(strtolower($row[6])));
														  $html->assign('modPC',ucfirst(strtolower($row[21])));
														  //'dia' =>$row[16];
														  $html->assign('inicio',substr($row[17],0,16));
														  $html->assign('fin',substr($row[18],0,16));
														  $html->assign('nroPC',$row[19]);
														  $html->assign('numero',$row[22]);
														  //'tiempo' => $this->obtieneDif($row[20])			;
				/**/
				// Setea Campos del Formulario
				setlocale(LC_TIME, $this->zonaHoraria);
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
				$html->assign_by_ref('logo',$logo);
				
				$html->display($this->pathTemplate . 'printDocumento.tpl.php');
			}
			$rs->Close();
		}
	}

	function ImprimeInfTecnico($id){
	
		$this->abreConnDB();
		//$this->conn->debug = true;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT t.NOMBRES_TECNICO+' '+t.APELLIDOS_TECNICO
				FROM dbo.h_tecnico t,db_general.dbo.h_trabajador tr,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.usuario=tr.email and tr.codigo_trabajador=t.codigo_trabajador
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$tecnico=$rs2->fields[0];
		}
		
		$sql3="SELECT d.asunto,d.observaciones 
				FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
				";
		$rs3 = & $this->conn->Execute($sql3);
		unset($sql3);
		if (!$rs3)
			$this->conn->ErrorMsg();
		else{
			$asunto=$rs3->fields[0];
			$observaciones=$rs3->fields[1];
			$html->assign_by_ref('asunto',$asunto);
			$html->assign_by_ref('observaciones',strtoupper($observaciones));
		}
		
		$sql="select max(contador) from db_tramite_documentario.dbo.cuentainternot where coddep=13 and id_tipo_documento=4 /*and email='".$_SESSION['cod_usuario']."'*/";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs)
			$this->conn->ErrorMsg();
		else{
			$numero=$rs->fields[0];
			$html->assign_by_ref('numero',$numero);
		}
		
		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));
				// Setea Campos del Formulario
				setlocale(LC_TIME, "spanish");
				$html->assign('fechaGen',ucfirst(strftime("%d de %B del %Y")));
				$html->assign_by_ref('fecha',date('d/m/Y'));
				$html->assign_by_ref('tecnico',$tecnico);
				$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/logoCONVENIO_SITRADOC.jpg';
				
				$options = "--size 'a4' --fontsize 8.0 --browserwidth 1360 --left 0.5cm --right 0.8cm --jpeg=100 --path " . $_SERVER['DOCUMENT_ROOT'];		
				$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		
				$filename = 'infTec'.mktime();
					
				$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/printDocumento2.tpl.php'),false,$logo);
				//$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/printImpresora.tpl.php'),false,$logo);
				$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
				header("Content-type: application/pdf");
				header("Location: $destination");
				exit;
			}
			$rs->Close();
		}
	}


	function FormBuscaTrabajador($page=NULL,$nombreTrabajador=NULL,$status=NULL,$profesion=NULL,$dependencia=NULL,$condicion=NULL,$cargo=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('status',$status);
		$html->assign_by_ref('nombreTrabajador',$nombreTrabajador);
		
		$html->assign_by_ref('cargo',$cargo);
		
		// Contenido Select de la Profesi�n
		$sql_st = "SELECT id,descripcion+' '+sigla ".
					"FROM db_general.dbo.profesion ".
					"order by 2";
		$html->assign_by_ref('selProfesion',$this->ObjFrmSelect($sql_st, $profesion, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select de la Dependencia
		$sql_st = "SELECT codigo_dependencia,substring(dependencia,1,80) ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Todas las Dependencias')));
		unset($sql_st);
		
		// Contenido Select de la Condicion
		$sql_st = "SELECT descripcion, descripcion ".
				  "FROM dbo.CONDICION_TRABAJADOR ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCondicion',$this->ObjFrmSelect($sql_st, $condicion, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function obtieneDif($min){
		if($min>=1440){//nro de Dias
			$nroDias=floor($min/1440);
			$resto1=$min%1440;
			$nroHoras=floor($resto1/60);
			$nroMin=$resto1%60;
			$tiempo=$nroDias." dia(s) ".$nroHoras." hora(s) ".$nroMin." min"; 
		}elseif($min<1440&& $min>=60){//nro Horas
			$nroHoras=floor($min/60);
			$nroMin=$min%60;
			$tiempo=$nroHoras." hora(s) ".$nroMin." min";
		}elseif($min<60){//nro Minutos
			$tiempo=$min." min";
		}
		return($tiempo);
	}
	
	function buscaInformeTecnico($id){
		$this->abreConnDB();
		$this->conn->debug = false;
		// Crea el Objeto HTML
		$html = new Smarty;
		$sql2="SELECT count(*)
				FROM dbo.h_reporte_2002 r,db_tramite_documentario.dbo.documento d
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
				";
		$rs2 = & $this->conn->Execute($sql2);
		unset($sql2);
		if (!$rs2)
			$this->conn->ErrorMsg();
		else{
			$numero=$rs2->fields[0];
		}
		return($numero);
	}

	function BuscaTrabajador($page,$nombreTrabajador,$status,$profesion,$dependencia,$condicion,$cargo){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaTrabajador($page,$nombreTrabajador,$status,$profesion,$dependencia,$condicion,$cargo,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		//$condConsulta[] = "r.usuario=t.email";
		
		if($status!='T'){
			$condConsulta[] = " tr.estado='$status' ";				
		}
		if($profesion>0){
			$condConsulta[] = "tr.idprofesion=$profesion ";
		}
		if($dependencia>0){
			$condConsulta[] = "tr.coddep=$dependencia ";
		}
		
		if($condicion!='none'){
			$condConsulta[] = " tr.condicion='$condicion' ";				
		}
		if($cargo!="none"){
			if($cargo=="NULL"){
				$condConsulta[] = "(tr.director is NULL or tr.director=0)";
			}else{
				$condConsulta[] = "tr.director='$cargo'";
			}	
		}
		
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
        // Evalua que no contenga sentencias SQL
	    $this->evaluaNoSql($nombreTrabajador);

		$sql_SP = sprintf("EXECUTE sp_busIDTrab %s,%s,%s,0,0",
							($nombreTrabajador) ? "'".$this->PrepareParamSQL($nombreTrabajador)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDTrab %s,%s,%s,%s,%s",
								($nombreTrabajador) ? "'".$this->PrepareParamSQL($nombreTrabajador)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosTrab %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($AtencionData = $rs->FetchRow())
							$html->append('arrTrab', array('id' => $id[0],
														  'nomTrab' => ucfirst($AtencionData[1]),
														  'depe' => $AtencionData[2],
														  'cond' => $AtencionData[3],
														  'email' => ucwords($AtencionData[4]),
														  'profesion' => strtoupper($AtencionData[5]),
														  'fecNac' => ucfirst($AtencionData[7]),
														  'dni' =>$AtencionData[8],
														  'telef' =>$AtencionData[9],
														  'estado' =>$AtencionData[10],
														  'publi' => ucwords($AtencionData[11]),
														  'idsubdepe' => strtoupper($AtencionData[12]),
														  'coddeppub' => strtoupper($AtencionData[13]),
														  'cargo' => strtoupper($AtencionData[14]),
														  'publiintra' =>$AtencionData[15],
														  'anexo' =>$AtencionData[16],
														  'publicontact' =>$AtencionData[17],
														  'est' =>$AtencionData[18],
														  'userCrea' =>$AtencionData[19],
														  'auditCrea' =>$AtencionData[20],
														  'userMod' =>$AtencionData[21],
														  'auditMod' =>$AtencionData[22]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_TRABAJADOR], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function FormAgregaTrabajador($dependencia=NULL,$apellidoTrabajador=NULL,$nombreTrabajador=NULL,$condicion=NULL,$email=NULL,$profesion=NULL,$dni=NULL,$telefono=NULL,$desFechaIni=NULL,$publi=NULL,$subDependencia=NULL,$dependenciaPortal=NULL,$cargo=NULL,$publi2=NULL,$anexo=NULL,$publi3=NULL,$errors=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddTrab';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('apellidoTrabajador',$apellidoTrabajador);
		$html->assign_by_ref('nombreTrabajador',$nombreTrabajador);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('dni',$dni);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('publi',$publi);
		$html->assign_by_ref('cargo',$cargo);
		$html->assign_by_ref('publi2',$publi2);
		$html->assign_by_ref('anexo',$anexo);
		$html->assign_by_ref('publi3',$publi3);

		// Contenido Select de la Profesi�n
		$sql_st = "SELECT id,SIGLA+' '+descripcion ".
					"FROM db_general.dbo.profesion ".
					"order by 2";
		$html->assign_by_ref('selProfesion',$this->ObjFrmSelect($sql_st, $profesion, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		// Contenido Select de la Dependencia
		$sql_st = "SELECT codigo_dependencia,substring(dependencia,1,80) ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		
		// Contenido Select de LA SUBDEPENDENCIA
		if($dependencia>0 && $dependencia!=38 && $dependencia!=8){
		$sql_st = "SELECT id_subdependencia, substring(sigla,1,15) ".
						  "FROM db_general.dbo.h_subdependencia ".
						  "where id_dependencia=$dependencia ".
						  "and id_subdependencia>0 ".
						  "AND sigla is not null ".
						  "ORDER BY 2";
		}
		$html->assign_by_ref('selSubDependencia',$this->ObjFrmSelect($sql_st, $subDependencia, true, true, array('val'=>'none','label'=>'Seleccione')));
		unset($sql_st);
		
		// Contenido Select de la Dependencia publicada en el portal
		if($dependencia>0){
			$sql_st = "SELECT codigo_dependencia,substring(dependencia,1,80) ".
						"FROM db_general.dbo.h_dependencia ".
						"WHERE condicion='ACTIVO' ".
						"AND codigo_dependencia not in (34,49) ";
			if($dependencia==8||$dependencia==10||$dependencia==12||$dependencia==9||$dependencia==45)		
				$sql_st .="and codigo_dependencia=8 ";
			elseif($dependencia==42||$dependencia==39||$dependencia==41||$dependencia==46)	
				$sql_st .="and codigo_dependencia=38 ";
			elseif($dependencia==50||$dependencia==66)
				$sql_st .="and codigo_dependencia=1 ";
			elseif($dependencia==52||$dependencia==51)
				$sql_st .="and codigo_dependencia=36 ";
			else					
				$sql_st .="and codigo_dependencia=$dependencia ";
			$sql_st .=" order by 2";
			$html->assign_by_ref('selDependenciaPortal',$this->ObjFrmSelect($sql_st, $dependenciaPortal, true, true));
		}else{			
			$html->assign_by_ref('selDependenciaPortal',$this->ObjFrmSelect($sql_st, $dependenciaPortal, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		}
		unset($sql_st);		
		
		// Contenido Select de la Condicion
		$sql_st = "SELECT descripcion, descripcion ".
				  "FROM dbo.CONDICION_TRABAJADOR ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCondicion',$this->ObjFrmSelect($sql_st, $condicion, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddTrab.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}
	
	function ObtieneCadena(){
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		$nombre=$_SERVER['REMOTE_HOST'];
		$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);

		$check = substr($ip, 0, 7); 
		if ($check == $blocked1 || $check == $blocked2) { return; }
		//$ip=$ip."/".$nombre."/ ".$nombre2."/ ".$nombre3."/ ".$a."/ ".$b."/ ".$c."/D ".$nombre_host;
		
		$tamano=strlen($nombre_host);
		$cadenaI=$tamano-19;
		
		$cadena=substr($nombre_host,$cadenaI,2);		
		
		return($cadena);
	}
	
	function ObtieneNombrePC(){
		$blocked1 = "192.168"; // For viewing pages on remote server on local network
		$blocked2 = "127.0.0"; // For viewing pages while logged on to server directly
		
		$register_globals = (bool) ini_get('register_gobals');
		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = $_SERVER['REMOTE_ADDR'];
		$nombre=$_SERVER['REMOTE_HOST'];
		$nombre2=$_SERVER['SERVER_NAME'];
		//$nombre2=getenv(REMOTE_HOST);
		//$g=$_SERVER['REMOTE_HOST'];
		$g=getenv('REMOTE_HOST');
		//echo $g."matrix";
		$nombre_host = gethostbyaddr($ip);

		$check = substr($ip, 0, 7); 
		if ($check == $blocked1 || $check == $blocked2) { return; }
		//$ip=$ip."/".$nombre."/ ".$nombre2."/ ".$nombre3."/ ".$a."/ ".$b."/ ".$c."/D ".$nombre_host;
		
		$tamano=strlen($nombre_host);
		//echo "HOLAS".$nombre_host;exit;
		$cadenaI=$tamano-15;
		
		$cadena=substr($nombre_host,0,$cadenaI);		
		
		return($cadena);
	}		

	function AgregaTrabajador($dependencia,$apellidoTrabajador,$nombreTrabajador,$condicion,$email,$profesion,$dni,$telefono,$desFechaIni,$publi,$subDependencia,$dependenciaPortal,$cargo,$publi2,$anexo,$publi3){
		
		// Comprueba Valores	
		if(!$apellidoTrabajador){ $bTr1 = true; $this->errors .= 'El apellido debe ser especificado<br>'; }
		if(!$nombreTrabajador){ $bTr2 = true; $this->errors .= 'El nombre debe ser especificado<br>'; }
		if(!$dni||strlen($dni)!=8||$dni=="00000000"){ $bTr3 = true; $this->errors .= 'Ingrese un DNI v�lido<br>'; }
		if(!$telefono||strlen($telefono)<7){ $bTr4 = true; $this->errors .= 'Ingrese un n�mero de tel�fono v�lido<br>'; }
		if(!$anexo||strlen($anexo)<3){ $bTr5 = true; $this->errors .= 'Ingrese un n�mero de anexo v�lido<br>'; }
	
		if($bTr1||$bTr2||$bTr3||$bTr4||$bTr5){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaTrabajador($dependencia,$apellidoTrabajador,$nombreTrabajador,$condicion,$email,$profesion,$dni,$telefono,$desFechaIni,$publi,$subDependencia,$dependenciaPortal,$cargo,$publi2,$anexo,$publi3,$errors);
			
			$objIntranet->Footer();
		}else{
		//echo "holas";exit;	
			$this->abreConnDB();
			$this->conn->debug = true;
			
	        // Evalua que no contenga sentencias SQL
        	$this->evaluaNoSql($apellidoTrabajador);
            $this->evaluaNoSql($nombreTrabajador);			

			$sql_SP = sprintf("EXECUTE sp_ins_newTrabajador '%s','%s',%d,%d,'%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s',%d,'%s'",
							  $this->PrepareParamSQL($nombreTrabajador),
							  $this->PrepareParamSQL($apellidoTrabajador),
							  $this->PrepareParamSQL($dependencia),
							  ($subDependencia>0) ? $subDependencia : "0",
							  $this->PrepareParamSQL($condicion),
							  $this->PrepareParamSQL($desFechaIni),
							  $this->PrepareParamSQL($dni),
							  $this->PrepareParamSQL($telefono),
							  $this->PrepareParamSQL($email),
							  $this->PrepareParamSQL($profesion),
							  ($publi) ? $publi : "0",
							  ($publi==1) ? $dependenciaPortal : $dependencia,
							  ($cargo>0) ? $cargo : "NULL",
							  ($publi2) ? $publi2 : "0",
							  ($anexo && $anexo!="") ? $this->PrepareParamSQL($anexo) : "NULL",
							  ($publi3) ? $publi3 : "0",
							  $_SESSION['cod_usuario']
							  );
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaTrabajador($id,$dependencia=NULL,$apellidoTrabajador=NULL,$nombreTrabajador=NULL,$condicion=NULL,$email=NULL,$profesion=NULL,$dni=NULL,$telefono=NULL,$desFechaIni=NULL,$status=NULL,$publi=NULL,$subDependencia=NULL,$dependenciaPortal=NULL,$cargo=NULL,$publi2=NULL,$anexo=NULL,$publi3=NULL,$reLoad=false,$errors=false){
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}

		if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("EXECUTE sp_listTrabajador %d",$this->PrepareParamSQL($id));
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$dependencia = $row[0];
					$apellidoTrabajador = $row[1];
					$nombreTrabajador = $row[2];
					$condicion = $row[3];
					$email = $row[4];
					$profesion = $row[5];
					$desFechaIni = $row[6];
					$dni = $row[7];
					$telefono = $row[8];
					$status=$row[9];
					$subDependencia=$row[10];
					$publi=$row[11];
					$cargo=$row[12];
					$dependenciaPortal=$row[13];
					$publi2=$row[14];
					$anexo=$row[15];
					$publi3=$row[16];
				}
				$rs->Close();
			}
			unset($rs);
		} 	
		
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyTrab';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('apellidoTrabajador',$apellidoTrabajador);
		$html->assign_by_ref('nombreTrabajador',$nombreTrabajador);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('dni',$dni);
		$html->assign_by_ref('telefono',$telefono);
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('status',$status);
		$html->assign_by_ref('publi',$publi);
		$html->assign_by_ref('cargo',$cargo);
		$html->assign_by_ref('publi2',$publi2);
		$html->assign_by_ref('anexo',$anexo);
		$html->assign_by_ref('publi3',$publi3);		


		// Contenido Select de la Profesi�n
		$sql_st = "SELECT id,SIGLA+' '+descripcion ".
					"FROM db_general.dbo.profesion ".
					"order by 2";
		$html->assign_by_ref('selProfesion',$this->ObjFrmSelect($sql_st, $profesion, true, true, array('val'=>'none','label'=>'Todos')));
		unset($sql_st);
		
		
		// Contenido Select de la Dependencia
		$sql_st = "SELECT codigo_dependencia,substring(dependencia,1,80) ".
					"FROM db_general.dbo.h_dependencia ".
					"WHERE condicion='ACTIVO' ".
					"AND codigo_dependencia not in (34,49) ".
					"order by 2";
		//echo "<br>sql_st: ". $sql_st ."<br>";
		
		
		//$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		$html->assign_by_ref('selDependencia',$this->ObjFrmSelect($sql_st, $dependencia, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		
		// Contenido Select de LA SUBDEPENDENCIA
		if($dependencia>0 && $dependencia!=38 && $dependencia!=8){
		$sql_st = "SELECT id_subdependencia, substring(sigla,1,15) ".
						  "FROM db_general.dbo.h_subdependencia ".
						  "where id_dependencia=$dependencia ".
						  "and id_subdependencia>0 ".
						  "AND sigla is not null ".
						  "ORDER BY 2";
		}
		//echo "<br>sql_st: ". $sql_st ."<br>";
		$html->assign_by_ref('selSubDependencia',$this->ObjFrmSelect($sql_st, $subDependencia, true, true, array('val'=>'none','label'=>'Seleccione')));
		unset($sql_st);
		
		
		// Contenido Select de la Dependencia publicada en el portal
		if($dependencia>0){
			$sql_st = "SELECT codigo_dependencia,substring(dependencia,1,80) ".
						"FROM db_general.dbo.h_dependencia ".
						"WHERE condicion='ACTIVO' ".
						"AND codigo_dependencia not in (34,49) ";
			if($dependencia==8||$dependencia==10||$dependencia==12||$dependencia==9||$dependencia==45)		
				$sql_st .="and codigo_dependencia=8 ";
			elseif($dependencia==42||$dependencia==39||$dependencia==41||$dependencia==46)	
				$sql_st .="and codigo_dependencia=38 ";
			elseif($dependencia==50||$dependencia==66)
				$sql_st .="and codigo_dependencia=1 ";
			elseif($dependencia==52||$dependencia==51)
				$sql_st .="and codigo_dependencia=36 ";
			else
				$sql_st .="and codigo_dependencia=$dependencia ";
			$sql_st .=" order by 2";
			$html->assign_by_ref('selDependenciaPortal',$this->ObjFrmSelect($sql_st, $dependenciaPortal, true, true));
		}else{			
			$html->assign_by_ref('selDependenciaPortal',$this->ObjFrmSelect($sql_st, $dependenciaPortal, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		}
		unset($sql_st);		
		
		// Contenido Select de la Condicion
		$sql_st = "SELECT descripcion, descripcion ".
				  "FROM dbo.CONDICION_TRABAJADOR ".
				  "ORDER BY 2";
		$html->assign_by_ref('selCondicion',$this->ObjFrmSelect($sql_st, $condicion, true, true, array('val'=>'none','label'=>'Todas')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmModifyTrab.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	
	
	
		function ModificaTrabajador($id,$dependencia,$apellidoTrabajador,$nombreTrabajador,$condicion,$email,$profesion,$dni,$telefono,$desFechaIni,$status,$publi,$subDependencia,$dependenciaPortal,$cargo,$publi2,$anexo,$publi3){
		// Comprueba Valores	
		if(!$apellidoTrabajador){ $bTr1 = true; $this->errors .= 'El apellido debe ser especificado<br>'; }
		if(!$nombreTrabajador){ $bTr2 = true; $this->errors .= 'El nombre debe ser especificado<br>'; }

		if($bTr1||$bTr2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaTrabajador($id,$dependencia,$apellidoTrabajador,$nombreTrabajador,$condicion,$email,$profesion,$dni,$telefono,$desFechaIni,$status,$publi,$subDependencia,$dependenciaPortal,$cargo,$publi2,$anexo,$publi3,$errors);
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;
			
			$sql_SP = sprintf("EXECUTE sp_modTrabajador %d,'%s','%s',%d,%d,'%s','%s','%s','%s','%s',%d,'%s',%d,%d,%d,%d,'%s',%d,'%s'",
							  $id,
							  $this->PrepareParamSQL($nombreTrabajador),
							  $this->PrepareParamSQL($apellidoTrabajador),
							  $this->PrepareParamSQL($dependencia),
							  ($subDependencia>0) ? $subDependencia : "0",
							  $this->PrepareParamSQL($condicion),
							  $this->PrepareParamSQL($desFechaIni),
							  $this->PrepareParamSQL($dni),
							  $this->PrepareParamSQL($telefono),
							  $this->PrepareParamSQL($email),
							  $this->PrepareParamSQL($profesion),
							  $status,
							  ($publi) ? $publi : "0",
							  ($publi==1) ? $dependenciaPortal : $dependencia,
							  ($cargo>0) ? $cargo : "NULL",
							  ($publi2) ? $publi2 : "0",
							  ($anexo && $anexo!="") ? $this->PrepareParamSQL($anexo) : "NULL",
							  ($publi3) ? $publi3 : "0",
							  $_SESSION['cod_usuario']
							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[0]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormAgregaInfTecnico($id,$asunto=NULL,$diagnostico=NULL,$sol=NULL,$errors=false){
		global $opcion,$documentos;
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		// Genera Objeto HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
/**/
		$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print 'error : '.$this->conn->ErrorMsg().'<BR>';
		else{
			if ($row = $rs->FetchRow()) {
				
														  $html->assign('usuario',ucfirst($row[0]));
														  $html->assign('coddep',ucfirst($row[1]));
														  $html->assign('descrip',ucfirst($row[2]));
														  $html->assign('flag',ucfirst($row[3]));
														  $html->assign('siglas',ucfirst($row[4]));
														  $html->assign('marca',ucfirst($row[5]));
														  $html->assign('modelo',ucfirst($row[6]));
														  $html->assign('codPat',ucfirst($row[7]));
														  $html->assign('serie',ucfirst($row[8]));
														  $html->assign('dep',ucfirst($row[9]));
														  $html->assign('codPC',ucfirst($row[10]));
										}
			}

/**/

			if($Buscar==1&&$numero2!=""&& $tipoDocc>0&& $anyo2>0&& $siglasDepe2!="none"){
				$this->abreConnDB();
				
				//Primeramente averiguamos si el doc es el padre o uno de los hijos
				//$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/".$siglasDepe2;
				$var=$numero2."-".$anyo2."-CONVENIO_SITRADOC/";
				$sql="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108) 
				
					from documento d,dbo.clase_documento_interno cdi 
					where d.id_clase_documento_interno=$tipoDocc and d.id_clase_documento_interno=cdi.id_clase_documento_interno
					      and d.coddep=$siglasDepe2 and d.indicativo_oficio like '%{$var}%'";
				
				$rs = & $this->conn->Execute($sql);
				unset($sql);
				if (!$rs){
					print $this->conn->ErrorMsg();
					return;
				}else{
					$idDoc1=$rs->fields[0];
					if($idDoc1>0){
						$sql2="select count(*) from movimiento_documento where id_documento=$idDoc1";
						$rs2 = & $this->conn->Execute($sql2);
						unset($sql2);
						if (!$rs2){
							print $this->conn->ErrorMsg();
							return;
						}else{
							$cont=$rs2->fields[0];
							
							if($cont>0){//es el documento padre
								$idDocumento=$idDoc1;
									$claseDoc=$rs->fields[1];
									$ind=$rs->fields[2];
									$asunto2=$rs->fields[3];
									$obs=$rs->fields[4];
									$fecRec=$rs->fields[5];
									$exito=1;
							}else{//es el documento hijo
								$sql3="select d.id_documento,cdi.descripcion,d.indicativo_oficio,d.asunto,d.observaciones,
								              convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108)
										from documento d,dbo.clase_documento_interno cdi,movimiento_documento md
										where d.id_documento=md.id_documento and md.id_oficio=$idDoc1 and
										      /*md.derivado=1 and md.finalizado=0 and md.id_dependencia_destino=".$this->userIntranet['COD_DEP']."*/
											  cdi.id_clase_documento_interno=d.id_clase_documento_interno and md.finalizado=0
								      ";
									  
								$rs3 = & $this->conn->Execute($sql3);
								unset($sql3);
								if (!$rs3){
									print $this->conn->ErrorMsg();
									return;
								}else{
									$idDocumento=$rs3->fields[0];
									$claseDoc=$rs3->fields[1];
									$ind=$rs3->fields[2];
									$asunto2=$rs3->fields[3];
									$obs=$rs3->fields[4];
									$fecRec=$rs3->fields[5];
									if($idDocumento>0){
										$exito=1;
									}else{
										$exito=0;
									}
								}
									  
									  
									  
									  
							}//fin del if($cont>0)
							
						}//fin del if(!$rs2)
					}//fin del if($idDoc1)
				}//fin del if(!$rs)
				
			}//fin del if($Buscar)

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddInfTecnico';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('asunto',$asunto);
		$html->assign_by_ref('diagnostico',$diagnostico);
		$html->assign_by_ref('sol',$sol);
		$html->assign_by_ref('opcion',$opcion);
		
		$sql_st="select d.id_documento,case when d.id_tipo_documento=4 then db_tramite_documentario.dbo.buscaReferencia(md.id_movimiento_documento)
					       else d.num_tram_documentario end,case when d.id_tipo_documento=1 then d.asunto
			                                                  when d.id_tipo_documento=2 then tup.descripcion
															  when d.id_tipo_documento=4 then d.asunto end,
					  case when sol.id_tipo_persona=1 then Upper(sol.apellidos)+' '+Upper(sol.nombres)
	             when sol.id_tipo_persona=2 then sol.razon_social 
				 else 'DOCUMENTO INTERNO' end,convert(varchar,d.auditmod,103)+' '+convert(varchar,d.auditmod,108),
				 convert(varchar,md.audit_mod,103)+' '+convert(varchar,md.audit_mod,108),t.apellidos_trabajador+' '+t.nombres_trabajador
				from db_tramite_documentario.dbo.documento d left join db_general.dbo.tupa tup on d.id_tup=tup.id_tupa
				                 left join db_general.dbo.persona sol on d.id_persona=sol.id,
     					db_tramite_documentario.dbo.movimiento_documento md left join db_general.dbo.h_trabajador t on md.codigo_trabajador=t.codigo_trabajador and t.estado='ACTIVO'
				WHERE d.id_documento=md.id_documento and md.derivado=0 and md.finalizado=0
					and md.id_dependencia_destino=13 and md.codigo_trabajador=87";
		$html->assign_by_ref('selDocumentos',$this->ObjFrmSelect($sql_st, $documentos, true, true, array('val'=>'none','label'=>'Escoja')));
		unset($sql_st);					
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'frmAddInfTecnico.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');	
	}
	
	function AgregaInfTecnico($id,$asunto,$diagnostico,$sol){
		global $opcion,$documentos;
		// Comprueba Valores	
		if(!$asunto){ $b1 = true; $this->errors .= 'Lo que presenta debe ser especificado<br>'; }
		if(!$diagnostico){ $b2 = true; $this->errors .= 'El Diagn�stico debe ser especificado<br>'; }
		if(!$sol){ $b3 = true; $this->errors .= 'La Soluci�n debe ser especificada<br>'; }

		if($b1||$b2||$b3){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaInfTecnico($id,$asunto,$diagnostico,$sol,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			$this->conn->debug = false;
			
			$sql_SP = sprintf("EXECUTE sp_insInfTecnico %d,'%s','%s','%s','%s','%s',%d,%d",
							  $id,
							  "-".date('Y')."-CONVENIO_SITRADOC/".$this->userIntranet['SIGLA_DEP']."-".$_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($asunto),
							  $this->PrepareParamSQL($diagnostico),
							  $this->PrepareParamSQL($sol),
							  $_SESSION['cod_usuario'],
							  ($opcion>0) ? $opcion : "0",
							  ($opcion>0) ? $documentos : "NULL"
 							  );
			 //echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);		

			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[3]['val']}";
			$destination .= "&id={$id}";
			header("Location: $destination");
			exit;
		}
	}
	
	function CreaArchivoPDF($filename,$path,&$cont,$landscape=false,$logo=false,$options="--size 'a4' --fontsize 6.0 --header ... --footer .t. "){
		// Pone el Contenido del Reporte en un archivo temporal
		$tmp_filename = tempnam ("/tmp/", "intraPDF_");
		if ($fp = fopen($tmp_filename, "w")){
			fwrite($fp, $cont);
			unset($cont);
			fclose($fp);
					
			// Convierte el Reporte HTML en PDF
			$filename = escapeshellcmd("{$path}/{$filename}.pdf");
			$output = exec(sprintf("/usr/bin/htmldoc -t pdf --quiet --jpeg --webpage %s %s %s -f '%s' %s",
						 			($landscape) ? '--landscape' : '',
									($logo) ? "--logoimage '{$logo}'" : '',
									($options) ? escapeshellcmd($options) : '',
									$filename,
									$tmp_filename));
			// Borra el archivo Temporal			
			unlink($tmp_filename);
			return true;
		}
	}
	
	function FormGeneraReporte($search2=false){
		global $desFechaIni,$fecDesembarqueIni,$desFechaFin,$fecDesembarqueFin;
		global $GrupoOpciones1;
		//$this->abreConnDB();
		//$this->conn->debug = true;

		//Manipulacion de las Fechas
		$fecAtencion = ($fecAtencion!='//'&&$fecAtencion) ? $fecAtencion : date('m/Y');
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmGeneraReporte';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('tipReporte',$tipReporte);
		$html->assign_by_ref('GrupoOpciones1',$GrupoOpciones1);
		
		$html->assign_by_ref('desFechaIni',$desFechaIni);
		$html->assign_by_ref('fecDesembarqueIni',$fecDesembarqueIni);
		$html->assign_by_ref('desFechaFin',$desFechaFin);
		$html->assign_by_ref('fecDesembarqueFin',$fecDesembarqueFin);
		
		
		//Mes y anyo
		$html->assign_by_ref('selMes',$this->ObjFrmMes(1, 12, true, substr($fecAtencion,0,2), true));
		$html->assign_by_ref('selAnyo',$this->ObjFrmAnyo(date('Y')-1, date('Y'), substr($fecAtencion,3,4), true));

		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Muestra el Formulario
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate . 'reportes.tpl.php');
		if(!$search2) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}

	function GeneraReporte($tipReporte,$mes,$anyo){
	global $GrupoOpciones1,$desFechaIni,$desFechaFin;
	
		switch($GrupoOpciones1){
			case 1:
			$this->ReporteAte1($mes,$anyo,$desFechaIni,$desFechaFin);
			break;
			case 2:
			$this->ReporteAte2($mes,$anyo,$desFechaIni,$desFechaFin);
			break;
			case 3:
			$this->ReporteAte3($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Software
			break;
			case 4:
			$this->ReporteAte4($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Hardware
			break;
			case 7:
			$this->ReporteAte5($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Otros
			break;
			case 5:
			$this->ReporteAte6($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Redes
			break;
			case 6:
			$this->ReporteAte7($mes,$anyo,$desFechaIni,$desFechaFin);//Reporte Mantenimiento Correctivo
			break;
			case 8:
			$this->ReporteAte8($mes,$anyo);//Reporte Software
			break;
			case 9:
			$this->ReporteEspecial($mes,$anyo);//Reporte Especial
			break;
			case 10:
			$this->ReporteAte2Esp($mes,$anyo);
			break;			
		}
	}

	function ReporteAte1($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		//$this->SetDirTemplate($html);
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
	
		$sql_st = sprintf("EXECUTE sp_reporteAte1 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	

	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate1',array('hw'=>$row[0],
										  'sw'=>$row[1],
										  'redes'=>$row[2],
										  'otros'=>$row[3],
										  'total'=>$row[4],
										  'mc'=>$row[5]));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;
		//$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$html->assign('fechaGen',ucfirst(strftime("%d/%m/%Y   %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate1'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte1.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte2($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$hoy=date('d');
		$ayer=$hoy-1;
		
		$mesinicial=$mes-1;
		if($mesinicial<10){$mesinicial='0'.$mesinicial;}
		$fecha1='15/'.$mesinicial.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}else{
			$fecha2='15/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}
		$anyoinicial=$anyo-1;
		if($mes==1){$fecha1='17/12/'.$anyoinicial;$fecha2='19/01/'.$anyo;$fecha3='20/'.$mes.'/'.$anyo;}
		
		$html->assign_by_ref('fecha1',$desFechaIni);
		$html->assign_by_ref('fecha2',$desFechaFin);
	
		$sql_st = sprintf("EXECUTE sp_reporteAte2 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate2',array('hw'=>$row[0],
										  'hw1'=>$row[1],
										  'hw2'=>$row[2],
										  'hw3'=>$row[3],
										  'sw'=>$row[4],
										  'sw1'=>$row[5],
										  'sw2'=>$row[6],
										  'sw3'=>$row[7],
										  're'=>$row[8],
										  're1'=>$row[9],
										  're2'=>$row[10],
										  're3'=>$row[11],
										  'ot'=>$row[12],
										  'ot1'=>$row[13],
										  'ot2'=>$row[14],
										  'ot3'=>$row[15],
										  'tot'=>$row[16],
										  'tot1'=>$row[17],
										  'tot2'=>$row[18],
										  'tot3'=>$row[19],
										  'mc'=>$row[20],
										  'mc1'=>$row[21],
										  'mc2'=>$row[22],
										  'mc3'=>$row[23],
										  'hw4'=>$row[24],
										  'sw4'=>$row[25],
										  're4'=>$row[26],
										  'mc4'=>$row[27],
										  'ot4'=>$row[28],
										  'tot4'=>$row[29],
										  'hw5'=>$row[30],
										  'sw5'=>$row[31],
										  're5'=>$row[32],
										  'mc5'=>$row[33],
										  'ot5'=>$row[34],
										  'tot5'=>$row[35],
										  'hw6'=>$row[36],
										  'sw6'=>$row[37],
										  're6'=>$row[38],
										  'mc6'=>$row[39],
										  'ot6'=>$row[40],
										  'tot6'=>$row[41],
										  'hw7'=>$row[42],
										  'sw7'=>$row[43],
										  're7'=>$row[44],
										  'mc7'=>$row[45],
										  'ot7'=>$row[46],
										  'tot7'=>$row[47],
										  'hw8'=>$row[48],
										  'sw8'=>$row[49],
										  're8'=>$row[50],
										  'mc8'=>$row[51],
										  'ot8'=>$row[52],
										  'tot8'=>$row[53]										  
										  ));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate2'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte2.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte2Esp($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$hoy=date('d');
		$ayer=$hoy-1;
		
		$mesinicial=$mes-1;
		if($mesinicial<10){$mesinicial='0'.$mesinicial;}
		$fecha1='17/'.$mesinicial.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}else{
			$fecha2='19/'.$mes.'/'.$anyo;
			$fecha3='20/'.$mes.'/'.$anyo;
			}
		$anyoinicial=$anyo-1;
		if($mes==1){$fecha1='17/12/'.$anyoinicial;$fecha2='19/01/'.$anyo;$fecha3='20/'.$mes.'/'.$anyo;}
		
		$html->assign_by_ref('fecha1',$fecha1);
		$html->assign_by_ref('fecha2',$fecha2);
	
		$sql_st = sprintf("EXECUTE sp_reporteAte2 '%s','%s'",
							$fecha1,
							$fecha3);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate2',array('hw'=>$row[0],
										  'hw1'=>$row[1],
										  'hw2'=>$row[2],
										  'hw3'=>$row[3],
										  'sw'=>$row[4],
										  'sw1'=>$row[5],
										  'sw2'=>$row[6],
										  'sw3'=>$row[7],
										  're'=>$row[8],
										  're1'=>$row[9],
										  're2'=>$row[10],
										  're3'=>$row[11],
										  'ot'=>$row[12],
										  'ot1'=>$row[13],
										  'ot2'=>$row[14],
										  'ot3'=>$row[15],
										  'tot'=>$row[16],
										  'tot1'=>$row[17],
										  'tot2'=>$row[18],
										  'tot3'=>$row[19],
										  'mc'=>$row[20],
										  'mc1'=>$row[21],
										  'mc2'=>$row[22],
										  'mc3'=>$row[23],
										  'hw4'=>$row[24],
										  'sw4'=>$row[25],
										  're4'=>$row[26],
										  'mc4'=>$row[27],
										  'ot4'=>$row[28],
										  'tot4'=>$row[29],
										  'hw5'=>$row[30],
										  'sw5'=>$row[31],
										  're5'=>$row[32],
										  'mc5'=>$row[33],
										  'ot5'=>$row[34],
										  'tot5'=>$row[35],
										  'hw6'=>$row[36],
										  'sw6'=>$row[37],
										  're6'=>$row[38],
										  'mc6'=>$row[39],
										  'ot6'=>$row[40],
										  'tot6'=>$row[41],
										  'hw7'=>$row[42],
										  'sw7'=>$row[43],
										  're7'=>$row[44],
										  'mc7'=>$row[45],
										  'ot7'=>$row[46],
										  'tot7'=>$row[47],
										  'hw8'=>$row[48],
										  'sw8'=>$row[49],
										  're8'=>$row[50],
										  'mc8'=>$row[51],
										  'ot8'=>$row[52],
										  'tot8'=>$row[53]										  
										  
										  ));
			$rs->Close();
		}
		unset($rs);

		setlocale(LC_TIME,"spanish");;

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate2'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte2Esp.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function num($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	
	function ReporteAte3($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=2 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte3 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function numm($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3  and r.usuario='obarja'
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function ReporteAte8($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) and r.usuario='obarja'
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		$sql_st = sprintf("EXECUTE sp_reporteAte8 '%s','%s'",
							$fecha1,
							$fecha2);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate3',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->numm($row[1],$fecha1,$fecha2)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"es_PE");;
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate3'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte3.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function num2($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num3($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}
	function num4($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	
	function num5($dat,$fecha1,$fecha2){
		$this->abreConnDB();
		//$this->conn->debug = true;
	
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$fecha1',103) AND r.AUDIT_FIN<=convert(datetime,'$fecha2',103) 
			and rs.codigo_soporte=$dat"; 
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
			}
			//unset($rs);
		return($numero);
	}	

	function ReporteAte4($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=1 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte4 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate4',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num2($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate4'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte4.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte5($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}
		
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=4 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte5 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate5',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num3($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate5'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte5.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}

	function ReporteAte6($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=3 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];
		}
		$html->assign_by_ref('total',$total);
		
		$sql_st = sprintf("EXECUTE sp_reporteAte6 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate6',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num4($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");

		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));
		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate6'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte6.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	function ReporteAte7($mes,$anyo,$desFechaIni,$desFechaFin){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			}

		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte and s.codigo_tipo_soporte=5 
			and r.AUDIT_FIN> convert(datetime,'$desFechaIni',103) AND r.AUDIT_FIN<=convert(datetime,'$desFechaFin',103) 
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = sprintf("EXECUTE sp_reporteAte7 '%s','%s'",
							$desFechaIni,
							$desFechaFin);
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('ate7',array('des' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant' => $this->num5($row[1],$desFechaIni,$desFechaFin)));
			$rs->Close();
		}
		unset($rs);
		
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'Ate7'.mktime();

		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte7.tpl.php'),false,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function cuentaAteToti($fecha1,$fecha2,$codTrab){
		$this->abreConnDB();
		$this->conn->debug = false;
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)>=convert(datetime,'$fecha1',103) 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)<=convert(datetime,'$fecha2',103) 
			and r.usuario=tr.email and tr.codigo_trabajador=$codTrab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
		
	}
	
	function cuentaAte($mes,$dia,$anyo,$trab){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email and tr.codigo_trabajador=$trab
			and r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}
	
	function cuentaAteTotalDia($mes,$dia,$anyo){
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha=$dia.'/'.$mes.'/'.$anyo;
				
		$sql="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s,db_general.dbo.h_trabajador tr  
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte --and s.codigo_tipo_soporte=5 
			and convert(datetime,convert(varchar,r.AUDIT_FIN,103),103)=convert(datetime,'$fecha',103) and r.usuario=tr.email 
			and.r.flag='O' ";
			
			$rs = & $this->conn->Execute($sql);
			unset($sql);
			if (!$rs){
				print $this->conn->ErrorMsg();
				$numero=0;
				exit;
				}
				else{
					while ($row = $rs->FetchRow())
						$numero=$row[0];
					$rs->Close();
				}
			unset($rs);
		return($numero);
	}

	function ReporteEspecial($mes,$anyo){
		// Genera HTML
		$html = new Smarty;
		
		$this->abreConnDB();
		$this->conn->debug = false;
		
		$fecha1='01/'.$mes.'/'.$anyo;
		if($mes==1||$mes==3||$mes==5||$mes==7||$mes==8||$mes==10||$mes==12){
			$fecha2='31/'.$mes.'/'.$anyo;
			$maxDias=31;
		}elseif($mes==2){
			$fecha2='28/'.$mes.'/'.$anyo;
			$maxDias=28;
			}else{
			$fecha2='30/'.$mes.'/'.$anyo;
			$maxDias=30;
			}
		/**/
		$sql_="SELECT count(*) 
			FROM dbo.h_reporte_2002 r,dbo.h_reporte_soporte2002 rs,dbo.h_soporte s 
			WHERE r.codigo_reporte=rs.codig_reporte and rs.codigo_soporte=s.codigo_soporte /*and s.codigo_tipo_soporte=5 */
			and r.AUDIT_FIN > convert(datetime,'$fecha1',103) 
			AND r.AUDIT_FIN <= convert(datetime,'$fecha2',103)
			and r.flag='O'  
			";
				 
		$rs_=& $this->conn->Execute($sql_);
		if(!$rs_){
			print $this->conn->ErrorMsg();
		}
		else{
			$total=$rs_->fields[0];//
		}
		$html->assign_by_ref('total',$total);

		
		$sql_st = "SELECT APELLIDOS_TECNICO+' '+NOMBRES_TECNICO,CODIGO_TRABAJADOR
					FROM dbo.h_tecnico 
					WHERE ESTADO='ACTIVO' 
					and codigo_trabajador>0 and codigo_trabajador<>646
					order by 1 
					";
	
	    //echo $sql_st; exit;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			while ($row = $rs->FetchRow())
				$html->append('trab',array('nomb' => strtoupper($row[0]),
										   'cod' => $row[1],
										   'cant1' => $this->cuentaAte($mes,1,$anyo,$row[1]),
										   'cant2' => $this->cuentaAte($mes,2,$anyo,$row[1]),
										   'cant3' => $this->cuentaAte($mes,3,$anyo,$row[1]),
										   'cant4' => $this->cuentaAte($mes,4,$anyo,$row[1]),
										   'cant5' => $this->cuentaAte($mes,5,$anyo,$row[1]),
										   'cant6' => $this->cuentaAte($mes,6,$anyo,$row[1]),
										   'cant7' => $this->cuentaAte($mes,7,$anyo,$row[1]),
										   'cant8' => $this->cuentaAte($mes,8,$anyo,$row[1]),
										   'cant9' => $this->cuentaAte($mes,9,$anyo,$row[1]),
										   'cant10' => $this->cuentaAte($mes,10,$anyo,$row[1]),
										   'cant11' => $this->cuentaAte($mes,11,$anyo,$row[1]),
										   'cant12' => $this->cuentaAte($mes,12,$anyo,$row[1]),
										   'cant13' => $this->cuentaAte($mes,13,$anyo,$row[1]),
										   'cant14' => $this->cuentaAte($mes,14,$anyo,$row[1]),
										   'cant15' => $this->cuentaAte($mes,15,$anyo,$row[1]),
										   'cant16' => $this->cuentaAte($mes,16,$anyo,$row[1]),
										   'cant17' => $this->cuentaAte($mes,17,$anyo,$row[1]),
										   'cant18' => $this->cuentaAte($mes,18,$anyo,$row[1]),
										   'cant19' => $this->cuentaAte($mes,19,$anyo,$row[1]),
										   'cant20' => $this->cuentaAte($mes,20,$anyo,$row[1]),
										   'cant21' => $this->cuentaAte($mes,21,$anyo,$row[1]),
										   'cant22' => $this->cuentaAte($mes,22,$anyo,$row[1]),
										   'cant23' => $this->cuentaAte($mes,23,$anyo,$row[1]),
										   'cant24' => $this->cuentaAte($mes,24,$anyo,$row[1]),
										   'cant25' => $this->cuentaAte($mes,25,$anyo,$row[1]),
										   'cant26' => $this->cuentaAte($mes,26,$anyo,$row[1]),
										   'cant27' => $this->cuentaAte($mes,27,$anyo,$row[1]),
										   'cant28' => $this->cuentaAte($mes,28,$anyo,$row[1]),
										   'cant29' => $this->cuentaAte($mes,29,$anyo,$row[1]),
										   'cant30' => $this->cuentaAte($mes,30,$anyo,$row[1]),
										   'cant31' => $this->cuentaAte($mes,31,$anyo,$row[1]),
										   'toti' => $this->cuentaAteToti($fecha1,$fecha2,$row[1])
										   
										   ));
			$rs->Close();
		}
		unset($rs);
		
			$html->assign_by_ref('total1',$this->cuentaAteTotalDia($mes,1,$anyo));
			$html->assign_by_ref('total2',$this->cuentaAteTotalDia($mes,2,$anyo));
			$html->assign_by_ref('total3',$this->cuentaAteTotalDia($mes,3,$anyo));
			$html->assign_by_ref('total4',$this->cuentaAteTotalDia($mes,4,$anyo));
			$html->assign_by_ref('total5',$this->cuentaAteTotalDia($mes,5,$anyo));
			$html->assign_by_ref('total6',$this->cuentaAteTotalDia($mes,6,$anyo));
			$html->assign_by_ref('total7',$this->cuentaAteTotalDia($mes,7,$anyo));
			$html->assign_by_ref('total8',$this->cuentaAteTotalDia($mes,8,$anyo));
			$html->assign_by_ref('total9',$this->cuentaAteTotalDia($mes,9,$anyo));
			$html->assign_by_ref('total10',$this->cuentaAteTotalDia($mes,10,$anyo));
			$html->assign_by_ref('total11',$this->cuentaAteTotalDia($mes,11,$anyo));
			$html->assign_by_ref('total12',$this->cuentaAteTotalDia($mes,12,$anyo));
			$html->assign_by_ref('total13',$this->cuentaAteTotalDia($mes,13,$anyo));
			$html->assign_by_ref('total14',$this->cuentaAteTotalDia($mes,14,$anyo));
			$html->assign_by_ref('total15',$this->cuentaAteTotalDia($mes,15,$anyo));
			$html->assign_by_ref('total16',$this->cuentaAteTotalDia($mes,16,$anyo));
			$html->assign_by_ref('total17',$this->cuentaAteTotalDia($mes,17,$anyo));
			$html->assign_by_ref('total18',$this->cuentaAteTotalDia($mes,18,$anyo));
			$html->assign_by_ref('total19',$this->cuentaAteTotalDia($mes,19,$anyo));
			$html->assign_by_ref('total20',$this->cuentaAteTotalDia($mes,20,$anyo));
			$html->assign_by_ref('total21',$this->cuentaAteTotalDia($mes,21,$anyo));
			$html->assign_by_ref('total22',$this->cuentaAteTotalDia($mes,22,$anyo));
			$html->assign_by_ref('total23',$this->cuentaAteTotalDia($mes,23,$anyo));
			$html->assign_by_ref('total24',$this->cuentaAteTotalDia($mes,24,$anyo));
			$html->assign_by_ref('total25',$this->cuentaAteTotalDia($mes,25,$anyo));
			$html->assign_by_ref('total26',$this->cuentaAteTotalDia($mes,26,$anyo));
			$html->assign_by_ref('total27',$this->cuentaAteTotalDia($mes,27,$anyo));
			$html->assign_by_ref('total28',$this->cuentaAteTotalDia($mes,28,$anyo));
			if($maxDias==30||$maxDias==31){
				$html->assign_by_ref('total29',$this->cuentaAteTotalDia($mes,29,$anyo));
				$html->assign_by_ref('total30',$this->cuentaAteTotalDia($mes,30,$anyo));
			}
			if($maxDias==31){
				$html->assign_by_ref('total31',$this->cuentaAteTotalDia($mes,31,$anyo));
			}

		$html->assign_by_ref('maxDias',$maxDias);
		setlocale(LC_TIME,"spanish");
		$html->assign('fechaGen',ucfirst(strftime("%A, %d de %B del %Y a las %H:%M %Ss")));

		$logo = $_SERVER['DOCUMENT_ROOT'] . '/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/institucional/informatica/helpdesk/reports';
		$filename = 'trab'.mktime();
		$this->CreaArchivoPDF($filename,$path,$html->fetch('oti/helpdesk/reporteAte8.tpl.php'),true,$logo);

		$destination = '/institucional/informatica/helpdesk/reports/' . $filename . '.pdf';
	    header("Content-type: application/pdf");
		header("Location: {$destination}");
		//echo $destination;
		exit;		
	}
	
	function DetalleInformeTecnico($id){
		// Genera HTML de Muestra
		$html = new Smarty;
		$this->abreConnDB();
		//$this->conn->debug=true;
		
		$sql="select d.asunto,d.observaciones,d.indicativo_oficio
				from db_tramite_documentario.dbo.documento d,dbo.h_reporte_2002 r
				where r.codigo_reporte=$id and r.id_documento=d.id_documento
			";
		$rs = & $this->conn->Execute($sql);
		unset($sql);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			$diagnostico=$rs->fields[0];
			$sol=$rs->fields[1];
			$indicativo=$rs->fields[2];
			$html->assign_by_ref('diagnostico',$diagnostico);
			$html->assign_by_ref('sol',$sol);
		}
		
				$sql_SP = sprintf("SELECT distinct Upper(t.APELLIDOS_TRABAJADOR)+', '+Upper(t.NOMBRES_TRABAJADOR),r.codigo_dependencia,r.descrip_realizado,
		                                   r.flag,Upper(dep.siglas), macpu.descripcion,mocpu.descripcion,cp.codigo_patrimonial,cp.numero_serie,
										   dep.dependencia,cp.area_pc
                            FROM dbo.h_reporte_2002 r, db_general.dbo.h_trabajador t,db_inventario_2002.user_inventario.pc p,
							     db_general.dbo.h_dependencia dep,db_inventario_2002.user_inventario.cpu cp,
                                 db_inventario_2002.user_inventario.marca_cpu macpu,db_inventario_2002.user_inventario.modelo_cpu mocpu  
                            WHERE r.codigo_reporte=%d and p.codigo_cpu=r.codigo_trabajador and t.codigo_trabajador=p.codigo_usuario and 
							      p.codigo_dependencia=dep.codigo_dependencia and p.codigo_cpu=cp.codigo_cpu
                                  and cp.codigo_marca_cpu=macpu.codigo_marca and cp.codigo_modelo_cpu=mocpu.codigo_modelo ",
								  $id
								  );;
		
		//echo $sql_SP;
		$rsData = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rsData){
			print $this->conn->ErrorMsg();
		}else{
				//while($DocumentoData = $rsData->FetchRow())
				$usuario=$rsData->fields[0];
				$coddep=$rsData->fields[1];
				$descrip=$rsData->fields[2];
				$flag=$rsData->fields[3];
				$siglas=$rsData->fields[4];
				$marca=$rsData->fields[5];
				$modelo=$rsData->fields[6];
				$codPat=$rsData->fields[7];
				$serie=$rsData->fields[8];
				$dep=$rsData->fields[9];
				$codPC=$rsData->fields[10];
		$html->assign_by_ref('usuario',$usuario);
		$html->assign_by_ref('coddep',$coddep);
		$html->assign_by_ref('descrip',$descrip);
		$html->assign_by_ref('flag',$flag);
		$html->assign_by_ref('siglas',$siglas);
		$html->assign_by_ref('marca',$marca);
		$html->assign_by_ref('modelo',$modelo);
		$html->assign_by_ref('codPat',$codPat);
		$html->assign_by_ref('serie',$serie);
		$html->assign_by_ref('dep',$dep);
		$html->assign_by_ref('codPC',$codPC);
			//unset($row);
			$rsData->Close();
		}
		unset($rsData);
		
		$titulo="INFORME T�CNICO N� ".$indicativo;
		$html->assign_by_ref('titulo',$titulo);

		// Variables de Applicacion
		$html->assign('frmName','frmPrint');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$html->assign_by_ref('accion',$this->arr_accion);
		
		$print ? $html->display('oti/helpdesk/printDetalle.tpl.php') : $html->display('oti/helpdesk/showDetalle.tpl.php');
	}
	
	function FormBuscaDependencia($page=NULL,$nombreDependencia=NULL,$status=NULL,$search=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		
		// Genera HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		
		
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));

		$html->assign_by_ref('status',$status);
		$html->assign_by_ref('nombreDependencia',$nombreDependencia);
				
		//Setea la Accion por Defecto del Formulario
		$html->assign_by_ref('accion',$this->arr_accion);

		// Setea el Numero de Pagina a Mostrar
		$html->assign('numPage',(empty($page)||is_null($page)) ? 1 : $page);
	
		// Muestra el Formulario
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate2 . 'search.tpl.php');
		if(!$search) $html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function BuscaDependencia($page,$nombreDependencia,$status){
		//$this->abreConnDB();
		//$this->conn->debug = true;
		// Genera Objeto HTML
		$html = new Smarty;
		//$html->assign('tipBusqueda',$tipBusqueda);
		// Setea Caracteristicas en el Formulario
		$frmName = 'frmBuscar';
		$page = (is_null($page)||empty($page)||$page==1) ? 1 : $page;
	
		// Se Muestra el Formulario de Busqueda Inicial
		$this->FormBuscaDependencia($page,$nombreDependencia,$status,true);

		// Arma los Condicionales de la Consulta
		$condConsulta = array();
		$condTable = array();
		
		//$condConsulta[] = "r.usuario=t.email";
		
		if($status!='T'){
			$condConsulta[] = " dep.condicion='$status' ";				
		}
	
		// Arma el Condicional que sera paramero del Store Procedure
		$table = (count($condTable)>0) ? ', '.implode(', ',$condTable) : '';
		$where = (count($condConsulta)>0) ? ' and '.implode(' and ',$condConsulta) : '';

		// Ejecuta el conteo General
		$this->abreConnDB();
		//$this->conn->debug = true;
		
        // Evalua que no contenga sentencias SQL
	    $this->evaluaNoSql($nombreDependencia);

		$sql_SP = sprintf("EXECUTE sp_busIDDepe %s,%s,%s,0,0",
							($nombreDependencia) ? "'".$this->PrepareParamSQL($nombreDependencia)."'" : 'NULL',
							($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
							($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL');
		//echo $sql_SP;
		$rs = & $this->conn->Execute($sql_SP);
		unset($sql_SP);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else{
			$numRegs = ($row = $rs->FetchRow()) ? $row[0] : 0;
			$rs->Close();
		}
		unset($rs);		

		// Si la Consulta tiene Resultados
		if($numRegs>0){
			// Arma Statement para Ejecutar el Store Procedure
			$start =  ($page == 1) ? 1 : ($page-1)*$this->numMaxResultsSearch;
			$stop = ($numRegs<($page*$this->numMaxResultsSearch)) ? $numRegs : $page*$this->numMaxResultsSearch;
			
			// Prepara el SP y agrega parametros
			$sql_SP = sprintf("EXECUTE sp_busIDDepe %s,%s,%s,%s,%s",
								($nombreDependencia) ? "'".$this->PrepareParamSQL($nombreDependencia)."'" : 'NULL',
								($table) ? "'".$this->PrepareParamSQL($table)."'" : 'NULL',
								($where) ? "'".$this->PrepareParamSQL($where)."'" : 'NULL',
								$start,
								$stop);
			//echo $sql_SP;
			// Ejecuta la Consulta Paginable a trav�s del Store Procedure
			$rsId = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rsId)
				print $this->conn->ErrorMsg();
			else{
				while($id = $rsId->FetchRow()){
					// Obtiene Todos los Datos del documento
					$sql_SP = sprintf("EXECUTE sp_busDatosDepe %d",$id[0]);
					//echo $sql_SP;
					$rs = & $this->conn->Execute($sql_SP);
					unset($sql_SP);
					if(!$rs)
						print $this->conn->ErrorMsg();
					else{
						while($AtencionData = $rs->FetchRow())
							$html->append('arrDepe', array('id' => $id[0],
														  'nomDepe' => ucfirst($AtencionData[1]),
														  'sigla' => $AtencionData[2],
														  'cond' => $AtencionData[3],
														  'telef' => $AtencionData[4],
														  'email' => ucwords($AtencionData[7]),
														  'seccion' => strtoupper($AtencionData[6]),
														  'userCrea' =>$AtencionData[8],
														  'auditCrea' =>$AtencionData[10],
														  'userMod' =>$AtencionData[9],
														  'auditMod' =>$AtencionData[11],
														  'tipDepe' =>$AtencionData[12]
														  ));
						$rs->Close();
					}
					unset($rs);
				}
				$rsId->Close();
			}
			unset($rsId);
		}else
			$start = 0;

		// Setea accion para el resultado Accion
		$html->assign('accion',$this->arr_accion);

		// Setea datos del Formulario CSV
		$html->assign('frmName','frmCSV');
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Datos del Resultado de la Busqueda
		$html->assign('outStart',($start!=1) ? $start+1 : $start);
		$html->assign_by_ref('outEnd',$stop);
		$html->assign_by_ref('outTotal',$numRegs);
		$html->assign_by_ref('menuSearchPaginable',$this->ObjMenuSearchPager($numRegs, $page, $frmName, $this->arr_accion[BUSCA_DEPENDENCIA], true));

		// Muestra el Resultado de la Busqueda
		$html->display($this->pathTemplate2 . 'searchResult.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function FormAgregaDependencia($tipDependencia=NULL,$nombreDependencia=NULL,$siglasDependencia=NULL,$condicion=NULL,$email=NULL,$seccion=NULL,$anexo=NULL,$piso=NULL,$directo=NULL,$categoriaDepe=NULL,$errors=false){
		//$this->abreConnDB();
		//$this->conn->debug = true;

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmAddDepe';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
				
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('nombreDependencia',$nombreDependencia);
		$html->assign_by_ref('siglasDependencia',$siglasDependencia);
		$html->assign_by_ref('condicion',$condicion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('seccion',$seccion);
		$html->assign_by_ref('anexo',$anexo);
		$html->assign_by_ref('piso',$piso);
		$html->assign_by_ref('directo',$directo);

		// Contenido Select de la Profesi�n
		$sql_st = "SELECT id,descripcion ".
					"FROM DB_GENERAL.DBO.TIPO_HDEPENDENCIA ".
					"order by 2";
		$html->assign_by_ref('selTipoDependencia',$this->ObjFrmSelect($sql_st, $tipDependencia, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		// Contenido Select de la Categor�a Dependencia
		$sql_st = "SELECT id_tipo_dependencia,descripcion ".
					"FROM DB_GENERAL.DBO.TIPO_DEPENDENCIA ".
					"order by 2";
		$html->assign_by_ref('selCategoriaDepe',$this->ObjFrmSelect($sql_st, $categoriaDepe, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate2 . 'frmAddDepe.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	
	}	

	function AgregaDependencia($tipDependencia,$nombreDependencia,$siglasDependencia,$condicion,$email,$seccion,$anexo,$piso,$directo,$categoriaDepe){

		// Comprueba Valores	
		if(!$nombreDependencia){ $bDep1 = true; $this->errors .= 'El nombre de la dependencia debe ser especificado<br>'; }
		if(!$siglasDependencia){ $bDep2 = true; $this->errors .= 'La sigla de la dependencia debe ser especificado<br>'; }
		
		if($bDep1||$bDep2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormAgregaDependencia($tipDependencia,$nombreDependencia,$siglasDependencia,$condicion,$email,$seccion,$anexo,$piso,$directo,$categoriaDepe,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;

	        // Evalua que no contenga sentencias SQL
        	$this->evaluaNoSql($nombreDependencia);
            $this->evaluaNoSql($siglasDependencia);

			$sql_SP = sprintf("EXECUTE sp_insnewDependencia %d,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%d",
							  $this->PrepareParamSQL($tipDependencia),
							  $this->PrepareParamSQL($nombreDependencia),
							  $this->PrepareParamSQL($siglasDependencia),
							  $this->PrepareParamSQL($condicion),
							  $this->PrepareParamSQL($email),
							  $this->PrepareParamSQL($seccion),
							  ($anexo && $anexo!="") ? $this->PrepareParamSQL($anexo) : "NULL",
							  $this->PrepareParamSQL($piso),
							  $this->PrepareParamSQL($directo),
							  $_SESSION['cod_usuario'],
							  $categoriaDepe
							  );
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[1]['val']}";
			header("Location: $destination");
			exit;
		}
	}
	
	function FormModificaDependencia($id,$tipDependencia=NULL,$nombreDependencia=NULL,$siglasDependencia=NULL,$condicion=NULL,$email=NULL,$seccion=NULL,$anexo=NULL,$piso=NULL,$directo=NULL,$categoriaDepe=NULL,$reLoad=false,$errors=false){
		if(empty($id)){
			$this->MuestraStatTrans($this->arr_accion[MALA_TRANS]);
			return;
		}
		if(!$reLoad){
			$this->abreConnDB();
			//$this->conn->debug = true;

			// Obtiene los Datos del Contacto
			$sql_SP = sprintf("EXECUTE sp_listDependencia %d",$this->PrepareParamSQL($id));
			// echo $sql_SP;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs){
				print $this->conn->ErrorMsg();
				return;
			}else{
				if($row = $rs->FetchRow()){
					$tipDependencia = $row[0];
					$nombreDependencia = $row[1];
					$siglasDependencia = $row[2];
					$condicion = $row[3];
					$email = $row[4];
					$seccion = $row[5];
					$piso = $row[6];
					$directo = $row[7];
					$anexo = $row[8];
					$categoriaDepe = $row[9];
				}
				$rs->Close();
			}
			unset($rs);
		}
		
		// Genera Objeto HTML
		$html = new Smarty;
		$html->assign_by_ref('menu',$this->GeneraMenuPager());
		$html->assign_by_ref('menuPager',$this->menuPager);
		
		$html->assign_by_ref('subMenu',$this->GeneraSubMenuPager());
		$html->assign_by_ref('subMenuPager',$this->subMenuPager);		

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmModifyDepe';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign('frmUrl',$_SERVER['PHP_SELF']);
		$st_jscript = "document.{$frmName}.accion.value=pAccion";
		$html->assign_by_ref('jscript',$this->insertaScriptSubmitForm($frmName,$st_jscript,'pAccion'));
		
		// Setea Campos de los Datos Ingresados por el Usuario
		$html->assign_by_ref('id',$id);
		$html->assign_by_ref('nombreDependencia',$nombreDependencia);
		$html->assign_by_ref('siglasDependencia',$siglasDependencia);
		$html->assign_by_ref('condicion',$condicion);
		$html->assign_by_ref('email',$email);
		$html->assign_by_ref('seccion',$seccion);
		$html->assign_by_ref('anexo',$anexo);
		$html->assign_by_ref('piso',$piso);
		$html->assign_by_ref('directo',$directo);
		$html->assign_by_ref('reLoad',$reLoad);

		// Contenido Select de la Profesi�n
		$sql_st = "SELECT id,descripcion ".
					"FROM DB_GENERAL.DBO.TIPO_HDEPENDENCIA ".
					"order by 2";
		$html->assign_by_ref('selTipoDependencia',$this->ObjFrmSelect($sql_st, $tipDependencia, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		// Contenido Select de la Categor�a Dependencia
		$sql_st = "SELECT id_tipo_dependencia,descripcion ".
					"FROM DB_GENERAL.DBO.TIPO_DEPENDENCIA ".
					"order by 2";
		$html->assign_by_ref('selCategoriaDepe',$this->ObjFrmSelect($sql_st, $categoriaDepe, true, true, array('val'=>'none','label'=>'Seleccione una opci�n')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Resultado de la Busqueda
		//$html->display($this->pathTemplate . 'headerArm.tpl.php');
		$html->display($this->pathTemplate2 . 'frmModifyDepe.tpl.php');
		$html->display($this->pathTemplate . 'footerArm.tpl.php');
	}
	
	function ModificaDependencia($id,$tipDependencia,$nombreDependencia,$siglasDependencia,$condicion,$email,$seccion,$anexo,$piso,$directo,$categoriaDepe){

		// Comprueba Valores	
		if(!$nombreDependencia){ $bDep1 = true; $this->errors .= 'El nombre de la dependencia debe ser especificado<br>'; }
		if(!$siglasDependencia){ $bDep2 = true; $this->errors .= 'La sigla de la dependencia debe ser especificado<br>'; }
		
		if($bDep1||$bDep2){
			$objIntranet = new Intranet();
			$objIntranet->Header('Help Desk',false,array('suspEmb'));
			$objIntranet->Body('helpdesk_tit.gif');
			
			$errors = & $this->muestraMensajeInfo($this->errors,true);
			$this->FormModificaDependencia($id,$tipDependencia,$nombreDependencia,$siglasDependencia,$condicion,$email,$seccion,$anexo,$piso,$directo,$categoriaDepe,$errors);
			
			$objIntranet->Footer();
		}else{
			$this->abreConnDB();
			//$this->conn->debug = true;

	        // Evalua que no contenga sentencias SQL
        	$this->evaluaNoSql($nombreDependencia);
            $this->evaluaNoSql($siglasDependencia);

			$sql_SP = sprintf("EXECUTE sp_modifyDependencia %d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%d",
							  $id,
							  $this->PrepareParamSQL($tipDependencia),
							  $this->PrepareParamSQL($nombreDependencia),
							  $this->PrepareParamSQL($siglasDependencia),
							  $this->PrepareParamSQL($condicion),
							  $this->PrepareParamSQL($email),
							  $this->PrepareParamSQL($seccion),
							  ($anexo && $anexo!="") ? $this->PrepareParamSQL($anexo) : "NULL",
							  $this->PrepareParamSQL($piso),
							  $this->PrepareParamSQL($directo),
							  $_SESSION['cod_usuario'],
							  $categoriaDepe
							  );
			// echo $sql_SP;exit;
			$rs = & $this->conn->Execute($sql_SP);
			unset($sql_SP);
			if (!$rs)
				$RETVAL=1;
			else{
				$RETVAL = ($row = $rs->FetchRow()) ? $row[0] : 1;
				$rs->Close();
			}
			unset($rs);
			
			$destination = $_SERVER['PHP_SELF'] . '?accion=';
			$destination .= ($RETVAL) ? $this->arr_accion['MALA_TRANS'] : $this->arr_accion['BUENA_TRANS'];
			$destination .= "&menu={$this->menu_items[1]['val']}&subMenu={$this->subMenu_items[2]['val']}";
			header("Location: $destination");
			exit;
		}
	}	

}
?>
