<?php
include_once 'mimemail/htmlMimeMail.php';
include_once 'claseModulos.inc.php';

class Mensajes extends Modulos{
	
	var $emailDomain = 'CONVENIO_SITRADOC.gob.pe';
	var $userSugerencia = 'intranet';
	var $maxEnvioGrupal = 2;

	function Mensajes() {
		$this->statConn = false;
		$this->serverDB = $this->arr_serverDB['postgres'][0];
		$this->typeDB = $this->arr_typeDB['postgres'];
		$this->DB = $this->arr_DB['postgres'][0];
		$this->userDB = $this->arr_userDB['postgres'][0];
		$this->passDB = $this->arr_passDB['postgres'][0];
		
		$this->arr_accion = array(
							FRM_ENVIA_MSG_GRUPAL => 'frmSendMsgGroup',
							ENVIA_MSG_GRUPAL => 'sendMsgGroup'
							);
	}
	
	function EnviaSugerencia($titulo, $mensaje){
		$eFrom = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;
		$eDest = $this->userSugerencia . "@" . $this->emailDomain;

		// Envia el correo de Texto plano
		$mail = & new htmlMimeMail();
		$mail->setSubject($titulo);
		$mail->setText($mensaje);
		$mail->setFrom($eFrom);
		$mail->setBcc($eDest);
		$mail->setReturnPath($eFrom);
		$result = $mail->send(array(''));
		
		return $result;
	}

	function FormEnviaMsgGrupal($desTitulo=NULL,$idGrupo=NULL,$desMensaje=NULL){
		// Genera Objeto HTML
		$html = new Smarty;

		// Setea Caracteristicas en el Formulario
		$frmName = 'frmMsgGrupal';
		$html->assign_by_ref('frmName',$frmName);
		$html->assign_by_ref('frmUrl',$_SERVER['PHP_SELF']);
		
		// Setea Valores de los Campos del Formulario
		$html->assign_by_ref('desTitulo',$desTitulo);
		$html->assign_by_ref('desMensaje',$desMensaje);

		// Grupos Permitidos
		$sql_st = sprintf("SELECT g.id_grupo, g.des_grupo ".
						  "FROM grupos_email g LEFT JOIN grupos_email_integridad gi ".
							   "ON g.id_grupo=gi.id_grupo ".
							   "AND date_trunc('day',gi.fec_envio)=date_trunc('day',now()) ".
							   "AND gi.cod_usuario='%s' ".
						  "GROUP BY g.id_grupo, g.des_grupo ".
						  "HAVING count(gi.fec_envio)<%d ".
						  "ORDER BY g.des_grupo",
						  $_SESSION['cod_usuario'],
						  $this->maxEnvioGrupal);

		$html->assign_by_ref('selGrupo',$this->ObjFrmSelect($sql_st, $idGrupo, true, false, array('val'=>'NULL','label'=>'Escoja un Grupo')));
		unset($sql_st);
		
		// Setea accion para el resultado Accion
		$html->assign_by_ref('accion',$this->arr_accion);
		
		// Setea los Errores
		$html->assign_by_ref('errors',$errors);

		// Muestra el Formulario de Envio
		$html->display('intranet/mensajeGrupal/frmMensajePrueba.tpl.php');
	}
	
	function EnviaMsgGrupal($desTitulo,$idGrupo,$desMensaje,&$adjunto){
	
		$this->abreConnDB();
		//$this->conn->debug = true;
		
		$sql_st="SELECT eml_grupo ".
				"FROM grupos_email ".
				"WHERE ind_activo IS TRUE and id_grupo=" . $idGrupo;
				//echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$eDest = str_replace(',', '@' . $this->emailDomain . ',', $row[0]) . '@' . $this->emailDomain;
			unset($row);
			$rs->Close(); # optional
		}
		unset($rs);
		
		//echo "dest".$eDest;

		$eFrom = $_SESSION['cod_usuario'] . "@" . $this->emailDomain;
	  //echo "<br>desde".$eFrom;
		// Envia el correo de Texto plano con el Adjunto
		$mail = & new htmlMimeMail();
		$mail->setSubject($desTitulo);
		//$mail->setText($desMensaje);
		$mail->setHtml($desMensaje);
		
		

		if(is_uploaded_file($adjunto['tmp_name'])){
			$attachment = & $mail->getFile($adjunto['tmp_name']);
			$mail->addAttachment($attachment, $adjunto['name'], $adjunto['type']);
		}
		
		$mail->setFrom($eFrom);
		$mail->setBcc($eDest);
		$mail->setReturnPath($eFrom);
		$result = $mail->send(array());
		//echo "gg".$result;
		
		if($result){
			$ins_st = sprintf("INSERT INTO grupos_email_integridad(cod_usuario,id_grupo) ".
							  "VALUES('%s',%d)",
							  $_SESSION['cod_usuario'],
							  $this->PrepareParamSQL($idGrupo));
			if( $this->conn->Execute($ins_st) === false )
				print $this->conn->ErrorMsg();
		}		

		// Genera Objeto HTML
		$html = new Smarty;

		$html->assign_by_ref('result',$result);
		// Muestra el Resultado del Envio
		$html->display('intranet/mensajeGrupal/statMensaje.tpl.php');
	}
}

?>
