<?
@umask(0007);
require_once('datosGlobales.php');
include_once('adodb.inc.php');
require_once('Smarty.class.php');

class General{
	var $HOST_SITE = 'intranetSITRADOC.pe';
	// Contiene los Target permitidos para los links
	var $targetLink = array("_self","_blank","_parent","_top","window");
	var $zonaHoraria = 'es_PE';
	var $errors = "<b>Ha ocurrido el(los) siguiente(s) error(es):</b><br>";

	// DECLARACION DE CONSTANTES DE CONEXION
	// Almacena el estado de la Conexion a la DB (boolean)
	var $statConn = false;
	var $arr_serverDB = array('postgres' => array('127.0.0.1','localhost'),
						'mssql' => array('mipe','CONVENIO_SITRADOCBK',CJDBMSSQL,'bk-srvbdsql','OGTIE-1361','pr-srvbdsql','srvsql2005','172.16.247.100',CJDBMSSQL),
						'mysql' => array('127.0.0.1'));
	// Almacena los tipos de Motor de DB's que puede manejar las App's
	var $arr_typeDB = array('postgres' => 'postgres',
							'mssql' => 'mssql',
							'mysql' => 'mysql',
							'oracle' => 'Oci8');
	// Almacena los nombres de las DB's contenidas en cada Motor
	var $arr_DB = array('postgres' => array(0 => 'intranet',
											1 => 'tramite',
											2 => 'portal'),
						'mssql'    => array(0 => 'DB_GENERAL',
											1 => 'HELPDESK',
											2 => 'DNA',
											3 => 'DB_SANCION_MULTA',
											4 => 'DB_OGA',
											5 => 'DB_DNEPP',
											6 => 'DNPA',
											7 => 'DB_ADMIN_PROYECTO',
											8 => 'DB_INVENTARIO_2002',
											9 => 'DB_DOCUMENTA',
											10 => 'BD_CENSO',
											11 => 'DB_TRAMITE_DOCUMENTARIO',
											12 => 'DB_DELITO_ADUANERO',
											13 => 'DB_PAPELETA',
											14 => 'DB_VISITA_RECEPCION',
											15 => 'PESQUERIADB'),
						'mysql'    => array(0 => 'minpesdb',
											1 => 'homeminpesdb'),
						'oracle'   => array(0 => 'PROD',
											1 => 'IBM205'));
	// Almacena los nombres de Usuario de DB's para cada Motor
	var $arr_userDB = array('postgres' => array('postgres','portal_user'),
							'mssql' => array(CJDBMSSQLUSER,CJDBMSSQLUSER),
							'mysql' => array('root'),
							'oracle' => array('web','cls'));
	// Almacena las contrase�as para cada Usuario de DB
	var $arr_passDB = array('postgres' => array('gonta_patan','CWScJk152TwNJjz'),
							'mssql' => array(CJDBMSSQLPASS,CJDBMSSQLPASS),
							'mysql' => array('05mar2004ja'),
							'oracle' => array('minpro','sogra'));
	
	// Declaracion de Variables Globales
	var $conn;		 		// Almacena la conexion a la base de datos
	var $serverDB;	 		// Almacena el Host del Servidor de DBs
	var $typeDB;			// Indica el Tipo de Motor de DB a utilizar
	var $DB;		 		// Indica el nombre de la DB a utilizar
	var $userDB;	 		// Indica el Usuario de la DB
	var $passDB;	 		// Indica el Password del Usuario $userdb
	var $mesesNombres; 		// Contiene el Nombre de los Meses del Anyo
	var $userIntranet;  	// Contiene los datos MSSQL del Usuario que esta Utilizando la Intranet
	
	var $userSITRADOC;  	// Contiene los datos MSSQL del Usuario que esta Utilizando la Intranet

	function abreConnDB(){
		if($this->statConn == false){
			$this->conn = & ADONewConnection($this->typeDB);
			$this->conn->debug = false;
			if($this->typeDB == 'postgres'){
				if($this->serverDB == $this->arr_serverDB['postgres'][0]||!$this->serverDB)
					$strConn = "dbname={$this->DB} user={$this->userDB} password={$this->passDB}";
				else
					$strConn = "host={$this->serverDB} dbname={$this->DB} user={$this->userDB} password={$this->passDB}";
					
				if(!$this->conn->Connect($strConn)){
					print $this->conn->ErrorMsg();
					$this->statConn = false;
				}else
					$this->statConn = true;
			}elseif($this->typeDB == 'Oci8'){
				if(!$this->conn->Connect(false,$this->userDB,$this->passDB,$this->DB)){
					print $this->conn->ErrorMsg();
					$this->statConn = false;
				}else
					$this->statConn = true;
			}else{
				if(!$this->conn->Connect($this->serverDB, $this->userDB, $this->passDB, $this->DB)){
					print $this->conn->ErrorMsg();
					$this->statConn = false;
				}else
					$this->statConn = true;
			}
		}
	}

	
	function cierraConnDB(){
		if($this->statConn == true){
			$this->conn->Close();
			unset($this->conn);
			$this->statConn = false;
		}
	}

	function PrepareParamSQL($param,$carac="'"){
		return str_replace($carac,$carac.$carac,$param);
	}
	
	function SequenceNextVal($seq){
		$this->abreConnDB();
		
		$sql_st = "SELECT nextval('{$seq}')";
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else
			return ($row = $rs->FetchRow()) ? $row[0] : false;
	}
	
	function datosUsuarioMSSQL(){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.codigo_trabajador, t.nombres_trabajador, t.apellidos_trabajador, t.coddep, d.dependencia, d.siglas, t.director, t.idsubdependencia, t.nivel ".
				  "FROM db_general.dbo.h_trabajador t, db_general.dbo.h_dependencia d ".
				  "WHERE t.coddep=d.codigo_dependencia and t.estado='ACTIVO' and t.email='{$_SESSION['cod_usuario']}'";
		
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$this->userIntranet = array(
									  'CODIGO' => $row[0],
									  'NOMBRE' => $row[1],
									  'APELLIDO' => $row[2],
									  'COD_DEP' => $row[3],
									  'DEPENDENCIA' => $row[4],
									  'SIGLA_DEP' => $row[5],
									  'DIRE' => $row[6],
									  'SUB_DEP' => $row[7],
									  'NIVEL' => $row[8]
									  );
			}else{
				$this->userIntranet = false;
				$this->errors .= "No ha sido registrado como usuario valido en el Sistema.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}
	
	function datosUsuarioPOSTGRESQL(){
		$this->abreConnDB();
		
		$sql_st = "SELECT t.id_trabajador, t.des_nombres, t.des_apellidos, s.id_subdependencia, ".
						  "CASE s.des_subdependencia WHEN 'NINGUNA' THEN d.des_dependencia ELSE s.des_subdependencia END, ".
						  "s.des_siglas ".
				  "FROM trabajador t, subdependencia s, dependencia d ".
				  "WHERE t.id_subdependencia=s.id_subdependencia and s.id_dependencia=d.id_dependencia and t.cod_trabajador='{$_SESSION['cod_usuario']}'";
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$this->userIntranet = array(
									  'CODIGO' => $row[0],
									  'NOMBRE' => $row[1],
									  'APELLIDO' => $row[2],
									  'COD_DEP' => $row[3],
									  'DEPENDENCIA' => $row[4],
									  'SIGLA_DEP' => $row[5]
									  );
			}else{
				$this->userIntranet = false;
				$this->errors .= "No ha sido registrado como usuario valido en el Sistema.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}
	
	function datosUsuarioPortal(){
		$this->abreConnDB();
		
		$sql_st = sprintf("SELECT id_usuario ".
						  "FROM public.usuario ".
						  "WHERE ind_activo IS TRUE and cod_usuario='%s'",$_SESSION['cod_usuario']);
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$this->userIntranet = array(
									  'CODIGO' => $row[0]
									  );
			}else{
				$this->userIntranet = false;
				$this->errors .= "No ha sido registrado como usuario valido en el Sistema.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}
	
	function CurrentDate($literal=true,$time=false){
		setlocale (LC_TIME, $this->zonaHoraria);
		$strDate = ($literal) ? (ucfirst(strtolower(strftime("%A, %d de %B de %Y")))) : strftime("%d/%m/%Y");
		$strDate .= ($time) ? strftime(" a las %T") : '';
		return $strDate;
	}

		
	function getURLLink($path,$target='self',$atributos=NULL){
		switch($target){
			case '_self':
				//$url = "/".session_id()."${path}";
				$url = "${path}";
				break;
			case '_blank':
				//$url = "/".session_id()."${path}\" target=\"_blank";
				$url = "${path}\" target=\"_blank";
				break;
			case 'window':
				//$url = "javascript: window.open('/".session_id()."${path}','window','${atributos}'); void('')";
				$url = "javascript: window.open('${path}','window','${atributos}'); void('')";
				break;
			case '_main':
				$url = (($_SERVER['SERVER_PORT']==443) ? 'https' : 'http') . '://'. $_SERVER['HTTP_HOST'] . $path;
				break;
		}
		return $url;
	}

	function ReturnOneRowSQL($sql_st){
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs)
			print $this->conn->ErrorMsg();
		else
			return $rs->FetchRow();

		unset($rs);
		
		return false;
	}
	
	function datosPermisoSITRADOC(){
		$this->abreConnDB();
		
		$sql_st = "SELECT 	case when ind_resolucion='TRUE' then 't' else 'f' end, 
							case when ind_correspondencia='TRUE' then 't' else 'f' end,
					      	case when ind_finalizacion='TRUE' then 't' else 'f' end,
							case when ind_archivo='TRUE' then 't' else 'f' end,
						  	case when CONSULTADOCSINTERNOS='TRUE' then 't' else 'f' end,
							case when ind_anulacion='TRUE' then 't' else 'f' end,
						  	case when ind_correccionDoc='TRUE' then 't' else 'f' end,
							case when administracion='TRUE' then 't' else 'f' end,
						  	case when ind_COORDINADOR='TRUE' then 't' else 'f' end,
						  	case when ind_adm_documento='TRUE' then 't' else 'f' end,
						  	case when ind_papeletas_deposito='TRUE' then 't' else 'f' end,
						  	case when ind_cert_retenciones='TRUE' then 't' else 'f' end,
							
						  	case when ind_dev_decomiso='TRUE' then 't' else 'f' end,
						  	case when ind_doc_garantia='TRUE' then 't' else 'f' end,
							case when id_utilitario='TRUE' then 't' else 'f' end,
							case when adm_ogtie='TRUE' then 't' else 'f' end,
							case when adm_rederivacion='TRUE' then 't' else 'f' end
													 													
					 ".
				  "FROM db_general.dbo.h_trabajador t, dbo.permiso p  ".
				  "WHERE t.email='{$_SESSION['cod_usuario']}' and t.codigo_trabajador=p.codigo_trabajador";
		//		echo $sql_st;
		$rs = & $this->conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $this->conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$this->userSITRADOC = array(
									  'RESOLUCION' => $row[0],
									  'CORRESPONDENCIA' => $row[1],
									  'FINALIZACION' => $row[2],
									  'ARCHIVO' => $row[3],
									  'CONSULTADOCSINTERNOS' => $row[4],
									  'ANULACION' => $row[5],
									  'CORRECCIONDOC' => $row[6],
									  'ADMINISTRACION' => $row[7],
									  'COORDINADOR' => $row[8],
									  'ADMINISTRACION_DOC' => $row[9],
									  'PAPELETAS_DEPOSITO' => $row[10],  
									  'CERTIFICA_RETEN' => $row[11],
									  'DEVOLUCION_DECOMISO' => $row[12],
									  'DOC_GARANTIA' => $row[13],
									  'ADM_UTILITARIO' => $row[14],
									  'ADM_OGTIE' => $row[15],
									  'ADM_REDERIVACION' => $row[16]									  
									  );
			}else{
				$this->userSITRADOC = false;
				$this->errors .= "No ha sido registrado como usuario valido en el SITRADOC.<br>";
			}
			$rs->Close();
		}
		unset($rs);
	}	

        function evaluaNoSql($desNombre){
         //C�digo para detectar intrusos
          if(strpos(strtoupper($desNombre), "SELECT") !== false) exit;
          if(strpos(strtoupper($desNombre), "DELETE") !== false) exit;
          if(strpos(strtoupper($desNombre), "INSERT") !== false) exit;
          if(strpos(strtoupper($desNombre), "DROP") !== false) exit;
          if(strpos(strtoupper($desNombre), "UPDATE") !== false) exit;
          if(strpos(strtoupper($desNombre), "ALTER") !== false) exit;
          return true;
        }
			
}

?>
