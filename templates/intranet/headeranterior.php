<?
include_once("auth.php");
begin_session();
entry_rigth();
include_once('modulos.php');
include_once('modulosadmin/claseModulos.inc.php');
$modulos = new Modulos;
?>
<html>
<head>
<title>Intranet MIDIS <?if( $titulo_pag != "" ) echo "- $titulo_pag"; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<script src="/js/intranet.js">
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="779" border="0" cellspacing="0" cellpadding="0" <?if($index_intranet){?> background="/img/800x600/bg_index.gif" <?}?>>
  <tr>
    <td>       
    <table width="779" border="0" cellspacing="0" cellpadding="0">
      <tr>
          
          <td width="199" valign="top"><img src="/img/800x600/cabecera1-1.gif" width="199" height="86"></td>
          <td width="471">
            
            <table width="580" border="0" cellspacing="0" cellpadding="0">
              <tr>
                
                <td valign="top" background="/img/800x600/f_header1.gif"> 
                  <table width="580" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td rowspan="4" height="70" class="menhea" valign="middle" width="109"><img src="/img/800x600/cabecera1-2.gif" width="109" height="70"></td>
                      
                    <td bgcolor="#464D95" height="25" class="menhea" valign="middle"> 
                      <!-- Aqui Empieza Items del Menu Institucional -->
                      <? $modulos->muestraIndexSubmodulosHTML(NULL,1); ?>
                      <!-- Aqui Termina Items del Menu Institucional -->
                    </td>
                    </tr>
                    <tr> 
                      <td height="1"></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#3270AB" height="25" class="menhea"> 
                        <!-- Aqui Empieza Items del Menu Personal -->
                        <? $modulos->muestraIndexSubmodulosHTML(NULL,1,2); ?>
                        <!-- Aqui Termina Items del Menu Personal -->
                      </td>
                    </tr>
                    <tr> 
                      <td height="19" align="right"><span class="userIntra"><strong>&nbsp;&nbsp;<?php echo $_SESSION['cod_usuario'] . '@' . $MAIL_DOMAIN ?> (En l&iacute;nea)&nbsp;&nbsp;</strong></span></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                
              <td align="right">
                  <table border="0" cellspacing="0" cellpadding="0" width="580">
                    <tr> 
                      <td class="fecha" valign="middle" width="258"><b> 
                        <? echo ($index_intranet) ? "&nbsp;" : muestraFechaActual(); ?>
                        </b></td>
                      <!--<td width="113"><img src="/img/lb_icos.gif" width="113" height="17" name="lb_ico"></td>-->
                      <td width="14">&nbsp;</td>
                      <td width="18">
                        <!-- Aqui Empieza Menu para mostrar los mensajes grupales -->
                        <? $msggrupo=$modulos->mensajesGrupos();
                           echo $msggrupo;
                        ?>
                        <!-- Aqui Termina Menu para mostrar los mensajes grupales -->
		      </td>
                      <td width="4">&nbsp;</td>
                      <!--<td width="18"><a href="<?echo "$abs_path/$SUGERENCIA_PAGE";?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('lb_ico','','/img/lb_suge.gif','ico_hea1','','/img/ico_suge_o.gif',0)"><img src="/img/ico_suge.gif" width="18" height="17" name="ico_hea1" border="0"></a></td>-->
			<?php /*?>								<td width="18"><a href="<?echo "$abs_path/$SUGERENCIA_PAGE";?>"><img src="/img/lb_suge.gif" alt="Enviar sugerencias" width="76" height="17" name="ico_hea1" vspace="0" border="0"></a></td><?php */?>
                      <td width="20">&nbsp;</td>
                      <td width="135" valign="top"><img src="/img/800x600/panel-logout.gif" width="135" height="16" usemap="#maplogout" border="0"></td>
                    </tr>
                  </table>
              </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
<map name="maplogout"> 
  <area shape="rect" coords="4,0,83,16" href="<?echo "/$INDEX_PAGE";?>">
  <area shape="rect" coords="84,0,135,16" href="<?echo "/$EXIT_PAGE";?>">
</map>
