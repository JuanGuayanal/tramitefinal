<?
// error_reporting(E_ALL);
require_once('datosGlobales.php');
include_once('adodb.inc.php');
include_once('config_intranet.php');

function begin_session(){
//	ereg("/([0-9a-z]{32})", $_SERVER['REQUEST_URI'], $regs);
//	$session_id = $regs[1];
//	if(isset($session_id) || !empty($session_id)){
//		session_id($session_id);
	global $SESSION_NAME;
		if(!isset($_SESSION)){
			session_name($SESSION_NAME);
			session_start();
			
			/*$_SESSION['secure_pas'] = 1;
			$_SESSION['cod_usuario'] = 'balva';
			$_SESSION['time_start'] = '1337476473.8378';
			$_SESSION['fob'] = '1 390.966';
			$_SESSION['uit'] = '3 650';
			$_SESSION['tasaDiaria'] = '0.0000';
			$_SESSION['factorAcumulado'] = '0.0000';
			$_SESSION['dolarCompra'] = '2.668';
			$_SESSION['dolarVenta'] = '2.669';
			$_SESSION['modulo_id'] = '';
			$_SESSION['mod_ind_leer'] = '';
			$_SESSION['mod_ind_insertar'] = '';
			$_SESSION['mod_ind_modificar'] = '';
			$_SESSION['cods_grupos'] = '1,3';*/
/*
Array
(
    [secure_pas] => 1
    [cod_usuario] => balva
    [time_start] => 1337476473.8378
    [fob] => 1 390.966
    [uit] => 3 650
    [tasaDiaria] => 0.0000
    [factorAcumulado] => 0.0000
    [dolarCompra] => 2.668
    [dolarVenta] => 2.669
    [modulo_id] => 
    [mod_ind_leer] => 
    [mod_ind_insertar] => 
    [mod_ind_modificar] => 
    [cods_grupos] => 1,3
)
*/
		}
		if(!status_secure()){
			session_unset();
			session_destroy();
		}
		/*
		session_start();
		if(!status_secure()){
			session_unset();
			session_destroy();
		}
		*/
//	}
//var_dump($_SESSION);
}

function destroy_session(){
	session_unset();
	session_destroy();
	$destination = "http://" . $_SERVER['HTTP_HOST'] . "/";
	header("Location: $destination");
	exit;
}

function status_secure(){
	if($_SESSION['secure_pas'])
		return 1;
	else
		return 0;	
}

function entry_rigth(){
	if(!status_secure()){
		$destination = "http://" . $_SERVER['HTTP_HOST'] . "/";
		header("Location: $destination");
		exit();
	}
	if(time_session_life()){
		destroy_session_by_time();
	}
	//getModRights();
}

function getModRights(){
	global $serverdb, $type_db, $userdb, $passdb, $db, $username, $userpass;
	
	// Obtiene el PATH de la Pagina visitada
	$url = $_SERVER['PHP_SELF'];
	ereg("/([0-9a-z]{32})", $_SERVER['PHP_SELF'], $regs);
	$path = dirname(str_replace($regs[0], "", $url))."/";
	
	// Define los derechos iniciales
	$_SESSION['modulo_id'] = NULL;
	$_SESSION['mod_ind_leer'] = false;
	$_SESSION['mod_ind_insertar'] = false;
	$_SESSION['mod_ind_modificar'] = false;
	//print_r($type_db);
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	//if(!$conn->Connect("host=". $serverdb['postgres'] ." dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
	
	if(!$conn->Connect($serverdb['mssql'], $userdb['mssql'][0], $passdb['mssql'][0], $db['postgres'][0])){
		print $conn->ErrorMsg();
	}else{
		// Obtiene los Grupos a los q pertenece el Usuario
		echo $sql_st = "SELECT DISTINCT(gu.id_grupo) ".
				  "FROM grupo g, grupo_usuario gu ".
				  "WHERE g.id_grupo=gu.id_grupo and gu.cod_usuario='{$_SESSION['cod_usuario']}' and g.ind_activo IS true";
		$rs = $conn->Execute($sql_st);
		unset($sql_st);
		//exit();
		if (!$rs){
			
			print $conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow()) {
				$cods_grupos[] = $row[0];
			}
			$_SESSION['cods_grupos'] = implode(",",$cods_grupos);
			unset($row);
			$rs->Close();
		}
		unset($rs);
		
		// Obtiene el ID del modulo que contiene la pagina visitada
		$sql_st = "SELECT id_modulo ".
				  "FROM modulo ".
				  "WHERE des_path='$path' and ind_activo IS true";
		$rs = $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$_SESSION['modulo_id'] = $row[0];
				// Obtiene los derechos del Usuario o Grupos a los que pertenece
				// sobre el Modulo Visitado
				$sql_ders_st = "SELECT ind_leer, ind_insertar, ind_modificar ".
							   "FROM modulo_derecho ".
							   "WHERE id_modulo={$row[0]} and (cod_usuario='{$_SESSION['cod_usuario']}'";
				$sql_ders_st .= (!is_null($_SESSION['cods_grupos'])) ? " or id_grupo IN ({$_SESSION['cods_grupos']})" : "";
				$sql_ders_st .= ")";
				$rs_ders = $conn->Execute($sql_ders_st);
				unset($sql_ders_st);
				if (!$rs_ders){
					print $conn->ErrorMsg();
				}else{
					while ($row_ders = $rs_ders->FetchRow()) {
						if($row_ders[0]=='t')
							$_SESSION['mod_ind_leer'] = true;
						if($row_ders[1]=='t')
							$_SESSION['mod_ind_insertar'] = true;
						if($row_ders[2]=='t')
							$_SESSION['mod_ind_modificar'] = true;
					}
					unset($row_ders);
					$rs_ders->Close();
				}
				unset($rs_ders);
			}
			unset($row);
			$rs->Close();
		}
		unset($rs);
		$conn->Close();
	}	
}

// Entrega True si el tiempo ha expirado y False si la session continuara viva
function time_session_life_09112013(){
	global $MAX_SESSION_TIME;
    list($usec, $sec) = explode(" ",microtime()); 
    $time_end = (float)$usec + (float)$sec; 
	if( ( $time_end - $_SESSION['time_start'] ) > $MAX_SESSION_TIME ){		
		return 1;
	}else{
		$_SESSION['time_start']=$time_end;
		return 0;
	}
}


function time_session_life(){
return 0;
}
function destroy_session_by_time(){
	global $EXPIRATION_PAGE;
	session_unset();
	session_destroy();	
	$destination = "http://" . $_SERVER['HTTP_HOST'] . "/$EXPIRATION_PAGE";
	header("Location: $destination");
	exit;
}

function autentifica(){
	
	//exit();
	global $SESSION_NAME, $serverdb, $type_db, $userdb, $passdb, $db, $username, $userpass;
	$username = strtolower($username);
	if(empty($username)||empty($userpass)) return 0;
		
		// Abre una conexi�n a la Base de Datos PostgreSQL
		
		/*$conn_pgsql = & ADONewConnection($type_db['postgres']);
		$conn_pgsql->debug = false;*/
		
		// Consulta de los Datos Personales del Usuario

		$conn_mssql = & ADONewConnection($type_db['mssql']);
		$conn_mssql->debug = false;
		if(!$conn_mssql->Connect($serverdb['mssql'], $userdb['mssql'][0], $passdb['mssql'][0], $db['mssql'][0]))
		{
			print $conn_mssql->ErrorMsg();
                        $_REQUEST['mensaje_error']="<CENTER>Error de Sistema, favor intente nuevamente o p�ngase en contacto con el <a href=\"mailto:intranet@CONVENIO_SITRADOC.gob.pe?subject=Problema de sistema\">administrador</a> .</CENTER>";
                        return 0;
		}
		else{
			$sql_st = "SELECT d.dependencia, idprofesion, t.condicion, nombres_trabajador, ".
				  // INICIO JGH 08/09/2016
                  //"apellidos_trabajador, fecha_nacimiento, dni, telefono, publicado, estado , t.password, d.confidencial ".
                  "apellidos_trabajador, fecha_nacimiento, dni, telefono, publicado, estado , t.password, d.confidencial,t.COD_DEP_MESA,d.DEP_PUBLICO ".
                  // FIN JGH 08/09/2016
				  "FROM dbo.h_trabajador t, dbo.h_dependencia d ".
				  "WHERE t.email='$username' and t.coddep_pub=d.codigo_dependencia and t.estado='ACTIVO'  ";
			$rs = & $conn_mssql->Execute($sql_st);
			unset($sql_st);
			if (!$rs){
				print $conn_mssql->ErrorMsg();
			}else{
				if ($row = $rs->FetchRow()) {
					// Se agrega el resultado en variables
					$dependencia=$row[0];
					($row[1]==NULL) ? $id_profesion=6 : $id_profesion=$row[1];
					$condicion=$row[2];
					$nombresTrabajador=$row[3];
					$apellidosTrabajador=$row[4];
					$fechaNacimiento=$row[5];
					$dni=$row[6];
					$telefono=$row[7];
					$publicado=$row[8];
					($row[9]=='ACTIVO') ? $ind_activo=1 : $ind_activo=0;
					$passwd=$row[10];
					// INICIO JGH 08/09/2016
                    $codDependenciaMesa=$row[12];
					$depPublico=$row[13];
                    // FIN JGH 08/09/2016

					// Valida Password en BD SQL
					if($passwd != md5("$username"."$userpass"))
					{
		$_REQUEST['mensaje_error']="<CENTER>Usuario o contrase�a incorrecta, favor intente nuevamente".
					   " o p�ngase en contacto con el <a href=\"mailto:intranet@midis.gob.pe?subject=Problema de usuario - contrase�a\">administrador</a> .</CENTER>";
						return 0;
					}else{
					
						// Crea Session ID Aleatorio e Inicia la Session
		// session_name($SESSION_NAME);
		srand((double)microtime()*1000000);
		$session_id = md5(uniqid(rand()));
		session_id($session_id);
		session_start();
		$_SESSION['secure_pas']=1;
		$_SESSION['cod_usuario']=$username;
		// INICIO JGH 08/09/2016
        $_SESSION['cod_dep_mesa']=$codDependenciaMesa;
		$_SESSION['dep_publico']=$depPublico;
        // FIN JGH 08/09/2016
		$_SESSION['confidencial']=$row[11];
		
		// Setea el Tiempo de Inicio de la sesion
		list($usec, $sec) = explode(" ",microtime()); 
		$_SESSION['time_start'] = (float)$usec + (float)$sec; 
		
		// Asigna valores Monetarios
		asignaValoresMonetarios();
		
	
		// Se agrega el resultado en variables de SESSION
/*		$_SESSION['dependencia']= $dependencia;
		$_SESSION['id_profesion']= $id_profesion;
		$_SESSION['id_condicion']= $id_condicion;
		$_SESSION['nombresTrabajador']= $nombresTrabajador;
		$_SESSION['apellidosTrabajador']= $apellidosTrabajador;
		$_SESSION['fechaNacimiento']= $fechaNacimiento;
		$_SESSION['dni']= $dni;
		$_SESSION['telefono']= $telefono;
		$_SESSION['publicado']= $publicado;
		$_SESSION['ind_activo']= $ind_activo;
*/
		// Todas las taras llevadas con exito
		return 1;
					}

					// si el usuario no esta activo en la Base de Datos, no inicia sesion
					if($ind_activo == 0)
					{
	                                        $_REQUEST['mensaje_error']="<CENTER>Hubo un problema al iniciar su sesi�n de Usuario, favor intente nuevamente".
        	                                   " o p�ngase en contacto con el <a href=\"mailto:intranet@midis.gob.pe?subject=Problema de session (2)\">administador</a>.</CENTER>";	
						return 0;	
					}

					unset($row);
				}
				else 
				{
					// si el usuario no existe en la Base de Datos, no inicia sesion
			                $_REQUEST['mensaje_error']="<CENTER>Hubo un problema al iniciar su sesi�n de Usuario, favor intente nuevamente".
                                           " o p�ngase en contacto con el <a href=\"mailto:intranet@midis.gob.pe?subject=Problema de sesion (1)\">administador</a>.</CENTER>";
					return 0;
				}
				$rs->Close();
			}
			$conn_mssql->Close();
		}
		unset($conn_mssql);
		
		// Actualizacion de la Tabla Usuario

		//[[SCRIPTCORTADO_00001]]
		

		// Inicio de la Sesion de Usuario
		
		
	//}
}
	
// Asignar valores monetarios

function asignaValoresMonetarios(){
	
	if (!($link=mssql_connect(CJDBMSSQL,CJDBMSSQLUSER,CJDBMSSQLPASS))) 
   	{ 
      echo "Error conectando a la base de datos."; 
      exit(); 
   } 
   if (!mssql_select_db("DB_GENERAL",$link)) 
   { 
      echo "Error seleccionando la base de datos."; 
      exit(); 
   }  		
                // bvalor fob
                //$SQL= "SELECT preciopromedio FROM DB_GENERAL.DBO.VALOR_FOB WHERE ID_ANYO=YEAR(GETDATE()) AND ID_MES=MONTH(GETDATE())-2 ";
				//$SQL= "select PRECIOPROMEDIO from db_general.dbo.valor_fob WHERE ID IN (SELECT MAX(ID) FROM DB_GENERAL.DBO.VALOR_FOB)";
//                $rs = mssql_query($SQL,$link); 
//                $row3=mssql_fetch_array($rs);
//                $_SESSION['fob'] = number_format($row3[0],3,'.',' ') ;
//                mssql_free_result($rs);       
//                unset($rs); 
//
//                // valor fob
//                $SQL= "select cantidad from db_general.dbo.uit where a�o=year(getdate())";
//                $rs = mssql_query($SQL,$link); 
//                $row3=mssql_fetch_array($rs);
//                $_SESSION['uit'] = number_format($row3[0],0,'.',' ') ;
//                mssql_free_result($rs);       
//                unset($rs); 
//
//                // tasa diaria
//                $SQL= "select tasa_diaria from db_sancion.dbo.tasa_diaria_legal where fecha_tasa=convert(varchar(10),DATEADD(day, -1, getdate()),103)";
//                $rs = mssql_query($SQL,$link); 
//                $row3=mssql_fetch_array($rs);
//                $_SESSION['tasaDiaria'] = number_format($row3[0],4,'.',' ') ;
//                mssql_free_result($rs);       
//                unset($rs); 
//				
//                // tasa diaria
//                $SQL= "select MN_FACTOR_ACUMUL from db_sancion.dbo.factor_acumulado WHERE fecha=convert(varchar(10),DATEADD(day, -1, getdate()),103)";
//                $rs = mssql_query($SQL,$link); 
//                $row3=mssql_fetch_array($rs);
//                $_SESSION['factorAcumulado'] = number_format($row3[0],4,'.',' ') ;
//                mssql_free_result($rs);       
//                unset($rs);
				
				//$hoy=date("d/m/Y");					
//					$SQL="select compra_tcdappd,venta_tcdappd,convert(varchar,fech_tcdappd,103)
//								from db_dnepp.user_dnepp.tipo_cambio_dappd 
//								where 
//								fech_tcdappd in (select max(fech_tcdappd) 
//								   	  		  from db_dnepp.user_dnepp.tipo_cambio_dappd
//									  		  where fech_tcdappd<=convert(datetime,'".$hoy."',103)
//									  )";
//				$rs = mssql_query($SQL,$link); 
//                $row3=mssql_fetch_array($rs);
//                $_SESSION['dolarCompra'] = number_format($row3[0],3) ;
//				$_SESSION['dolarVenta'] = number_format($row3[1],3) ;
//                mssql_free_result($rs);       
//                unset($rs);					   
//         
		        mssql_close($link); 
                unset($link);
}

?>