<!doctype html>
<html>
<head>
<meta>
<title>HOJA DE TRAMITE</title>
{literal}
<style type="text/css">
body{margin:0; text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;}

.detalle{ border-collapse:collapse; }
/*#acciones tr td, .detalle tr td,.detalle tr th{border:solid 1px #000000; padding:5px;}*/
.detalle tr th{border:solid 1px #000000; padding:5px;}
#acciones tr td{padding:2px 5px;}
.detalle tr td{border:solid 1px #000000;}
.detalle tr th{ background:#CCC;}
.detalle tr td div{ display:inline-block; width:100%;height:60px;/*overflow-y:auto;*/ overflow:hidden}
#acciones, #cabecera{ text-align:left;}
h1{ font-size:32px;}
.bold{ font-weight:bold;}
#logo{ text-align:left;}
/*.detalle tbody tr td{ height:70px;}*/
.sb{ border-style:none;}
.fechaGen{ color:#F00; font-style:italic; font-weight:bold; text-align:right; padding:10px 5px;}
.left{ text-align:left;}
@media print {
	 @page { margin: 0; }
	 .cabecera{ margin-top:20px;}
   #menu{display:none;}
	th{ background-color: #CCCCCC !important;
		-webkit-print-color-adjust: exact; }
}
</style>
{/literal}
</head>
<body>
{php}
if(isset($_GET['cantidad'])){$cantidad=$_GET['cantidad'];}
else{$cantidad=1;}
for($u=1;$u <= $cantidad;$u++){
{/php}
<div id="menu"><strong><a href="#" onClick="window.print();"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></div>
{php}
$info=array();
$tabla = $this->get_template_vars(Exp); 
foreach($tabla as $data){
	$info[]=$data;
	if(count($data['detalleMovimiento'])>0){
		foreach($data['detalleMovimiento'] as $datat){
			//$info[]=$datat;
		}
	}
}
$max=7;
	$cuadros_faltantes=(ceil((count($info))/$max) * $max)-(count($info));
	for($i=1;$i <=$cuadros_faltantes;$i++){
    	$info[]=array('tabla'=>'M');
    }
$num_filas=count($info);
$fila=0;
$contador_fila=0;
$max_paginas=ceil($num_filas/$max);
$numdepage=0;
$agrupados=array();
$repetidos='';
foreach($info as $reg){
$contador_fila++;
if( $fila % $max==0){
$fila=0;
$numdepage++;
{/php}
<div style="page-break-after: always; width:700px; margin:auto;">
<table width="100%" class="cabecera">
	<tr>
    	<td>
        <div id="logo"><img src="/img/logo-midis.png" alt="" /></div>
        <div><h1>HOJA DE TRAMITE</h1></div>
        </td>
    	<td valign="top">
         <table class="detalle" width="200px">
            <thead><tr><th>EXPEDIENTE</th></tr></thead>
            <tbody>
                <tr><td>{if $idexpediente >0}{$expediente}{else}&nbsp;{/if}</td></tr>
                <tr><td>ESTADO : {$estadoDocP}</td></tr>
                <tr><td>PRIORIDAD : {$prioridad}</td></tr>
                <tr><td>DIAS : {$dias}</td></tr>
                <tr><td>CREADO POR : {$user}</td></tr>
            </tbody>
        </table>
        </td>
    </tr>
</table>
<table width="100%" id="cabecera">
<tr>
<td class="bold" width="120"><span class="bold1">INGRESO</span></td>
<td width="350">{$fecRec|default:'NO ESPECIFICADO'}</td>
<td class="bold" width="100">&nbsp;</td><td width="130">&nbsp;</td>
</tr>
<tr>
<td class="bold" width="120"><span class="bold1">REMITENTE</span></td>
<td width="350">{$remitente}</td>
<td class="bold" width="100">&nbsp;</td><td width="130">&nbsp;</td>
</tr>
<tr>
<td class="bold">DESCRIPCION</td>
<td colspan="3">{$asunto}</td>
</tr>
<tr>
</tr>
</table>
<table width="100%" class="detalle">
	<thead><tr>
		<th>ENVIADO POR</th>
		<th>REMITIDO A</th>
		<th>FECHA DERIVACION</th>
		<th>FECHA RECEPCION</th>
        <th>ACCIONES</th>
		<th>DOCUMENTO ADJUNTO Y/O OBSERVACIONES</th>
        <th width="95">V&deg;B&deg;</th>
     </tr></thead>
{php}
}
if($reg['tabla']=='M'){
 echo '<tr>';
 echo '<td class="textoblack" align="center"><div>'.$reg['depo'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$reg['depd'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$reg['fDer'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$reg['fecRec'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$reg['acciones'].'</div></td>';
 echo '<td class="textoblack" align="left"><div>';
 	if(trim($reg['cla'])!='' && trim($reg['ind'])!=''){
    	echo $reg['cla'].' '.$reg['ind'].'<br>';
    }
 echo $reg['obs'];
 echo '</div></td>';
  echo '<td class="textoblack"> </td>';
echo '</tr>';
}
if($reg['tabla']=='T'){
 echo '<tr>';
    echo'<td class="textoblue" align="center"><div>'.$reg['depd'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$reg['trabajador1'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$reg['feccre'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$reg['fecrec'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$reg['acciones'].'</div></td>';
    $estadoTrab='';
    if($reg['estado']==0){$estadoTrab='<font color="#FF0000"><strong>PENDIENTE</strong></font>';}
    if($reg['estado']==1){$estadoTrab='<font color="#FF0000"><strong>PENDIENTE</strong></font>';}
    if($reg['estado']==2){$estadoTrab='<font color="#000066"><strong>DERIVADO</strong></font>';}
    if($reg['estado']==3){$estadoTrab='<font color="#000066"><strong>FINALIZADO</strong></font><br>'.$MovTrab['fecfin'].'<br>'.$reg['motfin'];}
    if($reg['estado']==4){$estadoTrab='<font color="#000066"><strong>ENVIADO A OTRO TRABAJADOR</strong></font>';}
    if($reg['estado']==5){$estadoTrab='<font color="#000066"><strong>DERIVADO</strong></font>';}
    if($reg['estado']==6){$estadoTrab='<font color="#000066"><strong>REASIGNADO A OTRO TRABAJADOR</strong></font>';}
 	if($reg['documento']!=''){echo'<td class="textoblue" align="left"><div>'.$reg['documento'].'<br>'.$estadoTrab.'</div></td>';}else{echo'<td class="textoblue" align="left"><div>'.$estadoTrab.'</div></td>';}
    echo'<td class="textoblue" align="left"><div>'.$reg['oficio'];
    if($reg['asunto_']!=''){echo '<br><strong>Asunto:</strong><br>'.$reg['asunto_'].'<br>';}
    if($reg['obs_']!=''){echo '<strong>Observaciones:</strong><br>'.$reg['obs_'];}
    echo '</div></td>';
    echo'<td class="textoblue"></td>';
 echo '</tr>';
}
if($reg['agrupa']>0){
	if(trim($reg['docAgrupados'])!=''){
    	if($repetidos!=$reg['cla'].' '.$reg['ind'])
			$agrupados[]=array($reg['cla'].' '.$reg['ind'],$reg['docAgrupados']);
        $repetidos=$reg['cla'].' '.$reg['ind'];
    }
}
if( ($fila==($max -1)) || $contador_fila==$num_filas){
echo '</table>';
if(count($agrupados)>0){
//foreach($agrupados as $listaagrupados)
	//echo '<div class="left"><strong>'.$listaagrupados[0].' : </strong>'.$listaagrupados[1].'</div>';
}
$agrupados=array();
$repetidos='';
{/php}
{if $expedientesAnexos !=''}<div style=" color:#F00; font-size:12px; font-style:oblique; text-align:left;">{$expedientesAnexos}</div>{/if}
<table width="100%">
<tr>
<td align="left"><br /><span class="bold">ACCIONES</span>
<table width="100%" id="acciones">
    	<tr><td>1. Tr&aacute;mite</td><td>7. Gest V&deg;B&deg y/o Firma</td><td>13. Estudio Y/o An&aacute;lisis</td><td>19. No Conpetencia</td></tr><tr><td>2. Adjuntar Antecedent</td><td>8. Opinar E Informar</td><td>14. Conocimiento Y Fines</td><td>20. Otros</td></tr><tr><td>3. Agregar El Expediente</td><td>9. Notificar Al Interesado</td><td>15. Recomendaci&oacute;n</td><td>21. Atender Y Responder</td></tr><tr><td>4. Archivar</td><td>10. Por Corresponderle</td><td>16. Ejecuci&oacute;n</td><td>22. Evaluar E Informar</td></tr><tr><td>5. Devolver Al Interesado</td><td>11. Preparar Respuesta</td><td>17. Revisar Y Coordinar</td><td>23. Coordinar</td></tr><tr><td>6. Muy Urgente</td><td>12. Proyectar Resoluci&oacute;n</td><td>18. Seguimiento</td>
        </tr></table>
  </td>
</tr>
</table>
<div class="bold left">OBSERVACIONES</div>
<div class="left">{$observaciones}</div>
<div class="fechaGen">Impreso al {$fechaGen}<br></div>
</div>
{php}}$fila++;}{/php}
</body>
</html>
{php}
}
{/php}
{php}
if($_GET[autoprint]==1){
	echo "<script>window.print();</script>";
}
{/php}