<!doctype html>
<html>
<head>
<meta>
<title>Hoja de Ruta - {if $idTipoDoc!=4}{$nroExp}{else}{$indicativo}{/if}</title>
{literal}
<style type="text/css">
body{margin:0; text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;}
div{ width:700px; margin:auto;}
#acciones,#detalle{ border-collapse:collapse; }
/*#acciones tr td, #detalle tr td,#detalle tr th{border:solid 1px #000000; padding:5px;}*/
#acciones tr td,#detalle tr th{border:solid 1px #000000; padding:5px;}
#detalle tr td{border:solid 1px #000000;}
#detalle tr th{ background:#CCC;}
#detalle tr td div{ display:inline-block; width:100%;height:60px;overflow-y:auto;}
#acciones, #cabecera{ text-align:left;}
h1{ font-size:32px;}
.bold{ font-weight:bold;}
#logo{ text-align:left;}
/*#detalle tbody tr td{ height:70px;}*/
.sb{ border-style:none;}
.fechaGen{ color:#F00; font-style:italic; font-weight:bold; text-align:right; padding:10px 5px;}
@media print {
   #menu{display:none;}
	th{ background-color: #CCCCCC !important;
		-webkit-print-color-adjust: exact; }
}
</style>
{/literal}
</head>
<body>
<div id="menu"><strong><a href="#" onClick="window.print();"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></div>
{php}
$fila=1;
while($fila < 10){
 if($fila%2 ==0){
 echo '<div style="page-break-after: always;">Contenido del div...</div>';
 }
{/php}
<div id="logo"><img src="/img/logo-midis.png" alt="" /></div>
<div>
  <h1>HOJA DE TRAMITE</h1></div>
<div>
<table width="100%" id="cabecera">
<tr>
<td class="bold" width="120"><span class="bold1">INGRESO</span></td>
<td width="350">{$fecRec|default:'NO ESPECIFICADO'}</td>
<td class="bold" width="100">&nbsp;</td><td width="130">&nbsp;</td>
</tr>
<tr>
<td class="bold" width="120"><span class="bold1">REMITENTE</span></td>
<td width="350">{$remitente}</td>
<td class="bold" width="100">&nbsp;</td><td width="130">&nbsp;</td>
</tr>
{if $idTipoDoc!=4}
<tr>
<td class="bold" width="120">N&deg; Registro</td><td width="350" style="font-size:12px;"><strong>{$nroExp}</strong></td><td class="bold" width="100">Fecha de<br>Ingreso</td><td width="130">{$fecRec|default:'NO ESPECIFICADO'}</td>
</tr>
<tr>
<td class="bold" width="120">Documento</td>
<td width="350">{$indicativo}</td>
<td class="bold" width="100">&nbsp;</td><td width="130">&nbsp;</td>
</tr>
{else}
<tr>
<td class="bold" width="120">Documento</td><td width="350">{$indicativo}</td><td class="bold" width="100">Fecha de<br>Ingreso</td><td width="130">{$fecRec|default:'NO ESPECIFICADO'}</td>
</tr>
{/if}
<tr>
<td class="bold">Raz&oacute;n Social</td><td colspan="3">{$RazonSocial}</td>
</tr>
<tr>
<td class="bold">ASUNTO</td>
<td colspan="3">{$asunto}</td>
</tr>
<tr>
<td class="bold">DESCRIPCION</td>
<td colspan="3">{$observaciones}</td>
</tr>
<tr>
</tr>
</table>
<table width="100%" id="detalle">
	<thead><tr>
		<th class="plomo">ENVIADO POR</th>
		<th class="plomo">REMITIDO A</th>
		<th class="plomo">FECHA DERIVACION</th>
		<th class="plomo">FECHA RECEPCION</th>
        <th class="plomo">ACCONES</th>
		<th class="plomo">DOCUMENTO ADJUNTO Y/O OBSERVACIONES</th>
		<th class="plomo">AVANCE</th>
        <th class="plomo" width="95">V&deg;B&deg;</th>
     </tr></thead>
{php}
$flujo = $this->get_template_vars(Exp); 

foreach($flujo as $mov){
$fila++;
if($mov['mostrar']==1){
 echo '<tr>';
 echo '<td class="textoblack" align="center"><div>'.$mov['depo'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$mov['depd'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$mov['fDer'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$mov['fecRec'].'</div></td>';
 echo '<td class="textoblack" align="center"><div>'.$mov['acciones'].'</div></td>';
 echo '<td class="textoblack" align="left"><div>'.$mov['cla'].' '.$mov['ind'];
	if ($mov['valRuta']==1){ echo'<a href="'.$mov['rutaI'].'" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>';}
	if($mov['sen_pendiente']!=""){echo'<b>';
                        if ($mov['agrupa']>0){
                    		echo '<br />'.$mov['agrupa'];
                        }
                    	if ($mov['sen_pendiente']=="PENDIENTE"){
                    	echo '<font color="#FF0000"><br />'.$mov['sen_pendiente'].'</font></b>';
                        }else{
                        	if($mov['sen_pendiente']=="FINALIZADO"){
                            echo '<font color="#000066"><br />'.$mov['sen_pendiente'].'</b><br>'.$mov['fecFin'].'<br />'.$mov['modFin'].'</font>';
                            }
                            else{echo '<font color="#000066"><br />'.$mov['sen_pendiente'].'</font></b>';}
                        }
    }
 echo '</div></td>';
 echo '<td class="textoblack"><div>'.$mov['avance'].'</div></td>';
  echo '<td class="textoblack"> </td>';
echo '</tr>';
 if(count($mov['detalleMovimiento'])>0){
 foreach($mov['detalleMovimiento'] as $MovTrab){
 	$fila++;
 echo '<tr>';
    echo'<td class="textoblue" align="center"><div>'.$mov['depd'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$MovTrab['trabajador1'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$MovTrab['feccre'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$MovTrab['fecrec'].'</div></td>';
    echo'<td class="textoblue" align="center"><div>'.$MovTrab['acciones'].'</div></td>';
    $estadoTrab='';
    if($MovTrab['estado']==0){$estadoTrab='<font color="#FF0000"><strong>PENDIENTE</strong></font>';}
    if($MovTrab['estado']==1){$estadoTrab='<font color="#FF0000"><strong>PENDIENTE</strong></font>';}
    if($MovTrab['estado']==2){$estadoTrab='<font color="#000066"><strong>DERIVADO</strong></font>';}
    if($MovTrab['estado']==3){$estadoTrab='<font color="#000066"><strong>FINALIZADO</strong></font><br>'.$MovTrab['fecfin'].'<br>'.$MovTrab['motfin'];}
    if($MovTrab['estado']==4){$estadoTrab='<font color="#000066"><strong>ENVIADO A OTRO TRABAJADOR</strong></font>';}
    if($MovTrab['estado']==5){$estadoTrab='<font color="#000066"><strong>DERIVADO</strong></font>';}
    if($MovTrab['estado']==6){$estadoTrab='<font color="#000066"><strong>REASIGNADO A OTRO TRABAJADOR</strong></font>';}
 	if($MovTrab['documento']!=''){echo'<td class="textoblue" align="left"><div>'.$MovTrab['documento'].'<br>'.$estadoTrab.'</div></td>';}else{echo'<td class="textoblue" align="left"><div>'.$estadoTrab.'</div></td>';}
    echo'<td class="textoblue" align="left"><div>'.$MovTrab['oficio'];
    if($MovTrab['asunto_']!=''){echo '<br><strong>Asunto:</strong><br>'.$MovTrab['asunto_'].'<br>';}
    if($MovTrab['obs_']!=''){echo '<strong>Observaciones:</strong><br>'.$MovTrab['obs_'];}
    echo '</div></td>';
    echo'<td class="textoblue"></td>';
 echo '</tr>';
 }}
}
}

{/php}
</table>
<table>
<tr><td>
{php}
foreach($flujo as $Mov){
	if($Mov['agrupa']>0)echo $Mov['agrupa'].' - '.$Mov['docAgrupados'].'<br />';
}
{/php}
</td></tr>
</table>
<table width="100%">
<tr>
<td align="left"><span class="bold">ACCIONES</span>
  <table width="100%" id="acciones">
    <tr>
      <td>1</td>
      <td>TR&Aacute;MITE</td>
      <td>7</td>
      <td>POR CORRESPONDERLE</td>
      <td>13</td>
      <td>AGREGAR AL EXPEDIENTE</td>
    </tr>
    <tr>
      <td>2</td>
      <td>ARCHIVAR</td>
      <td>8</td>
      <td>DIFUSI&Oacute;N</td>
      <td>14</td>
      <td>SEGUIMIENTO</td>
    </tr>
    <tr>
      <td>3</td>
      <td>REVISAR</td>
      <td>9</td>
      <td>PREPARAR RESPUESTA</td>
      <td>15</td>
      <td>RECOMENDACI&Oacute;N</td>
    </tr>
    <tr>
      <td>4</td>
      <td>REFRENDO Y VISACI&Oacute;N</td>
      <td>10</td>
      <td>PROYECTAR RESOLUCI&Oacute;N</td>
      <td>16</td>
      <td>RESPONDE DIRECTAMENTE</td>
    </tr>
    <tr>
      <td>5</td>
      <td>OPINAR</td>
      <td>11</td>
      <td>CONOCIMIENTO</td>
      <td>17</td>
      <td>VERIFICAR</td>
    </tr>
    <tr>
      <td>6</td>
      <td>INFORMAR</td>
      <td>12</td>
      <td>COORDINAR</td>
      <td class="sb">18</td>
      <td class="sb">OTROS</td>
    </tr>
  </table></td>
</tr>
</table>
</div>
<div class="fechaGen">Impreso al {$fechaGen}<br></div>
{php}}{/php}
</body>
</html>