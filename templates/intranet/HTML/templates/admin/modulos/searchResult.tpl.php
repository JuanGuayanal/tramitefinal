<form name="{$frmName}" action="{$frmUrl}" method="post">
{foreach key=name item=value from=$datos}
  <input type="hidden" name="{$name}" value="{$value}">
{/foreach}
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_PARTE_EXTENDIDA}">
</form>
<table width="540" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">

  <tr>
    <td align="right" class="textogray"><strong>Resultados:</strong>&nbsp; {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
  </tr>
  {section name=i loop=$modulo}
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td rowspan="4" valign="top" style="padding-left: {math equation="x*y" x=$modulo[i].prof y=15};"><img src="/img/800x600/ico-grupo.gif" width="17" height="16" hspace="3"></td>
            <td colspan="2" width="523" class="item" height="16"><b><a href="{$frmUrl}?id={$modulo[i].id}&accion={$accion.FRM_MODIFICA_MODULO}&amp;menu={$accion.FRM_MODIFICA_MODULO}">{$modulo[i].name}</a></b></td>
        </tr>
        <tr>
          <td class="sub-item" width="60" valign="top">Usuarios:</td>
          <td width="463" class="texto">
			{section name=u loop=$modulo[i].usrs}
			  {if $modulo[i].stat=='Activo'}
		    <!-- <a href="{$frmDerUrl}?idUsr={$modulo[i].usrs[u].user}&amp;idMod={$modulo[i].id}&amp;idModTip={$modulo[i].tMod}&amp;idIntTip=1&accion={$accionDer}">{$modulo[i].usrs[u].user} {$modulo[i].usrs[u].read}{$modulo[i].usrs[u].inse}{$modulo[i].usrs[u].modi}</a> -->
			 <a href="{$frmDerUrl}?idusr={$modulo[i].usrs[u].user}&amp;id_modulo={$modulo[i].id}&amp;id_modulotipo={$modulo[i].tMod}&amp;tip_integrante=1&accion={$accionDer}">{$modulo[i].usrs[u].user} {$modulo[i].usrs[u].read}{$modulo[i].usrs[u].inse}{$modulo[i].usrs[u].modi}</a>
		      {if not $smarty.section.u.last}&nbsp;-&nbsp;{/if}
   			  {elseif $modulo[i].stat=='Inactivo'}
				{$modulo[i].usrs[u].user} {$modulo[i].usrs[u].read}{$modulo[i].usrs[u].inse}{$modulo[i].usrs[u].modi}
              {/if}
			{sectionelse}
			  Ninguno
			{/section}
		  </td>
        </tr>
        <tr>
          <td class="sub-item">Grupos:</td>
          <td class="textored">
			{section name=g loop=$modulo[i].grps}
			  {if $modulo[i].stat=='Activo'}
		    <!-- <a href="{$frmGrpUrl}?idGrp={$modulo[i].grps[g].igrp}&amp;idMod={$modulo[i].id}&amp;idModTip={$modulo[i].grps[g].tMod}&amp;idIntTip=2&accion={$accion.FRM_GRUPO_DERECHOS}">{$modulo[i].grps[g].cgrp} {$modulo[i].grps[g].read}{$modulo[i].grps[g].inse}{$modulo[i].grps[g].modi}</a> -->
			 <a href="{$frmDerUrl}?idgrp={$modulo[i].grps[g].igrp}&amp;id_modulo={$modulo[i].id}&amp;id_modulotipo={$modulo[i].tMod}&amp;tip_integrante=2&accion={$accionDer}">{$modulo[i].grps[g].cgrp} {$modulo[i].grps[g].read}{$modulo[i].grps[g].inse}{$modulo[i].grps[g].modi}</a>
		      {elseif $modulo[i].stat=='Inactivo'}
				{$modulo[i].grps[g].cgrp} {$modulo[i].grps[g].read}{$modulo[i].grps[g].inse}{$modulo[i].grps[g].modi}
              {/if}
			  {if not $smarty.section.g.last}&nbsp;-&nbsp;{/if}
			{sectionelse}
			  Ninguno
			{/section}
		  </td>
        </tr>
        <tr>
          <td class="sub-item">Estado:</td>
          <td class="textored">{$modulo[i].stat}</td>
        </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
  {/section}
</table>
{if $menuSearchPaginable}
<table width="540" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
	<td><hr width="100%" size="1"></td>
  </tr>
  <tr>
	<td align="right" class="textogray">{$menuSearchPaginable}</td>
  </tr>
  <tr>
	<td><hr width="100%" size="1"></td>
  </tr>
</table>
{/if}
<br>