{$jscript}
<form action="{$frmUrl}" method="post" name="{$frmName}" id="{$frmName}">
  <table width="540" border="0" cellspacing="0" cellpadding="2">
    {if $errors}
    <tr bgcolor="#FFFFFF">
      <td class="item-sep">{$errors}</td>
    </tr>
	{/if}
    <tr bgcolor="#FFFFFF">
      <td class="item-sep"><strong><a name="disp" id="disp"></a>DEL MODULO</strong></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="item"><b>T&iacute;tulo Resumido</b></td>
            <td width="440"><input name="desTitCor" type="text" class="iptxt1" id="desTitCor" value="{$mod.tCor}" size="30" maxlength="50" /></td>
          </tr>
          <tr>
            <td class="item">T&iacute;tulo Completo</td>
            <td><input name="desTitLar" type="text" class="iptxt1" id="desTitLar" value="{$mod.tLar}" size="30" maxlength="150" /></td>
          </tr>
          <tr>
            <td class="item"><b>Tipo</b></td>
            <td><select name="idModTipo" id="idModTipo" class="ipsel2" onchange="selItems(document.forms['{$frmName}'].elements['users[]']);submitForm('{if $mod.id}{$accion.FRM_MODIFICA_MODULO}{else}{$accion.FRM_AGREGA_MODULO}{/if}')">
			{$selModTipo}
              </select>
            </td>
          </tr>
          <tr>
            <td class="item"><b>M&oacute;dulo Padre</b></td>
            <td><select name="idModPadre" id="idModPadre" class="ipsel2">
			{$selModPadre}            
            </select>
            </td>
          </tr>
          <tr>
            <td class="item">Descripci&oacute;n</td>
            <td width="440"><textarea name="desModulo" cols="60" rows="5" class="iptxt1" id="desModulo">{$mod.desc}</textarea></td>
          </tr>
          <tr>
            <td class="item">Imagen</td>
            <td><input name="desImagen" type="text" class="iptxt1" id="desImagen" value="{$mod.img}" size="30" maxlength="50" /></td>
          </tr>
          <tr>
            <td class="item">&Iacute;cono</td>
            <td><input name="desIcono" type="text" class="iptxt1" id="desIcono" value="{$mod.ico}" size="30" maxlength="50" /></td>
          </tr>
          <tr>
            <td class="item">Num. Orden</td>
            <td><input name="numOrden" type="text" class="iptxt1" id="numOrden" value="{$mod.nOrd}" size="4" maxlength="4" /></td>
          </tr>
          <tr>
            <td class="item"><strong>Activo</strong></td>
            <td><table width="100%%" border="0" cellspacing="0" cellpadding="0" class="TEmb">
                <tr>
                  <td valign="middle" nowrap="nowrap" class="texto"><input name="indActivo" type="radio" value="t"{if $mod.iAct!='f'} checked="checked"{/if} />
        Si</td>
                  <td width="100%" valign="middle" nowrap="nowrap" class="texto"><input name="indActivo" type="radio" value="f"{if $mod.iAct=='f'} checked="checked"{/if} />
        No </td>
                </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td class="item-sep"><strong><a name="disp" id="disp"></a>DE LA UBICACI&Oacute;N (URL) </strong></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="item"><b>Path Absoluto</b></td>
            <td><input name="desPath" type="text" class="iptxt1" id="desPath" value="{$mod.path}" size="30" maxlength="250" />
            </td>
          </tr>
          <tr>
            <td class="item"><strong>Target</strong></td>
            <td><select name="idTarget" id="idTarget" class="ipsel2">{$selTarget}</select>
            </td>
          </tr>
          <tr>
            <td class="item">Atributos Link</td>
            <td width="0"><input name="desAtribTarget" type="text" class="iptxt1" id="desAtribTarget" value="{$mod.aTar}" size="60" maxlength="250" /></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td width="540">&nbsp;</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td class="item-sep"><strong><a name="disp" id="disp"></a>DE LOS USUARIOS CON DERECHO </strong></td>
    </tr>
    <tr>
      <td><table width="100%"  border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="item"><b>Dependencia</b></td>
		  <td valign="middle">
			<select name="idDep" class="ipsel2" onchange="muestraSubdependencias(this);selItems(document.forms['{$frmName}'].elements['users[]']);submitForm('{if $mod.id}{$accion.FRM_MODIFICA_MODULO}{else}{$accion.FRM_AGREGA_MODULO}{/if}')">
			  {$selDep.OBJECT}
			</select>
		{$selDep.JSCRIPT} </td>
        </tr>
        <tr>
          <td class="item"><b>Sub Dependencia</b></td>
		  <td>
			<select name="idSDep" class="ipsel2" onchange="selItems(document.forms['{$frmName}'].elements['users[]']);submitForm('{if $mod.id}{$accion.FRM_MODIFICA_MODULO}{else}{$accion.FRM_AGREGA_MODULO}{/if}')">
			  <option>Ninguna en Particular</option>
			</select>
		{$jscriptSubDep} </td>
        </tr>
      </table>
	  </td>
    </tr>
    <tr align="center">
      <td width="540"> {$objItemsChange} </td>
    </tr>
    <tr>
      <td class="item" width="540">&nbsp;</td>
    </tr>
    {if $mod.id}
    <tr bgcolor="#FFFFFF">
      <td class="item-sep"><strong><a name="disp" id="disp"></a>DE LA MODIFICACION </strong></td>
    </tr>
    <tr>
      <td class="item" width="540"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="item"><b>Usuario</b></td>
            <td class="item">{$mod.uMod}
            </td>
          </tr>
          <tr>
            <td class="item"><b>Creaci&oacute;n</b></td>
            <td class="item">{$mod.fCre}</td>
          </tr>
          <tr>
            <td class="item"><b> Modificaci&oacute;n</b></td>
            <td class="item">{$mod.fMod} </td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td class="item" width="540">&nbsp;</td>
    </tr>
    {/if}
    <tr>
      <td class="item" align="center">
	  {if $mod.id}<input type="hidden" name="id" id="id" value="{$mod.id}" />{/if}
	  <input type="submit" value="{if not $mod.id}Agregar Modulo{else}Modificar Modulo{/if}" class="but-login" onclick="selItems(document.forms['{$frmName}'].elements['users[]']);MM_validateForm('des_titulocorto','','R','ord_modulo','','N','des_path','','R');return document.MM_returnValue" />
&nbsp;&nbsp;&nbsp;<input type="reset" value="BORRAR DATOS" class="but-login" />
  <input type="hidden" name="accion" value="{if not $mod.id}{$accion.AGREGA_MODULO}{else}{$accion.MODIFICA_MODULO}{/if}" />
  <input type="hidden" name="menu" value="{if not $mod.id}{$accion.FRM_AGREGA_MODULO}{else}{$accion.FRM_MODIFICA_MODULO}{/if}" />
  <input type="hidden" name="reLoad" value="1" />
      </td>
    </tr>
  </table>
</form>