<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>TIPO DE PRIORIDAD PARA CORRESPONDENCIA</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=insertaPrioridadCorr">
        <p> 
			<label for="password">Prioridad</label>
			<input name="prioridad" required="required" type="text" class="std_input" />
		</p>
        <p>
          <button class="std_button" id="btn_CambiarContrasena" rel="titulo">Agregar Prioridad</button></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=actualizarPrioridadCorr">
<table class="std_grilla">
	<tr><th>N&deg;</th>
	<th>Prioridad</th><th colspan="2">Activo</th></tr>
{section name="i" loop=$prioridad}
<tr>
	<td>{$smarty.section.i.iteration}<input type="hidden" name="arreglo[id_prioridad][]" value="{$prioridad[i].id_prioridad}" /></td>
    <td><input type="text" value="{$prioridad[i].prioridad}" name="arreglo[prioridad][]"/></td>
    <td>{if $prioridad[i].activo ==1}<img src="/img/800x600/activo.png" />{else}<img src="/img/800x600/inactivo.png" />{/if}</td>
    <td><select class="sel_activar" name="arreglo[activo][]">
    <option value="1" {if $prioridad[i].activo ==1}selected="selected"{/if}>ACTIVO</option>
    <option value="0" {if $prioridad[i].activo ==0}selected="selected"{/if}>INACTIVO</option></select>
    </td>
    
</tr>
{/section}
</table>
<div class="center"><p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Guardar Cambios</button></p></div>
</form>
</div>
<script src="/sitradocV3/js/src/correspondencia/configuracion/prioridad.js"></script>