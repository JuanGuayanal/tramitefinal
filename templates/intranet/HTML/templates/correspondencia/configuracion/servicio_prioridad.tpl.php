<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>SERVICIO - PRIORIDAD PARA CORRESPONDENCIA</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=insertaServicioPriCorr">
        <p> 
			<label for="">Descripci&oacute;n</label>
			<input name="descripcion" required="required" type="text" class="std_input" />
		</p>
        <p> 
			<label>Servicio</label>
			<select name="servicio">{$selServicio}</select>
		</p>
        <p> 
			<label>Prioridad</label>
			<select name="prioridad">{$selPrioridad}</select>		</p>
        <p> 
			<label for="">Tiempo de entrega</label>
			<input name="entrega" required="required" type="text" class="std_input" />
		</p>
        <p> 
			<label for="">Tiempo de devoluci&oacute;n</label>
			<input name="devolucion" required="required" type="text" class="std_input" />
		</p>
        <p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Agregar Parametros</button></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=actualizarServicioPriCorr">
<table class="std_grilla">
	<tr><th>N&deg;</th><th>Servicio - Prioridad</th><th>Servicio</th><th>Prioridad</th><th>Tiempo de Entrega</th><th>Tiempo de Devoluci&oacute;n</th><th colspan="2">Activo</th></tr>
{section name="i" loop=$serv_pri} 
<tr>
	<td>{$smarty.section.i.iteration}<input type="hidden" name="arreglo[id_serv_pri][]" value="{$serv_pri[i].id_serv_pri}" /></td>
    <td><input type="text" value="{$serv_pri[i].descripcion}" name="arreglo[descripcion][]"/></td>
    <td>{$serv_pri[i].servicio}</td>
    <td>{$serv_pri[i].prioridad}</td>
    <td><input type="text" value="{$serv_pri[i].entrega}" name="arreglo[entrega][]"/></td>
    <td><input type="text" value="{$serv_pri[i].devolucion}" name="arreglo[devolucion][]"/></td>
    <td>{if $serv_pri[i].activo ==1}<img src="/img/800x600/activo.png" />{else}<img src="/img/800x600/inactivo.png" />{/if}</td>
    <td><select class="sel_activarsp" name="arreglo[activo][]">
    <option value="1" {if $serv_pri[i].activo ==1}selected="selected"{/if}>ACTIVO</option>
    <option value="0" {if $serv_pri[i].activo ==0}selected="selected"{/if}>INACTIVO</option></select>
    </td>
    
</tr>
{/section}
</table>
<div class="center"><p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Guardar Cambios</button></p></div>
</form>
</div>
<script src="/sitradocV3/js/src/correspondencia/configuracion/servicio.js"></script>