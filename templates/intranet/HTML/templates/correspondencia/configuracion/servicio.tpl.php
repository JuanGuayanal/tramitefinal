<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>TIPO DE SERVICIOS PARA CORRESPONDENCIA</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=insertaServicioCorr">
        <p> 
			<label for="password">Servicio</label>
			<input name="servicio" required="required" type="text" class="std_input" />
		</p>
        <p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Agregar Servicio</button></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=actualizarServicioCorr">
<table class="std_grilla">
	<tr><th>N&deg;</th><th>Servicio</th><th colspan="2">Activo</th></tr>
{section name="i" loop=$servicios}
<tr>
	<td>{$smarty.section.i.iteration}<input type="hidden" name="arreglo[id_servicio][]" value="{$servicios[i].id_servicio}" /></td>
    <td><input type="text" value="{$servicios[i].servicio}" name="arreglo[servicio][]"/></td>
    <td>{if $servicios[i].activo ==1}<img src="/img/800x600/activo.png" />{else}<img src="/img/800x600/inactivo.png" />{/if}</td>
    <td><select class="sel_activar" name="arreglo[activo][]">
    <option value="1" {if $servicios[i].activo ==1}selected="selected"{/if}>ACTIVO</option>
    <option value="0" {if $servicios[i].activo ==0}selected="selected"{/if}>INACTIVO</option></select>
    </td>
    
</tr>
{/section}
</table>
<div class="center"><p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Guardar Cambios</button></p></div>
</form>
</div>
<script src="/sitradocV3/js/src/correspondencia/configuracion/servicio.js"></script>