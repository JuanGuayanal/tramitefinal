{literal}
<script language="JavaScript1.2">
<!-- 
function MM_findObj(n, d) { //v4.0
  var p,i,x;
  if(!d)
    d=document;
  if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document;
	n=n.substring(0,p);
  }
  if(!(x=d[n])&&d.all)
	x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++)
    x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++)
    x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById)
    x=document.getElementById(n);
  return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) {
    test=args[i+2];
	if (test.indexOf('Date')!=-1 || test.indexOf('Hour')!=-1 || test.indexOf('Sel')!=-1 || test.indexOf('date')!=-1 || test.indexOf('hour')!=-1) {
	  var cbxs = new Array;
	  var vals = new Array;
	  cbxs = args[i].split(",");
	  for(j=0;j<cbxs.length;j++)
		vals[j]=MM_findObj(cbxs[j]);
	  for(j=0;j<vals.length;j++)
	    vals[j] = (vals[j].length > 0) ? vals[j][vals[j].selectedIndex].value : false;
	  val = true
	}else
	  val=MM_findObj(args[i]);
	
    if (val) {
	  nm=args[i+1];
	  if (test.indexOf('Sel')!=-1){
	    if (vals[0] == 'NULL' || !vals[0])
		  errors+='- '+nm+' debe contener un valor.\n';
	  }else if (test.indexOf('Rad')!=-1){
	    for(var j = 0; j < val.length; j++)
		  if(val[j].checked){
		    var radCheck = true
			break
		  }
		if(!radCheck)
		  errors+='- '+nm+' debe selecionar un valor.\n';
	  }else if (test.indexOf('Date')!=-1){
	    if (vals[0] == 'NULL' || vals[1] == 'NULL' || vals[2] == 'NULL' )
		  errors+='- '+nm+' debe contener una fecha correcta.\n';
	  }else if (test.indexOf('dateMen')!=-1){
	    var fecIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]));
	    var fecFin = new Date(Number(vals[5]),Number(vals[4])-1,Number(vals[3]));
	    if (fecIni > fecFin)
		  errors+='- '+nm+'\n';
	  }else if (test.indexOf('Hour')!=-1) {
	    if (vals[0] == 'NULL' || vals[1] == 'NULL')
		  errors+='- '+nm+' debe contener una Hora correcta.\n';
	  }else if (test.indexOf('hourMen')!=-1){
	    var hourIni = new Date(1990,0,1,Number(vals[0]),Number(vals[1]),0,0);
	    var hourFin = new Date(1990,0,1,Number(vals[2]),Number(vals[3]),0,0);
	    if (hourIni > hourFin)
		  errors+='- '+nm+'\n';
	  }else if (test.indexOf('datetimeMen')!=-1){
	    var datetimeIni = new Date(Number(vals[2]),Number(vals[1])-1,Number(vals[0]),Number(vals[3]),Number(vals[4]),0,0);
	    var datetimeFin = new Date(Number(vals[7]),Number(vals[6])-1,Number(vals[5]),Number(vals[8]),Number(vals[9]),0,0);
	    if (datetimeIni >= datetimeFin)
		  errors+='- '+nm+'\n';
	  }else if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) {
		  p=val.indexOf('@');
          if (p<1 || p==(val.length-1))
		    errors+='- '+nm+' debe contener una direccion e-mail.\n';
        } else if (test!='R') {
          if (isNaN(val))
		    errors+='- '+nm+' debe contener un n�mero.\n';
          if (test.indexOf('inRange') != -1) {
		    p=test.indexOf(':');
            min=test.substring(8,p);
			max=test.substring(p+1);
            if (val<min || max<val)
			  errors+='- '+nm+' debe contener un n�mero entre '+min+' y '+max+'.\n';
          }
		}
	  } else if (test.charAt(0) == 'R')
	    errors += '- '+nm+' es un dato obligatorio.\n';
	}
  }
  if (errors)
    alert('Los siguientes errores han ocurrido:\n'+errors);
  document.MM_returnValue = (errors == '');
}
-->
</script>
{/literal}