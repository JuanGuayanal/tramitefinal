{$script}
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
	<td width="200" align="center" class="texto">{$selLabel1}</td>
	<td width="80" align="center">&nbsp;</td>
	<td width="200" align="center" class="texto"><b>{$selLabel2}</b></td>
  </tr>
  <tr>
	<td align="center"> 
	  <select name="{$selName1}[]" size="{$selSize}" multiple class="{$selClass}">
		{if $none}
		<option value="{$none.val}">{$none.label}</option>
		{/if}
		{$selContent1}
	  </select>
	</td>
	<td align="center"> 
	  <input type="button" value="Agregar &gt;&gt;" class="but1" onClick="insItems(document.forms['{$frmName}'].elements['{$selName1}[]'], document.forms['{$frmName}'].elements['{$selName2}[]'])">
	  <br>
	  <br>
	  <input type="button" value="&lt;&lt; Eliminar" class="but1" onClick="delItems(document.forms['{$frmName}'].elements['{$selName2}[]'])">
	</td>
	<td align="center">
	  <select name="{$selName2}[]" size="{$selSize}" multiple class="{$selClass}">
		{if $none}
		<option value="{$none.val}">{$none.label}</option>
		{/if}
		{$selContent2}
	  </select>
	</td>
  </tr>
</table>