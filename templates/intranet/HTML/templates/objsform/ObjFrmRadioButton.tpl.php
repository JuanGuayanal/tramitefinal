<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  {section name=i loop=$options}
  <td{if (($smarty.section.i.index+1) is not div by $nCols) and (($smarty.section.i.index+1) == $nItems)} colspan="{math equation='((nRows*nCols)-num)' nRows=$nRows nCols=$nCols num=$smarty.section.i.index}"{/if}>
    <label><input name="{$radName}" type="radio" value="{$options[i].val}" {$options[i].check}>{$options[i].label}</label>
  </td>
    {if (($smarty.section.i.index+1) is div by $nCols)}
  </tr>
  <tr>
    {/if}
  {/section}
  </tr>
</table>