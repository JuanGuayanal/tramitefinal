<table width="550" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrAte} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="27" align="center" valign="middle">{if $arrAte[i].flag=='P'}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_ATENCION}&id={$arrAte[i].id}&menu={$accion.FRM_MODIFICA_ATENCION}"><img src="/img/800x600/ico-mod.gif" alt="[ Cambiar de estado ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {if $arrAte[i].flag=='P'&& $arrAte[i].seCreoInfTec!=1}<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_INFTECNICO}&id={$arrAte[i].id}&menu={$accion.FRM_AGREGA_INFTECNICO}"><img src="/img/800x600/ico-mod.gif" alt="[ Crear Informe T�cnico ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {if $arrAte[i].seCreoInfTec==1}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_INFTECNICO}&id={$arrAte[i].id}&menu={$accion.FRM_MODIFICA_INFTECNICO}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar Informe T�cnico ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  </td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr>
                <td valign="top" class="item"><strong>EIP </strong></td>
                <td colspan="3" class="texto"><strong>:</strong>{$arrAte[i].eip|default:'No 
                  especificado'}</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item"><strong>Embarcaci&oacute;n </strong></td>
                <td class="texto" width="35%"><strong>:{$arrAte[i].emb|default:'No especificado'} {$arrAte[i].matriEmb|default:'No especificado'}</strong></td>
                <td class="sub-item" width="10"><strong>Puerto</strong></td>
                <td class="texto" width="30%"><strong>:{$arrAte[i].puerto|default:'No 
                  especificado'}</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Especie</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].especie|default:'No 
                  especificado'}</td>
                <td class="sub-item"><strong>Destino</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].destino|default:'No 
                especificado'}</td>
              </tr>
			  <tr> 
                <td valign="top" class="sub-item"><strong>Estado Descarga</strong></td>
                <td colspan="3" class="texto"><strong>:</strong>{$arrAte[i].status|default:'No 
                  especificado'}</td>
              </tr>
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Fecha</strong></td>
                <td colspan="3" class="texto"><strong>:</strong>{$arrAte[i].fecha|default:'No 
                  especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Hora de Inicio:&nbsp;&nbsp;</strong>{$arrAte[i].horaini|default:'No 
                  especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Hora de Fin:&nbsp;&nbsp;</strong>{$arrAte[i].horafin|default:'A�n 
                  no se concluye la Atenci�n'}</td>
              </tr>
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Cantidad</strong></td>
                <td colspan="3" class="texto"><strong>:{$arrAte[i].cant|default:'No especificado'} <strong>C&oacute;digo Nro  Tolva :&nbsp;&nbsp;</strong>{$arrAte[i].tolva|default:'No 
                  especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Nro Reporte:&nbsp;&nbsp;</strong>{$arrAte[i].nroReporte|default:'No 
                  especificado'}</strong></td>
              </tr>
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
