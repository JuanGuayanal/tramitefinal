<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Datos SGS</title>
<link href="/styles/desDNPA.css" rel="stylesheet" type="text/css" /></head>
<body>
<xml id="dsoEspecies" src="xml/especies.xml"></xml>
<xml id="dsoDestinos" src="xml/destinos.xml"></xml>
<xml id="dsoEmbarcaciones" src="xml/embarcaciones.xml"></xml>
<xml id="dsoPuertos" src="xml/puertos.xml"></xml>
<xml id="dsoEstablecimientos" src="xml/establecimientos.xml"></xml>
{$jscript}
<!-- {literal} -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>
<!-- {/literal} -->
<form name="{$frmName}" id="{$frmName}" method="post" action="{$frmURL}" >
<input name="accion" type="hidden" value="{$accion.BUSCA_DATOS}" />
<input name="menu" type="hidden" value="{$accion.FRM_BUSCA_DATOS}" />
<input name="page" type="hidden" id="page" value="{$numPage}" />
<table width="100%"  border="0" cellspacing="0" cellpadding="0" background="/img/800x600/dnpa/desembarque/bg.frm.gif">
  <tr><td style="height:10px"></td></tr>
  <tr>
    <td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tbMar">
    <tr>
      <td class="tdPad"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lbEmb"><strong>Embarcaci&oacute;n Pesquera </strong></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="lb1Frm"><input name="radiobutton" type="radio" value="radiobutton" checked="checked" />
                Nombre&nbsp;&nbsp;
                <input name="radiobutton" type="radio" value="radiobutton" disabled="disabled" />
                  Matr&iacute;cula</td>
            </tr>
            <tr>
              <td><input name="desEmbarcacion" type="text" class="txtFrm" id="desEmbarcacion" style="width: 170px" tabindex="1" onfocus="DispatcherOnBlur(); CurrentObj=this" onkeyup="BuscaEmbarcacion()" value="{$desEmbarcacion}" />
              <input name="idEmbarcacion" type="hidden" value="{$idEmbarcacion}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
          <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Especie</strong></td>
            </tr>
            <tr>
              <td><input name="desEspecie" type="text" class="txtFrm" id="desEspecie" style="width: 170px" tabindex="2" onfocus="DispatcherOnBlur(); CurrentObj=this" onkeyup="BuscaEspecie()" value="{$desEspecie}" />
              <input name="idEspecie" type="hidden" value="{$idEspecie}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
          <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Destino</strong></td>
            </tr>
            <tr>
              <td><input name="desDestino" type="text" class="txtFrm" id="desDestino" style="width: 120px" tabindex="3" onfocus="DispatcherOnBlur(); CurrentObj=this" onkeyup="BuscaDestino()" value="{$desDestino}" />
              <input name="idDestino" type="hidden" value="{$idDestino}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
          <td valign="bottom" width="100%">&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td background="/img/800x600/dnpa/desembarque/bg.frm.gif"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="width: 170px" nowrap="nowrap"><div id="cmbEmbLay" style="position:absolute; width:200px; height:115px; z-index:1; visibility: hidden;">
			  <select name="selEmb" size="6" id="selEmb" class="selXML" onfocus="CurrentObj=this">

			  </select>
			</div>
		  </td>
          <td nowrap="nowrap" class="tdSep1"></td>
          <td style="width: 170px" nowrap="nowrap"><div id="cmbEspLay" style="position:absolute; width:200px; height:115px; z-index:2; visibility: hidden;">
			  <select name="selEsp" size="6" id="selEsp" class="selXML" onfocus="CurrentObj=this">
			  </select>
			</div>
		  </td>
          <td nowrap="nowrap" class="tdSep1"></td>
          <td valign="bottom" style="width: 120px" nowrap="nowrap"><div id="cmbDesLay" style="position:absolute; width:200px; height:115px; z-index:2; visibility: hidden;">
			  <select name="selDes" size="6" id="selDes" class="selXML" onfocus="CurrentObj=this">
			  </select>
			</div></td>
          <td nowrap="nowrap" class="tdSep1"></td>
          <td valign="bottom" width="100%"></td>
        </tr>
      </table></td>
    </tr>
  </table></td>
  </tr>
  
  <tr>
  	<td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tbMar">
	<tr>
		  <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Puerto</strong></td>
            </tr>
            <tr>
              <td><input name="desPuerto" type="text" class="txtFrm" id="desPuerto" style="width: 170px" tabindex="3" onfocus="DispatcherOnBlur(); CurrentObj=this" onkeyup="BuscaPuerto()" value="{$desPuerto}" />
              <input name="idPuerto" type="hidden" value="{$idPuerto}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
		  <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>EIP</strong></td>
            </tr>
            <tr>
              <td><input name="desEstablecimiento" type="text" class="txtFrm" id="desEstablecimiento" style="width: 170px" tabindex="3" onfocus="DispatcherOnBlur(); CurrentObj=this" onkeyup="BuscaEstablecimiento()" value="{$desEstablecimiento}" />
              <input name="idEstablecimiento" type="hidden" value="{$idEstablecimiento}" /></td>
            </tr>
          </table></td>
		  <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
		  <td valign="bottom" width="100%">&nbsp;</td>	
	</tr>
	</table>
	</td>
  </tr>
  
  <tr>
      <td background="/img/800x600/dnpa/desembarque/bg.frm.gif"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
		  <td nowrap="nowrap" class="tdSep1"></td>
          <td valign="bottom" style="width: 170px" nowrap="nowrap"><div id="cmbPtoLay" style="position:absolute; width:200px; height:115px; z-index:1; visibility: hidden;">
			  <select name="selPto" size="6" id="selPto" class="selXML" onfocus="CurrentObj=this">
			  </select>
			</div></td>
		  <td nowrap="nowrap" class="tdSep1"></td>
          <td valign="bottom" style="width: 170px" nowrap="nowrap"><div id="cmbEIPLay" style="position:absolute; width:200px; height:115px; z-index:2; visibility: hidden;">
			  <select name="selEIP" size="6" id="selEIP" class="selXML" onfocus="CurrentObj=this">
			  </select>
			</div></td>
          <td nowrap="nowrap" class="tdSep1"></td>
          <td valign="bottom" width="100%"></td>
		</tr>
		</table>
	  </td>	  
    </tr>  
  
  <tr>
  	<td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tbMar">
    <tr>
      <td colspan="6"><img src="/img/800x600/dnpa/desembarque/sep1.frm.gif" alt="" width="70%" height="10" /></td>
    </tr>	
	<tr>
		  <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Nro. Tolva</strong></td>
            </tr>
            <tr>
              <td><input name="nroTolva" type="text" class="txtFrm" id="nroTolva" style="width: 170px" tabindex="3"  value="{$nroTolva}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
		  <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Nro. Reporte</strong></td>
            </tr>
            <tr>
              <td><input name="nroReporte" type="text" class="txtFrm" id="nroReporte" style="width: 170px" tabindex="3" value="{$nroReporte}" /></td>
            </tr>
          </table></td>
          <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
		  <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Estado descarga </strong></td>
            </tr>
            <tr>
              <td><select name="statusDescarga" class="selFrm" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selStatusDescarga}
		</select></td>
            </tr>
          </table></td>
		  <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
		  <td valign="bottom" width="100%">&nbsp;</td>	
	</tr>
	</table>
	</td>
  </tr>  
  
  <tr>
    <td><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tbMar">
    <tr>
      <td colspan="6"><img src="/img/800x600/dnpa/desembarque/sep1.frm.gif" alt="" width="70%" height="10" /></td>
    </tr>
    <tr>
      <td colspan="6"><img src="/img/800x600/bg_blank.gif" alt="" width="1" height="15" /></td>
    </tr>
    <tr>
      <td nowrap="nowrap" class="lb1Frm"><strong>Desde el </strong></td>
      <td nowrap="nowrap"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="desFechaIni" type="text" class="txtFrm" id="desFechaIni" value="{$desFechaIni}" tabindex="4" />
    <input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="{$datos.fecDesIni}" /></td>
    <td align="center"><a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
  </tr>
</table>
</td>
      <td nowrap="nowrap" class="tdSep1">&nbsp;</td>
      <td nowrap="nowrap" class="lb1Frm"><strong>hasta el </strong></td>
      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="desFechaFin" type="text" class="txtFrm" id="desFechaFin" value="{$desFechaFin}" tabindex="5" />
    <input name="fecDesembarqueFin" type="hidden" id="fecDesembarqueFin" value="{$datos.fecDesFin}" /></td>
    <td align="center"><a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
  </tr>
</table></td>
      <td width="100%" nowrap="nowrap"><input name="bSubmit" type="submit" class="txtFrm" alt="Buscar Desembarque" hspace="10" /></td>
    </tr>
    <tr>
      <td class="lb1Frm"></td>
      <td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td nowrap="nowrap" class="tdSep1"></td>
      <td class="lb1Frm"></td>
      <td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
  </table></td>
  </tr>
</table>
</form>
<br />
<script language="JavaScript" type="text/javascript">
<!--
var pFormDes = document.{$frmName}
-->
</script>
<script language="JavaScript" type="text/javascript" src="/js/desDNPA.js"></script>
</body>
</html>