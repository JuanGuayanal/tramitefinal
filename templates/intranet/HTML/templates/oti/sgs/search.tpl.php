{$jscript}
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = "tr-check";
}
{/literal}
-->
</script>
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DATOS}">
  <table width="490" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="8" class="textoblack">&nbsp;</td>
      <td width="150" class="textoblack"><strong>Estado de Descarga </strong></td>
      <td width="217" valign="middle"> 
	  	<select name="statusDescarga" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selStatusDescarga}
		</select></td>
      <td width="120" valign="middle"> <input type="submit" class="submit" onClick="document.{$frmName}.page.value=''" value="Listar"> 
      </td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Establecimiento Industrial Pesquero </strong></td>
      <td colspan="2" valign="middle"> <select name="eip" id="eip" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selEIP}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Especie descargada </strong></td>
      <td colspan="2" valign="middle"> <select name="especieDescargada" id="especieDescargada" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selEspecieDescargada}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Tipo de destino </strong></td>
      <td colspan="2" valign="middle"> <select name="destino" id="destino" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selDestino}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Puerto</strong></td>
      <td colspan="2" valign="middle"> <select name="puerto" id="puerto" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selPuerto}
			 </select></td>
    </tr>		
    <tr> 
      <td class="texto td-encuesta"><strong>Embarcarcaci&oacute;n pesquera </strong></td>
      <td colspan="2" valign="middle"> <select name="embPesq" id="embPesq" class="ipsel2" onChange="submitForm('{$accion.BUSCA_DATOS}')">
			{$selEmbPesquera}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Nro. de reporte </strong></td>
      <td colspan="2" valign="middle"><input name="nroReporte" type="text" class="iptxt1" value="{$nroReporte}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Nro. de tolva</strong></td>
      <td colspan="2" valign="middle"><input name="nroTolva" type="text" class="iptxt1" value="{$nroTolva}" size="30" maxlength="100"></td>
    </tr>	
  </table>
</form>