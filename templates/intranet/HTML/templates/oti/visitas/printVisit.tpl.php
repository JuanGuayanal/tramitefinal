<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registro de Visitas en el Ministerio de la Producci&oacute;n</title>
<link href="/styles/intranet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#000000">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF"><img src="/img/800x600/logo-CONVENIO_SITRADOC.jpg" width="175" height="49"></td>
          <td align="right" class="textogray" bgcolor="#FFFFFF">Servicio prestado por la Intranet Institucional del Ministerio de la Producci&oacute;n&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr> 
          <!--
		  <td class="texto-servicios-white"><strong><a href="{$frmUrl}?id={$emb.id}&accion={$accion.IMPRIME_DETALLE}"><img src="/img/apps/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong>&nbsp;&nbsp;&nbsp;<strong><a href="javascript:window.close()"><img src="/img/apps/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            </strong></td>-->
			<td class="texto-servicios-white"><strong><a href="javascript:print()"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong>&nbsp;&nbsp;&nbsp;<strong><a href="javascript:window.close()"><img src="/img/800x600/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            &nbsp;&nbsp;&nbsp;<strong><a href="{$frmUrl}?accion={$accion.IMPRIME_VISITA}&tipo={$tipo}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}&nombreVisitante={$nombreVisitante}&dniVisitante={$dniVisitante}&funcionario={$funcionario}&horInicio={$horInicio}&horInicioP={$horInicioP}&horFin={$horFin}&horFinP={$horFinP}&print=3&xls=1&tipo2={$tipo2}&tipo3={$codfuncio}" target="_blank"><img src="/img/800x600/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Descargar en Excel</a> </strong></td>
            <td align="right" class="texto-servicios-white"><!--<strong><a href="javascript:window.close()"><img src="/img/apps/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            </strong>-->
            </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="textoblack"><b>REGISTRO DE VISITAS EN EL MINISTERIO DE LA PRODUCCI&Oacute;N</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>&nbsp;</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>N&deg;</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>VISITANTE</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DOCUMENTO<br />
    (DNI / CARNET<br />
      EXTRANJERIA / OTROS
      )</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>ASUNTO</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>PERSONA<br />
    VISITADA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DEPENDENCIA</strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>FECHA<br />
      DE VISITA</strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA<br />
      PROGRAMADA </strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA
        DE<br />
        INGRESO<br />
    AL CONVENIO_SITRADOC</strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA
      DE<br />
      INGRESO 
      A 
      LA <br />
      DEPENDENCIA</strong></td>
    <td align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA
      DE<br />
      SALIDA A LA<br />
      DEPENDENCIA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA DE<br />
      SALIDA
          <br />
    DEL CONVENIO_SITRADOC</strong></td>
  </tr>
  {section name=i loop=$visi}
  <tr bgcolor="#FFFFF2">
    <td valign="top" class="textograyintranet" style="border-bottom: 1px solid #EBEBEB"><!--{if ($tipo=='Y')}{ if $visi[i].horaIngreso==""}<a href="{$frmUrl}?accion={$accion.REGISTRA_ENTRADA}&id={$visi[i].id}"><img src="/img/800x600/ico.entrada.jpg" alt="[ REGISTRAR ENTRADA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}{if ($visi[i].horaIngreso!="" && $visi[i].horaSalida=="")}<a href="{$frmUrl}?accion={$accion.REGISTRA_SALIDA}&id={$visi[i].id}"><img src="/img/800x600/ico.salida.jpg" alt="[ REGISTRAR SALIDA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}{/if}--></td>
	<td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$smarty.section.i.iteration}</td>
	<td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].nomb}<!--{$visi[i].codvisita}--></td>
    <td nowrap="nowrap" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center"><!--{$visi[i].docu}: -->{$visi[i].ndoc}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].asun}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].func}<!--{$visi[i].codfunci}--> </td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].siglas}<!--{$visi[i].coddepen}--></td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].fecE}<br /></td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">    {$visi[i].horaProg}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].horaE}</td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].horaIngreso}</td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].horaSalida}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].horaS}</td>
  </tr>
  {sectionelse}
  <tr>
    <td colspan="13" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="13" class="textograyintranet">No hay visitantes.</td>
  </tr>
  {/section}
  <tr>
      <td colspan="13" class="textograyintranet">&nbsp;</td>
  </tr>
    <tr>
        <td colspan="13" class="textograyintranet">&nbsp;&nbsp;<b>Oficina de Tecnolog&iacute;a de la Informaci&oacute;n - OGTIE<br>
        </b></td>
    </tr>
    <tr>
        <td colspan="13" class="textograyintranet">&nbsp;&nbsp;<b>Email: intranet@CONVENIO_SITRADOC.gob.pe<br>
        </b></td>
    </tr>
</table>
</body>
</html>
