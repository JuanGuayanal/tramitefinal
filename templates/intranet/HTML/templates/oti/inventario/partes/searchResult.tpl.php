{literal}
<script language="JavaScript">
function MinReq(pFrm,pMax){
	if(pFrm.elements['id[]'].length!=undefined){
		var cItems = 0
		for(var i=0;i<pFrm.elements['id[]'].length;i++)
			if(pFrm.elements['id[]'][i].checked)
				cItems++
		return (cItems>0) ? ((cItems<=pMax||pMax==-1) ? 1 : 2) : 0
	}else{
		if(pFrm.elements['id[]'].checked)
			return 1
	}
	return 0
}
function Modifica(pFrm,pAcc){
	var result = MinReq(pFrm,1)
	if(result==1){
		pFrm.accion.value=pAcc
		pFrm.subMenu.value=pAcc
		pFrm.submit()
	}else if(result==2)
		alert('Debe seleccionar como m�ximo una Parte o Pieza a Modificar')
	else if(result==0)
		alert('Debe seleccionar una Parte o Pieza a Modificar')
}
function DelParte(pFrm,pAcc){
	var result = MinReq(pFrm,-1)
	if(result==1){
		if(confirm('�Ud. est� seguro que desea DAR DE BAJA las Partes o Piezas seleccionadas?')){
			pFrm.accion.value=pAcc
			pFrm.subMenu.value=pAcc
			pFrm.submit()
		}
	}else
		alert('Debe seleccionar al menos una Parte o Pieza a Dar de baja')
}
</script>
{/literal}
<form name="{$frmName2}" action="{$frmUrl}" method="post">
{foreach key=name item=value from=$datos}
  <input type="hidden" name="{$name}" value="{$value}">
{/foreach}
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_PARTE_EXTENDIDA}">
</form>
<form name="{$frmName}" action="{$frmUrl}" method="post">
<input type="hidden" name="accion" />
<input type="hidden" name="menu" value="{$accion.FRM_BUSCA_PARTE}" />
<input type="hidden" name="subMenu" />
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td valign="middle">
		    {if $arrPar}
		      <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><a href="javascript:Modifica(document.{$frmName},'{$accion.FRM_MODIFICA_PARTE}')"><img src="/img/800x600/ico_calificar.gif" alt="" width="17" height="17" hspace="2" border="0" align="absmiddle" /></a></td>
                  <td nowrap="nowrap"><a href="javascript:Modifica(document.{$frmName},'{$accion.FRM_MODIFICA_PARTE}')"><strong>Modificar</strong></a></td>
                  <td>&nbsp;</td>
                  <td><a href="javascript:DelParte(document.{$frmName},'{$accion.ELIMINA_PARTE}')"><img src="/img/800x600/ico.eliminar.gif" alt="" width="12" height="12" hspace="2" border="0" align="absmiddle" /></a></td>
                  <td nowrap="nowrap"><a href="javascript:DelParte(document.{$frmName},'{$accion.ELIMINA_PARTE}')"><strong>Dar de baja</strong></a></td>
                  <td>&nbsp;</td>
                  <td><a href="javascript:SubmitForm(document.{$frmName2},'{$accion.BUSCA_PARTE_EXTENDIDA}');"><img src="/img/800x600/ico_doc.gif" alt="" width="17" height="17" hspace="2" border="0" align="absmiddle" /></a></td>
                  <td nowrap="nowrap"><a href="javascript:SubmitForm(document.{$frmName2},'{$accion.BUSCA_PARTE_EXTENDIDA}');"><strong>Ver Listado</strong></a></td>
                </tr>
              </table>
            <!-- <input name="button" type="button" class="submit" onClick="SubmitForm(document.{$frmName},'{$accion.BUSCA_PARTE_EXTENDIDA}');this.disabled=true" value="Ver Listado"> -->
            {/if}
		  </td>
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal}&nbsp;&nbsp; 
            {else} No hubo coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrPar} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="27" align="center" valign="middle">
		  {if ($codigoTrabajador==1551 && $arrPar[i].esta != 'DE BAJA')}
		 
		    <input type="checkbox" name="id[]" value="{$arrPar[i].id}" onclick="hL(this);" />
		  {else}
		    &nbsp;
		  {/if}
		  </td>
          <td colspan="2" valign="top" class="item">
		    <table width="100%">
              <tr> 
                <td valign="top" nowrap class="contenido-intranet"><strong>Descripci&oacute;n:&nbsp;&nbsp;</strong></td>
                <td width="100%" valign="top" class="texto">{$arrPar[i].desc|default:'No 
                  especificado'}</td>
              </tr>
              <tr> 
                <td colspan="2" valign="top" class="textogray"><strong>Disp:</strong> 
                  {$arrPar[i].disp|default:'No especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Ubic:</strong> 
                  {$arrPar[i].ubic|default:'No especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Tecno:</strong> 
                  {$arrPar[i].tecn|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td colspan="2" valign="top" class="textogray"><strong>Marc:</strong> 
                  {$arrPar[i].marc|default:'No especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Mod:</strong> 
                  {$arrPar[i].mode|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td colspan="2" valign="top" class="texto"><span class="contenido-intranet">Cod. 
                  Patrimonial:</span> {$arrPar[i].cPat|default:'NO ESPECIFICADO'}&nbsp;&nbsp;&nbsp;&nbsp;<span class="contenido-intranet">Num. 
                  Serie:</span> {$arrPar[i].nSer|default:'NO ESPECIFICADO'}</td>
              </tr>
              {if $arrPar[i].siglas} 
              <tr> 
                <td valign="top" nowrap class="sub-item">Dependencia:</td>
                <td class="texto">{$arrPar[i].siglas}</td>
              </tr>
              {/if}			  
              {if $arrPar[i].orig} 
              <tr> 
                <td valign="top" nowrap class="contenido-intranet">PC de Origen:</td>
                <td class="texto">{$arrPar[i].orig}</td>
              </tr>
              {/if} 
              {if $arrPar[i].obse} 
              <tr> 
                <td valign="top" class="sub-item-intranet">Observaci&oacute;n:</td>
                <td class="contenido-intranet">{$arrPar[i].obse}</td>
              </tr>
              {/if} 
              <tr> 
                <td valign="top" class="sub-item-intranet">Estado:</td>
                <td class="sub-item-intranet"><b>{$arrPar[i].esta}</b></td>
              </tr>
            </table>
		  </td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
</form>