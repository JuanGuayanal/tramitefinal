{$jscript}
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	var stat = E.checked;
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = (stat) ? "tr-check" : null;
}
function SubmitForm(pForm,pAccion){
	pForm.accion.value=pAccion
	pForm.submit()
	return false
}
{/literal}
-->
</script>
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_PARTE}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3" class="tree">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" class="tree">&nbsp;</td>
    </tr>
    <tr>
      
	  <td valign="middle" bgcolor="#31698C" class="tree">&nbsp;&nbsp;<a href="/institucional/aplicativos/oti/inventory/index.php?accion=frmSearchPart&menu=frmSearchPart" >Inventario de Partes y Piezas</a></td>
	  <td width="20" valign="middle" bgcolor="#31698C" class="tree"><!--<div align="center">|</div>--></td>
	  <td height="25" valign="middle" bgcolor="#31698C" class="treeOUT"><!--&nbsp;&nbsp;<a href="/institucional/aplicativos/oti/inventory/index.php?accion=frmSearchPC&menu=frmSearchPC" class="treeOUT">Inventario de PC's</a>--> </td>
      
      
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td width="200" class="contenido-intranet"><strong>:: <a href="/institucional/aplicativos/oti/inventory/index.php?accion=frmSearchPart&subMenu=frmSearchPart&menu=frmSearchPart">BUSCAR PARTE O PIEZA </a> :: </strong></td>
      <td align="center" class="contenido-intranet">|</td>
      <td width="400" class="contenido-intranet"><strong>:: <a href="/institucional/aplicativos/oti/inventory/index.php?accion=frmAddPart&subMenu=frmAddPart&menu=frmSearchPart">AGREGAR PARTE O PIEZA </a> :: </strong></td>
    </tr>
    <tr>
      <td colspan="3" class="contenido-intranet">&nbsp;</td>
    </tr>
  </table>
<table width="95%" height="60" border="0" align="center" cellpadding="0" cellspacing="0" background="/img/800x600/bg_tab_encuesta_intranet.gif" class="tabla-login">
    <tr> 
      <td align="right">&nbsp;</td>
      <td align="right">&nbsp;</td>
      <td width="21"><!--<img src="/img/800x600/ogdpe/proyectos/ezq_hdsearch_ard.gif" width="21" height="17">--></td>
    </tr>
    <tr> 
      <td width="10" align="right" class="textoblack" ><img src="/img/800x600/bg_blank.gif" width="10" height="8"></td>
      <td align="right" class="textoblack" ><table width="80%" height="60" border="0" align="center" cellpadding="0" cellspacing="2">
          <tr> 
            <td align="right" class="contenido-intranet">Dispositivo&nbsp;&nbsp;</td>
            <td colspan="3" valign="top" class="textoblack" align="left"> 
              <select name="idTipo" class="ip-login contenido" id="idTipo" onChange="document.{$frmName}.idUbi[0].selected=true;document.{$frmName}.idTecno[0].selected=true;document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('{$accion.BUSCA_PARTE}')">
			  {$selTipo}
              </select>              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estado&nbsp; 
              <select name="estado" class="ip-login contenido" id="estado" onChange="submitForm('{$accion.BUSCA_PARTE}')">
			    <option value="none"{if $estado=='none'} selected{/if}>Todos</option>
                <option value="A"{if $estado=='A'} selected{/if}>Asignado</option>
				<option value="O"{if $estado=='O'} selected{/if}>Operativo</option>
                <option value="I"{if $estado=='I'} selected{/if}>Inoperativo</option>
                <option value="B"{if $estado=='B'} selected{/if}>De Baja</option>
              </select> 
             
            </td>
          </tr>
          <tr> 
            <td colspan="4" align="right" class="textogray" ><hr width="100%" size="1"></td>
          </tr>
          <tr> 
            <td width="100" class="contenido-intranet">Ubicaci&oacute;n</td>
            <td ><select name="idUbi" class="ip-login contenido" id="idUbi" onChange="document.{$frmName}.idTecno[0].selected=true;document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('{$accion.BUSCA_PARTE}')">
			{$selUbi}
              </select> </td>
            <td class="contenido-intranet">Tecnolog&iacute;a</td>
            <td ><select name="idTecno" class="ip-login contenido" id="idTecno" onChange="document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('{$accion.BUSCA_PARTE}')">
			{$selTecno}
              </select> </td>
          </tr>
          <tr> 
            <td nowrap class="contenido-intranet">Marca&nbsp;</td>
            <td><select name="idMarca" class="ip-login contenido" id="idMarca" onChange="document.{$frmName}.idModelo[0].selected=true;submitForm('{$accion.BUSCA_PARTE}')">
			{$selMarca}
              </select> </td>
            <td class="contenido-intranet">Modelo&nbsp;</td>
            <td><select name="idModelo" class="ip-login contenido" id="idModelo" onChange="submitForm('{$accion.BUSCA_PARTE}')">
			{$selModelo}
              </select> </td>
          </tr>
        </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="textogray" >&nbsp;</td>
      <td align="right" class="textogray" >&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textogray" >&nbsp;</td>
      <td align="right" class="textogray" > &nbsp;&nbsp;&nbsp;<input name="bSubmit" type="submit" class="submitintranet" value="Buscar"> </td>
      <td><!--<img src="/img/800x600/ogdpe/proyectos/ezq_hdsearch_abd.gif" width="21" height="17">--></td>
    </tr>
    <tr>
      <td align="right" class="textogray" >&nbsp;</td>
      <td align="right" class="textogray" >&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
