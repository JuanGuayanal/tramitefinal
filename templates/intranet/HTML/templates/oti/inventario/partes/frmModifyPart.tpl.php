{$jscript} 
<!-- {literal} -->
<script language="JavaScript">
function DisEnItem(pCond,pItem){
  var args=DisEnItem.arguments
  var stat = (pCond.value=='N')
  for(var i=1;i<args.length;i++){
    if(stat)
      MM_findObj(args[i]).className = MM_findObj(args[i]).className+"-disabled"
	else
	  MM_findObj(args[i]).className = MM_findObj(args[i]).className.substring(0,MM_findObj(args[i]).className.indexOf("-disabled"))
    MM_findObj(args[i]).disabled = stat
  }
  if(stat&&MM_findObj('idCambio[]')){/literal}
    submitForm('#trans','{$accion.FRM_MODIFICA_PARTE}'){literal}
}
</script>
<!-- {/literal} -->
<script language="JavaScript">
function ListDisp(){literal}{{/literal}
	if(document.MM_returnValue){literal}{{/literal}
		submitForm('#trans','{$accion.FRM_MODIFICA_PARTE}')
	{literal}}{/literal}else
		return document.MM_returnValue
{literal}}{/literal}
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('desNombre','Descripción','R','idTipo','Tipo Dispositivo','Sel','idUbi','Ubicación Dispositivo','Sel','idTecno','Tecnología Dispositivo','Sel','idMarca','Marca Dispositivo','Sel','idModelo','Modelo Dispositivo','Sel'{if $indCambio},'idCambio[]','Cambio Por','Sel'{/if});return document.MM_returnValue">
<br>
  <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td class="tree">&nbsp;</td>
    </tr>
    <tr>
      <td class="tree">&nbsp;</td>
    </tr>
    <tr>
      <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp; Inventario de Partes y Piezas :: Modificar </td>
    </tr>
    <tr>
      <td class="contenido-intranet">&nbsp;</td>
    </tr>
    <tr>
      <td class="contenido-intranet"> ::<a href="/institucional/aplicativos/oti/inventory/index.php?accion=frmSearchPart&menu=frmSearchPart"><b>BUSCAR PARTE O PIEZA </b></a>::</td>
    </tr>
    <tr>
      <td class="tree">&nbsp;</td>
    </tr>
  </table>
  <table width="95%" border="0" align="center" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF">
    {if $errors} 
    <tr> 
      <td colspan="2" class="contenido-intranet">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="disp" id="disp"></a>DISPOSITIVO</strong></td>
    </tr>
    <tr> 
      <td width="100" nowrap class="contenido-intranet">Descripci&oacute;n:</td>
      <td width="430"> <input name="desNombre" type="text" class="ip-login contenido" id="desNombre" value="{$desNombre}" size="35"> 
      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Cod. Patrimonial:</td>
      <td><input name="codPatrimonial" type="text" class="ip-login contenido" id="codPatrimonial" value="{$codPatrimonial}" size="35"></tr>
    <tr> 
      <td class="contenido-intranet"> Num. Serial:</td>
      <td> <input name="desSerial" type="text" class="ip-login contenido" id="desSerial" value="{$desSerial}" size="35"></tr>
    <tr> 
      <td class="contenido-intranet">Fec. Ingreso</td>
      <td><select name="dia_ing" class="ip-login contenido" id="dia_ing">
      {$selDiaIng}
	    </select> <select name="mes_ing" class="ip-login contenido" id="mes_ing">
	  {$selMesIng}
        </select> <select name="anyo_ing" class="ip-login contenido" id="anyo_ing">
      {$selAnyoIng}
        </select></td>
    </tr>
    <tr>
      <td class="contenido-intranet">Proveedor</td>
      <td>
        <select name="codEmp" class="ip-login contenido" id="codEmp">
          
          
		  {$selEmpresa}
        
        
        </select>
      </td>
    </tr>
    <tr>
      <td class="contenido-intranet">Garantia (Meses)</td>
      <td><input name="numGaran" type="text" class="ip-login contenido" id="numGaran" value="{$numGaran}" size="5" maxlength="3"></td>
    </tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="carac" id="carac"></a>CARACTER&Iacute;STICAS</strong></td>
    </tr>
    <tr> 
      <td valign="middle" class="contenido-intranet"> Dispositivo:</td>
      <td> <table width="100%%" border="0" cellspacing="0" cellpadding="0" class="TEmb">
          <tr> 
            <td><select name="idTipo" class="ip-login contenido" id="idTipo" onChange="document.{$frmName}.idUbi[0].selected=true;document.{$frmName}.idTecno[0].selected=true;document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('#carac','{$accion.FRM_MODIFICA_PARTE}')">
			  {$selTipo}
              </select></td>
            <td><select name="idUbi" class="ip-login contenido" id="idUbi" onChange="document.{$frmName}.idTecno[0].selected=true;document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('#carac','{$accion.FRM_MODIFICA_PARTE}')">
			{$selUbi}
              </select></td>
            <td width="100%"><select name="idTecno" class="ip-login contenido" id="idTecno" onChange="document.{$frmName}.idMarca[0].selected=true;document.{$frmName}.idModelo[0].selected=true;submitForm('#carac','{$accion.FRM_MODIFICA_PARTE}')">
			{$selTecno}
              </select> </td>
          </tr>
        </table></tr>
    <tr> 
      <td class="contenido-intranet">Marc. y Mod:</td>
      <td> <table width="100%%" border="0" cellpadding="0" cellspacing="0" class="TEmb">
          <tr> 
            <td><select name="idMarca" class="ip-login contenido" id="idMarca" onChange="document.{$frmName}.idModelo[0].selected=true;submitForm('#carac','{$accion.FRM_MODIFICA_PARTE}')">
			{$selMarca}
              </select></td>
            <td width="100%"> <select name="idModelo" class="ip-login contenido" id="idModelo">
			{$selModelo}
              </select> </td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="2" background="/img/800x600/bg_horiz.gif" class="bg-med-rx"><img src="/img/800x600/bg_blank.gif" width="1" height="1" vspace="3"></td>
    </tr>
    <tr> 
      <td class="contenido-intranet"> <strong>Estado:</strong></td>
      <td><table width="100%%" border="0" cellspacing="0" cellpadding="0" class="TEmb">
          <tr> 
            <td valign="middle" nowrap class="texto"> <input name="indEstado" type="radio" value="A"{if $indEstadoCambio!='I'&&$indEstadoCambio!='B'} checked{/if}>
              Asignado</td>
			<td valign="middle" nowrap class="texto"><input name="indEstado" type="radio" value="O"{if $indEstado!='I'&&$indEstado!='B'} checked{/if}>
              Operativo</td>
            <td valign="middle" nowrap class="texto"> <input type="radio" name="indEstado" value="I"{if $indEstado=='I'} checked{/if}>
              Inoperativo </td>
            <td width="100%" valign="middle" class="texto"> <input type="radio" name="indEstado" value="B"{if $indEstado=='B'} checked{/if}>
              De Baja</td>
          </tr>
        </table></tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="trans" id="trans"></a>TRANSFERENCIA</strong></td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Destinado a PC:</td>
      <td class="contenido-intranet"><table width="100%%" border="0" cellspacing="0" cellpadding="0" class="TEmb">
          <tr> 
            <td valign="middle" nowrap class="texto"><input name="indTrans" type="radio" value="Y"{if $indTrans=='Y'} checked{/if} onClick="DisEnItem(this,'codInventariado','indCambio','numDuracionCambio')">
              Si</td>
            <td width="100%" valign="middle" nowrap class="texto"><input name="indTrans" type="radio" value="N"{if $indTrans!='Y'} checked{/if} onClick="DisEnItem(this,'codInventariado','indCambio','numDuracionCambio')">
              No </td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Cod. Inv. PC:</td>
      <td class="textored"> <input name="codInventariado" type="text" class="iptxt1{if $indTrans!='Y'}-disabled{/if}" id="codInventariado" value="{$codInventariado}" size="10" maxlength="4"{if $indTrans!='Y'} disabled{/if}> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="indCambio" type="checkbox" id="indCambio" value="1"{if $indCambio} checked{/if}{if $indTrans!='Y'} disabled{/if} onClick="MM_validateForm('codInventariado','Cod. Inv. PC','R','idTipo','Tipo Dispositivo','Sel','idUbi','Ubicación Dispositivo','Sel','idTecno','Tecnología Dispositivo','Sel','idMarca','Marca Dispositivo','Sel','idModelo','Modelo Dispositivo','Sel');return ListDisp();return document.MM_returnValue"> 
        <strong>&iquest;Destinado como C&aacute;mbio?</strong></td>
    </tr>
    <tr>
      <td class="contenido-intranet">Duraci&oacute;n Trans.:</td>
      <td class="texto"><input name="numDuracionCambio" type="text" class="iptxt1{if $indTrans!='Y'}-disabled{/if}" id="numDuracionCambio" value="{$numDuracionCambio}" size="5" maxlength="4"{if $indTrans!='Y'} disabled{/if} /> 
    minutos </td></tr>
    {if $indCambio} 
    <tr> 
      <td colspan="2" background="/img/800x600/bg_horiz.gif" class="bg-med-rx"><img src="/img/800x600/bg_blank.gif" width="1" height="1" vspace="3"></td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Cambio Por:</td>
      <td class="textored"> <select name="idCambio[]" size="3" multiple class="ip-login contenido" id="idCambio[]">
		{$selCambio}
        </select></td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Estado:</td>
      <td class="contenido-intranet"><table width="100%%" border="0" cellspacing="0" cellpadding="0" class="TEmb">
          <tr> 
            <td valign="middle" nowrap class="texto"> <input name="indEstadoCambio" type="radio" value="A"{if $indEstadoCambio!='I'&&$indEstadoCambio!='B'} checked{/if}>
              Asignado</td>
			<td valign="middle" nowrap class="texto"> <input name="indEstadoCambio" type="radio" value="O"{if $indEstadoCambio!='I'&&$indEstadoCambio!='B'} checked{/if}>
              Operativo</td>
            <td valign="middle" nowrap class="texto"> <input type="radio" name="indEstadoCambio" value="I"{if $indEstadoCambio=='I'} checked{/if}>
              Inoperativo </td>
            <td width="100%" valign="middle" class="texto"> <input type="radio" name="indEstadoCambio" value="B"{if $indEstadoCambio=='B'} checked{/if}>
              De Baja</td>
			  
          </tr>
        </table></td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="obs" id="obs"></a>OBSERVACIONES</strong></td>
    </tr>
    <tr> 
      <td class="contenido-intranet">Observaci&oacute;n:</td>
      <td class="contenido-intranet"><textarea name="desObservacion" cols="30" rows="3" class="ip-login contenido" id="desObservacion">{$desObservacion}</textarea></td>
    </tr>
    <tr> 
      <td colspan="2"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="2"> <input name="bSubmit" type="Submit" class="submitintranet" value="Modifica Parte" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitintranet" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_PARTE}&menu={$accion.FRM_BUSCA_PARTE}&subMenu={$accion.FRM_BUSCA_PARTE}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_PARTE}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}"> 
        <input name="id" type="hidden" id="id" value="{$id}"> <input name="reLoad" type="hidden" id="reLoad" value="1"> 
      </td>
    </tr>
  </table>
</form>