<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<br>
<table width="100%" height="40" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2"><img src="/img/800x600/line.single.solid1px.jpg" width="100%" height="1"></td>
  </tr>
  <tr>
    <td width="100">&nbsp;</td>
    <td>
    <font face="Arial" color="#000000" size="2"><b>DETALLE DE COMPONENTES DE UNA MICROCOMPUTADORA </b></font><br></td>
  </tr>
  <tr>
    <td colspan="2"><img src="/img/800x600/line.doble.solid1px.jpg" width="100%" height="3"></td>
  </tr>
</table>
<blockquote>
  <blockquote><br />
  <table width="1270" border="0" align="center" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
  <tr align="center">
    <td colspan="2" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
  </tr>
  <tr bgcolor="#EAEAEA">
    <td width="100%" colspan="2"><font size="2" face="Arial"><strong>USUARIO</strong></font></td>
  </tr>
  <tr align="center">
	<td colspan="2" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
  </tr>
  <tr>
    <td width="200" nowrap><font size="1" face="Arial"><strong>Dependencia</strong></font></td>
    <td width="100%" class="textogray"><font size="1" face="Arial">{$desDependencia|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td nowrap><font size="1" face="Arial"><strong>Trabajador</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desTrabajador|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><font size="1" face="Arial">&nbsp;</font></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
  </tr>
  <tr bgcolor="#EAEAEA">
    <td width="100%" colspan="2"><font size="2" face="Arial"><strong>M&Aacute;QUINA</strong></font></td>
  </tr>
  <tr align="center">
	<td colspan="2" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
  </tr>
  <tr>
    <td nowrap><font size="1" face="Arial"><strong>C&oacute;digo Inventario&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$codInventariado|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td valign="middle"><font size="1" face="Arial"><strong>Nombre PC</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desNombre|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Cod. Patrimonial</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$codPatrimonial|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Serial</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desSerial|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Tipo CPU</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desTipo|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Marca CPU</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desMarca|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Modelo CPU</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desModelo|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Desc. Modelo</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desTipo|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Fec. Ingreso</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{if $numDiaIng && $numMesIng && $numAnyoIng}{$numDiaIng}/{$numMesIng}/{$numAnyoIng}{else}"NO ESPECIFICADO"{/if}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Proveedor</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desEmpresa|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Garantia (Meses)</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$numGaran|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial"><strong>Entrada</strong></font></td>
    <td class="textogray"><font size="1" face="Arial">{$desEntrada|default:"NO ESPECIFICADO"}</font></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><font size="1" face="Arial">&nbsp;</font></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
  </tr>
  <tr bgcolor="#EAEAEA">
    <td width="100%" colspan="2"><font size="2" face="Arial"><strong>DISPOSITIVOS</strong></font></td>
  </tr>
  <tr align="center">
	<td colspan="2" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="textogray"><font size="1" face="Arial"><strong>Tipo</strong></font></td>
          <td class="textogray"><font size="1" face="Arial"><strong>Ubicaci�n</strong></font></td>
          <td class="textogray"><font size="1" face="Arial"><strong>Tecnolog�a</strong></font></td>
          <td class="textogray"><font size="1" face="Arial"><strong>Marca</strong></font></td>
          <td class="textogray"><font size="1" face="Arial"><strong>Modelo</strong></font></td>
          <td class="textogray"><font size="1" face="Arial"><strong>N� de Serie</strong></font></td>
		  <td class="textogray"><font size="1" face="Arial"><strong>Patrimonial</strong></font></td>
        </tr>
        {section name=i loop=$disp}
        <tr>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].tipo|default:"No especificado"}</font></td>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].ubic|default:"No especificado"}</font></td>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].tecn|default:"No especificado"}</font></td>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].marc|default:"No especificado"}</font></td>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].mode|default:"No especificado"}</font></td>
          <td class="textogray"><font size="1" face="Arial">{$disp[i].seri|default:"No especificado"}</font></td>
		  <td class="textogray"><font size="1" face="Arial">{$disp[i].codPatri|default:"No especificado"}</font></td>
        </tr>
        {sectionelse}
        <tr>
          <td colspan="6" class="textogray"><font size="1" face="Arial">A&uacute;n no se han agregado Dispositivos</font></td>
        </tr>
        {/section}
    </table>
	</td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><font size="1" face="Arial">&nbsp;</font></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
  </tr>
  <tr bgcolor="#EAEAEA">
    <td width="100%" colspan="2"><font size="2" face="Arial"><strong>SOFTWARE</strong></font></td>
  </tr>
  <tr align="center">
	<td colspan="2" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td class="textogray"><strong><font size="1" face="Arial">Descripci&oacute;n</font></strong></td>
        </tr>
        {section name=i loop=$soft}
        <tr>
          <td width="10">&nbsp;</td>
          <td class="textogray"><font size="1" face="Arial">{$soft[i].desc|default:"No especificado"}</font></td>
        </tr>
        {sectionelse}
        <tr>
          <td width="10">&nbsp;</td>
          <td class="textogray"><font size="1" face="Arial">A&uacute;n no se ha agregado Software</font></td>
        </tr>
        {/section}
    </table></td>
  </tr>
  <tr align="center">
    <td colspan="2" width="100%"><font size="1" face="Arial">&nbsp;</font></td>
  </tr>
</table>
</blockquote>
</blockquote>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="1" face="Arial"><i>Generado el {$fechaGen}</i></font></p>
</body>
</html>