<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<br>
<table width="100%" height="40" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2"><img src="/img/800x600/line.single.solid1px.jpg" width="100%" height="1"></td>
  </tr>
  <tr>
    <td width="100">&nbsp;</td>
    <td>
    <font face="Arial" color="#000000" size="2"><b>{$pcName} ({$pcCod}) -{if $pcDep || $pcTrab} {$pcTrab} ({$pcDep}) -{/if} Historial de Atenciones </b></font><br></td>
  </tr>
  <tr>
    <td colspan="2"><img src="/img/800x600/line.doble.solid1px.jpg" width="100%" height="3"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="20%"><b><font size="2" face="Arial">N&deg; T&eacute;cnico</font></b></td>
    <td width="50%"><b><font size="2" face="Arial">Apellidos y Nombres </font></b></td>
    <td width="30%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><img src="/img/800x600/line.single.solid1px.jpg" width="100%" height="1"></td>
  </tr>
</table>
{section name=u loop=$ate.users}
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="20%"><font size="2" face="Arial">{$smarty.section.u.iteration}</font></td>
    <td width="50%"><font size="2" face="Arial">{$ate.users[u].name|capitalize}</font></td>
    <td width="30%">&nbsp;</td>
  </tr>
</table>
<blockquote>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td><blockquote>
        <p><b><font size="2" face="Arial"><br>
          Atenciones Realizadas: </font></b></p>
    </blockquote></td>
  </tr>
  <tr>
    <td>
      <table width="1270" border="0" cellpadding="4" cellspacing="0">
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
        </tr>
        <tr align="center" bgcolor="#EAEAEA">
          <td width="2%"><font color="#000000" size="1" face="Arial"><b>N&deg;</b></font></td>
          <td width="40%"><font color="#000000" size="1" face="Arial"><b>Motivos</b></font></td>
          <td width="30%"><font color="#000000" size="1" face="Arial"><b>Observaci&oacute;n</b></font></td>
          <td width="23%"><font color="#000000" size="1" face="Arial"><b>Fecha Inicio-Fin </b></font></td>
          <td width="5%"><font color="#000000" size="1" face="Arial"><b>Horas</b></font></td>
        </tr>
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
        </tr>
		{assign var="totalHoras" value=0.00}
  		{section name=a loop=$ate.users[u].ates}
        <tr>
          <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$smarty.section.a.iteration}</font></td>
          <td bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].moti}</font></td>
          <td bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].obse}</font></td>
          <td align="right" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].fech}</font></td>
          <td align="right" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].tota|string_format:"%.2f"}</font></td>
        </tr>
		{math equation="n+m" n=$totalHoras m=$ate.users[u].ates[a].tota assign='totalHoras'}
		{if $smarty.section.a.last}
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF" colspan="4" align="right"><font color="#000000" size="1" face="Arial"><strong>Total de Horas acumuladas</strong></font></td>
          <td bgcolor="#FFFFFF" align="right"><font color="#000000" size="1" face="Arial"><b>{$totalHoras|string_format:"%.2f"}</b></font></td>
        </tr>
		{/if}
		{sectionelse}
        <tr>
          <td colspan="5" align="center" bgcolor="#FFFFFF"> <font face="Arial" size="1" color="#000000">No hay atenciones realizadas.</font> </td>
        </tr>
		{/section}
        <tr align="center">
          <td colspan="5"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</blockquote>
 <br>
{/section}
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="1" face="Arial"><i>Generado el {$fechaGen}</i></font></p>
</body>
</html>