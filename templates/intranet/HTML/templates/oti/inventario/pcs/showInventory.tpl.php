<html>
<head>
<title>Intranet Produce Inventario de Equipos Informáticos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<link rel="stylesheet" href="/styles/suspEmb.css" type="text/css">
<script src="/js/intranet.js" language="JavaScript1.2" type="text/javascript"></script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="530" border="0" align="center" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF">
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="user" id="user"></a>USUARIO</strong></td>
    </tr>
    <tr> 
      <td nowrap class="item"><strong>Dependencia</strong></td>
      <td class="textogray">{$desDependencia}</td>
    </tr>
    <tr> 
      <td nowrap class="item"><strong>Trabajador</strong></td>
      <td class="textogray">{$desTrabajador}</td>
    </tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="maq" id="maq"></a>M&Aacute;QUINA</strong></td>
    </tr>
    <tr> 
      <td nowrap class="item"> <strong>C&oacute;digo Inventario&nbsp;</strong></td>
      <td width="420" class="textogray">{$codInventariado}</td>
    </tr>
    <tr> 
      <td valign="middle" class="item"> <strong>Nombre PC</strong></td>
      <td class="textogray">{$desNombre}</td>
	</tr>
    <tr> 
      <td class="item"><strong>Cod. Patrimonial</strong></td>
      <td class="textogray">{$codPatrimonial}</td>
	</tr>
    <tr> 
      <td class="item"> <strong>Serial</strong></td>
      <td class="textogray">{$desSerial}</td>
	</tr>
    <tr> 
      <td class="item"><strong>Tipo CPU</strong></td>
      <td class="textogray">{$desTipo}</td>
    </tr>
    <tr> 
      <td class="item"> <strong>Marca CPU</strong></td>
      <td class="textogray">{$desMarca}</td>
    </tr>
    <tr> 
      <td class="item"> <strong>Modelo CPU</strong></td>
      <td class="textogray">{$desModelo}</td>
    </tr>
    <tr> 
      <td class="item"> <strong>Desc. Modelo</strong></td>
      <td class="textogray">{$desTipo}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Fec. Ingreso</strong></td>
      <td class="textogray">{$numDiaIng}/{$numMesIng}/{$numAnyoIng}</td>
    </tr>
    <tr>
      <td class="item"><strong>Proveedor</strong></td>
      <td class="textogray">{$desEmpresa}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Garantia (Meses)</strong></td>
      <td class="textogray">{$numGaran}</td>
    </tr>
    <tr>
      <td class="item"><strong>Entrada</strong></td>
      <td class="textogray">{$desEntrada}</td>
    </tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="disp"></a>DISPOSITIVOS</strong></td>
    </tr>
    <tr> 
      <td colspan="2" class="item"> <table width="100%%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="textogray"><strong>Tipo</strong></td>
            <td class="textogray"><strong>Ubica</strong></td>
            <td class="textogray"><strong>Tecno</strong></td>
            <td class="textogray"><strong>Marca</strong></td>
            <td class="textogray"><strong>Modelo</strong></td>
          </tr>
          {section name=i loop=$disp} 
          <tr> 
            <td class="textogray">{$disp[i].tipo}</td>
            <td class="textogray">{$disp[i].ubic}</td>
            <td class="textogray">{$disp[i].tecn}</td>
            <td class="textogray">{$disp[i].marc}</td>
            <td class="textogray">{$disp[i].mode}</td>
          </tr>
          {sectionelse} 
          <tr> 
            <td colspan="5" class="textogray">A&uacute;n no se han agregado Dispositivos</td>
          </tr>
          {/section} </table></td>
    </tr>
    <tr> 
      <td colspan="2" class="item"> <table width="100%%" border="0" cellpadding="0" cellspacing="0" class="TEmb">
          <tr> 
            <td width="100%" colspan="4" background="/img/800x600/bg_horiz.gif" class="bg-med-rx"><img src="/img/800x600/bg_blank.gif" width="1" height="1" vspace="3"></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="2" class="item-sep"> <strong><a name="soft" id="soft"></a>SOFTWARE 
        </strong> </td>
    </tr>
    <tr> 
      <td colspan="2" class="item"><table width="100%%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
            <td class="textogray"><strong>Descripci&oacute;n</strong></td>
          </tr>
          {section name=i loop=$soft} 
          <tr> 
            <td width="10">&nbsp;</td>
            <td class="textogray">{$soft[i].desc}</td>
          </tr>
          {sectionelse} 
          <tr> 
            <td width="10">&nbsp;</td>
            <td class="textogray">A&uacute;n no se ha agregado Software</td>
          </tr>
          {/section} </table></td>
    </tr>
    <tr> 
      <td colspan="2" class="item"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="TEmb">
          <tr> 
            <td width="100%" colspan="3" background="/img/800x600/bg_horiz.gif" class="bg-med-rx"><img src="/img/800x600/bg_blank.gif" width="1" height="1" vspace="3"></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
  </table>
</body>
</html>