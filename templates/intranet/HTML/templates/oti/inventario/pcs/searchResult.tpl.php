<table width="490" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrInv} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
		  {if $ind_mod}
          <td width="27" align="center" valign="middle"><a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_PC}&id={$arrInv[i].id}&menu={$accion.FRM_BUSCA_PC}&subMenu={$accion.FRM_MODIFICA_PC}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar PC]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a></td>
		  {/if}
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr> 
                <td width="100" valign="top" class="item"><strong>Nombre PC:</strong></td>
                <td class="texto"><strong>{$arrInv[i].desc|default:'No especificado'}</strong></td>
                <td class="texto" align="right"><strong><font color="#FF0000">{$arrInv[i].tipEnt|default:'No especificado'}</font></strong></td>
                <td width="20" rowspan="5" class="texto"><a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_PC}&amp;id={$arrInv[i].id}','detallePC','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=1,top=0,left=0,width=560,height=400'); void('')" onMouseOver="window.status=''"><img src="/img/800x600/ico_print.gif" alt="[ Detalle e Impresión ]" width="17" height="17" hspace="5" border="0"></a></td>
                <td width="20" rowspan="5" class="texto"><a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_ATENCIONES_PC}&amp;id={$arrInv[i].id}','atencionesPC','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=1,top=0,left=0,width=560,height=400'); void('')" onMouseOver="window.status=''"><img src="/img/800x600/ico_doc.gif" alt="[ Ver atenciones ]" width="17" height="17" hspace="5" border="0"></a></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Cod. Inventario:</strong></td>
                <td colspan="2" class="texto">{$arrInv[i].cInv|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Cod. Patrimonial:</strong></td>
                <td colspan="2" class="texto">{$arrInv[i].cPat|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Trabajador:</strong></td>
                <td colspan="2" class="texto">{$arrInv[i].trab|default:'No especificado'}</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item"><strong>Dependencia:</strong></td>
                <td colspan="2" class="texto">{$arrInv[i].depe|default:'No especificado'}</td>
              </tr>
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
