{$jscript}
<!-- {literal}  -->

<script language="JavaScript">
function Guardar(){//se procede a enviar el formulario para ser guardado en la base de datos
	//almacenar_tel();
}


function AgregarTelefono(pForm){//se agrega un telefono
	var telefono = pForm.telefono.value;
	if(!(/^(\({1}\d{2,4}\){1})?([1-9]{1})(\d{6})$/.test(telefono))){//forma [(anexo)] telefono
		alert('Telefono Incorrecto');
		pForm.telefono.focus();
		return false;
	}
	var contenido;
	for(i=0;i< pForm.elements['telefonos[]'].options.length;i++){
		contenido=pForm.elements['telefonos[]'].options[i].text;
		if(contenido==telefono){
			pForm.telefono.value="";
		return false;
		}
	}
	pForm.elements['telefonos[]'].options[pForm.elements['telefonos[]'].options.length] = new Option(telefono,telefono,false,false);
	pForm.telefono.value="";
}
function EliminarTelefono(pForm){//se eliminan los telefonos seleccionados
	var lon;
	lon=pForm.elements['telefonos[]'].options.length-1;
	for(i=lon;i>=0;i--)
		if(pForm.elements['telefonos[]'][i].selected)
			pForm.elements['telefonos[]'].options.remove(i);
}
function SelectedTel(pForm,stat){
	if(stat)
		for(var i=0;i<pForm.elements['telefonos[]'].length;i++)
			pForm.elements['telefonos[]'][i].selected=true;
	return stat;
}
</script>
<!-- {/literal}  -->
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td width="140" height="25" valign="middle" bgcolor="#31698C" class="tree">&nbsp;&nbsp;<a href="/institucional/aplicativos/oti/directorio/index.php?accion=frmSearchEmpresa&menu=frmSearchEmpresa">Directorio de Empresas </a> </td>
    <td class="tree" bgcolor="#31698C" valign="middle"><div align="center">|</div></td>
    <td valign="middle" bgcolor="#31698C" class="treeOUT"><a href="#" class="treeOUT">Agregar Empresa </a></td>
  </tr>
  <tr>
    <td colspan="3" class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="tree">&nbsp;</td>
  </tr>
</table>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('desNom','Nombre','R','codRubro','Rubro','Sel','direccion','Direcci�n','R','codDistrito','Distrito','Sel');this.bSubmit.disabled=document.MM_returnValue;return SelectedTel(document.{$frmName},document.MM_returnValue);">
  <table width="490" border="0" align="center" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
    {if $errors} 
    <tr> 
      <td colspan="2" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="contenido-intranet" align="right"> Nombre</td>
      <td align="left" class="lnk-more-new"> <input name="desNom" type="text" class="ip-login contenido" id="desNombre" value="{$desNom}" size="35" maxlength="50"> <strong>(*)</strong>
      </td>
    </tr>
	<tr> 
      <td class="contenido-intranet" align="right"> Rubro</td>
      <td align="left" class="lnk-more-new"><select name="codRubro" class="ip-login contenido" id="rubro" >
	{$selRubro}
        </select> <strong>(*)</strong> </td>
    </tr>
    <tr> 
      <td class="contenido-intranet" align="right"> Direcci&oacute;n</td>
      <td align="left" class="lnk-more-new"><input name="direccion" type="text" class="ip-login contenido" id="direccion"  value="{$direccion}" size="35" maxlength="50"> <strong>(*)</strong>
      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet" align="right">Distrito</td>
      <td align="left" class="lnk-more-new"><select name="codDistrito" class="ip-login contenido" id="distrito">
	{$selDistrito}
        </select> <strong>(*)</strong></td>
    </tr>
    <tr> 
		  <td rowspan="2" valign="top" class="contenido-intranet" align="right">Tel&eacute;fono</td>
		  <td align="left" class="lnk-more-new"> 
			<input name="telefono" type="text" class="ip-login contenido" id="telefono2" size="30" maxlength="7"> 
			<input type="button" class="submit" name="Submit9" value="+" onClick="AgregarTelefono(document.{$frmName});"> 
			<input type="button" class="submit" name="Submit10" value="-" onClick="EliminarTelefono(document.{$frmName});">
			<strong>(*)</strong></td>
	</tr>
		  <td align="left"> 
			<select name="telefonos[]" size="4" multiple id="select" class="ip-login contenido">
		    </select>
		  </td>
    	
    <tr> 
      <td height="25" class="contenido-intranet" align="right">Fax</td>
      <td align="left"><input name="fax" type="text" id="fax" size="35" maxlength="7" value="{$fax}" class="ip-login contenido"></td>
    </tr>
    <tr> 
      <td height="25" class="contenido-intranet" align="right">Email</td>
      <td align="left"><input name="email" type="text" id="email" size="35" maxlength="50"  value="{$email}" class="ip-login contenido"> 
      </td>
    </tr>
	<tr>
	   
      <td height="25" class="contenido-intranet" align="right">WebSite</td>
	   <td align="left"><input name="web" type="text" id="web" size="35" maxlength="30" value="{$web}" class="ip-login contenido"></td>
	</tr>
	<tr>
		<td height="25" class="contenido-intranet" align="right">Observaci&oacute;n</td>
		<td align="left"><input name="observacion" type="text" id="text" size="35" maxlength="255" value="{$observacion}" class="ip-login contenido"></td>
	</tr>
	
	<tr>
	  <td height="25" class="contenido-intranet" align="right"><font color="#990000">(*)</font></td>
	  <td align="left"><font color="#990000">Campos obligatorios</font></td>
    </tr>
    <tr> 
      <td colspan="2"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="2"> <input name="bSubmit" type="Submit" class="submitintranet" value="Agregar Empresa"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitintranet" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_EMPRESA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> </td>
    </tr>
  </table>
</form>