<table width="490" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20"><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
      <td> {section name="i" loop=$arrEmp} 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
            
			<td width="24" align="center"><table width="100%"  border="0">
              <tr>
                <td onClick="JavaScrip:mywin=open('frmDelete.php','tar','width=350 ,height=450,menubar=0,scrollbars=1,directories=0,top=220,left=330,location=0,Titlebar=yes,resizable=no,status=0,toolbar=no');" style="cursor:hand">Eli</td>
              </tr>
              <tr>
                <td>{if $ind_mod}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_EMPRESA}&id={$arrEmp[i].id}&menu={$accion.FRM_BUSCA_EMPRESA}&subMenu={$accion.FRM_MODIFICA_EMPRESA}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar Datos de Empresa ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{else}&nbsp;{/if}</td>
              </tr>
            </table></td>
          <td class="texto">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="80" class="contenido-intranet" align="left">Nombre:</td>
                <td width="100%" class="contenido-intranet"><strong>{$arrEmp[i].desc}</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet" align="left">Rubro:</td>
                <td class="contenido-intranet" align="left">{$arrEmp[i].rubro}</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet" align="left"> Direcci&oacute;n:&nbsp;&nbsp;</td>
                <td class="contenido-intranet" align="left">{$arrEmp[i].dir|default:'No Especificada'}</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet" align="left">Distrito:<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
                <td valign="top" class="contenido-intranet" align="left">{$arrEmp[i].dis}</td>
              </tr>
			  <tr>
			    <td valign="top" class="sub-item-intranet" align="left">Email: </td>
		        <td valign="top" class="contenido-intranet" align="left"><a href="mailto:{$arrEmp[i].mail|default:'No Especificado'}">{$arrEmp[i].mail|default:'No Especificado'}</a></td>
			  </tr>
			  <tr>
			  	<td valign="top" class="sub-item-intranet" align="left">WebSite: <strong>&nbsp;&nbsp;&nbsp;</strong></td>
			    <td valign="top" class="contenido-intranet"><a href="http://{$arrEmp[i].web|default:'No Especificado'}" target="_blank">{$arrEmp[i].web|default:'No Especificado'}</a><strong>&nbsp;</strong></td>
			  </tr>
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>