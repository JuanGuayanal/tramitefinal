{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('desNom','Nombre','R','desApel','Apellidos','R','codEmpresa','Empresa','Sel','codArea','Area','Sel');this.bSubmit.disabled=document.MM_returnValue;return document.MM_returnValue">
  <table width="490" border="0" align="center" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
    {if $errors} 
    <tr> 
      <td colspan="2" class="item">{$errors}</td>
    </tr>
    {/if} 
    <tr> 
      <td width="123" class="item"> <strong>Nombres</strong></td>
      <td width="347"> <input name="desNom" type="text" class="iptxt1" id="desNombre" onKeyPress="if ((event.keyCode > 32 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97)) event.returnValue = false;" value="{$desNom}" size="35" maxlength="255"> 
      </td>
    </tr>
	<tr> 
      <td class="item"> <strong>Apellidos</strong></td>
      <td><input name="desApel" type="text" class="iptxt1" id="desNombre2" onKeyPress="if ((event.keyCode > 32 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97)) event.returnValue = false;" value="{$desApel}" size="35" maxlength="255">
	  </td>
    </tr>
    <tr> 
      <td class="item"> <strong>Empresa</strong></td>
      <td> 
        <select name="codEmpresa" class="ipsel2" id="codEmpresa" >
	{$selEmpresa}
        </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>&Aacute;rea</strong></td>
      <td><select name="codArea" class="ipsel1" id="select">
	{$selArea}
        </select></td>
    </tr>
    
    
    <tr> 
      <td height="25" class="item" ><strong>Celular</strong></td>
      <td><input name="celular" type="text" id="celular" size="35" maxlength="9" 
	  onKeyPress="{literal}
	  if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 47 || event.keyCode > 77) && (event.keyCode < 42 || event.keyCode > 42) && (event.keyCode < 35 || event.keyCode > 35)) {
	  	event.returnValue = false;	  
	  }
	  {/literal}"
	   value="{$celular}" class="iptxt1"></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Email</strong></td>
      <td><input name="email" type="text" id="email" size="35" maxlength="150"  value="{$email}" class="iptxt1"> 
      </td>
    </tr>
	<tr>
	   <td height="25" class="item"><strong>Tel&eacute;fono Fijo </strong></td>
	   <td><input name="anexo" type="text" id="anexo" size="35" maxlength="15" onKeyPress="{literal}if ((event.keyCode < 45 || event.keyCode > 57)) {event.returnValue = false;}else{ if(event.keyCode==47){event.returnValue = false;}}{/literal}" value="{$anexo}" class="iptxt1"></td>
	</tr>
    <tr> 
      <td colspan="2"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="2"> 
	  	<input name="bSubmit"  type="Submit" class="submit" value="Modifica Contacto" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_CONTACTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="id" type="hidden" id="id" value="{$id}">
		 </td>
    </tr>
  </table>
</form>