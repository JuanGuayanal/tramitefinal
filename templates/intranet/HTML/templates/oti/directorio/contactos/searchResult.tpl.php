<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20"><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
      <td> {section name="i" loop=$arrCont} 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td width="24" align="center">{if $ind_mod}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_CONTACTO}&id={$arrCont[i].id}&menu={$accion.FRM_BUSCA_CONTACTO}&subMenu={$accion.FRM_MODIFICA_CONTACTO}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar Datos del Contacto ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{else}&nbsp;{/if}</td>
          <td class="texto">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td class="contenido-intranet" align="left">Nombres:</td>
                <td width="80%" class="contenido-intranet" align="left"><strong>{$arrCont[i].desc}</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet" align="left">Empresa:</td>
                <td class="contenido-intranet" align="left">{$arrCont[i].empr}</td>
              </tr>
              <tr> 
                <td valign="top" nowrap class="sub-item-intranet" align="left">&Aacute;rea:&nbsp;&nbsp;</td>
                <td class="contenido-intranet" align="left">{$arrCont[i].area} </td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet" >&nbsp;</td>
                <td valign="top" class="sub-item-intranet" align="left">Celular: {$arrCont[i].celu}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Email:</strong> {$arrCont[i].mail} <strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong> <strong>Tel. Fijo:</strong> {$arrCont[i].fijo} </td>
              </tr>
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>