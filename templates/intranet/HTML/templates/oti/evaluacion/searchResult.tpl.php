<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registro de Visitas en el Ministerio de la Producci&oacute;n</title>
</head>

<body>
<table width="98%" border="0" align="center">
  <tr>
    <td align="center" class="textoblack">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="textoblack">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="textoblack"><B>EVALUACI&Oacute;N MENSUAL DE PERSONAL - INCENTIVO ECON&Oacute;MICO LABORAL</B></td>
  </tr>
  <!--
  <tr>
            <td align="center" class="textoblack">Nombre del Trabajador: <b> {$nombreX}</b></td>
  </tr>
  -->
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="95%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
   <td rowspan="2" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>N&deg;</strong></td>
   <td rowspan="2" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>NOMBRE</strong><strong></strong></td>
   <td rowspan="2" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>A&Ntilde;O</strong><strong></strong></td>
   <td rowspan="2" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>MES</strong><strong></strong></td>
   <td colspan="3" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>CALIFICACI&Oacute;N DE CONCEPTOS</strong><strong></strong></td>
    <td rowspan="2" align="center" nowrap="nowrap" class="item-sep-visitas" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>NOTA<br />
         DEL MES</strong><strong></strong></td>    
  </tr>
    <tr>
   <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>ACTITUD<br />
   PERSONAL</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>CONOCIMIENTO<br />
    DEL CARGO</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>EFICIENCIA</strong></td>
    </tr>
  {section name=i loop=$visi}
  <tr bgcolor="#FFFFF2">
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$smarty.section.i.iteration}</td>
            <td align="left" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].nombre}</td>
            <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].anio}</td>
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].nombre_mes}</td>
            <td nowrap="nowrap" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].calif_personal}</td>
             <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].calif_cargo}</td>
   <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB"> {$visi[i].calif_eficiencia} </td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].calif_mes}</td>
            
            
           
            <!--
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="right">{$visi[i].hora_total}</td>
            <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_diarias}</td>
            <td  class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].nro_semana}</td>
            <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_semanales}</td>
            -->
  </tr>
  

  {sectionelse}
  <tr>
    <td colspan="13" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="13" class="textograyintranet">No hay registros de evaluación.</td>
  </tr>
  {/section}
      <tr>
    <td colspan="13" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="13" class="textoblack"><b>Aplicativo desarrollado por OTI.</b></td>
  </tr>

  <tr>
            <td colspan="13" class="textograyintranet">&nbsp;</td>
  </tr>
</table>
</body>
</html>
