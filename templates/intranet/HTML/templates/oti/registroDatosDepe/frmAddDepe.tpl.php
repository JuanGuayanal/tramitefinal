{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarFecNacTrab.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>

<script language="JavaScript">
<!--
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}

function ninguna_letra(){
	event.returnValue = false;
}

-->
</script>
<!-- {/literal} -->
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp; Datos de la Dependencia </td>
  </tr>
  <tr>
    <td class="contenido-intranet">&nbsp;</td>
  </tr>
  <tr>
    <td class="contenido-intranet"> ::<a href="/institucional/informatica/registroDatosTrab/index.php?accion=frmSearchDepe&subMenu=frmSearchDepe&menu=frmSearchDepe"><b>BUSCAR DEPENDENCIA </b></a>::</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
</table>
	<form action="{$frmUrl}" method="post" name="{$frmName}">
  <br>
  <table width="600" border="0" align="center" cellpadding="0" cellspacing="1">
        {if $errors} 
    <tr> 
      <td colspan="2" class="item">{$errors} </td>
    </tr>
    {/if} 
	<tr>
      <td> 
        <table width="100%" align="center">
          <tr> 
            <td width="120" class="contenido-intranet" ><strong>Tipo de Dependencia</strong></td>
            <td class="item"><select name="tipDependencia" id="tipDependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_DEPENDENCIA}')">
			{$selTipoDependencia}
      </select></tr>
          <tr> 
            <td width="120" class="contenido-intranet" ><strong>Categor&iacute;a</strong></td>
            <td class="item"><select name="categoriaDepe" id="categoriaDepe" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_DEPENDENCIA}')">
			{$selCategoriaDepe}
      </select></tr>	  
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Dependencia</strong></td>
            <td><input name="nombreDependencia" type="text" class="ip-login contenido" value="{$nombreDependencia}" size="40" maxlength="100"></td>
          </tr>
          <tr class="item"> 
            <td width="75" class="contenido-intranet"><b>Siglas</b></td>
            <td><input name="siglasDependencia" type="text" class="ip-login contenido" value="{$siglasDependencia}" size="30" maxlength="100"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><b>Condici&oacute;n</b></td>
				<td ><select name="condicion" id="tipAte" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_DEPENDENCIA}')">
        <option value="ACTIVO"{if $condicion=="ACTIVO"} selected{/if}>Activo</option>
        <option value="INACTIVO"{if $condicion=="INACTIVO"} selected{/if}>Inactivo</option>
      </select></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Email</strong></td>
            <td height="3"><input name="email" type="text" class="ip-login contenido"  value="{$email}" size="30" maxlength="50"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Secci&oacute;n</strong></td>
            <td ><select name="seccion" id="trabajador" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_DEPENDENCIA}')">
        <option value="P"{if $seccion=="P"} selected{/if}>P</option>
        <option value="I"{if $seccion=="I"} selected{/if}>I</option>
		  		</select></td>
          <tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Piso</strong></td>
            <td ><input name="piso" type="text" class="ip-login contenido"  value="{$piso}" size="15" maxlength="2" onKeyPress="solo_num();"></td>
          </tr>		   
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Directo</strong></td>
            <td ><input name="directo" type="text" class="ip-login contenido"  value="{$directo}" size="15" maxlength="15" onKeyPress="solo_num();"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Anexo</strong></td>
            <td ><input name="anexo" type="text" class="ip-login contenido"  value="{$anexo}" size="15" maxlength="5" onKeyPress="solo_num();"></td>
          </tr>	
        </table></td>
    </tr>
  </table>

  <br>
  <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
    <tr> 
      <td><div align="center"> 
          <input type="submit" class="submitintranet" value="Agregar" onClick="MM_validateForm('tipDependencia','Tipo de Dependencia','Sel','nombreDependencia','Nombre Dependencia','R','siglasDependencia','Siglas Dependencia','R','email','Email','R','anexo','Anexo','R');return document.MM_returnValue">
        </div></td>
      <td><div align="center"> 
          <input name="cancel" type="button" class="submitintranet" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.BUSCA_DEPENDENCIA}');return document.MM_returnValue">
        </div></td>
		<input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_DEPENDENCIA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
    </tr>
  	</table>
	</form>