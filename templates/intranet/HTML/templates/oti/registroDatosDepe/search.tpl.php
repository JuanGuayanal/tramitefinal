{$jscript}
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = "tr-check";
}
{/literal}
-->
</script>
<br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp; Datos de la Dependencia </td>
  </tr>
  <tr>
    <td class="contenido-intranet">&nbsp;</td>
  </tr>
  <tr>
    <td class="contenido-intranet"> ::<a href="/institucional/informatica/registroDatosTrab/index.php?accion=frmAddDepe&subMenu=frmAddDepe&menu=frmSearchDepe"><b>AGREGAR NUEVA DEPENDENCIA</b></a>::</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
</table>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page2">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">  
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DEPENDENCIA}">
  <table width="490" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="6" class="textoblack">&nbsp;</td>
      <td width="150" class="contenido-intranet"><strong>Nombre</strong></td>
      <td width="217" valign="middle"><input name="nombreDependencia" type="text" class="ip-login contenido" value="{$nombreDependencia}" size="25" maxlength="100"></td>
      <td width="120" valign="middle"> <input type="submit" class="submitintranet" onClick="document.{$frmName}.page.value=''" value="Listar">      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet"><strong>Status</strong></td>
      <td colspan="2" valign="middle"><select name="status" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_DEPENDENCIA}')">
        <option value="T"{if $status=="T"} selected{/if}>Todos</option>
        <option value="ACTIVO"{if $status=="ACTIVO"} selected{/if}>Activo</option>
        <option value="INACTIVO"{if $status=="INACTIVO"} selected{/if}>Inactivo</option>
      </select></td>
    </tr>
  </table>
</form>
