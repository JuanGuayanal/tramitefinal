<table width="535" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrDepe} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="20" align="center" valign="middle"><a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_DEPENDENCIA}&id={$arrDepe[i].id}&menu={$accion.FRM_BUSCA_DEPENDENCIA}&subMenu={$accion.FRM_MODIFICA_DEPENDENCIA}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a></td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr>
                <td width="15%" valign="top" class="contenido-intranet"><strong>Dependencia</strong></td>
                <td class="contenido-intranet"><strong>:</strong><span class="texto">{$arrDepe[i].sigla|default:'No 
                  especificado'}</span><strong> <br>
                  {$arrDepe[i].est} {$arrDepe[i].nomDepe|default:'No especificado'}</strong></td>
                <td class="texto" align="right"><!--<img src="/img/imagen4.gif" width="15" height="12" >--><strong><font color="#FF0000">{$arrDepe[i].cond}&nbsp;&nbsp;&nbsp;&nbsp;</font></strong>				</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet">Tipo</td>
                <td colspan="2" class="contenido-intranet"><strong>:</strong>{$arrDepe[i].tipDepe|default:'No 
                  especificado'}&nbsp;&nbsp;&nbsp;Telef.: {$arrDepe[i].telef|default:'No 
                  especificado'} Secci&oacute;n {$arrDepe[i].seccion|default:'No 
                  especificado'}</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item-intranet">Creado por </td>
                <td colspan="2" class="sub-item-intranet"><strong>:{$arrDepe[i].userCrea|default:'No especificado'} {$arrDepe[i].auditCrea|default:'No especificado'}&nbsp;&nbsp;
				{if ($arrDepe[i].auditCrea!=$arrDepe[i].auditMod)}Modificado: {$arrDepe[i].userMod|default:'No especificado'} {$arrDepe[i].auditMod|default:'No especificado'}{/if}</strong></td>
              </tr>			  
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
