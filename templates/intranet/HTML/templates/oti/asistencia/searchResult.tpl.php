<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registro de Visitas en el Ministerio de la Producci&oacute;n</title>
</head>

<body>
<form name="{$frmName}" action="{$frmUrl}" method="post" target="_blank">
	<input type="hidden" name="tipo" value="{$tipo}">
	<input type="hidden" name="campo_fecha" value="{$campo_fecha}">
	<input type="hidden" name="campo_fecha2" value="{$campo_fecha2}">
	<input type="hidden" name="print" value="1">
	<input type="hidden" name="nombreVisitante" value="{$nombreVisitante}">
	<input type="hidden" name="dniVisitante" value="{$dniVisitante}">
	<input type="hidden" name="funcionario" value="{$funcionario}">
	<input type="hidden" name="horInicio" value="{$horInicio}">
	<input type="hidden" name="horFin" value="{$horFin}">
	<input type="hidden" name="MAILtRABAJADOR" value="{$MAILtRABAJADOR}">
	<input name="accion" type="hidden" id="accion" value="{$accion.IMPRIME_VISITA}">
<table width="98%" border="0" align="center">
  <tr>
    <td colspan="2">
    <!--
    <a href="JavaScript:OpenWin3('index.php?accion={$accion.IMPRIME_VISITA}&tipo={$tipo}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}&print=1&nombreVisitante={$nombreVisitante}&dniVisitante={$dniVisitante}&funcionario={$funcionario}&horInicio={$horInicio}&horFin={$horFin}&MAILtRABAJADOR={$MAILtRABAJADOR}','pag')" class="Estilo4"><img src="/img/800x600/bot_detalle_asistencia.gif" width="150" height="20" hspace="2" vspace="2" border="0" align="absmiddle" alt="Ver detalle de tu registro de asistencia"></a>
    -->
    
    <!--<strong><a href="{$frmUrl}?accion={$accion.IMPRIME_VISITA}&tipo={$tipo}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}&print=1&nombreVisitante={$nombreVisitante}&dniVisitante={$dniVisitante}&funcionario={$funcionario}&horInicio={$horInicio}&horFin={$horFin}&MAILtRABAJADOR={$MAILtRABAJADOR}" target="_blank"><img src="/img/800x600/bot_detalle_asistencia.gif" width="150" height="20" hspace="2" vspace="2" border="0" align="absmiddle" alt="Ver detalle de tu registro de asistencia"></a></strong>-->
    <input type="submit" name="bSubmit" value="Ver Detalle Completo" class="submitintranet" />    </td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textoblack">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textoblack"></td>
  </tr>
  <tr>
    <td align="center" class="textoblack"><B>CONTROL DE ASISTENCIA DEL PERSONAL</B></td>
    <td rowspan="2" align="center" class="textoblack">
          <img src="/institucional/misce/directorio/fotosTrab/{$dniTrabajador}.jpg" border="0" alt="" width="60" height="77" />    </td>
  </tr>
  <tr>
            <td align="center" class="textoblack">Nombre del Trabajador: <b> {$nombreX}<br />
            Periodo del: <b>{$campo_fecha}</b> al <b>{$campo_fecha2}</b></b></td>
    </tr>
</table>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="2">
  <!--
  <tr>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" class="textograyvisitas" align="right" ><b>Hora de Entrada:</b></td>
            <td nowrap="nowrap"  align="center" >{$visi[i].hora_fijada}</td>
            <td nowrap="nowrap" align="center">&nbsp;</td>
  </tr>
  -->
  
  <tr>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>            
            <td nowrap="nowrap" align="center" class="textoblack"><b>Hora de Entrada:</b></td>
            <td nowrap="nowrap" align="center" class="textored"> <b> {$horaFijada}</b></td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
  </tr>
  <tr>
   <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>N&deg;</strong></td>
   <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DIA</strong></td>
   <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>FECHA</strong></td>
   <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA INGRESO <br />
              REGISTRADA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA 
              SALIDA<br />
              REGISTRADA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS DIARIAS<br />
    ESTABLECIDAS</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS<br />
    SEMANALES</strong></td>    
    <!--
         
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS DIARIAS<br />
    REGISTRADAS</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS DIARIAS <br />
              ESTABLECIDAS</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>Nro. <br />
              SEMANA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>    HORAS <br />
              SEMANALES</strong></td>
   -->           
  </tr>
  {section name=i loop=$visi}
  <tr bgcolor="#FFFFF2">
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$smarty.section.i.iteration}</td>
            <td align="left" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].dia}</td>
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].fecha}</td>
            <td nowrap="nowrap" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].hora_ini}</td>
             <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].hora_fin}</td>
   <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB"> {if $visi[i].hora_ini!=""}{$visi[i].total_horas_diarias}{/if} </td>
    <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_semanales}</td>
            
            
           
            <!--
            <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="right">{$visi[i].hora_total}</td>
            <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_diarias}</td>
            <td  class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].nro_semana}</td>
            <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_semanales}</td>
            -->
  </tr>
  

  {sectionelse}
  <tr>
    <td colspan="12" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="12" class="textograyintranet">No hay registros de asistencia.</td>
  </tr>
  {/section}
      <tr>
    <td colspan="12" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="12" class="textoblack"><b>El personal CAS verificará su asistencia diaria y  comunicará cualquier inconveniente que se presente.</b></td>
  </tr>

  <tr>
            <td colspan="12" class="textograyintranet">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
