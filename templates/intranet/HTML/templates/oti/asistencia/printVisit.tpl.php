<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registro de Visitas en el Ministerio de la Producci&oacute;n</title>
<link href="/styles/intranet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#000000">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF"><img src="/img/800x600/logo-CONVENIO_SITRADOC.jpg" width="175" height="49"></td>
          <td align="right" class="textogray" bgcolor="#FFFFFF">Servicio prestado por la Intranet Institucional del Ministerio de la Producci&oacute;n&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr> 
          <!--
		  <td class="texto-servicios-white"><strong><a href="{$frmUrl}?id={$emb.id}&accion={$accion.IMPRIME_DETALLE}"><img src="/img/apps/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong>&nbsp;&nbsp;&nbsp;<strong><a href="javascript:window.close()"><img src="/img/apps/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            </strong></td>-->
			<td class="texto-servicios-white"><strong><a href="javascript:print()"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong>&nbsp;&nbsp;&nbsp;<strong><a href="javascript:window.close()"><img src="/img/800x600/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            &nbsp;&nbsp;&nbsp;
            <!---
            <strong><a href="{$frmUrl}?accion={$accion.IMPRIME_VISITA}&tipo={$tipo}&campo_fecha={$campo_fecha}&campo_fecha2={$campo_fecha2}&print=1&nombreVisitante={$nombreVisitante}&dniVisitante={$dniVisitante}&funcionario={$funcionario}&horInicio={$horInicio}&horInicioP={$horInicioP}&horFin={$horFin}&horFinP={$horFinP}&print=3" target="_blank"><img src="/img/800x600/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Descargar en Excel</a>&nbsp; 
            </strong>
            --></td>
          <td align="right" class="texto-servicios-white"><!--<strong><a href="javascript:window.close()"><img src="/img/apps/ico-close.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Cerrar</a>&nbsp; 
            </strong>-->            </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="textoblack"><b>REGISTRO DE ASISTENCIA DEL PERSONAL</b></td>
  </tr>
  <tr>
            <td align="center" class="textoblack"><b> Nombre del Trabajador: {$nombreX}</b></td>
  </tr>
  <tr>
            <td align="center" class="textoblack"><b>Periodo del: {$campo_fecha} al {$campo_fecha2}</b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="75%" border="0" align="center" cellpadding="0" cellspacing="1">
  <tr>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" class="textoblack"><b>Hora de Entrada:</b></td>
            <td nowrap="nowrap" align="center" class="textored"> <b> {$horaFijada}</b></td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
            <td nowrap="nowrap"  align="center" >&nbsp;</td>
            <td nowrap="nowrap" align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>N&deg;</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>DIA</strong></td>
	<td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>FECHA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA<br />
              REGISTRADA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORA <br />
              SALIDA</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>OBSERVACIONES</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS DIARIAS<br />
    ESTABLECIDAS</strong></td>
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS<br />
    SEMANALES</strong></td>
    <!--
        
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>HORAS DIARIAS<br />
    REGISTRADAS</strong></td>
    
    <td nowrap="nowrap" class="item-sep-visitas" align="center" style="filter:progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FF8800', startColorstr='#FEBF01', gradientType='0')"><strong>Nro.<br />
    SEMANA</strong></td>
    
    -->
  </tr>
  {section name=i loop=$visi}
  <tr bgcolor="#FFFFF2">
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$smarty.section.i.iteration}</td>
	<td align="left" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].dia}</td>
	<td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].fecha}</td>
    <td nowrap="nowrap" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center"><!--{$visi[i].docu}: -->{$visi[i].hora_ini}</td>
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].hora_fin}</td>
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="center">{$visi[i].obs}</td>
    <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB"> {if $visi[i].hora_ini!=""}{$visi[i].total_horas_diarias}{/if} </td>
    <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_semanales}</td>
    <!--
        
    <td class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB" align="right">{$visi[i].hora_total}</td>
    
    <td align="center" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].nro_semana}</td>
    <td align="right" class="textograyvisitas" style="border-bottom: 1px solid #EBEBEB">{$visi[i].total_horas_semanales}
    <!---    {if ($visi[i].suma!=$visi[i.index_prev].suma)}{$visi[i].suma}{/if}--></td>
  </tr>

  {sectionelse}
  <tr>
    <td colspan="11" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="11" class="textograyintranet">No hay registro de asistencias.</td>
  </tr>
  {/section}
        <tr>
    <td colspan="12" class="textograyintranet">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="12" class="textoblack"><b>El personal CAS verificará su asistencia diaria y  comunicará cualquier inconveniente que se presente.</b></td>
  </tr>

  <tr>
            <td colspan="11" class="textograyintranet">&nbsp;</td>
  </tr>
</table>
</body>
</html>
