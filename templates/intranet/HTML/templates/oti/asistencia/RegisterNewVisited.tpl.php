<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
	<link href="/estilos/calendar-green.css" rel="stylesheet" type="text/css" />	
<link href="/styles/style_intranet.css" rel="stylesheet" type="text/css" />
	<SCRIPT src="estilos/calendar.js" type=text/javascript></SCRIPT>
	<SCRIPT src="estilos/calendar-es.js" 
	type=text/javascript></SCRIPT>
	<SCRIPT src="estilos/calendar-setup.js" 
	type=text/javascript></SCRIPT>
</head>

<body>
<form action="{$frmUrl}" method="post" name="{$frmName}">
<input name="accion" type="hidden" value="{$accion.BUSCA_VISITA}" />
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp;&nbsp;Sistema de Registro de Visitas</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="contenido-intranet">Permite registrar la hora de salida de los visitantes en las instalaciones del Ministerio de la Producci&oacute;n</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
</table>
<table width="95%" border="0" cellspacing="2" cellpadding="4" align="center" background="/img/800x600/bg_tab_encuesta_intranet.gif" class="tabla-login">
  <tr>
    <td><table width="70%" border="0" align="center">
      <tr>
        <td colspan="3" align="center" class="item-sep-intranet"><b>REGISTRO DE HORA DE SALIDA DE VISITANTE</b></td>
        </tr>
      <tr>
        <td>Nombres</td>
        <td colspan="2">{$nombresVisitante}</td>
      </tr>
      <tr>
        <td>Apellidos</td>
        <td colspan="2">{$apellidosVisitante}</td>
      </tr>
      <tr>
        <td>DNI N&uacute;mero</td>
        <td>{$dniVisitante}</td>
        <td>&nbsp;</td>
      </tr>
      
    </table>
      <br /></td>
  </tr>
  <tr>
    <td align="right" width="533"><input type="submit" value="Registrar Salida" name="boton1" class="submitintranet" />
    </td>
  </tr>
</table>
<br />
</form>
</body>
</html>
