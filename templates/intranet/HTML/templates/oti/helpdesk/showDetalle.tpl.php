<html>
<head>
<title>TRAMITE DOCUMENTARIO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.Estilo1 {color: #000000}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#000000">
  
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA" class="contenido"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          
        <td class="item">&nbsp;</td>
          
        <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
          </strong></td>
        </tr>
		
      </table></td>
  </tr>
  <tr> 
    <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        <tr> 
          <td align="left" bgcolor="#FFFFFF"><img src="/img/pie-logo.gif" width="124" height="30"></td>
          <td align="right" bgcolor="#FFFFFF"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          
        <td height="50" colspan="2" class="textoblack"><strong>{$titulo}</strong></td>
        </tr>
<tr>
      <td>
	  	<table width="600" border="1" align="center">
		  <tr>
			<td width="150" class="item"><div align="center"><strong>MARCA</strong></div></td>
			<td width="450" class="item">{$marca}</td>
		  </tr>
		  <tr>
			<td class="item"><div align="center"><strong>MODELO</strong></div></td>
			<td class="item">{$modelo}</td>
		  </tr>
		  <tr>
			<td class="item"><div align="center"><strong>NUM. SERIE</strong></div></td>
			<td class="item">{$serie}</td>
		  </tr>
		  <tr>
			<td class="item"><div align="center"><strong>COD. PC</strong></div></td>
			<td class="item">{$codPC}</td>
		  </tr>
		  <tr>
			<td class="item"><div align="center"><strong>COD. PATRIM</strong></div></td>
			<td class="item">{$codPat}</td>
		  </tr>
		  <tr>
			<td class="item"><div align="center"><strong>DEPENDENCIA</strong></div></td>
			<td class="item">{$dep}</td>
		  </tr>
		</table>
	  </td>
</tr>
  <tr> 
    <td class="texto"><div align="left">
      <p><strong><br>
              <span class="textogray"><span class="Estilo1">Presenta:</span>		  {$descrip}</span><br>
		      <br>
		      <span class="textogray"><span class="Estilo1">Se considera el Siguiente Diagn&oacute;stico:</span>		  {$diagnostico}</span><br>
		      <br>
		      <span class="textogray"><span class="Estilo1">Soluci�n del Problema(con costo referencial si fuera el caso):</span> {$sol}</span> </strong></p>
      <p>&nbsp;</p>
    </div></td>
  </tr>
  <tr> 
    <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        <tr> 
          <td align="left" bgcolor="#FFFFFF"><img src="/img/pie-logo.gif" width="124" height="30"></td>
          <td align="right" bgcolor="#FFFFFF"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>