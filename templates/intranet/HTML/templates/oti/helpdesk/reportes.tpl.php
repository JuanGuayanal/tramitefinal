{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript">
<!--

ie = document.all?1:0
function hL(E){
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = "tr-check";
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
-->
</script>
<!-- {/literal} -->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" target="_blank" onSubmit="MM_validateForm({if $GrupoOpciones1!=9}'desFechaIni','Fecha Inicial','R','desFechaFin','Fecha Final','R'{else}'mes','Mes','Sel'{/if});return document.MM_returnValue">
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.GENERAREPORTE}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <table width="490" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="7" class="textoblack">&nbsp;</td>
      <td width="55" rowspan="7" class="textoblack"><strong>Tipo de Reporte</strong></td>
      <td width="320" class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Atenciones por rubro </strong></label></td> 
      <td width="350" valign="middle" class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Atenciones por t&eacute;cnico </strong></label></td>
      <td width="30" rowspan="5" valign="middle"> <input type="submit" class="submit" value="Generar">      </td>
    </tr>
    <tr>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="4" {if ($GrupoOpciones1==4)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Hardware</strong></label></td>
      <td width="350" valign="middle" class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="3" {if ($GrupoOpciones1==3)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Software</strong></label></td>
    </tr>
    <tr>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="5" {if ($GrupoOpciones1==5)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Redes</strong></label></td>
      <td width="170" valign="middle" class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="6" {if ($GrupoOpciones1==6)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Mant. Cor</strong></label></td>
    </tr>
    <tr>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="7" {if ($GrupoOpciones1==7)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Otros</strong></label></td>
      <td width="170" valign="middle" class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="9" {if ($GrupoOpciones1==9)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Reporte Especial</strong></label></td>
    </tr>
    <tr>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="10" {if ($GrupoOpciones1==10)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTE}')">
              <strong>Informe T&eacute;nico</strong></label></td>
      <td width="170" valign="middle" class="texto td-encuesta"></td>
    </tr>
	{if ($GrupoOpciones1==9||$GrupoOpciones1==10)}
    <tr>
      <td>
	  {if $GrupoOpciones1==10}
	  <input name="nroInforme" type="text" class="ip-login contenido" size="10" maxlength="4" value="{$nroInforme}">{/if}
	  <!--<select name="tipReporte" class="ipsel2" >
        <option value="1"{if $tipreporte==1} selected{/if}>Reporte Tipo1</option>
        <option value="2"{if $tipreporte==2} selected{/if}>Reporte Tipo2</option>
        <option value="4"{if $tipreporte==4} selected{/if}>Rubro Hardware</option>
        <option value="3"{if $tipreporte==3} selected{/if}>Rubro Software</option>
        <option value="5"{if $tipreporte==5} selected{/if}>Rubro Redes</option>
        <option value="6"{if $tipreporte==6} selected{/if}>Rubro Mant. Correctivo</option>
        <option value="7"{if $tipreporte==7} selected{/if}>Rubro Otros</option>
        <option value="9"{if $tipreporte==9} selected{/if}>Reporte Especial</option>
        <!--<option value="10"{if $tipReporte==10} selected{/if}>Reporte Tipo2 V2</option>-->
      <!--</select>--></td>
      <td width="170" valign="middle">
	  	{if $GrupoOpciones1==9}
			<select name="mes" class="ipsel1">
        		{$selMes}
            </select>
        	<select name="anyo" class="ipsel1">
          		{$selAnyo}
            </select>
		{/if}
	  </td>
    </tr>
	{/if}
	{if ($GrupoOpciones1!=9 && $GrupoOpciones1!=10)}
    <tr>
      <td width="270"><input name="desFechaIni" type="text" class="iptxt1" id="desFechaIni" value="{$desFechaIni}" size="15" tabindex="4" onKeyPress="ninguna_letra();" />
    		<input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="{$datos.fecDesIni}" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
      <td valign="middle"><input name="desFechaFin" type="text" class="iptxt1" id="desFechaFin" value="{$desFechaFin}" size="15" tabindex="5" onKeyPress="ninguna_letra();" />
    <input name="fecDesembarqueFin" type="hidden" id="fecDesembarqueFin" value="{$datos.fecDesFin}" />
	 &nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
      <td width="40" valign="middle">&nbsp;</td>
    </tr>
	<tr>
	<td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
	<td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
	<td>&nbsp;</td>
	</tr>
	{/if}
  </table>
</form>
