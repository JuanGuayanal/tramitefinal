{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}">
  <br>
  <table width="531" border="0" align="center" cellpadding="0" cellspacing="1">
        {if $errors} 
    <tr> 
      <td colspan="2" class="item">{$errors} </td>
    </tr>
    {/if} 
	<tr>
      <td> 
        <table width="531" align="center">
          <tr> 
            <td width="75" class="item" ><strong>Dependencia</strong></td>
            <td class="item">
				{$dep} - {$codInternoPC}
			</tr>
          <tr>
            <td width="75" class="item" ><strong>Usuario</strong></td>
            <td class="item">{$usuario}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>Marca</strong></td>
            <td class="item">{$marca}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>Modelo</strong></td>
            <td class="item">{$modelo}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>Serie</strong></td>
            <td class="item">{$serie|default:'No especificado'}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>C&oacute;digo PC</strong></td>
            <td class="item">{$codPC}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>C&oacute;digo Patrimonial</strong></td>
            <td class="item">{$codPat}          
          </tr>
          <tr>
            <td width="75" class="item" ><strong>Referencias</strong></td>
            <td class="item"><input name="opcion" type="checkbox" id="checkbox" value="1" {if $opcion==1} checked {/if} onClick="submitForm('{$accion.FRM_AGREGA_INFTECNICO}')">
        &iquest;Va a responder alg&uacute;n documento ?</td>
          </tr>
		  {if $opcion==1}
          <tr>
            <td width="75" class="item" ><strong>Documentos</strong></td>
            <td class="item"><select name="documentos" class="ipsel2" id="select">
        			{$selDocumentos}
				</select>
          
          </tr>		  
		  {/if}
          <tr>
            <td width="75" class="item" ><strong></strong></td>
            <td class="item"><input name="opcionParte" type="checkbox" id="checkbox" value="1" {if $opcionParte==1} checked {/if} onClick="submitForm('{$accion.FRM_AGREGA_INFTECNICO}')">
        &iquest;Se crear&aacute; el informe t&eacute;cnico debido a la parte de la PC ?</td>
          </tr>
		  {if $opcionParte==1}
          <tr>
            <td width="75" class="item" ><strong>Documentos</strong></td>
            <td class="item"><select name="partePC" class="ipsel2" id="partePC">
        			{$selPartePC}
				</select>
          
          </tr>		  
		  {/if}		  		  
          <tr> 
            <td width="75" class="item"><strong>Presenta</strong></td>
            <td height="3"><textarea name="asunto" cols="70" rows="3" class="ip-login contenido">{if !$descrip}Sin Observaciones{else}{$descrip}{/if}</textarea></td>
          </tr>
          <tr> 
            <td width="75" class="item" ><strong>Se considera el siguiente Diagn&oacute;stico </strong></td>
            <td height="3"><textarea name="diagnostico" cols="70" rows="4" class="ip-login contenido">{if !$diagnostico}Sin Diagnóstico{else}{$diagnostico}{/if}</textarea></tr>
          <tr> 
          <tr> 
            <td width="75" valign="middle" class="item"><strong>Soluci&oacute;n del problema </strong></td>
            <td  height="3"><textarea name="sol" cols="70" rows="4" class="ip-login contenido">{if !$sol}Sin Observaciones{else}{$sol}{/if}</textarea> 
            </td>
          </tr>
        </table></td>
    </tr>
  </table>

  <br>
  <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
    <tr> 
      <td><div align="center"> 
          <input type="submit" class="but-login" value="Grabar" onClick="MM_validateForm('asunto','Presenta','R','diagnostico','Dianóstico','R','sol','Solución','R');return document.MM_returnValue">
        </div></td>
      <td><div align="center"> 
          <input name="cancel" type="button" class="but-login" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue">
        </div></td>
		<input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_INFTECNICO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="id" type="hidden" id="id" value="{$id}">
    </tr>
  	</table>
	</form>
