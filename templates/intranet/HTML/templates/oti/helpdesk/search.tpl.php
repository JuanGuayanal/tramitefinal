{$jscript}
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = "tr-check";
}
{/literal}
-->
</script>
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_ATENCION}">
  <table width="490" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="6" class="textoblack">&nbsp;</td>
      <td width="150" class="textoblack"><strong>C&oacute;digo de Inventario &oacute; 
        Nombre de PC</strong></td>
      <td width="217" valign="middle"> <select name="stt" class="ipsel2" onChange="submitForm('{$accion.BUSCA_ATENCION}')">
          <option value="T"{if $stt=="T"} selected{/if}>Todos</option>
          <!--<option value="I"{if $stt=="I"} selected{/if}>Inoperativo</option>-->
          <option value="O"{if $stt=="O"} selected{/if}>Atendido</option>
          <option value="P"{if $stt=="P"} selected{/if}>Pendiente</option>
        </select> <input name="nopc" type="text" class="iptxt1" value="{$nopc}" size="20" maxlength="100"> 
      </td>
      <td width="120" valign="middle"> <input type="submit" class="submit" onClick="document.{$frmName}.page.value=''" value="Listar"> 
      </td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>T&eacute;cnico</strong></td>
      <td colspan="2" valign="middle"> <select name="trabajador" id="trabajador" class="ipsel2" onChange="submitForm('{$accion.BUSCA_ATENCION}')">
			{$selTrabajador}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Dependencia</strong></td>
      <td colspan="2" valign="middle"> <select name="dependencia" id="dependencia" class="ipsel2" onChange="submitForm('{$accion.BUSCA_ATENCION}')">
			{$selDependencia}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Tipo de atenci&oacute;n</strong></td>
      <td colspan="2" valign="middle"> <select name="tipAte" id="tipAte" class="ipsel2" onChange="submitForm('{$accion.BUSCA_ATENCION}')">
			{$selTipAte}
			 </select></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Atenci&oacute;n</strong></td>
      <td colspan="2" valign="middle"> <select name="atencion" id="atencion" class="ipsel2" onChange="submitForm('{$accion.BUSCA_ATENCION}')">
			{$selAtencion}
			 </select></td>
    </tr>		
    <tr> 
      <td class="texto td-encuesta"><strong>Observaci&oacute;n</strong></td>
      <td colspan="2" valign="middle"><input name="observaciones" type="text" class="iptxt1" value="{$observaciones}" size="30" maxlength="100"></td>
    </tr>
  </table>
</form>
