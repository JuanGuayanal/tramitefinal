<table width="490" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrAte} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="27" align="center" valign="middle">{if $arrAte[i].flag=='P'}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_ATENCION}&id={$arrAte[i].id}&menu={$accion.FRM_MODIFICA_ATENCION}"><img src="/img/800x600/ico-mod.gif" alt="[ Cambiar de estado ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  <!--{if $arrAte[i].flag=='P'}<a href="javascript: window.open('{$frmUrl}?accion={$accion.IMPRIME_INF_TECNICO}&amp;id={$arrAte[i].id}&amp;tecnico={$arrAte[i].tec}','detallePC','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=1,top=0,left=0,width=560,height=400'); void('')" onMouseOver="window.status=''"><img src="/img/800x600/ico_print.gif" alt="[ Crear Informe T�cnico ]" width="17" height="17" hspace="5" border="0"></a>{/if}-->
		  {if $arrAte[i].flag=='P'&& $arrAte[i].seCreoInfTec!=1}<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_INFTECNICO}&id={$arrAte[i].id}&menu={$accion.FRM_AGREGA_INFTECNICO}"><img src="/img/800x600/ico-mod.gif" alt="[ Crear Informe T�cnico ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {if $arrAte[i].seCreoInfTec==1}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_INFTECNICO}&id={$arrAte[i].id}&menu={$accion.FRM_MODIFICA_INFTECNICO}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar Informe T�cnico ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  </td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr>
                <td valign="top" class="item"><strong>T&eacute;cnico</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].tec|default:'No 
                  especificado'}</td>
                <td class="texto" align="right">{if $arrAte[i].flag=='P'}<img src="/img/imagen4.gif" width="15" height="12" ><strong><font color="#FF0000">&iexcl;Pendiente!&nbsp;&nbsp;&nbsp;&nbsp;</font></strong>{/if}
					
				</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item"><strong>N&uacute;mero de PC</strong></td>
                <td colspan="2" class="texto"><strong>:{$arrAte[i].pc|default:'No especificado'}&nbsp;&nbsp;{$arrAte[i].depe|default:'No 
                  especificado'}&nbsp;&nbsp;&nbsp;{$arrAte[i].trab|default:'No 
                  especificado'}</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Atenci&oacute;n 1</strong></td>
                <td colspan="2" class="texto"><strong>:</strong>{$arrAte[i].tipate1|default:'No 
                  especificado'}:{$arrAte[i].ate1|default:'No especificado'}</td>
              </tr>
			  {if $arrAte[i].ate2!=''}
              <tr> 
                <td valign="top" class="sub-item"><strong>Atenci&oacute;n 2</strong></td>
                <td colspan="2" class="texto"><strong>:</strong>{if $arrAte[i].tipate2!=' 
                  '}{$arrAte[i].tipate2|default:'No especificado'}:{/if}{$arrAte[i].ate2|default:'No 
                  especificado'}</td>
              </tr>
			  {/if}
			  {if $arrAte[i].ate3!=''}
              <tr> 
                <td valign="top" class="sub-item"><strong>Atenci&oacute;n 3</strong></td>
                <td colspan="2" class="texto"><strong>:</strong>{if $arrAte[i].tipate3!=' 
                  '}{$arrAte[i].tipate3|default:'No especificado'}:{/if}{$arrAte[i].ate3|default:'No 
                  especificado'}</td>
              </tr>
			  {/if}
			  <!--
			  {if $arrAte[i].flag=='O'}
			  <tr> 
                <td valign="top" class="sub-item"><strong>Nro minutos:</strong></td>
                <td class="texto">{$arrAte[i].min|default:'No especificado'}</td>
              </tr>
			  {/if}
			  -->
			  {if $arrAte[i].flag=='P'}
			  <tr> 
                <td valign="top" class="sub-item"><strong>Tiempo</strong></td>
                <td colspan="2" class="texto"><strong>:</strong>{$arrAte[i].tiempo|default:'No 
                  especificado'}</td>
              </tr>
			  {/if}
			  <!--
			  {if $arrAte[i].flag=='P' && $arrAte[i].hora> 0 && $arrAte[i].dia== 0}
			  <tr> 
                <td valign="top" class="sub-item"><strong>Tiempo transcurrido</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].hora|default:'No 
                  especificado'}Hora(s)</td>
              </tr>
			  {/if}
			  {if $arrAte[i].flag=='P' && $arrAte[i].dia> 0}
			  <tr> 
                <td valign="top" class="sub-item"><strong>Tiempo transcurrido</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].dia|default:'No 
                  especificado'}D&iacute;a(s)</td>
              </tr>
			  {/if}
			  -->
			  <!--
              <tr>
                <td valign="top" class="sub-item"><strong>Trabajador:</strong></td>
                <td class="texto">{$arrAte[i].trab|default:'No especificado'}</td>
              </tr>
			  <tr>
                <td valign="top" class="sub-item"><strong>Dependencia:</strong></td>
                <td class="texto">{$arrAte[i].depe|default:'No especificado'}</td>
              </tr>
			  -->
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Hora de 
                  Inicio</strong></td>
                <td colspan="2" class="texto"><strong>:</strong>{$arrAte[i].horaini|default:'No 
                  especificado'}<strong>&nbsp;&nbsp;&nbsp;&nbsp;Hora de Fin:&nbsp;&nbsp;</strong>{$arrAte[i].horafin|default:'A�n 
                  no se concluye la Atenci�n'}<strong>{if $arrAte[i].flag=='O'}&nbsp;&nbsp;&nbsp;{$arrAte[i].min|default:'No 
                  especificado'}{/if}</strong></td>
              </tr>
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Observaciones</strong></td>
                <td colspan="2" class="texto"><strong>:{$arrAte[i].desc|default:'No especificado'}</strong></td>
              </tr>
			  {if $arrAte[i].seCreoInfTec==1}
              <tr> 
                <td colspan="3" class="texto"><strong><font color="#FF0000"><a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE}&id={$arrAte[i].id}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=400'); void('')" onMouseOver="window.status=''">&iexcl;Se ha creado un Informe T&eacute;cnico!</a></font></strong></td>
              </tr>
			  {/if}			  
			  <!--
              <tr> 
                <td width="100" valign="top" class="item"><strong>Hora de Fin:</strong></td>
                <td class="texto"><strong>{$arrAte[i].horafin|default:'A�n no se concluye la Atenci�n'}</strong></td>
              </tr>
			  -->
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
