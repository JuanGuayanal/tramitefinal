<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p align="center"><strong><font color="#265682" size="+4">INFORME T&Eacute;CNICO N&deg; 56-2007-CONVENIO_SITRADOC/OGTIE-Oti </font></strong></p>
<p align="right"><br> 
<font color="#265682" size="+3">San Isidro, {$fechaGen}</font></p>
<font color="#265682" size="+3"><strong>Se&ntilde;or<br>
  ELADIO PERCY SOL&Oacute;RZANO D&Iacute;AZ</strong><br>
  Director de la Oficina de Tecnolog&iacute;a de la Informaci&oacute;n (e) <br>
Presente.-</font>
<ul>
  <p><font size="+3">Tengo el agrado de saludarlo y a la vez comunicarle lo siguiente:</font></p>
</ul>
</p>
<p>
  <font color="#265682" size="+3"><strong>1 Que el equipo inform&aacute;tico con las caracter&iacute;sticas:</strong>
</font></p>
<table width="100%"  border="1">
  <tr>
    <td width="21%"><font color="#265682" size="+3">&nbsp;</font></td>
    <td width="79%" bgcolor="#808080" ><font color="#265682" size="+3">SCANNER</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">MARCA</font></td>
    <td><font color="#265682" size="+3">HP</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">MODELO</font></td>
    <td><font color="#265682" size="+3">Scanjet 5550c</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">NUM. SERIE </font></td>
    <td><font color="#265682" size="+3">CN3CRS71NQ</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">COD. PC </font></td>
    <td><font color="#265682" size="+3">0603</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">COD PATRIM.</font></td>
    <td><font color="#265682" size="+3">03327</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">DEPENDENCIA</font></td>
    <td><font color="#265682" size="+3">OFICINA DE COMUNICACION E IMAGEN INSTITUCIONAL</font></td>
  </tr>
</table>
<p>
<p>
<font color="#265682" size="+3"><strong>2 Presenta </strong></font>
<table width="100%"  border="1">
  <tr>
    <td><p><font size="+3">- Falla f&iacute;sica de hardware en el scanner.</font></p>
    </td>
  </tr>
</table>
<p>
<p>
<strong><font color="#265682" size="+3">3 Se considera el siguiente Diagn&oacute;stico :</font></strong>
<table width="100%"  border="1">
  <tr>
    <td><p><font size="+3">- Ruido interno.
      - Problemas en el separador de hojas del ADF
      <BR>
      </font></p>
    </td>
  </tr>
</table>
<p>
<p>
<strong><font color="#265682" size="+3">4 Soluci&oacute;n del problema (con costo referencial si fuera el caso) :</font></strong>
<table width="100%"  border="1">
  <tr>
    <td><font size="+3">- Seg&uacute;n informe emitido por la empresa DMI Sistemas y Servicios Integrados S.A.,   se recomienda que el mantenimiento integral del equipo para descartar error del   ADF,si la falla persiste se recomienda la adquisici&oacute;n de un scanner de similares   caracter&iacute;sticas ya que el costo de cambio del ADF supera al 50% del costo de un   scanner nuevo.</font>
      <BR><font size="+3">Caracteristicas T&eacute;cnicas:<BR>
        Resoluci&oacute;n Optica = 2400dpi<BR>
        Resoluci&oacute;n Mejorada   = 2400x2400dpi <BR>
        Profundidad del Color = 48bits <BR>
        Interfase = USB 2.0 <BR>
        Botones Inicio R&aacute;pido = Escanear, Copiar, E-mail, Share-to-web, Guardar doc.   disco. Soft. Creaci&oacute;n Im&aacute;genes Fotog. HP <BR>
        Tama&ntilde;o M&aacute;ximo de Documentos = 216 x   297mm. <BR>
        Diapositivas = S&iacute;. 35mm <BR>
        Escaneado de Negativos = S&iacute;. <BR>
        Alimentador Autom&aacute;tico = Est&aacute;ndar (50 P&aacute;g.) D&uacute;plex <BR>
        Tama&ntilde;o = 340 x 488 x   160mm. <BR>
        Compatibilidad = Win 9x/2000/ME/XP. Mac<BR>
        </font></td>
  </tr>
</table>
<p><font color="#265682" size="+3">Lo cual comunico a su Despacho para los fines que estime conveniente.
    </font>
<p align="center"><font color="#265682" size="+3">
  Atentamente</font>
<p align="center">
<p align="center"><font color="#265682" size="+3"><BR>
  <strong>{$tecnico}<br>
  Soporte T&eacute;cnico</strong></font> 
<p align="left"><font color="#265682" size="+3">Cc: OCII </font></p>

{literal}
  <script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
  </script>
    <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
    </script>
    <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
    </script>
  {/literal} </p>
</body>
</html>
