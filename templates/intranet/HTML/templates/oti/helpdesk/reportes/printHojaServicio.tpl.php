<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Boleta de Atención</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/styles/intranet.css" type="text/css" />
</head>
<body>
<p><img src="/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg"></p>
<p align="center"><strong><font color="#265682">HOJA DE SERVICIO T&Eacute;CNICO N&deg; {$numero} </font></strong><br>
  <br>
</p>
<p align="right"><strong><font color="#265682">Fecha: {$fecha} </font></strong><br>
  <br>
</p>
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr>
    <td><table width="100%" border="1" cellspacing="0" cellpadding="1">
        	<tr> 
            	<td width="25%" class="item">Nombre del T&eacute;cnico </td>
				<td colspan="2" width="50%" class="item">{$tecnico}</td>
        	    <td width="25%" class="item">N&deg; PC: {$nroPC}</td>
        	</tr>
        	<tr> 
            	<td class="item">Dependencia</td>        
        	    <td colspan="2" class="item">{$oficina}</td>
        	    <td class="item"><input type="checkbox" name="po" value="checkbox" {if $modPC=="Propio"} checked="checked"{/if}>Propia&nbsp;
				  <input type="checkbox" name="po" value="checkbox" {if $modPC!="Propio"} checked="checked"{/if}>Terceros&nbsp;</td>
        	</tr>
        	<tr> 
            	<td class="item">Nombre de usuario </td>        
        	    <td colspan="2" class="item">{$usuario}</td>
        	    <td class="item">Anexo: </td>
        	</tr>
        	<tr> 
            	<td colspan="2" class="item" align="center" bgcolor="#CCCCCC"><strong>Solicitud del servicio</strong></td>        
        	    <td colspan="2" class="item" align="center" bgcolor="#CCCCCC"><strong>Atenci&oacute;n del servicio</strong></td>
        	</tr>
        	<tr> 
            	<td class="item">Fecha: {$fecIni}</td>        
        	    <td class="item">Hora: {$horIni}</td>
        	    <td class="item">Fecha: {$fecFin}</td>
        	    <td class="item">Hora: {$horFin}</td>
        	</tr>
        	<tr> 
            	<td colspan="4" class="item">Tipo de servicio atendido: 
            	  <input type="checkbox" name="hw" value="checkbox" {if ($tipAte1=="Hardware"||$tipAte2=="Hardware"||$tipAte3=="Hardware")} checked="checked"{/if}>Hardware&nbsp;&nbsp;
				  <input type="checkbox" name="sw" value="checkbox" {if ($tipAte1=="Software"||$tipAte2=="Software"||$tipAte3=="Software")} checked="checked"{/if}>Software&nbsp;&nbsp;
				  <input type="checkbox" name="re" value="checkbox" {if ($tipAte1=="Redes"||$tipAte2=="Redes"||$tipAte3=="Redes")} checked="checked"{/if}>Redes&nbsp;&nbsp;
				  <input type="checkbox" name="ot" value="checkbox" {if ($tipAte1=="Otros"||$tipAte2=="Otros"||$tipAte3=="Otros")} checked="checked"{/if}>Otros&nbsp;&nbsp;				</td>        
        	</tr>			
        	<tr> 
            	<td colspan="4" class="item" align="center" bgcolor="#CCCCCC"><strong>Descripci&oacute;n del problema</strong></td>        
        	</tr>
			<tr>
				<td colspan="4" class="item">
					<table border="0" width="100%">
						<tr><td width="100%" class="item">{$problema}</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
					</table>
				</td> 
			</tr>
        	<tr> 
            	<td colspan="4" class="item" align="center" bgcolor="#CCCCCC"><strong>Descripci&oacute;n del trabajo realizado</strong></td>        
        	</tr>
			<tr>
				<td colspan="4" class="item">
					<table border="0" width="100%">
						<tr><td width="100%" class="item">{$trabajo}</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
						<tr><td width="100%">&nbsp;</td></tr>
					</table>
				</td> 
			</tr>			
        	<tr> 
            	<td colspan="4" class="item" align="center" bgcolor="#CCCCCC"><strong>Informaci&oacute;n a ser llenada por el usuario</strong></td>        
        	</tr>
        	<tr><td colspan="4">
				<table border=0 width="100%">
					<tr>
					<td colspan="2" class="item">La presentación de nuestro personal le pareci&oacute; a Ud.</td>
					<td colspan="2" class="item">:
					  <input type="checkbox" name="calificacion" value="checkbox">Muy Bueno&nbsp;&nbsp;
					  <input type="checkbox" name="calificacion" value="checkbox">Bueno&nbsp;&nbsp;
					  <input type="checkbox" name="calificacion" value="checkbox">Regular&nbsp;&nbsp;
					  <input type="checkbox" name="calificacion" value="checkbox">Malo&nbsp;&nbsp;
					  <input type="checkbox" name="calificacion" value="checkbox">P&eacute;simo&nbsp;&nbsp;
					  </td>
					</tr>
					<tr> 
						<td colspan="2" class="item">El tiempo de espera en atenderlo le pareci&oacute; </td>
						<td colspan="2" class="item">:
						  <input type="checkbox" name="calificacion" value="checkbox">Muy Bueno&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Bueno&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Regular&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Malo&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">P&eacute;simo&nbsp;&nbsp;</td>
					</tr>
					<tr> 
						<td colspan="2" class="item">En general, &iquest;C&oacute;mo calificar&iacute;a el servicio brindado? </td>
						<td colspan="2" class="item">:
						  <input type="checkbox" name="calificacion" value="checkbox">Muy Bueno&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Bueno&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Regular&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">Malo&nbsp;&nbsp;
						  <input type="checkbox" name="calificacion" value="checkbox">P&eacute;simo&nbsp;&nbsp;</td>
					</tr>
					<tr> 
						<td colspan="4" class="item">&nbsp;&nbsp;</td>
					</tr>					
					<tr> 
						<td colspan="4" class="item">En caso de haber marcado alguna respuesta como malo o p&eacute;simo, indique la causa en &quot;observaciones&quot; </td>
					</tr>
					<tr> 
						<td colspan="4" class="item">&nbsp;&nbsp;</td>
					</tr>					
					<tr> 
						<td colspan="4" class="item">S&iacute;rvase verificar los datos de fecha y hora de solicitud y atenci&oacute;n del servicio.  </td>
					</tr>
					<tr> 
						<td colspan="4" class="item">Observaciones:</td>
					</tr>
					<tr> 
						<td colspan="4" class="item">&nbsp;</td>
					</tr>					
					<tr> 
						<td colspan="4" class="item">&nbsp;</td>
					</tr>															
					<tr> 
						<td colspan="4" class="item">&nbsp;</td>
					</tr>															
					<tr> 
						<td colspan="4" class="item" align="center" width="100%">____________________</td>
					</tr>															
					<tr> 
						<td colspan="4" class="item" align="center">Firma del usuario </td>
					</tr>																																			
				</table>
				</td>  
        	</tr>
        	<tr> 
            	<td colspan="4" class="item" bgcolor="#CCCCCC" align="center"><strong>Verificaci&oacute;n del supervisor </strong></td>        
        	</tr>
        	<tr> 
            	<td colspan="4" class="item" width="100%">
					<table border="0" width="100%">
					<tr>
					  <td class="item">Nombre del supervisor: </td>
					</tr>
					<tr>
					  <td class="item">Observaciones:</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td class="item" align="center" width="100%">____________________</td></tr>
					<tr>
					  <td class="item" align="center">Firma del supervisor </td>
					</tr>
					</table>
				</td>        
        	</tr>			
      	</table>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
Oficina General de Tecnolog&iacute;a de la Informaci&oacute;n y Estad&iacute;stica - OGTIE
</p>
{literal}

<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opción de impresión. Presione Control/Opción + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>