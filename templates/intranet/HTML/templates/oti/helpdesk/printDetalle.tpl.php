<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Atenciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<br>
	  <table width="95%" border="0" align="center" cellpadding="1" cellspacing="0">
            <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                  <tr> 
                    <td> 
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
                        <tr> 
                          <td width="77%"><table width="75%" border="1" cellspacing="0" cellpadding="1">
                              <tr> 
                                <td class="item"><strong>USUARIO</strong></td>
                                <td class="item">{$usuario}</td>
                              </tr>
                              <tr> 
                                <td class="item"><strong>OFICINA</strong></td>
                                <td class="item">{$oficina}</td>
                              </tr>
                              <tr> 
                                <td class="item"><strong>N&deg; PC</strong></td>
                                <td class="item">{$nroPC}</td>
                              </tr>
                              <tr> 
                                <td class="item"><strong>MODALIDAD DE PC</strong></td>
                                <td class="item">{$modPC}</td>
                              </tr>
                            </table></td>
                          <td width="11%">&nbsp;</td>
                          <td width="12%">
						     <table width="100%" border="1" cellspacing="0" cellpadding="1">
                              <tr>
                                <td class="item">Fecha: {$fecha}</td>
                              </tr>
                            </table></td>
                        </tr>
                      </table>

					
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr>
							  <td class="item">Diagn&oacute;stico</td>
							  <td class="item">{$diagnostico}</td>
						  </tr>
						  <tr>
							  <td class="item">Liquidaci&oacute;n</td>
							  <td class="item">{$liquidacion}</td>
						  </tr>
						</table>
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr>
							
                          <td class="item"><strong>EVALUACI&Oacute;N</strong></td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">Agradeceremos su colaboraci&oacute;n 
                            en pos de un mejor servicio</td>
							<td class="item">Muy Bueno</td>
							<td class="item">Bueno</td>
							<td class="item">Regular</td>
							<td class="item">Malo</td>
						  </tr>
						  <tr>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">La presentaci&oacute;n de nuestro personal 
                            le pareci&oacute; a Ud.</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">Nuestro personal demostr&oacute; Profesionalismo</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">Desde que nuestro personal se present&oacute; 
                            en su Oficina, el tiempo empleado en la soluci&oacute;n 
                            de la aver&iacute;a, le pareci&oacute; a Ud.</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">Que opini&oacute;n tiene Ud. de todo 
                            el proceso de atenci&oacute;n de su aver&iacute;a</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item">En caso de alguna(s) de las respuesta(s) 
                            la Clasifique Ud., como Mala, indicamos la causa en 
                            Observaciones. </td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						</table>

					
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr>
							
                          <td class="item"><strong>OBSERVACIONES</strong>:</td>
						  </tr>
						  <tr>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="item">&nbsp;</td>
						  </tr>
						</table>

					
					</td>
                  </tr>
                </table>
		<p>
		        <table width="100%" border="0" cellspacing="0" cellpadding="1">
                  <tr> 
                    <td class="item">Conformidad</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">Hora de Inicio{$inicio}</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">Hora de T&eacute;rmino{$fin}</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td class="item">&nbsp;</td>
                    <td class="item">Firma y Sello del Usuario</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">Firma del T&eacute;cnico {$tecnico} </td>
                    <td class="item">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                    <td class="item">&nbsp;</td>
                  </tr>
                  <tr> 
                    <td colspan="6" class="item"><div align="center">Servicio 
                        de Atenci&oacute;n al Usuario: 616-2222 Anexo 331 - 330</div></td>
                  </tr>
                </table>

	</td>
  </tr>
</table>
{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>