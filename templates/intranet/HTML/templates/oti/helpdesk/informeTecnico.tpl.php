<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Boleta de Atenci�n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<p><img src="/img/logoCONVENIO_SITRADOC.jpg"></p>
<p align="center"><strong><font color="#265682">INFORME T&Eacute;CNICO </font><font color="#265682"> N&deg; {$numero}-2006-CONVENIO_SITRADOC/OGTIE-Oti </font></strong><br>
</p>
<p align="right"><font color="#265682">San Isidro, {fechaGen} </font></p>
<p><font color="#265682"><strong>Se&ntilde;or<br>
  ELADIO PERCY SOL&Oacute;RZANO D&Iacute;AZ </strong><br>
  Director de la Oficina de Tecnolog&iacute;a de la Informaci&oacute;n<br>
  Presente.-</font>
</p>
<p>
<p><font color="#265682"><ul>Tengo el agrado de saludarlo y a la vez comunicarle lo siguiente:</ul></font></p>
</p>
<p><font color="#265682"><strong>1 PQue el equipo inform&aacute;tico con las caracter&iacute;sticas:a :</strong></font></p>

<table width="100%"  border="1">
  <tr>
    <td width="21%"><font color="#265682">&nbsp;</font></td>
    <td width="79%" bgcolor="#808080" ><font color="#265682">PC</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">MARCA</font></td>
    <td><font color="#265682">{$marca}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">MODELO</font></td>
    <td><font color="#265682">{$modelo}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">NUM. SERIE </font></td>
    <td><font color="#265682">{$serie|default:'No 
                  especificado'}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">COD. PC </font></td>
    <td><font color="#265682">{$codPC}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">COD PATRIM.</font></td>
    <td><font color="#265682">{$codPat}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682">DEPENDENCIA</font></td>
    <td><font color="#265682">{$dep}</font></td>
  </tr>
</table>
<p><p>
<font color="#265682"><strong>2 Presenta :</strong></font><br>
<table width="100%"  border="1">
  <tr>
    <td><p>{$descrip}</p>
    <p>&nbsp;</p></td>
  </tr>
</table>
<p><p>
<strong><font color="#265682">3 Se considera el siguiente Diagn&oacute;stico :</font></strong><br>
<table width="100%"  border="1">
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
</table>
<p><p>
<strong><font color="#265682">4 Soluci&oacute;n del problema (con costo referencial si fuera el caso) :</font></strong><br>
<table width="100%"  border="1">
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
</table>
<p>
  <font color="#265682">Lo cual comunico a su Despacho para los fines que estime conveniente.
  </font>
<p align="center"><font color="#265682">Atentamente,
</font>
<p align="center">
<p align="center">
<p align="center"><font color="#265682">{$tecnico}<br>
  Soporte T&eacute;cnico </font>
<p align="center">
<p align="center">
<p align="left"><font color="#265682">Cc: </font>  
{literal}

<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>