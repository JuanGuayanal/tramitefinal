<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Boleta de Atenci�n</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<p><img src="/img/800x600/mmb.logo.CONVENIO_SITRADOC.jpg"></p>
<p align="center"><strong><font color="#265682">HOJA</font><font color="#265682"> DE SERVICIO T&Eacute;CNICO N&deg; {$numero} </font></strong><br>
  <br>
</p>
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0">
            <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="1">
                  <tr> 
                    <td> 
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
                        <tr> 
                          <td width="73%"><table width="95%" border="1" cellspacing="0" cellpadding="1">
                              <tr> 
                                <td bgcolor="#C0C0C0" class="item"><strong><font color="#265682" size="-1">USUARIO</font></strong></td>
                                <td class="item"><font color="#265682" size="-1">{$usuario}</font></td>
                              </tr>
                              <tr> 
                                <td bgcolor="#C0C0C0" class="item"><strong><font color="#265682" size="-1">OFICINA</font></strong></td>
                                <td class="item"><font color="#265682" size="-1">{$oficina}</font></td>
                              </tr>
                              <tr> 
                                <td bgcolor="#C0C0C0" class="item"><strong><font color="#265682" size="-1">N&deg; PC</font></strong></td>
                                <td class="item"><font color="#265682" size="-1">{$nroPC}</font></td>
                              </tr>
                              <tr> 
                                <td bgcolor="#C0C0C0" class="item"><strong><font color="#265682" size="-1">MODALIDAD DE PC</font></strong></td>
                                <td class="item"><font color="#265682" size="-1">{$modPC}</font></td>
                              </tr>
                            </table></td>
                          <td width="5%">&nbsp;</td>
                          <td width="23%">
						     <table width="100%" border="1" cellspacing="0" cellpadding="1">
                              <tr>
                                <td bgcolor="#C0C0C0" class="item"><strong><font color="#265682" size="-1">FECHA</font></strong></td>
                                <td class="item"><font color="#265682" size="-1">{$fecha}</font></td>
                              </tr>
                            </table></td>
                        </tr>
                      </table>

					
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr>
							  <td width="25%" bgcolor="#C0C0C0" class="item"><font color="#265682" size="-1"><strong>DIAGN&Oacute;STICO</strong></font></td>
							  <td width="76%" class="item"><font color="#265682" size="-1">{$tipAte1}: {$ate1}{if $tipAte2}<br>{$tipAte2}: {$ate2}{/if}{if $ate3}<br>{$tipAte3}: {$ate3}{/if}</font></td>
						  </tr>
						  <tr>
							  <td bgcolor="#C0C0C0" class="item"><font color="#265682" size="-1"><strong>LIQUIDACI&Oacute;N</strong></font></td>
							  <td class="item"><font color="#265682" size="-1">{$diagnostico}</font></td>
						  </tr>
					  </table>
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr bgcolor="#C0C0C0">
							
                          <td width="79%" class="item"><strong><font color="#265682" size="-1">EVALUACI&Oacute;N (<font color="#265682" size="-1">Agradeceremos su colaboraci&oacute;n en aras de un mejor servicio</font>)</font></strong></td>
							<td width="7%" class="item"><font color="#265682" size="-1">Muy Bueno</font></td>
							<td width="5%" class="item"><font color="#265682" size="-1">Bueno</font></td>
							<td width="5%" class="item"><font color="#265682" size="-1">Regular</font></td>
							<td width="4%" class="item"><font color="#265682" size="-1">Malo</font></td>
						  </tr>
						  <tr>
							
                          <td class="item"><font color="#265682" size="-1">La presentaci&oacute;n de nuestro personal 
                            le pareci&oacute; a Ud.</font></td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item"><font color="#265682" size="-1">Nuestro personal demostr&oacute; profesionalismo.</font></td>

							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item"><font color="#265682" size="-1">Desde que nuestro personal se present&oacute; 
                            en su oficina, el tiempo empleado en la soluci&oacute;n 
                            de la aver&iacute;a, le pareci&oacute; a Ud.</font></td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td class="item"><font color="#265682" size="-1">Que opini&oacute;n tiene Ud. de todo 
                            el proceso de atenci&oacute;n de su aver&iacute;a.</font></td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							
                          <td colspan="5" class="item"><font color="#265682" size="-1">En caso de alguna(s) de las respuesta(s) 
                            la clasifique Ud., como mala, indicarnos la causa en 
                            Observaciones.</font></td>
						  </tr>
						</table>

					
					</td>
                  </tr>
                  <tr>
                    <td>
						<table width="100%" border="1" cellspacing="0" cellpadding="1">
						  <tr>
							
                          <td class="item"><strong><font color="#265682" size="-1">OBSERVACIONES</font></strong>:</td>
						  </tr>
						  <tr>
							<td class="item">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="item">&nbsp;</td>
						  </tr>
						</table>

					
					</td>
                  </tr>
                </table>
		<p>
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr> 
          <td width="13%" class="item"><font color="#265682" size="-1"><strong>CONFORMIDAD</strong></font></td>
          <td width="27%" class="item">&nbsp;</td>
          <td width="1%" class="item">&nbsp;</td>
          <td width="19%" class="item"><font color="#265682" size="-1"><strong>Hora de Inicio</strong> </font></td>
                    <td width="18%" class="item"><font color="#265682" size="-1">{$inicio}</font></td>
          <td width="20%" class="item">&nbsp;</td>
          <td width="2%" class="item">&nbsp;</td>
        </tr>
        <tr> 
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item"><font color="#265682" size="-1"><strong>Hora de T&eacute;rmino</strong> </font></td>
          <td class="item"><font color="#265682" size="-1">{$fin}</font></td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
        </tr>
        <tr> 
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td colspan="2" class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
        </tr>
        <tr> 
          <td class="item">&nbsp;</td>
          <td class="item"><p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><font color="#265682" size="-1"><strong>__________________________<br>
&nbsp;&nbsp;&nbsp;Firma y Sello del Usuario</strong></font></p></td>
          <td class="item">&nbsp;</td>
          <td colspan="2" class="item"><p>&nbsp;</p>
          <p align="center">&nbsp;</p>
          <p align="center">&nbsp;</p>
          <p align="center"><font color="#265682" size="-1"><strong>___________________<br>
&nbsp;&nbsp;&nbsp;Firma del T&eacute;cnico<br>{$tecnico}</strong></font></p></td>
          <td class="item"><p>&nbsp;</p>
            <p><font color="#265682" size="-1"><br>
              <!--{$tecnico}-->
            </font></p></td>
          <td class="item">&nbsp;</td>
        </tr>
        <tr> 
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td colspan="2" class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
          <td class="item">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" class="item"><div align="center">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><font color="#265682" size="-1">Servicio 
                                    de Atenci&oacute;n al Usuario: 616-2222 Anexo 331 - 330</font></p>
          </div></td>
        </tr>
      </table>

	</td>
  </tr>
</table>
{literal}

<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>