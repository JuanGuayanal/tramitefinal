<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>HELPDESK</title>
</head>
<body> 
  <!-- HEADER LEFT "$LOGOIMAGE" --></div>
  </div>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="right"><font color="#265682" size="3" face="Arial"><b> </b></font><font color="#FF0000" size="5" face="Arial"></font> 
      <p align="right"><font color="#265682" size="3" face="Arial"><b> </b></font> 
      <p align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="7">SUMARIO</font><font size="7"> 
        DE ATENCIONES REALIZADAS POR PERSONAL DE SOPORTE<br>
        SEG&Uacute;N RUBRO</font><br>
        </font></b></font> <br>
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<div align="center"> 
  <p>&nbsp;</p>
  <table width="38%" border="1">
    {section name=i loop=$ate1} 
    <tr> 
      <td width="43%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>RUBRO 
          </strong></font></div></td>
      <td width="57%"><div align="center"><font size="6"><strong><font color="#0033CC">CANTIDAD</font></strong></font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>HARDWARE</b></font></div></td>
      <td><div align="center"><font size="6">{$ate1[i].hw}</font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>SOFTWARE</b></font></div></td>
      <td><div align="center"><font size="6">{$ate1[i].sw}</font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>MANTENIMIENTO 
          CORRECTIVO</b></font></div></td>
      <td><div align="center"><font size="6">{$ate1[i].mc}</font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>REDES</b></font></div></td>
      <td><div align="center"><font size="6">{$ate1[i].redes}</font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>OTROS</b></font></div></td>
      <td><div align="center"><font size="6">{$ate1[i].otros}</font></div></td>
    </tr>
    <tr> 
      <td bgcolor="#CCCCCC"><div align="center"><font color="#000000" size="6" face="Arial"><b>TOTAL</b></font></div></td>
      <td bgcolor="#999999"> 
        <div align="center"><font size="6">{$ate1[i].total}</font></div></td>
    </tr>
    {sectionelse} 
    <tr> 
      <td colspan="2" align="center" bgcolor="#FFFFFF"><strong><font size="6">NO 
        HAY ATENCIONES EN LA &Uacute;LTIMA FECHA</font></strong></td>
    </tr>
    {/section} 
  </table>
</div>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>
