<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p align="center"><strong><font color="#265682" size="+4">INFORME T&Eacute;CNICO N&deg; {$numero}-2007-CONVENIO_SITRADOC/OGTIE-Oti </font></strong><br>
</p>
<p align="right"><br> 
<font color="#265682" size="+3">San Isidro, {$fechaGen}</font></p>
<p><font color="#265682" size="+3"><strong>Se&ntilde;or<br>
  ELADIO PERCY SOL&Oacute;RZANO D&Iacute;AZ</strong><br>
  Director de la Oficina de Tecnolog&iacute;a de la Informaci&oacute;n(e)<br>
  Presente.-</font>
</p>
<p><ul>
  <p><font size="+3">Tengo el agrado de saludarlo y a la vez comunicarle lo siguiente:</font></p>
</ul>
</p>
<p>
  <font color="#265682" size="+3"><strong>1 Que el equipo inform&aacute;tico con las caracter&iacute;sticas:</strong>
</font></p>
<table width="100%"  border="1">
  <tr>
    <td width="21%"><font color="#265682" size="+3">&nbsp;</font></td>
    <td width="79%" bgcolor="#808080" ><font color="#265682" size="+3">{if ($codigoParte>0)}{$DispositivoParte}{else}PC{/if}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">MARCA</font></td>
    <td><font color="#265682" size="+3">{if ($codigoParte>0)}{$MarcaParte}{else}{$marca}{/if}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">MODELO</font></td>
    <td><font color="#265682" size="+3">{if ($codigoParte>0)}{$ModeloParte}{else}{$modelo}{/if}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">NUM. SERIE </font></td>
    <td><font color="#265682" size="+3">{if ($codigoParte>0)}{$SerialParte}{else}{$serie|default:'No 
                  especificado'}{/if}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">COD. PC </font></td>
    <td><font color="#265682" size="+3">{$codPC|default:'No 
                  especificado'}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">COD PATRIM.</font></td>
    <td><font color="#265682" size="+3">{if ($codigoParte>0)}{$PatrimonialParte}{else}{$codPat}{/if}</font></td>
  </tr>
  <tr>
    <td bgcolor="#808080"><font color="#265682" size="+3">DEPENDENCIA</font></td>
    <td><font color="#265682" size="+3">{$dep}</font></td>
  </tr>
</table>
<p><p>
<font color="#265682" size="+3"><strong>2 Presenta :</strong></font><br>
<table width="100%"  border="1">
  <tr>
    <td><p><font size="+3">{$descrip}</font></p>
    </td>
  </tr>
</table>
<p><p>
<strong><font color="#265682" size="+3">3 Se considera el siguiente Diagn&oacute;stico :</font></strong><br>
<table width="100%"  border="1">
  <tr>
    <td><p><font size="+3">{$asunto}</font></p>
    </td>
  </tr>
</table>
<p><p>
<strong><font color="#265682" size="+3">4 Soluci&oacute;n del problema (con costo referencial si fuera el caso) :</font></strong><br>
<table width="100%"  border="1">
  <tr>
    <td><p><font size="+3">{$observaciones}</font></p>
    </td>
  </tr>
</table>
<p>
<p><font color="#265682" size="+3">Lo cual comunico a su Despacho para los fines que estime conveniente.
    </font>
<p align="center"></p>
  <p align="center">
  <p align="center"><font color="#265682" size="+3"><br><br>Atentamente</font>
<p align="center"></p>  
  <p align="center"></p>
  <p align="center"></p>
  <p align="center"></p>
<p align="center">

<p align="center"><font color="#265682" size="+3"><br>
        <br>
    <strong>{$tecnico}<br>
    Soporte T&eacute;cnico</strong> </font>
    
<p> 
<p align="center"></p>  
  <p align="center"></p>
    <p align="left"><font color="#265682" size="+3">Cc: {$siglas}</font></p>

{literal}
  <script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
  </script>
    <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
    </script>
    <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
    </script>
  {/literal} </p>
</body>
</html>
