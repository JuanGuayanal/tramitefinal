<html>
<head>
<title>Documentos : {$doc.desc}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.Estilo1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; font-weight: bold; }
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.IMPRIME_DETALLE_DIR}&id={$id}"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="7" class="textoblack"><strong><img src="/img/ico-detalle.gif" width="24" height="24" hspace="4" align="absmiddle"><u>DETALLES 
            DEL DOCUMENTO PRINCIPAL</u></strong> <br> <strong>00057867-2006&nbsp;&nbsp;</strong><br> <strong>C&oacute;digo Interno: 333649&nbsp;&nbsp;</strong></td>
        </tr>
        <tr> 
          <td width="297"  colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>N&deg; Documento</strong></td>
          <td class="texto">00057867-2006</td>
          <td width="297" class="texto">&nbsp;</td>
          <td width="297" class="texto"><strong>Fecha de Ingreso</strong></td>
          <td width="293" class="texto">08/09/2006 11:13</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Asunto</strong></td>
          <td colspan="4" class="texto">1. Permiso de pesca para la operaci&oacute;n de embarcaciones pesqueras de mayor escala de bandera nacional del &aacute;mbito mar&iacute;timo.</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{if $idTipoDoc!=4}
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS 
              DE LA EMPRESA</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Raz&oacute;n Social</strong></td>
          <td class="texto">ELIZABETH S.C.R.L.</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>RUC</strong></td>
          <td class="texto">20445495141</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Direcci&oacute;n Legal</strong></td>
          <td class="texto">No especificado</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Tel&eacute;fono</strong></td>
          <td class="texto">No especificado</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{if ($email!=""&& $email!=" ")}
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>E-mail</strong></td>
          <td class="texto">No especificado </td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{/if}
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS 
              GENERALES DEL DOCUMENTO</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td width="186" class="textoblack"><strong>N&deg; de Documento</strong></td>
          <td width="148" class="texto">00057867-2006</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Folios</strong></td>
          <td class="texto">1</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Indicativo</strong></td>
          <td class="texto">S/N</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{/if}
		{if $anex}
		<tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>ANEXOS  DEL DOCUMENTO</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{section name=i loop=$anex}
        <tr> 
          <td colspan="2" class="textoblack"><strong>Anexo Nro. {$smarty.section.i.iteration}</strong></td>
          <td width="186" class="texto">{$anex[i].num|default:'No posee n�mero'}</td>
          <td width="148" class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Ingreso </strong></td>
          <td class="texto">{$anex[i].fecIng|default:'No especificado'}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Contenido</strong></td>
          <td colspan="5" class="texto">{$anex[i].cont|default:'No especificado'}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Observaciones</strong></td>
          <td colspan="5" class="texto">{$anex[i].obs|default:'No especificado'}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{/section}
		{/if}
		
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>SITUACI&Oacute;N 
              DEL DOCUMENTO</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Situaci&oacute;n Actual</strong></td>
          <td class="texto">Finalizado autom&aacute;ticamente el 13/09/2006 11:13 en OADA, SE VENCI&Oacute; EL PLAZO DE 48 HORAS FALTA REQUISITOS DEL TUPA  COMPENDIOSO DE DOMINIO,  EL RECIBO DE PAGO ES DEL A&Ntilde;O 2005, MATRICULA VIGENTE Y LA RESOLUCION DE CAPITANIA ES DEL A&Ntilde;O 1996</td>
          <td class="texto">{if $fechaMaxPlazo!=""}<strong><font color="#FF0000">Plazo M&aacute;ximo:</font></strong> <font color="#FF0000">{$fechaMaxPlazo}</font>{/if}</td>
          <td class="texto"><strong>{if !$sitActual}N&deg; de D&iacute;as en Tr&aacute;mite{/if}</strong></td>
          <td class="texto">{if !$sitActual}{$nroDias}{/if}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>Direcci&oacute;n Receptora</strong></td>
          <td class="texto">OADA</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>{if $nroDiasTupa}N&deg; de D&iacute;as catalogados 
            en TUPA{/if}</strong></td>
          <td class="texto">{if $nroDiasTupa}{$nroDiasTupa}&nbsp;{/if}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{if $user2}
        <tr> 
          <td colspan="2" class="textoblack"><strong>Finalizado por </strong></td>
          <td class="texto">{$user2}</td>
          <td class="texto">Observaciones</td>
          <td class="texto"><span class="item">{$observaciones}</span></td>
          <td class="texto">Fecha de finalizaci&oacute;n </td>
          <td class="texto">{$fecha1}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Fecha de Archivaje </strong></td>
          <td class="texto">{$fecha2|default:'No se ha archivado'}</td>
          <td class="texto">Nivel 1 </td>
          <td class="texto"><span class="item">{$nivel1|default:'No se ha archivado'}</span></td>
          <td class="texto">Nivel 2 </td>
          <td class="texto"><span class="item">{$nivel2|default:'No se ha archivado'}</span> <span class="item">{$nivel3|default:'No se ha archivado'}</span></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>

		{/if}
		{if ($resol||$noti)}
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS GENERALES DE LA RESOLUCI&Oacute;N Y/O CORRESPONDENCIA(S) </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{if $resol}
		{section name=i loop=$resol}
        <tr> 
          <td colspan="2" class="textoblack"><strong>{$resol[i].tipResol}</strong></td>
          <td class="texto">{$resol[i].nroResol}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Firma </strong></td>
          <td class="texto">{$resol[i].fFirma}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Sumilla</strong></td>
          <td colspan="3" class="texto">{$resol[i].sumilla}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Fecha de Inicio </strong></td>
          <td class="texto">{$resol[i].fInicio}</td>
          <td class="texto">Fecha de Fin </td>
          <td class="texto">{$resol[i].fFin}</td>
          <td class="texto"><strong>Fecha de Publicaci&oacute;n </strong></td>
          <td class="texto">{$resol[i].fPub}</td>
        </tr>
		
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{/section}
		{/if}		
		{if $noti}
		{section name=i loop=$noti}
        <tr> 
          <td colspan="2" class="textoblack"><strong>{$noti[i].tipCorrespondencia}</strong></td>
          <td colspan="5" class="texto">{$noti[i].nroCor}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Destinatario</strong></td>
          <td colspan="3" class="texto">{$noti[i].destinatario}</td>
          <td class="texto"><span class="textoblack"><strong>Domicilio</strong></span></td>
          <td class="texto">{$noti[i].domicilio}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Tipo de Mensajer&iacute;a </strong></td>
          <td class="texto">{$noti[i].tipMensajeria}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de entrega al Courier </strong></td>
          <td class="texto">{$noti[i].fecEntCourier}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Estado de la Norificaci&oacute;n </strong></td>
          <td class="texto">{$noti[i].estado}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Notificaci&oacute;n </strong></td>
          <td class="texto">{$noti[i].fecNoti}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Recibido por </strong></td>
          <td class="texto">{$noti[i].nomPerActo}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Identificado con </strong></td>
          <td class="texto">{$noti[i].perActo}</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{/section}
		{/if}
		{/if}
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>FLUJO 
              ENTRE DEPENDENCIAS </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<!--
        <tr> 
          <td class="textoblack"><strong>N&deg;</strong></td>
          <td class="textoblack"><strong>DEPENDENCIA</strong></td>
          <td class="textoblack"><strong>SUB DEPENDENCIA</strong></td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>TIPO DE DOCUMENTO</strong></td>
          <td class="textoblack"><strong>ASUNTO</strong></td>
          <td class="textoblack"><strong>FOLIOS</strong></td>
          <td class="textoblack"><strong>OBSERVACI&Oacute;N</strong></td>
          <td class="textoblack"><strong>FECHA Y HORA</strong></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {section name=i loop=$Exp} 
        <tr> 
          <td class="textoblack">{$smarty.section.i.iteration}</td>
          <td class="textoblack">{$Exp[i].depDest}</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack">{$Exp[i].tipDoc}&nbsp;{$Exp[i].ind}</td>
          <td class="textoblack">{$Exp[i].asu}</td>
          <td class="textoblack">{$folio}</td>
          <td class="textoblack">{$Exp[i].obs}</td>
          <td class="textoblack">{$Exp[i].fecha}</td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {sectionelse} 
        <tr> 
          <td colspan="9" class="textoblack"> <div align="center"><strong>No existe 
              informaci&oacute;n acerca del flujo del Documento</strong></div></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {/section}
        <tr> 
          <td colspan="2" class="texto">&nbsp;</td>
          <td colspan="7" class="texto">&nbsp;</td>
        </tr>
		--> 
        <tr> 
          <td colspan="7" class="texto"><table width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC">
              <tr> 
                <td class="textoblack"><strong>CLASE DE DOCUMENTO </strong></td>
                <td class="textoblack"><strong>INDICATIVO-OFICIO</strong></td>
                <td class="Estilo1">FECHA</td>
                <td class="textoblack"><strong>ASUNTO</strong></td>
                <td class="textoblack"><strong>OBSERVACIONES</strong></td>
                <td class="textoblack"><strong>AVANCE</strong></td>
                <td class="textoblack"><strong>DEPENDENCIA ORIGEN</strong></td>
				<td class="textoblack"><strong>DEPENDENCIA DESTINO</strong></td>
              </tr>
              <tr> 
                <td class="textoblack">{$claseDoc}</td>
                <td class="textoblack">{$indicativo}</td>
                <td class="textoblack">{$fecDer}</td>
                <td class="textoblack"></td>
                <td class="textoblack"></td>
                <td class="textoblack">{$avance|default:'No especificado'}</td>
                <td class="textoblack">{$depOrigen}</td>
				<td class="textoblack">{if $depDestino}{$depDestino}{else}{$depDestino2}{/if}</td>
              </tr>
        {section name=i loop=$Exp} 
              <tr> 
                <td class="textoblack">{$Exp[i].cla}</td>
                <td class="textoblack">{$Exp[i].ind}</td>
                <td class="textoblack">{$Exp[i].fDer}</td>
                <td class="textoblack">{$Exp[i].asu}</td>
                <td class="textoblack">{$Exp[i].obs}{if $Exp[i].fPlazo!=""}. Fecha de Plazo: {$Exp[i].fPlazo}{/if}</td>
                <td class="textoblack">{$Exp[i].avance}</td>
                <td class="textoblack">{$Exp[i].depo}</td>
				<td class="textoblack">{$Exp[i].depd}</td>
              </tr>
			  {/section}
            </table></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>FLUJO 
              ENTRE TRABAJADORES </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" class="texto"><table width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC">
              <tr>
			    <td class="textoblack"><div align="center"></div>			      <strong>TRABAJADOR</strong></td> 
                <td class="textoblack"><strong>FECHA DE DERIVACI&Oacute;N </strong></td>
                <td class="Estilo1">FECHA DE ACEPTACI&Oacute;N </td>
				<td class="textoblack"><strong>ASUNTO</strong></td>
                <td class="textoblack"><strong>OBSERVACIONES</strong></td>
                <td class="textoblack"><strong>AVANCE</strong></td>
                <td class="textoblack"><strong>DOCUMENTO GENERADO </strong></td>
              </tr>
        {section name=i loop=$doc} 
              <tr> 
                <td class="textoblack">{$doc[i].nombre}</td>
                <td class="textoblack">{$doc[i].diaEnvio} {$doc[i].horaEnvio}</td>
                <td class="textoblack">{if $doc[i].diaRec}{$doc[i].diaRec} {$doc[i].horaRec}{else}{if $doc[i].estado=='V'}Delegaci&oacute;n m&uacute;ltiple{else}No lo ha recibido a&uacute;n{/if}{/if}</td>
                <td class="textoblack">{$doc[i].link|default:' '}{if ($coddep!=5)}<br>Acci&oacute;n: {$doc[i].obsSecre|default:' '}{/if}</td>
				<td class="textoblack">{if ($doc[i.index_next].obs)!=""}{$doc[i.index_next].obs|default:'No Posee observaciones'}{else}{$doc[i].obsSecre|default:' '}{/if}{if $doc[i].fPlazo!=""}. Fecha de Plazo: {$doc[i].fPlazo}{/if}</td>
                <td class="textoblack">{$doc[i].avance|default:'No Posee avances'}</td>
                <td class="textoblack">{$doc[i.index_next].idOficio|default:'No ha generado documento'}</td>
              </tr>
			  {sectionelse}
              <tr> 
                <td colspan="7" class="textoblack"><div align="center"><strong>No existe informaci&oacute;n 
                    acerca del flujo entre trabajadores </strong></div></td>
              </tr>
			  {/section}
            </table></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <b><u>INFORMES:</u></b><br>
           Oficina de Gesti&oacute;n Documentaria y Atenci&oacute;n al Ciudadano<br> 
            <b>Telf.  209-8000   Anexo 4003,  1001 y 1015  </b><br>            <br>          </td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <div align="right"><em class="textored"><strong>Actualizado 
              al {$FechaActual}&nbsp;{$HoraActual}</strong> </em></div></td>
        </tr>
        <tr> 
          <td colspan="12" align="right">&nbsp;</td>
        </tr>
      </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
    </td>
  </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
</body>
</html>