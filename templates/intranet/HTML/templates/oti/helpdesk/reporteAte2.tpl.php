<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        </b></font>
      <p align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="7">SUMARIO 
        DE ATENCIONES REALIZADAS POR PERSONAL DE SOPORTE<br>
        SEG&Uacute;N T&Eacute;CNICO ENTRE EL {$fecha1} AL {$fecha2}</font></font></b></font><font color="#265682" size="3" face="Arial"><b><font color="#000000"><br>
        </font></b></font> <br>
      <hr width="100%" size="1" noshade> </p> <p>&nbsp;</p></td>
  </tr>
</table>
<div align="center">
  <table width="88%" border="1">
    {section name=i loop=$ate2} 
    <tr> 
      <td width="14%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>T&eacute;cnico</strong></font></div></td>
      <td width="17%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>Hardware</strong></font></div></td>
      <td width="17%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>Software</strong></font></div></td>
      <td width="16%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>Mantenimiento 
          Correctivo</strong></font></div></td>
      <td width="16%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>Redes</strong></font></div></td>
      <td width="16%" bgcolor="#CCCCCC"><div align="center"><font color="#0033CC" size="6" face="Arial"><strong>Otros</strong></font></div></td>
      <td width="20%" bgcolor="#CCCCCC"><div align="center"><font color="#333333" size="6" face="Arial"><strong>Total</strong></font></div></td>
    </tr>
	    <!--<tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Jaime 
          Martel</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw1}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw1}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc1}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re1}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot1}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot1}</font></div></td>
    </tr>
	-->
	    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Jes&uacute;s 
          Rod&iacute;guez</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw4}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw4}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc4}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re4}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot4}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot4}</font></div></td>
    </tr>
	    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Jos&eacute; 
          C&aacute;rdenas</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw6}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw6}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc6}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re6}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot6}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot6}</font></div></td>
    </tr>
	    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Juan Rodr&iacute;guez</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw7}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw7}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc7}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re7}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot7}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot7}</font></div></td>
    </tr>
	    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Luis Flores </font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw8}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw8}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc8}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re8}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot8}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot8}</font></div></td>
    </tr>
	    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Ovidio 
          Barja </font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw5}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw5}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc5}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re5}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot5}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot5}</font></div></td>
    </tr>
    <!--<tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Percy 
          Quispe</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw3}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw3}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc3}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re3}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot3}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot3}</font></div></td>
    </tr>-->
    <tr> 
      <td><div align="center"><font size="6"><strong><font color="#0033FF">Ulises 
          Jord&aacute;n</font></strong></font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].hw2}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].sw2}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].mc2}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].re2}</font></div></td>
      <td><div align="center"><font size="6">{$ate2[i].ot2}</font></div></td>
      <td bgcolor="#999999">
<div align="center"><font size="6">{$ate2[i].tot2}</font></div></td>
    </tr>
    <tr bgcolor="#999999"> 
      <td> 
        <div align="center"><strong><font size="5">TOTAL</font></strong></div></td>
      <td> 
        <div align="center"><font size="6">{$ate2[i].hw}</font></div></td>
      <td bgcolor="#999999"> 
        <div align="center"><font size="6">{$ate2[i].sw}</font></div></td>
      <td> 
        <div align="center"><font size="6">{$ate2[i].mc}</font></div></td>
      <td> 
        <div align="center"><font size="6">{$ate2[i].re}</font></div></td>
      <td> 
        <div align="center"><font size="6">{$ate2[i].ot}</font></div></td>
      <td> 
        <div align="center"><font size="6">{$ate2[i].tot}</font></div></td>
    </tr>
    {sectionelse} 
    <tr> 
      <td colspan="6" align="center" bgcolor="#FFFFFF"><strong><font size="6">NO 
        HAY ATENCIONES EN LA &Uacute;LTIMA FECHA</font></strong></td>
    </tr>
    {/section} 
  </table>
</div>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>
