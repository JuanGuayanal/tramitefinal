<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrTrab} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="20" align="center" valign="middle">
			{if $tiene_derecho==1} 
			<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_TRABAJADOR}&id={$arrTrab[i].id}&menu={$accion.FRM_BUSCA_TRABAJADOR}&subMenu={$accion.FRM_MODIFICA_TRABAJADOR}"><img src="/img/800x600/ico-mod.gif" alt="[ Modificar ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>
			{/if}
		  </td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr>
                <td valign="top" class="contenido-intranet" width="15%"><strong>Dependencia</strong></td>
                <td class="contenido-intranet"><strong>:</strong>&nbsp;{$arrTrab[i].depe|default:'No 
                  especificado'}</td>
                <td class="contenido-intranet" align="right">{$arrTrab[i].email}<!--<img src="/img/imagen4.gif" width="15" height="12" >--><strong><font color="#FF0000">{$arrTrab[i].estado}&nbsp;&nbsp;&nbsp;&nbsp;</font></strong>				</td>
              </tr>
              <tr>
                <td valign="top" class="contenido-intranet"><strong>Trabajador</strong></td>
                <td colspan="2" class="contenido-intranet"><strong>:&nbsp;{$arrTrab[i].est} {$arrTrab[i].nomTrab|default:'No especificado'}&nbsp;&nbsp;</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet">Condici&oacute;n</td>
                <td colspan="2" class="contenido-intranet"><strong>:</strong>&nbsp;{$arrTrab[i].cond|default:'No 
                  especificado'}&nbsp;&nbsp;Profesi&oacute;n: {$arrTrab[i].profesion|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item-intranet">Fecha Nac.</td>
                <td colspan="2" class="contenido-intranet"><strong>:</strong>&nbsp;{$arrTrab[i].fecNac|default:'No 
                  especificado'}&nbsp;&nbsp;Dni: {$arrTrab[i].dni|default:'No 
                  especificado'}&nbsp;&nbsp;Telef.: {$arrTrab[i].telef|default:'No 
                  especificado'}</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item-intranet">Creado</td>
                <td colspan="2" class="sub-item-intranet"><strong>:&nbsp;{$arrTrab[i].userCrea|default:'No especificado'} {$arrTrab[i].auditCrea|default:'No especificado'}&nbsp;&nbsp;
				{if ($arrTrab[i].auditCrea!=$arrTrab[i].auditMod)}Modificado: {$arrTrab[i].userMod|default:'No especificado'} {$arrTrab[i].auditMod|default:'No especificado'}{/if}</strong></td>
              </tr>			  
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
