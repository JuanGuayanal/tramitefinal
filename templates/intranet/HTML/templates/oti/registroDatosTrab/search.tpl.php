{$jscript}
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = "tr-check";
}
{/literal}
-->
</script>
<br>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp; Datos del Trabajador</td>
  </tr>
  <tr>
    <td class="contenido-intranet">&nbsp;</td>
  </tr>
  <tr>
    <td class="contenido-intranet">
		{if $tiene_derecho==1}
		::<a href="/institucional/informatica/registroDatosTrab/index.php?accion=frmAddTrab&subMenu=frmAddTrab&menu=frmSearchAte"><b>AGREGAR NUEVO TRABAJADOR</b></a>::
		{/if}
	 </td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
</table>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page2">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_TRABAJADOR}">
  <table width="600" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="6" class="textoblack">&nbsp;</td>
      <td width="150" class="contenido-intranet"><strong>Nombre</strong></td>
      <td width="217" valign="middle"><input name="nombreTrabajador" type="text" class="ip-login contenido" value="{$nombreTrabajador}" size="25" maxlength="100"></td>
      <td width="120" valign="middle"> <input type="submit" class="submitintranet" onClick="document.{$frmName}.page.value=''" value="Listar">      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet"><strong>Status</strong></td>
      <td colspan="2" valign="middle"><select name="status" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_TRABAJADOR}')">
        <option value="T"{if $status=="T"} selected{/if}>Todos</option>
        <option value="ACTIVO"{if $status=="ACTIVO"} selected{/if}>Activo</option>
        <option value="INACTIVO"{if $status=="INACTIVO"} selected{/if}>Inactivo</option>
      </select></td>
    </tr>
    <tr> 
      <td class="contenido-intranet"><strong>Profesi&oacute;n</strong></td>
      <td colspan="2" valign="middle"><select name="profesion" id="trabajador" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_TRABAJADOR}')">
			{$selProfesion}
      </select>      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet"><strong>Dependencia</strong></td>
      <td colspan="2" valign="middle"><select name="dependencia" id="dependencia" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_TRABAJADOR}')">
			{$selDependencia}
      </select>      </td>
    </tr>
    <tr> 
      <td class="contenido-intranet"><strong>Condici&oacute;n</strong></td>
      <td colspan="2" valign="middle"><select name="condicion" id="tipAte" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_TRABAJADOR}')">
			{$selCondicion}
      </select>      </td>
    </tr>		
    <tr> 
      <td class="contenido-intranet"><strong>Cargo</strong></td>
      <td colspan="2" valign="middle"><select name="cargo" id="atencion" class="ip-login contenido" onChange="submitForm('{$accion.BUSCA_TRABAJADOR}')">
        <option value="none"{if $cargo=="none"} selected{/if}>Todos</option>
        <option value="1"{if $cargo=="1"} selected{/if}>Director General</option>
        <option value="6"{if $cargo=="6"} selected{/if}>Secretaria de Direcci�n General</option>
        <option value="5"{if $cargo=="5"} selected{/if}>Director de L�nea</option>
        <option value="7"{if $cargo=="7"} selected{/if}>Secretaria de Direcci�n de L�nea</option>
		<option value="2"{if $cargo=="2"} selected{/if}>Usuario Notificaciones</option>
		<option value="NULL"{if $cargo=="NULL"} selected{/if}>Usuario Com�n</option>		
	  </select>      </td>
    </tr>
  </table>
</form>
