{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarFecNacTrab.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>

<script language="JavaScript">
<!--
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}

function ninguna_letra(){
	event.returnValue = false;
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('dependencia','Dependencia','Sel','apellidoTrabajador','Apellidos','R','nombreTrabajador','Nombres','R','condicion','Condici�n','Sel','email','E-mail','R','profesion','Profesion','Sel','dni','DNI','R','telefono','Tel�fono','R','anexo','Anexo','R','desFechaIni','Fecha de nacimiento','R'{/literal});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
-->
</script>
<!-- {/literal} -->
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
  <tr>
    <td class="tree" bgcolor="#31698C" height="25" valign="middle">&nbsp; Datos del Trabajador</td>
  </tr>
  <tr>
    <td class="contenido-intranet">&nbsp;</td>
  </tr>
  <tr>
    <td class="contenido-intranet"> ::<a href="/institucional/informatica/registroDatosTrab/index.php?accion=frmSearchAte&subMenu=frmSearchAte&menu=frmSearchAte"><b>BUSCAR TRABAJADOR</b></a>::</td>
  </tr>
  <tr>
    <td class="tree">&nbsp;</td>
  </tr>
</table>
	<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <br>
  <table width="531" border="0" align="center" cellpadding="0" cellspacing="1">
        {if $errors} 
    <tr> 
      <td colspan="2" class="item">{$errors} </td>
    </tr>
    {/if} 
	<tr>
      <td> 
        <table width="600" align="center">
          <tr> 
            <td width="120" class="contenido-intranet" ><strong>Dependencia</strong></td>
            <td colspan="2"><select name="dependencia" id="dependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
			{$selDependencia}
      </select></tr>
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>SubDependencia</strong></td>
            <td colspan="2"><select name="subDependencia" id="subDependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
			{$selSubDependencia}
      </select></tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Apellidos</strong></td>
            <td colspan="2"><input name="apellidoTrabajador" type="text" class="iptxt1" value="{$apellidoTrabajador}" size="40" maxlength="100"></td>
          </tr>
          <tr class="item"> 
            <td width="75" class="contenido-intranet" ><strong>Nombres</strong></td>
            <td colspan="2"><input name="nombreTrabajador" type="text" class="iptxt1" value="{$nombreTrabajador}" size="40" maxlength="100"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Condici&oacute;n</strong></td>
            <td colspan="2" ><select name="condicion" id="tipAte" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
			{$selCondicion}
      </select></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet"><strong>Email</strong></td>
            <td height="3" colspan="2"><input name="email" type="text" class="ip-login contenido"  value="{$email}" size="30" maxlength="50"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Profesi&oacute;n</strong></td>
            <td colspan="2" ><select name="profesion" id="trabajador" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
			{$selProfesion}
		  		</select></td>
          <tr> 
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Dni</strong></td>
            <td colspan="2" ><input name="dni" type="text" class="ip-login contenido"  value="{$dni}" size="30" maxlength="8" onKeyPress="solo_num();"></td>
          <tr> 
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Tel&eacute;fono</strong></td>
            <td colspan="2" ><input name="telefono" type="text" class="ip-login contenido"  value="{$telefono}" size="30" maxlength="15" onKeyPress="solo_num();"></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Publicado en el portal?</strong></td>
            <td colspan="2" class="item"><input name="publi" type="radio" value="1" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi==1)} checked="checked"{/if}><strong>Si</strong> <input type="radio" name="publi" value="0" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi==0||!$publi)} checked="checked"{/if}> 
        <strong>No</strong></td>
          </tr>
		  {if $publi==1}
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Dependencia en el portal</strong></td>
            <td colspan="2" class="item"><select name="dependenciaPortal" id="dependenciaPortal" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
			{$selDependenciaPortal}
      </select></tr>
	  	  {/if}
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Cargo</strong></td>
            <td colspan="2" class="item"><select name="cargo" id="atencion" class="ip-login contenido" onChange="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')">
        <option value="none"{if $cargo=="none"} selected{/if}>Seleccione una opci�n</option>
        <option value="1"{if $cargo=="1"} selected{/if}>Director General</option>
        <option value="6"{if $cargo=="6"} selected{/if}>Secretaria de Direcci�n General</option>
        <option value="5"{if $cargo=="5"} selected{/if}>Director de L�nea</option>
        <option value="7"{if $cargo=="7"} selected{/if}>Secretaria de Direcci�n de L�nea</option>
		<option value="2"{if $cargo=="2"} selected{/if}>Usuario Notificaciones</option>
		<option value="NULL"{if $cargo=="NULL"} selected{/if}>Usuario Com�n</option>		
	  </select></tr>
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Publicado en la Intranet?</strong></td>
            <td colspan="2" class="item"><input name="publi2" type="radio" value="1" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi2==1)} checked="checked"{/if}><strong>Si</strong> <input type="radio" name="publi2" value="0" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi2==0||!$publi2)} checked="checked"{/if}> 
        <strong>No</strong></td>
          </tr>
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Anexo</strong></td>
            <td colspan="2" ><input name="anexo" type="text" class="ip-login contenido"  value="{$anexo}" size="15" maxlength="15" onKeyPress="solo_num();"></td>
          </tr>	
          <tr> 
            <td width="75" class="contenido-intranet" ><strong>Publicado en el Correo Web?</strong></td>
            <td colspan="2"><input name="publi3" type="radio" value="1" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi3==1)} checked="checked"{/if}><strong>Si</strong> <input type="radio" name="publi3" value="0" onClick="submitForm('{$accion.FRM_AGREGA_TRABAJADOR}')" {if ($publi3==0||!$publi3)} checked="checked"{/if}> 
        <strong>No</strong></td>
          </tr>
          <tr> 
            <td width="75" valign="middle" class="contenido-intranet"><strong>Fecha de Nac. </strong></td>
            <td  height="3"><input name="desFechaIni" type="text" class="iptxt1" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
			</td>
            <td width="400" valign="top">&nbsp;<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
          </tr>		  		  	  	  		  		  		  		  
        </table></td>
    </tr>
  </table>

  <br>
  <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
    <tr> 
      <td><div align="center"> 
          <input type="submit" class="submitintranet" value="Agregar" onClick="MM_validateForm('obs','La Observaci�n','R','nopc','N�mero de la PC','R','tipo1','Las atenciones','Sel','aten1','La Primera Atenci�n','Sel');return document.MM_returnValue">
        </div></td>
      <td><div align="center"> 
          <input name="cancel" type="button" class="submitintranet" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue">
        </div></td>
		<input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_TRABAJADOR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
    </tr>
  	</table>
	</form>
