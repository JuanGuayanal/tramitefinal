<script src="/sitradocV3/js/src/consulta_general/frmBuscarDocumento.js"></script>
<div id="std_nav">
<a href="/institucional/aplicativos/oad/sitradoc.php">Inicio</a> &gt;&gt; Consulta General
</div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=ReporteConsultaGeneral" target="_blank" name="formulario" id="formulario">
<table align="left" style="border: dashed 1px #CCC; margin-left:62px;" width="725px">

<tr class="std_oculto">
	<td align="left" width="90">
	  <input type="hidden" name="id_tipo_documento" /></td>
    <td align="left" colspan="2"><input type="radio" name="tipo_cd" value="0" />Todos</td>
</tr>
<tr>
<td rowspan="2" align="left" width="70">Clase Doc. </td>
	<td align="left"><input type="radio" name="tipo_cd" value="1" checked="checked"/>Externo</td>
    
</tr>
<tr>
	<td align="left"><input type="radio" name="tipo_cd" value="4" />Interno</td>
    
</tr>

<tr>
	<td align="left"><span>Expediente</span></td>
    <td align="left"><span><input type="text" name="n_registro" class="iptxtn"></span></td>
</tr>

<tr>
	<td align="left" valign="top">Tipo de documento</td>
    <td align="left" colspan="2"><select name="id_cde" class="ipseln" id="clase_doc">{$selClaseDocExt}</select><select name="id_cdi" class="ipseln std_oculto">{$selClaseDoc}</select> N&deg; Documento y/o Documento Externo y/o N&deg; Ticket<input type="text" name="n_indicativo" class="iptxtn"><span class="internos"><select name="coddep" class="ipseln">{$selDependencia}</select></span></td>
</tr>
<tr>
	<td align="left" valign="top">Asunto</td>
    <td align="left" colspan="2"><textarea name="asunto" class="iptxtn" cols="80" rows="3"></textarea></td>
</tr>
<tr class="para_externos">
	<td align="left">Raz&oacute;n social</td>
    <td align="left" colspan="2"><input name="razon_social" type="text" class="iptxtn input"></td>
</tr>
<tr class="para_externos">
	<td align="left">Tipo Asunto</td>
    <td align="left" colspan="2"><select name="id_tipoasunto" class="ipseln input">{$selTipoAsunto}</select></td>
</tr>
<tr>
	<td align="left">Fecha</td>
    <td align="left" colspan="2"> De <input type="text" name="fecini" class="iptxtn datepicker" value="{php}echo '01/01/'.date('Y');{/php}"> a <input type="text" name="fecfin" class="iptxtn datepicker" value="{php}echo date('d/m/Y');{/php}"></td>
</tr>
<tr>
	<td></td>
    <td colspan="2" align="left"><button class="submitV2" id="exportar_excel" title="Generar Excel"><img src="/img/800x600/ico_excel2.jpg"></button> <button class="submitV2" id="exportar_pdf" title="Generar PDF"><img src="/img/800x600/ico-acrobat.gif"></button></td>
</tr>
<tr>
	<td></td>
	<td colspan="2" align="left"><input type="hidden" value="0" name="busca" /><input type="hidden" value="0" name="tipo_reporte" /><input type="submit" name="buscar" value="Buscar" class="std_button" id="buscar"></td>
</tr>
</table>
</form>
<table>
<tr>
	<td></td>
</tr>
<tr>
	<td></td>
</tr>
</table>
<div class="limpiar"></div><br /><br />
<table id="b_documentos">
<thead>
<tr>
	<th></th><th></th>
	<th>FECHA DE INGRESO</th>
	<th>TIPO DOCUMENTO</th>
	<th>N&deg; DOCUMENTO</th>
	<th>EXPEDIENTE</th>
	<th>ASUNTO</th>
    <th>RAZ&Oacute;N SOCIAL</th>
</tr>
</thead>
</table>