<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE ADJUNTOS QUE HAN INGRESADO A LA DEPENDENCIA DEL {$fecInicio2} {if $fecInicio2==$fecFin2}{else}AL {$fecFin2}{/if}
        </font> <br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td width="10%"> <div align="center"><strong><font size="5">N&deg; DE ADJUNTO</font></strong></div></td>
    <td width="8%"> <div align="center"><strong><font size="5">TIPO DE DOCUMENTO</font></strong></div></td>
    <td width="12%"> <div align="center"><strong><font size="5">DEPENDENCIA DESTINO</font></strong></div></td>
    <td width="25%"> <div align="center"><strong><font size="5">PERSONA DESTINO</font></strong></div></td>
    <td width="3%"><strong><font size="5">CONTENIDO</font></strong></td>
    <td width="42%"> <div align="center"><strong><font size="5">OBSERVACI&Oacute;N</font></strong></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td width="10%"><font size="5">{$list[i].numAdj}</font></td>
    <td width="8%"><font size="5">{$list[i].tipDoc}</font></td>
    <td width="12%"><font size="5">{$list[i].dep}</font></td>
    <td width="25%"><font size="5">{$list[i].per}</font></td>
    <td width="3%"><div align="center"><font size="5">{$list[i].cont}</font></div></td>
    <td width="42%"><font size="5">{$list[i].obs}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="6"><div align="center"><font size="6"><strong>No se han ingresado 
        adjuntos en las fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
