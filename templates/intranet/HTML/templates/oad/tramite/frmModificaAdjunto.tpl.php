{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('destino','Dependencia destino','Sel','Persona','Persona destino','Sel','tipDocumento','El tipo de Documento','Sel','contenido','El Contenido','R','folios','N�mero de folios','R');return document.MM_returnValue">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> <br>
      <td width="25%" class="item"><strong>No del documento </strong></td>
      <td colspan="3" class="item">{$nroadjunto}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Fecha de Emisi&oacute;n</strong></td>
      <td width="26%" class="item">{$FechaActual}</td>
      <td width="23%" class="item"><strong>Hora del Emisi�n</strong></td>
      <td width="26%" class="item">{$HoraActual}</td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Destino</strong></td>
      <td colspan="3"> <select name="Destino" class="ipsel2" onChange="submitForm('{$accion.FRM_MODIFICA_ADJUNTO}');" >
	  {$selDestino}
       </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Se&ntilde;or</strong></td>
      <td colspan="3"> <select name="Persona" class="ipsel2">
	  {$selPersona}
	   </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Tipo documento</strong></td>
      <td> <select name="tipDocumento" class="ipsel2" id="tipDocumento">
	  {$selDocumento}
       </select> </td>
      <td class="item"><div align="right"><strong>N&deg; de Folios</strong></div></td>
      <td><input name="folios" type="text" class="iptxt1" onKeyPress="solo_num();" value="{if $folios==''}1{else}{$folios}{/if}" size="4" maxlength="2"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Contenido</strong></td>
      <td colspan="3"> <textarea name="contenido" cols="70" rows="3" class="iptxt1">{$contenido}</textarea> 
      </td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong></td>
      <td colspan="3"> <textarea name="observaciones" cols="70" rows="3" class="iptxt1">{$observaciones}</textarea> 
      </td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> <input type="submit" name="Submit" value="Modificar Adjunto" class="submit"> 
        &nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_ADJUNTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
        <input name="idAdj" type="hidden" id="idAdj" value="{$idAdj}"> 
		<input name="nroadjunto" type="hidden" id="nroadjunto" value="{$nroadjunto}"></td>
    </tr>
  </table>
</form>