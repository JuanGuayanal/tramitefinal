<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<!-- {/literal} -->
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE DOCUMENTOS GENERADOS EN LA DEPENDENCIA DE ACUERDO A LA FECHA DE SALIDA {if $fecInicio2!=$fecFin2}ENTRE EL  
        {$fecInicio2} Y EL {$fecFin2}{else}DEL {$fecInicio2}{/if}
        <!--ENTRE LAS FECHAS {$fecInicio} y {$fecFin} -->
        </font> <br>
        </font></b></font> 
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
{if $tipodeDoc}<font size="5">CLASE DE DOCUMENTO: {$tipodeDoc}</font><br>{/if}
{if $tipodeTrat}<font size="5">CLASE DE TRATAMIENTO: {$tipodeTrat}</font><br>{/if}<br>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="8%"> <div align="center"><font size="5"><strong>TIPO</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>INDICATIVO</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>FECHA</strong></font></div></td>
	<td width="10%"> <div align="center"><font size="5"><strong>REFERENCIA</strong></font></div></td>
    <td width="40%"> <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="40%"> <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
    <td width="6"> <div align="center"><font size="5"><strong>DEP. RECEPTORA</strong></font></div></td>
    <!--<td width="6"> <div align="center"><font size="5"><strong>DEP.ACTUAL</strong></font></div></td>-->
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td width="8%"><font size="5">{$list[i].tipDoc}</font></td>
	<td width="10%"><font size="5">{$list[i].ind}</font></td>
	<td width="10%"><font size="5">{$list[i].fecCrea}</font></td>
	<td width="10%"><font size="5"><!--{if $list[i].ref==""&& $list[i].numTram==""}{$list[i].ind}{else}{$list[i].ref}<br>
	    {$list[i].numTram}{/if}-->
		{if $list[i].ref==""&& $list[i].numTram==""}{$list[i].refsinRep|default:'No especificado'}{else}{$list[i].ref}<br>
    {$list[i].numTram}{/if}
		</font></td>
    <td width="40%"><div align="center"><font size="5">{if $list[i].asunto==""}{$list[i].asunto2}{else}{$list[i].asunto}{/if}</font></div></td>
    <td width="40%"><font size="5">{$list[i].obs}</font></td>
    <td width="6"><font size="5">{$list[i].dep}</font></td>
    <!--<td width="6"><font size="5">{$list[i].ubiActual}</font></td>-->
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="8" width="100%"><div align="center"><font size="6"><strong>No 
        existen documentos ingresados en las fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen2}{$hora}s</i></font></p>
</body>
</html>
