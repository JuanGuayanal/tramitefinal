<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE DOCUMENTOS DEL D&Iacute;A 
        {$fechaGen} 
        <!--ENTRE LAS FECHAS {$fecInicio} y {$fecFin} -->
        </font> <br>
        </font></b></font> 
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td> <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td> <div align="center"><font size="5"><strong>PROVIENE</strong></font></div></td>
    <td> <div align="center"><font size="5"><strong>N&deg; DOCUMENTO</strong></font></div></td>
    <td> <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="8"> <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td><font size="5">{$list[i].nroTram}</font></td>
    <td><font size="5">{$list[i].remi}<br>
      {$list[i].remi2}</font></td>
    <td><font size="5">{$list[i].ind}<br>
      {$list[i].ind2}</font></td>
    <td><div align="center"><font size="5">{$list[i].asunto}</font></div></td>
    <td width="8"><font size="5">{$list[i].obs}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="6" width="100%"><div align="center"><font size="6"><strong>No 
        se han ingresado Documentos a la Oficina en las fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen2}{$hora}s</i></font></p>
</body>
</html>
