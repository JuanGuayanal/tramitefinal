<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
		<table width="100%"  border="0">
		  <tr>
			<td>
				<table width="30%"  border="1">
				  <tr>
					<td bgcolor="#B9B9B9"><div align="center"><strong><font size="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIGSECOVI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font size="6"><br>
&nbsp;</font></strong></div></td>
				  </tr>
				</table>
			</td>
		    <td>
				<table width="100%"  border="1">
				  <tr>
					<td bgcolor="#B9B9B9"><div align="center"><strong><font size="7">C�DULA DE NOTIFICACI&Oacute;N</font></strong><font size="6"><br>
				          <font size="5">PROCEDIMIENTO ADMINISTRATIVO SANCIONADOR</font></font></div></td>
				  </tr>
				</table>
			</td>
		    <td>
				<table width="30%"  border="1">
				  <tr>
					<td bgcolor="#B9B9B9"><strong><font size="6">N&deg;<br>
					</font></strong><font size="4">{$notificacion}</font><br>
				    &nbsp;</td>
				  </tr>
				</table>
			</td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<strong><font size="6">EXPEDIENTE N&deg;:</font></strong><font size="6"> {$acto}</font>      <table width="100%"  border="1">
		  <tr>
			<td colspan="4"><font size="6">Se�or(es): {$destinatario}</font></td>
			<td><font size="6">EP/IP</font></td>
			<td><font size="6">{$embarcacion}</font></td>
		  </tr>
		  <tr>
			<td rowspan="2"><div align="center"><strong><font size="6">DOMICILIO</font></strong></div></td>
			<td><font size="6">Jr/Av</font></td>
			<td colspan="4"><font size="6">{$domicilio}</font></td>
		  </tr>
		  <tr>
			<td><font size="6">Dist.</font></td>
			<td><font size="6">{$distrito}</font></td>
			<td><font size="6">Prov. {$provincia}</font></td>
			<td><font size="6">Dpto.</font></td>
			<td><font size="6">{$departamento}</font></td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%"  border="1">
		  <tr>
			<td bgcolor="#B9B9B9"><div align="center"><strong><font size="6">PRESUNTAS INFRACCIONES</font> </strong></div></td>
		  </tr>
		  <tr>
			<td><font size="5"><strong>Base Legal:</strong><br>
		    &nbsp;&nbsp;&nbsp;{section name=i loop=$infraccion}
				&nbsp;&nbsp;&nbsp;{$infraccion[i].inf}<br>
			{sectionelse}
				&nbsp;&nbsp;&nbsp;
			{/section}</font> </td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%"  border="1">
		  <tr>
			<td bgcolor="#B9B9B9"><div align="center"><strong><font size="6">HECHOS CONSTATADOS</font></strong></div></td>
		  </tr>
		  <tr>
			<td><font size="5">Los siguientes son los hechos constatados:<br>
				{$hechos}
			</font></td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%"  border="1">
		  <tr>
			<td bgcolor="#B9B9B9"><div align="center"><strong><font size="6">DOCUMENTOS QUE SE ANEXAN A LA NOTIFICACI&Oacute;N</font></strong></div></td>
		  </tr>
		  <tr>
			<td>
				<table width="100%"  border="0">
				  <tr>
					<td><input type="checkbox" name="checkbox" value="checkbox">
				      <font size="5"><strong>Reporte de Ocurrencia</strong></font></td>
					<td><font size="5">N&deg;</font></td>
					<td><font size="5"><strong>Fecha</strong></font></td>
				  </tr>
				  <tr>
					<td><input name="checkbox" type="checkbox" value="checkbox" {if $codigoProcedencia==11}{else} checked{/if}>
				      <font size="5"><strong>Reporte SISESAT</strong></font> </td>
					<td><font size="5">N&deg; {if $codigoProcedencia==11}{else}{$infSISESAT}{/if}</font> </td>
					<td><font size="5"><strong>Fecha</strong> &nbsp;&nbsp;{if $codigoProcedencia==11}{else}{$fecSISESAT}{/if}</font></td>
				  </tr>
				  <tr>
					<td><input type="checkbox" name="checkbox" value="checkbox">
				      <font size="5"><strong>Reporte de Descargas</strong></font></td>
					<td><font size="5">N&deg;</font></td>
					<td><font size="5"><strong>Fecha</strong></font> </td>
				  </tr>
				  <tr>
					<td colspan="2"><input type="checkbox" name="checkbox" value="checkbox">
				      <font size="5"><strong>Otros</strong></font></td>
					<td><font size="5"><strong>Fecha</strong></font></td>
				  </tr>
				</table>
			</td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%"  border="1">
		  <tr>
			<td bgcolor="#B9B9B9"><div align="center"><strong><font size="6">PRESENTACI&Oacute;N DE DESCARGOS</font></strong></div></td>
		  </tr>
		  <tr>
			<td><p><font size="5">El presunto infractor deber&aacute; presentar sus descargos en el plazo y ante la instancia que se se&ntilde;ala a fin de ejercer su derecho a la defensa y continuar con el procedimiento administrativo sancionador. Asimismo, deber&aacute;n precisar en su escrito el N&deg; de Expediente, domicilio procesal o real, entre otras informaciones que permitan la debida identificaci&oacute;n del Procedimiento. </font></p>
		      <p><font size="5">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Plazo</strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:Siete(07) d&iacute;as calendario, a partir del d&iacute;a siguiente de recibida la presente notificaci&oacute;n. <br>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>&Oacute;rgano Sancionador</strong>&nbsp;&nbsp;:Direcci&oacute;n General de Seguimiento, Control y Vigilancia - DIGSECOVI<br>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Domicilio</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:Calle Uno Oeste, N&deg; 060, San Isidro - Lima</font> </p>
		    <p>&nbsp;</p></td>
		  </tr>
		</table>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%"  border="0">
		<tr>
			<td colspan="2"><font size="6"><strong>NOTA</strong>: Ver al dorso r&eacute;gimen de beneficios de pago de multas e indicaciones. </font> </td>
		  </tr>
		  <tr>
			<td>
		    <p><font size="6">San Isidro, {$fechaGen2}</font></p>
		    <p>&nbsp;</p>
			<p>&nbsp;</p>
		    <div align="center"><font size="6">_____________________________________<br>
			    <strong>Direcci&oacute;n de Seguimiento,Vigilancia y Sanciones</strong><br>
	        Ing. ALEJANDRO RAM�REZ SALDA&Ntilde;A</font> </div></td>
			<td>
			  <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
			  <div align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                  <strong><font size="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></strong><font size="6"><br>
			______________________________________</font></div></td>
		  </tr>
		  <tr>
			<td><table width="100%"  border="1" align="center">
              <tr>
                <td><p><font size="6"><strong>RECIBIDO POR</strong><br> 
					...............................................................................<br>
					...............................................................................<br>
					<strong>RELACI&Oacute;N CON EL INTERESADO</strong><br> 
					..........................................................................<br>
					<strong>DNI</strong>........................................................................<br>
					<strong>FECHA</strong>....................................... <strong>HORA</strong> ..............<br>
					<strong>DIRECCI&Oacute;N</strong>....................................................<br>
					...............................................................................</font></p>
                  <p align="center">&nbsp;                    </p>
                  <p align="center">&nbsp;</p>
                  <p align="center"><font size="6">__________________________<br>
                      <strong>Firma</strong></font> </p></td>
              </tr>
            </table></td>
			<td><div align="center">
			  <p><strong><font size="6">Firma del Inspector o Notificador </font></strong></p>
			  <p align="left"><font size="6"><br>
			    <strong>Nombre:</strong>....................................................................<br>
			  				  <strong>DNI:</strong>..........................................................................<br>
							  <strong>Credencial:</strong>................................................................</font><br>
			  </p>
			</div>
			<br><br>
			<table width="100%"  border="1">
			  <tr>
				<td><font size="6"><strong>Caracter&iacute;sticas del domicilio:</strong><br>
                    <strong>Suministro N&deg;</strong> ................... <strong>Medidor N&deg;</strong> ...................<br>
					<strong>Material y color de la fachada</strong> ...................................................................<br>
					<strong>Material y color de la puerta</strong> .....................................................................</font><br>
				</td>
			  </tr>
			</table>

			</td>
		  </tr>
	  </table>

	</td>
  </tr>
</table>


</body>
</html>
