<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000"> <font size="6">REPORTE DEL SERVICIO DE MENSAJER&Iacute;A 
        LOCAL - SERGER</font></font></b></font> 
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table width="100%" border="1" align="center">
  <tr bgcolor="#B9B9B9"> 
    <td> 
      <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td> 
      <div align="center"><strong><font size="5">N&deg; DE DOCUMENTO</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="5">REMITENTE</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="5">RAZ&Oacute;N SOCIAL</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="5">DESTINO</font></strong></div></td>    
	<td> 
      <div align="center"><strong><font size="4">FEC. ENT. COURIER</font></strong></div></td>
    	<td> 
      <div align="center"><strong><font size="4">FEC. NOTIFICACI&Oacute;N</font></strong></div></td>
	<td> 
      <div align="center"><strong><font size="5">OBSERVACIONES</font></strong></div></td>
  </tr>
  {section name=i loop=$not} 
  <tr> 
    <td><font size="5">{$smarty.section.i.iteration}</font></td>
    <td><font size="5">{$not[i].nro}</font></td>
    <td><font size="5">{$not[i].remitente}</font></td>
    <td><font size="5">{$not[i].razonsocial}</strong></font></td>
    <td><font size="5">{if $not[i].destino}{$not[i].destino}{else}{if $not[i].domicilio}{$not[i].domicilio}{else}No especificado{/if}{/if}</font></td>
    <td><div align="center"><font size="5">{$not[i].fecEntCourier}</font></div></td>	   
    <td><div align="center"><font size="5">{$not[i].fecNot}</font></div></td>
    <td><font size="5">&nbsp;</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="8" align="center" bgcolor="#FFFFFF"><strong><font size="6">No 
      existen notificaciones en las fecha(s) dada(s).</font></strong></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
