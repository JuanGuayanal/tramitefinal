<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b>
          <font color="#000000"> <font size="6">LISTADO DE DOCUMENTOS DEL {if $fecInicio2==$fecFin2}{$fecInicio2}{else}{$fecInicio2} AL {$fecFin2}{/if}
          <!--ENTRE LAS FECHAS {$fecInicio} y {$fecFin} -->
          </font> </font></b></font> 
      </div>
      <hr width="100%" size="1" noshade>  </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td width="5%"> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td width="37%"> <div align="center"><font size="5"><strong>PROVIENE</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>N&deg; DOCUMENTO</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>FECHA</strong></font></div></td>
    <td width="46%"> <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="20%"> <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
	<!--<td width="20%"> <div align="center"><font size="5"><strong>DETALLES</strong></font></div></td>-->
	<!--<td width="10%"> <div align="center"><font size="5"><strong>FIRMA</strong></font></div></td>-->
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td width="5%"><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td width="10%"><font size="5">{$list[i].nroTram}</font></td>
    <td width="37%"><font size="5">{$list[i].remi}<br>
      {$list[i].remi2}</font></td>
    <td width="10%"><font size="5">{$list[i].ind}<br>
      {$list[i].ind2}</font></td>
    <td width="10%"><font size="5">{$list[i].fecha}</font></td>
    <td width="46%"><div align="center"><font size="5">{$list[i].asunto}</font></div></td>
    <td width="20%"><font size="5">{$list[i].obs}</font></td>
	<!--<td width="20%"><font size="5">{$list[i].detalle}</font></td>-->
	<!--<td width="10%"><font size="5">&nbsp;</font></td>-->
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="7" width="100%"><div align="center"><font size="6"><strong>No 
        se han ingresado Documentos a la Oficina en la(s) fecha(s) dada(s)</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen2}{$hora}s</i></font></p>
</body>
</html>
