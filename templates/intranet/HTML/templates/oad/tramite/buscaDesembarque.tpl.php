<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<link href="/styles/desDNPA.css" rel="stylesheet" type="text/css" /></head>
<body>
<xml id="dsoEspecies" src="xml/especies.xml"></xml>
<xml id="dsoDestinos" src="xml/destinos.xml"></xml>
<xml id="dsoEmbarcaciones" src="xml/embarcaciones.xml"></xml>
{$jscript}
<!-- {literal} -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>
<!-- {/literal} -->
<form name="{$frmName}" id="{$frmName}" method="post" action="{$frmURL}" onsubmit="return BuscarDesembarque()">
<input name="accion" type="hidden" value="{$accion.BUSCA_DOCUMENTO}" />
<input name="menu" type="hidden" value="{$accion.FRM_BUSCA_DOCUMENTO}" />
<input name="page" type="hidden" id="page" value="{$numPage}" />
<table width="100%"  border="0" cellspacing="0" cellpadding="0" background="/img/800x600/dnpa/desembarque/bg.frm.gif">
  <tr><td style="height:10px"></td></tr>
    <tr>
      <td colspan="6"><img src="/img/800x600/bg_blank.gif" alt="" width="1" height="15" /></td>
    </tr>
    <tr>
      <td colspan="6" class="lbEmb"><strong>Tipo de documento</strong></td>
    </tr>
    <tr>
      <td colspan="6"><select name="tipDocumento" class="selFrm" onChange="submitForm('{$accion.FRM_BUSCA_DOCUMENTO}')">
          <option value="3"{if $tipDocumento==3} selected{/if}>Todos</option>
          <option value="1"{if $tipDocumento==1} selected{/if}>Documentos Externos</option>
          <option value="2"{if $tipDocumento==2} selected{/if}>Expedientes</option>
      </select>&nbsp;&nbsp;<select name="tipBusqueda" class="selFrm" onChange="submitForm('{$accion.FRM_BUSCA_DOCUMENTO}')">
          <option value="1"{if $tipBusqueda==1} selected{/if}>Incompletos</option>
          <option value="2"{if $tipBusqueda==2} selected{/if}>Por Derivar</option>
          <option value="8"{if ($tipBusqueda==8||!$tipBusqueda)} selected{/if}>Derivados</option>
		  <option value="3"{if $tipBusqueda==3} selected{/if}>Finalizados</option>
        </select></td>
    </tr>
	<tr>
		<td class="lbEmb">Nro. Tr&aacute;mite</td>
	    
	    <td class="lbEmb">Indicativo / Oficio</td>
	    <td class="lbEmb">&nbsp;</td>
	    <td class="lbEmb">&nbsp;</td>
	    <td class="lbEmb">&nbsp;</td>
	    <td class="lbEmb">&nbsp;</td>
	</tr>
	<tr>
		<td><input name="nroTD" type="text" class="txtFrm" value="{$nroTD}" size="30" maxlength="100" /></td>
	    <td><input name="indicativo" type="text" class="txtFrm" value="{$indicativo}" size="30" maxlength="100"></td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	</tr>
    <tr>
      <td colspan="6"><img src="/img/800x600/dnpa/desembarque/sep1.frm.gif" alt="" width="70%" height="10" /></td>
    </tr>	
    <tr>
      <td class="tdPad" colspan="6"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lbEmb"><strong>Raz&oacute;n Social</strong></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="lb1Frm"><input name="radiobutton" type="radio" value="1" {if (!$radiobutton||$radiobutton==1)} checked="checked"{/if} onClick="submitForm('{$accion.FRM_BUSCA_DOCUMENTO}')" />
                Nombre&nbsp;&nbsp;
                <input name="radiobutton" type="radio" value="2" {if $radiobutton==2} checked="checked"{/if} onClick="submitForm('{$accion.FRM_BUSCA_DOCUMENTO}')"/>
                  RUC</td>
            </tr>
            <tr>
              <td class="lb1Frm"><input name="RZ" type="text" class="txtFrm" value="{$RZ}" size="30" maxlength="100">
              </td>
            </tr>
          </table></td>
          <td colspan="3" valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Procedimiento y/o asunto </strong></td>
            </tr>
            <tr>
              <td class="lb1Frm"><input name="asunto" type="text" class="txtFrm" id="asunto" style="width: 120px" tabindex="2" value="{$asunto}" />
              <input name="idEspecie" type="hidden" value="{$datos.idEsp}" /></td>
            </tr>
          </table></td>
          <td valign="bottom"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="lb1Frm"><strong>Observaciones</strong></td>
            </tr>
            <tr>
              <td class="lb1Frm"><input name="observaciones" type="text" class="txtFrm" id="observaciones" style="width: 120px" tabindex="3" value="{$observaciones}" />
             </td>
            </tr>
          </table></td>
        </tr>  

    <tr>
      <td nowrap="nowrap" class="lbEmb"><strong>Desde el </strong></td>
      <td nowrap="nowrap" class="lbEmb"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="desFechaIni" type="text" class="txtFrm" id="desFechaIni" value="{$fecDesIni}" tabindex="4" /></td>
    <td align="center"><a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
  </tr>
</table></td>
      <td colspan="2" nowrap="nowrap" class="lbEmb"><strong>hasta el </strong></td>
      <td class="lbEmb"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input name="desFechaFin" type="text" class="txtFrm" id="desFechaFin" value="{$fecDesFin}" tabindex="5" />
    </td>
    <td align="center"><a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
  </tr>
</table></td>
      <td width="100%" nowrap="nowrap"><input name="bSubmit" type="image" src="/img/800x600/dnpa/desembarque/bt.buscar.gif" alt="Buscar Desembarque" hspace="10" /></td>
    </tr>
    <tr>
      <td class="lb1Frm"></td>
      <td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td nowrap="nowrap" class="tdSep1"></td>
      <td class="lb1Frm"></td>
      <td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td></td>
    </tr>		
		
		</table></td>
  </tr>
</table>
</form>
<br />

</body>
</html>