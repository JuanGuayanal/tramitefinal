{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function cargar(pForm,a)
{
		pForm.action="index.php?"+a;
	    pForm.submit();
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('idClaseDoc','Clase de Documento','sel','dependencia','Dependencia','sel','asunto','Asunto','R','observaciones','Observaciones','R'{/literal}{if $dependencia==7&& $radio==1},'domicilio','Domicilio','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
  function AddTrab(a){literal}{{/literal}
	if(document.MM_returnValue){literal}{{/literal}
		var frm = document.{$frmName}
		var j = frm.destinatario[frm.destinatario.selectedIndex].value
		
		
		if(j==99999){literal}{{/literal}
			frm.wantsnewDest.value =1
				frm.tipPersonaNew.value = frm.tipPersona[frm.tipPersona.selectedIndex].value
				frm.tipIdentNew.value = frm.tipIdent[frm.tipIdent.selectedIndex].value
				frm.rucNew.value = frm.ruc.value			
		{literal}}{/literal}else{literal}{{/literal}
				frm.tipPersonaNew.value = 55//Asignando un valor cualquiera 
				frm.tipIdentNew.value = 55//Asignando un valor cualquiera 
				frm.rucNew.value = 55//Asignando un valor cualquiera 			
			{literal}}{/literal}
		
			var kk = frm.otroDomi[frm.otroDomi.selectedIndex].value
			if(kk==99999){literal}{{/literal}
				frm.wantsnewDomi.value =1
			{literal}}{/literal}else{literal}{{/literal}
				frm.wantsnewDomi.value =2
			{literal}}{/literal}		
		
		
		
		frm.idDestNew.value = frm.destinatario[frm.destinatario.selectedIndex].value
		frm.codDepaNew.value = frm.codDepa[frm.codDepa.selectedIndex].value
		frm.codProvNew.value = frm.codProv[frm.codProv.selectedIndex].value
		frm.codDistNew.value = frm.codDist[frm.codDist.selectedIndex].value
		frm.domicilioNew.value = frm.domicilio.value
		frm.personaNew.value = frm.observaciones.value
		frm.action = frm.action + a
		frm.accion.value = '{$accion.FRM_GENERA_REITERATIVO}'
		frm.submit()
	{literal}}{/literal}else
		return document.MM_returnValue
{literal}}{/literal}

-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="4" class="item-sep"><strong>Anulaci&oacute;n de una resoluci&oacute;n </strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>&nbsp;</strong></td>
      <td class="item">{$TipoResol}&nbsp;{$nroResol}<br>Sumilla: {$sumilla}<br>Referencia: {$referencia|default:'No especificado'}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta">&iquest;Disminuir el contador en uno? </td>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_ANULA_RESOLUCION}')">
              <strong>Si</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_ANULA_RESOLUCION}')">
              <strong>No</strong></label>&nbsp;(El contador es {$contResol})</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong>{$idResolucion}</td>
      <td><textarea name="observaciones" cols="80" rows="3" class="iptxtn" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="submitV2" value="Anular" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ANULA_RESOLUCION}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idResolucion" type="hidden" id="idResolucion" value="{$idResolucion}">
		 </td>
    </tr>
  </table>
</form>