<table width="740" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr> 
    <td height="20"><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias &nbsp;&nbsp; 
            {else} No se encontraron coincidencias &nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> <!--{section name="i" loop=$arrResol} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
            
          <td width="24" align="center">
		  	<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_RESOL_ESP}&idResolucion={$arrResol[i].id}&menu={$accion.FRM_BUSCA_RESOLUCION}&subMenu={$accion.FRM_MODIFICA_RESOL_ESP}"><img src="/img/800x600/ico-mod.gif" alt="[ MODIFICAR O NOTIFICAR ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>
			{if $coddep==5}<br />
			<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_FE_ERRATAS}&idResolucion={$arrResol[i].id}&menu={$accion.FRM_BUSCA_RESOLUCION}&subMenu={$accion.FRM_AGREGA_FE_ERRATAS}"><img src="/img/800x600/ico-mod.gif" alt="[ AGREGA FE ERRATAS ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  </td>
          <td class="texto">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="155" class="item"><strong>N&uacute;mero:</strong></td>
                <td class="texto"><strong>{$arrResol[i].tipResol} {$arrResol[i].nroResol}</strong></td>
                <td class="texto"><div align="right"><span class="item"><strong>Fecha de Firma:&nbsp;{$arrResol[i].fecFir}</strong></span></div></td>
              </tr>
			  <tr>
			  	<td valign="top" class="sub-item"><strong>Documento</strong></td>
				<td colspan="2" class="texto">{$arrResol[i].referencia}</td>
			  </tr>
			  <tr>
			  	<td valign="top" class="sub-item"><strong>Sumilla</strong></td>
				<td colspan="2" class="texto">{$arrResol[i].sumilla}</td>
			  </tr>
			  {if $arrResol[i].fecPub}
              <tr> 
                <td valign="top" class="sub-item"><strong>Fecha de Publicaci&oacute;n </strong></td>
                <td colspan="2" class="texto">{$arrResol[i].fecPub}</td>
              </tr>
			  {/if}
			  {if $arrResol[i].fecIni}
              <tr> 
                <td valign="top" nowrap class="sub-item"><strong> Fecha de Inicio </strong></td>
                <td colspan="2" class="texto">{$arrResol[i].fecIni} </td>
              </tr>
			  {/if}
			  {if $arrResol[i].fecFin}
              <tr> 
                <td valign="top" nowrap class="sub-item"><strong>Fecha de Fin </strong></td>
                <td colspan="2" class="texto">{$arrResol[i].fecFin}</td>
              </tr>
			  {/if}
			  {if $arrResol[i].fecErratas}
              <tr> 
                <td valign="top" nowrap class="sub-item"><strong>Fecha de Fe de Erratas</strong></td>
                <td colspan="2" class="texto">{$arrResol[i].fecErratas}</td>
              </tr>
			  {/if}			  
            </table></td>
        </tr>
      </table>
{/section}-->
{if $arrResol}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" style="width: 15px; height: 25px">&nbsp;</td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 70px"><strong>Tipo de resolucion</strong></td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 70px"><strong>N�mero</strong></td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 160px"><strong>Sumilla</strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 70px"><strong>Fecha Firma </strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 70px"><strong>Fecha Publicaci&oacute;n </strong></td>				  
				  {if $coddep==5}<td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 70px"><strong>Fecha Erratas</strong></td>{/if}
				  <td width="1%" bgcolor="#FEF7C1">&nbsp;</td>
  </tr>
				{section name=d loop=$arrResol}
		<tr>
						  <td class="item"><a href="{$frmUrl}?accion={$accion.FRM_ANULA_RESOLUCION}&idResolucion={$arrResol[d].id}&menu={$accion.FRM_BUSCA_RESOLUCION}&subMenu={$accion.FRM_ANULA_RESOLUCION}"><img src="/img/800x600/ico.eliminar.gif" alt="[ ANULAR ]" width="12" height="12" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a></td>
						  <td class="texto">{$arrResol[d].tipResol}</td>
						  <td class="texto">{$arrResol[d].nroResol}</td>
						  <td class="texto">{$arrResol[d].sumilla}<BR /><strong>Referencia: {$arrResol[d].referencia}</strong>&nbsp;&nbsp;<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_REFERENCIA_RESOL}&idResolucion={$arrResol[d].id}&menu={$accion.FRM_BUSCA_RESOLUCION}&subMenu={$accion.FRM_MODIFICA_REFERENCIA_RESOL}">(Modificar referencia)</a></td>
						  <td align="right" class="texto">{$arrResol[d].fecFir}</td>
						  <td align="right" class="texto">{$arrResol[d].fecPub}</td>
						  {if $coddep==5}<td align="right" class="texto">{$arrResol[d].fecErratas}</td>{/if}				  
						  <td align="right" class="texto"></td>
  </tr>
        <tr> 
          <td colspan="11"><hr width="100%" size="1"></td>
        </tr>  
				  {/section}
</table>
{/if} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <!--<tr> 
          <td><hr width="100%" size="1"></td>
        </tr>-->
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>