{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}

function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
-->
</script>
<!-- {/literal}-->
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}">
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="10" class="item">{$errors}</td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td colspan="3" class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      <div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="10" width="700"> 
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="td-lb-login" width="130"><b>DEPENDENCIA</b></td>
            <td width="100"><select name="dependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_DISMINUYE_CONTADOR}')">
                {$selDependencia}
              </select></td>
            <td class="td-lb-login" width="180"><b>&nbsp;&nbsp;CLASE DE DOCUMENTO </b></td>
            <td width="100"><select name="claseDoc" class="ip-login contenido" onChange="submitForm('{$accion.FRM_DISMINUYE_CONTADOR}')">
                {$selClaseDoc}
              </select></td>
            <td class="td-lb-login"><b>&nbsp;</b></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="10"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="10" align="right" class="texto">
<input type="button" name="Busca" class="but-login" value="Desplegar" onClick="submitForm('{$accion.FRM_DISMINUYE_CONTADOR}')">
        &nbsp;&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
    <tr> 
      <td width="540" rowspan="2" align="center" bgcolor="#CCE7F9" class="texto"><b>CLASE DE DOCUMENTO </b></td>
      <td colspan="10" align="center" width="240" class="texto" bgcolor="#CCE7F9"><center>
        <b>CONTADOR</b>
      </center></td>
    </tr>
    <tr> 
      <td align="center" width="240" bgcolor="#DFEDF6" class="texto">N&uacute;mero</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Click</td>
    </tr>
	{$aa}
    <tr> 
      <td colspan="10" class="item"> <div align="right"><strong> </strong></div></td>
    </tr>
	{if $claseDoc>0}
    <tr> 
      <td colspan="10" class="item"> <strong>Aseg&uacute;rese que este n&uacute;mero de documento est&eacute; anulado previamente. Gracias! </strong></td>
    </tr>
	{/if}	
    <tr> 
      <td colspan="10"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="10">{if $claseDoc>0} <input name="bSubmit" type="Submit" class="submitV2" value="DISMINUIR" >{/if} 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="CANCELAR" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_ASIGNA_DERECHOS}&menu={$accion.FRM_ASIGNA_DERECHOS}&subMenu={$accion.FRM_ASIGNA_DERECHOS}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.DISMINUYE_CONTADOR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Busca" type="hidden" id="Busca" value="1"> 
		 </td>
    </tr>
  </table>
</form>