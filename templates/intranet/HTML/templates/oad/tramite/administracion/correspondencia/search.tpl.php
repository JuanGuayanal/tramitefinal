{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
	MM_showHideLayers('popCalDiliIni','','hide')
	MM_showHideLayers('popCalDiliFin','','hide')
	MM_showHideLayers('popCalCreaIni','','hide')
	MM_showHideLayers('popCalCreaFin','','hide')
	MM_showHideLayers('popCalVeriIni','','hide')
	MM_showHideLayers('popCalVeriFin','','hide')
	
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
-->
</script>
<!-- {/literal} -->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_CORRESPONDENCIA}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  
  <table width="740" height="30" border="0" align="center" cellpadding="0" cellspacing="2" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif"  class="TEmb">
    <tr> 
      <td width="32" rowspan="14" class="textoblack"><img src="/img/Buscar2.jpg" ></td>
      <td width="180" class="textoblack"><strong>Tipo de Correspondencia:</strong></td>
      <td colspan="2" width="150" align="left"><select name="TipoCor" id="TipoCor" class="ipseln" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
			{$selTipoCorrespondencia}
		</select></td>
      <td width="115" valign="middle"><input name="bsubmit" type="submit" class="submitV2" value="Buscar"> 
      </td>
    </tr>
        <td class="textogray"><strong>{if $TipoCor==1}N&uacute;mero de Notificaci&oacute;n{else}N&uacute;mero de Documento{/if}</strong></td>
          <td colspan="3" align="left"><input name="noti" type="text" class="iptxtn" value="{$noti}" size="20" maxlength="100">
			&nbsp;&nbsp;
			<select name="anyo3" class="ipseln" id="select" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
			<!--		<option value="none">Todos</option>
					<option value="2005"{if ($anyo3==2005)} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo3==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo3==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo3==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo3==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo3==2011} selected{/if}>2011</option>
					<option value="2012"{if $anyo3==2012} selected{/if}>2012</option>  -->
					<option value="2013"{if $anyo3==2013} selected{/if}>2013</option>
		</select>
			<select name="dependencia" class="ipseln" id="dependencia" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
						{$selDependencia}
		    </select>
	  </td>
    </tr>
    <tr> 
      <td colspan="5" align="right" class="textogray" ><hr width="100%" size="1"></td>
    </tr>
    <tr>
	{if $TipoCor==1} 
    <tr> 
      <td class="textogray"><strong>Nro. Resoluci&oacute;n</strong></td>
      <td colspan="3" align="left"><input name="resol" type="text" class="iptxtn" value="{$resol}" size="20" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>El que expidi&oacute; el acto q se notifica</strong></td>
      <td colspan="3" align="left"><input name="detal" type="text" class="iptxtn" value="{$detal}" size="20" maxlength="100"></td>
    </tr>
	{/if}
    <tr> 
      <td class="textogray"><strong>Destinatario</strong></td>
      <td colspan="3" align="left"><input name="destinatario" type="text" class="iptxtn" value="{$destinatario}" size="20" maxlength="100"></td>
    </tr>
	<!--
    <tr> 
      <td class="textogray"><strong>Fecha de creaci&oacute;n </strong></td>
      <td width="190"><select name="dia_ini" class="ipsel2" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipsel2" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
      {$selAnyoIng}
        </select></td>
      <td width="10">&nbsp;  </td><td><select name="dia_fin" class="ipsel2" id="dia_fin">
        
      {$selDiaSal}
	    
      </select>
        <select name="mes_fin" class="ipsel2" id="mes_fin">
          
	  {$selMesSal}
        
        </select>
        <select name="anyo_fin" class="ipsel2" id="anyo_fin">
          
      {$selAnyoSal}
        
        </select></td>
    </tr>
	-->
    <tr> 
      <td class="textogray"><strong>Estado de Diligencia</strong></td>
      <td width="190" align="left"><select name="status" class="ipseln" id="status" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
						<option value="none">Todos</option>
						<option value="4"{if ($status==4)} selected{/if}>Por entregar al Courier</option>
						<option value="5"{if ($status==5)} selected{/if}>Por notificar</option>
						<option value="1"{if ($status==1)} selected{/if}>Notificada</option>
						<option value="2"{if $status==2} selected{/if}>Rechazada</option>
						<option value="3"{if $status==3} selected{/if}>No practicada</option>
					</select>
	  </td>
      <td width="10">&nbsp;  </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Tipo de Diligencia</strong></td>
      <td width="190" align="left"><select name="tipMensajeria" class="ipseln" id="tipMensajeria" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
						<option value="none">Todos</option>
						<option value="1"{if ($tipMensajeria==1)} selected{/if}>Nacional</option>
						<option value="2"{if $tipMensajeria==2} selected{/if}>Local</option>
					</select></td>
      <td width="10">&nbsp;  </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Visto bueno</strong></td>
      <td width="190" align="left"><select name="vistoBueno" class="ipseln" id="vistoBueno" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIA}');">
						<option value="none">Todos</option>
						<option value="1"{if ($vistoBueno==1)} selected{/if}>Se ha dado visto bueno</option>
						<option value="2"{if $vistoBueno==2} selected{/if}>A�n no se da visto bueno</option>
					</select></td>
      <td width="10">&nbsp;  </td>
      <td>&nbsp;</td>
    </tr>	
    <!--<tr> 
      <td class="textogray"><strong>Fecha de Creaci&oacute;n</strong></td>
      <td width="190"><input name="FechaIniCrea" type="text" class="iptxtn" id="FechaIniCrea" value="{$FechaIniCrea}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('CreaIni',document.{$frmName}.FechaIniCrea,document.{$frmName}.FechaIniCrea,popCalCreaIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalCreaIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
      <td width="10">&nbsp;  </td>
      <td><input name="FechaFinCrea" type="text" class="iptxtn" id="FechaFinCrea" value="{$FechaFinCrea}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('CreaFin',document.{$frmName}.FechaFinCrea,document.{$frmName}.FechaFinCrea,popCalCreaFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalCreaFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
    </tr>
	-->	
    <tr> 
      <td class="textogray"><strong>Fecha de Entrega Courier </strong></td>
      <td width="190" align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
    		<input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="{$datos.fecDesIni}" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
      <td width="10">&nbsp;  </td>
      <td align="left"><input name="desFechaFin" type="text" class="iptxtn" id="desFechaFin" value="{$desFechaFin}" tabindex="5" onKeyPress="ninguna_letra();" />
    <input name="fecDesembarqueFin" type="hidden" id="fecDesembarqueFin" value="{$datos.fecDesFin}" />
	 &nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
	 <div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div>
	 </td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Fecha de Diligencia</strong></td>
      <td width="190" align="left"><input name="FechaIniDili" type="text" class="iptxtn" id="FechaIniDili" value="{$FechaIniDili}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('DiliIni',document.{$frmName}.FechaIniDili,document.{$frmName}.FechaIniDili,popCalDiliIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalDiliIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
      <td width="10">&nbsp;  </td>
      <td align="left"><input name="FechaFinDili" type="text" class="iptxtn" id="FechaFinDili" value="{$FechaFinDili}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('DiliFin',document.{$frmName}.FechaFinDili,document.{$frmName}.FechaFinDili,popCalDiliFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalDiliFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Fecha de Verificaci&oacute;n</strong></td>
      <td width="190" align="left"><input name="FechaIniVeri" type="text" class="iptxtn" id="FechaIniVeri" value="{$FechaIniVeri}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('VeriIni',document.{$frmName}.FechaIniVeri,document.{$frmName}.FechaIniVeri,popCalVeriIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalVeriIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
      <td width="10">&nbsp;  </td>
      <td align="left"><input name="FechaFinVeri" type="text" class="iptxtn" id="FechaFinVeri" value="{$FechaFinVeri}" tabindex="4" onKeyPress="ninguna_letra();" />
    		&nbsp;&nbsp;&nbsp;<a href="javascript:;"  onclick="fPopCalendar('VeriFin',document.{$frmName}.FechaFinVeri,document.{$frmName}.FechaFinVeri,popCalVeriFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
			<div id="popCalVeriFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
    </tr>
  </table>
</form>
