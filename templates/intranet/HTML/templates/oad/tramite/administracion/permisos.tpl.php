{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}

-->
</script>
<!-- {/literal}-->
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}">
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="10" class="item">{$errors}</td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td colspan="3" class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      <div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="10" width="700"> 
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="td-lb-login" width="130"><b>DEPENDENCIA</b></td>
            <td width="100"><select name="dependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_ASIGNA_DERECHOS}')">
                {$selDependencia}
              </select></td>
            <td class="td-lb-login" width="150"><b>&nbsp;SUBDEPENDENCIA</b></td>
            <td width="100"><select name="subDependencia" class="ip-login contenido" onChange="submitForm('{$accion.FRM_ASIGNA_DERECHOS}')">
                {$selSubDependencia}
              </select></td>
            <td class="td-lb-login" width="150"><b>&nbsp;TRABAJADOR</b></td>
            <td width="100" valign="top"><select name="trabajador" class="ip-login contenido">
                {$selTrabajador}
              </select></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="10"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="10" align="right" class="texto">
<input type="button" name="Busca" class="but-login" value="Desplegar" onClick="submitForm('{$accion.FRM_ASIGNA_DERECHOS}')">
        &nbsp;&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="4" width="540">&nbsp; </td>
    </tr>
    <tr> 
      <td width="540" rowspan="2" align="center" bgcolor="#CCE7F9" class="texto"><b>NOMBRE</b></td>
      <td colspan="17" align="center" width="240" class="texto" bgcolor="#CCE7F9"><center><b>DERECHOS</b></center></td>
    </tr>
    <tr> 
      <td align="center" width="240" bgcolor="#DFEDF6" class="texto">Resoluci&oacute;n</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Correspondencia</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Finalizaci&oacute;n</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Archivo</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Consulta Internos </td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Anulaci&oacute;n</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Correcci&oacute;n</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Administracci&oacute;n</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Coordinador</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Adm. Documentos<br>(Lectura)</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Admin<br>(OGTIE)</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Papeletas<br>Dep&oacute;sito</td>	
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Cert.<br>Retenciones</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Dev. x<br>Decomiso</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Doc. <br>Garant&iacute;a</td>	
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Utilitarios</td>
      <td align="center" width="30" bgcolor="#DFEDF6" class="texto">Control<br>Individual<br>de Documentos</td>	  		    	  		    	  
    </tr>
	{$aa}
    <tr> 
      <td colspan="10" class="item"> <div align="right"><strong> </strong></div></td>
    </tr>
    <tr> 
      <td colspan="10"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="10"> <input name="bSubmit" type="Submit" class="submitV2" value="INGRESAR DERECHOS" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="CANCELAR" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_ASIGNA_DERECHOS}&menu={$accion.FRM_ASIGNA_DERECHOS}&subMenu={$accion.FRM_ASIGNA_DERECHOS}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ASIGNA_DERECHOS}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Busca" type="hidden" id="Busca" value="1"> 
		 </td>
    </tr>
  </table>
</form>