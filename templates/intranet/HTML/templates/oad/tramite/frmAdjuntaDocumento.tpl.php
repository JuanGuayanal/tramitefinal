{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('destino','Dependencia destino','Sel','Persona','Persona destino','Sel','documento','El tipo de Documento','Sel','Contenido','El Contenido','R','Folios','N�mero de folios','R');return document.MM_returnValue">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> <br>
      <td width="25%" class="item"><strong>No del documento </strong></td>
      <td colspan="3" class="item">{$nroadjunto} {$Anyo}<strong class="item">-CONVENIO_SITRADOC</strong></td>
    </tr>
	{if $contadorF>=1}
    <tr> 
      <td colspan="4" class="item"><center><strong>EL DOCUMENTO YA EST� FINALIZADO</strong></center></td>
    </tr>
	{/if} 
    <tr> 
      <td class="item"><strong>Fecha de Emisi&oacute;n</strong></td>
      <td width="26%" class="item">{$FechaActual}</td>
      <td width="23%" class="item"><strong>Hora del Emisi�n</strong></td>
      <td width="26%" class="item">{$HoraActual}</td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Destino</strong></td>
      <td colspan="3"><select name="Destino" class="ipsel2" onChange="submitForm('{$accion.FRM_ADJUNTA_DOCUMENTO}');" >
	  {$selDestino}
       </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Se&ntilde;or</strong></td>
      <td colspan="3"> <select name="Persona" class="ipsel2" onChange="submitForm('{$accion.FRM_ADJUNTA_DOCUMENTO}');">
	  {$selPersona}
	   </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Tipo documento</strong></td>
      <td> <select name="Documento" class="ipsel2" id="Documento" onChange="submitForm('{$accion.FRM_ADJUNTA_DOCUMENTO}');">
	  {$selDocumento}
       </select> </td>
      <td class="item"><div align="right"><strong>N&deg; de Folios</strong></div></td>
      <td><input name="Folios" type="text" class="iptxt1" onKeyPress="solo_num();" value="{if $Folios==''}1{else}{$Folios}{/if}" size="4" maxlength="3"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Contenido</strong></td>
      <td colspan="3"> <textarea name="Contenido" cols="70" rows="3" class="iptxt1">{if ($Documento==25||$Documento==26||$Documento==27)}{if ($Documento==25)}Recurso de Reconsideraci�n contra la {$RD}{elseif $Documento==26}Recurso de Apelaci�n contra la {$RD}{else}Recurso de Nulidad contra la {$RD}{/if}{else}{$Contenido}{/if}</textarea> 
      </td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong></td>
      <td colspan="3"> <textarea name="Observacion" cols="70" rows="3" class="iptxt1">{$Observacion}</textarea> 
      </td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> <input type="submit" name="Submit" value="Adjuntar Documento" class="submit"> 
        &nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ADJUNTA_DOCUMENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
        <input name="id" type="hidden" id="id" value="{$id}">
		<input name="reLoad" type="hidden" id="reLoad" value="1"> 
		<input name="nroadjunto" type="hidden" id="nroadjunto" value="{$nroadjunto}"></td>
    </tr>
  </table>
</form>