<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE ADJUNTOS ENTRE LAS FECHAS 
        {$fecInicio2} Y {$fecFin2}</font> <br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999">
  <td width="4%"> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td> 
    <td width="10%"> 
      <div align="center"><font size="5"><strong>N&deg; DE ADJUNTO</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>FECHA DE ADJUNTO</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>REMITENTE</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="8%"> 
      <div align="center"><font size="5"><strong>TIPO DE DOCUMENTO</strong></font></div></td>
    <td width="12%"> 
      <div align="center"><font size="5"><strong>DOC EXT Y/O N&deg; TICKET</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>FOLIO</strong></font></div></td>
    <td width="12%"> 
      <div align="center"><font size="5"><strong>UNIDAD DESTINO</strong></font></div></td>
    <td width="12%"> 
      <div align="c5ter"><font size="5"><strong>UNIDAD ORIGEN</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr>
  	<td width="4%"><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td> 
    <td width="10%"><font size="5">{$list[i].numAdj}</font></td>
    <td width="10%"><font size="5">{$list[i].fecha}</font></td>
    <td width="10%"><font size="5">{$list[i].remitente}</font></td>
    <td width="10%"><font size="5">{$list[i].asunto}</font></td>
    <td width="8%"><font size="5">{$list[i].tipDoc}</font></td>
    <td width="12%"><font size="5">{$list[i].ndocumento}</font></td>
    <td width="10%"><font size="5">{$list[i].folios}</font></td>
    <td width="12%"><font size="5">{$list[i].destino}</font></td>
    <td width="15%"><font size="5">{$list[i].origen}</font></td>
  </tr>
  {sectionelse}  
  <tr>
    <td colspan="6"><div align="center"><font size="5"><strong>No se han ingresado 
        adjuntos en las fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section}
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
