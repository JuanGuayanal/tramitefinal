<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000" size="6"> DOCUMENTOS RECIBIDOS A LA FECHA
        DEL CONGRESO DE LA REP&Uacute;BLICA</font></b></font><br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
	
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
  <tr bgcolor="#999999"> 
    <td width="5%" align="center"><font size="5"><strong>N&deg;</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
    <td width="10%" align="center"><font size="5"><strong>N&deg; DE REG. OTD</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
    <td width="18%" align="center"><font size="5"><strong>RAZ&Oacute;N SOCIAL</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
	<td width="58%" align="center"><font size="5"><strong>ASUNTO</strong></font></td>
    <td width="3%" align="center"><font size="5"><strong>D&Iacute;AS EN CURSO</strong></font></td>
    <td width="15%" align="center"><font size="5"><strong>DOCUMENTO</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>FECHA DE INGRESO</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>UBICACI&Oacute;N</strong></font></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial">{$smarty.section.i.iteration}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].tram}</font></td>
	<td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].razonsocial}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].asunto}</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$list[i].nroDias}</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$list[i].doc}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$list[i].fecRec}</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5" >{$list[i].depe}{if $list[i].trab}/{$list[i].trab}{/if}</font></div></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="12" align="center" bgcolor="#FFFFFF"><font size="5"><strong>NO 
      EXISTEN DOCUMENTOS INGRESADOS A LA FECHA</strong></font></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>