<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>

<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        </b></font>
      <p align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="7">DISTRIBUCI&Oacute;N 
        DE LA DOCUMENTACI&Oacute;N EN {$SiglaSubDep}</font><br>
        </font></b></font> <br>
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>

<div align="center">
  <p>&nbsp;</p>
  <table width="68%" border="1">
    <tr bgcolor="#999999"> 
      <td width="85%"><div align="center"><font color="#000000" size="5"><strong>TRABAJADOR</strong></font></div></td>
      <td width="15%"><div align="center"><font color="#000000" size="5"><strong>DERIVADOS</strong></font></div></td>
      <td width="15%"><div align="center"><font color="#000000" size="5"><strong>ACEPTADOS</strong></font></div></td>
    </tr>
    {section name=i loop=$list} 
    <tr> 
      <td><div align="left"><font size="5">{$list[i].nomTrab}</font></div></td>
      <td><div align="center"><font size="5">{$list[i].nroDocDer}</font></div></td>
      <td><div align="center"><font size="5">{$list[i].nroDocAcep}</font></div></td>
    </tr>
    {/section} 
    {if ($cargo==5||$cargo==7)}<tr bgcolor="#999999"> 
      <td><div align="left"><font size="5">TOTALES</font></div></td>
      <td><div align="center"><font size="5">{$totCargaDoc}</font></div></td>
      <td><div align="center"><font size="5">{$totCargaAcept}</font></div></td>
    </tr>{/if}
  </table>
</div>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen2}{$hora}s</i></font></p>
</body>
</html>
