{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function cargar(pForm)
{
	var texto=pForm.tipDocumento.value;
	var texto2=pForm.tipBusqueda.value;
		pForm.action="index.php?accion=frmSearchDocTrab&subMenu=frmSearchDocTrab&menu=SumarioTrab&tipDocumento="+texto+"&tipBusqueda"+texto2;
	    pForm.bsubmit.disabled=true;
		pForm.submit();
}
-->
</script>
<!-- {/literal} -->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" >
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DOCTRAB}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  {if $msjAlerta==1}<font size=1><strong><center>
    �La ejecuci�n de la tarea se ha llevado a cabo con �xito!
  </center></strong></font>{/if}
	{if $indInt}
		<font size=1><strong><center>
		El n�mero es {$claseindInt} {$indInt} 
		</center></strong><br></font>
	{/if}	
  <table width="740" height="30" border="0" align="center" cellpadding="0" cellspacing="2" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif"  class="TEmb">
    <tr> 
      <td width="60" rowspan="14" class="textoblack"><!--<img src="/img/800x600/dirinternas.jpg">-->
	  <img src="/img/administracion.gif" width="170" height="160"></td>
      <td width="60" class="textoblack"><strong>Tipo de Documento</strong></td>
      <td colspan="2" width="400"><!--<select name="tipDocumento" class="ipsel2" onChange="document.{$frmName}.tipDocumento.selected=true;cargar(document.{$frmName})">-->
	  <select name="tipDocumento" class="ipsel2" onChange="document.{$frmName}.tipDocumento.selected=true;submitForm('{$accion.FRM_BUSCA_DOCTRAB}');">
          <option value="3"{if $tipDocumento==3} selected{/if}>Todos</option>
          <option value="1"{if $tipDocumento==1} selected{/if}>Documento Externo</option>
          <option value="2"{if $tipDocumento==2} selected{/if}>Expediente</option>
          <option value="4"{if $tipDocumento==4} selected{/if}>Documento Interno</option>
        </select> </td>
      <td width="200" valign="middle"> <div align="center"> 
          <input name="bsubmit" type="submit" class="submit" value="Buscar" onClick="cargar(document.{$frmName})">
        </div></td>
    </tr>
    <tr> 
      <td width="60" class="textoblack"><strong>Estado del Documento</strong></td>
      <td colspan="3"> <!--<select name="tipBusqueda" class="ipsel2" id="tipBusqueda" onChange="document.{$frmName}.tipBusqueda.selected=true;cargar(document.{$frmName})">-->
	  <select name="tipBusqueda" class="ipsel2" id="tipBusqueda" onChange="submitForm('{$accion.FRM_BUSCA_DOCTRAB}');">
          <option value="9"{if $tipBusqueda==9} selected{/if}>Todos</option>
          <option value="1"{if $tipBusqueda==1} selected{/if}>Recibido (Otra Dependencia)</option>
		  {if $tipDocumento==4}
          <option value="2"{if $tipBusqueda==2} selected{/if}>Recibido (Misma 
          Dependencia)</option>
          <option value="4"{if $tipBusqueda==4} selected{/if}>Enviado (Misma Dependencia)</option>
		  {/if}
		  <option value="3"{if $tipBusqueda==3} selected{/if}>Finalizado</option>
          <!--<option value="2"{if $tipBusqueda==2} selected{/if}>Derivados</option> -->
        </select> </td>
    </tr>
    <tr> 
      <td colspan="4" align="right" class="textogray" ><hr width="100%" size="1"></td>
    </tr>
    {if $tipDocumento!=4} 
    <tr> 
      <td class="texto"><strong>N&uacute;mero de Tr&aacute;mite Documentario 
        </strong></td>
      <td colspan="3"><input name="nroTD" type="text" class="iptxt1" value="{$nroTD}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>N&uacute;mero de Adjunto 
        </strong></td>
      <td colspan="3"><input name="nroAnexo" type="text" class="iptxt1" value="{$nroAnexo}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Raz&oacute;n Social</strong></td>
      <td colspan="3"><input name="RZ" type="text" class="iptxt1" value="{$RZ}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>RUC </strong></td>
      <td colspan="3"><input name="ruc" type="text" class="iptxt1" value="{$ruc}" size="30" maxlength="11"></td>
    </tr>
	{/if}
    <tr> 
      <td class="texto"><strong>Indicativo/Oficio </strong></td>
      <td colspan="3">
	  {if ($tipBusqueda==1||$tipBusqueda==3)}<select name="tipodDoc" class="ipsel2" id="tipodDoc">
			{$selTipodDoc}
        </select>{/if}
	  <input name="indicativo" type="text" class="iptxt1" value="{$indicativo}" size="10" maxlength="100">
	  {if ($tipBusqueda==1||$tipBusqueda==3)}
	    <select name="anyo3" class="ipsel2" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo3==2004} selected{/if}>2004</option>
					<option value="2005"{if ($anyo3==2005)} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo3==2007} selected{/if}>2007</option>-->
					<option value="2013" {if $anyo3==2013} selected{/if}>2013</option>
					</select>
	  	<select name="siglasDep" class="ipsel2" id="siglasDep">
			{$selSiglasDep}
			<option value="VMP"{if $siglasDep=="VMP"} selected{/if}>Vmp</option>
        </select>
	  {/if}
	  </td>
    </tr>
    <tr> 
      <td class="texto"><strong>C&oacute;digo Interno</strong></td>
      <td colspan="3"><input name="codigoInterno" type="text" class="iptxt1" value="{$codigoInterno}" size="30" maxlength="100"></td>
    </tr>	
    {if $tipDocumento==2} 
    <tr> 
      <td class="textogray"><strong>N&uacute;mero de Procedimiento</strong></td>
      <td colspan="4" class="textogray"><input name="procedimiento" type="text" class="iptxt1" value="{$procedimiento}" size="30" maxlength="100"></td>
    </tr>
    {/if} 
    <tr> 
      <td class="textogray"><strong>Asunto</strong></td>
      <td colspan="3"><input name="asunto" type="text" class="iptxt1" value="{$asunto}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Observaciones</strong></td>
      <td colspan="3"><input name="observaciones" type="text" class="iptxt1" value="{$observaciones}" size="30" maxlength="100"></td>
    </tr>
	<!--
    <tr> 
      <td class="textogray" width="60"><strong>Entre las fechas</strong></td>
      <td colspan="2"><select name="dia_ini" class="ipsel2" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipsel2" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
      {$selAnyoIng}
        </select></td>
      <td  ><select name="dia_fin" class="ipsel2" id="dia_fin">
      {$selDiaSal}
	    </select> <select name="mes_fin" class="ipsel2" id="select">
	  {$selMesSal}
        </select> <select name="anyo_fin" class="ipsel2" id="select2">
      {$selAnyoSal}
        </select> </td>
    </tr>
	-->
    <tr> 
      <td class="textogray" width="60"><strong>Entre las fechas</strong></td>
      <td colspan="2"><input name="FechaIni" type="text" class="iptxt1" id="FechaIni" value="{$FechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.FechaIni,document.{$frmName}.FechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td  >&nbsp;<input name="FechaFin" type="text" class="iptxt1" id="FechaFin" value="{$FechaFin}"  tabindex="5" onKeyPress="ninguna_letra();" />
      &nbsp;&nbsp;<!--<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.FechaFin,document.{$frmName}.FechaFin,popCalFin,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>-->
	  <a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.FechaFin,document.{$frmName}.FechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
	 <div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
    </tr>	
    <tr> 
      <td colspan="4" class="textored"><strong>&iexcl;Para interactuar con los 
        documentos debe seleccionar el estado del documento: Recibido, previamente darle &quot;OK&quot;!</strong></td>
    </tr>
  </table>
  </form>
