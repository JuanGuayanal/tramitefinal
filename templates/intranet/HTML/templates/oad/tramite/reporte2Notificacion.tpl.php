<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="7">LISTADO DE NOTIFICACIONES</font></font></b></font>
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table width="100%" border="1" align="center">
  <tr bgcolor="#B9B9B9"> 
    <td> 
      <div align="center"><font size="+3"><strong>ID</strong></font></div></td>
    <td> 
      <div align="center"><strong><font size="+3">N&deg; NOTIFICACI&Oacute;N</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="+3">N&deg; RESOLUCI&Oacute;N</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="+3">DESTINATARIO</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="+3">FECHA/HORA INGRESO</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="+3">USUARIO</font></strong></div></td>
  </tr>
  {section name=i loop=$not} 
  <tr> 
    <td><font size="6"><strong>{$smarty.section.i.iteration}</strong></font></td>
    <td><font size="6"><strong>{$not[i].nro}</strong></font></td>
    <td><font size="6"><strong>{$not[i].resol}</strong></font></td>
    <td><font size="6"><strong>{$not[i].dest}</strong></font></td>
    <td><div align="center"><font size="6"><strong>{$not[i].fec}</strong></font></div></td>
    <td><font size="6"><strong>{$not[i].usuario}</strong></font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="6" align="center" bgcolor="#FFFFFF"><strong><font size="6">No 
      existen notificaciones en las fechas solicitadas.</font></strong></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="6" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>
