<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Sistema de Tr�mite Documentario - C�digo Interno {if ($idDocPadre>0)}{$idDocPadre}{else}{$idMemo}{/if}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/styles/intranet.css" rel="stylesheet" type="text/css">
</head>
<body>
<center>
<br>
  <table width="50%" border="0" cellspacing="0">
   <tr> 
      <td colspan="4"><img src="../../../../../../images/0/0/logomidis.gif" width="220" height="68"></td>
    </tr>
    <tr> 
      <td colspan="4"><table width="100%" border="0" cellspacing="0">
          <tr> 
            <td class="texto-sitradoc14"><div align="center"><strong>HOJA DE DERIVACION N&deg; {$indicativo}<!---{$anyo}-CONVENIO_SITRADOC/{$siglas}-->.<br>
			<hr width="80%" size="1" noshade></strong></div></td>
          </tr>
        </table></td>
    </tr>

	
	    <tr> 
      <td valign="top" class="texto-sitradoc"><strong>FECHA</strong></td>
      <td valign="top" class="texto-sitradoc"><strong>:</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td valign="top" class="texto-sitradoc"><strong></strong>{if $fecMemo}{$fecMemo}{else}{$FechaActual}{/if}</td>
    </tr>
	    <tr>
      <td width="9%" valign="top">
      <strong>&nbsp;</strong></td>
      <td width="5%" valign="top"><strong>
      &nbsp;</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top">
      <strong></strong>&nbsp;</td>
    </tr>
    <tr>
      <td width="9%" valign="top" class="texto-sitradoc"><strong>A</strong></td>
      <td width="5%" valign="top" class="texto-sitradoc"><strong>:</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top" class="texto-sitradoc"><strong></strong>{$depDestino}</td>
    </tr>
  <!--  <tr>
      <td width="9%" valign="top">
      <strong>&nbsp;</strong></td>
      <td width="5%" valign="top"><strong>
      &nbsp;</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top">
      <strong></strong>&nbsp;</td>
    </tr>	
    <tr>
      <td width="9%" valign="top" class="texto-sitradoc"> 
        <strong>ASUNTO</strong></td>
      <td width="5%" valign="top" class="texto-sitradoc"><strong>:</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top" class="texto-sitradoc">
        <strong></strong>{$asunto}</td>
    </tr>
	-->
    <tr>
      <td width="9%" valign="top">
      <strong>&nbsp;</strong></td>
      <td width="5%" valign="top"><strong>
      &nbsp;</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top">
      <strong></strong>&nbsp;</td>
    </tr>		
    <tr> 
      <td valign="top" class="texto-sitradoc"><strong>REFERENCIA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
      <td valign="top" class="texto-sitradoc"><strong>:</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td valign="top" class="texto-sitradoc"><strong><!--:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $referencia}{$tipo} {$referencia}{else}{if $asunto!="sin asunto"&& $asunto!="Sin asunto"}{$asunto}{/if}{/if}-->
	  				{if $referencia}{if $tipo}{$tipo} {/if}{$referencia}<br>{/if}
	                          {section name=i loop=$refs}
	                          {$refs[i].ind}<br>{/section}
							  {if $refSinResp}{$refSinResp}{/if}							  
	   </strong> </td>
    </tr>
	
		    <tr>
      <td width="9%" valign="top">
      <strong>&nbsp;</strong></td>
      <td width="5%" valign="top"><strong>
      &nbsp;</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top">
      <strong></strong>&nbsp;</td>
    </tr>
    <tr>
      <td width="9%" valign="top" class="texto-sitradoc"><strong> ACCION A REALIZAR</strong></td>
      <td width="5%" valign="top" class="texto-sitradoc"><strong>:</strong></td>
	  <td width="5%"><strong>&nbsp;</strong></td>
      <td width="80%" valign="top" class="texto-sitradoc"></td>
    </tr>	

    <tr> 
      <td colspan="4" width="100%" class="texto-sitradoc">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="4"><table width="100%" border="1" cellspacing="1">
          <tr> 
            <td width="160" class="texto-sitradoc">TRAMITE</td>
            <td width="27">{if $a1=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox" value="checkbox">
              {/if}</td>
            <td width="160" class="texto-sitradoc">NOTIFICAR AL INTERESADO</td>
            <td width="31">{if $a2=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox5" value="checkbox">
              {/if}</td>
            <td width="180" class="texto-sitradoc">COORDINAR</td>
            <td>{if $a3=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox9" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="160" class="texto-sitradoc">ARCHIVAR</td>
            <td width="27">{if $a4=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox2" value="checkbox">
              {/if}</td>
            <td width="160" class="texto-sitradoc">POR CORRESPONDERLE</td>
            <td width="31">{if $a5=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox6" value="checkbox">
              {/if}</td>
            <td width="180" class="texto-sitradoc">AGREGAR AL EXPEDIENTE</td>
            <td>{if $a6=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox10" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="160" class="texto-sitradoc">EJECUCION</td>
            <td width="27">{if $a7=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox3" value="checkbox">
              {/if}</td>
            <td width="160" class="texto-sitradoc">PREPARAR RESPUESTA</td>
            <td width="31">{if $a8=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox7" value="checkbox">
              {/if}</td>
            <td width="180" class="texto-sitradoc">SEGUIMIENTO</td>
            <td>{if $a9=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox11" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="160" class="texto-sitradoc">GEST V&ordm;B&ordm; Y/O FIRMA</td>
            <td width="27">{if $a10=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox4" value="checkbox">
              {/if}</td>
            <td width="160" class="texto-sitradoc">PROYECTAR RESOLUCION</td>
            <td width="31">{if $a11=='T'} <img src="/img/800x600/stat_true.gif">{else} 
              <input type="checkbox" name="checkbox8" value="checkbox">
              {/if}</td>
            <td width="180" class="texto-sitradoc">RECOMENDACION</td>
            <td>{if $a12=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox12" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="160" class="texto-sitradoc">OPINAR O INFORMAR</td>
            <td>{if $a13=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox17" value="checkbox">
              {/if}</td>
            <td width="160" class="texto-sitradoc">CONOCIMIENTOS Y FINES</td>
            <td>{if $a14=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
            <td width="180" class="texto-sitradoc">OTROS</td>
            <td>{if $a15=='T'} <img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
          </tr>

        </table></td>
    </tr>
	 <tr> 
      <td colspan="4" width="100%" class="texto-sitradoc">&nbsp;</td>
    </tr>
	 <tr> 
      <td colspan="4" >
	  <table  width="100%" border="1" cellspacing="1">
  <tr>
  <td class="texto-sitradoc" width="80%" valign="top" height="200"><p><strong>OBSERVACIONES:</strong></p>
  {$observaciones}
  </td>
  <td class="texto-sitradoc" width="20%" align="center" valign="top"><strong>V�B�</strong></td>
  </tr>
  </table>
	  </td>
    </tr>
  </table>
  <br>
  <br>
  
</center>

<!--<table width="80%" border="0" align="center">
  <tr> 
    <td class="texto-sitradoc14" align="center"><br><br>Atentamente,<br><br><br><br></td>
  </tr>
  <tr> 
    <td > <div align="center"></div></td>
  </tr>
  <tr> 
    <td > <div align="center"></div></td>
  </tr>
  <tr> 
    <td > <div align="center"></div></td>
  </tr>
  {if $codigoDep!=152}
  <tr> 
    <td class="texto-sitradoc14"> <div align="center"><strong>
	{if ($codigoDep==12&& ($direAnt==1||$direAnt==2))}
		{if ($direAnt==1)} <span style=" font-size:12px;"> {$director}</span> {else}PERCY SALAS{/if}
	{else}
		{if ($codigoDep==100006)}KITTY ELISA TRINIDAD GUERRERO
		{else}
			{if ($codigoDep==5 && $idMemo < 9382818)}
			MARCELO GONZALO CEDAMANOS RODRIGUEZ
			{else}
					{if ($codigoDep==5 && $idMemo < 9568961)}
							M. MILAGRO DELGADO ARROYO
					{else}
						{if ($codigoDep==10006)}JOS� LUIS CHICOMA LUCAR
						{else}
							{if ($idMemo<10036251 && $codigoDep==16)}
								ELSA PATRICIA GALARZA CONTRERAS
							{else}
								{if $codigoDep==170}
								<span style=" font-size:10px;">{$director}</span>
								{else}
								{$director}
								{/if}
							{/if}
						{/if}
					{/if}
			{/if}
		{/if}
	{/if}</strong></div></td>
  </tr>
  {/if}
  <!-- 21/03/2013-->
 <!-- {if $codigoDep!=5}
  <tr>  -->
    <!--<td class="texto-sitradoc"> <div align="center"><strong>{if $codigoDep!=16}{$cargo}{/if}{if $codigoDep==16}Viceministro de Pesquer&iacute;a(e){/if}</strong></div></td>-->
<!--    <td class="texto-sitradoc"> <div align="center"><strong> 
	{if $codigoDep==170}
	R.M. N�164-2012-CONVENIO_SITRADOC
	{else}
	{$cargo}
	{/if}
	</strong></div></td>
  </tr>
  {/if}
  -->
 <!-- {if ($codigoDep!=15 && $codigoDep!=152)}
  <tr> --> 
    <!-- <td class="texto-sitradoc"> <div align="center"><strong>{if $codigoDep==5}SECRETARIO GENERAL{else}{if ($codigoDep!=16 && $codigoDep!=36 && $codigoDep!=50)}{$descripcion}{/if}{/if}</strong></div></td> -->
<!--	<td class="texto-sitradoc"> <div align="center"><strong>{if $codigoDep==5}SECRETARIO GENERAL{else} {if $codigoDep==62}{else} {if ($codigoDep!=16 && $codigoDep!=36 && $codigoDep!=50)}{$descripcion}{/if}{/if}{/if}</strong></div></td> -->
<!--  </tr>-->
<!--  {/if} -->

</table>


{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
//window.onafterprint = function() {window.close()}   07/06/2013
window.onafterprint = function() {return false;}
function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>