{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
-->
</script>
<!-- {/literal} -->
<br>
<div style="width:700px;">
<div class="std_form">
<form name="{$frmName}" action="{$frmUrl}" method="post" {if ($GrupoOpciones1!=10)} target="_blank"{/if}>
  <input name="page" type="hidden" id="page2">
  <!--<input name="accion" type="hidden" id="accion" value="{$accion.GENERAREPORTETRAB}">-->
  <input name="accion" type="hidden" id="accion" value="GeneraReporteDir">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <input type="hidden" name="GrupoOpciones1" value="21" />
  <input type="hidden" name="destino" value="0" />
<table width="800" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
	<tr> 
		<td align="right" class="label"><b>Tipo de Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  <td align="left" class="tree"><select name="tipoDocc" class="ipseln" id="tipDoc">
          <option>Todos</option>
          <option value="1">Documentos Externos</option>
          <option value="2">Expedientes</option>
          <option value="4">Documentos Internos</option>
        </select></td>
    </tr>
	<tr align="left">
    	<td align="right" class="label"><b>Estado del Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><select name="status" class="ipseln" id="status">
          <option value="5">Todos</option>
          <option value="1" selected="">Pendientes</option>
          <option value="11" class="std_oculto">Pendientes (Congreso)</option>
          <option value="3">Generados/Derivados </option>
          <option value="4">Finalizados</option></select></td>
    </tr>
    {if $nivel==2}
	<tr align="left">
    	<td align="right" class="label"><b>Trabajador&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><select name="especialista" class="ipseln" id="especialista">
          <option value="none"></option>{$selTrabajador}</select></td>
    </tr>
    {/if}
    <tr>
    	<td align="right" class="label"><b>Por Fechas</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="left"> <span class="label">Desde</span> <input name="desFechaIni" type="text" class="iptxtn datepicker" id="FechaIni" value="{php}echo '01/01/'.date('Y');{/php}" size="10"/> <span class="label">hasta</span> <input name="desFechaFin" type="text" class="iptxtn datepicker" id="FechaFin" value="{php}echo date('d/m/Y');{/php}" size="10"/></td>
    </tr>
    <tr><td width="80" colspan="2" valign="middle" align="center">&nbsp;</tr>
    <tr><td width="80" colspan="2" valign="middle" align="center"> <input type="submit" class="std_button" value="Generar"> </tr>
</table>
</form>
</div>
</div>