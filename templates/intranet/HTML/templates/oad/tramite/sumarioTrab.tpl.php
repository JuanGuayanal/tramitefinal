<!--
<table width="640" border="6" align="center" cellpadding="0"cellspacing="0" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif" class="TEmb">
  <br>
  <tr> 
    <td align="left"> <table  width="100%" border="0" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
        <tr> 
          <td width="55%" class="texto"><strong>Nuevos Documentos Externos</strong></td>
          <td width="21%" class="textored"><strong>{$DocNuevosExt}</strong></td>
          <td width="24%" rowspan="4" class="textored"><img src="/img/800x600/dirinternas.jpg"></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Documentos Externos Finalizados</strong></td>
          <td class="textored"><strong>{$DocNuevosExt2}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Nuevos Expedientes</strong></td>
          <td class="textored"><strong>{$DocNuevosExp}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Expedientes Finalizados</strong></td>
          <td class="textored"><strong>{$DocNuevosExp2}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Documentos Internos de otra Dependencia</strong></td>
          <td class="textored"><strong>{$DocIntDep}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Documentos Internos Finalizados de otra Dependencia</strong></td>
          <td class="textored"><strong>{$DocIntDep2}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Documentos Internos de misma Dependencia</strong></td>
          <td class="textored"><strong>{$DocIntOf}</strong></td>
        </tr>
      </table></td>
  </tr>
</table>
-->

<br>
  <table width="700" border="0" cellspacing="0" cellpadding="0" class="tabla-encuestas">
    <tr>
      
    <td >&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="center" >       
      <table width="680" border="0" cellspacing="0" cellpadding="2">
        <tr> 
          <td colspan="3" class="td-nada td-encuesta" height="8">&nbsp;</td>
        </tr>
        <tr class="tr-encuesta"> 
          <td width="320" class="td-encuesta texto"><strong>DOCUMENTOS EXTERNOS</strong></td>
          <td width="172" class="texto td-encuesta">{if ($codigoPersona==646||$codigoPersona==46||$codigoPersona==915)}<a href="{$frmUrl}?accion={$accion.MUESTRA_DOCUMENTOS_GENERADOS}" target="_blank">VER</a>{/if}          </td>
          <td width="173" rowspan="9" class="texto td-encuesta"><div align="center"><img src="/img/gerencia.jpg" width="120" height="110"><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELII}&coddep={$codigoDependencia}&dep={$dep}&trabajador={$codigoPersona}" target="_blank"><br><strong>CONSULTA</strong></a></div></td>
        </tr>
        {section name=i loop=$option} 
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta" width="320">&nbsp;&nbsp;&nbsp;En curso </td>
          <td class="texto td-encuesta" width="172"> {$option[i].barra} &nbsp;&nbsp;{$DocNuevosExt} 
          </td>
        </tr>
        {/section}
		<!--
        {section name=i loop=$option8} 
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta" width="320">&nbsp;&nbsp;&nbsp;Documentos Externos Enviados</td>
          <td class="texto td-encuesta" width="345"> {$option8[i].barra8} &nbsp;&nbsp;{$dif1} 
          </td>
        </tr>
        {/section}
		--> 
		{section name=i loop=$option2}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;Finalizados</td>
          <td class="texto td-encuesta">{$option2[i].barra2} &nbsp;&nbsp;{$DocNuevosExt2}</td>
        </tr>
		{/section}
        <tr class="tr-encuesta"> 
          <td width="320" class="td-encuesta texto"><strong>EXPEDIENTES</strong></td>
          <td width="172" class="texto td-encuesta">  
          </td>
        </tr>
		{section name=i loop=$option3}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;En curso </td>
          <td class="texto td-encuesta">{$option3[i].barra3} &nbsp;&nbsp;{$DocNuevosExp}</td>
        </tr>
		{/section}
		<!--
        {section name=i loop=$option9} 
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta" width="320">&nbsp;&nbsp;&nbsp;Expedientes Enviados</td>
          <td class="texto td-encuesta" width="345"> {$option9[i].barra9} &nbsp;&nbsp;{$dif2} 
          </td>
        </tr>
        {/section}
		--> 
		{section name=i loop=$option4}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;Finalizados</td>
          <td class="texto td-encuesta">{$option4[i].barra4} &nbsp;&nbsp;{$DocNuevosExp2}</td>
        </tr>
		{/section}
        <tr class="tr-encuesta"> 
          <td width="320" class="td-encuesta texto"><strong>DOCUMENTOS INTERNOS</strong></td>
          <td width="172" class="texto td-encuesta">  
          </td>
        </tr>
		{section name=i loop=$option5}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;En curso </td>
          <td class="texto td-encuesta">{$option5[i].barra5} &nbsp;&nbsp;{$DocIntDep}</td>
        </tr>
		{/section}
		{section name=i loop=$option6}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;Finalizados</td>
          <td class="texto td-encuesta">{$option6[i].barra6} &nbsp;&nbsp;{$DocIntDep2}</td>
        </tr>
		{/section}
		<!--
		{section name=i loop=$option7}
        <tr class="tr-encuesta"> 
          <td class="texto td-encuesta">&nbsp;&nbsp;&nbsp;Documentos Internos de misma Dependencia</td>
          <td class="texto td-encuesta">{$option7[i].barra7} &nbsp;&nbsp;{$DocIntOf}</td>
        </tr>
		{/section}
		-->
        <tr> 
          <td colspan="3" class="texto">&nbsp;</td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
