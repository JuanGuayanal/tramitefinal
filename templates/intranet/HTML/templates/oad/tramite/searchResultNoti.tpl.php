{literal}
<script language="JavaScript">
function MinReq(pFrm){
	if(pFrm.elements['ids[]'].length!=undefined){
		for(var i=0;i<pFrm.elements['ids[]'].length;i++)
			if(pFrm.elements['ids[]'][i].checked)
				return true
	}else{
		if(pFrm.elements['ids[]'].checked)
			return true
	}
	return false
}
function SubmitForm3(pFrm,pAcc,pMenu){
	if(MinReq(pFrm)){
		pFrm.accion.value=pAcc
		pFrm.menu.value=pMenu
		//pFrm.menu.value='SumarioDir'
		pFrm.subMenu.value=pAcc
		pFrm.submit()
	}else
		alert('Debe seleccionar al menos un Documento')
}
function EntregaCourier(pFrm,pAcc,pMenu){
	if(MinReq(pFrm)){
		if(confirm('żUd. está seguro que desea ENTREGAR AL COURIER los Documentos seleccionados?'))
			SubmitForm3(pFrm,pAcc,pMenu)
	}else
		alert('Debe seleccionar al menos un Documento a Entregar')
}
function selecciontodo(p,ppForm){
	  if(p==1){
	  var valor=true;
	  }else{
	  var valor=false;
	  }
	  //alert(ppForm);
	  for(i=0;i<ppForm.elements['ids[]'].length;i++){
		  ppForm.elements['ids[]'][i].checked=valor;
		}
 }

</script>
{/literal}
<form name="{$frmName}" action="{$frmUrl}" method="post">
{foreach key=name item=value from=$datos}
  <input type="hidden" name="{$name}" value="{$value}">
{/foreach}
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_NOTIFICACION}">
	<input type="hidden" name="menu">
	<input type="hidden" name="subMenu">
<table width="740" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
		  
          <td align="right" valign="middle" class="textogray" colspan="2"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
		{if ($arrNoti)}
        <tr> 
          <td colspan="2"><hr width="100%" size="1"></td>
        </tr>
          <tr> 
            <td valign="middle">  
              &nbsp;<a href="javascript:SubmitForm3(document.{$frmName},'{$accion.ACEPTA_NOTI}','{$accion.FRM_BUSCA_NOTIFICACION}')"><img src="/img/ico_suge.gif" width="18" height="18" border="0" align="absmiddle" alt="[ ACEPTAR DOCUMENTOS ]"><strong>Visto bueno lo procesado por el Robot?</strong></a>
			  &nbsp;<a href="javascript:EntregaCourier(document.{$frmName},'{$accion.FRM_ENTREGA_COURIER}','{$accion.FRM_BUSCA_NOTIFICACION}')" ><img src="/img/firma2.gif" width="26" height="25" border="0" align="absmiddle" alt="[ ENTREGAR AL COURIER ]"><strong>Courier</strong></a></td>
            <td align="right" valign="middle" class="textogray"> <input type="button" name="todos" class="submit" value="Todos" onclick="selecciontodo(1,document.{$frmName});"> 
              <input type="button" name="todos" class="submit" value="Ninguno" onclick="selecciontodo(2,document.{$frmName});"> 
            </td>
          </tr>
		  {/if}
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrNoti}  
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="27" align="center" valign="middle">{if ($arrNoti[i].dep==12&& !$arrNoti[i].user)}{if $arrNoti[i].fecEntCourier}<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_CORRESPONDENCIA}&idUpdateNot={$arrNoti[i].id}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_MODIFICA_CORRESPONDENCIA}"><img src="/img/800x600/ico-mod.gif" alt="[ Cambiar de estado ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {if !$arrNoti[i].fecEntCourier}<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_CORRESPONDENCIA}&idNoti={$arrNoti[i].id}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_AGREGA_CORRESPONDENCIA}"><img src="/img/devolver.jpg" alt="[ Entregar al Courier ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {/if}</td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>{if ($arrNoti[i].tipCorrespondencia==1)}N&deg; 
                  de Notificaci&oacute;n{else}N&deg; de Documento{/if}</strong></td>
                <td class="texto"><strong>:{if ($arrNoti[i].claseDoc && $arrNoti[i].claseDoc!="" && $arrNoti[i].claseDoc!="NOTIFICACIÓN/ CÉDULA DE NOTIFICACIÓN")}{$arrNoti[i].claseDoc} {/if}{$arrNoti[i].not|default:'No especificado'}</strong></td>
                <td class="textogreen"><!--{if $arrNoti[i].fecNoti}Fecha de 
                  Notificaci&oacute;n: {$arrNoti[i].fecNoti}{/if}--></td>
                <td rowspan="9" class="texto" width="5">
				{if ($arrNoti[i].dep==12)}<input name="ids[]" type="checkbox" id="ids[]" value="{$arrNoti[i].id}" onClick="hL(this)">{/if}				</td>
              </tr>
              <!--
			  <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Funcionario</strong></td>
                <td class="texto">:DIRECCI&Oacute;N DE {$arrNoti[i].siglasDep|default:'No 
                  especificado'}</td>
              </tr>
			  -->
              {if ($arrNoti[i].tipCorrespondencia==1)} 
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Acto que 
                  notifica</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].acto|default:'No especificado'}
					{if ($arrNoti[i].tipCorrespondencia==1 && $arrNoti[i].detal)}
					 expedida por {$arrNoti[i].detal|default:'No especificado'}
					{/if}
				</td>
              </tr>
              {/if} 
			  <!--
			  {if ($arrNoti[i].tipCorrespondencia==1 && $arrNoti[i].detal)} 
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Expidi&oacute; 
                  el acto</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].detal|default:'No especificado'}</td>
              </tr>
              {/if} 
			  -->
			  {if ($arrNoti[i].tipCorrespondencia==1 && $arrNoti[i].proc)} 
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Procedimiento</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].proc|default:'No especificado'}</td>
              </tr>
              {/if} {if $arrNoti[i].flag=='P' && $arrNoti[i].hora> 0 && $arrNoti[i].dia== 
              0} 
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Vigencia</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].vigencia|default:'No especificado'}</td>
              </tr>
              {/if} 
			  
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Destinatario</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].destinatario|default:'No especificado'}</td>
              </tr>
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Domicilio</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].domicilio|default:'No especificado'}</td>
              </tr>
              
			   {if ($arrNoti[i].fecEntCourier&& $arrNoti[i].fecEntCourier!="")}
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Fecha de Entrega al Courier</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].fecEntCourier|default:'No especificado'} (Recepcionado en OADA el {$arrNoti[i].fecRecibido|default:'No especificado'})</td>
              </tr>			   
			   {/if}				    
              <tr>
			    <td width="26%" valign="top" class="sub-item"><strong>Estado</strong></td> 
                <td colspan="2" class="texto"><strong><font color="#FF0000">:{if $arrNoti[i].flag==1}&iexcl;Notificada! - </font><font color="#000000">Fecha de Notificación: {$arrNoti[i].fecNoti} </font><br> Recibido por {$arrNoti[i].nombrePerActo} - Identificación {$arrNoti[i].dniPerActo}{elseif 
                  $arrNoti[i].flag==2}&iexcl;Rechazada!<font color="#000000"> - Fecha de Diligencia: {$arrNoti[i].fecNoti} </font> {elseif $arrNoti[i].flag==3}&iexcl;No 
                  Practicada!</font><font color="#000000"> - Fecha de Diligencia: {$arrNoti[i].fecNoti} </font><br> {if $motivo==1} Desconoce lugar de Notificación {else if $motivo==2} Imposibilidad practicar Notificación{/if}{else}&iexcl;A&uacute;n no se ha notificado!{/if}
				  {if $arrNoti[i].observ&& $arrNoti[i].observ!=""}</font>{$arrNoti[i].observ}{/if}
				  </strong></td>
              </tr>
			  <!--
			  {if $arrNoti[i].fecNoti} 
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Fecha de 
                  Notificaci&oacute;n</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].fecNoti|default:'No especificado'}</td>
              </tr>
			  {/if}
			  -->
			  <!--
			  {if $arrNoti[i].fecRecibido}
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Recibido en OADA</strong></td>
                <td class="texto">:{$arrNoti[i].fecRecibido|default:'No especificado'}</td>
              </tr>
              {/if}
			  -->
			  {if ($arrNoti[i].fecAcepOADA && $arrNoti[i].fecAcepOADA!=" ")}
              <tr> 
                <td width="26%" valign="top" class="sub-item"><strong>Visto bueno por</strong></td>
                <td colspan="2" class="texto">:{$arrNoti[i].userAcepOADA} el {$arrNoti[i].fecAcepOADA|default:'No especificado'} lo procesado por el Robot</td>
              </tr>
              {/if}			  
            </table></td>
        </tr>
      </table>
      {/section}
<!--	  
{if $arrNoti}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" style="width: 15px; height: 25px">&nbsp;</td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 140px"><strong>N&deg; de Notificaci&oacute;n </strong></td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 95px"><strong>Destinatario</strong></td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 130px"><strong>Domicilio</strong></td>
				  <td nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 50px"><strong>Entrega a OADA</strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 50px"><strong>Entrega a Courier</strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 50px"><strong>Notificaci&oacute;n</strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 50px"><strong>V&deg;B&deg; </strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 40px"><strong>Detalles</strong></td>
				  <td align="center" nowrap="nowrap" bgcolor="#FEF7C1" class="item" style="width: 40px"><strong>Reg.</strong></td>
				  <td width="100%" bgcolor="#FEF7C1">&nbsp;</td>
  </tr>
				{section name=d loop=$arrNoti}
		<tr>
						  <td class="item">{if $list[d].flag==1}<a href="{$frmUrl}?accion={$accion.FRM_DEVUELVE_CARTA}&id={$list[d].id}&menu={$accion.FRM_BUSCA_CARTA}&subMenu={$accion.FRM_DEVUELVE_CARTA}" target="_self"><img src="/img/ico_msggrp_o.gif" alt="[ DEVOLVER LA CARTA FIANZA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>
						  <br /><a href="{$frmUrl}?accion={$accion.FRM_EJECUTA_CARTA}&id={$list[d].id}&menu={$accion.FRM_BUSCA_CARTA}&subMenu={$accion.FRM_EJECUTA_CARTA}" target="_self"><img src="/img/firma2.gif" alt="[ EJECUTAR CARTA FIANZA ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>
						  {/if}
						  {if $arrNoti[d].flag==3}<a href="{$frmUrl}?accion={$accion.FRM_SUSPENDE_CARTA}&id={$list[d].id}&menu={$accion.FRM_BUSCA_CARTA}&subMenu={$accion.FRM_SUSPENDE_CARTA}" target="_self"><img src="/img/transfer.gif" alt="[ SUSPENDER LA CARTA FIANZA ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  </td>
						  <td class="textogray">{$arrNoti[d].not}</td>
						  <td class="textogray">{$arrNoti[d].destinatario}</td>
						  <td class="textogray">{$arrNoti[d].domicilio}</td>
						  <td class="textogray">{$arrNoti[d].fecRecibido|default:'No especificado'}</td>
						  <td align="center" class="textogray">{$arrNoti[d].fecEntCourier|default:'No especificado'}</td>
						  <td align="right" class="textogray">{$arrNoti[d].fecNoti|default:'No especificado'}</td>
						  <td align="center" class="textogray">{$arrNoti[d].fecAcepOADA|default:'No especificado'}</td>
						  <td align="center" class="textogray"><strong><font color="#FF0000">{if $arrNoti[d].flag==1}&iexcl;Notificada!</font><font color="#000000"> Recibido por {$arrNoti[d].nombrePerActo} - Identificación {$arrNoti[d].dniPerActo}{elseif 
                  $arrNoti[d].flag==2}&iexcl;Rechazada! {elseif $arrNoti[d].flag==3}&iexcl;No 
                  Practicada!</font><font color="#000000"> {if $motivo==1} Desconoce lugar de Notificación {else if $motivo==2} Imposibilidad practicar Notificación{/if}{else}&iexcl;A&uacute;n no se ha notificado!{/if}</font>
				  {if $arrNoti[d].observ&& $arrNoti[d].observ!=""}{$arrNoti[d].observ}{/if}</strong></td>
						  <td class="textogray">{$arrNoti[d].docInt}</td>
						  <td align="right" class="textogray">&nbsp;</td>
  </tr>
        <tr> 
          <td colspan="11"><hr width="100%" size="1"></td>
        </tr>  
				  {/section}
</table>
{/if}
-->	   
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
</form>