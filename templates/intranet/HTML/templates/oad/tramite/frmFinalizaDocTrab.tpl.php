{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"> <strong>Observaciones</strong></td>
      <td> <textarea name="observaciones" cols="90" rows="4" class="iptxt1" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Finalizar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_TRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.SUMARIO_TRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.FINALIZA_DOCTRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idFinTrab" type="hidden" id="idFinTrab" value="{$idFinTrab}">
		{section name=i loop=$ids} 
		
        <!--<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">-->
		<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">
		
		{/section}
		
		{if $tipDocumento}<input name="tipDocumento" type="hidden" id="tipDocumento" value="{$tipDocumento}">{/if}
		{if $tipBusqueda}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $fecIniTrab2}<input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="{$fecIniTrab2}">{/if}
		{if $fecFinTrab2}<input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="{$fecFinTrab2}">{/if}
		{if $asunto2}<input name="asunto2" type="hidden" id="asunto2" value="{$asunto2}">{/if}
		{if $indicativo}<input name="indicativo" type="hidden" id="indicativo" value="{$indicativo}">{/if}
		{if $observaciones2}<input name="observaciones2" type="hidden" id="observaciones2" value="{$observaciones2}">{/if}
		{if $nroTD}<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">{/if}
		{if $page}<input name="page" type="hidden" id="page" value="{$page}">{/if}
		{if ($siglasDep&& $siglasDep!='none')}<input name="siglasDep" type="hidden" id="siglasDep" value="{$siglasDep}">{/if}
		{if ($tipodDoc&& $tipodDoc!='none')}<input name="tipodDoc" type="hidden" id="tipodDoc" value="{$tipodDoc}">{/if}
		{if ($anyo3&& $anyo3!='none')}<input name="anyo3" type="hidden" id="anyo3" value="{$anyo3}">{/if}
		{if $FechaIni}<input name="FechaIni" type="hidden" id="FechaIni" value="{$FechaIni}">{/if}
		{if $FechaFin}<input name="FechaFin" type="hidden" id="FechaFin" value="{$FechaFin}">{/if}		
		 </td>
    </tr>
  </table>
</form>