{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar esta Acci�n?')){
		MM_validateForm('trab','Trabajador','Sel'{/literal}{if $GrupoOpciones1==1},'nroTD','N�mero de OTD','R'{else},'numero2','Indicativo','R'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})" >
  <table width="725" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors}</td>
    </tr>
    {/if}
    <tr> 
      <td colspan="4" class="item-sep"><strong>Reasignaci�n de un documento que se encuentra en poder de otro trabajador.</strong></td>
    </tr>	 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong> </td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_REASIGNA_DOCUMENTO}')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_REASIGNA_DOCUMENTO}')">
              <strong>Documento Interno</strong></label></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Digite el {if ($GrupoOpciones1==1||!$GrupoOpciones1)}Nro. de Tr&aacute;mite{else}Nro. de documento{/if}</strong></td>
      <td>{if ($GrupoOpciones1==1||!$GrupoOpciones1)}
	  			<input name="nroTD" type="text" class="iptxt1" id="nroTD" value="{$nroTD}" size="30">
	      {else}
	  			<select name="tipoDocc" class="ipsel2" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxt1" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipsel2" id="select">
					<option value="none">Todos</option>
					<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
				</select> -CONVENIO_SITRADOC/
				<select name="siglasDepe2" class="ipsel2" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  	  {/if}&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_REASIGNA_DOCUMENTO}')"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>

	{if (empty($idDocumento)&& $exito==1)}
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no existe el n&uacute;mero o no est&aacute; en la dependencia! </strong></font></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS GENERALES DEL DOCUMENTO </strong></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Ubicaci&oacute;n Actual</strong> </td>
      <td class="item">{$dependencia}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{if $numTram!=""}
    <tr> 
      <td class="texto td-encuesta"><strong>Nro.Tramite</strong> </td>
      <td class="item">{$numTram}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>	
	{/if}
    <tr> 
      <td class="texto td-encuesta"><strong>Detalles</strong> </td>
      <td class="item">{if $GrupoOpciones1==1}{$ind}<br>{$asunto}<br>
      {$fecRec}{else}{$claseDoc} {$ind}<br>
      {$asunto}<br>{$fecRec}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Flujo Documentario </strong></td>
      <td><table width="100%" >
	  		  {section name=i loop=$list} 
				<tr>
				  <td class="item">{$list[i].ind}</td>
				  <td class="item">{$list[i].fDer}</td>
				  <td class="item">{$list[i].depo}</td>
				  <td class="item">{$list[i].depd}</td>
				</tr>
			  {sectionelse}
		
		      {/section}
      	   </table>
	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	<tr> 
    	<td class="texto td-encuesta"> <strong> Trabajadores </strong></td>
		  <td><table width="100%" >
	  		  {section name=i loop=$user} 
				<tr>
				  <td class="item">{$user[i].nombre}</td>
				  <td class="item">{$user[i].diaEnvio} {$user[i].horaEnvio}</td>
				  <td class="item">{$user[i].diaRec} {$user[i].horaRec}</td>
				  <td class="item">{$user[i].obs}</td>
				</tr>
			  {sectionelse}
		
		      {/section}
      	   </table>
	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
	</tr>
	{if $user}
	<tr> 
      <td class="texto td-encuesta"><strong>Pasar a</strong></td>
		  <td><select name="trab" class="ipsel2" id="select">
        			{$selTrab}
				</select>
		  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>
	<tr> 
      <td class="texto td-encuesta"> <strong>Observaciones</strong></td>
		  <td class="item">Reasignado a otro trabajador</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  		  
	</tr>
	{/if}	
	
	{/if}
		
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3">{if $idDocumento>0} <input name="bSubmit" type="Submit" class="submit" value="Guardar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> {/if}
        <input name="accion" type="hidden" id="accion" value="{$accion.REASIGNA_DOCUMENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
        {if $idDocumento>0} 
        <input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
        {/if} 
		</td>
    </tr>
  </table>
</form>