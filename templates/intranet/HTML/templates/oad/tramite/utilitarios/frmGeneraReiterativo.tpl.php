{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('idClaseDoc','Clase de Documento','sel','dependencia','Dependencia','sel','asunto','Asunto','R','observaciones','Observaciones','R'{/literal}{if $dependencia==7&& $radio==1},'domicilio','Domicilio','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="4" class="item-sep"><strong>Generaci�n de un documento a una dependencia de CONVENIO_SITRADOC en base a una referencia que se encuentra fuera de la dependencia</strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
              <strong>Documento Interno</strong></label></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el {if ($GrupoOpciones1==1||!$GrupoOpciones1)}Nro. de Tr&aacute;mite{else}Nro. de documento{/if}</strong></td>
      <td>{if ($GrupoOpciones1==1||!$GrupoOpciones1)}
	  			<input name="nroTD" type="text" class="iptxt1" id="nroTD" value="{$nroTD}" size="30">-
				<select name="anyo3" class="ipsel2" id="select">
					<option value="none">Todos</option>
					<!--<option value="2002"{if $anyo3==2002} selected{/if}>2002</option>
					<option value="2003"{if $anyo3==2003} selected{/if}>2003</option>
					<option value="2004"{if $anyo3==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo3==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo3==2007} selected{/if}>2007</option>-->
					<option value="2013"{if $anyo3==2013} selected{/if}>2013</option>
				</select>
	      {else}
	  			<select name="tipoDocc" class="ipsel2" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxt1" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipsel2" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>-->
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>
				</select> -MIDIS/
				<select name="siglasDepe2" class="ipsel2" id="siglasDepe2">
					
					{$selSiglasDep2}
					<option values="VMP" {if $siglasDepe2=="VMP"} selected{/if}>Vmp</option>
       			</select>
	  	  {/if}&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if (!$exito||$exito==0)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene el acceso para generar un reiterativo!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td>{if $numTram}{$numTram}<br>{/if}{$asunto2}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Ubicaci&oacute;n Actual </strong> </td>
      <td>{$UbiActual}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Clase de Documento</strong></td>
      <td><select name="idClaseDoc" class="ipsel1" id="idClaseDoc" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
        {$selClaseDoc}
        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Derivar a:</strong></td>
      <td><select name="dependencia" class="ipsel1" id="dependencia" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
        {$selDependencia}
        </select>
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if ($dependencia==7)}
    <tr> 
      <td class="item"><strong>&iquest;Genera correspondencia ?</strong> </td>
      <td><input name="radio" type="radio" value="1" {if ($radio==1)} checked{/if} onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"> 
        <strong>Si</strong> <input type="radio" name="radio" value="0" {if ($radio==0||!$radio)} checked{/if} onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"><strong>No</strong></td>
      <td colspan="3"><div align="center"></div></td>
    </tr>
	{/if}	
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td><textarea name="asunto" cols="80" rows="3" class="iptxt1" id="textarea" >{$asunto}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>{if ($dependencia==7&& $radio==1)}Destinatario{else}Observaciones{/if}</strong> </td>
      <td><textarea name="observaciones" cols="80" rows="3" class="iptxt1" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if ($dependencia==7&& $radio==1)}
    <tr> 
      <td class="texto td-encuesta"><strong>Ubigeo</strong> </td>
      <td class="item">
	  		<select name="codDepa" class="ipsel2" id="select3" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
					{$selDepa}
            </select>
			<select name="codProv" class="ipsel2" id="select2" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
					{$selProv}
            </select>
			<select name="codDist" class="ipsel2" id="select" >
					{$selDist}
              </select>
	  </td>
      <td><strong></strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Domicilio</strong> </td>
      <td><textarea name="domicilio" cols="80" rows="3" class="iptxt1" id="textarea" >{$domicilio}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}	
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha de Plazo</strong> </td>
      <td class="item"><input name="desFechaIni" type="text" class="iptxt1" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
    		<input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="{$datos.fecDesIni}" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
      <td><strong></strong></td>
    </tr>
    <tr>
      <td class="lb1Frm"></td>
      <td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td nowrap="nowrap" class="tdSep1"></td>
      <td class="lb1Frm"></td>
      <td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td></td>
    </tr>	
	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="submit" value="Generar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.GENERA_REITERATIVO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		 </td>
    </tr>
  </table>
</form>