{$jscript}

<script language="JavaScript">
<!--
{literal}
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('idClaseDoc','Clase de Documento','sel','dependencia','Dependencia','sel','asunto','Asunto','R','observaciones','Observaciones','R');
		return document.MM_returnValue;
	}else
		return false;
}
 {/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Digite el Nro. de documento</strong></td>
      <td>
	  			<select name="tipoDocc" class="ipsel2" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxt1" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipsel2" id="select">
					<option value="none">Escoja</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
				</select> -CONVENIO_SITRADOC/{$siglasDepe2}
	  	  &nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_ANULA_DOC_DIR}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if (!$exito||$exito==0)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede anular el documento debido a los siguientes motivos:!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td>{if $numTram}{$numTram}<br>{/if}{$asunto2}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Ubicaci&oacute;n Actual </strong> </td>
      <td>{$UbiActual}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	
    <tr> 
      <td colspan="5" class="item"><table width="100%" border="1" cellspacing="0" bordercolor="#000000">
		<tr>
          <td class="textoblack" width="20%" ><div align="left"><strong>CLASE</strong></div></td>
          <td class="textoblack" width="25%" ><div align="left"><strong>INDICATIVO</strong></div></td>
          <td class="textoblack" width="30%" ><div align="left"><strong>ASUNTO</strong></div></td>
          <td class="textoblack" width="20%" ><div align="left"><strong>OBSERVACIONES</strong></div></td>
          <td class="textoblack" width="15%" ><div align="left"><strong>ORIGEN</strong></div></td>
          <td class="textoblack" width="15%" ><div align="left"><strong>DESTINO</strong></div></td>
          <td class="textoblack" width="15%" ><div align="left"><strong>FECHA DERIVACI&Oacute;N</strong></div></td>
          <td class="textoblack" width="15%" ><div align="left"><strong>FECHA DE RECEPCI&Oacute;N</strong></div></td>
          <td class="textoblack" width="15%" ><div align="left"><strong>H</strong></div></td>
          <td class="textoblack" width="15%" ><strong>A</strong></td>
		  <td class="textoblack" width="15%" ><strong>F</strong></td>
        </tr>        
		{section name=i loop=$list}
		<tr>
          <td class="texto" width="20%" ><div align="left">{$list[i].cla}</div></td>
          <td class="item" width="25%" ><div align="left">{$list[i].ind}</div></td>
          <td class="item" width="30%" ><div align="left">{$list[i].asu}</div></td>
          <td class="item" width="20%" ><div align="left">{$list[i].obs}</div></td>
          <td class="item" width="15%" ><div align="left">{$list[i].depO}</div></td>
          <td class="item" width="15%" ><div align="left">{$list[i].depD}</div></td>
          <td class="item" width="15%" ><div align="left">{$list[i].fecDer}</div></td>
          <td class="item" width="15%" ><div align="left">{if ($list[i].fecRec!='')}{$list[i].fecRec}{else}No especificado{/if}</div></td>
          <td class="item" width="15%" ><div align="left"><input name="ids2[]" type="checkbox" id="ids2[]" value="{$list[i].idMov}" onClick="hL(this)" {if ($list[i].derivado==0)} checked{/if}></div></td>
          <td class="item" width="15%" ><input name="ids[]" type="checkbox" id="ids[]" value="{$list[i].idMov}" onClick="hL(this)"></td>
		  <td class="item" width="15%" >{if ($list[i].finalizado==1)}Fin{/if}</td>
        </tr>
		{/section}
      </table></td>
    </tr>
	
    <tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td><textarea name="observaciones" cols="80" rows="3" class="iptxt1" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> 
	  &nbsp;&nbsp; <input name="bSubmit" type="Submit" class="submit" value="Anular" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ANULA_DOC_DIR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		 </td>
    </tr>
  </table>
</form>