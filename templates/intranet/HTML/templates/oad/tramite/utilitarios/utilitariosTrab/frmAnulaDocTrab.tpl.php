{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Anular este Registro?')){
		MM_validateForm('observaciones','Observaciones','R');
		return document.MM_returnValue;
	}else
		return false;
}
 {/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> 
      <td colspan="4" class="item-sep"><strong>Anulaci&oacute;n de un documento creado por el trabajador (No involucra disminuci&oacute;n de numeraci&oacute;n).</strong></td>
    </tr>	 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el Nro. de documento</strong></td>
      <td><select name="tipoDocc" class="ipsel2" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxt1" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipsel2" id="select">
					<option value="none">Escoja</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
				</select> -CONVENIO_SITRADOC/{$siglasDepe2}
	  	  &nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_ANULA_DOC_TRAB}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if (!$exito||$exito==0)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene opci&oacute;n para anular el documento!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td class="item"><strong>Datos del documento</strong> </td>
      <td>{$claseDocumento} {$indicativo} {$auditmod} <strong>{if $idEstadoDoc==5}FINALIZADO{else if $idEstadoDoc==1}ACTIVO{/if}</strong>
	  		<br>{if $contador>1}<strong>No se podr�a anular ya que ya fue enviado a otro trabajador</strong>{/if}
	  </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td><textarea name="asunto" cols="80" rows="4" class="iptxt1" id="textarea" readonly="readonly">{$asunto}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td><textarea name="observaciones" cols="80" rows="5" class="iptxt1" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="submit" value="Anular" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_TRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.SUMARIO_TRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ANULA_DOC_TRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		<input name="contador" type="hidden" id="contador" value="{$contador}">
		<input name="contador2" type="hidden" id="contador2" value="{$contador2}">
		 </td>
    </tr>
  </table>
</form>