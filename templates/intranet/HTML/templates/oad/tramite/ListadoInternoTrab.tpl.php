<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE DOCUMENTOS INTERNOS {if $fecInicio2!=$fecFin2}ENTRE EL  
        {$fecInicio2} Y {$fecFin2}{else}DEL {$fecInicio2}{/if}
        <!--ENTRE LAS FECHAS {$fecInicio} y {$fecFin} -->
        </font> <br>
        </font></b></font> 
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>TIPO</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>INDICATIVO</strong></font></div></td>
    <td width="45%"> <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="45%"> <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
    <td width="8%"> <div align="center"><font size="5"><strong>FECHA</strong></font></div></td>
    <td width="48%"> <div align="center"><font size="5"><strong>PER.DESTINO</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td width="10%"><font size="5">{$list[i].tipDoc}</font></td>
	<td width="10%"><font size="5">{$list[i].ind}</font></td>
    <td width="45%"><div align="center"><font size="5">{$list[i].asunto}</font></div></td>
    <td width="45%"><font size="5">{$list[i].obs}</font></td>
    <td width="8%"><font size="5">{$list[i].fec}</font></td>
    <td width="48%"><font size="5">{$list[i].per}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="7" width="100%"><div align="center"><font size="6"><strong>No 
        existen documentos ingresados en las fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen2}{$hora}s</i></font></p>
</body>
</html>
