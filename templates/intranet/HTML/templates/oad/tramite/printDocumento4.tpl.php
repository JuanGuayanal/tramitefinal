<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" align="center">
  <tr>
  	<td rowspan="22"></td> 
    <td><div align="center"><font size="+1"><strong>MIDIS VIRTUAL</strong></font></div></td>
    
  </tr>
  <tr>
    <td><div align="center"><font size="+1"><strong>SITRADOC</strong></font></div></td>
  </tr>
  <tr>
  	<td><div align="left"><font size="+1"><strong></strong></font></div></td>
  </tr>  
  <tr>
  	<td><hr width="100%" size="1" noshade></td>
  </tr>
  <tr>
  	<td><div align="left"><font size="+1"><strong></strong></font></div></td>
  </tr>
    
  <tr>
  	<td><div align="center"><font size="+1"><strong>Registro N&deg; </strong><font size="+1">{$num}</font></font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><strong>Contrase&ntilde;a P&aacute;gina Web</strong>: {$clave}</font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><strong>Registrado por</strong>: {$user}</font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><strong>Fecha y hora</strong>: {$fecRec}</font></div></td>
  </tr>
  <tr>
  	<td><div align="left"><font size="+1"><strong></strong></font></div></td>
  </tr>  
  <tr>
  	<td><hr width="100%" size="1" noshade></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><em>Oficina de Atenci&oacute;n al Ciudadano y Gesti&oacute;n Documentaria</em></font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><em>Telefax 
    51-1-209-8000 </em></font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><strong>otd@midis.gob.pe</strong></font></div></td>
  </tr>
  <tr>
  	<td><div align="center"><font size="+1"><strong>www.midis.gob.pe</strong></font></div></td>
  </tr>
  <tr>
  	<td><hr width="100%" size="1" noshade></td>
  </tr>
    <tr>
	<td><div align="left"><font size="+1"><strong></strong></font></div></td>
  </tr>  
  
  <tr>
  	<td><div align="center"><font size="+1"><strong>Gracias</strong></font></div></td>
  </tr>
  <tr>
  	<td><div align="left"><font size="+1"><strong></strong></font></div></td>
  </tr>  
  <tr>
  	<td><div align="left"><strong>OGTI</strong></div></td>
  </tr>  
</table>


{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>