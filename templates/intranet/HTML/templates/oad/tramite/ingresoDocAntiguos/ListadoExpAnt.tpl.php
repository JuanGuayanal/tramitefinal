<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b>
          <font color="#000000"> <font size="6">LISTADO DE EXPEDIENTES</font> </font></b></font>     
      </div>
      <hr width="100%" size="1" noshade>  </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td bgcolor="#999999"> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
	<td width="10%"> 
      <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td width="12%"><div align="center"><font size="5"><strong>FECHA</strong></font></div></td>
    <td width="27%"> 
      <div align="center"><font size="5"><strong>REMITENTE</strong></font></div></td>
	<td width="65%"><div align="center"><font size="5"><strong>PROCEDIMIENTO</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
	<td width="10%"><font size="5">{$list[i].tram}</font></td>
    <td width="12%"><font size="5">{$list[i].fecRec}</font></td>
    <td width="27%"><font size="5">{$list[i].razonsocial}</font></td>
    <td width="65%"><font size="5">{$list[i].asunto}</font></td>
  </tr>
  {sectionelse}  
  <tr>
    <td colspan="6"><div align="center"><font size="6"><strong>No existen coincidencias </strong></font></div></td>
  </tr>
  {/section}
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
