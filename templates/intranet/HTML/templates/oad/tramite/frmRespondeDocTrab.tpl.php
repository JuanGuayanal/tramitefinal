{$jscript}
<script language="JavaScript">
<!--
{literal}
function cargar(pForm,a)
{
	//var texto33=pForm.Observaciones.value;
	//var texto55=pForm.tipo2.value;	 
		//alert(texto33+" "+texto66);	 
		//alert(texto33);
		pForm.action="index.php?"+a;
	    pForm.submit();
}
{/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('asunto','Asunto','R');return document.MM_returnValue">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"> <strong>Clase de Documento</strong></td>
      <td><select name="idClaseDoc" class="ipsel1" id="idClaseDoc">
        {$selClaseDoc}
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if (($justo=='T' ||$a)&& $nro_doc)}
    <tr> 
      <td class="texto td-encuesta"> <strong>N&deg; Documento/Oficio</strong></td>
      <td> <input name="nro_doc" type="text" class="iptxt1" id="nro_doc" value="{$nro_doc}" size="35" maxlength="255"> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td height="25" class="texto td-encuesta" ><strong>Responder a</strong></td>
      <td><select name="trabajador" class="ipsel1" id="trabajador">
        {$selTrabajador}
        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Asunto</strong> </td>
      <td><textarea name="asunto" cols="65" rows="3" class="iptxt1" id="textarea" >{$asunto}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>{if $justo=='T'}Agregar observaciones{else}Observaciones{/if}</strong> </td>
      <td class="item"><textarea name="observaciones" cols="65" rows="3" class="iptxt1" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea></td>
      <td colspan="3"><strong></strong></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
	<!-- Tipo de doc=2||=4 => Se formula una resolución cuando se trata de Expediente y algunos casos de Doc Interno -->
	{if !$justo&& !$a&& ($tipDoc==2||$tipDoc==4)&& $dire==2 }
	<tr>
	<td colspan="3" class="item"><strong><a name="der" id="der"></a> 
				<input name="ProyResol" type="checkbox" id="ProyResol" value="1" onClick="submitForm('{$accion.FRM_RESPONDE_DOCTRAB}');" {if $ProyResol} checked{/if}>
        Formular Resoluci&oacute;n Ahora</strong></td>
			</tr>    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
			{if $ProyResol} 
				<tr> 
      				<td class="item"> <strong>Tipo de Resoluci&oacute;n:</strong></td>
      				<td colspan="2"><select name="TipoResol" class="ipsel1" id="TipoResol">
        			{$selTipoResol}
        			</select></td>
				</tr>
				<tr> 
      				<td class="item"> <strong>Sumilla:</strong></td>
      				<td colspan="2"><textarea name="sumilla" cols="54" class="iptxt1" id="textarea2" >{$sumilla}</textarea> 
      </td>
				</tr>
		<tr> 
      	  <td class="item"> <strong>Fec. Inicio</strong></td>
		  <td colspan="3"><select name="dia_ini" class="ipsel2" id="dia_ini">
		  {$selDiaIni}
			</select> <select name="mes_ini" class="ipsel2" id="mes_ini">
		  {$selMesIni}
			</select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
		  {$selAnyoIni}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Inicio." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Fin</strong></td>
		  <td colspan="3"><select name="dia_fin" class="ipsel2" id="dia_fin">
		  {$selDiaFin}
			</select> <select name="mes_fin" class="ipsel2" id="mes_fin">
		  {$selMesFin}
			</select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
		  {$selAnyoFin}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Fin." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Publicaci&oacute;n</strong></td>
		  <td colspan="3"><select name="dia_pub" class="ipsel2" id="dia_pub">
		  {$selDiaPub}
			</select> <select name="mes_pub" class="ipsel2" id="mes_pub">
		  {$selMesPub}
			</select> <select name="anyo_pub" class="ipsel2" id="anyo_pub">
		  {$selAnyoPub}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicación." width="20" height="20" align="top"> 
		  </td>
		</tr>
				<tr> 
      				<td colspan="3"><hr width="100%" size="1"></td>
    			</tr>
				{/if} 
		{/if}
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Responder" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_TRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.SUMARIO_TRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.RESPONDE_DOCTRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="id" type="hidden" id="id" value="{$id}"> 
		<input name="tipDoc" type="hidden" id="tipDoc" value="{$tipDoc}">
		<input name="tipBus" type="hidden" id="tipBus" value="{$tipBus}">
		<input name="a" type="hidden" id="a" value="{$a}">
		{if $justo=='T'}<input name="justo" type="hidden" id="justo" value="{$justo}">{/if}
		{if $tipDocumento}<input name="tipDocumento" type="hidden" id="tipDocumento" value="{$tipDocumento}">{/if}
		{if $tipBusqueda}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $fecIniTrab2}<input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="{$fecIniTrab2}">{/if}
		{if $fecFinTrab2}<input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="{$fecFinTrab2}">{/if}
		{if $asunto2}<input name="asunto2" type="hidden" id="asunto2" value="{$asunto2}">{/if}
		{if $indicativo}<input name="indicativo" type="hidden" id="indicativo" value="{$indicativo}">{/if}
		{if $observaciones2}<input name="observaciones2" type="hidden" id="observaciones2" value="{$observaciones2}">{/if}
		{if $nroTD}<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">{/if}
		{if $page}<input name="page" type="hidden" id="page" value="{$page}">{/if}
		{if ($siglasDep&& $siglasDep!='none')}<input name="siglasDep" type="hidden" id="siglasDep" value="{$siglasDep}">{/if}
		{if ($tipodDoc&& $tipodDoc!='none')}<input name="tipodDoc" type="hidden" id="tipodDoc" value="{$tipodDoc}">{/if}
		{if ($anyo3&& $anyo3!='none')}<input name="anyo3" type="hidden" id="anyo3" value="{$anyo3}">{/if}
		{if $codigoInterno}<input name="codigoInterno" type="hidden" id="codigoInterno" value="{$codigoInterno}">{/if}
		 </td>
    </tr>
  </table>
</form>