{literal}
<script language="JavaScript">
function submitForm(pAcc){
		document.buscaAdjunto.accion.value=pAcc
		document.buscaAdjunto.submit()
}
</script>
{/literal}
<script src="/sitradocV3/js/src/mesadepartes/acciones.js"></script>
<br>
<form name="buscaAdjunto" action="{$frmUrl}" method="post">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_ADJUNTO}">
	<input type="hidden" name="menu">
	<input type="hidden" name="subMenu">
	<input type="hidden" name="page">
	<input name="id" type="hidden" id="id" value="{$id}">	
<table width="700" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
  	<td height="24" align="center" class="item" valign="bottom"> <strong>N&uacute;mero 
      de Tr&aacute;mite Documentario: {$docOrigen}</strong> <a href="{$frmUrl}?accion={$accion.BUSCA_DOCUMENTO}&nroTD={$docOrigen}&menu={$accion.SUMARIO}&subMenu={$accion.BUSCA_DOCUMENTO}">Retornar a la b&uacute;squeda</a></td>
  </tr>
  <tr>
	  <td>
	  	<hr width="100%" size="1">
	  </td>
  </tr>
  <tr> 
    <td height="20"><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} adjuntos&nbsp;&nbsp; 
            {else} No se encontraron documentos adjuntos&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrDoc} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
            
          <td width="24" valign="middle"> <!-- <img src="/img/800x600/ico-mod.gif" alt="[ ADJUNTAR DOCUMENTO ]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"> -->
		  	<a href="{$frmUrl}?accion={$accion.FRM_MODIFICA_ADJUNTO}&idAdj={$arrDoc[i].id}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_MODIFICA_ADJUNTO}"><img src="/img/800x600/ico-mod.gif" alt="[ MODIFICAR ADJUNTO]" width="17" height="17" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);" title="MODIFICAR ADJUNTO"></a>{if $arrDoc[i].ultimo==1}<a href="#" class="btn_eliminar_adj" id="{$arrDoc[i].id}"><img src="/img/800x600/eliminar_16x16.gif" / title="ELIMINAR REGISTRO ADJUNTO"></a>{/if}
		  </td>
          <td class="texto">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="162" class="item" align="left"><strong>Nro.Doc.Asociado:</strong></td>
                <td class="texto" align="left"><strong>{$arrDoc[i].nroTD}</strong></td>
                <td class="texto" align="left"><span class="item"><strong>Fecha de Recepci&oacute;n:&nbsp;{$arrDoc[i].fecRec}</strong></span></td>
              </tr>
			  <tr>
			  	<td valign="top" class="sub-item" align="left"><strong>Tipo de Documento</strong></td>
				<td colspan="2" class="texto" align="left">{$arrDoc[i].tipo}</td>
			  </tr>
			  <tr class="std_oculto">
			  	<td valign="top" class="sub-item" align="left"><strong>Unidad Destino</strong></td>
				<td colspan="2" class="texto" align="left">{$arrDoc[i].dependencia}</td>
			  </tr>
              <tr class="std_oculto"> 
                <td valign="top" class="sub-item" align="left"><strong>Persona a destinar</strong></td>
                <td colspan="2" class="texto" align="left">{$arrDoc[i].persona}</td>
              </tr>
              <tr> 
                <td valign="top" nowrap class="sub-item" align="left"><strong> Contenido</strong></td>
                <td colspan="2" class="texto" align="left">{$arrDoc[i].contenido} </td>
              </tr>
              <tr> 
                <td valign="top" nowrap class="sub-item" align="left"><strong>Observacion</strong></td>
                <td colspan="2" class="texto" align="left">{$arrDoc[i].observacion}</td>
              </tr>
            </table></td>
        </tr>
      </table>
{/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
</form>