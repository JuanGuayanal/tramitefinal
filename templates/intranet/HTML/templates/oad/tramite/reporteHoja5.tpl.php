<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
    
<table width="100%" border="0" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
  {if $errors} 
  <tr> 
    <td class="item">{$errors} </td>
  </tr>
  {/if} 
  <tr> <br>
    <td colspan="2" class="item-sep"> <strong><font size="+6">MEMORANDO No.</font></strong><font size="+6"> 
      {$indicativo}-<strong>CONVENIO_SITRADOC</strong> </font> </td>
  </tr>
  <tr> 
    <td class="item"><font size="+6"><strong>A</strong></font></td>
    <td><font size="+6"> {$depDestino} </font></td>
  </tr>
  <tr> 
    <td class="item"><font size="+6"><strong>REF.</strong></font></td>
    <td class="item"><font size="+6"><strong>Tipo de documento : </strong> 
      <select name="TipoDocumento" class="ipsel2">
	  {$selTipoDocumento}
        
      </select>
      <strong>A&ntilde;o</strong> 
      <select name="select3" class="ipsel2">
      </select>
      <strong>Numero</strong> 
      <select name="select5" class="ipsel2">
      </select>
      </font></td>
  </tr>
  <tr> 
    <td class="item"><font size="+6"><strong>Fecha</strong></font></td>
    <td class="item"><font size="+6">{$FechaActual}</font></td>
  </tr>
  <tr> 
    <td colspan="2"><hr size="1"></td>
  </tr>
  <tr> 
    <td class="item-sep" colspan="2"><strong><font size="+6">REFERENCIA</font></strong> 
    </td>
  </tr>
  <tr> 
    <td colspan="2"><table width="100%" border="0" cellspacing="0">
        <tr> 
          <td><strong> 
            <input type="checkbox" name="checkbox" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Tramitaci&oacute;n </strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox5" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Opini&oacute;n/Recomendaci&oacute;n 
            </strong></font></td>
          <td><strong> 
            <input type="checkbox" name="checkbox9" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Calificar/Evaluar</strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox13" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Visaci&oacute;n </strong></font></td>
        </tr>
        <tr> 
          <td><strong> 
            <input type="checkbox" name="checkbox2" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Atenci&oacute;n</strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox6" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Ayuda Memoria</strong></font></td>
          <td><strong> 
            <input type="checkbox" name="checkbox10" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Preparar Respuesta </strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox14" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Ampliar Informe </strong></font></td>
        </tr>
        <tr> 
          <td><strong> 
            <input type="checkbox" name="checkbox3" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Conocimiento y Fines </strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox7" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Informe</strong></font></td>
          <td><strong> 
            <input type="checkbox" name="checkbox11" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Proyectar Resoluci&oacute;n 
            </strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox15" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Reformular Informe </strong></font></td>
        </tr>
        <tr> 
          <td><strong> 
            <input type="checkbox" name="checkbox4" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Coordinar</strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox8" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Agregar a sus Antecedentes 
            </strong></font></td>
          <td><strong> 
            <input type="checkbox" name="checkbox12" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Notificar al Interesado</strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox16" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Aclarar Redacci&oacute;n </strong></font></td>
        </tr>
        <tr> 
          <td><strong> 
            <input type="checkbox" name="checkbox17" value="checkbox" >
            </strong></td>
          <td class="item"><font size="+6"><strong>Acci&oacute;n</strong></font></td>
          <td><font size="+6"><strong> 
            <input type="checkbox" name="checkbox18" value="checkbox" >
            </strong></font></td>
          <td class="item"><font size="+6"><strong>Archivar</strong></font></td>
          <td colspan="4"></td>
        </tr>
        <tr> 
          <td colspan="8"><table width="100%" border="0" cellspacing="0">
              <tr> 
                <td class="item"><strong><font size="+6">Otros y/o Observaciones</font></strong> 
                </td>
                <td><font size="+6">{$observaciones}</font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"><hr size="1"></td>
  </tr>
  <tr align="center"> 
    <td colspan="2"> <input type="submit" name="Submit" value="Guardar Memorando" class="submit"> 
      &nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
      <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_MEMORANDO}"> 
      <input name="menu" type="hidden" id="menu" value="{$menuPager}"> </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
