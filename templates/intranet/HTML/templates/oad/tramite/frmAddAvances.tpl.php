<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('observaciones','Observaciones','R');return document.MM_returnValue">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"> <strong>Avances</strong></td>
      <td> <textarea name="observaciones" cols="80" rows="5" class="iptxt1" id="observaciones" >{$observaciones}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Guardar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_TRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.SUMARIO_TRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ADDOBS_DOCTRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idAddObs" type="hidden" id="idAddObs" value="{$idAddObs}"> 
		 </td>
    </tr>
  </table>
</form>