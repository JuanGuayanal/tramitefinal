<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE EXPEDIENTES ENTRE EL {$fecInicio2} Y {$fecFin2}</font> <br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999">
  	<td bgcolor="#999999"><div align="center"><font size="5"><strong>N&deg;</strong></font></div></td> 
    <td width="10%"> 
      <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td width="8%"> 
      <div align="center"><font size="5"><strong>FECHA DE RECEPCI&Oacute;N</strong></font></div></td>
    <td width="12%"> 
      <div align="center"><font size="5"><strong>INDICATIVO</strong></font></div></td>
    <td width="25%"> 
      <div align="center"><font size="5"><strong>REMITENTE</strong></font></div></td>
    <td width="3%"><font size="5"><strong>FOLIOS</strong></font></td>
    <td width="42%"> 
      <div align="center"><font size="5"><strong>PROCEDIMIENTO</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr>
  	<td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td> 
    <td width="10%"><font size="5">{$list[i].numTram}</font></td>
    <td width="8%"><font size="5">{$list[i].fecRec}</font></td>
    <td width="12%"><font size="5">{$list[i].oficio}</font></td>
    <td width="25%"><font size="5">{$list[i].razSoc}</font></td>
    <td width="3%"><div align="center"><font size="5">{$list[i].folio}</font></div></td>
    <td width="42%"><font size="5">{$list[i].proc}</font></td>
  </tr>
  {sectionelse}  
  <tr>
    <td colspan="6"><div align="center"><font size="6"><strong>No se han ingresado 
        expedientes en la(s) fecha(s) dada(s).</strong></font></div></td>
  </tr>
  {/section}
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
