<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE CORRESPONDENCIA {if $fecIniNoti==$fecFinNoti} DEL 
		{$fecIniNoti2}{else}ENTRE EL {$fecIniNoti2} Y EL {$fecFinNoti2}{/if}
		 </font></font></b></font>
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table width="100%" border="1" align="center">
  <tr bgcolor="#B9B9B9"> 
    <td> 
      <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td> 
      <div align="center"><strong><font size="5">N&deg; DE DOCUMENTO</font></strong></div></td>
    {if $coddep==48}
    <td><div align="center"><strong><font size="5">ACTO</font></strong></div></td>	
	{/if}
	<td> 
      <div align="center"><strong><font size="5">REMITENTE</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="5">RAZ&Oacute;N SOCIAL</font></strong></div></td>
    <td> 
      <div align="center"><strong><font size="5">DESTINO</font></strong></div></td>    
	<td> 
      <div align="center"><strong><font size="4">FEC. ENT. COURIER</font></strong></div></td>
    	<td> 
      <div align="center"><strong><font size="4">FEC. NOTIFICACI&Oacute;N</font></strong></div></td>
	<td> 
      <div align="center"><strong><font size="5">OBSERVACIONES</font></strong></div></td>
  </tr>
  {section name=i loop=$not} 
  <tr> 
    <td><font size="5">{$smarty.section.i.iteration}</font></td>
    <td><font size="5">{$not[i].nro}</font></td>
	{if $coddep==48}<td><font size="4">{$not[i].resol|default:'No especificado'}</font></td>{/if}
    <td><font size="5">{$not[i].remitente}</font></td>
    <td><font size="5">{$not[i].razonsocial}</strong></font></td>
    <td><font size="5">{if $not[i].destino}{$not[i].destino}{else}{if $not[i].domicilio}{$not[i].domicilio}{else}Sin Domicilio{/if}{/if}</font></td>
    <td><div align="center"><font size="5">{$not[i].fecEntCourier}</font></div></td>	   
    <td><div align="center"><font size="5">{$not[i].fecNot}</font></div></td>
    <td><font size="5">{if $not[i].registro!=""}Registro: {$not[i].registro}<br>Observaciones:{$not[i].obsDigsecovi}{/if}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="{if $coddep==48}9{else}8{/if}" align="center" bgcolor="#FFFFFF"><strong><font size="6">No 
      existen notificaciones en las fecha(s) dada(s).</font></strong></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
