{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function cargar(pForm,a,b)
{
		//alert(a);
		pForm.action="index.php?accion=frmAddNotificacion&menu=frmSearchNoti&subMenu=frmAddNotificacion&idUpdateNot="+a+b;
	    pForm.submit();
}
function cargar2(pForm,a)
{
		//alert(a);
		pForm.action="index.php?accion=frmAddNotificacion&menu=frmSearchNoti&subMenu=frmAddNotificacion&codDepa="+a;
	    pForm.submit();
}
function SubmitForm2(pSel,pForm,pAccion){
	if(pSel.options[pSel.selectedIndex].value!='none'){
		pForm.accion.value=pAccion
		pForm.action="index.php?"+pAccion+"#user"
		pForm.submit()
	}
}

{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('campo1','El Cargo del funcionario que notifica','Sel','camp2','El tipo de Resoluci�n','Sel','campo2','El acto que se notifica','R','campo3','El nombre y cargo','R','campo4','Procedimiento Administrativo','R','destinatario','El Destinatario','R','DomProc','El Domicilio Procesal','R');return document.MM_returnValue">
  <table width="490" border="0" align="center" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} <br>
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td colspan="2" class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Director:</strong></td>
      <td class="item">{if empty($idUpdateNot)} <select name="campo1" class="ipsel1" id="campo1">
						{$selCampo1}
				     </select>{else}{$funcionario}{/if}</td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el cargo del funcionario que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Acto que se notifica</strong></td>
      <td class="item"> {if empty($idUpdateNot)}<select name="camp2" class="ipsel1" id="camp2">
	  <option value="none">Seleccione una opci�n</option>
	  <option value="1"{if $camp2==1} selected{/if}>Resoluci�n Suprema</option>
	  <option value="2"{if $camp2==2} selected{/if}>Resoluci�n Ministerial</option>
	  <option value="3"{if $camp2==3} selected{/if}>Resoluci�n Viceministerial</option>
	  <option value="4"{if $camp2==4} selected{/if}>Resoluci�n Directoral</option>
	  <option value="5"{if $camp2==5} selected{/if}>Resoluci�n Secretarial</option>
	  </select>&nbsp;<input name="campo2" type="text" class="iptxt1" id="campo2" value="{$campo2}" size="30" maxlength="255">{else}{$campo2}{/if} 
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el acto que se notifica(resoluci�n, oficio, etc.) y la fecha de expedici�n." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Nombre y Cargo</strong></td>
      <td class="item">{if empty($idUpdateNot)}<input name="campo3" type="text" class="iptxt1" id="campo3" value="{$campo3}" size="35" maxlength="255">{else}{$campo3}{/if} 
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el Nombre y Cargo del funcionario que expedi� el acto que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Procedimiento</strong></td>
      <td class="item">{if empty($idUpdateNot)}<input name="campo4" type="text" class="iptxt1" id="campo4" value="{$campo4}" size="35" maxlength="255">{else}{$campo4}{/if}
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong><a name="user" id="user"></a>Destinatario</strong> </td>
      <td colspan="2" class="item">{if empty($idUpdateNot)}<textarea name="destinatario" cols="54" class="iptxt1" id="textarea" >{$destinatario}</textarea>{else}{$destinatario}{/if}</td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Domicilio Procesal</strong> </td>
      <td colspan="2" class="item">{if empty($idUpdateNot)} 
        <table width="75%" border="0">
          <tr> 
            <td><textarea name="DomProc" cols="54" class="iptxt1" id="DomProc" >{$DomProc}</textarea></td>
          </tr>
        </table>
        {else}{$DomProc}{/if}</td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" > <strong>Vigencia del Acto Notificado</strong></td>
      <td colspan="3"> <select name="vigencia" class="ipsel1" id="campo1" {if !empty($idUpdateNot)} disabled{/if}>
          <option value="none"{if $vigencia=="none"} selected{/if}>Seleccione una 
          opci�n</option>
          <option value="1"{if $vigencia==1} selected{/if}>Desde la fecha de su 
          emisi�n</option>
          <option value="2"{if $vigencia==2} selected{/if}>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"{if $vigencia==3} selected{/if}>Desde el d�a de su notificaci�n</option>
          <option value="4"{if $vigencia==4} selected{/if}>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Agota la v&iacute;a administrativa</strong></td>
      <td colspan="3" class="item">{if empty($idUpdateNot)}<input name="radio" type="radio" value="1" checked> 
        <strong>Si</strong> <input type="radio" name="radio" value="0"> 
        <strong>No</strong>{else} <strong>{if $radio==1}Si agota la v&iacute;a administrativa{else}
		                                                No agota la v&iacute;a administrativa{/if}</strong>{/if}</td>
    </tr>
    <tr> 
      <td rowspan="3" class="item"><strong>Recurso Administrativo:</strong></td>
      <td colspan="3" class="item"><input type="checkbox" name="checkbox" value="1"{if ($idUpdateNot>0)}{if $recurso1==1} checked{/if}{/if}{if !empty($idUpdateNot)} disabled{/if}>
        Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td colspan="3" class="item"><input type="checkbox" name="checkbox2" value="1"{if ($idUpdateNot>0)}{if $recurso2==1} checked{/if}{/if}{if !empty($idUpdateNot)} disabled{/if}>
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td colspan="3" class="item"><input type="checkbox" name="checkbox3" value="1"{if ($idUpdateNot>0)}{if $recurso3==1} checked{/if}{/if}{if !empty($idUpdateNot)} disabled{/if}>
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td class="item"><strong>N&uacute;mero de folios</strong></td>
      <td colspan="3" class="item">{if empty($idUpdateNot)} <input name="folio" type="text" class="iptxt1" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="4" maxlength="2">{else}{$folio}{/if}</td>
    </tr>
    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
	{if ($idUpdateNot>0)}
    <tr> 
      <td class="item" > <strong><a name="proc" id="proc"></a>Estado de la Notificaci&oacute;n</strong></td>
      <td colspan="3"> <select name="flag" class="ipsel1" id="flag" onChange="submitForm('{$accion.FRM_NOTIFICACION}');cargar(document.{$frmName},{$idUpdateNot},'#proc');">
		<option value="none">Seleccione una opci�n</option>
		<option value="1"{if $flag=="1"} selected{/if}>Notificada</option>
		<option value="2"{if $flag=="2"} selected{/if}>Rechazada</option>
		<option value="3"{if $flag=="3"} selected{/if}>No practicada</option>
        </select> </td>
    </tr>
		{if ($flag==1||$flag==2||$flag==3)}
		<tr> 
		  
      <td class="item"> <strong>Fec. Notificaci&oacute;n</strong></td>
		  <td colspan="3"><select name="dia_ing" class="ipsel2" id="dia_ing">
		  {$selDiaIng}
			</select> <select name="mes_ing" class="ipsel2" id="mes_ing">
		  {$selMesIng}
			</select> <select name="anyo_ing" class="ipsel2" id="anyo_ing">
		  {$selAnyoIng}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      <td class="item" > <strong>Departamento</strong></td>
		  <td colspan="3"> <select name="codDepa" class="ipsel2" id="select3" onChange="document.{$frmName}.codProv.options[0].selected=true;SubmitForm2(this,document.{$frmName},'{$accion.FRM_NOTIFICACION}');">
		{$selDepa}
              </select> </td>
		</tr>
		<tr> 
      <td class="item" > <strong>Provincia</strong></td>
		  <td colspan="3"> <select name="codProv" class="ipsel2" id="select2" onChange="SubmitForm2(this,document.{$frmName},'{$accion.FRM_NOTIFICACION}')">
		{$selProv}
              </select> </td>
		</tr>
		<tr> 
      <td class="item" > <strong>Distrito</strong></td>
		  <td colspan="3"> <select name="codDist" class="ipsel2" id="select" >
		{$selDist}
              </select></td>
		</tr>

		{/if}
		<!--
		<tr> 
		  <td class="item"> <strong>Hora. Ingreso</strong></td>
		  <td colspan="3"><select name="hora_ing" class="ipsel2" id="hora_ing">
		  {$selHorIng}
			</select> <select name="min_ing" class="ipsel2" id="min_ing">
		  {$selMinIng}
			</select>
			<img src="/img/800x600/ico-info3.gif" alt="Corresponde a la hora en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		-->
		{if $flag==1}
		<tr>
			<td class="item"><strong>Recibido por:</strong></td>
		  <td colspan="3" class="item"><input name="aaaa" type="text" class="iptxt1" onKeyPress="solo_num();" value="{$aaaa}" maxlength="8">
        <img src="/img/800x600/ico-info3.gif" alt="DNI del que recibe la notificaci�n" width="20" height="20" align="top"></td>
		</tr>
		{/if}
		{if $flag==3}
		<tr> 
		  
      <td class="item" > <strong><a name="aa" id="aa"></a>Motivo</strong></td>
		  <td colspan="3"> <select name="estadoNot" class="ipsel1" id="estadoNot" onChange="document.{$frmName}.estadoNot.selected=true;submitForm('{$accion.FRM_NOTIFICACION}');cargar(document.{$frmName},{$idUpdateNot},'#aa');">
			{$selEstado}
			</select> </td>
		</tr>
		{/if}
		{if ($flag==2||($flag==3&& $estadoNot==3))}
				<tr> 
				  <td class="item"><strong>Observaciones</strong> 
        		 </td>
				  <td colspan="3"><textarea name="Observaciones" cols="55" rows="2" class="iptxt1" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
		{/if}
	{/if}
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> <input name="bbSubmit" type="Submit" class="submit" value="{if empty($idUpdateNot)}Crear Notificaci�n{else}Actualizar Datos{/if}" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_NOTIFICACION}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_BUSCA_NOTIFICACION}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.NOTIFICACION}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"><input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{if ($idUpdateNot>0)}<input name="idUpdateNot" type="hidden" value="{$idUpdateNot}">{/if}
		{if ($idExp>0)}<input name="idExp" type="hidden" value="{$idExp}">{/if}</td>
    </tr>
  </table>
</form>