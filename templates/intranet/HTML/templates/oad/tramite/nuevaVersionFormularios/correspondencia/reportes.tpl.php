{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
-->
</script>
<!-- {/literal} onSubmit="document.{$frmName}.page.value=''";onClick="document.{$frmName}.page.value=''"-->
<br>
{if !$tipReporte || $tipReporte==2}
<script src="/sitradocV3/js/src/correspondencia/reportes.js"></script>
<form name="{$frmName}" action="/institucional/aplicativos/oad/sitradocV2/index.php" method="post" target="_blank">
<table width="780" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
<tr class="std_oculto">
	<td width="1" class="textoblack">&nbsp;</td>
	<td width="156" class="textoblack"><strong>Tipo de Reporte</strong></td>
    <td align="left">
		<select name="tipReporte" class="ipseln" onChange="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
          <option value="2"{if $tipReporte==2} selected{/if}>Todos</option>
		  <option value="3"{if $tipReporte==3} selected{/if}>Destinatario</option>
		  <option value="5"{if $tipReporte==5} selected{/if}>Expedida por</option>		  		  
		  <option value="1"{if $tipReporte==1} selected{/if}>Nro.Notificacion</option>
		  <option value="4"{if $tipReporte==4} selected{/if}>Nro.Resolucion</option>
        </select>
    </td>
    <td></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
	<td width="156" align="left">Fecha</td>
    <td align="left"><input type="text" id="C1"  class="datepicker ipseln"/> - 
        <input type="text" id="C2"  class="datepicker ipseln"/> Situaci&oacute;n&nbsp;
        <select name="estadoEnvio" class="ipseln">
        <option value="0">TODOS</option>
        <option value="2">POR RECIBIR</option>
        <option value="3">ENVIADOS X COURIER</option>
        <option value="5">DEVUELTOS</option>
        </select>
	</td>
    <td><input type="submit" id="generar_reporte" value="Generar Reporte" class="submitV2" /></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
    <td>Tipo de Servicio</td>
    <td align="left" colspan="2">Todos <input type="radio" name="paquete" value="0" checked="checked"> Sobre <input type="radio" name="paquete" value="2"> Paquete <input type="radio" name="paquete" value="1">
	</td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
	<td align="left">Ambito</td>
    <td align="left"><select name="tipo_mensajeria" class="ipseln">
<!--    		<option value="10">TODOS</option>
            <option value="1">LOCAL</option>
            <option value="0">NACIONAL</option>-->
            {$selServicios}
    	</select> Prioridad <select name="id_prioridad" class="ipseln">
            {$selPrioridad}
    	</select>
	</td>
    <td></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
	<td width="156" align="left">Forma de env�o</td>
    <td align="left"><select name="forma_envio" class="ipseln">
   			<option value="0">TODOS</option>
    		<option value="1">OGDA-ME</option>
            <option value="2">OTROS</option>
    	</select>
	</td>
    <td></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
	<td width="156" align="left">Destinatario</td>
    <td align="left"><input type="text" name="destinatario" class="ipseln"/></td>
    <td></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
	<td width="156" align="left">Usuario</td>
    <td align="left"><input type="text" name="usuario" class="ipseln"/></td>
    <td></td>
</tr>
<tr>
	<td width="1" class="textoblack">&nbsp;</td>
    <td colspan="2" align="left">PDF<input name="tipo_reporte" type="radio" value="1" checked="checked" /> EXCEL<input type="radio" name="tipo_reporte" value="2" /></td>
	<td></td>
</tr>
</table>
<input type="hidden" name="accion" value="ReporteCorrespondencia" />
<input type="hidden" name="fechaIni" id="fechaIni"/>
<input type="hidden" name="fechaFin" id="fechaFin"/>
</form>
{else}
<form name="{$frmName}" action="{$frmUrl}" method="post" {if $tipReporte==11} target="_self"{else} target="_blank"{/if} {if ($tipReporte==12||$tipReporte==13)} enctype="multipart/form-data"{/if}>
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.GENERA_REPORTE_NOTI}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <table width="780" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr class="std_oculto"> 
      <td width="1" class="textoblack">&nbsp;</td>
      <td width="170" class="textoblack"><strong>Tipo de Reporte</strong></td>
      <td width="62"><select name="tipReporte" class="ipseln" onChange="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
          <option value="2"{if $tipReporte==2} selected{/if}>Todos</option>
		  <option value="3"{if $tipReporte==3} selected{/if}>Destinatario</option>
		  <!--<option value="12"{if $tipReporte==12} selected{/if}>Enviar x mail</option>-->
		  <option value="5"{if $tipReporte==5} selected{/if}>Expedida por</option>		  
		  {if ($coddep_Trab==13||$coddep_Trab==48)}<option value="13"{if $tipReporte==13} selected{/if}>Exportar al SITRADOC (Oec)</option>{/if}		  
		  <option value="1"{if $tipReporte==1} selected{/if}>Nro.Notificacion</option>
		  <option value="4"{if $tipReporte==4} selected{/if}>Nro.Resolucion</option
        ></select></td> 
      <td width="200" valign="middle">{if ($tipReporte>=1&& $tipReporte<=5)}<input name="notificacion" type="text" class="iptxtn" value="{$notificacion}" size="6" maxlength="100">{/if}
	  	{if ($tipReporte==15)}<input name="primeraNoti" type="text" class="iptxtn" value="{$primeraNoti}" size="6" maxlength="100">&nbsp;
			<select name="primerAnyo" class="ipseln" id="primerAnyo">
      			<option value="2009" {if ($primerAnyo==2009 || !$primerAnyo)} selected{/if}>2009</option>
				<option value="2008" {if ($primerAnyo==2008)} selected{/if}>2008</option>
        	</select>{/if}
	  </td>
      <td width="200" valign="middle">
	    {if ($tipReporte==15)}<input name="segundaNoti" type="text" class="iptxtn" value="{$segundaNoti}" size="6" maxlength="100">&nbsp;
			<select name="segundoAnyo" class="ipseln" id="segundoAnyo">
      			<option value="2009" {if ($segundoAnyo==2009 || !$segundoAnyo)} selected{/if}>2009</option>
				<option value="2008" {if ($segundoAnyo==2008)} selected{/if}>2008</option>
        	</select>{/if}
	  	<!--<select name="mes" class="ipsel1">
		            {$selMes}
              </select><select name="anyo" class="ipsel1">
		            {$selAnyo}
              </select>-->
			  </td>
	  <td width="140" valign="middle"> <input type="submit" class="submitV2" value="Generar Reporte" onClick="MM_validateForm('nrotramite','Debe indicar el Nro de Tr�mite,','R');return document.MM_returnValue"> 
      </td>
    </tr>
	<tr>
		<td></td>
		<td></td>
		<td>{if $tipReporte==11}
		<select name="tipMensajeria" class="ipseln" id="tipMensajeria">
          <option value="3"{if $tipMensajeria==3} selected{/if}>Todo</option>
		  <option value="1"{if $tipMensajeria==1} selected{/if}>Mensajer�a Local</option>
		  <option value="2"{if $tipMensajeria==2} selected{/if}>Mensajer�a Nacional</option>		
		</select>{/if}</td>
		<td>
			{if ($tipReporte<>13)}
			<select name="dia_ini" class="ipseln" id="dia_ini">
      			{$selDiaIng}
	    	</select>
			<select name="mes_ini" class="ipseln" id="mes_ini">
	  			{$selMesIng}
        	</select>
			<select name="anyo_ini" class="ipseln" id="anyo_ini">
      			{$selAnyoIng}
        	</select>
			{/if}
			{if $tipReporte==11}
			<br><select name="ora_ini" class="ipseln" id="ora_ini">
          {$selHoraIni}
        </select> <select name="min_ini" class="ipseln" id="min_ini">
          {$selMinIni}
        </select>
		{/if}
		</td>
		<td>
			{if ($tipReporte<>13)}
			<select name="dia_fin" class="ipseln" id="dia_fin">
      			{$selDiaSal}
	    	</select>
			<select name="mes_fin" class="ipseln" id="mes_fin">
	  			{$selMesSal}
        	</select>
			<select name="anyo_fin" class="ipseln" id="anyo_fin">
      			{$selAnyoSal}
        	</select>
			{/if}
			{if $tipReporte==11}
			<br><select name="ora_fin" class="ipseln" id="ora_fin">
          {$selHoraFin}
        </select> <select name="min_fin" class="ipseln" id="min_fin">
          {$selMinFin}
        </select>
		{/if}
		</td>
		<td></td>
	</tr>
	{if ($tipReporte==12||$tipReporte==13)}
    {if ($tipReporte==12)}
	<tr>
		<td></td>
		<td></td>
		<td><strong>Enviar por mail</strong></td>
		<td><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
              <strong>Msj. Local</strong></label></td>
		<td><label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
              <strong>Msj. Nacional</strong></label></td>
		<td></td>
	</tr>
	{/if}
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td><strong>Archivo adjunto:</strong></td>
		<td colspan="2"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
		
	</tr>
    {/if}
  </table>
  </form>
{/if}