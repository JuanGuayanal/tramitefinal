{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
-->
</script>
<!-- {/literal} -->
{if $oficio_creado!=''}<br />{$oficio_creado}{/if}
{if $mensaje!=''}<br />{$mensaje}{/if}
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="page" type="hidden" id="page">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_CORRESPONDENCIAA}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  
  <table width="800" height="30" border="0" align="center" cellpadding="0" cellspacing="2" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif"  class="TEmb">
    <tr class="std_oculto"> 
      <td width="32" rowspan="8" class="textoblack"><img src="/img/Buscar2.jpg" ></td>
      <td width="180" class="textoblack"><strong>Tipo de Correspondencia:</strong></td>
      <td colspan="2" width="150" align="left"><select name="TipoCor" id="TipoCor" class="ipseln" onChange="submitForm('{$accion.BUSCA_CORRESPONDENCIAA}');">
			{$selTipoCorrespondencia}
		</select></td>
      <td width="115" valign="middle"> 
      </td>
    </tr>
    <tr>
        <td class="textogray"><strong>{if $TipoCor==1}N&uacute;mero de Notificaci&oacute;n{else}N&uacute;mero de Documento{/if}</strong></td>
          <td colspan="3" align="left"><input name="noti" type="text" class="iptxtn" value="{$noti}" size="20" maxlength="100">
	  	{if ($TipoCor==2&& $coddep==12)}
			&nbsp;&nbsp;
			<select name="anyo3" class="ipseln" id="select">
					<option value="none">Todos</option>
					<option value="2005"{if ($anyo3==2005)} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
		</select>
			<select name="dependencia" class="ipseln" id="dependencia">
						{$selDependencia}
		    </select>
		{/if} <input name="bsubmit" type="submit" class="submitV2" value="Buscar">
	  </td>
    </tr>
    <tr>
        <td class="textogray"><strong>Estado</strong></td>
          <td colspan="3" align="left">
			<select name="estado" class="ipseln" id="select">
					<option value="none" {if $estado=='none'} selected{/if}>Todos</option>
					<option value="0" {if ($estado!='none') && ($estado==0)} selected{/if}>Enviados</option>
					<option value="1" {if ($estado!='none') && ($estado==1)} selected{/if}>Devueltos</option>
		</select>
	  </td>
    </tr>
    <tr> 
      <td colspan="5" align="right" class="textogray" ><hr width="100%" size="1"></td>
    </tr>
    <tr>
	{if $TipoCor==1} 
    <tr> 
      <td class="textogray"><strong>Nro. Resoluci&oacute;n o Expediente</strong></td>
      <td colspan="3" align="left"><input name="resol" type="text" class="iptxtn" value="{$resol}" size="20" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>El que expidi&oacute; el acto q se notifica</strong></td>
      <td colspan="3" align="left"><input name="detal" type="text" class="iptxtn" value="{$detal}" size="20" maxlength="100"></td>
    </tr>
	{/if}
    <tr> 
      <td class="textogray"><strong>Destinatario</strong></td>
      <td colspan="3" align="left"><input name="destinatario" type="text" class="iptxtn" value="{$destinatario}" size="20" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textogray"><strong>Creada entre las fechas</strong></td>
      <td width="190" align="left"><select name="dia_ini" class="ipseln" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipseln" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipseln" id="anyo_ini">
      {$selAnyoIng}
        </select></td>
      <td width="10">&nbsp;  </td><td><select name="dia_fin" class="ipseln" id="dia_fin">
        
      {$selDiaSal}
	    
      </select>
        <select name="mes_fin" class="ipseln" id="mes_fin">
          
	  {$selMesSal}
        
        </select>
        <select name="anyo_fin" class="ipseln" id="anyo_fin">
          
      {$selAnyoSal}
        
        </select></td>
    </tr>
  </table>
</form>
