{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}

function solo_carac(){
	/**/if (event.keyCode == 13) {
			event.returnValue = false;
	}/**/
	//a=event.keyCode;
	//alert(a);
}

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}

function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('idClaseDoc','Clase de Documento','Sel','dependencia','Dependencia','Sel','asunto','Asunto','R'{/literal}{if $dependencia==7},'domicilio','Domicilio','R'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<!--<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('idClaseDoc','Clase de Documento','Sel','dependencia','Dependencia','Sel','asunto','Asunto','R');return document.MM_returnValue">-->
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA NOTIFICACI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" valign="middle" align="left"><span class="item"><strong>Procedimiento</strong></span> </td>
      <td class="item" align="left">
	  	<textarea name="campo4" cols="70" rows="12" class="iptxtn" id="textarea" >{$campo4}</textarea>
	  	<!--<input name="campo4" type="text" class="iptxtn" id="campo4" value="{$campo4}" size="35" maxlength="255">-->
	   &nbsp;<img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top">
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Destinatario</strong></span></td>
      <td class="item" align="left"><textarea name="destinatario" cols="70" rows="3" class="iptxtn" id="textarea" >{$destinatario}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Ubigeo</strong></span> </td>
      <td class="item" align="left">
	  		<select name="codDepa" class="ipseln" id="select3" onChange="submitForm('{$accion.FRM_INSERTA_CORRESPONDENCIA_PERSONAL}')">
					{$selDepa}
            </select>
			<select name="codProv" class="ipseln" id="select2" onChange="submitForm('{$accion.FRM_INSERTA_CORRESPONDENCIA_PERSONAL}')">
					{$selProv}
            </select>
			<select name="codDist" class="ipseln" id="select" >
					{$selDist}
              </select>	  
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Domicilio Procesal</strong></span> </td>
      <td class="item" align="left"><textarea name="DomProc" cols="70" rows="3" class="iptxtn" id="DomProc" >{$DomProc}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if ($coddep==13||$coddep==23||$coddep==125)} 
	<tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Acto notificado</strong></span> </td>
      <td class="item" align="left"><input name="actoNotificado" type="text" class="iptxtn" id="actoNotificado" value="{$actoNotificado}" size="60"/></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Vigencia del Acto Notificado</strong></span> </td>
      <td class="item" align="left"><select name="vigencia" class="ipseln" id="campo1" >
          <option value="none"{if $vigencia=="none"} selected{/if}>Seleccione una 
          opci�n</option>
          <option value="1"{if $vigencia==1} selected{/if}>Desde la fecha de su 
          emisi�n</option>
          <option value="2"{if $vigencia==2} selected{/if}>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"{if $vigencia==3} selected{/if}>Desde el d�a de su notificaci�n</option>
          <option value="4"{if $vigencia==4} selected{/if}>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>Agota la v&iacute;a administrativa </strong></span> </td>
      <td class="item" align="left"><input name="radio" type="radio" value="1" checked> 
        <strong>Si</strong> <input type="radio" name="radio" value="0"> 
        <strong>No</strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td rowspan="3" class="texto td-encuesta" align="left"><span class="item"><strong>Recurso Administrativo</strong></span> </td>
      <td class="item" align="left"><input type="checkbox" name="checkbox" value="1">
        Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
      <td colspan="3" rowspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr>
      <td class="item" align="left"><input type="checkbox" name="checkbox2" value="1">
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr>
      <td class="item" align="left"><input type="checkbox" name="checkbox3" value="1">
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><span class="item"><strong>N&uacute;mero de Folios</strong></span> </td>
      <td class="item" align="left"><input name="folio" type="text" class="iptxtn" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="4" maxlength="2"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>

    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>	
    <tr> 
      <td class="item" align="left"> <strong>&iquest;Desea vincularlo a un documento?</strong></td>
      <td  class="texto td-encuesta" align="left"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_INSERTA_CORRESPONDENCIA_PERSONAL}')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_INSERTA_CORRESPONDENCIA_PERSONAL}')">
              <strong>Documento Interno</strong></label></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"> <strong>Digite el {if ($GrupoOpciones1==1||!$GrupoOpciones1)}Nro. de Tr&aacute;mite{else}Nro. de documento{/if}</strong></td>
      <td align="left">{if ($GrupoOpciones1==1||!$GrupoOpciones1)}
	  			<input name="nroTD" type="text" class="iptxtn" id="nroTD" value="{$nroTD}" size="30">-
				<select name="anyo3" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo3==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo3==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo3==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo3==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo3==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo3==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo3==2011} selected{/if}>2011</option>
					<option value="2012"{if $anyo3==2012} selected{/if}>2012</option>-->
					<option value="2013"{if $anyo3==2013} selected{/if}>2013</option>
				</select>
	      {else}
	  			<select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo2==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo2==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo2==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo2==2011} selected{/if}>2011</option>
					<option value="2012"{if $anyo2==2012} selected{/if}>2012</option>
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>-->
				</select> -MIDIS/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  	  {/if}&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submitV2" onClick="submitForm('{$accion.FRM_INSERTA_CORRESPONDENCIA_PERSONAL}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if $exito==1}
    <tr> 
      <td class="item" align="left"><strong>Asunto</strong> </td>
      <td align="left">{if $numTram}{$numTram}<br>{/if}{$asunto2}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Insertar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_CORRESPONDENCIAA}&menu={$accion.FRM_BUSCA_CORRESPONDENCIAA}&subMenu={$accion.FRM_BUSCA_CORRESPONDENCIAA}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.INSERTA_CORRESPONDENCIA_PERSONAL}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1"> 
		{if $GrupoOpciones2>0}<input name="BuscarRZ" type="hidden" id="BuscarRZ" value="1">{/if}
		<input name="idPersona" type="hidden" id="idPersona" value="{$idPersona}">
		 </td>
    </tr>
  </table>
</form>