{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('desFechaIni','Fecha de Plazo','R');
		return document.MM_returnValue;
	}else
		return false;
}
 {/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
<br>
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> 
      <td colspan="4" class="item-sep"><strong>Reactivaci�n de documentos internos finalizados en la dependencia.</strong></td>
    </tr>	 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Complete los datos: </strong></td>
      <td align="left"><select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
				</select>&nbsp;<input name="numero" type="text" class="iptxtn" id="numero" value="{$numero}" size="5">  -  
	  	  <select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>{$selAnio}
		</select>/
		<select name="siglasDep" class="ipseln" id="siglasDep">					
					{$selSiglasDep}
       			</select>
		  &nbsp;&nbsp;
		  <input type="button" name="Busca" value="Buscar" class="submitV2" onClick="submitForm('{$accion.FRM_REACTIVA_DOC_INTERNO}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if ($idDocumento==0||!$idDocumento)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, ya que no est&aacute; finalizado o no est&aacute; en su &aacute;mbito!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left">{$asunto}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item">&nbsp; </td>
      <td align="left">{$UbiActual}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
	
    <tr align="center"> 
      <td colspan="5">{if $idDocumento>0} <input name="bSubmit" type="Submit" class="submitV2" value="Reactivar" > 
<!--        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue">  -->
         &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_REASIGNA_DOCUMENTO}&menu={$accion.FRM_REASIGNA_DOCUMENTO}&subMenu={$accion.FRM_REASIGNA_DOCUMENTO}');return document.MM_returnValue"> 
        {/if}
		<input name="accion" type="hidden" id="accion" value="{$accion.REACTIVA_DOC_INTERNO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
	  </td>
    </tr>
	
  </table>
</form>