{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalFin','','hide')
	MM_showHideLayers('popCalIni','','hide')
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
-->
</script>
<!-- {/literal} -->
<script src="/sitradocV3/js/src/reportes/frmReporteDocumentos.js"></script>
<br>
<div style="width:700px;">
<div class="std_form">
<form name="{$frmName}" action="{$frmUrl}" method="post" target="_blank" >
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.GENERAREPORTEDIR}">
  <input name="GrupoOpciones1" type="hidden" id="GrupoOpciones1" value="21">
  <input name="versionAnt" type="hidden" id="versionAnt" > 
  <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla-encuestas">
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="label"><b>Tipo de Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipoDocc" class="ipseln" id="tipDoc" onChange="document.{$frmName}.versionAnt.value=2;document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTEDIR}')">
          <option >Todos</option>
          <option value="1"{if $tipoDocc==1} selected{/if}>Documentos Externos</option>
          <option value="2"{if $tipoDocc==2} selected{/if}>Expedientes</option>
          <option value="4"{if $tipoDocc==4} selected{/if}>Documentos Internos</option>
        </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="label"><b>Estado del Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="status" class="ipseln" id="status" onChange="document.{$frmName}.versionAnt.value=2;document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTEDIR}')">
          <!--<option>Todos</option>-->
          <!--<option value="5" {if $status==5} selected{/if} class="std_oculto">Todos</option>-->
          <option value="1" {if $status==1} selected{/if}>Pendientes</option>
          <option value="11" {if $status==11} selected{/if} class="std_oculto">Pendientes (Congreso)</option>
          <option value="2" {if $status==2} selected{/if}>Asignados a un trabajador</option>
          <option value="3" {if $status==3} selected{/if}>Documentos Generados</option>
          <option value="4" {if $status==4} selected{/if}>Archivados</option>
		  {if (($coddep==13||$coddep==2) && $tipoDocc==2)}<option value="9" {if $status==9} selected{/if}>Pendientes que han ingresado a partir del 04/01/08</option>{/if}
		  {if (($coddep==13||$coddep==2) && $tipoDocc==2)}<option value="8" {if $status==8} selected{/if}>Finalizados a partir del 04/01/08</option>{/if}
		  {if ($coddep==1||$coddep==13)}<option value="6" {if $status==6} selected{/if}>Registros asignados</option>{/if}
		  {if ($coddep==13 && $tipoDocc==2)}<option value="7" {if $status==7} selected{/if}>Estadísticas</option>{/if}
        </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
	{if ($status==3 )}
    <tr> 
      <td align="right" class="label">Por tipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipoDoc" class="ipseln" id="tipoDoc" onChange="document.{$frmName}.versionAnt.value=2;document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTEDIR}')">
        {$selTipoDoc}
		{if ($codigoDependenciaF==1)}
			<option value="994" {if ($tipoDoc==994)} selected {/if}>Memorando DM Ases</option>
			<option value="995" {if ($tipoDoc==995)} selected {/if}>Cargo DM Ases</option>
			<option value="996" {if ($tipoDoc==996)} selected {/if}>Informe DM Ases</option>
			<option value="998" {if ($tipoDoc==998)} selected {/if}>Nota DM Ases</option>
			<option value="997" {if ($tipoDoc==997)} selected {/if}>Oficio DM Ases</option>
			<option value="999" {if ($tipoDoc==999)} selected {/if}>Nota DM J.Gab.</option>
		{/if}
       </select>
	  	&nbsp;&nbsp;
	  </td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
	{/if}
    {if ($status==7||$status==1||$status==8||$status==9)} 
    <tr class="std_oculto"> 
      <td align="right" class="textoblack"><b>Por tipo de aprobación</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipAprobacion" class="ipseln" id="tipAprobacion" onChange="document.{$frmName}.versionAnt.value=2;document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTEDIR}')">
          <option >Todos</option>
          <option value="1"{if $tipAprobacion==1} selected{/if}>Silencio Positivo</option>
          <option value="4"{if $tipAprobacion==4} selected{/if}>Silencio Negativo</option>
          <option value="2"{if $tipAprobacion==2} selected{/if}>Automático</option>
		  <option value="3"{if $tipAprobacion==3} selected{/if}>Por definir</option>
       </select></td>
    </tr>
    <tr class="std_oculto"> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    {/if}	
	{if (($status==3||$status==7||$status==8||$status==9||($status==1 && ($coddep==13 || $coddep==2 || $coddep==36 || $coddep==16 || $coddep==5) && $tipoDocc==2)))|| ($status==11) || ($status==1 && $coddep==12)} 
    <tr> 
      <td align="right" class="label"><b>Por Unidad</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="depInicial" class="ipseln" id="depInicial" onChange="document.{$frmName}.versionAnt.value=2;document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTEDIR}')">
		{$selDepInicial}
       </select></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    {/if}
    {if ($status!=3 && $status!=7)} 
    <tr> 
      <td align="right" class="label"><b>Por trabajador</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="trabajador" class="ipseln" id="trabajador"><option value="vacio"></option>
        {$selTrabajador}
       </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    {/if} 
    <tr> 
      <td align="right" class="label">Por Fechas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="left" class="label"><span class="label">Desde</span> <input name="desFechaIni" type="text" class="iptxtn datepicker" id="desFechaIni" tabindex="4" onKeyPress="ninguna_letra();" value="{$desFechaIni}" size="10" /> <span class="label">hasta</span> <input name="desFechaFin" type="text" class="iptxtn datepicker" id="desFechaFin" tabindex="5" onKeyPress="ninguna_letra();" value="{$desFechaFin}" size="10" /></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top"><input name="bsubmit" type="submit" class="std_button" value="Generar" /></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <!--<tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td colspan="2" align="center" class="trees"><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=frmGeneraReporteDir&versionAnt=1"><b>Otros 
        Reportes</b></a></td>-->
    </tr>
  </table>
</form>
</div>
</div>