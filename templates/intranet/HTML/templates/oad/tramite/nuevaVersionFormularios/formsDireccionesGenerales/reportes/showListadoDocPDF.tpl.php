<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<!-- HEADER LEFT "$LOGOIMAGE" -->
<!-- FOOTER LEFT "{$fechaGen}" -->
<!--SI ES PENDIENTE-->
{if ($status==1)}
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS PENDIENTES{if ($nombresTrabajador && $nombresTrabajador!=" ")}<br>{$nombresTrabajador}{/if}</font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="5%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="9%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA DERIVACI&Oacute;N</font></strong></div></td>
        <td width="8%" class="item"><div align="center"><strong><font size="5" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	{section name=i loop=$list} 
	<tr> 
		<td width="5%"  class="item"><font size="4" face="Arial">{$smarty.section.i.iteration}</font></td>
		<td width="10%" class="item"><font size="4" face="Arial">{$list[i].docRecibido}</font></td>
        <td width="9%" class="item"><font size="4" face="Arial">{$list[i].numDocRec}</font></font></td>
        <td width="15%" class="item"><font size="4" face="Arial">{$list[i].tup}</font></td>
        <td width="9%" class="item"><font size="4" face="Arial">{$list[i].nroTD}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].razSoc}</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial">{$list[i].fecIngP}</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial">{$list[i].fecIngD}</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial">{$list[i].fechaAccep}</font></td>
        <td width="9%" class="item"><font size="4" face="Arial">{if ($list[i].ubicacionDepe && $list[i].ubicacionDepe!="")}{$list[i].ubicacionDepe}/<br />{/if}{if ($list[i].trab!=""&& $list[i].trab!='NULL'&& $list[i].trab!=" ")}{$list[i].trab}{else}DIRECTOR{/if}</font></td>
        <td width="9%" class="item"><font size="4" face="Arial">{$list[i].avance}</font></td>
 	</tr>
	{sectionelse} 
	<tr> 
		<td colspan="11" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	{/section}
</table>
<!--SI ES PENDIENTE CONGRESO-->
{elseif ($status==11)}
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS PENDIENTES DEL CONGRESO DE LA REPUBLICA {if ($nombresTrabajador && $nombresTrabajador!=" ")}<br>{$nombresTrabajador}{/if}</font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="5%" class="item"><div align="center"><strong><font size="4" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="4" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="9%" class="item"><div align="center"><strong><font size="4" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="4" face="Arial">ASUNTO</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="4" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="4" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="4" face="Arial">FECHA DERIVACI&Oacute;N</font></strong></div></td>
        <td width="8%" class="item"><div align="center"><strong><font size="4" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	{section name=i loop=$list} 
	<tr> 
		<td width="5%"  class="item"><font size="3" face="Arial">{$smarty.section.i.iteration}</font></td>
		<td width="10%" class="item"><font size="3" face="Arial">{$list[i].docRecibido}</font></td>
        <td width="9%" class="item"><font size="3" face="Arial">{$list[i].numDocRec}</font></font></td>
        <td width="15%" class="item"><font size="3" face="Arial">{$list[i].tup}</font></td>
        <td width="9%" class="item"><font size="3" face="Arial">{$list[i].nroTD}{if 
                    ($list[i].ind && $list[i].ind!="")}<br>
                    {$list[i].ind}{/if}</font></td>
        <td width="10%" class="item"><font size="3" face="Arial">{$list[i].razSoc}</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial">{$list[i].fecIngP}</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial">{$list[i].fecIngD}</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial">{$list[i].fechaAccep}</font></td>
        <td width="9%" class="item"><font size="3" face="Arial">{if ($list[i].ubicacionDepe && $list[i].ubicacionDepe!="")}{$list[i].ubicacionDepe}/<br />{/if}{if ($list[i].trab!=""&& $list[i].trab!='NULL'&& $list[i].trab!=" ")}{$list[i].trab}{else}DIRECTOR{/if}</font></td>
        <td width="9%" class="item"><font size="3" face="Arial">{$list[i].avance}</font></td>
 	</tr>
	{sectionelse} 
	<tr> 
		<td colspan="11" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	{/section}
</table>
<!--SI ES FINALIZADO-->
{elseif ($status==4)}
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS FINALIZADOS {if ($nombresTrabajador && $nombresTrabajador!=" ")}<br>{$nombresTrabajador}{/if}</font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="3%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="7%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="7%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="7%" class="item"><div align="center"><strong><font size="5" face="Arial">FECHA FINALIZACI&Oacute;N</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">MOTIVO DE FINALIZACI&Oacute;N</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	{section name=i loop=$list} 
	<tr> 
	  <td width="3%"  class="item"><font size="4" face="Arial">{$smarty.section.i.iteration}</font></td>
		<td width="10%" class="item"><font size="4" face="Arial">{$list[i].docRecibido}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].numDocRec}</font></font></td>
        <td width="15%" class="item"><font size="4" face="Arial">{$list[i].tup}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].nroTD}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].razSoc}</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial">{$list[i].fecIngP}</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial">{$list[i].fecIngD}</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial">{$list[i].fecT}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{if ($list[i].trab!=""&& $list[i].trab!='NULL'&& $list[i].trab!=" ")}{$list[i].trab}{else}DIRECTOR{/if}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].motivo_finalizacion}</font></td>
        <td width="10%" class="item"><font size="4" face="Arial">{$list[i].avance}</font></td>
 	</tr>
	{sectionelse} 
	<tr> 
		<td colspan="12" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	{/section}
</table>
{else}
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">LISTADO DE DOCUMENTOS{if ($nombresTrabajador && $nombresTrabajador!=" ")}<br>{$nombresTrabajador}{/if}</font></strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td colspan="3" class="textoblack" bgcolor="#999999"> <div align="center"></div>
      														<div align="center"></div>
      														<div align="center"><strong><font size="6" face="Arial">DOCUMENTO</font></strong></div>
					   </td>
   					   <td width="30%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
   					   <td width="5%"rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA INGRESO MIDIS</font></strong></div></td>
   					   <td width="5%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">{if $sit==4}FECHA DE DERIVACI�N{else}FECHA INGRESO DEPENDENCIA{/if}</font></strong></div></td>
					   {if ($status==1||$status==9||$status==5)}<td width="5" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ACEPTACI�N</font></strong></div></td>{/if}
    					{if $status==1}<td width="5" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">D�AS �TILES</font></strong></div></td>{/if}
						{if $status==4}<td width="8%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA DE T&Eacute;RMINO</font></strong></div></td>{/if}
					   {if !$nombresTrabajador}<td width="40%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>{/if}
					   <td width="20%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">COMENTARIO</font></strong></div></td>
  						<td width="20%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
                     </tr>
  					 <tr bgcolor="#999999"> 
   					   <td class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
   					   <td width="35%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">{if $tipDoc==2}TUPA{else}ASUNTO{/if}</font></strong></div></td>
   					   <td width="8%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
  					 </tr>
  					{section name=i loop=$list} 
  					 <tr> 
    					<td width="5%" class="item"><div align="center"><font size="4" face="Arial">{$smarty.section.i.iteration}</font></div></td>
    					<td width="35%" class="item"><font size="4" face="Arial">{$list[i].tup}</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial">{$list[i].nroTD}{if ($list[i].ind && $list[i].ind!="")} - {$list[i].ind}{/if}</font></td>
    					<td width="30%" class="item"><font size="4" face="Arial">{$list[i].razSoc}</font></td>
    					<td width="5%" class="item"><font size="4" face="Arial">{if $tipDoc==4}NO TIENE{else}{$list[i].fecIngP}{/if}</font></td>
    					<td width="5%" class="item"><font size="4" face="Arial">{$list[i].fecIngD}</font></td>
						{if ($status==1||$status==9||$status==5)}<td width="5" class="item"><font size="4" face="Arial">{$list[i].fechaAccep}</font></td>{/if}
    					{if $status==1}<td width="5" class="item"><font size="4" face="Arial">{$list[i].fecT}</font></td>{/if}
						{if $status==4}<td width="8%" class="item"><font size="4" face="Arial">{$list[i].fecT}</font></td>{/if}
						{if !$nombresTrabajador}<td width="40%" class="item"><font size="4" face="Arial">{if ($list[i].ubicacionDepe && $list[i].ubicacionDepe!="")}{$list[i].ubicacionDepe}/{/if}{if ($list[i].trab!=""&& $list[i].trab!='NULL'&& $list[i].trab!=" ")}{$list[i].trab}{else}DIRECTOR{/if}</font></td>{/if}
						<td width="20%" class="item"><font size="4" face="Arial">{$list[i].comentario}</font></td>
  						  <td width="20%" class="item"><font size="4" face="Arial">{$list[i].avance}</font></td>
                	 </tr>
  					{sectionelse} 
  					 <tr> 
    					<td colspan="{if $sit==2}8{else}9{/if}" class="textored"><div align="center"><strong><font size="4" face="Arial">NO SE HAN ENCONTRADO RESULTADOS</font></strong></div></td>
  					 </tr>
  					{/section} 
		  	      </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
</table>
{/if}
</body>
</html>
