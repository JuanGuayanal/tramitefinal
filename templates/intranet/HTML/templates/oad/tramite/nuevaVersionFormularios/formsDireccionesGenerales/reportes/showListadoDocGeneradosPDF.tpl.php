<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<!-- HEADER LEFT "$LOGOIMAGE" -->
<!-- FOOTER LEFT "{$fechaGen}" -->
<!-- REPORTE DE DOCUMENTOS DERIVADOS A OTRA DEPENDENCIA -->
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS GENERADOS EN LA DEPENDENCIA DE ACUERDO A LA FECHA DE SALIDA {if $desFechaIni!=$desFechaFin}ENTRE EL
				{$desFechaIni} Y EL {$desFechaFin}{else}DEL {$desFechaFin}{/if}</font></strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
    					<td width="21%" bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
    					<td width="8%" bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
   					   <td width="20%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
   					   <td width="5%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA DE DERIVACI&Oacute;N</font></strong></div></td>
   					   <td width="5" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
    				   <td width="8%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">OFICINA DESTINO</font></strong></div></td>
    				   <td width="8%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ACCIONES</font></strong></div></td>
					   <td width="20%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">DESTINATARIO DE CORRESPONDENCIA</font></strong></div></td>
  					 </tr>
  					 
  					{section name=i loop=$list} 
  					 <tr> 
    					<td width="5%" class="item"><div align="center"><font size="4" face="Arial">{$smarty.section.i.iteration}</font></div></td>
    					<td width="21%" class="item"><font size="4" face="Arial">{$list[i].tipDoc}</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial">{$list[i].ind}</font></td>
    					<td width="20%" class="item"><font size="4" face="Arial"><font size="4" face="Arial">{if $list[i].asunto==""}{$list[i].asunto2}{else}{$list[i].asunto}{/if}</font></font></td>
    					<td width="5%" class="item"><font size="4" face="Arial">{$list[i].fecCrea}</font></td>
    					<td width="5" class="item"><font size="4" face="Arial">{if $list[i].ref==""&& $list[i].numTram==""}{$list[i].refsinRep|default:'No especificado'}{else}{$list[i].ref}<br>{$list[i].numTram}{/if}</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial">{$list[i].dep}</font></td>
                        <td width="8%" class="item"><font size="4" face="Arial"><font size="4" face="Arial">{$list[i].accionesHojaDerivacion}</font></font></td>
						<td width="20%" class="item"><font size="4" face="Arial"><font size="4" face="Arial">{$list[i].obs}</font></font></td>
  					 </tr>
  					{sectionelse} 
  					 <tr> 
    					<td colspan="{if $sit==2}9{else}9{/if}" class="textored" width="100%"><div align="center"><strong><font size="4" face="Arial">NO SE HAN ENCONTRADO RESULTADOS</font></strong></div></td>
  					 </tr>
  					{/section} 
		  	      </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
</table>
</body>
</html>
