{$jscript}
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('asunto','Asunto','R','observaciones','Observaciones','R');
		return document.MM_returnValue;
	}else
		return false;
}
$(document).ready(function(){
	$('#btn_guardar').click(function(){
		var mensaje='';
		/*if($.trim($('textarea[name=asunto]').val())==''){
			mensaje=mensaje+'- Asunto es un dato obligatorio.\n';
		}*/
		if($('select[name=tipoDocc]').val()!=74 && $('textarea[name=asunto]').val()==''){
			mensaje=mensaje+'- Asunto es un dato obligatorio.\n';
		}
		/*if($.trim($('textarea[name=observaciones]').val())==''){
			mensaje=mensaje+'- Observaciones es un dato obligatorio.\n'
		}*/
		
		if($('select[name=tipoDocc]').val()==74){
			if($("#acc input:checked").length==0){mensaje=mensaje+'- Debe seleccionar por lo menos una acci\u00f3n'}
		}
		if(mensaje==''){
			if(confirm('�Desea Guardar este Registro?')){return true;}
			else{return false;}
		}
		else{
			alert(mensaje);return false;
		}
	})
});
 {/literal}
-->
</script>
<!--<form action="{$frmUrl}" method="post" name="{$frmName}" enctype="multipart/form-data" onsubmit="return ConfirmaRegistro(document.{$frmName})">-->
<form action="{$frmUrl}" method="post" name="{$frmName}" enctype="multipart/form-data">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> 
      <td colspan="4" class="item-sep"><strong>Cambio en el asunto y / o observaciones de un documento creado en la dependencia.</strong></td>
    </tr>	 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el Nro. de documento</strong></td>
      <td align="left"><select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
					{if ($codigoDependencia==1)}
					<option value="994"{if $tipoDocc==994} selected{/if}>Cargo DM-Ases</option>
					<option value="995"{if $tipoDocc==995} selected{/if}>Informe DM-Ases</option>
					<option value="996"{if $tipoDocc==996} selected{/if}>Memorando DM-Ases</option>
					<option value="997"{if $tipoDocc==997} selected{/if}>Nota DM-Ases</option>
					<option value="998"{if $tipoDocc==998} selected{/if}>Oficio DM-Ases</option>
					<option value="999"{if $tipoDocc==999} selected{/if}>Proveido DM-Ases</option>
					{/if}
				</select>
				<input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>{$selAnio}
				</select> 
				-MIDIS/PNCM/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  	  &nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="std_button" onClick="submitForm('{$accion.FRM_CAMBIA_DATOS_DOC}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if (!$exito||$exito==0)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
     <!-- <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene opci&oacute;n para modificar los datos del documento, quiz&aacute; est&eacute; anulado!</strong></font></span></td>  19/04/2013-->
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0 && $aceptado==0}
    <tr> 
      <td class="item"><strong>Datos del documento</strong> </td>
      <td align="left">{$claseDocumento} {$indicativo} {$auditmod} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item" valign="middle"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="80" rows="4" class="iptxtn" id="textarea" >{$asunto}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    {if $tipoDocc==74}
	<tr align="left"> 
      <td class="item"><strong>Acciones</strong></td>                                                 
      <td><table width="500" id="acc">{$acciones}
        </table></td>
        <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    {/if}
    <tr valign="middle"> 
      <td class="item"><strong>Observaciones</strong></td>
      <td align="left"><textarea name="observaciones" cols="80" rows="5" class="iptxtn" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if ($codigoDependencia==5 || $codigoTrabajador==785 || $codigoTrabajador==865 || $codigoTrabajador==1456 || $codigoTrabajador==712 || $codigoDependencia==13 || $codigoTrabajador==1856 || $codigoDependencia==8 || $codigoDependencia==18 || $codigoTrabajador==175 || $codigoTrabajador==418)}
	<tr class="std_oculto"> 
      <td class="td-encuesta texto"><strong>Archivo</strong></td>
      <td align="left"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}	
	{/if}
    {if $aceptado>0}    <tr> 
      <td colspan="3" class="item"> <div align="center"><strong><font color="#FF0000">El documento ya fue aceptado.</font></strong></div></td>
    </tr>
    {/if}
    {if $noexiste==1}    <tr> 
      <td colspan="3" class="item"> <div align="center"><strong><font color="#FF0000">El documento no existe.</font></strong></div></td>
    </tr>
    {/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5">{if $aceptado==0 || $noexiste==0}<input name="bSubmit" type="Submit" class="std_button" value="Guardar cambios" id="btn_guardar">{/if} 
<!--        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue">  22/05/2013 -->
       &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_REASIGNA_DOCUMENTO}&menu={$accion.FRM_REASIGNA_DOCUMENTO}&subMenu={$accion.FRM_REASIGNA_DOCUMENTO}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.CAMBIA_DATOS_DOC}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		 </td>
    </tr>
  </table>
</form>