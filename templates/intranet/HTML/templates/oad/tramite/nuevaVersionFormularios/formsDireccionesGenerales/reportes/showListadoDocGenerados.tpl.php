<html>
<head>
<title>LISTADO DE DOCUMENTOS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sitra {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #003366;
	background-color: #FEF7C1;
	border: 1px #E9E9E9;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA" class="contenido"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
         <td class="texto"><strong><a href="{$frmUrl}?accion={$accion.LISTADO_REPORTESS}&tipoDocc={$tipoDocc}&status={$status}&depInicial={$depInicial}&trabajador={$trabajador}&print=1&desFechaIni={$desFechaIni}&desFechaFin={$desFechaFin}&tipoDoc={$tipoDoc}&checkEscaneado={$checkEscaneado}" target="_self"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a>&nbsp;<a href="{$frmUrl}?accion={$accion.LISTADO_REPORTESS}&tipoDocc={$tipoDocc}&status={$status}&depInicial={$depInicial}&trabajador={$trabajador}&print=2&desFechaIni={$desFechaIni}&desFechaFin={$desFechaFin}&tipoDoc={$tipoDoc}&checkEscaneado={$checkEscaneado}" target="_blank"><img src="/img/800x600/ico-acrobat.gif" width="20" height="20" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir en PDF</a>&nbsp;<a href="{$frmUrl}?accion={$accion.LISTADO_REPORTESS}&tipoDocc={$tipoDocc}&status={$status}&depInicial={$depInicial}&trabajador={$trabajador}&print=3&desFechaIni={$desFechaIni}&desFechaFin={$desFechaFin}&tipoDoc={$tipoDoc}&checkEscaneado={$checkEscaneado}" target="_self"><img src="/img/800x600/ico_excel2.jpg" width="23" height="23" hspace="2" vspace="2" border="0" align="absmiddle">Exportar a Excel</a></strong></td>
         <td align="right" class="texto"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp;</strong></td>
        </tr>
      </table>
	 </td>
  </tr>
  <tr> 
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        <tr> 
          <td align="left" bgcolor="#FFFFFF"><img src="/images/0/0/logoCONVENIO_SITRADOC.gif" width="124" height="30"></td>
          <td align="right" bgcolor="#FFFFFF"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
        </tr>
      </table>
	</td>
  </tr>
  <tr align="center"> 
    <td height="40" class="textoblack"><br>
        <strong>REPORTE DE DOCUMENTOS GENERADOS EN LA DEPENDENCIA DE ACUERDO A LA FECHA DE SALIDA {if $desFechaIni!=$desFechaFin}ENTRE EL  
        {$desFechaIni} Y EL {$desFechaFin}{else}DEL {$desFechaFin}{/if}</strong><br>
    </td>
  </tr>

  <tr> 
    <td height="8" bgcolor="#CCCCCC"> 
<table border="0" width="100%" align="center" cellpadding="0" cellspacing="1">
        <tr style="height: 25px" class="item-sitra"> 
    <td class="texto" align="center"> <strong>N&deg;</strong></div></td>
    <td class="texto" align="center" width="8%"> <strong>DOCUMENTO</strong></div></td>
    <td class="texto" align="center" width="10%"> <strong>N&deg; DOC.</strong></div></td>
    <td class="texto" align="center" width="35%"> <strong>ASUNTO</strong></div></td>
    <td class="texto" align="center" width="10%"> <strong>FECHA DE DERIVACI&Oacute;N</strong></div></td>
	<td class="texto" align="center" width="10%"> <strong>REFERENCIA</strong></div></td>
    <td class="texto" align="center" width="5%"> <strong>ACCIONES</strong></div></td>
    <td class="texto" align="center" width="5%"> <strong>OFICINA DESTINO</strong></div></td>
    <td class="texto" align="center" width="35%"> <strong>DESTINATARIO DE CORRESPONDENCIA</strong>      </div></td>
    <!--<td width="6"> <font size="5"><strong>DEP.ACTUAL</strong></font></div></td>-->
  </tr>
  {section name=i loop=$list} 
  <tr> 
          <td bgcolor="#FFFFFF" class="texto">{$smarty.section.i.iteration}</div></td>
          <td width="8%" bgcolor="#FFFFFF" class="texto">{$list[i].tipDoc}</td>
	      <td width="10%" bgcolor="#FFFFFF" class="texto">{$list[i].ind} 
		  {if ($list[i].valRuta==1)}
						  {if ($list[i].idClaseDoc==1 && $list[i].idDepOrigen==5)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/oficios/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==2 && $list[i].idDepOrigen==5)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/memorandos/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==21 && $list[i].idDepOrigen==5)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/notas/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==22 && $list[i].idDepOrigen==5)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/informes/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==27 && $list[i].idDepOrigen==5)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/oficiosMultiples/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  
						  {if ($list[i].idClaseDoc==2 && $list[i].idDepOrigen==1)}<a href="/institucional/aplicativos/oad/sitradocV2/DM/memo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==22 && $list[i].idDepOrigen==1)}<a href="/institucional/aplicativos/oad/sitradocV2/DM/informe/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==21 && $list[i].idDepOrigen==1)}<a href="/institucional/aplicativos/oad/sitradocV2/DM/nota/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==20 && $list[i].idDepOrigen==1)}<a href="/institucional/aplicativos/oad/sitradocV2/DM/cargo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==37 && $list[i].idDepOrigen==1)}<a href="/institucional/aplicativos/oad/sitradocV2/DM/proveido/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  
						  {if ($list[i].idClaseDoc==1 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/oficios/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==2 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/memo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==22 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/informe/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==21 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/nota/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==20 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/cargo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==37 && $list[i].idDepOrigen==50)}<a href="/institucional/aplicativos/oad/sitradocV2/DM-Ases/proveido/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  
						  {if ($list[i].idClaseDoc==1 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/oficios/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==2 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/memo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==22 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/informe/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==21 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/nota/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==20 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/cargo/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==27 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/oficiosmultiples/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  {if ($list[i].idClaseDoc==37 && $list[i].idDepOrigen==13)}<a href="/institucional/aplicativos/oad/sitradocV2/OGTIE/proveido/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
						  
						  
						  {if ($list[i].idClaseDoc==97)}<a href="/institucional/aplicativos/oad/sitradocV2/SG/oficiosDM/{$list[i].id}.pdf" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a>{/if}
		  {/if}
		  </td>
          <td width="35%" bgcolor="#FFFFFF" class="texto">{if $list[i].asunto==""}{$list[i].asunto2}{else}{$list[i].asunto}{/if}</div></td>
	      <td width="10%" bgcolor="#FFFFFF" class="texto">{$list[i].fecCrea}</td>
	      <td width="10%" bgcolor="#FFFFFF" class="texto">
            <!--{if $list[i].ref==""&& $list[i].numTram==""}{$list[i].ind}{else}{$list[i].ref}<br>
	    {$list[i].numTram}{/if}-->
            <a href="{$frmUrl}?accion={$accion.MUESTRA_DETALLE_FLUJODOCDIR}&id={$list[i].idDocumento}" target="_blank"> 
            {if $list[i].ref==""&& $list[i].numTram==""}{$list[i].refsinRep|default:'No 
            especificado'}{else}{$list[i].ref}<br>
    {$list[i].numTram}{/if}
		</a>
		</td>
          <td width="5%" bgcolor="#FFFFFF" class="texto">{$list[i].accionesHojaDerivacion}</td>
          <td width="5%" bgcolor="#FFFFFF" class="texto">{$list[i].dep}</td>
          <td width="35%" bgcolor="#FFFFFF" class="texto">{$list[i].obs}</td>
    <!--<td width="6"><font size="5">{$list[i].ubiActual}</font></td>-->
  </tr>
  {sectionelse} 
  <tr> 
          <td width="100%" colspan="8" bgcolor="#FFFFFF" class="textored"><strong>No 
            existen documentos ingresados en las fecha(s) dada(s).</strong></div></td>
  </tr>
  {/section} 
</table>
</td>
</tr>
  	<tr> 
    	<td> 
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        		<tr> 
          			<td align="left" bgcolor="#FFFFFF" class="textored"><strong><i>Actualizado al {$fechaGen}</i></strong></td>
          			<td align="right" bgcolor="#FFFFFF"><img src="/img/800x600/peruavanza.gif" height="30"></td>
        		</tr>
      		</table>
		</td>
  	</tr>

</table>
<ul>
</ul>
</body>
</html>
