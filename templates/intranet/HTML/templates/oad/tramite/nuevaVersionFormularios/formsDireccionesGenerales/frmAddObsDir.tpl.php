<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('observaciones','Observaciones','R');return document.MM_returnValue">
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"> <strong>Avances</strong></td>
      <td align="left"> <textarea name="observaciones" cols="65" rows="5" class="iptxtn" id="observaciones" >{$observaciones}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Guardar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.BUSCA_DOCDIR}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.BUSCA_DOCDIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ADDOBS_DOCDIR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idObsDir" type="hidden" id="idObsDir" value="{$idObsDir}">
		<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">
		<input name="page" type="hidden" id="page" value="{$page}">
		<input name="FechaIni" type="hidden" id="FechaIni" value="{$FechaIni}">
		<input name="FechaFin" type="hidden" id="FechaFin" value="{$FechaFin}">
		{if $tipEstado>0}<input name="tipEstado" type="hidden" id="tipEstado" value="{$tipEstado}">{/if}		
		{if $checkTodos==1}<input name="checkTodos" type="hidden" id="checkTodos" value="{$checkTodos}">{/if} 
		{if $checkTrab==1}<input name="checkTrab" type="hidden" id="checkTrab" value="{$checkTrab}">{/if} 
		{if $checkRazon==1}<input name="checkRazon" type="hidden" id="checkTodos" value="{$checkRazon}"> {/if}
		{if $checkAsunto==1}<input name="checkAsunto" type="hidden" id="checkTodos" value="{$checkAsunto}"> {/if}
		 </td>
    </tr>
  </table>
</form>