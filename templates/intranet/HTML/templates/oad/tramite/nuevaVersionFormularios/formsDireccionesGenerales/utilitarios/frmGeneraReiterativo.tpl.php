{$jscript}

<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function cargar(pForm,a)
{
		pForm.action="index.php?"+a;
	    pForm.submit();
}
function ConfirmaRegistro(pForm){
	if(confirm('¿Desea Guardar este Registro?')){
		MM_validateForm('idClaseDoc','Clase de Documento','sel','dependencia','Dependencia','sel','asunto','Asunto','R','observaciones','Observaciones','R'{/literal}{if $dependencia==7&& $radio==1},'domicilio','Domicilio','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
  function AddTrab(a){literal}{{/literal}
	if(document.MM_returnValue){literal}{{/literal}
		var frm = document.{$frmName}
		var j = frm.destinatario[frm.destinatario.selectedIndex].value
		
		
		if(j==99999){literal}{{/literal}
			frm.wantsnewDest.value =1
				frm.tipPersonaNew.value = frm.tipPersona[frm.tipPersona.selectedIndex].value
				frm.tipIdentNew.value = frm.tipIdent[frm.tipIdent.selectedIndex].value
				frm.rucNew.value = frm.ruc.value			
		{literal}}{/literal}else{literal}{{/literal}
				frm.tipPersonaNew.value = 55//Asignando un valor cualquiera 
				frm.tipIdentNew.value = 55//Asignando un valor cualquiera 
				frm.rucNew.value = 55//Asignando un valor cualquiera 			
			{literal}}{/literal}
		
			var kk = frm.otroDomi[frm.otroDomi.selectedIndex].value
			if(kk==99999){literal}{{/literal}
				frm.wantsnewDomi.value =1
			{literal}}{/literal}else{literal}{{/literal}
				frm.wantsnewDomi.value =2
			{literal}}{/literal}		
		
		
		
		frm.idDestNew.value = frm.destinatario[frm.destinatario.selectedIndex].value
		frm.codDepaNew.value = frm.codDepa[frm.codDepa.selectedIndex].value
		frm.codProvNew.value = frm.codProv[frm.codProv.selectedIndex].value
		frm.codDistNew.value = frm.codDist[frm.codDist.selectedIndex].value
		frm.domicilioNew.value = frm.domicilio.value
		frm.personaNew.value = frm.observaciones.value
		frm.action = frm.action + a
		frm.accion.value = '{$accion.FRM_GENERA_REITERATIVO}'
		frm.submit()
	{literal}}{/literal}else
		return document.MM_returnValue
{literal}}{/literal}

-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="4" class="item-sep"><strong>Generaci&oacute;n de un documento a una dependencia de SITRADOC en base a una referencia que se encuentra fuera de la dependencia</strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
              <strong>Documento Interno</strong></label></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el {if ($GrupoOpciones1==1||!$GrupoOpciones1)}Nro. de Tr&aacute;mite{else}Nro. de documento{/if}</strong></td>
      <td align="left">{if ($GrupoOpciones1==1||!$GrupoOpciones1)}
	  			<input name="nroTD" type="text" class="iptxtn" id="nroTD" value="{$nroTD}" size="30">-
				<select name="anyo3" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2002"{if $anyo3==2002} selected{/if}>2002</option>
					<option value="2003"{if $anyo3==2003} selected{/if}>2003</option>
					<option value="2004"{if $anyo3==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo3==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo3==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo3==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo3==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo3==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo3==2011} selected{/if}>2011</option>
					<option value="2012"{if $anyo3==2012} selected{/if}>2012</option>-->
					<option value="2013"{if $anyo3==2013} selected{/if}>2013</option>
				</select>
	      {else}
	  			<select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo2==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo2==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo2==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo2==2011} selected{/if}>2011</option>
					<option value="2012"{if $anyo2==2012} selected{/if}>2012</option>-->
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>					
				</select> -MIDIS/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  	  {/if}&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submitV2" onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if (!$exito||$exito==0)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene el acceso para generar un reiterativo!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if $idDocumento>0}
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left">{if $numTram}{$numTram}<br>{/if}{$asunto2}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Ubicaci&oacute;n Actual </strong> </td>
      <td align="left">{$UbiActual}</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Clase de Documento</strong></td>
      <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
        {$selClaseDoc}
        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Derivar a:</strong></td>
      <td align="left"><select name="dependencia" class="ipseln" id="dependencia" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
        {$selDependencia}
        </select>
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if ($dependencia==7)}
    <tr> 
      <td class="item"><strong>&iquest;Genera correspondencia ?</strong> </td>
      <td align="left"><input name="radio" type="radio" value="1" {if ($radio==1)} checked{/if} onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"> 
        <strong>Si</strong> <input type="radio" name="radio" value="0" {if ($radio==0||!$radio)} checked{/if} onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')"><strong>No</strong></td>
      <td colspan="3"><div align="center"></div></td>
    </tr>
	{/if}	
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="80" rows="3" class="iptxtn" id="textarea" >{$asunto}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{if $radio!=1}
    <tr> 
      <td class="item"><strong>{if ($dependencia==7&& $radio==1)}Destinatario{else}Observaciones{/if}</strong> </td>
      <td align="left"><textarea name="observaciones" cols="80" rows="3" class="iptxtn" id="textarea" >{$observaciones}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}
	<!--
	{if ($dependencia==7&& $radio==1)}
    <tr> 
      <td class="texto td-encuesta"><strong>Ubigeo</strong> </td>
      <td class="item">
	  		<select name="codDepa" class="ipseln" id="select3" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
					{$selDepa}
            </select>
			<select name="codProv" class="ipseln" id="select2" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
					{$selProv}
            </select>
			<select name="codDist" class="ipseln" id="select" >
					{$selDist}
              </select>
	  </td>
      <td><strong></strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Domicilio</strong> </td>
      <td><textarea name="domicilio" cols="80" rows="3" class="iptxtn" id="textarea" >{$domicilio}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}
	-->
	{if ($dependencia==7&& $radio==1)}
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>	
    <tr> 
      <td class="item-sep" colspan="4"><strong>M&oacute;dulo para generar una o muchas notificaciones y/o correspondencias.</strong></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta"><a name="der3" id="der3"></a><strong>Posee datos?</strong> </td>
      <td class="item" align="left"><label> 
              <input name="GrupoOpciones2" type="radio" value="1" {if ($GrupoOpciones2==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
              <strong>B&uacute;squeda por Raz&oacute;n Social</strong></label>
			  <label> 
              <input name="GrupoOpciones2" type="radio" value="2" {if ($GrupoOpciones2==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
              <strong>B&uacute;squeda por RUC</strong></label></td>
      <td><strong></strong></td>
    </tr>
    {section name=i loop=$soft}
	<tr> 
      <td class="texto td-encuesta"><strong></strong> </td>
      <td class="item" align="left">{$soft[i].RZ}<br />{$soft[i].desDepartamento}&nbsp;{$soft[i].desProvincia}&nbsp;{$soft[i].desDistrito}<br />
	  {$soft[i].departamento}{$soft[i].provincia}{$soft[i].distrito}:{$soft[i].domicilio}<br />
	  {$soft[i].t}<br />{if ($soft[i].id==99999)}{$soft[i].tipPersona}-{$soft[i].tipIdent}- Nro. Documento: {$soft[i].ruc}{/if}</td>
      <td class="item"><img src="/img/800x600/ico.eliminar.gif" width="12" height="12" border="0" alt="Eliminar" onClick="hL(this);submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')" class="mano"> 
              <input name="idDest[]" type="hidden" id="idDest[]" value="{$soft[i].id}">
			  <input name="personas[]" type="hidden" id="personas[]" value="{$soft[i].RZ}">
			  <input name="domicilios[]" type="hidden" id="domicilios[]" value="{$soft[i].domicilio}">
			  <input name="departamentos[]" type="hidden" id="departamentos[]" value="{$soft[i].departamento}">
			  <input name="provincias[]" type="hidden" id="provincias[]" value="{$soft[i].provincia}">
			  <input name="distritos[]" type="hidden" id="distritos[]" value="{$soft[i].distrito}">
			  <input name="nuevoDomicilios[]" type="hidden" id="nuevoDomicilios[]" value="{$soft[i].t}">
			  <input name="tiposPersonas[]" type="hidden" id="tiposPersonas[]" value="{$soft[i].tipPersona}">
			  <input name="tiposIdentificaciones[]" type="hidden" id="tiposIdentificaciones[]" value="{$soft[i].tipIdent}">
			  <input name="rucs[]" type="hidden" id="rucs[]" value="{$soft[i].ruc}">
			  			  </td>
      <td></td>
    </tr>
	{/section}	
	{/if}
	{if ($GrupoOpciones2>0 && $radio==1)}
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item" align="left"><input name="RZoRUC" type="text" class="iptxtn" value="{$RZoRUC}" size="35" maxlength="50">
	  	&nbsp;&nbsp;<input type="button" name="BuscaRZ" value="BuscarRZ" class="submitV2" onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
	  </td>
      <td><strong></strong></td>
    </tr>
	{/if}
	{if ($radio==1 && $GrupoOpciones2>0)}
    <tr> 
      <td class="texto td-encuesta"><strong><a name="der2" id="der2"></a>Escoja destinatario</strong> </td>
      <td colspan="2" class="item" align="left"><select name="destinatario" class="ipsel2" id="select3" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
		{$selDestinatario}
		{if ($GrupoOpciones2>0 && $RZoRUC=="")}<option value="none" >Seleccione una opción</option>{/if}
		{if ($GrupoOpciones2>0 && $RZoRUC!="" && $BuscarRZ==1)}<option value="99999" {if $destinatario=="99999"} selected{/if}>Agregar nuevo destinatario</option>{/if}
              </select></td>
    </tr>
		{if $destinatario==99999}
		<tr> 
		  <td class="texto td-encuesta"><strong>Llenar datos</strong> </td>
		  <td colspan="2" class="item" align="left"><select name="tipPersona" class="ipseln" id="tipPersona" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
        {$selTipoPersona}
        </select>&nbsp;<select name="tipIdent" class="ipseln" id="tipIdent" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der3')">
        {$selTipoIdentificacion}
        </select>&nbsp;
		<input name="ruc" type="text" class="iptxtn" id="ruc" value="{$ruc}" size="20" maxlength="{if $tipIdent=="1"}8{elseif $tipIdent=="8"}11{else}25{/if}" onKeyPress="solo_num();"></td>
		</tr>
		{/if}	
	{/if}
	{if $radio==1}		
    <tr> 
      <td class="item"><strong>{if $radio==1}Destinatario{else}Observaciones{/if}</strong> </td>
      <td class="item" align="left"><textarea name="observaciones" cols="65" rows="5" class="iptxtn" id="observaciones" onKeyPress="solo_carac();">{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea></td>
      <td>{if $radio==1}<div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div>{/if}</td>
    </tr>
	{/if}
	{if ($destinatario>0 && $radio==1)}
    <tr> 
      <td class="texto td-encuesta"><a name="der5" id="der5"></a><strong>Otras Direcciones</strong> </td>
      <td class="item" align="left"><select name="otroDomi" class="ipsel2" id="select3" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der5')">
		{$selDomAlt}
		<option value="99999" {if $otroDomi=="99999"} selected{/if}>Agregar nueva dirección</option>
              </select></td>
    </tr>
	{/if}	
	{if $dependencia==7&& $radio==1}
	
    <tr> 
      <td class="texto td-encuesta"><a name="der4" id="der4"></a><strong>Ubigeo</strong> </td>
      <td class="item" align="left">
	  		<select name="codDepa" class="ipsel2" id="select3" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der4')">
					{$selDepa}
            </select>
			<select name="codProv" class="ipsel2" id="select2" onChange="submitForm('{$accion.FRM_GENERA_REITERATIVO}');cargar(document.{$frmName},'#der4')">
					{$selProv}
            </select>
			<select name="codDist" class="ipsel2" id="select" >
					{$selDist}
              </select>
	  </td>
      <td><strong></strong></td>
    </tr>	
	
    <tr> 
      <td class="texto td-encuesta"><strong>Domicilio</strong> </td>
      <td class="item" align="left"><textarea name="domicilio" cols="45" rows="4" class="iptxtn" id="domicilio" onKeyPress="solo_carac();">{if !$domicilio}Sin Domicilio{else}{$domicilio}{/if}</textarea>
	  <!--<input name="bDestinatario" type="button" class="submitV2" id="bDestinatario" value="Agregar" onClick="MM_validateForm('observaciones','Destinatario','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'{if $destinatario==99999},'tipPersona','Tipo Persona','Sel','tipIdent','Tipo Identificación','Sel','ruc','RUC o DNI','R'{/if});return AddTrab('#der3');"> 
              <input name="idDestNew" type="hidden" id="idDestNew">
			  <input name="codDepaNew" type="hidden" id="codDepaNew">
			  <input name="codProvNew" type="hidden" id="codProvNew">
			  <input name="codDistNew" type="hidden" id="codDistNew">
			  <input name="personaNew" type="hidden" id="personaNew">
			  <input name="domicilioNew" type="hidden" id="domicilioNew">
			  {if $destinatario==99999}<input name="wantsnewDest" type="hidden" id="wantsnewDest">{/if}
			  <input name="wantsnewDomi" type="hidden" id="wantsnewDomi">			  	  
			  
			  	<input name="tipPersonaNew" type="hidden" id="tipPersonaNew">
				<input name="tipIdentNew" type="hidden" id="tipIdentNew">
				<input name="rucNew" type="hidden" id="rucNew">-->
	  </td>
      <td><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($radio!=1 && $dependencia>0 && $dependencia!=49)}
		<tr>
		  <td>&nbsp;</td>
		<td class="item" align="left"><input name="OpcionConocimiento" type="checkbox" id="checkbox" value="1" {if $OpcionConocimiento==1} checked {/if} onClick="submitForm('{$accion.FRM_GENERA_REITERATIVO}')">
			&iquest;Es de conocimiento? (Si hace check se finalizará automáticamente en la oficina receptora.)</td>
		<td></td>
		</tr>
	{/if}			
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha de Plazo</strong> </td>
      <td class="item" align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
    		<input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="{$datos.fecDesIni}" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
      <td><strong></strong></td>
    </tr>
    <tr>
      <td class="lb1Frm"></td>
      <td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td nowrap="nowrap" class="tdSep1"></td>
      <td class="lb1Frm"></td>
      <td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td></td>
    </tr>	
	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="submitV2" value="Generar" > 
<!--        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue">  22/05/2013 -->
       &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_REASIGNA_DOCUMENTO}&menu={$accion.FRM_REASIGNA_DOCUMENTO}&subMenu={$accion.FRM_REASIGNA_DOCUMENTO}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.GENERA_REITERATIVO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		{if $GrupoOpciones2>0}<input name="BuscarRZ" type="hidden" id="BuscarRZ" value="1">{/if}
		 </td>
    </tr>
  </table>
</form>