{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('motivo','Motivo','R'{/literal}{if $idTipoDoc==1},'tipProced','Clase de Procedimiento','Sel','procedimiento','Procedimiento','Sel'{else},'asunto','Asunto','R'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
{/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>N&uacute;mero de TD </strong></td>
      <td>{$nroOTD}</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Indicativo</strong></td>
      <td> <input name="indicativo" type="text" class="iptxt1" id="indicativo" value="{$indicativo}" size="35" maxlength="80" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>	
	{if $tipoPersona==2}
	<tr> 
      <td class="item"><strong>Raz&oacute;n Social</strong> </td>
      <td><textarea name="razonsocial" cols="70" rows="3" class="iptxt1" id="textarea" readonly="readonly">{$razonsocial}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	{else}
    <tr> 
      <td class="item"> <strong>Nombres</strong></td>
      <td> <input name="nombres" type="text" class="iptxt1" id="nombres" value="{$nombres}" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Apellidos</strong></td>
      <td> <input name="apellidos" type="text" class="iptxt1" id="apellidos" value="{$apellidos}" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="item"> <strong>RUC</strong></td>
      <td class="item"> <input name="ruc" type="text" class="iptxt1" id="ruc" value="{$ruc}" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Direcci&oacute;n</strong> </td>
      <td><textarea name="direccion" cols="70" rows="3" class="iptxt1" id="direccion" readonly="readonly">{$direccion}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Dependencia</strong> </td>
      <td>{$dependencia}</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong></strong> </td>
      <td>Status Inicial: {if $idTipoDoc==1} EXTERNO {else} EXPEDIENTE{/if}
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>		
	<tr> 
      <td class="item"><strong>SE CAMBIA A</strong> </td>
      <td>	  {if $idTipoDoc==2}
	  		  <label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($idTipoDoc==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_CAMBIA_TIPO_DOCUMENTO_X}')">
              <strong>Documento Externo</strong></label>
			  {/if}
			  {if $idTipoDoc==1}
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($idTipoDoc==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_CAMBIA_TIPO_DOCUMENTO_X}')">
              <strong>Expediente</strong></label>
			  {/if}
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $idTipoDoc==1}
	<tr> 
      <td class="item"><strong>Clase de Procedimiento</strong> </td>
      <td><select name="tipProced" class="ipsel1" onChange="submitForm('{$accion.FRM_CAMBIA_TIPO_DOCUMENTO_X}')">{$selTipoProcedimiento}</select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Procedimiento</strong></td>
      <td><select name="procedimiento" class="ipsel1" onChange="submitForm('{$accion.FRM_CAMBIA_TIPO_DOCUMENTO_X}')">
																{$selProcedimiento}
																  </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Dependencia</strong></td>
      <td><select name="dependencia2" class="ipsel1" id="dependencia2">
					  {$selDependencia}
						 </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($idTipoDoc==2)}
	<tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td><textarea name="asunto" cols="70" rows="3" class="iptxt1" id="asunto" >{$asunto}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	<tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td><textarea name="observaciones" cols="70" rows="4" class="iptxt1" id="observaciones" >{$observaciones}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Motivo del cambio</strong> </td>
      <td><textarea name="motivo" cols="70" rows="4" class="iptxt1" id="motivo" >{$motivo}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Cambiar Tipo Documento" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.CAMBIA_TIPO_DOCUMENTO_X}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
		<input name="idTipoDoc" type="hidden" id="idTipoDoc" value="{$idTipoDoc}">
		<input name="tipoPersona" type="hidden" id="tipoPersona" value="{$tipoPersona}">
		<input name="dependencia" type="hidden" id="dependencia" value="{$dependencia}">
		{if $id}<input name="id" type="hidden" id="id" value="{$id}">{/if}
		 </td>
    </tr>
  </table>
</form>