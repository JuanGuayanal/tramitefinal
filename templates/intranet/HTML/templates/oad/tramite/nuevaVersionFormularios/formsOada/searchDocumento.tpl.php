{$jscript}
<!-- {literal}  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function cargar(pForm)
{
	var texto=pForm.tipDocumento.value;
	var texto2=pForm.tipBusqueda.value;
		pForm.action="index.php?accion=frmSearchDoc&subMenu=frmSearchDoc&menu=frmSumario&tipDocumento="+texto+"&tipBusqueda"+texto2;
	    pForm.submit();
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function SubmitFormm(pForm,pAccion){
	pForm.accion.value=pAccion
	pForm.submit()
	return false
}
-->
</script>
<!-- {/literal} -->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" >
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DOCUMENTO}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <table width="720" height="30" border="0" align="center" cellpadding="0" cellspacing="2" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif" class="TEmb">
    <tr> 
      <td width="1" class="textoblack">&nbsp;</td>
      <td width="209" class="textoblack"><strong>Tipo de Documento</strong></td>
      <td width="141"><select name="tipDocumento" class="ipseln" onChange="document.{$frmName}.tipDocumento.selected=true;cargar(document.{$frmName})" >
          <option value="3"{if $tipDocumento==3} selected{/if}>Todos</option>
          <option value="1"{if $tipDocumento==1} selected{/if}>Documentos Externos</option>
          <option value="2"{if $tipDocumento==2} selected{/if}>Expedientes</option>
        </select> </td>
      <td width="130" valign="middle"> <input type="submit" class="submitV2" value="Buscar"> 
      </td>
    </tr>
	<tr> 
      <td width="1" class="textoblack">&nbsp;</td>
      <td width="209" class="textoblack"><strong>Estado del Documento</strong></td>
      <td width="141"><select name="tipBusqueda" class="ipseln" onChange="document.{$frmName}.tipBusqueda.selected=true;cargar(document.{$frmName})">
          <!--<option value="9"{if $tipBusqueda==9} selected{/if}>Todos</option>-->
          <option value="1"{if $tipBusqueda==1} selected{/if}>Incompletos</option>
          <option value="2"{if $tipBusqueda==2} selected{/if}>Por Derivar</option>
          <option value="8"{if ($tipBusqueda==8||!$tipBusqueda)} selected{/if}>Derivados</option>
		  <option value="3"{if $tipBusqueda==3} selected{/if}>Finalizados</option>
        </select> </td>
      <td width="130" valign="middle">&nbsp; </td>
    </tr>
	  <tr> 
		<td colspan="4" align="right" class="textogray" ><hr width="100%" size="1"></td>
	  </tr>
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>N&uacute;mero de Tr&aacute;mite Documentario 
        </strong></td>
      <td colspan="2"><input name="nroTD" type="text" class="iptxtn" value="{$nroTD}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td rowspan="2" class="textoblack">&nbsp;</td>
      <td rowspan="2" class="textogray"><strong>Raz&oacute;n Social</strong></td>
      <td colspan="2"><!--<select name="RazonSocial" class="ipsel2" id="select">
			{$selRazonSocial}
               
        </select>--></td>
    </tr>
    <tr>
      <td colspan="2"><input name="RZ" type="text" class="iptxtn" value="{$RZ}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>RUC</strong></td>
      <td colspan="2"><input name="ruc" type="text" class="iptxtn" value="{$ruc}" size="30" maxlength="100"></td>
    </tr>
	<tr>
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Resoluci&oacute;n</strong></td>
	  <td colspan="2" class="textoblack"><select name="tipoResol" class="ipseln" id="select">
						<option value="none"{if $tipoResol==none} selected{/if}>Todos</option>
						<option value="2"{if $tipoResol==2} selected{/if}>RM</option>
						<option value="3"{if $tipoResol==3} selected{/if}>RVM</option>
						<option value="4"{if $tipoResol==4} selected{/if}>RD</option>
						<option value="5"{if $tipoResol==5} selected{/if}>RS</option>
						<option value="7"{if $tipoResol==7} selected{/if}>RCas</option>
        		</select>
				<input name="numero" type="text" class="iptxtn" value="{$numero}" size="10" maxlength="100">
				 -
				<select name="anyo" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2003"{if $anyo==2003} selected{/if}>2003</option>
					<option value="2004"{if $anyo==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo==2007} selected{/if}>2007</option>-->
					<option value="2013"{if $anyo==2013} selected{/if}>2013</option>
				</select> -MIDIS/
				<select name="siglasDepe" class="ipseln" id="siglasDepe">
					{$selSiglasDep}
       			</select>
	  </td>
	</tr>
	{if ($tipBusqueda==8||!$tipBusqueda)}
	<tr>
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Documento</strong></td>
	  <td colspan="2" class="textoblack"><select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>-->
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>
				</select> -MIDIS/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  </td>
	</tr>
	{/if}
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Indicativo/Oficio</strong></td>
      <td colspan="2"><input name="indicativo" type="text" class="iptxtn" value="{$indicativo}" size="30" maxlength="100"></td>
    </tr>
	<tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Derivado inicialmente a</strong></td>
      <td colspan="2"><select name="dependencia" class="ipseln" id="select">
			{$selDependencia}
        		</select></td>
    </tr>
	  <tr> 
		<td colspan="4" align="right" class="textogray" ><hr width="100%" size="1"></td>
	  </tr>
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>N&uacute;mero de Procedimiento</strong></td>
      <td colspan="2"><input name="procedimiento" type="text" class="iptxtn" value="{$procedimiento}" size="30" maxlength="100"></td>
    </tr>
	<!--
    <tr>
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Entre las fechas</strong></td>
      <td><select name="dia_ini" class="ipsel2" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipsel2" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
      {$selAnyoIng}
        </select>  
      </td>
      <td><select name="dia_fin" class="ipsel2" id="dia_fin">
      {$selDiaSal}
	    </select> <select name="mes_fin" class="ipsel2" id="mes_fin">
	  {$selMesSal}
        </select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
      {$selAnyoSal}
        </select>  
      </td>
    </tr>
	-->
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Asunto</strong></td>
      <td colspan="2"><input name="asunto" type="text" class="iptxtn" value="{$asunto}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Observaciones</strong></td>
      <td colspan="2"><input name="observaciones" type="text" class="iptxtn" value="{$observaciones}" size="30" maxlength="100"></td>
    </tr>
    <tr> 
      <td class="textoblack">&nbsp;</td>
      <td class="textogray"><strong>Entre las fechas</strong></td>
      <td><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td>&nbsp;<input name="desFechaFin" type="text" class="iptxtn" id="desFechaFin" value="{$desFechaFin}"  tabindex="5" onKeyPress="ninguna_letra();" />
      &nbsp;&nbsp;<!--<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,{$smarty.now|date_format:'%Y'},'{php}echo date('n'){/php}',{$smarty.now|date_format:'%e'});return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>-->
	  <a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
	 <div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
    </tr>
  </table>
  </form>
