{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
{/literal}
-->
</script>
<script src="/sitradocV3/js/src/mesadepartes/frmAdjuntadocumento.js"></script>
<div style="width:750px;">
<div class="std_form">
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('destino','Dependencia destino','Sel','Persona','Persona destino','Sel','documento','El tipo de Documento','Sel','Contenido','El Contenido','R','Folios','N�mero de folios','R');return document.MM_returnValue">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> <br>
      <td width="25%" class="item"><strong>No del documento </strong></td>
      <td colspan="3" class="item" align="left">{$nroadjunto} {$Anyo}<strong class="item">-CUNA M&Aacute;S</strong></td>
    </tr>
	{if $contadorF>=1}
<!--    <tr> 
      <td colspan="4" class="item"><center><strong>EL DOCUMENTO YA EST� FINALIZADO</strong></center></td>
    </tr>-->
	{/if} 
    <tr> 
      <td class="item"><strong>Fecha de Emisi&oacute;n</strong></td>
      <td width="26%" class="item" align="left">{$FechaActual}</td>
      <td width="23%" class="item"><strong>Hora del Emisi�n</strong></td>
      <td width="26%" class="item" align="left">{$HoraActual}</td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
	<tr>
    	<td class="item" align="left" width="134"><strong>Remitente</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn ui-autocomplete-input" id="personal" size="80" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"><input type="hidden" id="id_persona" name="id_persona" value=""></td>
	</tr>
	<tr> 
		<td class="item" align="left" valign="middle"><strong>Apellidos y Nombres<br>Raz�n Social</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_nombre" disabled="disabled" class="iptxtn" id="_nombre"></textarea></td>
	</tr>
	<tr> 
		<td class="item" align="left"><strong>DNI / RUC</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn" id="_ruc" disabled="disabled" value=""></td>
	</tr>
    <tr>
		<td class="item" align="left" valign="middle"><strong>Domicilio</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_domicilio" disabled="disabled" class="iptxtn" id="_domicilio">{$domicilio}</textarea></td>
	</tr>
    <tr> 
      <td class="item" align="left"><strong>Tipo de Documento</strong></td>
      <td align="left"> <select name="Documento" class="ipseln" id="Documento" style="width:190px;">
	  {$selDocumento}
       </select> </td>
      <td class="item"><div align="right"><strong>N&deg; de Folios</strong></div></td>
      <td align="left"><input name="Folios" type="text" class="iptxtn" onKeyPress="solo_num();" value="{if $Folios==''}1{else}{$Folios}{/if}" size="4" maxlength="3"></td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Documento Externo</strong></td>
      <td colspan="3" align="left"> <input type="text" name="Observacion" style="width:190px;" class="iptxtn" value="{$Observacion}" />
      </td>
    </tr>
    <tr> 
      <td class="item" align="left" valign="middle"><strong>Asunto</strong></td>
      <td colspan="3" align="left"> <textarea name="Contenido" cols="80" rows="3" class="iptxtn"></textarea> 
      </td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Destino</strong></td>
      <td colspan="3" align="left"><select name="Destino" class="ipseln" style="width:340px;" id="dependencia">
	  {$selDestino}
       </select> <button id="std_btn_agrega_dep" class="std_button">Agregar</button></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item" align="left"><strong>Se&ntilde;or</strong></td>
      <td colspan="3" align="left"> <select name="Persona" class="ipseln" style="width:190px;">
	  {$selPersona}
	   </select></td>
    </tr>
    <tr>
    	<td colspan="4">
                 <table id="lista_dependencias">
                     <thead>	
                        <tr>
                            <th width="90%">UNIDAD</th>
                            <th>ACCIONES</th>
                            <th>&nbsp;</th>
                        </tr>
                   	</thead>
                    <tbody></tbody>
                 </table>
        </td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item" align="left"><strong>�Responde correspondencia?</strong></td>
      <td colspan="3" align="left"> Si<input type="radio" name="respuesta" value="1" /> No<input type="radio" name="respuesta" value="2" checked="checked"/></td>
    </tr>
    <tr class="std_oculto" id="tr_lcorrespondencia"> 
      <td class="item" align="left"></td>
      <td colspan="3" align="left">
      <table id="lcorrespondencia">
      <thead><tr><th>Documento</th><th>Destino</th><th></th></tr></thead>
		<tbody>
      {section name=i loop=$correspondencia}
      	<tr>
        	<td>{$correspondencia[i].documentoEnviado}</td><td>{$correspondencia[i].destino}</td><td><input type="radio" value="{$correspondencia[i].id}-{$correspondencia[i].idDocumentoEnviado}" name="correspondencia"/></td>
        </tr>
      {/section}
        </tbody>
      </table>
      </td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> <input type="submit" name="Submit" value="Adjuntar Doc.Asociado" class="std_button" id="adjuntar"> 
        &nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ADJUNTA_DOCUMENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
        <input name="id" type="hidden" id="id" value="{$id}">
		<input name="reLoad" type="hidden" id="reLoad" value="1"> 
		<input name="nroadjunto" type="hidden" id="nroadjunto" value="{$nroadjunto}"></td>
    </tr>
  </table>
</form>
</div>
</div>
<div id="modal_deracciones" class="std_oculto">
<div class="std_form">
		<h1>ACCIONES</h1>
        <table>
    	{$acciones}
        </table><br />
<div><span class="std_button" id="btn_aceptarAcciones">Aceptar</span></div>
    </div>
</div>