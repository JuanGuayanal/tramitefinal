{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
{/literal}
-->
</script>
<script src="/sitradocV3/js/src/mesadepartes/frmModAdjunto.js"></script>
<div style="width:750px;">
<div class="std_form">
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('destino','Dependencia destino','Sel','Persona','Persona destino','Sel','tipDocumento','El tipo de Documento','Sel','contenido','El Contenido','R','folios','N�mero de folios','R');return document.MM_returnValue">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> <br>
      <td width="25%" class="item" align="left"><strong>No del documento </strong></td>
      <td colspan="3" class="item" align="left">{$nroadjunto}</td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Fecha de Emisi&oacute;n</strong></td>
      <td width="26%" class="item" align="left">{$FechaActual}</td>
      <td width="23%" class="item" align="left"><strong>Hora del Emisi�n</strong></td>
      <td width="26%" class="item" align="left">{$HoraActual}</td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
	<tr>
    	<td class="item" align="left" width="134"><strong>Remitente</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn ui-autocomplete-input" id="personal" size="80" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"><input type="hidden" value="{$id_persona}" name="id_persona" id="id_persona"/></td>
	</tr>
	<tr> 
		<td class="item" align="left" valign="middle"><strong>Apellidos y Nombres<br>Raz�n Social</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_domicilio" disabled="disabled" class="iptxtn" id="_domicilio">{$razon_social}</textarea></td>
	</tr>
	<tr> 
		<td class="item" align="left"><strong>DNI / RUC</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn" id="_ruc" disabled="disabled" value="{$ruc}"></td>
	</tr>
    <tr>
		<td class="item" align="left" valign="middle"><strong>Domicilio</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_domicilio" disabled="disabled" class="iptxtn" id="_domicilio">{$direccion}</textarea></td>
	</tr>
    <tr> 
      <td class="item" align="left"><strong>Tipo de Documento</strong></td>
      <td align="left"> <select name="tipDocumento" class="ipseln" id="tipDocumento" style="width:190px;">
	  {$selDocumento}
       </select> </td>
      <td class="item"><div align="right"><strong>N&deg; de Folios</strong></div></td>
      <td align="left"><input name="folios" type="text" class="iptxtn" onKeyPress="solo_num();" value="{if $folios==''}1{else}{$folios}{/if}" size="4" maxlength="5"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Documento Externo</strong></td>
      <td colspan="3" align="left"> <input type="text" name="observaciones" style="width:190px;" class="iptxtn" value="{$observaciones}" />
      </td>
    </tr>
    <tr> 
      <td class="item" align="left" valign="middle"><strong>Asunto</strong></td>
      <td colspan="3" align="left"> <textarea name="contenido" cols="50" rows="3" class="iptxtn">{$contenido}</textarea> 
      </td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Destino</strong></td>
      <td colspan="3" align="left"> <select name="Destino" class="ipseln" style="width:340px;" id="dependencia">
	  {$selDestino}
       </select> <button id="std_btn_agrega_dep" class="std_button">Agregar</button></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item" align="left"><strong>Se&ntilde;or</strong></td>
      <td colspan="3" align="left"> <select name="Persona" class="ipseln" style="width:190px;">
	  {$selPersona}
	   </select> </td>
    </tr>
    <tr>
    	<td colspan="4">
                 <table id="lista_dependencias">
                     <thead>	
                        <tr>
                            <th width="90%">UNIDAD</th>
                            <th width="90%">ACCIONES</th>
                            <th>&nbsp;</th>
                        </tr>
                   	</thead>
                    <tbody>
    	    {section name=customer loop=$DependenciasAct}
    	    <tr valign="middle">
            
           <td><input type="hidden" value="{$DependenciasAct[customer].id}" name="dependencias_multiples[]"/>{$DependenciasAct[customer].dependencia}</td>
           <td><input type="hidden" name="arr_derlista[acciones][]" value="{$DependenciasAct[customer].idacciones}"/><input type="text" class="text_deracciones iptxtn" value="{$DependenciasAct[customer].acciones}"/></td>
           <td>{if $DependenciasAct[customer].aceptado==''}<a href="#" class="btn_eliminardep"> X </a>{/if}</td>
    	      </tr>
    	    {/section}
                    </tbody>
                 </table>
        </td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr align="center"> 
  <!--    <td colspan="4"> <input type="submit" name="Submit" value="Modificar Adjunto" class="submitV2">           23/04/2013--->
      <td colspan="4"> <input type="submit" name="Submit" value="Modificar Documento Asociado" class="std_button" id="modificar_adjunto"> 
        &nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue">
        &nbsp; <input name="imphr" type="button" class="std_button" value="Imprimir Hoja de Tr&aacute;mite" onClick="window.open('/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id={$idDocPadre}','HOJA DE TRAMITE','height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0');return false;"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_ADJUNTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
        <input name="idAdj" type="hidden" id="idAdj" value="{$idAdj}"> 
        <input name="idDocPadre" type="hidden" id="idDocPadre" value="{$idDocPadre}"> 
		<input name="nroadjunto" type="hidden" id="nroadjunto" value="{$nroadjunto}"></td>
    </tr>
  </table>
</form>
</div>
</div>
<div id="modal_deracciones" class="std_oculto">
<div class="std_form">
		<h1>ACCIONES</h1>
        <table>
    	{$acciones}
        </table><br />
<div><span class="std_button" id="btn_aceptarAcciones">Aceptar</span></div>
    </div>
</div>