{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} {$numTram} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item" align="left"><strong>Nro de Tr�mite</strong></td>
      <td class="item" align="left">{$numTram} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>	
    <tr> 
      <td class="item" align="left"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"> <strong>Dependencia</strong></td>
      <td align="left"> <select name="dependencia" class="ipseln" id="dependencia" onChange="submitForm('{$accion.FRM_REACTIVA_REGISTRO}')">
        {$selDependencia}
        </select>
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"> <strong>Observaciones</strong></td>
      <td align="left"> <textarea name="observaciones" cols="90" rows="4" class="iptxtn" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Reactivar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.REACTIVA_REGISTRO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="id" type="hidden" id="id" value="{$id}">

		{section name=i loop=$ids}		
		<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">
		{/section}
		
		 </td>
    </tr>
  </table>
</form>