<script src="/sitradocV3/js/src/mesadepartes/frmAddDocumenntoMP.js"></script>
<script src="/sitradocV3/js/src/mesadepartes/frmModDocExterno.js"></script>
{$jscript}
<script language="JavaScript">
<!--
//cargamos los datos que se reciben de este mis pagina 
	opcion_enviado={if $RazonSocial && $RazonSocial!="congreso"}{$RazonSocial}{else}""{/if};
	texto_enviado={if $texto}"{$texto}"{else}""{/if};
{literal}
	contenido_temporal="";//esta variable se utiliza para almacenar el contenido pasado de la caja de texto "texto"
	xmldoc = new ActiveXObject("msxml");//creamos nuestro objeto para conectarnos al xml
{/literal}
	xmldoc.url = "{$archivoXml}";//definimos la ubicacion del archivo xml
{literal}
	function solo_num(){
		if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		}
	}
	function opcion(ascii){
{/literal}
		  var puntero=document.{$frmName};
{literal}
		  var primero = xmldoc.root.children;//selecionamos la primera estiqueta OBS: la etiqueta que engloba todo el cml se ovia
		  var segundo = primero.item(ascii).children;//escogemos la segunda etiqueta del XML pero para una primera etiqueta definida
		  var tamanyo=segundo.length;//cantidad de etiquetas dentro de la primaera etiqueta
		  var contador=-1;
		  var persona;
		  var tam;
		  for(i=0;i<tamanyo;i++){
			if(segundo.item(i).children!=null){
				persona=segundo.item(i).children;//obenemos la cantidad de etiquetas que tiene la segunda etiqueta
				tam=persona.length;				//que para nuetsro caso sera la etiqueta persona
				for(j=0;j<tam;j++){//para cada etiqueta persona se obtiene su texto y se agregar una opcion al select opciones
					contador++;
					var valor=persona.item(j).children;
					puntero.RazonSocial[contador] = new Option(valor.item(1).text,valor.item(0).text);
					if(valor.item(0).text==opcion_enviado){//solo se ejecutara esta line siempre y cuando la opcion sea igual a la opcion enviada
						puntero.RazonSocial.options[contador].selected=true;
						texto_enviado="";
					}
				}
			}
		  }
	}
	function seleccionar(){		//esta funcion se encarga de 
{/literal}
		var puntero=document.{$frmName};
{literal}
		var valor=puntero.texto.value;
		if(valor!=contenido_temporal){//utilizado para no ejecutar operaciones imnecesarias
			if(valor=="")eliminar();
			if((valor.length==1)||(valor.length>1 && puntero.RazonSocial.options.length==0)){//solo se agregaran opciones cuando se introduce la prinera letra
				var ascii=puntero.texto.value.charCodeAt(0);//se obtiene el ascii del primer caracter
				eliminar();//se elimina todas las opciones anteriores
				if((ascii>64 && ascii<96)){//mayusculas
					ascii=ascii-65;		
					opcion(ascii);
				}
				if((ascii>96 && ascii<123)){
					ascii=ascii-97;
					opcion(ascii);
				}
				if((ascii>=48 && ascii<=57)|| ascii==38){
					ascii=ascii;
					opcion(ascii);
				}
			}
		}
	} 
	function eliminar(){
{/literal}
		document.{$frmName}.RazonSocial.options.length=0;
{literal}
	}
	function buscar(){//esta funcion selecciona la opcion mas cercana al texto que se introduce
{/literal}
		var puntero=document.{$frmName};
{literal}
		var contenido=puntero.texto.value;
		if(contenido!=contenido_temporal){//una barrera para no hacer una busqueda imnecesaria 
			if(contenido.length>1){		 //cuando el contenido actual y el anterior contenido de texto
				var valor;
				var tama=puntero.RazonSocial.options.length;
				for(i=0;i<tama;i++){
					valor=puntero.RazonSocial.options[i].text;
					valor=valor.toUpperCase();
					contenido=contenido.toUpperCase();
					var estado=valor.indexOf(contenido);
					if(estado==0){
						puntero.RazonSocial.options[i].selected=true;
						break;
					}
				}
			}
		}
	}
function crear(){
	seleccionar();
	buscar();
{/literal}
	contenido_temporal=document.{$frmName}.texto.value;
{literal}
}
//-->
{/literal}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
{literal}
	var fer_mes=new Array(3,3,4,5,5,6,6,7,9,9,10,11);//mes desde 0->en,1->feb,2->mar,3->abr...
		var fer_dia=new Array(9,10,1,28,29,28,29,30,8,9,1,25);//dia
	function fecha(idform){
	//var idform;//nombre de formulario
		//var anyo_asig=idform.anyo_asig.value;
		var today=new Date()
		var anyo_asig=today.getFullYear();
		var mes_asi=today.getMonth();
		if (mes_asi<10){
			var mes_asig="0"+mes_asi;
		}else{
			var mes_asig=mes_asi;
		}
		var dia_asi=today.getDate();
		if (dia_asi<10){
			var dia_asig="0"+dia_asi;
		}else{
			var dia_asig=dia_asi;
		}
		if(validar(anyo_asig,mes_asig,dia_asig)){
			var num=idform.maxdias.value;
			aumentar(anyo_asig,mes_asig,dia_asig,num,idform);
		}
		else{
			//alert('no es un dia laborable');
		}
		
	}
	function validar(anyo_asig,mes_asig,dia_asig){
		//alert('xx1');
		var fecha=new Date(anyo_asig,mes_asig,dia_asig);
		var dia_semana=fecha.getDay();
		//alert('xx2');alert(dia_semana);
		if((dia_semana==0)||(feriado(mes_asig,dia_asig))||(dia_semana==6)){
			//alert('xx3');
			return false;
		}
		//alert('xx4');
		return true;
	}
	function feriado(mes_asig,dia_asig){
		var fer_mes=new Array(3,3,4,5,5,6,6,7,9,9,10,11);//mes desde 0->en,1->feb,2->mar,3->abr...
		var fer_dia=new Array(9,10,1,28,29,28,29,30,8,9,1,25);//dia
		//alert('yy1');alert(fer_mes.length);alert('yy2');
		for(i=0;i<fer_mes.length;i++){
			if((mes_asig==fer_mes[i])&&(dia_asig==fer_dia[i])){
				return true;
			}
		}
		//alert('yy4');
		return false;
	}
	function fechaMaximaPlazo(idform){
	//var idform;//nombre de formulario
		//var anyo_asig=idform.anyo_asig.value;
		//alert('esta es una prueba');
		//exit();
		
		var today=new Date()
		var anyo_asig=today.getFullYear();
		var mes_asi=today.getMonth();
		if (mes_asi<10){
			var mes_asig="0"+mes_asi;
		}else{
			var mes_asig=mes_asi;
		}
		var dia_asi=today.getDate();
		if (dia_asi<10){
			var dia_asig="0"+dia_asi;
		}else{
			var dia_asig=dia_asi;
		}
		//alert('prueba1');
		//alert(anyo_asig+'-'+mes_asig+'-'+dia_asig);
		if(validar(anyo_asig,mes_asig,dia_asig)){
			//alert('prueba2');
			var num=idform.numeroDiasPlazoTupa.value;
			//alert(num);//exit;
			aumentarFechaPlazo(anyo_asig,mes_asig,dia_asig,num,idform);
		}
		else{
			//alert('prueba3');
			//alert('no es un dia laborable');
		}
		
	}
//////////////////////////////////////////MODIFICANDO////////////////////////////////////////////////////////
	function aumentar(anyo_asig,mes_asig,dia_asig,num,idform){

		var fec=new Date(anyo_asig,mes_asig,dia_asig);
		var numero=num;
		var  cadena="  ";
		while(numero>0){
			var anyo1=fec.getFullYear();
			var mes1=fec.getMonth();
			var dia1=fec.getDate();
			dia1=eval(dia1+" + 1");
			if(validar(anyo1,mes1,dia1)){
				numero=numero-1;			
			}
			fec=new Date(anyo1,mes1,dia1);
		}

		if((fec.getMonth()>=0)&&(fec.getMonth()<=9)){		var	texto="0"+eval(fec.getMonth()+" + 1");		}
		else{												var texto=fec.getMonth(); texto=texto+1;						}
		if((fec.getDate()>=0)&&(fec.getDate()<=9)){			var	texto1="0"+fec.getDate();		}
		else{												var texto1=fec.getDate();			}
		idform.result.value=texto+'/'+texto1+'/'+fec.getFullYear();
	}

	function aumentarFechaPlazo(anyo_asig,mes_asig,dia_asig,num,idform){

		var fec=new Date(anyo_asig,mes_asig,dia_asig);
		var numero=num;
		var  cadena="  ";
		while(numero>0){
			var anyo1=fec.getFullYear();
			var mes1=fec.getMonth();
			var dia1=fec.getDate();
			dia1=eval(dia1+" + 1");
			if(validar(anyo1,mes1,dia1)){
				numero=numero-1;			
			}
			fec=new Date(anyo1,mes1,dia1);
		}

		//if((fec.getMonth()>=0)&&(fec.getMonth()<=9)){		var	texto="0"+eval(fec.getMonth()+" + 1");		}
		if((fec.getMonth()>=0)&&(fec.getMonth()<9)){		var	texto="0"+eval(fec.getMonth()+" + 1");		}
		else{												var texto=fec.getMonth(); texto=texto+1;						}
		if((fec.getDate()>=0)&&(fec.getDate()<=9)){			var	texto1="0"+fec.getDate();		}
		else{												var texto1=fec.getDate();			}
		idform.resultPlazoTupa.value=texto1+'/'+texto+'/'+fec.getFullYear();
	}

/*function SubmitForm(pSel,pForm,pAccion){
	if(pSel.options[pSel.selectedIndex].value!='none'){
		pForm.accion.value=pAccion
		pForm.submit()
	}
}*/
function cargar(pForm,a)
{
	//var texto33=pForm.Observaciones.value;
	//var texto55=pForm.tipo2.value;	 
	//var texto66=pForm.tipo3.value;
	//var pc=pForm.nopc.value;
	//var justo=pForm.aten1.value;
	//var justo2=pForm.aten2.value;	 	 
		//alert(texto33+" "+texto66);	 
		//alert(texto33);
		//pForm.action="index.php?Observaciones"+texto33+a;
		pForm.action="index.php?"+a;
		pForm.submit();
}
function mostrar(pForm)
{
	var mm=pForm.nrotram2.value;
	alert(mm);
}
{/literal}
/*Prueba para que se seleccionen todos los checkbox*/
 {literal}
function seleccionar_todo(p){
  if(p==1){
  var valor=true;
  }else{
  var valor=false;
  }
 {/literal} 
  {$codigo_js}
 {literal}
 }
 {/literal}

/*Prueba para que se seleccionen todos los checkbox*/

</script>

<form action="{$frmUrl}" method="post" name="{$frmName}">
  <table border="0" width="800" cellpadding="2" cellspacing="4" class="tabla-encuestas" {if $TipoDoc=="none"} background="/img/800x600/dnepp/embarcaciones/bg.search.gif"{/if} >
    {if $errors} 
    <tr > 
      <td class="item" colspan="4">{$errors} </td>
    </tr>
    {/if} 
    <tr> <br>
      <td colspan="4" align="center" class="item"> <b>Tipo de Documento</b> 
		<select name="TipoDoc" id="TipoDoc" class="ipseln" {if $TipoDoc!="none"} disabled{/if} onChange="submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');">
			{$selTipoDocumento}
		</select>
<!--Prueba-->		 
	{if $TipoDoc==1}
		<!--<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_DOCUMENTO}&congreso=10&menu={$accion.SUMARIO}&subMenu={$accion.FRM_AGREGA_DOCUMENTO}">
			Congreso
		</a>-->
	{/if}
	<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
		<strong>&nbsp;&nbsp;&nbsp;&nbsp;
	<!--<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}">
			Registro Especial para Digsecovi
		</a>-->
		</strong>		
<!--Prueba-->
		</td>
    </tr>
    {if $TipoDoc>0} 
			<tr> 
			  <td colspan="4"><HR size="1"></td>
			</tr>
			{if $IdUpdate>0}
			<tr> 
			  <td width="100" class="item" align="left"><strong>Documento No.</strong></td>
			  <td class="item" colspan="3" align="left">{$nrotramite}<!--<input name="nrotramite" type="text" class="ip-login contenido" id="nrotramite" value="{$nrotramite}">--></td>
			</tr>
			{/if}
			<tr> 
			  <td class="item" align="left"><strong>Fecha Recepci�n</strong></td>
			  <td class="item" align="left">{$FechaActual}</td>
			  <td class="item" align="left"><strong>Hora de Recepci�n</strong></td>
			  <td class="item" align="left">{$HoraActual}</td>
			</tr>		
		{if $TipoDoc!=3 && $RazonSocial!="congreso"}
			<tr class="std_oculto"> 
			  <td colspan="4" class="item" align="left"><strong>Persona Natural 
				<input name="TipoPersona" type="checkbox" id="TipoPersona" value="1" {if $TipoPersona==1} checked {/if} onClick="document.{$frmName}.TipoDoc.disabled=false;{if $tipBusqueda!=2}if(document.{$frmName}.RazonSocial.options.length>0)document.{$frmName}.RazonSocial[0].selected=true;document.{$frmName}.ruc.value='';document.{$frmName}.domicilio.value='';{/if}submitForm('{$accion.FRM_AGREGA_DOCUMENTO}')">
				</strong></td>
			</tr>    
		{/if}
		{if $TipoDoc!=3 && $RazonSocial!="congreso" && empty($IdUpdate)}
			<tr> 
			  
      <td colspan="4" class="item std_oculto" align="left"><strong>&iquest;Qu&eacute; tipo de Informaci&oacute;n 
        se posee? <select name="tipBusqueda" class="ipseln" onChange="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');cargar(document.{$frmName},'#razsoc')">
          
          <option value="1"{if ($tipBusqueda==1||!$tipBusqueda)} selected{/if}>B�squeda x Raz�n Social</option>
          <option value="2"{if $tipBusqueda==2} selected{/if}>B�squeda x RUC</option>
		  </select></strong></td>
			</tr>    
		{/if}
		{if $RazonSocial!="congreso" && $tipBusqueda!=2}
			<tr class="std_oculto"> 
			  
      <td class="item" align="left"><strong><a name="razsoc" id="razsoc"></a>{if $TipoDoc==3}Congresista/comision{else}{if $TipoPersona==1}Apellidos 
        y Nombre{else}Razon Social{/if}{/if}</strong></td>
			  
      <td colspan="3" class="item" align="left">
		{if empty($IdUpdate)}	  
	  	<input name="texto" type="text" id="texto" class="ipseln" onKeyUp="crear();" value="{$texto}">
        <strong><font color="#FF0000">�Escriba!</font></strong> <br>
		{/if}        
		<select name="RazonSocial" class="ipseln" id="RazonSocial"{if $TipoDoc!=3} onChange="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');cargar(document.{$frmName},'#razsoc')" {/if}>
		{if (!empty($IdUpdate) || ($RazonSocial>0 && ($tipProced!='none' || $procedimiento!='none' || $Derivar==1)))}{$selRazonSocial}{/if}
		</select>
      </td>
			</tr>
		{/if}
		{if ($RazonSocial=="congreso")}
			<tr>
				
      <td class="item"><strong>Congresista/comision</strong> </td>
				<td colspan="3">
					<select name="TipoCongreso" class="ipseln">
						{$selTipoCongreso}
					</select>
				</td>
			</tr>
		{/if}
		{if $TipoDoc!=3 && $RazonSocial!="congreso" && $tipBusqueda!=2}
			<tr class="std_oculto"> 
			  <td class="item" align="left"><strong>{if $TipoPersona==1}Dni{else}Ruc{/if}</strong></td>
			  <td class="item" align="left"><input type="text" class="ip-login contenido" id="ruc" value="{$ruc}" disabled></td>
			  <td colspan="2" class="item">&nbsp;</td>
			</tr>
			<tr class="std_oculto"> 
			  <td class="item" align="left"><strong>Domicilio</strong></td>
			  <td colspan="3" align="left"><textarea cols="80" rows="2" disabled="disabled" class="ip-login contenido" id="domicilio">{$domicilio}</textarea></td>
			</tr>
		{/if}
		{if $tipBusqueda==2}
			<tr> 
			  
      <td class="item" align="left"><strong>RUC</strong></td>
			  <td colspan="3" class="item" align="left"><input type="text" name="ruc2" class="iptxtn" id="ruc2" value="{$ruc2}" onKeyPress="solo_num();" maxlength="11"> <input name="bBusca" type="checkbox" id="bBusca" value="1" {if $bBusca==1} checked {/if} onClick="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');cargar(document.{$frmName},'#razsoc')">
        &iexcl;Click!</td>
			</tr>
			<tr class="std_oculto"> 
			  
      <td class="item" align="left"><strong>{if $TipoPersona==1}Apellidos y Nombre{else}Razon 
        Social{/if}</strong></td>
			  <td colspan="3" align="left"><textarea name="RazonSocial2" cols="80" rows="2" disabled="disabled" class="ip-login contenido" id="RazonSocial2">{$RazonSocial2}</textarea></td>
			</tr>
			<tr class="std_oculto"> 
			  
      <td class="item" align="left"><strong>Domicilio</strong></td>
			  <td colspan="3" align="left"><textarea cols="80" rows="2" name="domicilio2" disabled="disabled" class="ip-login contenido" id="domicilio2">{$domicilio2}</textarea></td>
			</tr>
		{/if}
              <td class="item" align="left" width="134"><strong>Buscar Remitente</strong></td>
			  <td colspan="3" align="left"><input type="text" class="ipseln" id="personal" size="80" autocomplete="off"/><input type="hidden" id="id_persona" name="id_persona" /> <a href="#"  id="lnk_regper">Registrar</a></td>
			</tr>
			<tr> 
      <td class="item" align="left" valign="middle"><strong>Apellidos y Nombres<br>Raz&oacute;n Social</strong></td>
			  <td colspan="3" align="left"><textarea cols="80" rows="2" name="_nombre" disabled="disabled" class="ip-login contenido" id="_nombre"></textarea></td>
			</tr>
			<tr> 
      <td class="item" align="left"><strong>DNI / RUC</strong></td>
			  <td colspan="3" align="left"><input type="text" class="ip-login contenido" id="_ruc" disabled="" value="{$ruc}"></td>
			</tr>
<tr>
			  
      <td class="item" align="left"><strong>Domicilio</strong></td>
			  <td colspan="3" align="left"><textarea cols="80" rows="2" name="_domicilio" disabled="disabled" class="ip-login contenido" id="_domicilio">{$domicilio}</textarea></td>
			</tr>
		{if $TipoDoc==1 || $TipoDoc==3 || $RazonSocial=="congreso"} 
			<tr> 
			  <td class="item" align="left"><strong>Asunto  </strong></td>
			  <td colspan="3" align="left"><textarea name="asunto" cols="80" rows="3" class="iptxtn" id="asunto" >{$asunto}</textarea> 
			  </td>
			</tr>
            <!--     31/05/2013 se agrega Tipo de Asunto  -->
            <tr>
                   <td class="item" align="left"><strong>Tipo Asunto  </strong></td>
                   <td colspan="3" align="left"><select name="idtipoAsunto" class="ipseln" id="idtipoAsunto">
                      {$seltipoAsunto}
                  </select></td>
			</tr>
		{/if}
		{if $TipoDoc==2}
			<tr>
				<td class="item" align="left"><strong><a name="tiproc" id="tiproc"></a>Clase de Procedimiento</strong></td>
				<td align="left"><select name="tipProced" class="ipseln" onChange="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');cargar(document.{$frmName},'#tiproc')">{$selTipoProcedimiento}</select></td>
			</tr>
			<tr> 
			  <td class="item" align="left"><strong><a name="proc" id="proc"></a>Procedimiento</strong></td>
			  <td colspan="3" align="left"> <select name="procedimiento" class="ipseln" onChange="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}');cargar(document.{$frmName},'#proc')">
																{$selProcedimiento}
																  </select> </td>
			</tr>
			<tr><td colspan="4" align="left">
				<table border="0" cellspacing="0">
				  <tr>
					<td class="item"><strong>REQUISITOS</strong></td>
					<td align="right"><input type="button" name="todos" class="submitV2" value="Todos" onclick="seleccionar_todo(1);"></td>
            	<td ><input type="button" name="todos" class="submitV2" value="Ninguno" onclick="seleccionar_todo(2);"></td>
				  </tr>
				 {section name="j" loop=$requisitos}
				  <tr class="item">
					<td colspan="2">{$requisitos[j].descripcion}</td>
					<td><input type="checkbox" name="R{$requisitos[j].codigo}" value="{$requisitos[j].codigo}" {$requisitos[j].checked} {if $Derivar} onClick="document.{$frmName}.TipoDoc.disabled=false;submitForm('{$accion.FRM_AGREGA_DOCUMENTO}')"{/if}></td>
				  </tr>
				  {/section}
				</table>
			</td></tr>
		{/if}
		<tr> 
		  <td class="item" align="left"><strong>Clase de Documento</strong></td>
		  <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc">
		    
					  {$selClaseDoc}
						 
	      </select></td>
		  <td class="item" align="left"><div align="right" class="std_oculto"><strong>Folios</strong>&nbsp;<input name="folio" type="text" class="iptxtn" id="folio" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="5" maxlength="5"></div></td>
		  <td></td>
		</tr>
		<tr> 
		  <td class="item" align="left"><strong>{if $TipoDoc==3}Nro Documento/Oficio{else}Numero de Indicativo{/if}</strong></td>
		  <td align="left">	      <input name="indicativo" type="text" class="iptxtn" id="indicativo" value="{$indicativo}" size="40"></td>
		  <td class="item" align="right"><strong>N&deg; Ticket</strong></td>
		  <td><input name="nticket" type="text" class="iptxtn" id="nticket" value="{$nticket}" size="40" /></td>
		</tr>
		{if $TipoDoc!=3}
			{if $RazonSocial!="congreso"}
			<!--Aqu� pongo las observaciones-->
				<tr class="std_oculto"> 
				  <td class="item" align="left"><strong>Observaciones</strong><input name="maxdias" type="hidden" class="iptxt1" id="maxdias" value="3" size="2" maxlength="5"> 
        <input name="result" type="hidden" class="iptxt1" id="result" value="{$result}"> </td>
				  <td colspan="3" align="left"><textarea name="Observaciones" cols="80" rows="3" class="iptxtn" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
			<!--Aqu� pongo las observaciones-->
			<tr> 
			  <td colspan="4" class="item" align="left"><strong><a name="der" id="der"></a> 
				<input name="Derivar" type="checkbox" id="Derivar" value="1">
				Derivar Ahora</strong></td>
			</tr>
			{/if}
				<tr class="std_oculto tr_derivar"> 
				  <td colspan="4"><hr width="100%" size="1"></td>
				</tr>
				<tr class="std_oculto tr_derivar"> 
				  <td class="item" align="left"> <strong>Derivar a :</strong> </td>
				  <td colspan="3" align="left">    <select name="dependencia" class="ipseln" id="dependencia">{$selDependencia}</select><button id="std_btn_agrega_dep" class="submitV2">+</button></td>
			    </tr>
				
				<!-- Antes en esta parte iban las observaciones 
				<tr> 
				  <td class="item"><strong>Observaciones</strong></td>
				  <td colspan="3"><textarea name="Observaciones" cols="55" rows="2" class="iptxt1" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
				 Antes en esta parte iban las observaciones-->
                 <tr>
                 <td></td>
                 <td colspan="3" align="left">

                 </td>
                 </tr>
			
		{/if}
		<tr> 
		  <td></td>
		  <td></td>
		  <td></td>
		  <td></td>
		</tr>
    {/if}
    <tr class="std_oculto tr_derivar">
    	<td colspan="4">
                 <table class="str_tabla_lista" width="100%" id="std_lista_dependencias">
                     <thead>	
                        <tr>
                            <th width="3%">N&deg;</th><th width="50%">DEPENDENCIA</th>
                            <th>FOLIOS</th>
                            <th>OBSERVACIONES</th>
                            <th>&nbsp;</th>
                        </tr>
                   	</thead>
                    <tbody></tbody>
                 </table>
        </td>
    </tr> 
    <tr align="center"> 
      <td colspan="4"> {if $TipoDoc > 0} 
        <input name="bSubmit" type="Submit" class="submitV2" value="{if $TipoDoc==3}Derivar a DM{else}Ingresar{/if}" onClick="{if $TipoDoc==2}fechaMaximaPlazo(document.{$frmName});{/if}fecha(document.{$frmName});" > 
        &nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue">
        {/if} 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_DOCUMENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{if $IdUpdate>0}<input name="IdUpdate" type="hidden" id="IdUpdate" value="{$IdUpdate}">
        {/if}
		{if $idPersona2}<input name="idPersona2" type="hidden" id="idPersona2" value="{$idPersona2}">{/if}
		{if $tipBusqueda>0}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $TipoDoc>0}<input name="TipoDoc" type="hidden" id="TipoDoc" value="{$TipoDoc}">{/if} 
        <input name="congreso" type="hidden" id="congreso" value="{$congreso}"> <!--por el momento-->
		<input name="resultad" type="hidden" id="resultad" value="{$result}">
		<input name="numeroDiasPlazoTupa" type="hidden" id="numeroDiasPlazoTupa" value="{$numeroDiasPlazoTupa}">
		<input name="resultPlazoTupa" type="hidden" id="resultPlazoTupa" value="{$resultPlazoTupa}"></td>
    </tr>
  </table>
</form>
<div id="modal_regper" class="std_oculto">
	<div class="std_form">
		<iframe src="#" width="700" height="700" id="iframe_regper"></iframe>
	</div>
</div>
<script language="JavaScript">
{literal}
	if(texto_enviado.length>0){
		var c=texto_enviado.charCodeAt(0);//se obtiene el ascii del primer caracter
		if((c>64 && c<96)){//mayusculas
			c=c-65;opcion(c);
		}
		if((c>96 && c<123)){
			c=c-97;opcion(c);
		}
		if((c>=48 && c<=57)|| c==38){
			opcion(c);
		}
	}
	opcion_enviado="";
{/literal}
</script>