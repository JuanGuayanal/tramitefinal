{$jscript}
<script language="JavaScript">
<!--
//cargamos los datos que se reciben de este mis pagina 
	opcion_enviado={if $RazonSocial && $RazonSocial!="congreso"}{$RazonSocial}{else}""{/if};
	texto_enviado={if $texto}"{$texto}"{else}""{/if};
{literal}
	contenido_temporal="";//esta variable se utiliza para almacenar el contenido pasado de la caja de texto "texto"
	xmldoc = new ActiveXObject("msxml");//creamos nuestro objeto para conectarnos al xml
{/literal}
	xmldoc.url = "{$archivoXml}";//definimos la ubicacion del archivo xml
{literal}
	function solo_num(){
		if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		}
	}
	function opcion(ascii){
{/literal}
		  var puntero=document.{$frmName};
{literal}
		  var primero = xmldoc.root.children;//selecionamos la primera estiqueta OBS: la etiqueta que engloba todo el cml se ovia
		  var segundo = primero.item(ascii).children;//escogemos la segunda etiqueta del XML pero para una primera etiqueta definida
		  var tamanyo=segundo.length;//cantidad de etiquetas dentro de la primaera etiqueta
		  var contador=-1;
		  var persona;
		  var tam;
		  for(i=0;i<tamanyo;i++){
			if(segundo.item(i).children!=null){
				persona=segundo.item(i).children;//obenemos la cantidad de etiquetas que tiene la segunda etiqueta
				tam=persona.length;				//que para nuetsro caso sera la etiqueta persona
				for(j=0;j<tam;j++){//para cada etiqueta persona se obtiene su texto y se agregar una opcion al select opciones
					contador++;
					var valor=persona.item(j).children;
					puntero.RazonSocial[contador] = new Option(valor.item(1).text,valor.item(0).text);
					if(valor.item(0).text==opcion_enviado){//solo se ejecutara esta line siempre y cuando la opcion sea igual a la opcion enviada
						puntero.RazonSocial.options[contador].selected=true;
						texto_enviado="";
					}
				}
			}
		  }
	}
	function seleccionar(){		//esta funcion se encarga de 
{/literal}
		var puntero=document.{$frmName};
{literal}
		var valor=puntero.texto.value;
		if(valor!=contenido_temporal){//utilizado para no ejecutar operaciones imnecesarias
			if(valor=="")eliminar();
			if((valor.length==1)||(valor.length>1 && puntero.RazonSocial.options.length==0)){//solo se agregaran opciones cuando se introduce la prinera letra
				var ascii=puntero.texto.value.charCodeAt(0);//se obtiene el ascii del primer caracter
				eliminar();//se elimina todas las opciones anteriores
				if((ascii>64 && ascii<96)){//mayusculas
					ascii=ascii-65;		
					opcion(ascii);
				}
				if((ascii>96 && ascii<123)){
					ascii=ascii-97;
					opcion(ascii);
				}
				if((ascii>=48 && ascii<=57)|| ascii==38){
					ascii=ascii;
					opcion(ascii);
				}
			}
		}
	} 
	function eliminar(){
{/literal}
		document.{$frmName}.RazonSocial.options.length=0;
{literal}
	}
	function buscar(){//esta funcion selecciona la opcion mas cercana al texto que se introduce
{/literal}
		var puntero=document.{$frmName};
{literal}
		var contenido=puntero.texto.value;
		if(contenido!=contenido_temporal){//una barrera para no hacer una busqueda imnecesaria 
			if(contenido.length>1){		 //cuando el contenido actual y el anterior contenido de texto
				var valor;
				var tama=puntero.RazonSocial.options.length;
				for(i=0;i<tama;i++){
					valor=puntero.RazonSocial.options[i].text;
					valor=valor.toUpperCase();
					contenido=contenido.toUpperCase();
					var estado=valor.indexOf(contenido);
					if(estado==0){
						puntero.RazonSocial.options[i].selected=true;
						break;
					}
				}
			}
		}
	}
function crear(){
	seleccionar();
	buscar();
{/literal}
	contenido_temporal=document.{$frmName}.texto.value;
{literal}
}
//-->
{/literal}

{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm({/literal}{if ($tipBusqueda==2)}'RazonSocial2','Raz�n Social2','R'{else}'RazonSocial','Raz�n Social','Sel'{/if}{literal},'indicativo','Indicativo','R','asunto','Asunto','R','observaciones','Observaciones','R');
		return document.MM_returnValue;
	}else
		return false;
}
{/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas" >
    {if $errors} 
    <tr> 
      <td colspan="5" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td class="texto td-encuesta" align="left"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td class="item"><strong>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{$frmUrl}?accion={$accion.FRM_AGREGA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_AGREGA_DOCUMENTO}">
			Ir al ingreso normal
		</a></strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Tipo de tr&aacute;mite </strong></td>
      <td colspan="2" class="texto td-encuesta" align="left"><label>
              <input name="tipTramite" type="radio" value="1" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" {if ($tipTramite==1||!$tipTramite)} checked {/if} >
              Respuesta a un Oficio.</label>
              <br>
	    <label>
              <input name="tipTramite" type="radio" value="2" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" {if ($tipTramite==2)} checked {/if}>
        Descargo a una notificaci&oacute;n.</label>
			  <br>
	    <label>
              <input name="tipTramite" type="radio" value="3" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" {if ($tipTramite==3)} checked {/if} >
        Recurso de Reconsideraci&oacute;n / apelaci&oacute;n a una R.D.</label>
			  <br>
	    <label>
              <input name="tipTramite" type="radio" value="4" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" {if ($tipTramite==4)} checked {/if} >
        Descargo a un expediente.</label>
		</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Dirigido a:</strong></td>
      <td colspan="2" align="left">Digsecovi</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{if ($tipTramite==3)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left">Resoluci&oacute;n</td>
      <td colspan="2" align="left">&nbsp;&nbsp;&nbsp; <select name="tipoResol" class="ipseln" id="select" >
                  <!--<option value="none"{if $tipoResol==none} selected{/if}>Todos</option>-->
                  <option value="2"{if $tipoResol==2} selected{/if}>RM</option>
                  <option value="3"{if $tipoResol==3} selected{/if}>RVM</option>
                  <option value="4"{if ($tipoResol==4||!$tipoResol)} selected{/if}>RD</option>
                  <option value="5"{if $tipoResol==5} selected{/if}>RS</option>
                  <option value="7"{if $tipoResol==7} selected{/if}>RCas</option>
                </select> <input name="numero" type="text" class="iptxtn" value="{$numero}" size="9" maxlength="4" onKeyPress="solo_num();">
                - 
                <select name="anyo" class="ipseln" id="select">
                  <!--<option value="none">Todos</option>-->
                  <!--<option value="2003"{if $anyo==2003} selected{/if}>2003</option>
                  <option value="2004"{if $anyo==2004} selected{/if}>2004</option>
                  <option value="2005"{if $anyo==2005} selected{/if}>2005</option>
                  <option value="2006"{if $anyo==2006} selected{/if}>2006</option>
                  <option value="2007"{if $anyo==2007} selected{/if}>2007</option>
				  <option value="2008"{if ($anyo==2008||!$anyo)} selected{/if}>2008</option>
				  <option value="2009"{if $anyo==2009} selected{/if}>2009</option>
				  <option value="2010"{if $anyo==2010} selected{/if}>2010</option>
				  <option value="2011"{if $anyo==2011} selected{/if}>2011</option>
			      <option value="2012"{if $anyo==2012} selected{/if}>2012</option>-->
				  <option value="2013"{if $anyo==2013} selected{/if}>2013</option>
                </select>
                -MIDIS/Digsecovi &nbsp; <input name="bSubmit1" type="button" class="submitV2" value="Validar" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" />
                </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==1||!$tipTramite)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left">Documento</td>
      <td colspan="2" align="left">&nbsp;&nbsp;&nbsp; <select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
				</select> <input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="9" maxlength="6" onKeyPress="solo_num();">
                - 
                <select name="anyo2" class="ipseln" id="select">
                  <!--<option value="none">Todos</option>-->
                 <!-- <option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
                  <option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
                  <option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
                  <option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
				  <option value="2008"{if ($anyo2==2008||!$anyo2)} selected{/if}>2008</option>
				  <option value="2009"{if $anyo2==2009} selected{/if}>2009</option>
				  <option value="2010"{if $anyo2==2010} selected{/if}>2010</option>
				  <option value="2011"{if $anyo2==2011} selected{/if}>2011</option>
				  <option value="2012"{if $anyo2==2012} selected{/if}>2012</option>-->
				  <option value="2013"{if $anyo2==2013} selected{/if}>2013</option>
                </select>
                -MIDIS/Digsecovi &nbsp; <input name="bSubmit2" type="button" class="submitV2" value="Validar" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" />
                </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==2)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left">Notificaci&oacute;n</td>
      <td colspan="2" align="left">&nbsp;&nbsp;&nbsp;
	  <input name="numero3" type="text" class="iptxtn" value="{$numero3}" size="9" maxlength="5" onKeyPress="solo_num();">
                - 
                <select name="anyo3" class="ipseln" id="select">
                  <!--<option value="none">Todos</option>-->
                 <!-- <option value="2004"{if $anyo3==2004} selected{/if}>2004</option>
                  <option value="2005"{if $anyo3==2005} selected{/if}>2005</option>
                  <option value="2006"{if $anyo3==2006} selected{/if}>2006</option>
                  <option value="2007"{if $anyo3==2007} selected{/if}>2007</option>
				  <option value="2008"{if ($anyo3==2008||!$anyo3)} selected{/if}>2008</option>
				  <option value="2009"{if $anyo3==2009} selected{/if}>2009</option>
				  <option value="2010"{if $anyo3==2010} selected{/if}>2010</option>
				  <option value="2011"{if $anyo3==2011} selected{/if}>2011</option>
				  <option value="2012"{if $anyo3==2012} selected{/if}>2012</option>-->
				  <option value="2013"{if $anyo3==2013} selected{/if}>2013</option>				  
                </select>
                -MIDIS/Digsecovi &nbsp; <input name="bSubmit3" type="button" class="submitV2" value="Validar" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" />              
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==4)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left">Expediente Digsecovi</td>
      <td colspan="2" align="left">&nbsp;&nbsp;&nbsp;
	  <input name="numero4" type="text" class="iptxtn" value="{$numero4}" size="9" maxlength="5" onKeyPress="solo_num();">
                - 
                <select name="anyo4" class="ipseln" id="select">
                  <!--<option value="none">Todos</option>-->
                 <!-- <option value="2004"{if $anyo4==2004} selected{/if}>2004</option>
                  <option value="2005"{if $anyo4==2005} selected{/if}>2005</option>
                  <option value="2006"{if $anyo4==2006} selected{/if}>2006</option>
                  <option value="2007"{if $anyo4==2007} selected{/if}>2007</option>
				  <option value="2008"{if ($anyo4==2008||!$anyo4)} selected{/if}>2008</option>
				  <option value="2009"{if $anyo4==2009} selected{/if}>2009</option>
				  <option value="2010"{if $anyo4==2010} selected{/if}>2010</option>
				  <option value="2011"{if $anyo4==2011} selected{/if}>2011</option>
				  <option value="2012"{if $anyo4==2012} selected{/if}>2012</option>	-->
				  <option value="2013"{if $anyo4==2013} selected{/if}>2013</option>				  
                </select>
                -MIDIS/Digsecovi &nbsp; <input name="bSubmit4" type="button" class="submitV2" value="Validar" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" />              
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==1)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Mensaje</strong></td>
      <td colspan="2" align="left">{if ($contPadre==0)}{$mensajeError}{else}Oficio {$indicativoOficio}. Asunto: {$asuntoOficio}. Fecha: {$fechaOficio}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==2)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Mensaje</strong></td>
      <td colspan="2" align="left">{if ($contPadre2==1)}{$mensajeError2}{else}Notificaci�n {$numeroNotificacion}. Destinatario: {$destinatarioNotificacion}. Domicilio: {$domicilioNotificacion}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==3)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Mensaje</strong></td>
      <td colspan="2" align="left">{if ($contPadre3==1)}{$mensajeError3}{else}Resoluci�n {$numeroResolucion}. Sumilla: {$sumillaResolucion}. Firma: {$firmaResolucion}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
	{if ($tipTramite==4)}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Mensaje</strong></td>
      <td colspan="2" align="left">{if ($contPadre4==1)}{$mensajeError4}{else}Expediente: {$actoNotificacionExpediente}. Notificaci�n {$numeroNotificacionExpediente}. Destinatario: {$destinatarioNotificacionExpediente}. Domicilio: {$domicilioNotificacionExpediente}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" >&nbsp;</td>
      <td colspan="2" align="left"><strong>Persona Natural 
				<input name="TipoPersona" type="checkbox" id="TipoPersona" value="1" {if $TipoPersona==1} checked {/if} onClick="{if $tipBusqueda!=2}if(document.{$frmName}.RazonSocial.options.length>0)document.{$frmName}.RazonSocial[0].selected=true;document.{$frmName}.ruc.value='';document.{$frmName}.domicilio.value='';{/if}submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}')">
				</strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" ><strong>&nbsp;</strong></td>
      <td colspan="2" align="left"><strong>&iquest;Qu&eacute; tipo de Informaci&oacute;n 
        se posee? <select name="tipBusqueda" class="ipseln" onChange="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');">
          
          <option value="1"{if ($tipBusqueda==1||!$tipBusqueda)} selected{/if}>B�squeda x Raz�n Social</option>
          <option value="2"{if $tipBusqueda==2} selected{/if}>B�squeda x RUC</option>
		  </select></strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{if !$tipBusqueda||$tipBusqueda==1}
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Digite la raz&oacute;n social o RUC:</strong></td>
      <td colspan="2" class="item" align="left"><!--<input name="texto" type="text" class="iptxt1" id="texto" value="{$texto}" size="35">
				<input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}')">-->
				{if empty($IdUpdate)}	  
	  	<input name="texto" type="text" id="texto" class="ipseln" onKeyUp="crear();" value="{$texto}">
        <strong><font color="#FF0000">�Escriba!</font></strong> <br>
		{/if}        
		<select name="RazonSocial" class="ipseln" id="RazonSocial" onChange="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');" >
		{$selRazonSocial}
		</select>	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<!--
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="td-encuesta texto"><strong>Raz&oacute;n Social </strong></td>
      <td><select name="razonSocial" class="ipsel1" id="razonSocial">
        {$selRazonSocial}
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	-->
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Nro. Documento :</strong></td>
      <td colspan="2" align="left"><input type="text" class="ip-login contenido" id="ruc" value="{$ruc}" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Domicilio:</strong></td>
      <td colspan="2" align="left"><textarea cols="80" rows="2" disabled="disabled" class="ip-login contenido" id="domicilio">{$domicilio}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
	{if $tipBusqueda==2}
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>RUC</strong></td>
      <td colspan="2" align="left"><input type="text" name="ruc2" class="iptxtn" id="ruc2" value="{$ruc2}" onKeyPress="solo_num();" maxlength="11"> <input name="bBusca" type="checkbox" id="bBusca" value="1" {if ($bBusca==1&& $tipBusqueda==2)} checked {/if} onClick="submitForm('{$accion.FRM_AGREGA_REGISTRO_DIGSECOVI}');">
        &iexcl;Click!</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>{if $TipoPersona==1}Apellidos y Nombre{else}Razon 
        Social{/if}</strong></td>
      <td colspan="2" align="left"><textarea name="RazonSocial2" cols="80" rows="2" disabled="disabled" class="ip-login contenido" id="RazonSocial2">{$RazonSocial2}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td height="25" class="texto td-encuesta" align="left"><strong>Domicilio</strong></td>
      <td colspan="2" align="left"><textarea cols="80" rows="2" name="domicilio2" disabled="disabled" class="ip-login contenido" id="domicilio2">{$domicilio2}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" >(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Indicativo</strong></td>
      <td colspan="2" align="left"><input name="indicativo" type="text" class="iptxtn" id="indicativo" value="{$indicativo}" size="15" maxlength="50"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Folios</strong></td>
      <td colspan="2" align="left"><input name="folio" type="text" class="iptxtn" id="folio" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="5" maxlength="4"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Asunto</strong></td>
      <td colspan="2" class="item" align="left"><textarea name="asunto" cols="70" rows="4" class="iptxtn" id="asunto" >{$asunto}</textarea></td>
      <td><strong></strong></td>
    </tr>	
    <tr> 
      <td class="item">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><strong>Observaciones</strong></td>
      <td colspan="2" class="item" align="left"><textarea name="observaciones" cols="70" rows="4" class="iptxtn" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea></td>
      <td><strong></strong></td>
    </tr>
    <tr> 
      <td colspan="5" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5">
		{if (($contPadre==1 && $tipTramite==1 && $idOficio>0)||($contPadre2==0 && $tipTramite==2 && $idNotificacion>0)||($contPadre3==0 && $tipTramite==3 && $idResolucion>0)||($contPadre4==0 && $tipTramite==4 && $idNotificacionExpediente>0))}<input name="bSubmit" type="submit" class="submitV2" value="Crear" />{/if}
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_REGISTRO_DIGSECOVI}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{if $tipBusqueda==2}<input name="bBusca" type="hidden" id="subMenu" value="1">{/if}
		{if $idPersona2}<input name="idPersona2" type="hidden" id="idPersona2" value="{$idPersona2}">{/if}		
		{if ($tipTramite==1 && $idOficio>0)}<input name="idOficio" type="hidden" id="idOficio" value="{$idOficio}">{/if}
		{if ($tipTramite==2 && $idNotificacion>0)}<input name="idNotificacion" type="hidden" id="idNotificacion" value="{$idNotificacion}">{/if}
		{if ($tipTramite==3 && $idResolucion>0)}<input name="idResolucion" type="hidden" id="idResolucion" value="{$idResolucion}">{/if}
		{if ($tipTramite==4 && $idNotificacionExpediente>0)}<input name="idNotificacionExpediente" type="hidden" id="idNotificacionExpediente" value="{$idNotificacionExpediente}">{/if}
	  </td>
    </tr>
  </table>
</form>