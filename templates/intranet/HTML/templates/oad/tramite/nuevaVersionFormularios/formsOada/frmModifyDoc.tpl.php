<script src="/sitradocV3/js/src/mesadepartes/frmModDocExterno.js"></script>
{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('dependencia','Dependencia','Sel','folios','Folios','R','RZ','Raz�n Social','R'{/literal}{if $idTipoDoc==2},'tipProced','Clase de Procedimiento','Sel','procedimiento','Procedimiento','Sel'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
{/literal}
-->
</script>
<br>
<div style="width:780px;">
<div class="std_form">
<!--<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
--><form action="{$frmUrl}" method="post" name="{$frmName}">
  <table border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item" align="left"><strong>Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left" width="20%"> <strong>N&uacute;mero de OTD </strong></td>
      <td align="left">{$nroOTD}</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong> Digite la Raz&oacute;n Social </strong></td>
      <td align="left"><input name="RZ" type="text" class="iptxtn" id="RZ" value="{$RZ}" size="70">
	  <input type="button" name="Busca" value="Buscar" class="submitV2" onClick="submitForm('{$accion.FRM_MODIFICA_DOCUMENTO}')"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $tipoPersona==2}
	<tr> 
      <td class="item" align="left"><strong>Raz&oacute;n Social</strong> </td>
      <td align="left"><textarea name="razonsocial" cols="50" rows="3" class="iptxtn" id="textarea" disabled>{$razonsocial}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{else}
    <tr> 
      <td class="item" align="left"> <strong>Nombres</strong></td>
      <td align="left"> <input name="nombres" type="text" class="iptxtn" id="nombres" value="{$nombres}" size="35" maxlength="255" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"> <strong>Apellidos</strong></td>
      <td align="left"> <input name="apellidos" type="text" class="iptxtn" id="apellidos" value="{$apellidos}" size="35" maxlength="255" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="item" align="left"> <strong>RUC</strong></td>
      <td class="item" align="left"> <input name="ruc" type="text" class="iptxtn" id="ruc" value="{$ruc}" size="35" maxlength="255" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item" align="left"><strong>Direcci&oacute;n</strong> </td>
      <td align="left"><textarea name="direccion" cols="50" rows="3" class="iptxtn" id="direccion" disabled>{$direccion}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $idTipoDoc==2}
	<tr> 
      <td class="item" align="left"><strong>Clase de Procedimiento</strong> </td>
      <td align="left"><select name="tipProced" class="ipseln" onChange="submitForm('{$accion.FRM_MODIFICA_DOCUMENTO}')">{$selTipoProcedimiento}</select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item" align="left"><strong>Procedimiento</strong></td>
      <td align="left"><select name="procedimiento" class="ipseln" onChange="submitForm('{$accion.FRM_MODIFICA_DOCUMENTO}')">
																{$selProcedimiento}
																  </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="item" align="left"> <strong>Documento Externo y/o N&deg; Ticket</strong></td>
      <td align="left"> <input name="indicativo" type="text" class="iptxtn" id="indicativo" value="{$indicativo}" size="75" maxlength="80"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	
	<tr> 
      <td class="item" align="left"><strong>Tipo de Documento</strong></td>
      <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc">		    
					  {$selClaseDoc}						 
	      </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $idTipoDoc!=2}
	<tr> 
      <td class="item" align="left" valign="middle"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="50" rows="3" class="iptxtn" id="asunto" >{$asunto}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <!--   31/05/2013   Se agrega Tipo de Asunto  -->
    <tr>
              <td class="item" align="left"><strong>Tipo Asunto</strong> </td>
              <td align="left"><select name="idtipoAsunto" class="ipseln" id="idtipoAsunto">
                  {$seltipoAsunto}
              </select>
              </td>
	</tr>
	{/if}
    {if $idTipoDoc==1}
    <tr>
    	<td class="item" align="left"><strong>Unidad</strong></td>
    	<td colspan="2">   <select name="dependencia" class="ipseln" id="dependencia">{$selDependencia}</select> <button id="std_btn_agrega_dep" class="std_button">Agregar</button>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="left" class="item">
    	  <table id="lista_dependencias">
    	    <thead>
    	      <tr><th>UNIDAD</th><th>FOLIOS</th><th>ACCIONES</th><th>OBSERVACION</th><th>&nbsp;</th></tr>
    	      </thead>
    	    {section name=customer loop=$DependenciasAct}
    	    <tr valign="middle">
            
           <td><input type="hidden" value="{$DependenciasAct[customer].id}" name="dependencias_multiples[]"/>{$DependenciasAct[customer].dependencia}</td>
           <td><input type="text" name="foliosv[]" values="1" size="2" maxlength="5" class="iptxtn" value="{$DependenciasAct[customer].folios}"/></td>
           <td><input type="hidden" name="arr_derlista[acciones][]" value="{$DependenciasAct[customer].idacciones}"/><input type="text" class="text_deracciones iptxtn" value="{$DependenciasAct[customer].acciones}"/></td>
           <td><input type="text" name="obsv[]" class="iptxtn" value="{$DependenciasAct[customer].obs}" /></td>
           <td>{if $DependenciasAct[customer].aceptado==''}<a href="#" class="btn_eliminardep"> X </a>{/if}</td>
    	      </tr>
    	    {/section}
  	    </table>  	  </td>
    	</tr>
    {else}
	<tr> 
      <td class="item" align="left"><strong>Dependencia</strong> </td>
      <td align="left"><select name="dependencia" class="ipseln" id="dependencia">
					  {$selDependencia}
						 </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item" align="left"><strong>Observaciones</strong> </td>
      <td align="left"><textarea name="observaciones" cols="80" rows="6" class="iptxtn" id="observaciones" >{$observaciones}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"> <strong>Folios</strong></td>
      <td align="left"> <input name="folios" type="text" class="iptxtn" id="nombres" value="{$folios}" size="7" maxlength="4" onKeyPress="solo_num();"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    {/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="std_button" value="Modificar" id="btn_modificar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCUMENTO}&menu={$accion.SUMARIO}&subMenu={$accion.FRM_BUSCA_DOCUMENTO}');return document.MM_returnValue"> 
        &nbsp;&nbsp; <input name="imphr" type="button" class="std_button" value="Imprimir Hoja de Tr&aacute;mite" onClick="window.open('/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id={$id}&autoprint=1','HOJA DE TRAMITE','height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0');return false;">
        <input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_DOCUMENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		<input name="nroOTD" type="hidden" id="nroOTD" value="{$nroOTD}">
		<input name="idTipoDoc" type="hidden" id="idTipoDoc" value="{$idTipoDoc}">
		<input name="tipoPersona" type="hidden" id="tipoPersona" value="{$tipoPersona}">
		{if $idPersona}<input name="idPersona" type="hidden" id="idPersona" value="{$idPersona}">{/if}
		{if $id}<input name="id" type="hidden" id="id" value="{$id}">{/if}
        {php}
        	if($_GET['autohr']==1){
            	echo "<script>window.open('/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=";
		{/php}{$id}{php}echo "&autoprint=1&cantidad=".$_GET['cantidad']."','HOJA DE TRAMITE','height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0');</script>";}
        {/php}
		 </td>
    </tr>
  </table>
</form>
</div>
</div>
<div id="modal_deracciones" class="std_oculto">
<div class="std_form">
		<h1>ACCIONES</h1>
        <table>
    	{$acciones}
        </table><br />
<div><span class="std_button" id="btn_aceptarAcciones">Aceptar</span></div>
    </div>
</div>