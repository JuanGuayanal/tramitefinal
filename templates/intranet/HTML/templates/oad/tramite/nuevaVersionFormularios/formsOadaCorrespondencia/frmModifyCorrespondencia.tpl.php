{$jscript}
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide
{/literal}
-->
</script>
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ninguna_letra(){
	event.returnValue = false;
}
function cargar(pForm,a,b)
{
		//alert(a);
		pForm.action="index.php?accion=frmModifyCorrespondencia&menu=frmSearchNoti&subMenu=frmModifyCorrespondencia&idUpdateNot="+a+b;
	    pForm.submit();
}
function cargar2(pForm,a)
{
		//alert(a);
		pForm.action="index.php?accion=frmModifyCorrespondencia&menu=frmSearchNoti&subMenu=frmModifyCorrespondencia&codDepa="+a;
	    pForm.submit();
}
function SubmitForm2(pSel,pForm,pAccion,ee){
	if(pSel.options[pSel.selectedIndex].value!='none'){
		pForm.accion.value=pAccion
		pForm.action="index.php?"+pAccion+"&"+ee+"#user"
		pForm.submit()
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('folio','Folio','R','DomProc','Domicilio Procesal','R','flag','Estado de la Correspondencia','Sel'{/literal}{if $flag==1},'bbbb','Recibido por','R','aaaa','Identificado con','R'{/if}{if $flag==2},'Observaciones','Observaciones','R'{/if}{if $flag==3},'estadoNot','Motivo','Sel'{if $estadoNot==3},'Observaciones','Observaciones','R'{/if}{/if});
		{literal}
		return document.MM_returnValue;
	}else
		return false;
}
{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} <br>
    <tr> <br>
      <td colspan="4" align="center" class="item"> <b>Tipo de Correspondencia</b> 
        {$aa} </td>
	</tr>
    <tr> 
      <td class="item"><strong>&nbsp;</strong></td>
      <td colspan="2" class="item">&nbsp;&nbsp;&nbsp; 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Fecha</strong></td>
      <td colspan="2" class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
	
    <tr> 
      <td class="item" align="left"> <strong>{if $TipoCor==1}Acto que se notifica{elseif $TipoCor==2}N&uacute;mero de Documento{/if}</strong></td>
      <td class="item" align="left">{$numero}</td>
      <td><!--<img src="/img/800x600/ico-info3.gif" alt="Indica el acto que se notifica(resoluci�n, oficio, etc.) y la fecha de expedici�n." width="20" height="20" align="top">--></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	
	{if $TipoCor==1}
    <tr> 
      <td height="25" class="item" align="left"><strong>Nombre y Cargo</strong></td>
      <td class="item" align="left">{$campo3} 
      </td>
      <td><!--<img src="/img/800x600/ico-info3.gif" alt="Indica el Nombre y Cargo del funcionario que expedi� el acto que se notifica." width="20" height="20" align="top">--></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" align="left"><strong>Procedimiento</strong></td>
      <td class="item" align="left">{$campo4}
      </td>
      <td><!--<img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top">--></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if $TipoCor>0}
    <tr> 
      <td class="item" align="left"><strong><a name="user" id="user"></a>Destinatario</strong> </td>
      <td colspan="2" class="item" align="left">{$destinatario}</td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Domicilio Procesal</strong> </td>
      <td colspan="2" class="item" align="left"> 
        <table width="75%" border="0">
          <tr> 
            <td class="item" align="left"><!--{$DomProc}--><textarea name="DomProc" cols="60" rows="3" class="iptxtn" id="DomProc" >{$DomProc}</textarea></td>
          </tr>
        </table>
        </td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $TipoCor==1}
    <tr> 
      <td class="item" align="left"> <strong>Vigencia del Acto Notificado</strong></td>
      <td colspan="3" align="left"> <select name="vigencia" class="ipsel1" id="campo1" disabled>
          <option value="none"{if $vigencia=="none"} selected{/if}>Seleccione una 
          opci�n</option>
          <option value="1"{if $vigencia==1} selected{/if}>Desde la fecha de su 
          emisi�n</option>
          <option value="2"{if $vigencia==2} selected{/if}>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"{if $vigencia==3} selected{/if}>Desde el d�a de su notificaci�n</option>
          <option value="4"{if $vigencia==4} selected{/if}>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select> </td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Agota la v&iacute;a administrativa</strong></td>
      <td colspan="3" class="item" align="left">{if $radio==1}Si agota la v&iacute;a administrativa{else}
		                                                No agota la v&iacute;a administrativa{/if}</td>
    </tr>
    <tr> 
      <td rowspan="3" class="item" align="left"><strong>Recurso Administrativo:</strong></td>
      <td colspan="3" class="item" align="left">{if $recurso1==1}Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.{/if}</td>
    </tr>
    <tr> 
      <td colspan="3" class="item" align="left">{if $recurso2==1}
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.{/if}</td>
    </tr>
    <tr> 
      <td colspan="3" class="item" align="left">{if $recurso3==1}
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.{/if}</td>
    </tr>
	{/if}
    <tr> 
      <td class="item" align="left"><strong>N&uacute;mero de folios</strong></td>
      <td colspan="3" class="item" align="left"><!--{$folio}-->&nbsp;<input name="folio" type="text" class="iptxtn" onKeyPress="solo_num();" value="{if $folio}{$folio}{else}1{/if}" size="4" maxlength="10"></td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Fecha de Entrega al Courier </strong></td>
      <td colspan="3" class="item" align="left">{$fecEntCourier}</td>
    </tr>
    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
	{/if}
	{if ($idUpdateNot>0)}
    <tr> 
      <td class="item" align="left"> <strong><a name="proc" id="proc"></a>Estado de la Correspondencia</strong></td>
      <td colspan="3" align="left"> <select name="flag" class="ipseln" id="flag" onChange="submitForm('{$accion.FRM_MODIFICA_CORRESPONDENCIA}');cargar(document.{$frmName},{$idUpdateNot},'#proc');">
		{$selFlag}
		<!--<option value="none">Seleccione una opci�n</option>
		<option value="1"{if $flag=="1"} selected{/if}>Notificada</option>
		<option value="2"{if $flag=="2"} selected{/if}>Rechazada</option>
		<option value="3"{if $flag=="3"} selected{/if}>No practicada</option>-->
        </select> </td>
    </tr>
		{if ($flag==1||$flag==2||$flag==3||$flag==4)}
		<tr> 
		  
      <td class="item" align="left"> <strong>Fec. Notificaci&oacute;n</strong></td>
		  <td colspan="3" align="left"><select name="dia_ing" class="ipseln" id="dia_ing">
		  {$selDiaIng}
			</select> <select name="mes_ing" class="ipseln" id="mes_ing">
		  {$selMesIng}
			</select> <select name="anyo_ing" class="ipseln" id="anyo_ing">
		  {$selAnyoIng}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<!--
		<tr> 
      <td class="item" > <strong>Departamento</strong></td>
		  <td colspan="3"> <select name="codDepa" class="ipsel2" id="select3" onChange="document.{$frmName}.codProv.options[0].selected=true;SubmitForm2(this,document.{$frmName},'{$accion.FRM_MODIFICA_CORRESPONDENCIA}',{$idUpdateNot});">
		{$selDepa}
              </select> </td>
		</tr>
		<tr> 
      <td class="item" > <strong>Provincia</strong></td>
		  <td colspan="3"> <select name="codProv" class="ipsel2" id="select2" onChange="SubmitForm2(this,document.{$frmName},'{$accion.FRM_MODIFICA_CORRESPONDENCIA}',{$idUpdateNot})">
		{$selProv}
              </select> </td>
		</tr>
		<tr> 
      <td class="item" > <strong>Distrito</strong></td>
		  <td colspan="3"> <select name="codDist" class="ipsel2" id="select" >
		{$selDist}
              </select></td>
		</tr>
		-->
		{/if}
		<!--
		<tr> 
		  <td class="item"> <strong>Hora. Ingreso</strong></td>
		  <td colspan="3"><select name="hora_ing" class="ipsel2" id="hora_ing">
		  {$selHorIng}
			</select> <select name="min_ing" class="ipsel2" id="min_ing">
		  {$selMinIng}
			</select>
			<img src="/img/800x600/ico-info3.gif" alt="Corresponde a la hora en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		-->
		{if $flag==1}
		<tr>
			<td class="item" align="left"><strong>Recibido por:</strong></td>
		  <td colspan="3" class="item" align="left"><input name="bbbb" type="text" class="iptxtn" value="{$bbbb}" maxlength="50">
        <img src="/img/800x600/ico-info3.gif" alt="Nombre del que recibe la notificaci�n" width="20" height="20" align="top"></td>
		</tr>
		<tr>
			<td class="item" align="left"><strong>Identificado con DNI:</strong></td>
		  <td colspan="3" class="item" align="left"><input name="aaaa" type="text" class="iptxtn" value="{$aaaa}" maxlength="30">
        <img src="/img/800x600/ico-info3.gif" alt="DNI del que recibe la notificaci�n" width="20" height="20" align="top"></td>
		</tr>
		{/if}
		{if ($flag==3||$flag==4)}
		<tr> 
		  
      <td class="item" align="left"> <strong><a name="aa" id="aa"></a>Motivo</strong></td>
		  <td colspan="3" align="left"> <select name="estadoNot" class="ipseln" id="estadoNot" onChange="document.{$frmName}.estadoNot.selected=true;submitForm('{$accion.FRM_MODIFICA_CORRESPONDENCIA}');cargar(document.{$frmName},{$idUpdateNot},'#aa');">
			{$selEstado}
			</select> </td>
		</tr>
		{/if}
		{if ($flag==2||($flag==3&& $estadoNot==17))}
				<tr> 
				  <td class="item" align="left"><strong>Observaciones</strong> 
        		 </td>
				  <td colspan="3" align="left"><textarea name="Observaciones" cols="60" rows="3" class="iptxtn" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
		{/if}
		{if ($flag==4&& $estadoNot==12)}
				<tr> 
				  <td class="item" align="left"><strong>Fecha Primera Visita</strong> 
        		 </td>
				  <td colspan="3" align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
						&nbsp;&nbsp;
						<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
						<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
				</tr>
				<tr> 
				  <td class="item" align="left"><strong>Fecha Segunda Visita</strong> 
        		 </td>
				  <td colspan="3" align="left"><input name="desFechaFin" type="text" class="iptxtn" id="desFechaFin" value="{$desFechaFin}" tabindex="5" onKeyPress="ninguna_letra();" />
    					&nbsp;&nbsp;
						<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
	 					<div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
				</tr>
		{/if}
		{if ($flag==5)}
				<tr> 
				  <td class="item" align="left"><strong>Fecha de denuncia</strong> 
        		 </td>
				  <td colspan="3" align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="{$desFechaIni}" tabindex="4" onKeyPress="ninguna_letra();" />
						&nbsp;&nbsp;
						<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
						<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
				</tr>
				<tr> 
				  <td class="item" align="left"><strong>Fecha de comunicaci&oacute;n</strong> 
        		 </td>
				  <td colspan="3" align="left"><input name="desFechaFin" type="text" class="iptxtn" id="desFechaFin" value="{$desFechaFin}" tabindex="5" onKeyPress="ninguna_letra();" />
    					&nbsp;&nbsp;
						<a href="javascript:;"  onclick="fPopCalendar('Fin',document.{$frmName}.desFechaFin,document.{$frmName}.desFechaFin,popCalFin,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
	 					<div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onClick="event.cancelBubble=true"></div></td>
				</tr>
				<tr> 
				  <td class="item" align="left"><strong>Observaciones</strong> 
        		 </td>
				  <td colspan="3" align="left"><textarea name="Observaciones" cols="60" rows="3" class="iptxtn" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
		{/if}
	{/if}
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> 
	  		
		<input name="bbSubmit" type="Submit" class="submitV2" value="Modifica Correspondencia"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_NOTIFICACION}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_BUSCA_NOTIFICACION}');return document.MM_returnValue"> 
        
		<input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_CORRESPONDENCIA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{if ($idExp>0)}<input name="idExp" type="hidden" value="{$idExp}">{/if}
		{if ($idInt>0)}<input name="idInt" type="hidden" value="{$idInt}">{/if}
		{if ($idUpdateNot>0)}<input name="idUpdateNot" type="hidden" value="{$idUpdateNot}">{/if}
		
		</td>
    </tr>
  </table>
</form>