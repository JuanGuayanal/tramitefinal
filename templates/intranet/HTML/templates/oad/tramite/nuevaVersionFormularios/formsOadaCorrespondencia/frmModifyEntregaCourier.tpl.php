{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} <br>
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td colspan="2" class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
	<tr> 
	  <td class="item"> <strong>Fec. Entrega al Courier</strong></td>
		  <td colspan="3" align="left"><select name="dia_ing" class="ipseln" id="dia_ing">
		  {$selDiaIng}
			</select> <select name="mes_ing" class="ipseln" id="mes_ing">
		  {$selMesIng}
			</select> <select name="anyo_ing" class="ipseln" id="anyo_ing">
		  {$selAnyoIng}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que se entrega al Courier." width="20" height="20" align="top"> 
		  </td>
	</tr>
			<tr>
				<td class="item"><strong>Tipo de Mensajer&iacute;a</strong></td>
				<td colspan="3" class="item" align="left"><input name="radio2" type="radio" value="1" checked>&nbsp;<strong>Mensajer&iacute;a Local</strong>&nbsp;&nbsp;
				<input type="radio" name="radio2" value="0">&nbsp;<strong>Mensajer&iacute;a Nacional </strong></td>
			</tr>
    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> 
		<input name="bbSubmit" type="Submit" class="submitV2" value="Modificar Entrega"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_NOTIFICACION}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_BUSCA_NOTIFICACION}');return document.MM_returnValue"> 
		<input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_ENTREGA_COURIER}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{section name=i loop=$ids}
		<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">
		{/section}
	  </td>
    </tr>
  </table>
</form>