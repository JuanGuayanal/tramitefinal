{$jscript}
<br>
<div style="width:700px;">
<div class="std_form">
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta label">Fecha</td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	 <tr class="std_oculto"> 
      <td colspan="5" class="item-sep" align="left"><strong>Los siguientes datos (*) s�lo se llenar�n para el caso de Expedientes.</strong></td>
    </tr>
	<tr class="std_oculto"> 
      <td class="item"><strong>&iquest;Finalizado definitivamente?</strong></td>
      <td class="item" align="left"><input name="finalizacionDefinitiva" type="radio" value="1">Si
	  		&nbsp;<input name="finalizacionDefinitiva" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item"><strong>Estado del Expediente</strong></td>
      <td class="item" align="left"><input name="estadoExpediente" type="radio" value="1" checked>Favorable/ Firme/ Consentido para el administrado
	  		&nbsp;<input name="estadoExpediente" type="radio" value="2">Desfavorable
			&nbsp;<input name="estadoExpediente" type="radio" value="3">No aplica
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item"><strong>&iquest;Sujeto a Proceso Judicial?</strong></td>
      <td class="item" align="left"><input name="esttadoProcesoJudicial" type="radio" value="1">Si
	  		&nbsp;<input name="esttadoProcesoJudicial" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
	
    <tr> 
      <td class="texto td-encuesta label" valign="middle">Observaciones</td>
      <td align="left"> <textarea name="observaciones" cols="50" rows="4" class="iptxtn" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td colspan="3" class="textored"> <div align="left"><strong>(*): Datos a registrarse s&oacute;lo para el caso de Expedientes.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Si finaliza definitivamente el expediente, no se podr&aacute; reactivar ni se podr&aacute; agregar adjuntos al expediente.
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las observaciones son obligatorias para cualquier tipo de documento.</strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="std_button" value="Archivar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCTRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.FRM_BUSCA_DOCTRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.FINALIZA_DOCTRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idFinTrab" type="hidden" id="idFinTrab" value="{$idFinTrab}">
		{section name=i loop=$ids} 
		
        <!--<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">-->
		<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">
		
		{/section}
		
		{section name=j loop=$ids2} 
		<input name="ids2[]" type="hidden" id="ids2[]" value="{$ids2[j]}">
		{/section}		
		
		{if $tipDocumento}<input name="tipDocumento" type="hidden" id="tipDocumento" value="{$tipDocumento}">{/if}
		{if $tipBusqueda}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $fecIniTrab2}<input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="{$fecIniTrab2}">{/if}
		{if $fecFinTrab2}<input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="{$fecFinTrab2}">{/if}
		{if $asunto2}<input name="asunto2" type="hidden" id="asunto2" value="{$asunto2}">{/if}
		{if $indicativo}<input name="indicativo" type="hidden" id="indicativo" value="{$indicativo}">{/if}
		{if $observaciones2}<input name="observaciones2" type="hidden" id="observaciones2" value="{$observaciones2}">{/if}
		{if $nroTD}<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">{/if}
		{if $page}<input name="page" type="hidden" id="page" value="{$page}">{/if}
		{if ($siglasDep&& $siglasDep!='none')}<input name="siglasDep" type="hidden" id="siglasDep" value="{$siglasDep}">{/if}
		{if ($tipodDoc&& $tipodDoc!='none')}<input name="tipodDoc" type="hidden" id="tipodDoc" value="{$tipodDoc}">{/if}
		{if ($anyo3&& $anyo3!='none')}<input name="anyo3" type="hidden" id="anyo3" value="{$anyo3}">{/if}
		{if $FechaIni}<input name="FechaIni" type="hidden" id="FechaIni" value="{$FechaIni}">{/if}
		{if $FechaFin}<input name="FechaFin" type="hidden" id="FechaFin" value="{$FechaFin}">{/if}		
		<input name="checkTodos" type="hidden" id="checkTodos" value="{$checkTodos}">
		<input name="checkAsunto" type="hidden" id="checkAsunto" value="{$checkAsunto}">
		<input name="checkRazon" type="hidden" id="checkRazon" value="{$checkRazon}">
		<input name="checkTrab" type="hidden" id="checkTrab" value="{$checkTrab}">
		<input name="FechaIni" type="hidden" id="FechaIni" value="{$FechaIni}">
		<input name="FechaFin" type="hidden" id="FechaFin" value="{$FechaFin}">		
		 </td>
    </tr>
  </table>
</form>
</div>
</div>