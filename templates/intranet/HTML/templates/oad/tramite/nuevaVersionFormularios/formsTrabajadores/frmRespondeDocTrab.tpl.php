{$jscript}
<script language="JavaScript">
<!--
{literal}
function cargar(pForm,a)
{
	//var texto33=pForm.Observaciones.value;
	//var texto55=pForm.tipo2.value;	 
		//alert(texto33+" "+texto66);	 
		//alert(texto33);
		pForm.action="index.php?"+a;
	    pForm.submit();
}
{/literal}
-->
</script>
<br>
<div style="width:700px;">
<div class="std_form">
<form action="{$frmUrl}" method="post" name="{$frmName}" enctype="multipart/form-data" onSubmit="MM_validateForm('asunto','Asunto','R');return document.MM_returnValue">
		{section name=i loop=$ids} 
			<input name="ids[]" type="hidden"  value="{$ids[i]}">
		{/section}
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="texto td-encuesta label" align="left">Fecha</td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left">Clase de Documento</td>
      <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc" onChange="submitForm('{$accion.FRM_RESPONDE_DOCTRAB}');">
        {$selClaseDoc}
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if (($justo=='T' ||$a)&& $nro_doc)}
    <tr> 
      <td class="texto td-encuesta" align="left"> <strong>N&deg; Documento/Oficio</strong></td>
      <td align="left"> <input name="nro_doc" type="text" class="iptxtn" id="nro_doc" value="{$nro_doc}" size="35" maxlength="255"> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	{/if}
    <tr class="std_oculto"> 
      <td height="25" class="texto td-encuesta" align="left"><strong>Responder a</strong></td>
      <td align="left"><select name="trabajador" class="ipseln" id="trabajador">
        {$selTrabajador}
        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left" valign="middle">Asunto </td>
      <td align="left"><textarea name="asunto" cols="50" rows="3" class="iptxtn" id="textarea" >{$asunto}</textarea></td>
      <td colspan="3" valign="middle"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left" valign="middle">{if $justo=='T'}Agregar observaciones{else}Observaciones{/if}</td>
      <td class="item" align="left"><textarea name="observaciones" cols="50" rows="3" class="iptxtn" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea></td>
      <td colspan="3"><strong></strong></td>
    </tr>
	{if $email=="jtumay"}
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Archivo</strong></td>
      <td class="item" align="left"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
      <td colspan="3"><strong></strong></td>
    </tr>	
	{/if}
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
	<!-- Tipo de doc=2||=4 => Se formula una resolución cuando se trata de Expediente y algunos casos de Doc Interno -->
	{if !$justo&& !$a&& ($tipDoc==2||$tipDoc==4)&& $dire==2 }
	<tr>
	<td colspan="3" class="item"><strong><a name="der" id="der"></a> 
				<input name="ProyResol" type="checkbox" id="ProyResol" value="1" onClick="submitForm('{$accion.FRM_RESPONDE_DOCTRAB}');" {if $ProyResol} checked{/if}>
        Formular Resoluci&oacute;n Ahora</strong></td>
			</tr>    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
			{if $ProyResol} 
				<tr> 
      				<td class="item"> <strong>Tipo de Resoluci&oacute;n:</strong></td>
      				<td colspan="2"><select name="TipoResol" class="ipsel1" id="TipoResol">
        			{$selTipoResol}
        			</select></td>
				</tr>
				<tr> 
      				<td class="item"> <strong>Sumilla:</strong></td>
      				<td colspan="2"><textarea name="sumilla" cols="54" class="iptxt1" id="textarea2" >{$sumilla}</textarea> 
      </td>
				</tr>
		<tr> 
      	  <td class="item"> <strong>Fec. Inicio</strong></td>
		  <td colspan="3"><select name="dia_ini" class="ipsel2" id="dia_ini">
		  {$selDiaIni}
			</select> <select name="mes_ini" class="ipsel2" id="mes_ini">
		  {$selMesIni}
			</select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
		  {$selAnyoIni}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Inicio." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Fin</strong></td>
		  <td colspan="3"><select name="dia_fin" class="ipsel2" id="dia_fin">
		  {$selDiaFin}
			</select> <select name="mes_fin" class="ipsel2" id="mes_fin">
		  {$selMesFin}
			</select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
		  {$selAnyoFin}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Fin." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Publicaci&oacute;n</strong></td>
		  <td colspan="3"><select name="dia_pub" class="ipsel2" id="dia_pub">
		  {$selDiaPub}
			</select> <select name="mes_pub" class="ipsel2" id="mes_pub">
		  {$selMesPub}
			</select> <select name="anyo_pub" class="ipsel2" id="anyo_pub">
		  {$selAnyoPub}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicación." width="20" height="20" align="top"> 
		  </td>
		</tr>
				<tr> 
      				<td colspan="3"><hr width="100%" size="1"></td>
    			</tr>
				{/if} 
		{/if}
    <tr align="center"> 
      <td colspan="3">{if ($idClaseDoc>0)}<input name="bSubmit" type="Submit" class="std_button" value="Responder" >{/if}
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_DOCTRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.FRM_BUSCA_DOCTRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.RESPONDE_DOCTRAB}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="id" type="hidden" id="id" value="{$id}"> 
		<input name="tipDoc" type="hidden" id="tipDoc" value="{$tipDoc}">
		<input name="tipBus" type="hidden" id="tipBus" value="{$tipBus}">
		<input name="a" type="hidden" id="a" value="{$a}">
		{if $justo=='T'}<input name="justo" type="hidden" id="justo" value="{$justo}">{/if}
		{if $tipDocumento}<input name="tipDocumento" type="hidden" id="tipDocumento" value="{$tipDocumento}">{/if}
		{if $tipBusqueda}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $fecIniTrab2}<input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="{$fecIniTrab2}">{/if}
		{if $fecFinTrab2}<input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="{$fecFinTrab2}">{/if}
		{if $asunto2}<input name="asunto2" type="hidden" id="asunto2" value="{$asunto2}">{/if}
		{if $indicativo}<input name="indicativo" type="hidden" id="indicativo" value="{$indicativo}">{/if}
		{if $observaciones2}<input name="observaciones2" type="hidden" id="observaciones2" value="{$observaciones2}">{/if}
		{if $nroTD}<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">{/if}
		{if $page}<input name="page" type="hidden" id="page" value="{$page}">{/if}
		{if ($siglasDep&& $siglasDep!='none')}<input name="siglasDep" type="hidden" id="siglasDep" value="{$siglasDep}">{/if}
		{if ($tipodDoc&& $tipodDoc!='none')}<input name="tipodDoc" type="hidden" id="tipodDoc" value="{$tipodDoc}">{/if}
		{if ($anyo3&& $anyo3!='none')}<input name="anyo3" type="hidden" id="anyo3" value="{$anyo3}">{/if}
		{if $codigoInterno}<input name="codigoInterno" type="hidden" id="codigoInterno" value="{$codigoInterno}">{/if}
		<input name="checkTodos" type="hidden" id="checkTodos" value="{$checkTodos}">
		<input name="checkAsunto" type="hidden" id="checkAsunto" value="{$checkAsunto}">
		<input name="checkRazon" type="hidden" id="checkRazon" value="{$checkRazon}">
		<input name="checkTrab" type="hidden" id="checkTrab" value="{$checkTrab}">
		<input name="FechaIni" type="hidden" id="FechaIni" value="{$FechaIni}">
		<input name="FechaFin" type="hidden" id="FechaFin" value="{$FechaFin}">		
		 </td>
    </tr>
  </table>
</form>
</div>
</div>