<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Autor</Author>
  <LastAuthor>Autor</LastAuthor>
  <Created>2006-12-04T20:10:16Z</Created>
  <Company>Empresa</Company>
  <Version>11.5606</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>6105</WindowHeight>
  <WindowWidth>10425</WindowWidth>
  <WindowTopX>600</WindowTopX>
  <WindowTopY>90</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
<Style ss:ID="cabecera">
	   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
	   <Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#000000"/>
		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#000000"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#000000"/>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#000000"/>
	   </Borders>
	   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#000000"   ss:Bold="1"/>
	   <Interior ss:Color="#FEF7C1" ss:Pattern="Solid"/>
	  </Style>
<Style ss:ID="detallenumero">
	   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom" ss:WrapText="1"/>
	   <Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
	   </Borders>
	   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="7" ss:Color="#666699" ss:Bold="1"/>
	   <Interior ss:Color="#F5F5F5" ss:Pattern="Solid"/>
	   <NumberFormat ss:Format="0.0"/>   
	   <Protection x:HideFormula="1"/>         
	 </Style>	  
<Style ss:ID="detalledecripcion">
	   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="1"/>
	   <Borders>
		<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
		<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2" ss:Color="#000000"/>
	   </Borders>
	   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="7" ss:Color="#666699" ss:Bold="1"/>
	   <Interior ss:Color="#F5F5F5" ss:Pattern="Solid"/>
	 </Style>  
 </Styles>
 <Worksheet ss:Name="Hoja1">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="80">
   <Column ss:AutoFitWidth="0" ss:Width="24"/>
   <Column ss:AutoFitWidth="0" ss:Width="170.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>
   <Row>
    <Cell ss:StyleID="cabecera" ss:MergeAcross="9"><Data ss:Type="String">LISTADO DE DOCUMENTO QUE SE ENCUENTRAN EN OL PROVENIENTES DEL DVI</Data></Cell>
   </Row>
	<Row ss:AutoFitHeight="0" ss:Height="30">
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">ID</Data></Cell>
			
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">ASUNTO</Data></Cell>
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">DOCUMENTO QUE ORIGINO EL TRAMITE</Data></Cell>
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">REFERENCIA</Data></Cell>
			
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">RAZON SOCIAL</Data></Cell>
			
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">FECHA DE CREACION</Data></Cell>
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">FECHA DE DERIVACION</Data></Cell>
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">TRABAJADOR</Data></Cell>
			
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">DEPENDENCIA</Data></Cell>
			
			<Cell ss:StyleID="cabecera" ><Data ss:Type="String">ESTADO</Data></Cell>
			
			
	</Row>
	{section name=i loop=$list}
	<Row ss:AutoFitHeight="0" ss:Height="20">
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$smarty.section.i.iteration}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].asunto}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].ref1}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].ref2}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].razonSocial}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].fec1}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].fec2}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].trab}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].depe}</Data></Cell>
			
			<Cell ss:StyleID="detalledecripcion" ><Data ss:Type="String">{$list[i].estado}</Data></Cell>			
	</Row>
	{/section}	 
   
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0"/>
    <Footer x:Margin="0"/>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Selected/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>   