{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('TipoResol','Tipo de Resoluci�n','Sel','sumilla','Sumilla','R',{/literal});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong> </td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA RESOLUCI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Tipo de Resoluci&oacute;n</strong> </td>
      <td align="left"><select name="TipoResol" class="ipseln" id="TipoResol">
        {$selTipoResol}
        </select>&nbsp;<input name="nroResol" type="text" class="iptxtn" id="nroResol" value="{$nroResol}" size="30" maxlength="255" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Asunto </strong></td>
      <td align="left"><textarea name="sumilla" cols="70" rows="4" class="iptxtn" id="textarea" >{$sumilla}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr>     	  
      <td class="texto td-encuesta"> <strong>Fecha de Fe de Erratas </strong></td>
		  <td align="left"><select name="dia_pub" class="ipseln" id="dia_pub">
		  {$selDiaPub}
			</select> <select name="mes_pub" class="ipseln" id="mes_pub">
		  {$selMesPub}
			</select> <select name="anyo_pub" class="ipseln" id="anyo_pub">
		  {$selAnyoPub}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicaci�n." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
	</tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Guardar Fe Erratas"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_RESOLUCION}&menu={$accion.FRM_BUSCA_RESOLUCION}&subMenu={$accion.FRM_BUSCA_RESOLUCION}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_FE_ERRATAS}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
        {if ($idResolucion>0)} 
        <input name="idResolucion" type="hidden" id="idResolucion" value="{$idResolucion}">
        {/if} 
		</td>
    </tr>
  </table>
</form>