<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<br>
<table width="100%" height="40" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2"><img src="/img/800x600/line.single.solid1px.jpg" width="100%" height="1"></td>
  </tr>
  <tr>
    <td width="100">&nbsp;</td>
    <td>
    <font face="Arial" color="#000000" size="2"><b>N&Uacute;MERO DE EXPEDIENTES EN PROCESO A LA FECHA
		 DE ACUERDO AL TUPA {$descTipoAprobacion} POR DEPENDENCIA<br>
		 {if $descripcion}DEPENDENCIA: {$descripcion}{else}TODAS LAS DEPENDENCIAS{/if}</b></font><br></td>
  </tr>
  <tr>
    <td colspan="2"><img src="/img/800x600/line.doble.solid1px.jpg" width="100%" height="3"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="20%"><b><font size="2" face="Arial">N&deg;</font></b></td>
    <td width="50%"><b><font size="2" face="Arial">Procedimiento</font></b></td>
    <td width="30%"><b><font size="2" face="Arial">Tiempo Tupa</font></b></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/img/800x600/line.single.solid1px.jpg" width="100%" height="1"></td>
  </tr>
</table>
{section name=u loop=$ate.users}
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="20%"><font size="2" face="Arial">{$smarty.section.u.iteration}</font></td>
    <td width="50%"><font size="2" face="Arial"><!--{$ate.users[u].name|capitalize}-->{$ate.users[u].proc|capitalize}</font></td>
    <td width="30%">{$ate.users[u].nroDias|capitalize}</td>
  </tr>
</table>
<blockquote>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td><blockquote>
        <p><b><font size="2" face="Arial"><br>
          Dependencias involucradas: </font></b></p>
    </blockquote></td>
  </tr>
  <tr>
    <td>
      <table width="1270" border="0" cellpadding="4" cellspacing="0">
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
        </tr>
        <tr align="center" bgcolor="#EAEAEA">
          <td width="2%"><font color="#000000" size="1" face="Arial"><b>N&deg;</b></font></td>
          <td width="40%"><font color="#000000" size="1" face="Arial"><b>Dependencia</b></font></td>
          <td width="30%"><font color="#000000" size="1" face="Arial"><b>En curso</b></font></td>
          <td width="23%"><font color="#000000" size="1" face="Arial"><b>Vencidos</b></font></td>
          <td width="5%"><font color="#000000" size="1" face="Arial"><b>No vencidos</b></font></td>
        </tr>
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
        </tr>
		{assign var="totalHoras" value=0.00}
		{assign var="totalHoras2" value=0.00}
		{assign var="totalProceso" value=0.00}
  		{section name=a loop=$ate.users[u].ates}
        <tr>
          <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$smarty.section.a.iteration}</font></td>
          <td bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].depe}</font></td>
          <td bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].cont|string_format:"%.2f"}</font></td>
          <td align="right" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial">{$ate.users[u].ates[a].venc|string_format:"%.2f"}</font></td>
          <td align="right" bgcolor="#FFFFFF"><font color="#000000" size="1" face="Arial"><!--{$ate.users[u].ates[a].novenc|string_format:"%.2f"}-->{$ate.users[u].ates[a].novenc|string_format:"%.2f"}</font></td>
        </tr>
		{math equation="n+m" n=$totalHoras m=$ate.users[u].ates[a].novenc assign='totalHoras'}
		{math equation="p+q" p=$totalHoras2 q=$ate.users[u].ates[a].venc assign='totalHoras2'}
		{math equation="r+s" r=$totalProceso s=$ate.users[u].ates[a].cont assign='totalProceso'}
		{if $smarty.section.a.last}
        <tr align="center">
          <td colspan="5" width="100%"><img src="/img/800x600/line.single.solid1px.gif" width="1270" height="1"></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF" align="right"><font color="#000000" size="1" face="Arial"><strong>Total acumulado</strong></font></td>
          <td bgcolor="#FFFFFF" align="right">&nbsp;</td>
          <td bgcolor="#FFFFFF" align="right"><font color="#000000" size="1" face="Arial"><b>{$totalProceso|string_format:"%.2f"}</b></font></td>
          <td bgcolor="#FFFFFF" align="right"><font color="#000000" size="1" face="Arial"><b>{$totalHoras2|string_format:"%.2f"}</b></font></td>
          <td bgcolor="#FFFFFF" align="right"><font color="#000000" size="1" face="Arial"><b>{$totalHoras|string_format:"%.2f"}</b></font></td>
        </tr>
		{/if}
		{sectionelse}
        <tr>
          <td colspan="5" align="center" bgcolor="#FFFFFF"> <font face="Arial" size="1" color="#000000">No hay atenciones realizadas.</font> </td>
        </tr>
		{/section}
        <tr align="center">
          <td colspan="5"><img src="/img/800x600/line.doble.solid1px.gif" width="1270" height="3"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</blockquote>
 <br>
{/section}
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="1" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>