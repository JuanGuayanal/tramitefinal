<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000" size="6"> N&Uacute;MERO DE EXPEDIENTES EN PROCESO A LA FECHA
		 DE ACUERDO AL TUPA {$descTipoAprobacion} POR DEPENDENCIA </font><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6">{if $desFechaIni!=$desFechaFin} ENTRE EL {$desFechaIni} Y EL {$desFechaFin}{else}DEL {$desFechaFin}{/if}</font></font></b></font></b></font><br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<br>
<font size="6">{if $descripcion}DEPENDENCIA: {$descripcion}{else}TODAS LAS DEPENDENCIAS{/if}</font>
<br>
<br>
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
  <tr bgcolor="#999999"> 
    <td width="5%" align="center"><font size="5"><strong>N&deg;</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
    <td width="6%" align="center"><font size="5"><strong>NRO_TUPA</strong></font></td>
    <td width="39%" align="center"><font size="5"><strong>TUPA</strong></font></td>
	<td width="10%" align="center"><font size="5"><strong>TIEMPO_TUPA</strong></font></td>
	<td width="10%" align="center"><font size="5"><strong>DEPENDENCIA</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>VENCIDOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>NO VENCIDOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>EN CURSO</strong></font></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial">{$smarty.section.i.iteration}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><center>{$list[i].proc}</center></font></td>
	<td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].cant}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><center>{$list[i].dep}</center></font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$list[i].depe}</font></div></td>
	<td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$list[i].venc}</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><center>{$list[i].novenc}</center></font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><center>{$list[i].cont}</center></font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="12" align="center" bgcolor="#FFFFFF"><font size="5"><strong>NO 
      EXISTEN EXPEDIENTES INGRESADOS EN LA(S) FECHA(S) DADA(S) </strong></font></td>
  </tr>
  {/section} 
  <tr> 
    <td colspan="2" align="center" bgcolor="#FFFFFF"><font color="#000000" size="5">TOTALES</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$derivados}</font></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$sumaDer}</font></div></td>
	<td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$finalizados}</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$operacion}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$correspondencia}</font></td>
  </tr>
</table>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>