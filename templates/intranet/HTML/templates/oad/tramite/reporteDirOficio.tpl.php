<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">DETALLES DEL DOCUMENTO INTERNO</font><br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table width="90%" border="1" align="center">
  <tr bgcolor="#999999"> 
    <td> <div align="center"><font color="#000000" size="5"><strong>INDICATIVO-OFICIO</strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>TIPO DE DOCUMENTO</strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>CLASE DE DOCUMENTO</strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>ASUNTO</strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>OBSERVACIONES</strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>DEPENDENCIA 
        ORIGEN </strong></font></div></td>
    <td> <div align="center"><font color="#000000" size="5"><strong>DEPENDENCIA 
        DESTINO</strong></font></div></td>
  </tr>
  <tr> 
    <td><font size="5">{$indicativo}</font></td>
    <td><font size="5">{$tipoDoc}</font></td>
    <td><font size="5">{$claseDoc}</font></td>
    <td><font size="5">{$asunto}</font></td>
    <td><font size="5">{$observaciones}</font></td>
    <td><font size="5">{$depOrigen}</font></td>
    <td><font size="5">{$depDestino}</font></td>
  </tr>
  {section name=i loop=$dir2} 
  <tr> 
    <td><font size="5">{$dir2[i].ind}</font></td>
    <td><font size="5">INTERNO</font></td>
    <td><font size="5">{$dir2[i].cla}</font></td>
    <td><font size="5">{$dir2[i].asu}</font></td>
    <td><font size="5">{$dir2[i].obs}</font></td>
    <td><font size="5">{$dir2[i].depo}</font></td>
    <td><font size="5">{$dir2[i].depd}</font></td>
  </tr>
  {sectionelse} 
  <!--
  <tr> 
    <td colspan="7" align="center" bgcolor="#FFFFFF"><strong><font size="6">El 
      Documento no se ha derivado a&uacute;n a ninguna dependencia</font></strong></td>
  </tr>
  -->
  {/section} 
</table>

<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
