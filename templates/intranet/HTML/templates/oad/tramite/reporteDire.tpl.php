<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><br>
        <font color="#000000" size="6"><u><strong>DETALLES DEL DOCUMENTO EXTERNO O EXPEDIENTE</strong></u><strong><br>
        <!--{$numTramDoc}--></strong></font></p>      
      <!--<hr width="100%" size="1" noshade>--></td>
  </tr>
</table>

  
<table width="95%" border="1" align="center">
  <tr> 
    <td width="21%"><font color="#000000" size="5"><strong>N&deg; DOCUMENTO</strong></font></td>
    <td width="35%" ><font color="#000000" size="7"><strong>{$numTramDoc}</strong></font></td>
    <td width="9%" ><font color="#000000" size="5"><strong>TIPO DE PERSONA</strong></font></td>
    <td width="9%" ><font color="#000000" size="5">{$TipoPer}</font></td>
    <td width="21%"><font color="#000000" size="5"><strong>FECHA DE INGRESO</strong></font></td>
    <td width="23%"><font color="#000000" size="5">{$fecRec}</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>ASUNTO</strong></font></td>
    <td colspan="5"><font color="#000000" size="5">{if $idTipoDoc==2}{$proced}{else}{$asunto|default:'No 
      especificado'}{/if}</font><font color="#0066CC" size="5">&nbsp;</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>OBSERVACIONES</strong></font></td>
    <td colspan="5"><font color="#000000" size="5">{$observaciones|default:'No 
      especificado'}</font><font color="#0066CC" size="5">&nbsp;</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#333333" size="6"><strong>DATOS 
        DE LA EMPRESA</strong></font></div></td>
  </tr>
  <tr> 
    <td width="21%"><font color="#000000" size="5"><strong>RAZ&Oacute;N SOCIAL</strong></font></td>
    <td width="35%" colspan="3" ><font color="#000000" size="5">{$persona}</font></td>
    <td width="21%"><font color="#000000" size="5"><strong>RUC</strong></font></td>
    <td width="23%"><font color="#000000" size="5">{$ruc|default:'No especificado'}</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>DIRECCI&Oacute;N LEGAL</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$direccion|default:'No especificado'}</font><font color="#0066CC" size="5">&nbsp;</font><font color="#000000" size="5">&nbsp;</font><font color="#3270aa" size="4">&nbsp;</font><font color="#0066CC" size="5">&nbsp;</font></td>
    <td><font color="#000000" size="5"><strong>TEL&Eacute;FONO</strong></font></td>
    <td><font color="#000000" size="5">{$telefono|default:'No especificado'}</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#000000" size="6"><strong>DATOS 
        GENERALES DEL DOCUMENTO</strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>N&deg; DE DOCUMENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$numTramDoc}</font></td>
    <td><font color="#000000" size="5"><strong>INDICATIVO-OFICIO</strong></font></td>
    <td><font color="#000000" size="5">{$indicativo}</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>TIPO DE DOCUMENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$descripcion}</font></td>
    <td><font color="#000000" size="5"><strong>N&deg; FOLIOS</strong></font></td>
    <td><font color="#000000" size="5">{$folios}</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>PROCEDENCIA</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$proce}</font></td>
    <td><font color="#000000" size="5"><strong>FECHA DE INGRESO</strong></font></td>
    <td><font color="#000000" size="5">{$fecRec}</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#000000" size="6"><strong>SITUACI&Oacute;N 
        DEL DOCUMENTO </strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>DIRECCI&Oacute;N RECEPTORA</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$dep}</font></td>
    <td><font color="#000000" size="5"><strong>N&deg; D&Iacute;AS EN TR&Aacute;MITE</strong></font></td>
    <td><font color="#000000" size="5">{$nroDias}</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>TRATAMIENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5">{$sitAct}</font></td>
    <td><font color="#000000" size="5"><strong>{if $nroDiasTupa}N&deg; D&Iacute;AS 
      CATALOGADO EN TUPA {/if}</strong></font></td>
    <td><font color="#000000" size="5">{if $nroDiasTupa}{$nroDiasTupa}{/if}</font></td>
  </tr>
  {if $Retrazo&& $idTipoDoc==2}
  <tr> 
    <td colspan="4"><font color="#000000" size="5">&nbsp;</font><font color="#3270aa" size="5">&nbsp;</font></td>
    <td><font color="#000000" size="5"><strong>RETRAZO </strong></font></td>
    <td><font color="#000000" size="5">{if $Retrazo}{$Retrazo}{/if}</font></td>
  </tr>
  {/if}
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="95%" border="1" align="center">
  <tr bgcolor="#999999"> 
    <td colspan="7"> <div align="center"><font color="#000000" size="6"><strong>FLUJO 
        DOCUMENTARIO</strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>CLASE DE DOCUMENTO</strong></font></td>
    <td><font color="#000000" size="5"><strong>INDICATIVO</strong></font></td>
    <td><font color="#000000" size="5"><strong>ASUNTO</strong></font></td>
    <td><font color="#000000" size="5"><strong>OBSERVACIONES</strong></font></td>
    <td><font color="#000000" size="5"><strong>DEPENDENCIA ORIGEN</strong></font></td>
    <td><font color="#000000" size="5"><strong>DEPENDENCIA DESTINO</strong></font></td>
    <td><font color="#000000" size="5"><strong>FECHA DE DERIVACI&Oacute;N</strong></font></td>
  </tr>
  {section name=i loop=$dir1} 
  <tr> 
    <td><font color="#0066CC" size="5">&nbsp;</font><font size="5">{$dir1[i].cla}</font></td>
    <td><font size="5">{$dir1[i].ind}</font></td>
    <td><font size="5">{$dir1[i].asu}</font></td>
    <td><font size="5">{$dir1[i].obs}</font></td>
    <td><font size="5">{$dir1[i].depo}</font></td>
    <td><font size="5">{$dir1[i].depd}</font></td>
    <td><font size="5">{$dir1[i].fec}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="7" align="center" bgcolor="#FFFFFF"><strong><font size="5">El 
      Documento no se ha derivado a otra dependencia</font></strong></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>
