<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0">
	<tr>
	<td colspan="2">
		<table width="100%" border="1" cellspacing="0">
		  <tr>
			<td>
				<div align="center"><strong><font size="+3">C&Eacute;DULA DE NOTIFICACI&Oacute;N DE INICIO DE PROCEDIMIENTO ADMINISTRATIVO SANCIONADOR </font>
					<br/>
				<font size="+2">Ley N&deg; 27444, Ley del Procedimiento Administrativo General modificado por Decreto Legislativo N&deg; 1029</font>
			  </strong></div>
			</td>
		  </tr>
		</table>
	</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="30%" border="1" cellspacing="0" align="right">
		  <tr>
			<td>
				<div align="left"><strong><font size="+3">N&deg;{$notificacion}</font></strong></div>
			</td>
		  </tr>
		</table>
	</td>
	</tr>

  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="4">&nbsp;</td>
			  </tr>			  
			  <tr>
				<td colspan="4"><div align="right"><strong><font size="+3">EXPEDIENTE N&deg;: {$acto}</font></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td width="60%">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="17%"><strong><font size="+1">Destinatario</font></strong></td>
				<td colspan="3"><font size="+1">: {$destinatario}</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio</font></strong></td>
				<td colspan="3"><font size="+1">: {$domicilio} , {$distrito} {$provincia} {$departamento}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Entidad</font></strong></td>
				<td colspan="3"><font size="+1">: MINISTERIO DE LA   PRODUCCION</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Norma que atribuye competencia</font></strong></td>
				<td colspan="3"><font size="+1">:Articulo 81&deg; de la Ley General de Pesca  y el literal c) del Art&iacute;culo 26&deg; del Decreto Supremo N&deg; 016-2007-CONVENIO_SITRADOC</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio Entidad</font></strong> </td>
				<td colspan="2"><font size="+1">:Calle Uno Oeste # 60 Piso 5 Urbanizaci&oacute;n Corpac, San Isidro</font></td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Materia</font></strong> </td>
				<td colspan="3"><font size="+1">:Inicio de Procedimiento Administrativo Sancionador</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <!--<tr>
				<td><strong><font size="+1">Fecha de infracci&oacute;n</font></strong> </td>
				<td colspan="2"><font size="+1">:{if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13)}Las indicadas en los Hechos constatados{else}{$fechaInfraccion}{/if}</font></td>
				<td>&nbsp;</td>
			  </tr>-->
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Documentos adjuntos</font></strong></td>
				<td colspan="3"><font size="+1">:{if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13)}Acta de Inspecci�n  {else}{if $codigoProcedencia==4}Reporte SISESAT N� {$infSISESAT}{else}{$infSISESAT}{/if}{/if}</font></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <!--
			  <tr>
			    <td colspan="4"><div align="center"><strong>DOCUMENTO A NOTIFICAR </strong></div></td>
		      </tr>
			  -->
			  <tr>
			    <td></td>
			    <td></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td colspan="2"><strong><font size="3">Presunta infracci&oacute;n:</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <!--
			  <tr>
			    <td><font size="+1">Base Legal:</font></td>
			    <td><font size="+1">{section name=i loop=$infraccion}
						&nbsp;&nbsp;&nbsp;{$infraccion[i].inf}<br>
					{sectionelse}
						&nbsp;&nbsp;&nbsp;
					{/section}</font></td>
			    <td></td>
		      </tr>
			  -->
			  <tr>
			    <td colspan="3">
					<font size="3">Base Legal: &nbsp;&nbsp;
					
			    	{section name=i loop=$infraccion}
						&nbsp;&nbsp;&nbsp;{$infraccion[i].inf}<br>
					{sectionelse}
						&nbsp;&nbsp;&nbsp;
					{/section}
					</font>
				</td>
		      </tr>			  
			  
			  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  
			  <!--
			  <tr>
			    <td colspan="2"><strong><font size="+1">Hechos constatados:</font></strong></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td><font size="+1">Los siguientes son los hechos constatados:<br>{$hechos}</font></td>
			    <td></td>
		      </tr>
			  -->
			  <tr>
			    <td colspan="3">
					<font size="3">Hechos constatados: &nbsp;&nbsp;
					
					Los siguientes son los hechos constatados:<br>{$hechos}
					</font>
				</td>
		      </tr>			  
			  
				 <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>			  			  
			  <!--
			  <tr>
			    <td colspan="2"><strong><font size="+1">Sanci&oacute;n Administrativa: </font></strong></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td><font size="+1">{$presuntaSancion}</font></td>
			    <td></td>
		      </tr>
			  -->
			  
			  <tr>
			    <td colspan="3">
					<font size="3">Sanci&oacute;n Administrativa:
					
					{$presuntaSancion}
					</font>
				</td>
		      </tr>			  
			  			  

				<tr> <td colspan="3"></td> </tr>
				
			  <tr>
			    <td colspan="2"></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td colspan="2"><div align="right"><strong><font size="+1">__________________________________________</font></strong></div></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td colspan="2"><div align="right"><strong><font size="+1">Director de Seguimiento, Vigilancia y Sanciones </font></strong></div></td>
			    <td></td>
		      </tr>
			  <tr>
			    <!-- <td colspan="2"><div align="right"><font size="+1">Ing. ALEJANDRO RAM&Iacute;REZ SALDA&Ntilde;A &nbsp;&nbsp;&nbsp;&nbsp;</font></div></td> -->
				<td colspan="2"><div align="right"><font size="+1">FELIX FRANCISCO CHUMBIRAY MENDOZA &nbsp;&nbsp;&nbsp;&nbsp;</font></div></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td colspan="2"></td>
			    <td></td>
		      </tr>
			  <tr>
			    <td colspan="3"><font size="+1">Plazo: Cinco(05) dias h&aacute;biles, a partir del dia siguiente de recibida la presente notificaci&oacute;n</font></td>
		      </tr>
			  
				<tr> <td colspan="3"></td> </tr>			  
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="3"><div align="center"><font size="+1"><strong>CARGO DE ENTREGA</strong></font></div></td>
			  </tr>
			  <!--<tr>
				<td colspan="3">&nbsp;&nbsp;</td>
			  </tr>-->
			  <tr>
				<td colspan="3">&nbsp;&nbsp;</td>
			  </tr>
			  <tr>
			    <td><font size="+1"><strong>RECIBIDO POR</strong>________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Documento de Identidad: __________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE LA DEVOLUCI&Oacute;N</strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Relaci&oacute;n con el destinatario _________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Domicilio errado o inexistente ( ) </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Fecha __________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Hora ___________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE ENTREGA CON ACTA </strong></font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1"><strong>FIRMA DEL QUE RECIBE</strong>______________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Se neg&oacute; a recibir ( ) o firmar ( ) </font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">y sello (de ser empresa) </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Ausencia primera Notificaci&oacute;n ( ) </font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Ausencia segunda Notificaci&oacute;n ( ) </font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1"><strong>CARACTERISTICAS DEL DOMICILIO</strong></font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>DATOS DEL NOTIFICADOR </strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Nro. de medidor agua ______o luz ______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Nombres y apellidos:</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Material y color de la fachada _______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">__________________________________________</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Material y color de la puerta _______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">DNI: __________________</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Otros datos: ____________________________ </font> </td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Firma del Notificador: ________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><font size="+1"><strong>Observaciones:</strong></font><font size="+1"> ______________________________________________________________________________________________________________________ </font></td>
		      </tr>
			  <tr>
			    <td colspan="3">_____________________________________________________________________________________________________________________________________________________________</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td></td>
		      </tr>
			  </table>
		</td>
		</tr>		
		
		</table>
	</td>
  </tr>
  <!--
  <tr>
    <td>(*) Nota: En este espacio se consignar&aacute;n los documentos a notificar </td>
  </tr>
  <tr>
  	<td>1) Notificaci&oacute;n de Cargos: Requisitos establecidos en art&iacute;culo 17&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC) . A s&iacute; mismo, se consignar&aacute;n los documentos anexados. </td>
  </tr>
  <tr>
  	<td>2) Notificaci&oacute;n de Resoluciones: Requisitos establecidos en el art&iacute;culo 19&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC). </td>
  </tr>
  <tr>
  	<td>3) Comunicaciones con el administrado: Requisitos establecidos en los numerales 132.4 y 169.1 de los art&iacute;culos 132&deg; y 169&deg;, respectivamente, de la Ley N&deg; 27444, Ley del Procedimiento Administrativo General. </td>
  </tr>
  -->
  <tr>
  	<td align="right">(*)Ver especificaciones al dorso.</td>
  </tr>     
</table>


</body>
</html>
