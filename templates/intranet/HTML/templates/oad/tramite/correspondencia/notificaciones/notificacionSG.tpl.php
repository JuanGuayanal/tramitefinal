<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="4"><div align="center"><strong><font size="+3">C&eacute;dula de Notificaci&oacute;n N&deg;{$notificacion}</font></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="4">&nbsp;</td>
			  </tr>			  
			  <tr>
				<td colspan="4"><div align="right"><strong><font size="+3">EXPEDIENTE N&deg;: {$nroTramiteDoc}</font></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td width="60%">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="28%"><strong><font size="+1">Destinatario</font></strong></td>
				<td colspan="3"><font size="+1">: {$destinatario}</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio</font></strong></td>
				<td colspan="3"><font size="+1">: {$domicilio}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Entidad</font></strong></td>
				<td colspan="3"><font size="+1">: MINISTERIO DE LA PRODUCCION</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Dependencia</font></strong></td>
				<td colspan="3"><font size="+1">: SECRETARÍA GENERAL</font></td>
			  </tr>			  
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Domicilio Entidad</font></strong> </td>
				<td colspan="2"><font size="+1">: Calle Uno Oeste # 60 Urbanizaci&oacute;n Corpac San Isidro</font></td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Materia</font></strong> </td>
				<td colspan="3"><font size="+1">: {$procedimiento|default:'NO DISPONIBLE'}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Documentos adjuntos</font></strong></td>
				<td colspan="3"><font size="+1">: {$acto|default:'NO DISPONIBLE'}</font></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
			    <td colspan="4"><div align="center"><strong>DOCUMENTO A NOTIFICAR </strong></div></td>
		      </tr>
			  -->
			  <tr>
			    <td colspan="3"><strong><font size="+1">Marcar con &quot;X&quot; la opci&oacute;n que corresponda:</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;El acto notificado 
		        entra en vigencia</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha de su emisi&oacute;n {if $vigencia==1}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia anticipada){if $vigencia==2}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n {if $vigencia==3}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n {if $vigencia==4}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</font><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $estado==1}(X){else}( ){/if}SI &nbsp;&nbsp;{if 
        $estado==0}(X){else}( ){/if}NO</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>			  
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;El administrado podr&aacute; interponer Recurso administrativo de</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;{if $recurso1==1}(X){else}( ){/if} ;</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;o Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para que se eleve al superior jer&aacute;rquico {if $recurso2==1}(X){else}( ){/if};</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para que se eleve al superior jer&aacute;rquico{if $recurso3==1}(X){else}( ){/if};</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>			  
			  <tr>
			    <td colspan="3"><font size="+1">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as &uacute;tiles 
        (h&aacute;biles consecutivos) contados desde el d&iacute;a siguiente de 
        su fecha de su Notificaci&oacute;n.<br>
		Se adjunta copia autenticada u original (en su caso) del 
        texto &iacute;ntegro del acto notificado con {$folio|default:'NO DISPONIBLE'} folios.</font></td>
			    <td>&nbsp;</td>
		      </tr>			  			  
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" align="center"><font size="+2">{if $coddep==5}________________________________________{/if}</font></td>
			    <td align="center">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>			  
			  <tr>
			    <td colspan="2" align="center"><font size="+2">{if $coddep==5}KITTY ELISA TRINIDAD GUERRERO{/if}</font></td>
			    <td align="center">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" align="center"><font size="+1">{if $coddep==5}Secretaria General{else}FIRMA Y SELLO{/if}</font></td>
			    <td align="center"><font size="+1">Fecha:</font></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2" align="center"><font size="+1">{if $coddep==5}Ministerio de la Producci&oacute;n{/if}</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>			  		  			  			  			  
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="3"><!--<div align="center"><strong>Cargo de Entrega</strong></div>--></td>
			  </tr>
			  <tr>
			    <td><font size="+1">Recibido por ____________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">DNI/Otro _______________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Relaci&oacute;n con el destinatario _________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE LA DEVOLUCI&Oacute;N</strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Fecha __________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Hora ___________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se neg&oacute; a recibir</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___No firm&oacute;</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">___________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Domicilio cerrado</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">Firma y/o sello</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se mud&oacute;</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Domicilio errado (inexistente)</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se encuentra en el extranjero</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">Notificador ______________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Persona desaparecida</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">N&deg; de DNI del notificador __________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Otros _________________________________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">______________________________________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">_______________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Firma del Notificador</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>CARACTERISTICAS DEL DOMICILIO</strong></font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Nro. de medidor agua/luz ______________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Observaciones ___________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Material y color de la fachada ___________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Material y color de la puerta ____________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>
		</td>
		</tr>		
		
		</table>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <!--
  <tr>
    <td>(*) Nota: En este espacio se consignar&aacute;n los documentos a notificar </td>
  </tr>
  <tr>
  	<td>1) Notificaci&oacute;n de Cargos: Requisitos establecidos en art&iacute;culo 17&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC) . A s&iacute; mismo, se consignar&aacute;n los documentos anexados. </td>
  </tr>
  <tr>
  	<td>2) Notificaci&oacute;n de Resoluciones: Requisitos establecidos en el art&iacute;culo 19&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC). </td>
  </tr>
  <tr>
  	<td>3) Comunicaciones con el administrado: Requisitos establecidos en los numerales 132.4 y 169.1 de los art&iacute;culos 132&deg; y 169&deg;, respectivamente, de la Ley N&deg; 27444, Ley del Procedimiento Administrativo General. </td>
  </tr>
  -->
  <tr>
  	<td>&nbsp;</td>
  </tr>   
  <tr>
  	<td>Ver al dorso regimen de beneficios de pago de multas e indicaciones.</td>
  </tr>     
</table>


</body>
</html>
