<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0">
	<tr>
	<td colspan="2">
		<table width="100%" border="1" cellspacing="0">
		  <tr>
			<td>
				<div align="center"><strong><font size="+3">C&Eacute;DULA DE NOTIFICACI&Oacute;N PERSONAL</font>
					<br/>
				<font size="+2">Ley N&deg; 27444, Ley del Procedimiento Administrativo General modificado por Decreto Legislativo N&deg; 1029</font>
			  </strong></div>
			</td>
		  </tr>
		</table>
	</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="30%" border="1" cellspacing="0" align="right">
		  <tr>
			<td>
				<div align="left"><strong><font size="+3">N&deg;{$notificacion}</font></strong></div>
			</td>
		  </tr>
		</table>
	</td>
	</tr>
	
  <tr>
    <td colspan="2">
		<table width="100%" border="1" cellspacing="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td><div align="left"><strong><font size="+1">EXPEDIENTE N&deg;</font></strong></div></td>
			    <td><div align="left"><strong><font size="+1">: {$nroTramiteDoc}</font></strong></div></td>
			  </tr>
			  <tr>
				<td></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><div align="left"><strong><font size="+1">N&deg; DE FOLIOS</font></strong></div></td>
			    <td><div align="left"><strong><font size="+1">: {$foliosDoc|default:'NO DISPONIBLE'}</font></strong></div></td>
			  </tr>			  
			  <!--<tr>
				<td colspan="2">&nbsp;</td>
				<td width="60%">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			  </tr>-->
			  <tr>
				<td width="20%"><strong><font size="+1">Destinatario</font></strong></td>
				<td colspan="4"><font size="+1">: {$destinatario}</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio</font></strong></td>
				<td colspan="4"><font size="+1">: {$domicilio}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Entidad</font></strong></td>
				<td colspan="4"><font size="+1">: MINISTERIO DE LA PRODUCCION</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Dependencia</font></strong></td>
				<td colspan="4"><font size="+1">: SECRETARÍA GENERAL</font></td>
			  </tr>			  
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Domicilio Entidad</font></strong> </td>
				<td colspan="4"><font size="+1">: Calle Uno Oeste N&deg; 060 Piso  Urbanizaci&oacute;n Corpac San Isidro - Lima. </font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Materia/ Procedimiento</font></strong></td>
				<td colspan="4"><font size="+1">: {$procedimiento|default:'NO DISPONIBLE'}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Documento(s) adjunto(s)</font></strong></td>
				<td colspan="4"><font size="+1">: Copia autenticada  de la {$acto|default:'NO DISPONIBLE'} con ({$folio|default:'NO DISPONIBLE'}) folios. </font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Fecha</font></strong></td>
				<td colspan="4"><font size="+1">: </font></td>
			  </tr>			  
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <!--<tr>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>-->
			  <!--
			  <tr>
			    <td colspan="4"><div align="center"><strong>DOCUMENTO A NOTIFICAR </strong></div></td>
		      </tr>
			  -->
			  <tr>
			    <td colspan="3"><strong><font size="+1">MARCAR CON &quot;X&quot; LA OPCI&Oacute;N QUE CORRESPONDA:</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;El acto notificado 
		        entra en vigencia:</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha de su emisi&oacute;n {if $vigencia==1}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia anticipada){if $vigencia==2}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n {if $vigencia==3}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n {if $vigencia==4}(X){else}( ){/if}</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <!--<tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>-->
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</font><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $estado==1}(X){else}( ){/if}SI &nbsp;&nbsp;{if 
        $estado==0}(X){else}( ){/if}NO</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><strong><font size="+1">RECURSOS QUE PROCEDEN:</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;{if $recurso1==1}(X){else}( ){/if} ;</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para que se eleve al superior jer&aacute;rquico {if $recurso2==1}(X){else}( ){/if};</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para que se eleve al superior jer&aacute;rquico{if $recurso3==1}(X){else}( ){/if};</font></td>
			    <td colspan="2">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3">&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>			  
			  <tr>
			    <td colspan="4"><font size="+1">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as h&aacute;biles consecutivos contados desde el d&iacute;a siguiente de 
        la fecha de su Notificaci&oacute;n.</font></td>
		      </tr>			  			  
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <!--<tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>-->
			  <tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="4" align="center"><strong><font size="+1">__________________________________________</font></strong></td>
		      </tr>
			  <tr>
			    <td colspan="4" align="center"><font size="+1">{if $coddep==5}PEDRO PRADO PRADO{/if}</font></td>
		      </tr>
			  <tr>
			    <td colspan="4" align="center"><font size="+1">{if $coddep==5}Secretario General{else}FIRMA Y SELLO{/if}</font></td>
		      </tr>
			  <!--<tr>
			    <td colspan="3"></td>
			    <td>&nbsp;</td>
		      </tr>-->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <!--<tr>
				<td colspan="3">&nbsp;&nbsp;</td>
			  </tr>-->
			  <tr>
				<td colspan="3"><div align="center"><font size="+1"><strong>CONSTANCIA DE ENTREGA</strong></font></div></td>
			  </tr>
			  <!--<tr>
				<td colspan="3">&nbsp;&nbsp;</td>
			  </tr>-->
			  <tr>
				<td colspan="3">&nbsp;&nbsp;</td>
			  </tr>
			  <tr>
			    <td><font size="+1"><strong>RECIBIDO POR</strong>________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Documento de Identidad: __________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE LA DEVOLUCI&Oacute;N</strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Relaci&oacute;n con el destinatario _________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Domicilio errado o inexistente ( ) </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Fecha __________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Hora ___________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE ENTREGA CON ACTA </strong></font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1"><strong>FIRMA DEL QUE RECIBE</strong>______________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Se neg&oacute; a recibir ( ) o firmar ( ) </font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">y sello (de ser empresa) </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Ausencia primera Notificaci&oacute;n ( ) </font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Ausencia segunda Notificaci&oacute;n ( ) </font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1"><strong>CARACTERISTICAS DEL DOMICILIO</strong></font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>DATOS DEL NOTIFICADOR </strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Nro. de medidor agua() o luz() ______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Nombres y apellidos:</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Material y color de la fachada _______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">__________________________________________</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Material y color de la puerta _______________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">DNI: __________________</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Otros datos: ____________________________ </font> </td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Firma del Notificador: ________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><font size="+1"><strong>Observaciones:</strong></font><font size="+1"> ______________________________________________________________________________________________________________________ </font></td>
		      </tr>
			  <tr>
			    <td colspan="3">_____________________________________________________________________________________________________________________________________________________________</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td></td>
		      </tr>
			</table>
		</td>
		</tr>		
		
		</table>
	</td>
  </tr>
  <tr>
    <td><u>www.CONVENIO_SITRADOC.gob.pe</u></td>
    <td align="right">Central telef&oacute;nica 616-2222 </td>
  </tr>
  <!--
  <tr>
    <td>(*) Nota: En este espacio se consignar&aacute;n los documentos a notificar </td>
  </tr>
  <tr>
  	<td>1) Notificaci&oacute;n de Cargos: Requisitos establecidos en art&iacute;culo 17&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC) . A s&iacute; mismo, se consignar&aacute;n los documentos anexados. </td>
  </tr>
  <tr>
  	<td>2) Notificaci&oacute;n de Resoluciones: Requisitos establecidos en el art&iacute;culo 19&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC). </td>
  </tr>
  <tr>
  	<td>3) Comunicaciones con el administrado: Requisitos establecidos en los numerales 132.4 y 169.1 de los art&iacute;culos 132&deg; y 169&deg;, respectivamente, de la Ley N&deg; 27444, Ley del Procedimiento Administrativo General. </td>
  </tr>
  -->
  <tr>
  	<td colspan="2">&nbsp;</td>
  </tr>   
  <tr>
  	<td colspan="2">&nbsp;</td>
  </tr>     
</table>


</body>
</html>
