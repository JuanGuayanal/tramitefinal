<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0">
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="4"><div align="center"><strong><font size="+3">C&eacute;dula de Notificaci&oacute;n N&deg;{$notificacion}-Dsvs</font></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="4">&nbsp;</td>
			  </tr>			  
			  <tr>
				<td colspan="4"><div align="right"><strong><font size="+3">EXPEDIENTE N&deg;: {$acto}</font></strong></div></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td width="60%">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="28%"><strong><font size="+1">Destinatario</font></strong></td>
				<td colspan="3"><font size="+1">: {$destinatario}</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio</font></strong></td>
				<td colspan="3"><font size="+1">: {$domicilio} , {$distrito} {$provincia} {$departamento}</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Entidad</font></strong></td>
				<td colspan="3"><font size="+1">:Direcci&oacute;n General de Seguimiento, Control y Vigilancia. MINISTERIO DE LA   PRODUCCION</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Norma que atribuye competencia</font></strong></td>
				<td colspan="3"><font size="+1">:Articulo 81&deg; de la Ley General de Pesca  y el Decreto Supremo 016-2007-Produce Art&iacute;culo 26 literal C)</font></td>
			  </tr>
			  <tr>
				<td><strong><font size="+1">Domicilio Entidad</font></strong> </td>
				<td colspan="2"><font size="+1">:Calle Uno Oeste # 60 Piso 5 Urbanizaci&oacute;n Corpac San Isidro</font></td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Materia</font></strong> </td>
				<td colspan="3"><font size="+1">:Procedimiento Administrativo Sancionador</font></td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <!--<tr>
				<td><strong><font size="+1">Fecha de infracci&oacute;n</font></strong> </td>
				<td colspan="2"><font size="+1">:{if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13)}Las indicadas en los Hechos constatados{else}{$fechaInfraccion}{/if}</font></td>
				<td>&nbsp;</td>
			  </tr>-->
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			  <tr>
				<td><strong><font size="+1">Documentos adjuntos</font></strong></td>
				<td colspan="3"><font size="+1">:{if ($codigoProcedencia==11||$codigoProcedencia==12||$codigoProcedencia==13)}Acta de Inspecci�n  {else}{if $codigoProcedencia==4}Reporte SISESAT N� {$infSISESAT}{else}{$infSISESAT}{/if}{/if}</font></td>
			  </tr>
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  -->
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <!--
			  <tr>
			    <td colspan="4"><div align="center"><strong>DOCUMENTO A NOTIFICAR </strong></div></td>
		      </tr>
			  -->
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"><strong><font size="+1">Presuntas infracciones</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Base Legal:</font></td>
			    <td><font size="+1">{section name=i loop=$infraccion}
						&nbsp;&nbsp;&nbsp;{$infraccion[i].inf}<br>
					{sectionelse}
						&nbsp;&nbsp;&nbsp;
					{/section}</font></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"><strong><font size="+1">Hechos constatados</font></strong></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td><font size="+1">Los siguientes son los hechos constatados:<br>{$hechos}</font></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="3"><font size="+1">Plazo: Cinco(05) dias h&aacute;biles, a partir del dia siguiente de recibida la presente notificaci&oacute;n</font></td>
		      </tr>
			  <tr>
			    <td colspan="2"></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"><div align="right"><strong><font size="+1">__________________________________________</font></strong></div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"><div align="right"><strong><font size="+1">Director de Seguimiento, Vigilancia y Sanciones </font></strong></div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"><div align="right"><font size="+1">Ing. ALEJANDRO RAM&Iacute;REZ SALDA&Ntilde;A &nbsp;&nbsp;&nbsp;&nbsp;</font></div></td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td colspan="2"></td>
			    <td>&nbsp;</td>
		      </tr>			  		  			  			  			  
			</table>
		</td>
		</tr>
		
		<tr>
		<td>
			<table width="100%" border="0" cellspacing="0">
			  <tr>
				<td colspan="3"><!--<div align="center"><strong>Cargo de Entrega</strong></div>--></td>
			  </tr>
			  <tr>
			    <td><font size="+1">Recibido por ____________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">DNI/Otro _______________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Relaci&oacute;n con el destinatario _________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>MOTIVO DE LA DEVOLUCI&Oacute;N</strong></font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Fecha __________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
	          </tr>
			  <tr>
			    <td><font size="+1">Hora ___________________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se neg&oacute; a recibir</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___No firm&oacute;</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">___________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Domicilio cerrado</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">Firma y/o sello</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se mud&oacute;</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Domicilio errado (inexistente)</font></td>
	          </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Se encuentra en el extranjero</font></td>
	          </tr>
			  <tr>
			    <td><font size="+1">Notificador ______________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">___Persona desaparecida</font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">N&deg; de DNI del notificador __________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Otros _________________________________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">______________________________________________________</font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">_______________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td><font size="+1">Firma del Notificador</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1"><strong>CARACTERISTICAS DEL DOMICILIO</strong></font></td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Nro. de medidor agua/luz ______________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">Observaciones ___________________________________________________ </font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Material y color de la fachada ___________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td><font size="+1">Material y color de la puerta ____________ </font></td>
		      </tr>
			  <tr>
			    <td><font size="+1">________________________________________________________________</font></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>
		</td>
		</tr>		
		
		</table>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <!--
  <tr>
    <td>(*) Nota: En este espacio se consignar&aacute;n los documentos a notificar </td>
  </tr>
  <tr>
  	<td>1) Notificaci&oacute;n de Cargos: Requisitos establecidos en art&iacute;culo 17&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC) . A s&iacute; mismo, se consignar&aacute;n los documentos anexados. </td>
  </tr>
  <tr>
  	<td>2) Notificaci&oacute;n de Resoluciones: Requisitos establecidos en el art&iacute;culo 19&deg; y 48&deg; del Reglamento de Inspecciones y Sanciones Pesqueras y Acu&iacute;colas (RISPAC). </td>
  </tr>
  <tr>
  	<td>3) Comunicaciones con el administrado: Requisitos establecidos en los numerales 132.4 y 169.1 de los art&iacute;culos 132&deg; y 169&deg;, respectivamente, de la Ley N&deg; 27444, Ley del Procedimiento Administrativo General. </td>
  </tr>
  -->
  <tr>
  	<td>&nbsp;</td>
  </tr>   
  <tr>
  	<td>Ver al dorso regimen de beneficios de pago de multas e indicaciones.</td>
  </tr>     
</table>


</body>
</html>
