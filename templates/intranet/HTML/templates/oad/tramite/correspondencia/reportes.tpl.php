{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}

-->
</script>
<!-- {/literal} onSubmit="document.{$frmName}.page.value=''";onClick="document.{$frmName}.page.value=''"-->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" {if $tipReporte==11}target="_self"{else} target="_blank"{/if} {if ($tipReporte==12||$tipReporte==13)} enctype="multipart/form-data"{/if}>
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.GENERA_REPORTE_NOTI}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <table width="720" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" class="textoblack">&nbsp;</td>
      <td width="170" class="textoblack"><strong>Tipo de Reporte{$tipReporte}</strong></td>
      <td width="62"><select name="tipReporte" class="ipsel2" onChange="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
          <option value="2"{if $tipReporte==2} selected{/if}>Todos</option>
		  <option value="3"{if $tipReporte==3} selected{/if}>Destinatario</option>
		  <option value="12"{if $tipReporte==12} selected{/if}>Enviar x mail</option>
		  <option value="5"{if $tipReporte==5} selected{/if}>Expedida por</option>		  
		  {if ($coddep_Trab==13||$coddep_Trab==48)}<option value="13"{if $tipReporte==13} selected{/if}>Exportar al SITRADOC (Oec)</option>{/if}		  
		  <option value="1"{if $tipReporte==1} selected{/if}>Nro.Notificacion</option>
		  <option value="4"{if $tipReporte==4} selected{/if}>Nro.Resolucion</option>
		  <option value="8"{if $tipReporte==8} selected{/if}>Listado en Excel</option>		  
		  <option value="6"{if $tipReporte==6} selected{/if}>Mensajer�a Local</option>
		  <option value="7"{if $tipReporte==7} selected{/if}>Mensajer�a Nacional</option>
		  <option value="9"{if $tipReporte==9} selected{/if}>Mensajer�a Nacional en Excel</option>
		  <option value="10"{if $tipReporte==10} selected{/if}>Mensajer�a Local en Excel</option>		  
		  <option value="11"{if $tipReporte==11} selected{/if}>TOTAL en Excel</option>
        </select></td> 
      <td width="200" valign="middle">{if ($tipReporte>=1&& $tipReporte<=5)}<input name="notificacion" type="text" class="iptxt1" value="{$notificacion}" size="6" maxlength="100">{/if}</td>
      <td width="200" valign="middle"><!--<select name="mes" class="ipsel1">
		            {$selMes}
              </select><select name="anyo" class="ipsel1">
		            {$selAnyo}
              </select>-->
			  </td>
	  <td width="140" valign="middle"> <input type="submit" class="submit" value="Generar Reporte" onClick="MM_validateForm('nrotramite','Debe indicar el Nro de Tr�mite,','R');return document.MM_returnValue"> 
      </td>
    </tr>
	<tr>
		<td></td>
		<td></td>
		<td>{if $tipReporte==11}
		<select name="tipMensajeria" class="ipsel1" id="tipMensajeria">
          <option value="3"{if $tipMensajeria==3} selected{/if}>Todo</option>
		  <option value="1"{if $tipMensajeria==1} selected{/if}>Mensajer�a Local</option>
		  <option value="2"{if $tipMensajeria==2} selected{/if}>Mensajer�a Nacional</option>		
		</select>{/if}</td>
		<td>
			{if ($tipReporte<>13)}
			<select name="dia_ini" class="ipsel2" id="dia_ini">
      			{$selDiaIng}
	    	</select>
			<select name="mes_ini" class="ipsel2" id="mes_ini">
	  			{$selMesIng}
        	</select>
			<select name="anyo_ini" class="ipsel2" id="anyo_ini">
      			{$selAnyoIng}
        	</select>
			{/if}
			{if $tipReporte==11}
			<br><select name="ora_ini" class="ipsel1" id="ora_ini">
          {$selHoraIni}
        </select> <select name="min_ini" class="ipsel1" id="min_ini">
          {$selMinIni}
        </select>
		{/if}
		</td>
		<td>
			{if ($tipReporte<>13)}
			<select name="dia_fin" class="ipsel2" id="dia_fin">
      			{$selDiaSal}
	    	</select>
			<select name="mes_fin" class="ipsel2" id="mes_fin">
	  			{$selMesSal}
        	</select>
			<select name="anyo_fin" class="ipsel2" id="anyo_fin">
      			{$selAnyoSal}
        	</select>
			{/if}
			{if $tipReporte==11}
			<br><select name="ora_fin" class="ipsel1" id="ora_fin">
          {$selHoraFin}
        </select> <select name="min_fin" class="ipsel1" id="min_fin">
          {$selMinFin}
        </select>
		{/if}
		</td>
		<td></td>
	</tr>
	{if ($tipReporte==12||$tipReporte==13)}
    {if ($tipReporte==12)}
	<tr>
		<td></td>
		<td></td>
		<td><strong>Enviar por mail</strong></td>
		<td><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
              <strong>Msj. Local</strong></label></td>
		<td><label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERA_REPORTE_NOTI}')">
              <strong>Msj. Nacional</strong></label></td>
		<td></td>
	</tr>
	{/if}
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td><strong>Archivo adjunto:</strong></td>
		<td colspan="2"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
		
	</tr>
    {/if}
  </table>
  </form>
