{$jscript}
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('flag','Estado Diligencia','Sel'{/literal}{if $flag==1},'bbbb','Nombre','R','aaaa','DNI','R','vinculoDest','V�nculo','R'{/if}{if $flag==2},'Observaciones','Observaciones','R'{/if}{if $flag==3},'estadoNot','Motivo','Sel'{if $estadoNot==3},'Observaciones','Observaciones','R'{/if}{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} <br>
    <tr> <br>
      <td colspan="4" align="center" class="item">&nbsp;</td>
	</tr>
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td colspan="2" class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
	
    <tr> 
      <td class="item"> <strong>Indicativo</strong></td>
      <td class="item">{$numero}</td>
      <td><!--<img src="/img/800x600/ico-info3.gif" alt="Indica el acto que se notifica(resoluci�n, oficio, etc.) y la fecha de expedici�n." width="20" height="20" align="top">--></td>
      <td colspan="5" class="item"><div align="center"></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Destinatario</strong> </td>
      <td colspan="2" class="item">{$destinatario}</td>
      <td colspan="5" class="item"><div align="center"></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Domicilio Procesal</strong> </td>
      <td colspan="2" class="item">{$DomProc}</td>
      <td colspan="5" class="item"></td>
    </tr>
    <tr> 
      <td class="item"><strong>N&uacute;mero de folios</strong></td>
      <td colspan="3" class="item">{$folio}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Fecha Entrega Courier </strong></td>
      <td colspan="3" class="item">{$fecEntCourier}</td>
    </tr>
    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td class="item" > <strong><a name="proc" id="proc"></a>Estado de la Correspondencia</strong></td>
      <td colspan="3"> <select name="flag" class="ipsel1" id="flag" onChange="submitForm('{$accion.FRM_MODIFICA_ESTADO_DILIGENCIA}')">
		<option value="none">Seleccione una opci�n</option>
		<option value="1"{if $flag=="1"} selected{/if}>Notificada</option>
		<option value="2"{if $flag=="2"} selected{/if}>Rechazada</option>
		<option value="3"{if $flag=="3"} selected{/if}>No practicada</option>
        </select> </td>
    </tr>
		{if ($flag==1||$flag==2||$flag==3)}
		<tr> 
		  
      <td class="item"> <strong>Fecha Diligencia </strong></td>
		  <td colspan="3"><select name="dia_ing" class="ipsel2" id="dia_ing">
		  {$selDiaIng}
			</select> <select name="mes_ing" class="ipsel2" id="mes_ing">
		  {$selMesIng}
			</select> <select name="anyo_ing" class="ipsel2" id="anyo_ing">
		  {$selAnyoIng}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		{/if}
		<!--
		<tr> 
		  <td class="item"> <strong>Hora. Ingreso</strong></td>
		  <td colspan="3"><select name="hora_ing" class="ipsel2" id="hora_ing">
		  {$selHorIng}
			</select> <select name="min_ing" class="ipsel2" id="min_ing">
		  {$selMinIng}
			</select>
			<img src="/img/800x600/ico-info3.gif" alt="Corresponde a la hora en que el acto notificado fue recibido." width="20" height="20" align="top"> 
		  </td>
		</tr>
		-->
		{if $flag==1}
		<tr>
			<td class="item"><strong>Recibido por:</strong></td>
		  <td colspan="3" class="item"><input name="bbbb" type="text" class="iptxt1" value="{$bbbb}" maxlength="50">
        <img src="/img/800x600/ico-info3.gif" alt="Nombre del que recibe la notificaci�n" width="20" height="20" align="top"></td>
		</tr>
		<tr>
			<td class="item"><strong>Identificado con DNI:</strong></td>
		  <td colspan="3" class="item"><input name="aaaa" type="text" class="iptxt1" value="{$aaaa}" maxlength="30">
        <img src="/img/800x600/ico-info3.gif" alt="DNI del que recibe la notificaci�n" width="20" height="20" align="top"></td>
		</tr>
		<tr>
			<td class="item"><strong>Relaci&oacute;n con el destinatario</strong></td>
		  <td colspan="3" class="item"><input name="vinculoDest" type="text" class="iptxt1" value="{$vinculoDest}" maxlength="30">
        <img src="/img/800x600/ico-info3.gif" alt="V�nculo o relaci�n con el destinatario" width="20" height="20" align="top"></td>
		</tr>		
		{/if}
		{if $flag==3}
		<tr> 
      	  <td class="item" > <strong><a name="aa" id="aa"></a>Motivo</strong></td>
		  <td colspan="3"> <select name="estadoNot" class="ipsel1" id="estadoNot" onChange="submitForm('{$accion.FRM_MODIFICA_ESTADO_DILIGENCIA}')">
			{$selEstado}
			</select> </td>
		</tr>
		{/if}
		{if ($flag==2||($flag==3&& $estadoNot==3))}
				<tr> 
				  <td class="item"><strong>Observaciones</strong> 
        		 </td>
				  <td colspan="3"><textarea name="Observaciones" cols="60" rows="3" class="iptxt1" id="Observaciones" >{$Observaciones}</textarea></td>
				</tr>
		{/if}
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> 
	  		
		<input name="bbSubmit" type="Submit" class="submit" value="Modifica Correspondencia"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_NOTIFICACION}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_BUSCA_NOTIFICACION}');return document.MM_returnValue"> 
        
		<input name="accion" type="hidden" id="accion" value="{$accion.MODIFICA_ESTADO_DILIGENCIA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="idUpdateNot" type="hidden" value="{$idUpdateNot}">
		
		</td>
    </tr>
  </table>
</form>