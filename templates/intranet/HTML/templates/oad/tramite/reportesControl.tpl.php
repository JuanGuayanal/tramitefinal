{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}

-->
</script>
<!-- {/literal} -->
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" target="_blank">
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.GENERAREPORTECONTROL}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
  <table width="740" height="30" border="0" align="center" cellpadding="0" cellspacing="2" class="tabla-encuestas">
    <tr> 
      <td width="1" rowspan="4" class="textoblack">&nbsp;</td>
      <td width="50" rowspan="4" class="textoblack"><strong>Tipo de Reporte</strong></td>
      <td width="540" colspan="2"> <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr> 
            <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Detalles Externos-Exp</strong></label></td>
            <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Exp. Resueltos</strong></label></td>
            <td class="texto td-encuesta"><label>
                <input name="GrupoOpciones1" type="radio" value="3" {if ($GrupoOpciones1==3)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">              
              <strong>Exp. Pendientes</strong></label></td>
			{if ($depe==5)||($cod==646)}
			<td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="4" {if ($GrupoOpciones1==4)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Congreso</strong></label></td>			{/if}
          </tr>
		  <tr>
            <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="5" {if ($GrupoOpciones1==5)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Listado de ExpxTUPA</strong></label></td>
            <td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="6" {if ($GrupoOpciones1==6)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Listado de ExpxDepe</strong></label></td>
			<td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="7" {if ($GrupoOpciones1==7)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Listado Resoluci</strong></label></td>
			<td class="texto td-encuesta">
			<label> 
              <input name="GrupoOpciones1" type="radio" value="8" {if ($GrupoOpciones1==8)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Antiguos</strong></label>
			</td>
		  </tr>
		  <tr>
		  	<td class="texto td-encuesta"><label> 
              <input name="GrupoOpciones1" type="radio" value="9" {if ($GrupoOpciones1==9)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_GENERAREPORTECONTROL}')">
              <strong>Sumario</strong></label></td>
			<td></td>
			<td></td>
			<td></td>
		  </tr>
        </table></td>
      <td colspan="2" rowspan="4" valign="middle"> <input type="submit" class="submit" value="Generar" {if ($GrupoOpciones1==1||!$GrupoOpciones1)} onClick="MM_validateForm('nrotramite','Debe indicar el Nro de Documento','R');return document.MM_returnValue"{/if} {if ($GrupoOpciones1==2||$GrupoOpciones1==3)} onClick="MM_validateForm('dia_ini','D�a inicial','Sel','mes_ini','Mes inicial','Sel','anyo_ini','A�o inicial','Sel','dia_fin','D�a final','Sel','mes_fin','Mes final','Sel','anyo_fin','A�o final','Sel'{if $GrupoOpciones1==3},'dependencia','Dependencia','Sel'{/if});return document.MM_returnValue"{/if}{if $GrupoOpciones1==6} onClick="MM_validateForm('dependencia','Dependencia','Sel');return document.MM_returnValue"{/if}> 
      </td>
    </tr>
    <tr> 
      <td width="62" colspan="2"> 
        <!--<select name="tipReporte" class="ipsel2" onChange="submitForm('{$accion.FRM_GENERAREPORTECONTROL}')" >
          <option value="1"{if $tipreporte==1} selected{/if}>Detalle Externos-Exp</option>
          <option value="2"{if $tipreporte==2} selected{/if}>Exp. Resueltos</option>
          <option value="3"{if $tipreporte==3} selected{/if}>Exp. Pendientes</option>
        </select>-->
        {if $GrupoOpciones1==1||!$GrupoOpciones1} 
        <input name="nrotramite" type="text" class="iptxt1" value="{$nrotramite}" size="20" maxlength="100">
        {/if} 
		{if $GrupoOpciones1==8}
			<select name="dependencia2" class="ipsel1" id="dependencia2">
				{$selDependencia2}
			</select>&nbsp;&nbsp;
			<select name="estado" class="ipsel1" id="estado">
          		<option value="3"{if $estado==3} selected{/if}>Todos</option>
				<option value="1"{if $estado==1} selected{/if}>Modificados</option>
          		<option value="2"{if $estado==2} selected{/if}>Pendientes</option>
			</select>
		{/if}
		
	</td>
      {if $GrupoOpciones1>1} {/if} </tr>
    {if (($GrupoOpciones1>1&& $GrupoOpciones1<4)||($GrupoOpciones1==5||$GrupoOpciones1==6||$GrupoOpciones1==7))} 
    <tr> 
      <td><select name="dia_ini" class="ipsel2" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipsel2" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
      {$selAnyoIng}
        </select></td>
      <td><select name="dia_fin" class="ipsel2" id="dia_fin">
      {$selDiaSal}
	    </select> <select name="mes_fin" class="ipsel2" id="mes_fin">
	  {$selMesSal}
        </select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
      {$selAnyoSal}
        </select></td>
    </tr>
		{if $GrupoOpciones1==5}
		<tr> 
		  <td colspan="2"><select name="tupa" class="ipsel1" id="tupa">
			{$selTupa}
			</select></td>
		</tr>
		{/if}
		{if ($GrupoOpciones1==6||$GrupoOpciones1==7||$GrupoOpciones1==3)}
		<tr> 
		  <td colspan="2"><select name="dependencia" class="ipsel1" id="dependencia">
			{$selDependencia}
			</select></td>
		</tr>
		{/if}
		{if ($GrupoOpciones1==5||$GrupoOpciones1==6||$GrupoOpciones1==9)}
		<tr> 
		  <td colspan="6">{if ($GrupoOpciones1==5)}Nota: N&uacute;mero de Expedientes por procedimiento TUPA{/if}
		  				  {if ($GrupoOpciones1==6)}Nota: N&uacute;mero de Expedientes por dependencia de acuerdo al procedimiento administrativo{/if}
						  {if ($GrupoOpciones1==9)}Nota: Distribuci&oacute;n de la documentaci&oacute;n ingresada a MIDIS anterior al 01/06/2005{/if}
		  </td>
		</tr>
		{/if}
    {/if}
	
  </table>
</form>
  