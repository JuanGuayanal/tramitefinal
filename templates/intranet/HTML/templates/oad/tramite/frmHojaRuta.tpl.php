<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>



<!-- HEADER LEFT "$LOGOIMAGE" --> 
  <table width="100%" border="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellpadding="1" cellspacing="4" bgcolor="#FFFFFF">
        <tr> <br>
            <td colspan="6" class="item-sep"><strong>CONTENIDO</strong></td>
          </tr>
		  {section name=i loop=$hoja} 
          <tr> 
            <td  class="item"><strong>Clase</strong></td>
            <td colspan="5">{$hoja[i].clas}</td>
          </tr>
          <tr> 
            
          <td height="24" class="item"><strong>Numero</strong></td>
            <td colspan="5">{$hoja[i].num}</td>
          </tr>
          <tr> 
            <td class="item"><strong>Remitente</strong></td>
            <td colspan="5" class="item">{$hoja[i].num}</td>
          </tr>
          <tr> 
            <td class="item"><strong>Asunto</strong></td>
            <td colspan="5">{$hoja[i].asunto}</td>
          </tr>
          <tr> 
            <td class="item"><strong>Fecha:</strong></td>
            
          <td class="item">{$FechaActual}</td>
            <td class="item"><strong>Hora</strong></td>
            
          <td class="item">{$HoraActual}</td>
            <td class="item"><strong>Folios</strong></td>
            <td >&nbsp;</td>
          </tr>
		  {/section}
          <tr> 
            <td colspan="6"><hr size="1"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="1" cellspacing="0" bordercolor="#000000" bgcolor="#FFFFFF">
        <tr> 
            <td width="14%" class="item-sep"><strong>Area</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="item-sep"><strong>Fecha</strong></td>
            <td>&nbsp;</td>
            <td class="item-sep"><strong>Hora</strong></td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td class="item-sep"><strong>Folio</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item-sep"><strong>Acc</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item-sep"><strong>V.B.</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td rowspan="3" class="item-sep"><strong>Observaciones</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td width="12%" class="item-sep"><strong>V.B.</strong></td>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item-sep"><strong>Fecha</strong></td>
            <td width="19%">&nbsp;</td>
            <td width="26%" class="item-sep"><strong>Hora</strong></td>
            <td width="29%">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellpadding="1" cellspacing="4" bgcolor="#FFFFFF">
        <tr> 
            <td colspan="8" class="item-sep"><strong>NOTA</strong></td>
          </tr>
          <tr> 
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="8" class="item-sep"><strong>ACCIONES</strong></td>
          </tr>
          <tr> 
            <td class="item">1.-</td>
            <td class="item">Tramitaci&oacute;n</td>
            <td class="item">6.-</td>
            <td class="item">Ayuda memoria</td>
            <td class="item">11.-</td>
            <td class="item">Respuesta Directa</td>
            <td class="item">16.-</td>
            <td class="item">Archivar</td>
          </tr>
          <tr> 
            <td class="item">2.-</td>
            <td class="item">Atenci&oacute;n</td>
            <td class="item">7.-</td>
            <td class="item">Informe</td>
            <td class="item">12.-</td>
            <td class="item">Proyectar Resoluci&oacute;n</td>
            <td class="item">17.-</td>
            <td class="item">Otros</td>
          </tr>
          <tr> 
            <td class="item">3.-</td>
            <td class="item">Conocimiento y fines</td>
            <td class="item">8.-</td>
            <td class="item">Agregar a sus antecedentes</td>
            <td class="item">13.-</td>
            <td class="item">Revisi&oacute;n</td>
            <td class="item">&nbsp;</td>
            <td class="item">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item">4.-</td>
            <td class="item">Coordinar</td>
            <td class="item">9.-</td>
            <td class="item">Evaluar segun disponibilidad</td>
            <td class="item">14.-</td>
            <td class="item">Visaci&oacute;n</td>
            <td class="item">&nbsp;</td>
            <td class="item">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item">5.-</td>
            <td class="item">Opini&oacute;n y/o recomendaci&oacute;n</td>
            <td class="item">10.-</td>
            <td class="item">Preparar respuesta</td>
            <td class="item">15.-</td>
            <td class="item">Notificar al interesado</td>
            <td class="item">&nbsp;</td>
            <td class="item">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
  
</body>
</html>
