<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Sistema de Tr�mite Documentario - C�digo Interno {if ($idDocPadre>0)}{$idDocPadre}{else}{$idMemo}{/if}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<center>
  <table width="70%" border="0" cellspacing="0">
    <tr> 
      <td colspan="4"><table width="100%" border="0" cellspacing="0">
          <tr> 
            <td><div align="center"><strong><font size="-2">MEMORANDO N&deg; {$indicativo}</font><br>
			<hr width="70%" size="1" noshade></strong></div></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td width="9%" valign="top">
      <strong>&nbsp;</strong></td>
      <td width="5%" valign="top"><font size="-1"><strong>
      &nbsp;</strong></font></td>
	  <td width="5%"><font size="-1"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top">
      <font size="-1"><strong></strong>&nbsp;</font></td>
    </tr>
    <tr>
      <td width="9%" valign="top"><strong><font size="-2">A</font></strong></td>
      <td width="5%" valign="top"><font size="-2"><strong>:</strong></font></td>
	  <td width="5%"><font size="-1"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top"><font size="-2"><strong></strong>{$depDestino}</font></td>
    </tr>
    <tr>
      <td width="9%" valign="top"><font size="-2">
      <strong>&nbsp;</strong></font></td>
      <td width="5%" valign="top"><font size="-2"><strong>
      &nbsp;</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top">
      <font size="-2"><strong></strong>&nbsp;</font></td>
    </tr>	
    <tr>
      <td width="9%" valign="top"> 
        <strong><font size="-2">ASUNTO</font></strong></td>
      <td width="5%" valign="top"><font size="-2"><strong>:</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top" >
        <font size="-2"><strong></strong>{$asunto}</font></td>
    </tr>
    <tr>
      <td width="9%" valign="top"><font size="-2">
      <strong>&nbsp;</strong></font></td>
      <td width="5%" valign="top"><font size="-2"><strong>
      &nbsp;</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top">
      <font size="-2"><strong></strong>&nbsp;</font></td>
    </tr>		
    <tr> 
      <td valign="top"><strong><font size="-2">REFERENCIA</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
      <td valign="top"><font size="-2"><strong>:</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td valign="top"><strong><font size="-2"><!--:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $referencia}{$tipo} {$referencia}{else}{if $asunto!="sin asunto"&& $asunto!="Sin asunto"}{$asunto}{/if}{/if}-->
	  				{if $referencia}{if $tipo}{$tipo} {/if}{$referencia}<br>{/if}
	                          {section name=i loop=$refs}
	                          {$refs[i].ind}<br>{/section}
							  {if $refSinResp}{$refSinResp}{/if}
	  </font> </strong> </td>
    </tr>
    <tr>
      <td width="9%" valign="top"><font size="-2">
      <strong>&nbsp;</strong></font></td>
      <td width="5%" valign="top"><font size="-2"><strong>
      &nbsp;</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td width="80%" valign="top">
      <font size="-2"><strong></strong>&nbsp;</font></td>
    </tr>	
    <tr> 
      <td valign="top"><strong><font size="-2">FECHA</font></strong></td>
      <td valign="top"><font size="-2"><strong>:</strong></font></td>
	  <td width="5%"><font size="-2"><strong>&nbsp;</strong></font></td>
      <td valign="top"><font size="-2"><strong></strong>{if $fecMemo}{$fecMemo}{else}{$FechaActual}{/if}</font></td>
    </tr>
    <tr> 
      <td colspan="4" width="95%"><br><font size="-2">Tengo el agrado de dirigirme a usted, a 
        fin de remitirle adjunto el documento de referencia para:</font><br><br></td>
    </tr>
    <tr> 
      <td colspan="4"><table width="100%" border="1" cellspacing="1">
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Conocimiento 
              y fines</font></strong></td>
            <td width="24">{if $a1=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Revisi&oacute;n 
              y an&aacute;lisis</font></strong></td>
            <td width="24">{if $a2=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox5" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Preparar respuesta</font></strong></td>
            <td>{if $a3=='T'}<img src="/img/800x600/stat_true.gif">{else} <input type="checkbox" name="checkbox9" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Acci&oacute;n</font></strong></td>
            <td width="24">{if $a4=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox2" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Preparar Informe</font></strong></td>
            <td width="31">{if $a5=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox6" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Contestar directamente</font></strong></td>
            <td>{if $a6=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else}<input type="checkbox" name="checkbox10" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Coordinar</font></strong></td>
            <td width="24">{if $a7=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox3" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Reformular Informe</font></strong></td>
            <td width="24">{if $a8=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox7" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Designar representante</font></strong></td>
            <td>{if $a9=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox11" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Tr&aacute;mite 
              correspondiente </font></strong></td>
            <td width="24">{if $a10=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox4" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Exposici&oacute;n 
              de Motivos</font></strong></td>
            <td width="24">{if $a11=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} 
              <input type="checkbox" name="checkbox8" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Reiterar</font></strong></td>
            <td>{if $a12=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox12" value="checkbox">
              {/if}<strong></strong></td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Opini&oacute;n / Recomendaci&oacute;n</font> 
              </strong></td>
            <td>{if $a13=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox17" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Ayuda Memoria</font></strong></td>
            <td>{if $a14=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Regularizar</font></strong></td>
            <td>{if $a15=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Comentarios</font> </strong></td>
            <td>{if $a16=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox17" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Proyectar Resoluci&oacute;n</font></strong></td>
            <td>{if $a17=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Proceder seg&uacute;n normatividad vigente</font></strong></td>
            <td>{if $a18=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Correcci&oacute;n</font> </strong></td>
            <td>{if $a19=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else}
              <input type="checkbox" name="checkbox17" value="checkbox">
            {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Visaci&oacute;n</font></strong></td>
            <td>{if $a20=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
            <td width="100" class="item"><strong><font size="-2">Proceder seg&uacute;n disponibilidad presupuestal</font></strong></td>
            <td>{if $a21=='T'}<img src="/img/800x600/stat_true.gif" width="20" height="20">{else} <input type="checkbox" name="checkbox18" value="checkbox">
              {/if}</td>
          </tr>
          <tr> 
            <td width="100" class="item"><strong><font size="-2">Otros y/o Observaciones</font></strong></td>
            <td colspan="5" align="left"><font size="-2"> &nbsp;{$observaciones}</font></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <br>
  <br>
</center>

<table width="85%" border="0" align="center">
  <tr> 
    <td ><font size="-2"><div align="center">
        <p>Atentamente,</p>
        <p>&nbsp;</p>
      </div></font></td>
  </tr>
  <tr> 
    <td > <div align="center"><font size="-2"></font></div></td>
  </tr>
  <tr> 
    <td > <div align="center"><font size="-2"></font></div></td>
  </tr>
  <tr> 
    <td > <div align="center"><font size="-2"></font></div></td>
  </tr>
  <tr> 
    <td > <div align="center"><strong><font size="-2">{if ($codigoDep==12 && ($direAnt==1||$direAnt==2))}{if ($direAnt==1)}GINA PINEDO{else}PERCY SALAS{/if}{else}{if ($codigoDep==106)}XXXX{else}{if ($codigoDep==5 && $idMemo < 9382818)}MARCELO GONZALO CEDAMANOS RODRIGUEZ{else}{$director}{/if}{/if}{/if}</font></strong></div></td>
  </tr>
  {if $codigoDep!=15}
  <tr> 
    <td > <div align="center"><font size="-2"><strong>
	{if $codigoDep==170}
	R.M. N�374-2011-CONVENIO_SITRADOC
	{else}
	{$cargo}
	{/if}
	</strong></font></div></td>
  </tr>
  {/if}
  <tr> 
    <td > <div align="center"><font size="-2"><strong>{if $codigoDep==66}SECRETARIO GENERAL{else}{if ($codigoDep!=16 && $codigoDep!=36)}{$descripcion}{/if}{/if}</strong></font></div></td>
  </tr>
</table>

{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>