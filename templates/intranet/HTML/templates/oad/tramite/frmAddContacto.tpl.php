{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function SubmitForm2(pSel,pForm,pAccion){
	if(pSel.options[pSel.selectedIndex].value!='none'){
		pForm.accion.value=pAccion
		pForm.action="index.php?"+pAccion
		pForm.submit()
	}
}
{/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('nombres','Nombre','R','apellidos','Apellidos','R','direccion','Direccion','R','telefono','Telefono','R','mail','mail','R');return document.MM_returnValue">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>&nbsp;&nbsp;&nbsp;Fecha</strong></td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;Nombres</strong></td>
      <td align="left"> <input name="nombres" type="text" class="iptxt1" id="nombres" value="{$nombres}" size="35" maxlength="255"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;Apellidos</strong></td>
      <td align="left"> <input name="apellidos" type="text" class="iptxt1" id="apellidos" value="{$apellidos}" size="35" maxlength="255"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>&nbsp;&nbsp;&nbsp;Direcci&oacute;n</strong> </td>
      <td align="left"><textarea name="direccion" cols="70" rows="3" class="iptxt1" id="direccion" >{$direccion}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;Tel&eacute;fono</strong></td>
      <td align="left"> <input name="telefono" type="text" class="iptxt1" id="telefono" value="{$telefono}" size="35" maxlength="9" onKeyPress="solo_num();"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;Fax</strong></td>
      <td align="left"> <input name="fax" type="text" class="iptxt1" id="fax" value="{$fax}" size="35" maxlength="9" onKeyPress="solo_num();"></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;E-mail<input name="radio" type="hidden" value="R"></strong></td>
      <td align="left"> <input name="mail" type="text" class="iptxt1" id="mail" value="{$mail}" size="35" maxlength="35"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <!--<tr> 
      <td class="item"> <strong>&nbsp;&nbsp;&nbsp;Ingreso SITRADOC</strong></td>
      <td><input name="radio" type="radio" value="R"> 
        <strong>Base de Datos Real  </strong> <input type="radio" name="radio" value="P" checked> 
        <strong>Base de Datos de Prueba </strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>-->
	<tr>
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Agregar Contacto" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_PERSONA}&menu={$accion.FRM_BUSCA_PERSONA}&subMenu={$accion.FRM_BUSCA_PERSONA}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_CONTACTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
        {if $id>0}<input name="id" type="hidden" id="subMenu" value="{$id}">{/if}
		 </td>
    </tr>
  </table>
</form>