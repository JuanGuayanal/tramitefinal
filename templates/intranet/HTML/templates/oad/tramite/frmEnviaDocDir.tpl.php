{$jscript}
<script language="JavaScript">
<!--
{literal}

 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('trabajador','Trabjador','Sel');return document.MM_returnValue">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if}
    <tr> 
      <td class="texto td-encuesta"><strong>&nbsp;&nbsp;Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>	 
    <tr> 
      <td height="25" class="texto td-encuesta" ><strong>&nbsp;&nbsp;Enviar a</strong></td>
      <td><select name="trabajador" class="ipsel1" id="trabajador">
        {$selTrabajador}
        </select>&nbsp;&nbsp;&nbsp;
      </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<tr>
	<td class="td-encuesta texto"><strong>Observaciones</strong></td>
	<td class="item"><textarea name="observaciones" cols="54" rows="4" class="iptxt1" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea></td>
	<td></td>
	</tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Enviar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_TRAB}&menu={$accion.SUMARIO_TRAB}&subMenu={$accion.SUMARIO_TRAB}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ENVIA_DOCDIR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="id" type="hidden" id="id" value="{$id}">
	  </td>
    </tr>
  </table>
</form>