<html>
<head>
<title>SUMARIO POR STATUS SEG�N TIPO DE DOCUMENTO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" > 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_STATUS_NIVELII}&categoria={$categoria}&dep={$dep}&print=1" target="_self"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a>&nbsp;&nbsp;
		  <a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_STATUS_NIVELII}&categoria={$categoria}&dep={$dep}&print=2" target="_blank"><img src="/img/800x600/ico-acrobat.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir en PDF</a>&nbsp;&nbsp;
		  <a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_STATUS_NIVELII}&categoria={$categoria}&dep={$dep}&print=3" target="_self"><img src="/img/800x600/ico_excel2.jpg" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Exportar a Excel</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6" >
        <tr align="center"> 
          <td height="50" colspan="92" class="textoblack"><strong>SUMARIO POR STATUS SEG�N TIPO DE DOCUMENTO POR CADA MES</strong></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td colspan="90" class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td width="130" colspan="2" class="textoblack"><strong></strong></td>
          <td colspan="15" class="texto"><strong><center>{$textoHaceCincoMes}
          </center>
          </strong></td>
          <td width="17%" colspan="15" class="texto"><strong><center>{$textoHaceCuatroMes}
          </center></strong></td>
          <td width="17%" colspan="15" class="texto"><strong><center>{$textoHaceTresMes}
          </center></strong></td>
          <td width="17%" colspan="15" class="texto"><strong><center>{$textoHaceDosMes}
          </center></strong></td>
		  <td width="17%" colspan="15" class="texto"><strong><center>{$textoHaceunMes}
		  </center></strong></td>
		  <td width="17%" colspan="15" class="texto"><strong><center>{$textoHoy}
		  </center></strong></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td colspan="90" class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>DEPENDENCIA</strong></td>
          <td colspan="3" class="texto"><div align="center">Ingresos</div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
          <td width="17%" colspan="3" class="texto"><div align="center">Ingresos</div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
          <td width="17%" colspan="3" class="texto"><div align="center"><strong>Ingresos</strong></div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
          <td width="17%" colspan="3" class="texto"><div align="center"><strong>Ingresos</strong></div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
          <td width="17%" colspan="3" class="texto"><div align="center">Ingresos</div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
          <td width="17%" colspan="3" class="texto"><div align="center"><strong>Ingresos</strong></div></td>
          <td colspan="3" class="texto"><div align="center">Creados</div></td>
          <td colspan="3" class="texto"><div align="center">Derivados</div></td>
          <td colspan="3" class="texto"><div align="center">Finalizados</div></td>
          <td colspan="3" class="texto"><div align="center">Pendientes</div></td>
        </tr>		
        <tr> 
          <td colspan="2" class="textoblack">&nbsp;</td>
          <td class="texto"><strong>ExpIng</strong></td>
          <td class="texto">ExtIng</td>
          <td class="texto">IntIng</td>
          <td width="20" class="texto">ExpCrea</td>
          <td width="2" class="texto">ExtCrea</td>
          <td width="10" class="texto">IntCrea</td>
          <td width="20" class="texto">ExpDer</td>
          <td width="2" class="texto">ExtDer</td>
          <td width="10" class="texto">IntDer</td>
          <td width="-4" class="texto">ExpFin.</td>
          <td width="2" class="texto">ExtFin</td>
          <td width="10" class="texto">IntFin</td>
          <td width="-4" class="texto">ExpPend</td>
          <td width="2" class="texto">ExtPend</td>
          <td width="10" class="texto">IntPend</td>
          <td width="17%" class="texto">ExpIng</td>
          <td width="9%" class="texto">ExtIng</td>
          <td width="17%" class="texto">IntIng</td>
          <td width="7" class="texto">ExpCrea</td>
          <td width="7" class="texto">ExtCrea</td>
          <td width="20" class="texto">IntCrea</td>
          <td width="7" class="texto">ExpDer</td>
          <td width="7" class="texto">ExtDer</td>
          <td width="20" class="texto">IntDer</td>
          <td width="7" class="texto">ExpFin</td>
          <td width="7" class="texto">ExtFin</td>
          <td width="20" class="texto">IntFin</td>
          <td width="7" class="texto">ExpPend</td>
          <td width="7" class="texto">ExtPend</td>
          <td width="20" class="texto">IntPend</td>
          <td width="17%" class="texto">Exp<strong>Ing</strong></td>
          <td width="9%" class="texto">ExtIng</td>
          <td width="17%" class="texto">IntIng</td>
          <td width="7" class="texto">ExpCrea</td>
          <td width="7" class="texto">ExtCrea</td>
          <td width="20" class="texto">IntCrea</td>
          <td width="7" class="texto">ExpDer</td>
          <td width="7" class="texto">ExtDer</td>
          <td width="20" class="texto">IntDer</td>
          <td width="7" class="texto">ExpFin</td>
          <td width="7" class="texto">ExtIng</td>
          <td width="20" class="texto">IntIng</td>
          <td width="7" class="texto">ExpPend</td>
          <td width="7" class="texto">ExtPend</td>
          <td width="20" class="texto">IntPend</td>
          <td width="17%" class="texto">Exp<strong>Ing</strong></td>
          <td width="9%" class="texto">ExtIng</td>
          <td width="17%" class="texto">IntIng</td>
          <td width="7" class="texto">ExpCrea</td>
          <td width="7" class="texto">ExtCrea</td>
          <td width="20" class="texto">IntCrea</td>
          <td width="7" class="texto">ExpDer</td>
          <td width="7" class="texto">ExtDer</td>
          <td width="20" class="texto">IntDer</td>
          <td width="7" class="texto">ExpFin</td>
          <td width="7" class="texto">ExtFin</td>
          <td width="20" class="texto">IntFin</td>
          <td width="7" class="texto">ExpPend</td>
          <td width="7" class="texto">ExtPend</td>
          <td width="20" class="texto">IntPend</td>
          <td width="17%" class="texto">ExpIng.</td>
          <td width="9%" class="texto">ExtIng</td>
          <td width="17%" class="texto">IntIng</td>
          <td width="7" class="texto">ExpCrea</td>
          <td width="7" class="texto">ExtCrea</td>
          <td width="20" class="texto">IntCrea</td>
          <td width="7" class="texto">ExpDer</td>
          <td width="7" class="texto">ExtDer</td>
          <td width="20" class="texto">IntDer</td>
          <td width="7" class="texto">ExpFin</td>
          <td width="7" class="texto">ExtFin</td>
          <td width="20" class="texto">IntFin</td>
          <td width="7" class="texto">ExpPend</td>
          <td width="7" class="texto">ExtPend</td>
          <td width="20" class="texto">IntPend</td>
          <td width="17%" class="texto">Exp<strong>Ing.</strong></td>
          <td width="9%" class="texto">ExtIng</td>
          <td width="17%" class="texto">IntIng</td>
          <td width="7" class="texto">ExpCrea</td>
          <td width="7" class="texto">ExtCrea</td>
          <td width="20" class="texto">IntCrea</td>
          <td width="7" class="texto">ExpDer</td>
          <td width="7" class="texto">ExtDer</td>
          <td width="20" class="texto">IntDer</td>
          <td width="7" class="texto">ExpFin</td>
          <td width="7" class="texto">ExtFin</td>
          <td width="20" class="texto">IntFin</td>
          <td width="7" class="texto">ExpPend</td>
          <td width="7" class="texto">ExtPend</td>
          <td width="20" class="texto">IntPend</td>
        </tr>
        <tr> 
          <td colspan="92" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{section name=i loop=$list}
        <tr> 
          <td colspan="2" class="textoblack">{$list[i].sigla}</td>
          <td class="texto"><span class="textoblack"><strong><center>
            {$list[i].ExpIngHace5mes|default:"0"}
          </center></strong></span></td>
		  <td class="texto"><strong>{$list[i].ExtIngHace5mes|default:"0"}</strong></td>
		  <td class="texto"><strong>{$list[i].IntIngHace5mes|default:"0"}</strong></td>
		  <td class="texto">{$list[i].ExpCreaHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExtCreaHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].IntCreaHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExpDerHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExtDerHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].IntDerHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExpFinHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExtFinHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].IntFinHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExpPendHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].ExtPendHace5mes|default:"0"}</td>
		  <td class="texto">{$list[i].IntPendHace5mes|default:"0"}</td>
		  <td width="17%" class="texto"><span class="textoblack"><strong><center>
		    {$list[i].ExpIngHace4mes|default:"0"}
		  </center></strong></span></td>
          <td width="9%" class="texto"><strong>{$list[i].ExtIngHace4mes|default:"0"}</strong></td>
          <td width="17%" class="texto"><strong>{$list[i].IntIngHace4mes|default:"0"}</strong></td>
          <td width="7" class="texto">{$list[i].ExpCreaHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtCreaHace4mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntCreaHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpDerHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtDerHace4mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntDerHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpFinHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtFinHace4mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntFinHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpPendHace4mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtPendHace4mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntPendHace4mes|default:"0"}</td>
          <td width="17%" class="texto"><span class="textoblack"><strong><center>{$list[i].ExpIngHace3mes|default:"0"}</center></strong></span></td>
          <td width="9%" class="texto">{$list[i].ExtIngHace3mes|default:"0"}</td>
          <td width="17%" class="texto">{$list[i].IntIngHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpCreaHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtCreaHace3mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntCreaHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpDerHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtDerHace3mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntDerHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpFinHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtFinHace3mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntFinHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpPendHace3mes|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtIngHace3mes|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntIngHace3mes|default:"0"}</td>
          <td width="17%" class="texto"><span class="textoblack"><strong><center>{$list[i].ExpIngHace2mes|default:"0"}</center></strong></span></td>
		  <td width="9%" class="texto">{$list[i].ExtIngHace2mes|default:"0"}</td>
		  <td width="17%" class="texto">{$list[i].IntIngHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpCreaHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtCreaHace2mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntCreaHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpDerHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtDerHace2mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntDerHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpFinHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtFinHace2mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntFinHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpPendHace2mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtPendHace2mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntPendHace2mes|default:"0"}</td>
		  <td width="17%" class="texto"><span class="textoblack"><strong><center>{$list[i].ExpIngHace1mes|default:"0"}</center></strong></span></td>
		  <td width="9%" class="texto">{$list[i].ExtIngHace1mes|default:"0"}</td>
		  <td width="17%" class="texto">{$list[i].IntIngHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpCreaHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtCreaHace1mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntCreaHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpDerHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtDerHace1mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntDerHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpFinHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtFinHace1mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntFinHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExpPendHace1mes|default:"0"}</td>
		  <td width="7" class="texto">{$list[i].ExtPendHace1mes|default:"0"}</td>
		  <td width="20" class="texto">{$list[i].IntPendHace1mes|default:"0"}</td>
		  <td width="17%" class="texto"><span class="textoblack"><strong><center>
		    {$list[i].ExpIngActual|default:"0"}
		  </center></strong></span></td>
          <td width="9%" class="texto">{$list[i].ExtIngActual|default:"0"}</td>
          <td width="17%" class="texto">{$list[i].IntIngActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpCreaActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtCreaActual|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntCreaActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpDerActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtDerActual|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntDerActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpFinActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtFinActual|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntFinActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExpPendActual|default:"0"}</td>
          <td width="7" class="texto">{$list[i].ExtPendActual|default:"0"}</td>
          <td width="20" class="texto">{$list[i].IntPendActual|default:"0"}</td>
        </tr>
		{/section}
        <tr> 
          <td colspan="92" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="texto">&nbsp;</td>
          <td colspan="90" class="texto">&nbsp;</td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp;</td>
  </tr>
  
</table>
{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>