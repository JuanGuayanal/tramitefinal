<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">LISTADO DE DOCUMENTOS</font></strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td colspan="5" class="textoblack" bgcolor="#999999"> <div align="center"></div>
      														<div align="center"></div>
      														<div align="center"><strong><font size="6" face="Arial">DOCUMENTO</font></strong></div>
					   </td>
   					   <td width="30%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
   					   <td width="5%"rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA INGRESO CONVENIO_SITRADOC</font></strong></div></td>
   					   <td width="5" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">{if $sit==4}FECHA DE DERIVACIÓN{else}FECHA INGRESO DEPENDENCIA{/if}</font></strong></div></td>
    					{if $sit==3}<td width="8%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">RECEPCIÓN DEPENDENCIA</font></strong></div></td>{/if}
						{if $sit==2}<td width="8%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA DE T&Eacute;RMINO</font></strong></div></td>{/if}
					   <td width="40%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>
  					 </tr>
  					 <tr bgcolor="#999999"> 
   					   <td class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
   					   <td width="35%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">{if $tipDoc==2}TUPA{else}ASUNTO{/if}</font></strong></div></td>
   					   <td width="8%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">{if $tipDoc==4}N&Uacute;MERO{else}NRO.TRAMITE{/if}</font></strong></div></td>
					   <td width="3%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">INDICATIVO</font></strong></div></td>
					   <td width="2%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">FOLIOS</font></strong></div></td>
  					 </tr>
  					{section name=i loop=$list} 
  					 <tr> 
    					<td width="5%" class="item"><div align="center"><font size="4" face="Arial">{$smarty.section.i.iteration}</font></div></td>
    					<td width="35%" class="item"><font size="4" face="Arial">{$list[i].tup}</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial">{$list[i].nroTD}</font></td>
						<td width="3%" class="item"><font size="4" face="Arial">{$list[i].indicativo}</font></td>
						<td width="2%" class="item"><font size="4" face="Arial">{$list[i].folios}</font></td>
    					<td width="30%" class="item"><font size="4" face="Arial">{$list[i].razSoc}</font></td>
    					<td width="5%" class="item"><font size="4" face="Arial">{if $tipDoc==4}NO TIENE{else}{$list[i].fecIngP}{/if}</font></td>
    					<td width="5" class="item"><font size="4" face="Arial">{$list[i].fecIngD}</font></td>
    					{if $sit==3}<td width="8%" class="item"><font size="4" face="Arial">{$list[i].fecRecD}</font></td>{/if}
						{if $sit==2}<td width="8%" class="item"><font size="4" face="Arial">{$list[i].fecT}</font></td>{/if}
						<td width="40%" class="item"><font size="4" face="Arial">{if ($list[i].trab!=""&& $list[i].trab!='NULL'&& $list[i].trab!=" ")}{$list[i].trab}{else}DIRECTOR{/if}</font></td>
  					 </tr>
  					{sectionelse} 
  					 <tr> 
    					<td colspan="{if $sit==2}8{else}7{/if}" class="textored"><div align="center"><strong><font size="4" face="Arial">NO SE HAN ENCONTRADO RESULTADOS</font></strong></div></td>
  					 </tr>
  					{/section} 
		  	      </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}s</i></font></p>
</body>
</html>
