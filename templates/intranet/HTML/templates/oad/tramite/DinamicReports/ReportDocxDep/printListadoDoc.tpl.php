<html>
<head>
<title>TRAMITE DOCUMENTARIO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA" class="contenido"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
         <td class="item">&nbsp;</td>
         <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp;</strong></td>
        </tr>
      </table>
	 </td>
  </tr>
  <tr> 
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        <tr> 
          <td align="left" bgcolor="#FFFFFF"><img src="/img/pie-logo.gif" width="124" height="30"></td>
          <td align="right" bgcolor="#FFFFFF"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
        </tr>
      </table>
	</td>
  </tr>
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong>LISTADO DE {$tipDoc}S</strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td colspan="3" class="textoblack"> <div align="center"></div>
      														<div align="center"></div>
      														<div align="center"><strong>DOCUMENTO</strong></div>
						</td>
    					<td width="15%" rowspan="2" class="item"><div align="center"><strong>REFERENCIA</strong></div></td>
    					<td width="75%"rowspan="2" class="item"><div align="center"><strong>ASUNTO</strong></div></td>
    					<td width="3" rowspan="2" class="item"><div align="center"><strong>OBSERVACIONES</strong></div></td>
    					<td width="8%" rowspan="2" class="item"><div align="center"><strong> DEP INICIAL </strong></div></td>
						<td width="8%" rowspan="2" class="item"><div align="center"><strong>DEP FINAL </strong></div></td>
  					 </tr>
  					 <tr bgcolor="#999999"> 
    					<td bgcolor="#999999" class="item"> <div align="center"><strong>N&deg;</strong></div></td>
    					<td width="10%" class="item"> <div align="center"><strong>INDICATIVO</strong></div></td>
    					<td width="8%" class="item"> <div align="center"><strong>FECHA</strong></div></td>
  					 </tr>
  					{section name=i loop=$list} 
  					 <tr> 
    					<td width="5%" class="item"><div align="center">{$smarty.section.i.iteration}</div></td>
    					<td width="10%" class="item">{$list[i].ind}</td>
    					<td width="8%" class="item">{$list[i].fecha}</td>
    					<td width="15%" class="item">{$list[i].ref}</td>
    					<td width="75%" class="item">{$list[i].asu}</td>
    					<td width="3" class="item">{$list[i].obs}</td>
    					<td width="8%" class="item">{$list[i].depIni}</td>
						<td width="8%" class="item">{$list[i].depFin}</td>
  					 </tr>
  					{sectionelse} 
  					 <tr> 
    					<td colspan="8" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
  					 </tr>
  					{/section} 
			  	   </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
  	<tr> 
    	<td> 
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        		<tr> 
          			<td align="left" bgcolor="#FFFFFF"><img src="/img/pie-logo.gif" width="124" height="30"></td>
          			<td align="right" bgcolor="#FFFFFF"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
        		</tr>
      		</table>
		</td>
  	</tr>
</table>
{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>