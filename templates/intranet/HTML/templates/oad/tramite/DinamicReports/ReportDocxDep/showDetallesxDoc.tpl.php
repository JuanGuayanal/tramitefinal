<html>
<head>
<title>Documentos : {$doc.desc}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.IMPRIME_FLUJO}&id={$id}"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF">
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="2" class="textoblack"><p align="center"><strong>{$dependencia} </strong></p>
            <p><strong>{if $idTipoDoc!=4}{$numTram}{/if}<br>
          {$descripcion}&nbsp;&nbsp;{$indicativo}</strong></p></td>
        </tr>
        <tr> 
          <td class="textoblack">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Tipo de Documento</strong></td>
          <td width="75%" class="texto">{$claseDoc}</td>
        </tr>
		{if $idTipoDoc!=4}
        <tr> 
          <td width="130" class="textoblack"><strong>Raz&oacute;n Social</strong></td>
          <td width="75%" class="texto">{$RazonSocial}<br>
		                                &nbsp;&nbsp;<strong>Direcci&oacute;n</strong>:{if $direccion&& $direccion!=" "}{$direccion}{else}No especificado{/if}<br>
										&nbsp;&nbsp;<strong>E-mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>: {if $email&& $email!=" "}{$email}{else}No especificado{/if}<br>
										&nbsp;&nbsp;<strong>Tel&eacute;fono&nbsp;</strong>:{if $telefono&& $telefono!=" "}{$telefono}{else}No especificado{/if}     
		  </td>
        </tr>
		{/if}
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Indicativo</strong></td>
          <td width="75%" class="texto">{$indicativo}</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Asunto</strong></td>
          <td width="75%" class="texto">{$asunto}</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Observaciones</strong></td>
          <td class="texto">{$obs|default:'NO ESPECIFICADO'}</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Fecha de Creaci&oacute;n</strong></td>
          <td class="texto">{$fecRec}</td>
        </tr>
        <tr> 
          <td class="texto"><strong>Anexos</strong></td>
          <td class="texto" >{section name=i loop=$anex}
		  <table width="100%" >
  <tr>
    <td class="texto">N�mero</td>
    <td class="texto">:{$anex[i].num|default:'No posee n�mero'}</td>
  </tr>
  <tr>
    <td class="texto">Fecha de Ingreso </td>
    <td class="texto">:{$anex[i].fecIng|default:'No especificado'}</td>
  </tr>
  <tr>
    <td class="texto">Contenido</td>
    <td class="texto">:{$anex[i].cont|default:'No especificado'}</td>
  </tr>
  <tr>
    <td class="texto">Observaciones</td>
    <td class="texto">:{$anex[i].obs|default:'No especificado'}</td>
  </tr>
  <tr>
    <td class="texto">Folios</td>
    <td class="texto">:{$anex[i].folio|default:'No especificado'}</td>
  </tr>
</table>

		  				
						
								{sectionelse} No posee anexos
									{/section}

		</td>
        </tr>
        <tr> 
          <td class="texto"><strong>Avances</strong></td>
          <td class="texto">{$avance|default:'No posee avances'}</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Flujo de Usuarios-Tratamiento</strong></td>
          <td class="texto">{$usuario}</td>
        </tr>
		
		<tr>
			<td class="textoblack">&nbsp;</td>
          <td class="texto">{section name=i loop=$trat} - {$trat[i].desc}{/section} </td>
		</tr>
		
		{section name=i loop=$doc}
        <tr> 
          <td class="textoblack"><strong>Usuario {$smarty.section.i.iteration}</strong></td>
          <td class="texto">
		  <!--
		  <strong>{$doc[i].nombre}</strong>
		  					<br>Enviado el {$doc[i].diaEnvio} a las {$doc[i].horaEnvio}
							<br>{if $doc[i].diaRec}Recibido el {$doc[i].diaRec} a las {$doc[i].horaRec}{else}No lo ha recibido a&uacute;n{/if}
		  					<br>Observaciones:{$doc[i.index_next].obs|default:'No Posee observaciones'}
							<br>Avances:{$doc[i].avance|default:'No Posee avances'}
							
							{math equation="x - y" x=$smarty.section.i.iteration y=1}
							<br>Documento Creado:{$doc[i.index_next].idOficio|default:'No ha creado un documento'}
			-->
							<table width="100%" >
							  <tr>
								<td colspan="2" class="textoblack"><strong>{$doc[i].nombre}</strong></td>
							  </tr>
							  <tr>
								<td width="26%" class="textoblack">&nbsp;&nbsp;Enviado</td>
								<td width="74%" class="texto">:{$doc[i].diaEnvio} {$doc[i].horaEnvio}</td>
							  </tr>
							  <tr>
								<td class="textoblack">&nbsp;&nbsp;Recibido</td>
								<td class="texto">:{if $doc[i].diaRec}{$doc[i].diaRec} {$doc[i].horaRec}{else}No lo ha recibido a&uacute;n{/if}</td>
							  </tr>
							  <tr>
								<td class="textoblack">&nbsp;&nbsp;Observaciones</td>
								<td class="texto">:{$doc[i.index_next].obs|default:'No Posee observaciones'}</td>
							  </tr>
							  <tr>
								<td class="textoblack">&nbsp;&nbsp;Avances</td>
								<td class="texto">:{$doc[i].avance|default:'No Posee avances'}</td>
							  </tr>
							  <tr>
								<td class="textoblack">&nbsp;&nbsp;Documento Generado</td>
								<td class="texto">:{$doc[i.index_next].idOficio|default:'No ha generado documento'}</td>
							  </tr>
							  {if $smarty.section.i.iteration==1}
							  <tr>
							  	<td class="textoblack">Asuntos</td>
								<td class="texto">:{$doc[i].link|default:'No Posee asunto'}</td>
							  </tr>
							  <tr>
							  	<td class="textoblack">Acci&oacute;n</td>
								<td class="texto">:{$doc[i].obsSecre|default:'No Posee acci�n'}</td>
							  </tr>
							  {/if}
							</table>

							
		  </td>
        </tr>
        {sectionelse} 
        <tr> 
          <td colspan="2" class="texto"><div align="center"><strong>No se tiene 
              informaci�n de los trabajadores relacionados a este Documento</strong></div></td>
        </tr>
        {/section} 
        <tr> 
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
      </table> 
	  
<table width="90%" border="1" align="center" cellpadding="1">
  <tr bgcolor="#999999"> 
    <!--<td class="textoblack"> <div align="center"><strong>TIPO DE DOCUMENTO</strong></div></td>-->
    <td class="textoblack"> <div align="center"><strong>CLASE DE DOCUMENTO</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>INDICATIVO-OFICIO</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>FECHA</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>ASUNTO</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>OBSERVACIONES</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>AVANCE</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>DEPENDENCIA ORIGEN</strong></div></td>
    <td class="textoblack"> <div align="center"><strong>DEPENDENCIA DESTINO</strong></div></td>
  </tr>
  <!---->
  {if $idTipoDoc==4}
  <tr> 
    <!--<td class="textoblack">INTERNO</td>-->
    <td class="textoblack">{$claseDoc}</td>
    <td class="textoblack">{$indicativo}</td>
    <td class="textoblack">{$fecDer}</td>
    <td class="textoblack">{$asunto}</td>
    <td class="textoblack">{$obs|default:'No especificado'}</td>
    <td class="textoblack">{$avance|default:'No especificado'}</td>
    <td class="textoblack">{$depOrigen}</td>
    <td class="textoblack">{if $depDestino}{$depDestino}{else}{$depDestino2}{/if}</td>
  </tr>
  {/if}
  <!---->
  {section name=i loop=$dir2} 
  <tr> 
    <!--<td class="textoblack">INTERNO</td>-->
    <td class="textoblack">{$dir2[i].cla}</td>
    <td class="textoblack">{$dir2[i].ind}</td>
    <td class="textoblack">{$dir2[i].fDer}</td>
    <td class="textoblack">{$dir2[i].asu}</td>
    <td class="textoblack">{$dir2[i].obs}</td>
    <td class="textoblack">{$dir2[i].avance}</td>
    <td class="textoblack">{$dir2[i].depo}</td>
    <td class="textoblack">{$dir2[i].depd}</td>
  </tr>
  {sectionelse}
  <tr>
  	<td colspan="8" class="texto"><div align="center">No se ha derivado a otra Dependencia</div></td>
  </tr>
  {/section} 
</table>
{if $user2}
<br>
<table width="100%" >
  <tr>
    <td class="item" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;Usuario</td>
    <td class="item">:{$user2}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Observaciones de finalizaci&oacute;n </td>
    <td class="item">:{$observaciones}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Fecha y Hora de finalizaci&oacute;n </td>
    <td class="item">:{$fecha1}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Nivel 1</td>
    <td class="item">:{$nivel1|default:'No se ha archivado'}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Nivel 2</td>
    <td class="item">:{$nivel2|default:'No se ha archivado'}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Nivel 3</td>
    <td class="item">:{$nivel3|default:'No se ha archivado'}</td>
  </tr>
  <tr>
    <td class="item">&nbsp;&nbsp;&nbsp;&nbsp;Fecha y Hora de archivaje </td>
    <td class="item">:{$fecha2|default:'No se ha archivado'}</td>
  </tr>
</table>
{/if}
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp;</td>
  </tr>
</table>

</body>
</html>