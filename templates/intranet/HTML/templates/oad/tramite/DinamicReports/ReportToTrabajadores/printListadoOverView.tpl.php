<html>
<head>
<title>SUMARIO POR STATUS SEG�N TIPO DE DOCUMENTO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" > 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELII}&coddep={$coddep}&dep={$dep}&trabajador={$trabajador}&print=1" target="_self"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6" >
        <tr align="center"> 
          <td height="50" colspan="6" class="textoblack"><strong>SUMARIO POR STATUS SEG�N TIPO DE DOCUMENTO<br>{$dep}<br>{$nameTrabajador}</strong></td>
        </tr>
        <tr> 
          <td class="textoblack">&nbsp;</td>
          <td colspan="5" class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong></strong></td>
          <td width="25%" class="texto"><strong><center>
            EN CURSO
          </center></strong></td>
          <td width="25%" class="texto"><strong><center>
            VENCIDOS SEG&Uacute;N PLAZO GLOBAL
          </center></strong></td>
          <td width="25%" class="texto"><strong><center>
            VENCIDOS SEG&Uacute;N PLAZO INTERNO
          </center></strong></td>          
		  <td width="25%" class="texto"><strong><center>
            FINALIZADOS EN LOS &Uacute;LTIMOS 30 D&Iacute;AS 
          </center></strong></td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>EXPEDIENTES</strong></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=2&sit=1&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExpPendientes}</a></center></strong></span></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=2&sit=3&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExpVencGlobal}</a></center></strong></span></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=2&sit=4&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExpVencInterno}</a></center></strong></span></td>          
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=2&sit=2&coddep={$coddep}&fecha1={$fecha1}&fecha2={$fecha2}&trabajador={$trabajador}" target="_blank">{$ExpFinalMes}</a></center></strong></span></td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>EXTERNOS</strong></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=1&sit=1&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExtPendientes}</a></center></strong></span></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=1&sit=3&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExtVencGlobal}</a></center></strong></span></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=1&sit=4&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$ExtVencInterno}</a></center></strong></span></td>          
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=1&sit=2&coddep={$coddep}&fecha1={$fecha1}&fecha2={$fecha2}&trabajador={$trabajador}" target="_blank">{$ExtFinalMes}</a></center></strong></span></td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>INTERNOS</strong></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=4&sit=1&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$IntPendientes}</a></center></strong></span></td>
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=4&sit=3&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$IntVencGlobal}</a></center></strong></span></td>
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=4&sit=4&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{$IntVencInterno}</a></center></strong></span></td>          
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=4&sit=2&coddep={$coddep}&fecha1={$fecha1}&fecha2={$fecha2}&trabajador={$trabajador}" target="_blank">{$IntFinalMes}</a></center></strong></span></td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>TOTALES</strong></td>
          <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=5&sit=1&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{math equation="ExpPend+ExtPend+IntPend" ExpPend=$ExpPendientes ExtPend=$ExtPendientes IntPend=$IntPendientes}</a></center></strong></span></td>
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=5&sit=3&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{math equation="ExpVencGlobal+ExtVencGlobal+IntVencGlobal" ExpVencGlobal=$ExpVencGlobal ExtVencGlobal=$ExtVencGlobal IntVencGlobal=$IntVencGlobal}</a></center></strong></span></td>
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=5&sit=4&coddep={$coddep}&trabajador={$trabajador}" target="_blank">{math equation="ExpVencInterno+ExtVencInterno+IntVencInterno" ExpVencInterno=$ExpVencInterno ExtVencInterno=$ExtVencInterno IntVencInterno=$IntVencInterno}</a></center></strong></span></td>          
		  <td width="25%" class="texto"><span class="textoblack"><strong><center><a href="{$frmUrl}?accion={$accion.MUESTRA_REPORT_TRAB_NIVELIII}&tipDoc=5&sit=2&coddep={$coddep}&fecha1={$fecha1}&fecha2={$fecha2}&trabajador={$trabajador}" target="_blank">{math equation="ExpFin+ExtFin+IntFin" ExpFin=$ExpFinalMes ExtFin=$ExtFinalMes IntFin=$IntFinalMes}</a></center></strong></span></td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="texto">&nbsp;</td>
          <td colspan="5" class="texto">&nbsp;</td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp;</td>
  </tr>
  
</table>
{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>