<center>
  <table width="100%" border="0" cellspacing="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0">
          <tr align="center"> 
            <td colspan="6" class="item-sep"><strong>HOJA DE RUTA</strong></td>
          </tr>
          <tr> 
            <td colspan="6"><hr size="1"></td>
          </tr>
          <tr> 
            <td width="12%" class="item"><strong>Clase</strong></td>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>Numero</strong></td>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr> 
            <td><strong>Remitente</strong></td>
            <td colspan="5" class="item">&nbsp;</td>
          </tr>
          <tr> 
            <td><strong>Asunto</strong></td>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>Fecha</strong></td>
            <td width="18%">{$FechaActual}</td>
            <td width="9%" class="item"><strong>Hora</strong></td>
            <td width="26%">{$HoraActual}</td>
            <td width="7%" class="item"><strong>Folios</strong></td>
            <td width="28%">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="6"><hr size="1"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="1" cellspacing="0" bordercolor="#000000">
          <tr> 
            <td width="14%" class="item"><strong>Area</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="item"><strong>Fecha</strong></td>
            <td>&nbsp;</td>
            <td class="item"><strong>Hora</strong></td>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>Folio</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>Acc</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>V.B.</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td rowspan="3" class="item"><strong>Observaciones</strong></td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td width="12%" class="item"><strong>V.B.</strong></td>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item"><strong>Fecha</strong></td>
            <td width="19%">&nbsp;</td>
            <td width="26%" class="item"><strong>Hora</strong></td>
            <td width="29%">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0">
          <tr> 
            <td colspan="4" class="item"><strong>NOTA</strong></td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="4" class="item"><strong>ACCIONES</strong></td>
          </tr>
          <tr> 
            <td width="27%" class="item">1.-Tramitaci&oacute;n</td>
            <td width="27%" class="item">6.-Ayuda memoria</td>
            <td width="24%" class="item">11.-Respuesta Directa</td>
            <td width="22%" class="item">16.-Archivar</td>
          </tr>
          <tr> 
            <td class="item">2.-Atenci&oacute;n</td>
            <td class="item">7.-Informe</td>
            <td class="item">12.-Proyectar Resoluci&oacute;n</td>
            <td class="item">17.-Otros</td>
          </tr>
          <tr> 
            <td class="item">3.-Conocimiento y fines</td>
            <td class="item">8.-Agregar a sus antecedentes</td>
            <td class="item">13.-Revisi&oacute;n</td>
            <td class="item">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item">4.-Coordinar</td>
            <td class="item">9.-Evaluar segun disponibilidad</td>
            <td class="item">14.-Visaci&oacute;n</td>
            <td class="item">&nbsp;</td>
          </tr>
          <tr> 
            <td class="item">5.-Opini&oacute;n y/o recomendaci&oacute;n</td>
            <td class="item">10.-Preparar respuesta</td>
            <td class="item">15.-Notificar al interesado</td>
            <td class="item">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
  </center>