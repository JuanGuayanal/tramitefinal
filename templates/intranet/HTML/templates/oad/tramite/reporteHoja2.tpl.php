<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">DETALLES DEL DOCUMENTO PRINCIPAL</font><br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>

<table width="75%" border="1">
  
<table width="490" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="24" align="center" class="item" valign="bottom"> <div align="left"><font color="#0066CC"><strong><font size="6">N&uacute;mero 
        de Tr&aacute;mite Documentario: </font></strong> </font></div></td>
    <td height="24" align="center" class="item" valign="bottom"><div align="left"><strong><font size="6">{$docOrigen}</font></strong></div></td>
  </tr>
  <tr> 
    <td><div align="left"><font color="#0066CC" size="6"><strong>Tipo de Documento</strong></font></div></td>
    <td><div align="left"><font size="6"><strong>{$descripcion}</strong></font></div></td>
  </tr>
  <tr> 
    <td><div align="left"><font color="#0066CC" size="6"><strong>Remitente</strong></font></div></td>
    <td><div align="left"><font size="6"><strong>{$persona}</strong></font></div></td>
  </tr>
  <tr> 
    <td><div align="left"><font color="#0066CC" size="6"><strong>Indicativo-Oficio</strong></font></div></td>
    <td><div align="left"><font size="6"><strong>{$indicativo}</strong></font></div></td>
  </tr>
  <tr> 
    <td><div align="left"><font color="#0066CC" size="6"><strong>Asunto</strong></font></div></td>
    <td><div align="left"><font size="6"><strong>{$asunto|default:'No especificado'}</strong></font></div></td>
  </tr>
  <tr> 
    <td><div align="left"><font color="#0066CC" size="6"><strong>Folios</strong></font></div></td>
    <td><div align="left"><font size="6"><strong>{$folios}</strong></font></div></td>
  </tr>
  {section name=i loop=$hoja2} 
  <tr> 
    <td colspan="2"> <hr width="100%" size="1"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
      </table></td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Nro. Adjunto:</strong></font></td>
    <td><font size="6"><strong>{$hoja2[i].nro}</strong></font></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Tipo de Documento:</strong></font></td>
    <td><font size="6">{$hoja2[i].des}</font></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Dependencia Destino:</strong></font></td>
    <td><font size="6">{$hoja2[i].dep}</font></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Persona a destinar:</strong></font></td>
    <td><font size="6">{$hoja2[i].nom}</font></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Contenido:</strong></font></td>
    <td><font size="6">{$hoja2[i].cont}</font></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><font size="6"><strong>Observacion</strong></font></td>
    <td><font size="6">{$hoja2[i].obs}</font></td>
    <td>&nbsp;</td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="2" align="center" bgcolor="#FFFFFF"><strong><font size="6">NO 
      HAY ADJUNTOS QUE SE HAYAN ANEXADO AL DOCUMENTO PRINCIPAL</font></strong></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
</body>
</html>
