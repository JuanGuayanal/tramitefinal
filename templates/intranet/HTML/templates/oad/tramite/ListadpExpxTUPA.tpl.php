<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000" size="6"> N&Uacute;MERO DE EXPEDIENTES POR PROCEDIMIENTO TUPA INGRESADOS A LA DEPENDENCIA</font></b></font><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6"> {if $fecInicio2!=$fecFin2} ENTRE EL {$fecInicio2} Y EL {$fecFin2}{else}DEL {$fecInicio2}{/if}</font></font></b></font><br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<br>
<font size="7">PROCEDIMIENTO: {$descripcion}</font>
<br>
<font size="7">DEPENDENCIA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {$siglasDep}</font>
<br>
<br>
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
  <tr bgcolor="#999999"> 
    <td width="5%" align="center"><font size="5"><strong>N&deg;</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
    <td width="45%" align="center"><font size="5"><strong>DEPENDENCIA</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>RECIBIDOS</strong></font></td>
	<td width="10%" align="center"><font size="5"><strong>DERIVADOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>FINALIZADOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>EN PROCESO</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>CORRESPONDENCIA</strong></font></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial">{$smarty.section.i.iteration}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].dep}</font></td>
	<td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].cant}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$list[i].der}</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$list[i].nroFin}</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$list[i].dif}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$list[i].nroCor}</font></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="11" align="center" bgcolor="#FFFFFF"><font size="5"><strong>NO 
      EXISTEN EXPEDIENTES INGRESADOS EN LA(S) FECHA(S) DADA(S) </strong></font></td>
  </tr>
  {/section}
  <!--
  <tr> 
    <td colspan="2" align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial">TOTALES</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$derivados}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5">{$sumaDer}</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5">{$finalizados}</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$operacion}</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" >{$correspondencia}</font></td>
  </tr>
  -->
</table>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
</body>
</html>