<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" -->
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#000000" size="5"><b>MINISTERIO 
        DE LA PRODUCCI&Oacute;N<br>
        ARCHIVO - {$dependencia}<br>
        REGISTRO</b></font> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999">
    <td><div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>N&deg; REGISTRO</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>PERSONA NATURAL/JUR&Iacute;DICA</strong></font></div></td>
    <!--<td width="8%"> 
      <div align="center"><font size="5"><strong>EMBARCACI&Oacute;N</strong></font></div></td>-->
    <td width="18%"> 
      <div align="center"><font size="5"><strong>DIRECCI&Oacute;N</strong></font></div></td>
    <!--<td width="12%"> 
      <div align="center"><font size="5"><strong>MATR&Iacute;CULA</strong></font></div></td>-->
    <td width="5%"><font size="5"><strong>FEC-INI. TRAMITE</strong></font></td>
    <td width="12%"> 
      <div align="center"><font size="5"><strong>PROCEDIMIENTO ADMINISTRATIVO</strong></font></div></td>
    <td width="3%"> 
      <div align="center"><font size="5"><strong>FOL.</strong></font></div></td>
	<td width="18%"> 
      <div align="center"><font size="5"><strong>RD/RM/RVM</strong></font></div></td>
    <td width="10%"> 
      <div align="center"><font size="5"><strong>FEC.RESOL</strong></font></div></td>
    <td width="3%"> 
      <div align="center"><font size="5"><strong>RESPONSABLE</strong></font></div></td>
    <td width="18%"> 
      <div align="center"><font size="5"><strong>FECHA DE ARCHIVO</strong></font></div></td>
    <td width="18%"> 
      <div align="center"><font size="5"><strong>UBICACI&Oacute;N</strong></font></div></td>
	<td width="18%"> 
      <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr>
    <td><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td width="10%"><font size="5">{$list[i].nro}</font></td>
    <td width="10%"><font size="5">{$list[i].per}</font></td>
    <td width="18%"><font size="5">{$list[i].dire}</font></td>
    <!--<td width="8%"><font size="5">{$list[i].emb}</font></td>-->
    <td width="5%"><div align="center"><font size="5">{$list[i].fecIni}</font></div></td>
    <td width="12%"><font size="5">{$list[i].proc}</font></td>
    <!--<td width="12%"><font size="6">{$list[i].mat}</font></td>-->
    <td width="3%"><font size="5">{$list[i].folio}</font></td>
    <td width="18%"><font size="5">{$list[i].resol}</font></td>
    <td width="10%"><font size="5">{$list[i].fecResol}</font></td>
    <td width="3%"><font size="5">{$list[i].resp}</font></td>
    <td width="18%"><font size="5">{$list[i].fecAlmacen}</font></td>
	<td width="18%"><font size="5">{$list[i].nivel1}{$list[i].nivel2}{$list[i].nivel3}</font></td>
    <td width="18%"><font size="5">{$list[i].obs}</font></td>
  </tr>
  {sectionelse}  
  <tr>
    <td colspan="13"><div align="center"><font size="5"><strong>No se ha archivado 
        Documentos hasta el momento</strong></font></div></td>
  </tr>
  {/section}
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>
