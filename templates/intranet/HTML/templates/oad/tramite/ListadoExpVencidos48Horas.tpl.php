<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b>
        <font color="#000000"> <font size="6">LISTADO DE EXPEDIENTES VENCIDOS QUE INGRESARON CON DOS D&Iacute;AS H&Aacute;BILES DE PLAZO PARA COMPLETAR REQUISITOS </font><br>
        </font></b></font> <br>
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>

<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td colspan="3"> <div align="center"><font size="5"><strong>EXPEDIENTE</strong></font></div>
      <div align="center"></div>
      <div align="center"></div></td>
    <td width="12%" rowspan="2"> <div align="center"><font size="5"><strong>RAZ&Oacute;N SOCIAL </strong></font></div></td>
    <td width="50%"rowspan="2"> <div align="center"><font size="5"><strong>PROCEDIMIENTO</strong></font></div></td>
    <td width="50%" rowspan="2"> <div align="center"><font size="5"><strong>&nbsp;OBSERVACIONES</strong></font></div></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td width="5%"> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>N&deg; DE REG. 
        OADA</strong></font></div></td>
    <td width="8%"> <div align="center"><font size="5"><strong>FECHA DE INGRESO</strong></font></div></td>
  </tr>
  {section name=i loop=$list} 
  <tr> 
    <td width="5%"><div align="center"><font size="5">{$smarty.section.i.iteration}</font></div></td>
    <td width="10%"><font size="5">{$list[i].numTram}</font></td>
    <td width="8%"><font size="5">{$list[i].fecRec}</font></td>
    <td width="12%"><font size="5">{$list[i].razSoc}</font></td>
    <td width="50%"><font size="5">{$list[i].proc}</font></td>
    <td width="50%"><div align="center"><font size="5">{$list[i].obs}. {$list[i].obsFin}</font></div></td>
  </tr>
  {sectionelse} 
  <tr> 
    <td colspan="6"><div align="center"><font size="6"><strong>No se han encontrado 
        resultados en las fecha(s) dada(s)</strong></font></div></td>
  </tr>
  {/section} 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  {$fechaGen}{$hora}s</i></font></p>
<font color="#000000" size="5" face="Arial"> 
  </font>
</body>
</html>
