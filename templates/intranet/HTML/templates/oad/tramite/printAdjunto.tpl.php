<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<div align="center">
  <p>&nbsp;</p>
  <p><strong>OFICINA DE TR&Aacute;MITE DOCUMENTARIO </strong>
    </p>
</div>
<br><br>
<table width="100%" >
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
  			<tr>
    			<td width="50%"><strong>TR&Aacute;MITE DOCUMENTARIO</strong></td>
    			<td width="50%"><div align="right"><strong>DOCUMENTO ADJUNTO </strong></div></td>
  			</tr>
	  </table>
	  <br>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		  <tr>
			<td colspan="2"><div align="center"><strong>DOCUMENTO PRINCIPAL : {$numTram} </strong></div></td>
		  </tr>
		  <tr>
			<td width="20%"><strong>{if $idTipoDoc==2}PROCEDIMIENTO{else}ASUNTO{/if}</strong></td>
			<td width="80%">{$asunto}</td>
			</tr>
	  </table><br>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" cellspacing="0">
		  <tr>
			<td colspan="4"><div align="center"><strong>DOCUMENTO ADJUNTO {$numAdj} </strong></div></td>
			</tr>
		  <tr>
			<td width="20%"><strong>Fecha de Emisi&oacute;n </strong></td>
			<td width="50%">{$fecha}</td>
			<td width="15%"><strong>Hora</strong></td>
			<td width="15%">{$hora}</td>
		  </tr>
		  <tr>
			<td width="20%"><strong>Destino</strong></td>
			<td colspan="3">{$dependencia}</td>
			</tr>
		  <tr>
			<td width="20%"><strong>Se&ntilde;or</strong></td>
			<td colspan="3">{$trabajador}</td>
			</tr>
		  <tr>
			<td colspan="4">&nbsp;</td>
		  </tr>
		  <tr>
			<td width="20%"><strong>Tipo de Documento </strong></td>
			<td width="50%">{$tipDoc}</td>
			<td width="15%"><strong>N&deg; de Folios </strong></td>
			<td width="15%">{$folios}</td>
		  </tr>
		  <tr>
			<td width="20%"><strong>Contenido</strong></td>
			<td colspan="3">{$contenido}</td>
			</tr>
		  <tr>
			<td width="20%"><strong>Observaci&oacute;n</strong></td>
			<td colspan="3">{$observaciones}</td>
			</tr>
	  </table>

	</td>
  </tr>
</table>


{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
<script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>