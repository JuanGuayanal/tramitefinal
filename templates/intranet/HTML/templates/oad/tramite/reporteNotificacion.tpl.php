<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6">NOTIFICACION {$notificacion|default:'NO DISPONIBLE'}</font></font></b></font>
      </div>
      <hr width="50%" size="1" noshade>  </td>
  </tr>
</table>
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td class="item"><p><font size="+3">El {if $coddep==5}Secretario General{else}{$funcionario|default:'NO DISPONIBLE'}{/if} del Ministerio de la 
        Producci&oacute;n, sito en Calle Uno Oeste N&deg; 60, Urbanizaci&oacute;n Corpac, 
        San Isidro, Departamento de Lima, de acuerdo a la Ley del Procedimiento 
        Administrativo General, Ley N&deg; 27444, cumple con notificar personalmente la
        {$acto|default:'NO DISPONIBLE'}, expedida por {$detalles|default:'NO DISPONIBLE'}, en el procedimiento {$procedimiento|default:'NO DISPONIBLE'}, 
        a:</font></p>
      <p><font size="+3">DESTINATARIO (s) : {$destinatario|default:'NO DISPONIBLE'}</font></p>
      <p><font size="+3">DOMICILIO PROCESAL : {$domicilio|default:'NO DISPONIBLE'}</font></p>
      <p><font size="+3"><strong>Marcar con &quot;X&quot; la opci&oacute;n que 
        corresponda:</strong></font></p>
      <p><font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado 
        entra en vigencia</strong> :</font><br><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde 
        la fecha de su emisi&oacute;n {if $vigencia==1}(X){else}( ){/if} <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia 
        anticipada){if $vigencia==2}(X){else}( ){/if}</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n 
        {if $vigencia==3}(X){else}( ){/if} <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n 
        {if $vigencia==4}(X){else}( ){/if}</font> </p>
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</strong> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $estado==1}(X){else}( ){/if}SI &nbsp;&nbsp;{if 
        $estado==0}(X){else}( ){/if}NO</font> </p>
		
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El administrado podr&aacute; interponer Recurso 
        administrativo de</strong>:</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que 
        lo expidi&oacute;{if $recurso1==1}(X){else}( ){/if} ; &nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o 
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para 
        que se eleve al superior jer&aacute;rquico {if $recurso2==1}(X){else}( ){/if};&nbsp;&nbsp; 
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para 
        que se eleve al superior jer&aacute;rquico{if $recurso3==1}(X){else}( ){/if};</font> 
      </p>
		<p>
        <font size="+3">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as &uacute;tiles 
        (h&aacute;biles consecutivos) contados desde el d&iacute;a siguiente de 
        su fecha de su Notificaci&oacute;n.</font> <br>
        <font size="+3">Se adjunta copia autenticada u original (en su caso) del 
        texto &iacute;ntegro del acto notificado con {$folio|default:'NO DISPONIBLE'} folios.</font></p>
      <p>&nbsp;</p>
      <p><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $coddep==5}Secretario General{else}FIRMA Y SELLO{/if} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;Fecha:{if $coddep==5}<br>&nbsp;&nbsp;&nbsp;Ministerio de la Producci&oacute;n{/if}</font><br>
      <hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que recibe 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3"><strong>FECHA 
        DE NOTIFICACI&Oacute;N</strong> : El acto notificado fue recibido el {if 
        $persona}{$fecha}{else}..........................{/if}, a horas {if $hora}{$hora}{else}............{/if}, 
        por {if $nombrePersona}{$nombrePersona}{else}......................................................................{/if}, 
        identificado con {if $persona}{$persona}{else}..........................................{/if}.</font> 
        <br>
        <font size="+3">Relaci&oacute;n con destinatario .................................................................................................</font> 
        <br>
        <br>
      </p>
      <p align="center"><font size="+3">.................................</font><font size="+3">..........</font><br>
        <font size="+3">Firma del que recibe</font><br>
      <font size="+3">y Sello (si tuviera)</font> </p>
		<hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que realiza 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3">Notificaci&oacute;n 
        rechazada (indicar): {if ($flag==2)}{$Observaciones} {else}...............................................................................{/if}</font> 
        <br>
        <font size="+3">Notificaci&oacute;n no practicada (marcar la opci&oacute;n):</font><br>
        <font size="+3">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Desconocer lugar de posible 
        notificación (domicilio procesal): &nbsp;&nbsp;&nbsp;&nbsp; {if $motivo==1}(X) 
        {else}.....{/if} <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Imposibilidad practicar la notificación:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;{if $motivo==2}(X){else}.....{/if} <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Otras circunstancias (indicar):{if $motivo==3}(X) 
        {$Observaciones}{else} ....................................................................{/if} 
        </font> </p>
      <p><br>
        <font size="+3">NOTIFICADOR</font><font size="+2"></font> 
      </p>
      <p><font size="+3">Firma: .............................................. 
        Nombre: ........................................................ DNI: 
        </font><font size="+3">..........</font><font size="+3">....................</font><font size="+3">..........</font></p>
    </td>
  </tr>
</table>

<!--
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6">NOTIFICACI&Oacute;N {$notificacion|default:'NO DISPONIBLE'}</font></font></b></font>
      </div>
    <hr width="50%" size="1" noshade>  </td>
  </tr>
</table>

  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td class="item"><p><font size="+3">El</font><font size="+3"> {$funcionario|default:'NO DISPONIBLE'} del Ministerio de la 
        Producci&oacute;n, sito en Calle Uno Oeste N&deg; 60, Urbanizaci&oacute;n Corpac, 
        San Isidro, Departamento de Lima, de acuerdo a la Ley del Procedimiento 
        Administrativo General, Ley N&deg; 27444, cumple con notificar personalmente la
        {$acto|default:'NO DISPONIBLE'}, expedida por {$detalles|default:'NO DISPONIBLE'}, en el procedimiento {$procedimiento|default:'NO DISPONIBLE'}, 
        a:</font></p>
      <p><font size="+3">DESTINATARIO (s) : {$destinatario|default:'NO DISPONIBLE'}</font></p>
      <p><font size="+3">DOMICILIO PROCESAL : {$domicilio|default:'NO DISPONIBLE'}</font></p>
      <p><font size="+3"><strong>Marcar con &quot;X&quot; la opci&oacute;n que 
        corresponda:</strong></font></p>
      <p><font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado 
        entra en vigencia</strong> :</font><br><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde 
        la fecha de su emisi&oacute;n {if $vigencia==1}(X){else}( ){/if} <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia 
        anticipada){if $vigencia==2}(X){else}( ){/if}</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n 
        {if $vigencia==3}(X){else}( ){/if} <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n 
        {if $vigencia==4}(X){else}( ){/if}</font> </p>
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</strong> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $estado==1}(X){else}( ){/if}SI &nbsp;&nbsp;{if 
        $estado==0}(X){else}( ){/if}NO</font> </p>
		
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El administrado podr&aacute; interponer Recurso 
        administrativo de</strong>:</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que 
        lo expidi&oacute;{if $recurso1==1}(X){else}( ){/if} ; &nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o 
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para 
        que se eleve al superior jer&aacute;rquico {if $recurso2==1}(X){else}( ){/if};&nbsp;&nbsp; 
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para 
        que se eleve al superior jer&aacute;rquico{if $recurso3==1}(X){else}( ){/if};</font> 
      </p>
		<p>
        <font size="+3">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as &uacute;tiles 
        (h&aacute;biles consecutivos) contados desde el d&iacute;a siguiente de 
        su fecha de su Notificaci&oacute;n.</font> <br>
        <font size="+3">Se adjunta copia autenticada u original (en su caso) del 
        texto &iacute;ntegro del acto notificado con {$folio|default:'NO DISPONIBLE'} folios.</font></p>
      <p>&nbsp;</p>
      <p><font size="+3">&nbsp;&nbsp;&nbsp;FIRMA Y SELLO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;Fecha:</font><br>
      <hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que recibe 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3"><strong>FECHA 
        DE NOTIFICACI&Oacute;N</strong> : El acto notificado fue recibido el {if 
        $persona}{$fecha}{else}..........................{/if}, a horas {if $hora}{$hora}{else}............{/if}, 
        por {if $nombrePersona}{$nombrePersona}{else}......................................................................{/if}, 
        identificado con {if $persona}{$persona}{else}..........................................{/if}.</font> 
        <br>
        <font size="+3">Relaci&oacute;n con destinatario .................................................................................................</font> 
        <br>
        <br>
      </p>
      <p align="center"><font size="+3">.................................</font><font size="+3">..........</font><br>
        <font size="+3">Firma del que recibe</font><br>
      <font size="+3">y Sello (si tuviera)</font> </p>
		<hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que realiza 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3">Notificaci&oacute;n 
        rechazada (indicar): {if ($flag==2)}{$Observaciones} {else}...............................................................................{/if}</font> 
        <br>
        <font size="+3">Notificaci&oacute;n no practicada (marcar la opci&oacute;n):</font><br>
        <font size="+3">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Desconocer lugar de posible 
        notificación (domicilio procesal): &nbsp;&nbsp;&nbsp;&nbsp; {if $motivo==1}(X) 
        {else}.....{/if} <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Imposibilidad practicar la notificación:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;{if $motivo==2}(X){else}.....{/if} <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Otras circunstancias (indicar):{if $motivo==3}(X) 
        {$Observaciones}{else} ....................................................................{/if} 
        </font> </p>
      <p><br>
        <font size="+3">NOTIFICADOR</font><font size="+2"></font> 
      </p>
      <p><font size="+3">Firma: .............................................. 
        Nombre: ........................................................ DNI: 
        </font><font size="+3">..........</font><font size="+3">....................</font><font size="+3">..........</font></p>
    </td>
  </tr>
</table>
-->
</body>
</html>
