<html>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
  
<table width="540" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
      <td width="40" rowspan="2" align="center">&nbsp;</td>
      
    <td rowspan="2" align="center" valign="top"><img src="http://www.CONVENIO_SITRADOC.gob.pe/img/apps/solicitudinfo/img_index.gif" width="130" height="119" hspace="3"></td>
      
    <td align="right">&nbsp;</td>
      <td width="40" rowspan="2" align="right">&nbsp;</td>
    </tr>
    <tr> 
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000; text-align: justify;"><strong><br>
      Datos principales del TR&Aacute;MITE {$docOrigen} y sus respectivos adjuntos</strong></td>
    </tr>
    <tr> 
      <td align="center">&nbsp;</td>
      <td colspan="2" align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr> 
      <td align="center">&nbsp;</td>
      <td colspan="2" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr> 
          <td colspan="4" bgcolor="#dcdccc" style="COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; FONT-SIZE: x-small;"><strong>Informaci&oacute;n 
            del Documento Principal</strong></td>
        </tr>
        <tr> 
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr> 
          <td width="115" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>N&deg; 
            TR&Aacute;MITE :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$docOrigen}</strong></td>
        </tr>
        <tr> 
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>TIPO 
            DOCUMENTO :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$descripcion}</strong></td>
        </tr>
        <tr> 
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>REMITENTE 
            :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$persona}</strong></td>
        </tr>
        <tr> 
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>INDICATIVO 
            :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$indicativo}</strong></td>
        </tr>
        <tr> 
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>ASUNTO 
            :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$asunto|default:'No especificado'}</strong></td>
        </tr>
        <tr> 
          <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000000;"><strong>FOLIOS 
            :</strong></td>
          <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><strong>{$folios}</strong></td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #CE001F; text-decoration: none;"><p>* 
              Los datos del Tr&aacute;mite est&aacute;n actualizados a la fecha.</p>
            </td>
        </tr>
      </table></td>
      <td align="center">&nbsp;</td>
    </tr>
  </table>
<br><br>
<table width="100%" border="1" align="center">
  <tr> 
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>N&deg; 
      ADJUNTO</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>TIPO 
      DE DOCUMENTO</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>DEPENDENCIA 
      DESTINO</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>PERSONA 
      A DESTINAR</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>CONTENIDO</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>OBSERVACI&Oacute;N</strong></td>
  </tr>
  {section name=i loop=$hoja2} 
  <tr> 
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].nro}</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].des}</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].dep}</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].nom}</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].cont}</strong></td>
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>{$hoja2[i].obs}</strong></td>
  </tr>
 {sectionelse} 
  <tr> 
    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;" colspan="6"><div align="center"><strong>NO 
        HAY ADJUNTOS QUE SE HAYAN ANEXADO AL DOCUMENTO PRINCIPAL</strong></div></td>
  </tr>
  {/section}
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="4" face="Arial"><i>Actualizado al 
  {$fechaGen}</i></font></p>
</body>
</html>