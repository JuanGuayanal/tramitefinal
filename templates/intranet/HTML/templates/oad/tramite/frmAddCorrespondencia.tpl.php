{$jscript}
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table border="0" align="center" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
    <tr> 
	<br>
      <td colspan="4" class="item-sep"> <strong>CARACTER&Iacute;STICAS GENERALES</strong></td>
    </tr> 
    <tr> 
      <td class="item"><strong>No documento</strong></td>
      <td class="item">{$NumDocumento}-{$Anyo}-<strong>CONVENIO_SITRADOC</strong></td>
      <td class="item"><strong>Oficina</strong></td>
      <td><select name="oficina" class="ipsel1" id="oficina">
        {$selOficina}
        </select></td>
    </tr>
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}</td>
	  <td class="item"><strong>Hora</strong></td>
	  <td class="item">{$HoraActual}</td>
    </tr>
    <tr> 
      <td class="item"><strong>No correlativo</strong></td>
      <td colspan="3" ><input type="text" class="iptxt1" id="correlativo" value="{$correlativo}"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Dependencia Origen</strong></td>
      <td colspan="3"><select name="depOrigen" class="ipsel1" id="depOrigen">
        {$selDepOrigen}
        </select></td>
    </tr>
    <tr> 
      <td class="item"><strong>Remitente</strong></td>
      <td colspan="3">
          <select name="remitente" class="ipsel1" id="remitente">
        {$selRemitente}
          </select>
        </td>
    </tr>
    <tr> 
      <td class="item"><strong>Tipo de documento</strong></td>
      <td><select name="tipo_doc" class="ipsel1" id="tipo_doc">
        {$selTipoDoc}
          </select></td>
      <td class="item"><strong>No Folios</strong></td>
      <td><input type="text" class="iptxt1" id="nro_folios" value="{$nro_folios}"></td>
    </tr>
	<tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="4" class="item-sep"> <strong>DATOS COURIER</strong></td>
    </tr> 
    <tr> 
      <td class="item"><strong>Empresa mensajer&iacute;a</strong></td>
      <td><select name="emp_mens" class="ipsel1" id="emp_mens">
        {$selEmpMens}
          </select></td>
      <td class="item"><strong>Empaque</strong></td>
      <td><select name="empaque" class="ipsel1" id="empaque">
        {$selEmpaque}
          </select></td>
    </tr>
    <tr> 
      <td class="item"><strong>Consignatario</strong></td>
      <td colspan="3"><select name="consignatario" class="ipsel1" id="consignatario">
        {$selConsignatario}
          </select></td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong></td>
      <td colspan="3"> 
        <textarea name="observaciones" cols="54" class="iptxt1" id="observaciones" >{$observaciones}</textarea>
      </td>
    </tr>
	<tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
	<tr> 
      <td colspan="4" class="item-sep"> <strong>OBSERVACIONES</strong></td>
    </tr> 
    <tr> 
      <td class="item"><strong>Departamento</strong></td>
      <td><select name="departamanto" class="ipsel1" id="departamento">
        {$selDepartamento}
          </select></td>
      <td class="item"><strong>Fecha Recibido</strong></td>
      <td>{$FechaRecibido}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Provincia</strong></td>
      <td><select name="provincia" class="ipsel1" id="provincia">
        {$selProvincia}
          </select></td>
      <td class="item"><strong>Fecha Envio</strong></td>
      <td>{$FechaEnvio}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Distrito</strong></td>
      <td><select name="distrito" class="ipsel1" id="distrito">
        {$selDistrito}
          </select></td>
      <td class="item"><strong>Fecha Recepci&oacute;n</strong></td>
      <td>{$FechaRecep}</td>
    </tr>
	<tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="4" align="center"> 
        <input name="bSubmit" type="Submit" class="submit" value="Agregar Correspondencia"> 
        &nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue">
		<input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_CORRESPONDENCIA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> </td>
    </tr>
  </table>
</form>