<!--
<table width="520" border="8" cellpadding="0"cellspacing="0" background="/img/800x600/ogdpe/proyectos/bg_hdserch.gif" class="TEmb">
  <br>
  <tr>
    <td align="left">
		<table  width="100%" border="0" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
        <tr> 
          <td width="50%" class="texto"><strong>Nuevos Documentos Externos en 
            el a&ntilde;o:</strong></td>
          <td width="25%" class="textored"><strong>{$nroExt}</strong></td>
          <td width="25%" rowspan="4" class="textored"><img src="/img/800x600/dirinternas.jpg"></td>
        </tr>
        <tr> 
          <td class="texto"><strong>Nuevos Expedientes en el a&ntilde;o:</strong></td>
          <td class="textored"><strong>{$nroExp}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>N&uacute;mero de Resoluciones en el a&ntilde;o:</strong></td>
          <td class="textored"><strong>{$nroResol}</strong></td>
        </tr>
        <tr> 
          <td class="texto"><strong>N&uacute;mero de Correspondencias en el a&ntilde;o:</strong></td>
          <td class="textored"><strong>{$nroCor}</strong></td>
        </tr>
      </table>
    </td>
  </tr>
</table>-->



<br>
  <table width="735" border="0" cellspacing="0" cellpadding="0" class="tabla-encuestas">
    <tr>
      
    <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="top">       
      <table width="700" border="0" cellspacing="0" cellpadding="2">
        <tr> 
          <td colspan="4" class="td-nada td-encuesta" height="8">&nbsp;</td>
        </tr>
        <tr class="tr-encuesta"> 
          <td width="269" colspan="2" class="td-encuesta texto"><strong>EXPEDIENTES</strong></td>
          <td width="223" colspan="2" class="texto td-encuesta"> 
          </td>
        </tr>
        {section name=i loop=$option} 
        <tr class="tr-encuesta"> 
          <td width="269" colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Expedientes a partir del 2005 </td>
          <td width="223" colspan="2" class="texto td-encuesta"> {$option[i].barra} &nbsp;&nbsp;{$nroExp} 
          </td>
        </tr>
        {/section}
		{section name=i loop=$option3} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Expedientes Incompletos</td>
          <td colspan="2" class="texto td-encuesta">{$option3[i].barra3} &nbsp;&nbsp;{$TotalExpI}</td>
        </tr>
        {/section}
		{section name=i loop=$option4} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Expedientes Incompletos Finalizados 
            en OADA</td>
          <td colspan="2" class="texto td-encuesta">{$option4[i].barra4} &nbsp;&nbsp;{$TotalExpF}</td>
        </tr>
        {/section} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="td-encuesta texto"><strong>DOCUMENTOS EXTERNOS</strong></td>
          <td colspan="2" class="texto td-encuesta"></td>
        </tr>
		{section name=i loop=$option2} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Documentos Externos a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option2[i].barra2} &nbsp;&nbsp;{$nroExt}</td>
        </tr>
        {/section} 
		{section name=i loop=$option5} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="td-encuesta texto"><strong>RESOLUCIONES</strong></td>
          <td colspan="2" class="texto td-encuesta">{$option5[i].barra5} &nbsp;&nbsp;{$TotalResol}</td>
        </tr>
        {/section} 
		{section name=i loop=$option7} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones Supremas a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option7[i].barra7} &nbsp;&nbsp;{$nroResol1}</td>
        </tr>
		{/section}
		{section name=i loop=$option8}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones 
            Ministeriales a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option8[i].barra8} &nbsp;&nbsp;{$nroResol2}</td>
        </tr>
		{/section}
		{section name=i loop=$option9}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones 
            ViceMinisteriales a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option9[i].barra9} &nbsp;&nbsp;{$nroResol3}</td>
        </tr>
		{/section}
		{section name=i loop=$option10}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones 
            Directorales a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option10[i].barra10} &nbsp;&nbsp;{$nroResol4}</td>
        </tr>
		{/section}
		<!--{section name=i loop=$option11}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones 
            Secretariales en el a�o</td>
          <td class="texto td-encuesta">{$option11[i].barra11} &nbsp;&nbsp;{$nroResol5}</td>
        </tr>
		{/section}-->
		{section name=i loop=$option12}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Decretos 
            Supremos a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option12[i].barra12} &nbsp;&nbsp;{$nroResol6}</td>
        </tr>
		{/section}
        <tr class="tr-encuesta"> 
          <td colspan="2" class="td-encuesta texto"><strong>CORRESPONDENCIAS</strong></td>
          <td colspan="2" class="texto td-encuesta"></td>
        </tr>
		{section name=i loop=$option6} 
        <tr class="tr-encuesta"> 
          <td colspan="2" class="texto td-encuesta">&nbsp;&nbsp;&nbsp;&nbsp;Correspondencias a partir del 2005 </td>
          <td colspan="2" class="texto td-encuesta">{$option6[i].barra6} &nbsp;&nbsp;{$TotalCor}</td>
        </tr>
        {/section} 
        <tr> 
          <td colspan="4" class="texto">&nbsp;</td>
        </tr>
		<!--
        <tr> 
          <td width="40" rowspan="4" class="texto"><img src="/img/ojo_tramite.gif"></td>
          <td width="210" class="texto td-encuesta">&nbsp;&nbsp;<a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_EXP1}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=400'); void('')" onMouseOver="window.status=''">Total 
            Expedientes</a></td>
          <td class="texto"></td>
          <td class="texto"></td>
        </tr>
        <tr> 
          <td width="210" class="texto td-encuesta">&nbsp;&nbsp;<a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_EXP2}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=400'); void('')" onMouseOver="window.status=''">Expedientes 
            Vencidos</a></td>
          <td class="texto"><span class="texto td-encuesta">:Expedientes que ya vencieron seg&uacute;n TUPA.</span></td>
          <td class="texto"></td>
        </tr>
        <tr> 
          <td width="210" class="texto td-encuesta">&nbsp;&nbsp;<a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_EXP3}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=400'); void('')" onMouseOver="window.status=''">Expedientes 
            No Vencidos</a></td>
          <td class="texto"><span class="texto td-encuesta">:Expedientes que disponen m&aacute;s de 7 d&iacute;as para vencerse seg&uacute;n TUPA.</span></td>
          <td class="texto"></td>
        </tr>
        <tr> 
          <td width="210" class="texto td-encuesta">&nbsp;&nbsp;<a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_EXP4}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=800,height=400'); void('')" onMouseOver="window.status=''">Expedientes 
            por Vencerse</a></td>
          <td class="texto"><span class="texto td-encuesta">:Expedientes que est&aacute;n a 7 d&iacute;as de vencerse seg&uacute;n TUPA.</span></td>
          <td class="texto"></td>
        </tr>
		-->
      </table>
      </td>
    </tr>
  </table>
