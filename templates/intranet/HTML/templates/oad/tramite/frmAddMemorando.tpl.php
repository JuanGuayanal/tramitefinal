{$jscript}
<form action="{$frmUrl}" method="post" name="{$frmName}">
    <table width="100%" border="0" cellpadding="2" cellspacing="4" bgcolor="#FFFFFF">
    {if $errors} 
    <tr> 
      <td class="item">{$errors} </td>
    </tr>
    {/if}
      <tr> <br>
        <td colspan="2" class="item-sep"> <strong>MEMORANDO No.</strong> {$NumMemorando}-{$anyo}-<strong>CONVENIO_SITRADOC</strong> 
        </td>
      </tr>
      <tr> 
        <td class="item"><strong>A</strong></td>
        <td> <select name="OficinaDestino" class="ipsel2">
	  {$selOficinaDestino}
       </select> </td>
      </tr>
      <tr> 
        <td class="item"><strong>REF.</strong></td>
        <td class="item"><strong>Tipo de documento : </strong> <select name="TipoDocumento" class="ipsel2">
	  {$selTipoDocumento}
       </select> <strong>A&ntilde;o</strong> <select name="select3" class="ipsel2">
          </select> <strong>Numero</strong> <select name="select5" class="ipsel2">
          </select> </td>
      </tr>
      <tr> 
        <td class="item"><strong>Fecha</strong></td>
        <td class="item">{$FechaActual}</td>
      </tr>
      <tr> 
        <td colspan="2"><hr size="1"></td>
      </tr>
	  <tr>
	  <td class="item-sep" colspan="2"><strong>REFERENCIA</strong>
	  </td>
	  </tr>
      <tr> 
        <td colspan="2"><table width="100%" border="0" cellspacing="0">
          <tr> 
            <td><strong> 
              <input type="checkbox" name="checkbox" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Tramitaci&oacute;n </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox5" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Opini&oacute;n/Recomendaci&oacute;n </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox9" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Calificar/Evaluar</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox13" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Visaci&oacute;n </strong></td>
          </tr>
          <tr> 
            <td><strong> 
              <input type="checkbox" name="checkbox2" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Atenci&oacute;n</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox6" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Ayuda Memoria</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox10" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Preparar Respuesta </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox14" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Ampliar Informe </strong></td>
          </tr>
          <tr> 
            <td><strong> 
              <input type="checkbox" name="checkbox3" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Conocimiento y Fines </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox7" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Informe</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox11" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Proyectar Resoluci&oacute;n </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox15" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Reformular Informe </strong></td>
          </tr>
          <tr> 
            <td><strong> 
              <input type="checkbox" name="checkbox4" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Coordinar</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox8" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Agregar a sus Antecedentes </strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox12" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Notificar al Interesado</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox16" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Aclarar Redacci&oacute;n </strong></td>
          </tr>
          <tr> 
            <td><strong> 
              <input type="checkbox" name="checkbox17" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Acci&oacute;n</strong></td>
            <td><strong> 
              <input type="checkbox" name="checkbox18" value="checkbox" >
              </strong></td>
            <td class="item"><strong>Archivar</strong></td>
            <td colspan="4"></td>
          </tr>
          <tr> 
            <td colspan="8"><table width="100%" border="0" cellspacing="0">
                <tr> 
                  <td class="item"><strong>Otros y/o Observaciones</strong> </td>
                  <td><textarea name="Observacion" cols="60" rows="3" class="iptxt1">{$Observacion}</textarea></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
      </tr>
	  <tr>
	  <td colspan="2"><hr size="1"></td>
	  </tr>
      <tr align="center"> 
        <td colspan="2"> <input type="submit" name="Submit" value="Guardar Memorando" class="submit"> 
          &nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}');return document.MM_returnValue"> 
          <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_MEMORANDO}"> 
          <input name="menu" type="hidden" id="menu" value="{$menuPager}"> </td>
      </tr>
    </table>
</form>