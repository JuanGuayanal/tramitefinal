{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function SubmitForm7(pForm,pAccion){
	//if(pSel.options[pSel.selectedIndex].value!='none'){
		pForm.accion.value=pAccion
		alert(pAccion);
		pForm.submit()
	//}
}
{/literal}
-->
</script>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="4" class="item">{$errors} </td>
    </tr>
    {/if} <br>
    <tr> <br>
      <td colspan="4" align="center" class="item"> <b>Tipo de Correspondencia</b> 
        <select name="TipoCor" id="TipoCor" class="ipsel1" onChange="submitForm('{$accion.FRM_AGREGA_CORRESPONDENCIA}');">
			{$selTipoCorrespondencia}
		</select>
	  </td>
	</tr>
	{if $TipoCor>0}
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td colspan="2" class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Director:</strong></td>
      <td class="item"><select name="campo1" class="ipsel1" id="campo1">
						{$selCampo1}
				     </select></td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el cargo del funcionario que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if $TipoCor>0}
    <tr> 
      <td class="item"> <strong>{if $TipoCor==1}Acto que se notifica{elseif $TipoCor==2}N&uacute;mero de Documento{/if}</strong></td>
      <td class="item">{if $TipoCor==1} {if ($idNoti>0)}{else}<select name="camp2" class="ipsel1" id="camp2">
	  <option value="none">Seleccione una opci�n</option>
	  <option value="1"{if $camp2==1} selected{/if}>Resoluci�n Suprema</option>
	  <option value="2"{if $camp2==2} selected{/if}>Resoluci�n Ministerial</option>
	  <option value="3"{if $camp2==3} selected{/if}>Resoluci�n Viceministerial</option>
	  <option value="4"{if $camp2==4} selected{/if}>Resoluci�n Directoral</option>
	  <option value="5"{if $camp2==5} selected{/if}>Resoluci�n Secretarial</option>
	  </select>&nbsp;{/if}<input name="campo2" type="text" class="iptxt1" id="campo2" value="{$campo2}" size="50" maxlength="255">
	  {elseif $TipoCor==2}
	  <input name="nroDoc" type="text" class="iptxt1" id="nroDoc" value="{$nroDoc}" size="50" maxlength="255">
	  {/if} 
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el acto que se notifica(resoluci�n, oficio, etc.) y la fecha de expedici�n." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if $TipoCor==1}
    <tr> 
      <td height="25" class="item" ><strong>Nombre y Cargo</strong></td>
      <td class="item"><input name="campo3" type="text" class="iptxt1" id="campo3" value="{$campo3}" size="35" maxlength="255"> 
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el Nombre y Cargo del funcionario que expedi� el acto que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Procedimiento</strong></td>
      <td class="item"><input name="campo4" type="text" class="iptxt1" id="campo4" value="{$campo4}" size="35" maxlength="255">
      </td>
      <td><img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top"></td>
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if $TipoCor>0}
    <tr> 
      <td class="item"><strong><a name="user" id="user"></a>Destinatario</strong> </td>
      <td class="item"><textarea name="destinatario" cols="54" class="iptxt1" id="textarea" >{$destinatario}</textarea></td>
      <td></td>
	  <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Domicilio Procesal</strong> </td>
      <td class="item"> 
        <textarea name="DomProc" cols="54" class="iptxt1" id="DomProc" >{$DomProc}</textarea>
      </td>
	  <td></td>	
      <td colspan="5" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $TipoCor==1}
    <tr> 
      <td class="item" > <strong>Vigencia del Acto Notificado</strong></td>
      <td colspan="3"> <select name="vigencia" class="ipsel1" id="campo1" >
          <option value="none"{if $vigencia=="none"} selected{/if}>Seleccione una 
          opci�n</option>
          <option value="1"{if $vigencia==1} selected{/if}>Desde la fecha de su 
          emisi�n</option>
          <option value="2"{if $vigencia==2} selected{/if}>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"{if $vigencia==3} selected{/if}>Desde el d�a de su notificaci�n</option>
          <option value="4"{if $vigencia==4} selected{/if}>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select> </td>
    </tr>
    <tr> 
      <td class="item"><strong>Agota la v&iacute;a administrativa</strong></td>
      <td colspan="3" class="item"><input name="radio" type="radio" value="1" checked> 
        <strong>Si</strong> <input type="radio" name="radio" value="0"> 
        <strong>No</strong></td>
    </tr>
    <tr> 
      <td rowspan="3" class="item"><strong>Recurso Administrativo:</strong></td>
      <td colspan="3" class="item"><input type="checkbox" name="checkbox" value="1">
        Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td colspan="3" class="item"><input type="checkbox" name="checkbox2" value="1">
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td colspan="3" class="item"><input type="checkbox" name="checkbox3" value="1">
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
	{/if}
    <tr> 
      <td class="item"><strong>N&uacute;mero de folios</strong></td>
      <td colspan="3" class="item"><input name="folio" type="text" class="iptxt1" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="4" maxlength="2"></td>
    </tr>
			{if (($idInt>0)||($idNoti>0))}
		 <tr> 
		  <td class="item"> <strong>Fec. Entrega al Courier</strong></td>
			  <td colspan="3"><select name="dia_ing" class="ipsel2" id="dia_ing">
			  {$selDiaIng}
				</select> <select name="mes_ing" class="ipsel2" id="mes_ing">
			  {$selMesIng}
				</select> <select name="anyo_ing" class="ipsel2" id="anyo_ing">
			  {$selAnyoIng}
				</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que se entrega al Courier." width="20" height="20" align="top"> 
			  </td>
	</tr>
			<tr>
				<td class="item"><strong>Tipo de Mensajer&iacute;a</strong></td>
				<td colspan="3" class="item"><input name="radio2" type="radio" value="1" checked>&nbsp;<strong>Mensajer&iacute;a Local</strong>&nbsp;&nbsp;
				<input type="radio" name="radio2" value="0">&nbsp;<strong>Mensajer&iacute;a Nacional </strong></td>
			</tr>
		{/if}

    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
	{/if}
    <tr align="center"> 
      <td colspan="4"> 
	  	{if $TipoCor>0}	
		<input name="bbSubmit" type="Submit" class="submit" value="{if (($idInt>0)||($idNoti>0))}Entregar al Courier{else}Crear Correspondencia{/if}"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.FRM_BUSCA_NOTIFICACION}&menu={$accion.FRM_BUSCA_NOTIFICACION}&subMenu={$accion.FRM_BUSCA_NOTIFICACION}');return document.MM_returnValue"> 
        {/if}
		<input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_CORRESPONDENCIA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		{if ($idExp>0)}<input name="idExp" type="hidden" value="{$idExp}">{/if}
		{if ($idInt>0)}<input name="idInt" type="hidden" value="{$idInt}">{/if}
		{if ($idNoti>0)}<input name="idNoti" type="hidden" value="{$idNoti}">{/if}
	  </td>
    </tr>
  </table>
</form>