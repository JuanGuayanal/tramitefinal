{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onSubmit="MM_validateForm('nivel1','Nivel 1','R','nivel2','Nivel 2','R','nivel3','Nivel 3','R');return document.MM_returnValue">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Nivel 1</strong></td>
      <td> <textarea name="nivel1" cols="80" rows="3" class="iptxt1" id="nivel1" >{$nivel1}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Nivel 2</strong> </td>
      <td><textarea name="nivel2" cols="80" rows="3" class="iptxt1" id="nivel2" >{$nivel2}</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Nivel 3</strong> </td>
      <td class="item"><textarea name="nivel3" cols="80" rows="3" class="iptxt1" id="nivel3" >{$nivel3}</textarea></td>
      <td><strong></strong></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Guardar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ARCHIVA_DOCDIR}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
        <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="id" type="hidden" id="id" value="{$id}"> 
		 </td>
    </tr>
  </table>
</form>