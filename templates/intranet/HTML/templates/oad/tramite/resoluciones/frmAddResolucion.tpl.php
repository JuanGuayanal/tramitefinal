{$jscript}
<script language="JavaScript">
<!--
{literal}
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('TipoResol','Tipo de Resoluci�n','Sel','sumilla','Sumilla','R','dia_firma','D�a de la Firma de la Resoluci�n','Sel','mes_firma','Mes de la Firma de la Resoluci�n','Sel','anyo_firma','A�o de la Firma de la Resoluci�n','Sel'{/literal}{if $opcion2==1},'campo4','Procedimiento','R','destinatario','Destinatario','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel','DomProc','Domicilio Procesal','R','vigencia','La vigencia del acto notificado','Sel','R','folio','Folio','R'{/if});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong> </td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp;</td>
      <td class="item"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" {if ($GrupoOpciones1==1)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
              <strong>Es respuesta a un doc</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" {if ($GrupoOpciones1==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
              <strong>No es respuesta a un doc</strong></label></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if ($GrupoOpciones1==1)}
    <tr> 
      <td class="texto td-encuesta"><strong>Seleccione:</strong> </td>
      <td class="item">
	  	<label> 
              <input name="GrupoOpciones2" type="radio" value="1" {if ($GrupoOpciones2==1||!$GrupoOpciones2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones2" type="radio" value="2" {if ($GrupoOpciones2==2)} checked {/if} onClick="document.{$frmName}.target='_self';submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
              <strong>Documento Interno</strong></label>
	  
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Digite el {if ($GrupoOpciones2==1||!$GrupoOpciones2)}Nro. de Tr&aacute;mite{else}Nro. de documento{/if}</strong> </td>
      <td class="item">{if ($GrupoOpciones2==1||!$GrupoOpciones2)}
	  			<input name="nroTD" type="text" class="iptxt1" id="nroTD" value="{$nroTD}" size="30">
	      {else}
	  			<select name="tipoDocc" class="ipsel2" id="select">
        			{$selTipoDocc}
				</select>
				<input name="numero2" type="text" class="iptxt1" value="{$numero2}" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipsel2" id="select">
					<option value="none">Todos</option>
					<option value="2004"{if $anyo2==2004} selected{/if}>2004</option>
					<option value="2005"{if $anyo2==2005} selected{/if}>2005</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
				</select> -CONVENIO_SITRADOC/
				<select name="siglasDepe2" class="ipsel2" id="siglasDepe2">
					{$selSiglasDep2}
       			</select>
	  	  {/if}&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submit" onClick="submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{if (($GrupoOpciones1==1||!$GrupoOpciones1)&& $idDocumento>0)}
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA RESOLUCI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Detalles</strong> </td>
      <td class="item">{if $GrupoOpciones2==1}{$ind}<br>{$asunto}<br>
      {$fecRec}{else}{$claseDoc} {$ind}<br>
      {$asunto}<br>{$fecRec}{/if}</td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	{/if}
    <tr> 
      <td class="texto td-encuesta"><strong>Tipo de Resoluci&oacute;n</strong> </td>
      <td><select name="TipoResol" class="ipsel1" id="TipoResol">
        {$selTipoResol}
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Asunto </strong></td>
      <td><textarea name="sumilla" cols="70" rows="4" class="iptxt1" id="textarea" {if !$GrupoOpciones1} disabled{/if}>{$sumilla}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
    	<td class="texto td-encuesta"> <strong>Fec. Inicio</strong></td>
		  <td><select name="dia_ini" class="ipsel2" id="dia_ini">
		  {$selDiaIni}
			</select> <select name="mes_ini" class="ipsel2" id="mes_ini">
		  {$selMesIni}
			</select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
		  {$selAnyoIni}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Inicio." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
	</tr>
		<tr> 
      	  
      <td class="texto td-encuesta"> <strong>Fec. Fin</strong></td>
		  <td ><select name="dia_fin" class="ipsel2" id="dia_fin">
		  {$selDiaFin}
			</select> <select name="mes_fin" class="ipsel2" id="mes_fin">
		  {$selMesFin}
			</select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
		  {$selAnyoFin}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Fin." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  		  
		</tr>
		
	<tr> 
      <td class="texto td-encuesta"> <strong>Fec. Firma</strong></td>
		  <td ><select name="dia_firma" class="ipsel2" id="dia_firma">
		  {$selDiaFirma}
			</select> <select name="mes_firma" class="ipsel2" id="mes_firma">
		  {$selMesFirma}
			</select> <select name="anyo_firma" class="ipsel2" id="anyo_firma">
		  {$selAnyoFirma}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Firma." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>
		
		<tr>     	  
      <td class="texto td-encuesta"> <strong>Fec. Publicaci&oacute;n</strong></td>
		  <td><select name="dia_pub" class="ipsel2" id="dia_pub">
		  {$selDiaPub}
			</select> <select name="mes_pub" class="ipsel2" id="mes_pub">
		  {$selMesPub}
			</select> <select name="anyo_pub" class="ipsel2" id="anyo_pub">
		  {$selAnyoPub}
			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicaci�n." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
		</tr>
	{if (empty($idExp))}
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item"><font color="#FF0000"><strong><span class="item"><font color="#FF0000"><strong><input name="opcion2" type="checkbox" id="checkbox" value="1" {if $opcion2==1} checked {/if} onClick="submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">&iquest;Va a generar Notificaci�n?</strong></font></span></strong></font></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{if $opcion2==1}
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA NOTIFICACI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Procedimiento</strong></span> </td>
      <td class="item"><input name="campo4" type="text" class="iptxt1" id="campo4" value="{$campo4}" size="35" maxlength="255">
	   &nbsp;<img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top">
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Destinatario</strong></span> </td>
      <td class="item"><textarea name="destinatario" cols="70" rows="3" class="iptxt1" id="textarea" >{$destinatario}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Ubigeo</strong></span> </td>
      <td class="item">
	  		<select name="codDepa" class="ipsel2" id="select3" onChange="submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
					{$selDepa}
            </select>
			<select name="codProv" class="ipsel2" id="select2" onChange="submitForm('{$accion.FRM_AGREGA_RESOL_ESP}')">
					{$selProv}
            </select>
			<select name="codDist" class="ipsel2" id="select" >
					{$selDist}
              </select>	  
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Domicilio Procesal</strong></span> </td>
      <td class="item"><textarea name="DomProc" cols="70" rows="3" class="iptxt1" id="DomProc" >{$DomProc}</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Vigencia del Acto Notificado</strong></span> </td>
      <td class="item"><select name="vigencia" class="ipsel1" id="campo1" >
          <option value="none"{if $vigencia=="none"} selected{/if}>Seleccione una 
          opci�n</option>
          <option value="1"{if $vigencia==1} selected{/if}>Desde la fecha de su 
          emisi�n</option>
          <option value="2"{if $vigencia==2} selected{/if}>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"{if $vigencia==3} selected{/if}>Desde el d�a de su notificaci�n</option>
          <option value="4"{if $vigencia==4} selected{/if}>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Agota la v&iacute;a administrativa </strong></span> </td>
      <td class="item"><input name="radio" type="radio" value="1" checked> 
        <strong>Si</strong> <input type="radio" name="radio" value="0"> 
        <strong>No</strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td rowspan="3" class="texto td-encuesta"><span class="item"><strong>Recurso Administrativo</strong></span> </td>
      <td class="item"><input type="checkbox" name="checkbox" value="1">
        Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
      <td colspan="3" rowspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr>
      <td class="item"><input type="checkbox" name="checkbox2" value="1">
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr>
      <td class="item"><input type="checkbox" name="checkbox3" value="1">
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>N&uacute;mero de Folios</strong></span> </td>
      <td class="item"><input name="folio" type="text" class="iptxt1" onKeyPress="solo_num();" value="{if $folio==''}1{else}{$folio}{/if}" size="4" maxlength="2"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	{/if}
	{/if}		
		
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Agregar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_RESOL_ESP}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
        {if ($idDocumento>0&& ($GrupoOpciones1==1||!$GrupoOpciones1))} 
        <input name="idDocumento" type="hidden" id="idDocumento" value="{$idDocumento}">
        {/if} 
		</td>
    </tr>
  </table>
</form>