{literal}
<script language="JavaScript">
function MinReq(pFrm){
	if(pFrm.elements['ids[]'].length!=undefined){
		for(var i=0;i<pFrm.elements['ids[]'].length;i++)
			if(pFrm.elements['ids[]'][i].checked)
				return true
	}else{
		if(pFrm.elements['ids[]'].checked)
			return true
	}
	return false
}
function SubmitForm3(pFrm,pAcc,pMenu){
	if(MinReq(pFrm)){
		pFrm.accion.value=pAcc
		pFrm.menu.value=pMenu
		//pFrm.menu.value='SumarioDir'
		//pFrm.subMenu.value=pAcc
		pFrm.submit()
	}else
		alert('Debe seleccionar al menos un Documento')
}
</script>
{/literal}
<form name="{$frmName}" action="{$frmUrl}" method="post">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DOC_EXTENDIDO}">
	<input type="hidden" name="menu">
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td height="20" align="right" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr> 
          <td align="right" valign="middle" class="textogray">{if $arrAte}<a href="javascript:SubmitForm3(document.{$frmName},'{$accion.FRM_ACEPTA_DOC}','{$accion.FRM_BUSCA_ARCHIVO}')"><img src="/img/ico_suge.gif" width="18" height="18" border="0" align="absmiddle" alt="[ ACEPTAR ]"><strong>OK?</strong></a>{/if}</td>
          <td align="right" valign="middle" class="textogray"><strong>Resultados:</strong>&nbsp; 
            {if $outTotal} Del {$outStart} al {$outEnd} de {$outTotal} coincidencias&nbsp;&nbsp; 
            {else} No se encontraron coincidencias&nbsp;&nbsp; {/if}</td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td> {section name="i" loop=$arrAte} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="27" align="center" valign="middle">&nbsp;<a href="javascript: window.open('{$frmUrl}?accion={$accion.MUESTRA_DETALLE_FLUJODOCDIR}&id={$arrAte[i].idDocPadre}','window','menubar=0,toolbar=0,scrollbars=yes,location=0,directories=0,status=0,resizable=0,top=0,left=0,width=790,height=600'); void('')" onMouseOver="window.status=''"><img src="/img/800x600/ico-info3.gif" alt="[ VER DETALLES ]" width="15" height="15" hspace="5" border="0"></a></td>
          <td colspan="2" valign="top" class="item"><table width="100%">
              <tr>
                <td valign="top" class="item"><strong>Dependencia</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].sigla|default:'No 
                  especificado'} - {$arrAte[i].depe}</td>
                <td rowspan="6" align="right" class="texto">{if (!$arrAte[i].fecAcept || $arrAte[i].fecAcept=="")}<input name="ids[]" type="checkbox" id="ids[]" value="{$arrAte[i].id}" onClick="hL(this)">{/if}</td>
              </tr>
              <tr>
                <td valign="top" class="sub-item"><strong>Indicativo</strong></td>
                <td class="texto"><strong>:{$arrAte[i].indicativo|default:'No especificado'}&nbsp;&nbsp;</strong></td>
              </tr>
              <tr> 
                <td valign="top" class="sub-item"><strong>Nivel 1</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].nivel1|default:'No especificado'}</td>
              </tr>
			  {if $arrAte[i].nivel2!=''}
              <tr> 
                <td valign="top" class="sub-item"><strong>Nivel 2</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].nivel2|default:'No 
                  especificado'}</td>
              </tr>
			  {/if}
			  {if $arrAte[i].nivel3!=''}
              <tr> 
                <td valign="top" class="sub-item"><strong>Nivel 3</strong></td>
                <td class="texto"><strong>:</strong>{$arrAte[i].nivel3|default:'No 
                  especificado'}</td>
              </tr>
			  {/if}
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Observaciones</strong></td>
                <td class="texto"><strong>:{$arrAte[i].obs|default:'No especificado'}</strong></td>
              </tr>
			  {if ($arrAte[i].fecAcept && $arrAte[i].fecAcept!="")}
              <tr> 
                <td width="100" valign="top" class="sub-item"><strong>Fecha de aceptaci&oacute;n</strong></td>
                <td class="texto"><strong>:{$arrAte[i].fecAcept|default:'No especificado'}</strong></td>
              </tr>
			  {/if}		  
            </table></td>
        </tr>
      </table>
      {/section} 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {if $menuSearchPaginable} 
        <tr> 
          <td align="right" class="textogray">{$menuSearchPaginable}</td>
        </tr>
        <tr> 
          <td><hr width="100%" size="1"></td>
        </tr>
        {/if} </table></td>
  </tr>
</table>
</form>