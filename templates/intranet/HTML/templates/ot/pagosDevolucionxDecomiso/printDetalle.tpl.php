<html>
<head>
<title>Sistema de Pagos de Devoluci�n por Decomisos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sitra {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #003366;
	background-color: #FEF7C1;
	border: 1px #E9E9E9;
}
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.IMPRIME_DETALLE}&id={$id}]&print=1"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1">
        <tr align="center"> 
          <td height="50" colspan="6" class="textoblack"><strong><img src="/img/ico-detalle.gif" width="24" height="24" hspace="4" align="absmiddle"><u>ESTADO 
            DE CUENTA</u></strong> <!--<br> <strong>{$nroExp}&nbsp;&nbsp;[{$folio} 
            Folios]</strong>--></td>
        </tr>
        <tr style="height: 15px"> 
          <td width="25%" colspan="2" class="textoblack"><strong>Embarcaci&oacute;n</strong></td>
          <td class="texto" width="25%">&nbsp;{$embarcacion}</td>
          <td class="texto" width="25%"><strong>Fecha de dep&oacute;sito</strong></td>
          <td colspan="2" width="25%" class="texto">{$fecDeposito}</td>
        </tr>
        <tr bgcolor="#F2F0F4">  
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<tr style="height: 15px"> 
          <td colspan="2" class="textoblack"><strong>Nro. de matr&iacute;cula </strong></td>
          <td class="texto">&nbsp;{$matricula}</td>
          <td class="texto"><strong>Documento</strong></td>
          <td colspan="2" class="texto">{$documento}</td>
        </tr>        
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr style="height: 15px"> 
          <td width="135" colspan="2" class="textoblack"><strong>Propietario</strong></td>
          <td colspan="4" class="texto">&nbsp;{$armador}</td>
        </tr>
		{if $flag=="P"}
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr style="height: 15px"> 
          <td width="135" colspan="2" class="textoblack"><strong>Nro. comprobante</strong></td>
          <td colspan="4" class="texto">&nbsp;{$nroComprobante}</td>
        </tr>		
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr style="height: 15px"> 
          <td width="135" height="14" colspan="2" class="textoblack"><strong>Registro SIAF</strong></td>
          <td colspan="4" class="texto">&nbsp;{$nroSiaf}</td>
        </tr>
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr style="height: 15px"> 
          <td width="135" colspan="2" class="textoblack"><strong>Fecha de Pago</strong></td>
          <td class="texto">&nbsp;{$fechaPago}</td>
          <td class="texto" width="25%"><strong>Resoluci&oacute;n</strong></td>
          <td colspan="2" width="25%" class="texto">{$nroResolucion}</td>		  
        </tr>		
		{/if}
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="6" style="height: 25px" class="item-sitra" align="center"><strong>DETALLES DE LOS INTERESES&nbsp;<font color="#FF0000">{if $flag=="A"}En curso{else}Devuelto{/if}</font></strong></td>
        </tr>
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<tr bgcolor="#F2F0F4"> 
          <td colspan="3" background="/img/apps/bg_horiz.gif" class="item"><strong>Monto depositado: {$monto}</strong></td>
          <td colspan="3" background="/img/apps/bg_horiz.gif" class="item"><strong>{if $flag=="A"}Monto a pagar{else}Monto Pagado{/if}: {$montoxPagar}</strong></td>
		</tr>
        <tr>
		  <td colspan="6"><table width="100%">
		  <tr>
		  <td class="textoblack" width="5%"><strong>N�</strong></td>
          <td class="textoblack" width="25%"><strong>Tasa Anual</strong></td>
		  <!--<td class="textoblack" width="25%"><strong>Tasa Diaria</strong></td>-->
          <td width="25%" class="texto"><strong>Fecha</strong></td>
          <td width="25%" class="texto"><strong>Interes ganado</strong></td>
		  <td width="25%" class="texto" align="center"><strong>Nuevo monto</strong></td>
		  </tr>
		  </table></td>
        </tr>							
        {section name=i loop=$interes} 
        <tr>
		  <td colspan="6"><table width="100%">
		  <tr>
		  <td class="textoblack" width="5%"><strong>{$smarty.section.i.iteration}</strong></td>
          <td width="25%" class="texto">&nbsp;{$interes[i].tasa format="%.1f"}</td>
		  <!--<td width="25%" class="texto">&nbsp;{$interes[i].factor format="%.15f"}</td>-->
          <td width="25%" class="texto">{$interes[i].fecTasa|default:'No especificado'}</td>
          <td width="25%" class="texto">{$interes[i].interes2|default:'No especificado'}</td>          
          <td width="25%" class="texto">&nbsp;{$interes[i].interes|default:'No especificado'}</td>
		  </tr>
		  </table></td>
        </tr>
		<tr bgcolor="#F2F0F4"> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>		
        {/section} 
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="11" class="textoblack"> <b><u>INFORMES:</u></b><br>
            Oficina de Tecnolog&iacute;a de la Informaci&oacute;n OGTIE - Ministerio 
            de la Producci&oacute;n<br> 
            <b>Telf. 616-2222 Anexo 680-331 </b><br>
            Email:<b> <a href="mailto:sitradoc@CONVENIO_SITRADOC.gob.pe">sitradoc@CONVENIO_SITRADOC.gob.pe</a></b><br>          </td>
        </tr>
        <tr> 
          <td colspan="11" class="textoblack"> <div align="right"><em class="textored"><strong>Actualizado 
              al {$fechaGen}&nbsp;{$HoraActual}</strong> </em></div></td>
        </tr>
        <tr> 
          <td colspan="11" align="right">&nbsp;</td>
        </tr>
      </table>
	</td>
  </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
{literal}
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
{/literal}
</body>
</html>