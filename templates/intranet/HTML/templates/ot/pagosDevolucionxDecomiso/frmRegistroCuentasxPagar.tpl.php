{$jscript}
<script language="JavaScript" type="text/javascript" src="/js/calendarFuturo.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode!=46) {
			event.returnValue = false;
	}
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('embarcacion','Embarcaci�n','Sel','armador','Propietario','R','matriculaEmb','Matr�cula','R','moneda','Tipo de moneda','Sel','monto','Monto','R','desFechaIni','Fecha de Custodia','R','tipoDocc','Clase de documento','Sel','numero2','N�mero','R','anyo2','Anyo','Sel','siglasDepe2','Siglas','Sel'{/literal});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Fecha</strong> </td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Digite la E/P o matr&iacute;cula </strong>  </td>
      <td class="item" align="left"><input name="RZoRUC" type="text" class="iptxtn" value="{$RZoRUC}" size="30" maxlength="50">
	  &nbsp;&nbsp;<input type="button" name="BuscaRZ" value="Buscar" class="submitV2" onClick="submitForm('{$accion.FRM_REGISTRA_DECOMISO}')"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Embarcaci&oacute;n </strong> </td>
      <td class="item" align="left"><select name="embarcacion" class="ipseln" id="embarcacion" onChange="submitForm('{$accion.FRM_REGISTRA_DECOMISO}')">
			{$selEmbarcacion}
              </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto" align="left"><strong>Propietario</strong></td>
      <td class="td-encuesta texto" align="left"><input name="armador" type="text" class="iptxtn" id="armador" value="{$armador}" size="40" maxlength="20" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Matr&iacute;cula</strong> </td>
      <td class="item" align="left"><input name="matriculaEmb" type="text" class="iptxtn" id="matriculaEmb" value="{$matriculaEmb}" size="20" maxlength="20" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto" align="left"><strong>Monto</strong></td>
      <td align="left"><select name="moneda" class="ipseln" id="moneda">
        {$selMoneda}
        </select>&nbsp;&nbsp;<input name="monto" type="text" class="iptxtn" id="monto" value="{$monto}" size="18" maxlength="15" onkeypress="solo_num();"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr>     	  
      <td class="texto td-encuesta" align="left"><strong>Documento</strong></td>
		  <td align="left"><select name="tipoDocc" class="ipseln" id="select">
        			{$selTipoDocc}
			  </select>
			  <input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="5" onkeypress="solo_num();">
				 -
			  <select name="anyo2" class="ipseln" id="select">
					<option value="none">Escoja</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo2==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo2==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo2==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo2==2011} selected{/if}>2011</option>	
					<option value="2012"{if $anyo2==2012} selected{/if}>2012</option>		
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>									
			  </select> -MIDIS/
			  <select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       		  </select>	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  
	</tr>
	<tr> 
      <td class="texto td-encuesta" align="left"> <strong>Fecha Dep&oacute;sito </strong></td>
	  <td align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" size="12" value="{if !$desFechaIni}{$hoy}{else}{$desFechaIni}{/if}" tabindex="4" onKeyPress="ninguna_letra();" />
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div>	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Registrar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.REGISTRA_DECOMISO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">		</td>
    </tr>
  </table>
</form>