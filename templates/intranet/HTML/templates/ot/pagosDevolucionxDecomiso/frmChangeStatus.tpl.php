{$jscript}
<script language="JavaScript" type="text/javascript" src="/js/calendarFuturo.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode!=46) {
			event.returnValue = false;
	}
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('tipResol','Tipo resoluci�n','Sel','numero2','N�mero','R','anyo2','Anyo','Sel','siglasDepe2','Siglas','Sel'{/literal});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Fecha</strong> </td>
      <td class="item" align="left">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong>

<input name="nroComprobante" type="hidden" class="iptxtn" id="nroComprobante" value="{$nroComprobante}">
<input name="nroSIAF" type="hidden" class="iptxtn" id="nroSIAF" value="{$nroSIAF}">
</strong></div></td>
    </tr>


	<tr>     	  
      <td class="texto td-encuesta" align="left"><strong>Resoluci&oacute;n</strong></td>
		  <td align="left"><select name="tipResol" class="ipseln" id="tipResol">
        			{$selTipoResol}
			  </select>
			  <input name="numero2" type="text" class="iptxtn" value="{$numero2}" size="5" maxlength="5" onkeypress="solo_num();">
				 -
			  <select name="anyo2" class="ipseln" id="select">
					<option value="none">Escoja</option>
					<option value="2006"{if $anyo2==2006} selected{/if}>2006</option>
					<option value="2007"{if $anyo2==2007} selected{/if}>2007</option>
					<option value="2008"{if $anyo2==2008} selected{/if}>2008</option>
					<option value="2009"{if $anyo2==2009} selected{/if}>2009</option>
					<option value="2010"{if $anyo2==2010} selected{/if}>2010</option>
					<option value="2011"{if $anyo2==2011} selected{/if}>2011</option>				
					<option value="2012"{if $anyo2==2012} selected{/if}>2012</option>
					<option value="2013"{if $anyo2==2013} selected{/if}>2013</option>					
			  </select> -MIDIS/
			  <select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					{$selSiglasDep2}
       		  </select>	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  
	</tr>
	<tr> 
      <td class="texto td-encuesta" align="left"> <strong>Fecha de Corte del
	<br>Interes seg�n resoluci�n </strong></td>
	  <td align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" size="12" value="{if !$desFechaIni}{$hoy}{else}{$desFechaIni}{/if}" tabindex="4" onKeyPress="ninguna_letra();" />
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div>
				Se considerar&aacute; el inter&eacute;s hasta un d�a antes de la fecha de corte. </td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>	


	<tr> 
		<td class="texto td-encuesta" align="left"> <strong>Se paga a:</strong></td>
		<td align="left">
			Empresa<input 		name="xpagoa" type="radio" value="1" onClick="{$frmName}.xpagoreal.value=1;" {if $xpagoreal==1} checked {/if} >&nbsp;&nbsp;
			CONVENIO_SITRADOC(RDR)<input 	name="xpagoa" type="radio" value="2" onClick="{$frmName}.xpagoreal.value=2;" {if $xpagoreal==2} checked {/if} >&nbsp;
			<input type="hidden" name="xpagoreal" value="{$xpagoreal}" >
		</td>
		<td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>	

    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Guardar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.CAMBIA_ESTADO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		<input name="id" type="hidden" id="id" value="{$id}">		
	  </td>
    </tr>
  </table>
</form>