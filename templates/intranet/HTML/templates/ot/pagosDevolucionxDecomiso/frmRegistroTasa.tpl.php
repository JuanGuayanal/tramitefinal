<link rel="stylesheet" href="/styles/intranet.css" type="text/css" />
<script src="/js/intranet.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/JSCookMenu.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/theme.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/mambojavascript.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/tiny_mce.js" language="JavaScript1.2" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="/js/calendarFuturo.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
{literal}
function CalendarHide(){
	MM_showHideLayers('popCalIni','','hide')
	MM_showHideLayers('popCalFin','','hide')
}

document.onclick = CalendarHide

function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode!=46) {
			event.returnValue = false;
	}
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm('�Desea Guardar este Registro?')){
		MM_validateForm('tasa','Tasa','R','desFechaIni','Fecha','R'{/literal});
		return document.MM_returnValue;{literal}
	}else
		return false;
}
 {/literal}
-->
</script>
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" onsubmit="return ConfirmaRegistro(document.{$frmName})">
  <table width="300" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Fechas</strong> </td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Monto</strong></td>
      <td><input name="tasa" type="text" class="iptxtn" id="tasa" value="{$tasa}" size="18" maxlength="15" onkeypress="solo_num();"></td><td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="texto td-encuesta"> <strong>Fecha</strong></td>
	  <td><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" size="12" value="{if !$desFechaIni}{$hoy}{else}{$desFechaIni}{/if}" tabindex="4" onKeyPress="ninguna_letra();" />
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.{$frmName}.desFechaIni,document.{$frmName}.desFechaIni,popCalIni,'{php}echo date('Y'){/php}','{php}echo date('n'){/php}','{php}echo date('d'){/php}');return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a>
				<div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div>	  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Registrar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.FRM_REGISTRA_TASA}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}"> 
		<input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
		<input name="valor" type="hidden" id="valor" value="{$valor}">
		<input name="Buscar" type="hidden" id="Buscar" value="1">		
	</td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>&nbsp;&nbsp;</strong></td>
      <td></td><td colspan="3" class="item"></td>
    </tr>	
    <tr> 
      <td class="td-encuesta texto"><strong>Tasas</strong></td>
      <td></td><td colspan="3" class="item"></td>
    </tr>	
	{section name=i loop=$arrCuenta} 
    <tr> 
      <td class="td-encuesta texto"><strong>&nbsp;</strong></td>
      <td>{$arrCuenta[i].fechaTasa}: {$arrCuenta[i].tasaAnual}</td><td colspan="3" class="item"></td>
    </tr>
	{/section}	
  </table>
</form>