<script language="JavaScript">
function DisableHour(check){literal}{{/literal}
	valCheck = (check.checked) ? true : false
	document.{$frmName}.ora_ini.disabled = valCheck
	document.{$frmName}.min_ini.disabled = valCheck
	document.{$frmName}.ora_fin.disabled = valCheck
	document.{$frmName}.min_fin.disabled = valCheck
{literal}}{/literal}
</script>
{include file='calendario/botonera.tpl.php'}
<form name="{$frmName}" method="post" action="{$frmUrl}">
  <table width="531" border="0" cellspacing="3" cellpadding="0">
    <tr> 
      <td colspan="3" class="textoblack"><strong>NUEVA ACTIVIDAD</strong></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr>
      <td colspan="3">{$errors}</td>
    </tr>
    <tr> 
      <td rowspan="3"><img src="/img/800x600/calendario/ico-actividad.gif" width="36" height="36" hspace="2"></td>
      <td class="item"><strong>Asunto</strong></td>
      <td><input name="asunto" type="text" class="iptxt1" id="asunto" value="{$desAsunto}" size="60"></td>
    </tr>
    <tr> 
      <td class="item">Ubicaci&oacute;n</td>
      <td><input name="ubicacion" type="text" class="iptxt1" id="ubicacion" value="{$desUbicacion}" size="60"></td>
    </tr>
    <tr> 
      <td valign="top" class="p-up-2 item">Descripci&oacute;n</td>
      <td><textarea name="actividad" cols="59" rows="4" class="iptxt1" id="descripcion">{$desActividad}</textarea></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td width="40" rowspan="2"><img src="/img/800x600/calendario/ico-clock.gif" width="36" height="36" hspace="2"></td>
      <td class="item"> <strong>Inicio</strong></td>
      <td valign="bottom"> <select name="mes_ini" class="ipsel1" id="mes_ini">
          {$selMesIni}
        </select> <select name="dia_ini" class="ipsel1" id="dia_ini">
          {$selDiaIni}
        </select> <select name="anyo_ini" class="ipsel1" id="anyo_ini">
          {$selAnyoIni}
        </select> <img src="/img/800x600/calendario/ico-cal.gif" width="16" height="16" align="absbottom"> 
        <select name="ora_ini" class="ipsel1" id="ora_ini">
          {$selHoraIni}
        </select> <select name="min_ini" class="ipsel1" id="min_ini">
          {$selMinIni}
        </select></td>
    </tr>
    <tr> 
      <td class="item"><strong>Fin</strong></td>
      <td> <select name="mes_fin" class="ipsel1" id="mes_fin">
          {$selMesFin}
        </select> <select name="dia_fin" class="ipsel1" id="dia_fin">
          {$selDiaFin}
        </select> <select name="anyo_fin" class="ipsel1" id="anyo_fin">
          {$selAnyoFin}
        </select> <img src="/img/800x600/calendario/ico-cal.gif" width="16" height="16" align="absbottom"> 
        <select name="ora_fin" class="ipsel1" id="ora_fin">
          {$selHoraFin}
        </select> <select name="min_fin" class="ipsel1" id="min_fin">
          {$selMinFin}
        </select> </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td class="item">&nbsp;</td>
      <td class="sub-item"><input name="diaEntero" type="checkbox" value="1" onClick="DisableHour(this)"{if $indDiaEntero} checked{/if}>
        Todo el d&iacute;a</td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/img/800x600/calendario/ico-person.gif" width="12" height="12"></td>
      <td class="item"><strong>Participantes</strong></td>
      <td class="sub-item"> <input name="grupal" type="checkbox" value="1"{if $indGrupal} checked{/if}>
        Otras personas participar&aacute;n en esta Actividad</td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td align="center" valign="top"><img src="/img/800x600/calendario/ico-lock.gif" width="16" height="16" vspace="3"></td>
      <td valign="top" class="p-up-2 item"><strong>Privado</strong></td>
      <td class="sub-item"> <input name="privado" type="checkbox" value="1"{if $indPrivado} checked{/if}>
        Marcar como privado<br> <span class="textogray"><em>Marque esta opci&oacute;n 
        si no quiere que la actividad sea vista por las personas a las cuales 
        comparte su calendario</em></span></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input type="submit" class="ipbot1" onClick="MM_validateForm('asunto','Asunto','R','dia_ini,mes_ini,anyo_ini','Fecha de Inicio','Date','dia_fin,mes_fin,anyo_fin','Fecha de Termino','Date','ora_ini,min_ini','Hora de Inicio','Hour','ora_fin,min_fin','Hora de Termino','Hour','dia_ini,mes_ini,anyo_ini,ora_ini,min_ini,dia_fin,mes_fin,anyo_fin,ora_fin,min_fin,diaEntero','El Inicio debe ser menor que el Fin','datetimeMen');return document.MM_returnValue" value="Agregar Actividad"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.ACTIVIDAD_PARTICIPANTES}">
        <input name="date" type="hidden" id="date" value="{$date}"></td>
    </tr>
  </table>
</form>