<table border="0" cellpadding="0" cellspacing="1" width="150">
  <tr>
    <td bgcolor="#CCCCCC">
      <table border="0" cellpadding="0" cellspacing="1">
        <tr>
          <td colspan="7" valign="middle" align="center">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="21" align="center"><a href="{$appUrl}?accion={$accion.VISTA_SEMANAL}&date={$mesAnt}"><img src="/img/800x600/cal_prev_black.gif" width="16" height="15" border="0"></a></td>
                <td align="center" class="textoblack"><b>{$tituloCal}</b></td>
                <td width="21" align="center"><a href="{$appUrl}?accion={$accion.VISTA_SEMANAL}&date={$mesPos}"><img src="/img/800x600/cal_next_black.gif" width="16" height="15" border="0"></a></td>
              </tr>
            </table> 
          </td> 
        </tr>
        <tr>
        {section name=i loop=$wdays}
          <td valign="middle" align="center" width="21" class="textoblack"><b>{$wdays[i]}</b></td>
        {/section}
        </tr>
		<tr>
        {section name=padIni loop=$daysAnt}
          <td bgcolor="#f7f7f7" valign="middle" align="center" class="textogray" height="16">{$daysAnt[padIni]}</td>
        {/section}
        {section name=cal loop=$numDays start=1}
          {if $daysPad >= 7}
        </tr>
        <tr>
            {assign var="daysPad" value=0}
      	  {/if}
          {if $smarty.section.cal.index == $today}
          <td bgcolor="#FFB2B2" valign="middle" align="center" class="bdr-1px texto" bordercolor="#FF0000" height="16"><b><a href="{$frmUrl}?accion={$accion.VISTA_DIARIA}&date={$month}/{$smarty.section.cal.index|string_format:"%02d"}/{$year}">{$smarty.section.cal.index}</a></b></td>
		  {elseif $smarty.section.cal.index == $dayCal}
		  <td bgcolor="#FFFF99" valign="middle" align="center" class="bdr-1px texto" bordercolor="#FF0000" height="16"><b><a href="{$frmUrl}?accion={$accion.VISTA_DIARIA}&date={$month}/{$smarty.section.cal.index|string_format:"%02d"}/{$year}">{$smarty.section.cal.index}</a></b></td>
          {else} 
          <td bgcolor="#FFFF99" valign="middle" align="center" class="texto" height="16"><a href="{$frmUrl}?accion={$accion.VISTA_DIARIA}&date={$month}/{$smarty.section.cal.index|string_format:"%02d"}/{$year}">{$smarty.section.cal.index}</a></td>
          {/if}
          {math equation="daysPad+1" daysPad=$daysPad assign=daysPad}
        {/section}
        {section name=padFin loop=$daysPos}
          {if $daysPad >= 7}
        </tr>
        <tr>
            {assign var="daysPad" value=0}
      	  {/if}
          <td bgcolor="#f7f7f7" valign="middle" align="center" class="textogray" height="16">{$daysPos[padFin]}</td>
		  {math equation="daysPad+1" daysPad=$daysPad assign=daysPad}
        {/section}
        </tr>
      </table>
	</td>
  </tr>
</table>
