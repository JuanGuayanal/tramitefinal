{include file='calendario/botonera.tpl.php'}
<br>
<table width="531" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#CCCCCC"><table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr> 
          <td colspan="2" rowspan="{if $todoDia and $diaAnterior}3{elseif $todoDia or $diaAnterior}2{else}1{/if}" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          <td align="center"><table width="100" border="0" cellspacing="0" cellpadding="4">
              <tr> 
                <td width="20"><a href="{$frmUrl}?accion={$accion.VISTA_DIARIA}&date={$fechaAnt}"><img src="/img/800x600/cal_prev_black.gif" width="16" height="15" border="0"></a></td>
                <td align="center" class="item" nowrap><strong>{$fecha}</strong></td>
                <td width="20"><a href="{$frmUrl}?accion={$accion.VISTA_DIARIA}&date={$fechaPos}"><img src="/img/800x600/cal_next_black.gif" width="16" height="15" border="0"></a></td>
              </tr>
            </table></td>
        </tr>
		{if $todoDia}
        <tr> 
          <td bgcolor="#66FFFF" class="texto"> {section name=todo loop=$todoDia} 
            - <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$todoDia[todo].id}&date={$date}">{$todoDia[todo].asunto}</a> 
            {$todoDia[todo].time}.<br>
		  {/section}
		  </td>
		</tr>
		{/if}
		{if $diaAnterior}
        <tr> 
          <td bgcolor="#F7F7F7" class="texto"> {section name=ant loop=$diaAnterior} 
            - <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$diaAnterior[ant].id}&date={$date}">{$diaAnterior[ant].asunto}</a> 
            {$diaAnterior[ant].time}.<br> 
		  {/section}
		  </td>
		</tr>
		{/if}
		{foreach key=acti_k from=$actividades item=acti}
        <tr> 
          <td rowspan="2" width="10" align="center" bgcolor="{if $acti_k<$horaIni or $acti_k>$horaFin}#F7F7F7{else}#EBEBEB{/if}" class="textoblack14"><strong>{$acti_k|date_format:"%I"}</strong></td>
          <td nowrap width="50" bgcolor="{if $acti_k<$horaIni or $acti_k>$horaFin}#F7F7F7{else}#EBEBEB{/if}" class="textoblack">:00 
            {if $acti_k>11}pm{else}am{/if}</td>
          <td bgcolor="{if $acti_k<$horaIni or $acti_k>$horaFin}#FFFFCE{else}#FFFF99{/if}" class="texto">
		  {foreach from=$acti[1] item=actiAnt}
            {if $actiAnt}
			- <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$actiAnt.id}&date={$date}">{$actiAnt.asunto}</a> ({$actiAnt.time}).<br> 
		    {else}
			&nbsp;
			{/if}
		  {/foreach}
		  </td>
        </tr>
        <tr> 
          <td nowrap bgcolor="{if $acti_k<$horaIni or $acti_k>$horaFin}#F7F7F7{else}#EBEBEB{/if}" class="textoblack">:30 {if $acti_k>11}pm{else}am{/if}</td>
          <td bgcolor="{if $acti_k<$horaIni or $acti_k>$horaFin}#FFFFCE{else}#FFFF99{/if}" class="texto"> 
            {foreach from=$acti[2] item=actiDes} {if $actiDes} - <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$actiDes.id}&date={$date}">{$actiDes.asunto}</a> 
            ({$actiDes.time}).<br> 
		    {else}
			&nbsp;
			{/if}
		  {/foreach}
		  </td>
        </tr>
		{/foreach}
      </table>
	</td>
  </tr>
</table>