{$script}
{include file='calendario/botonera.tpl.php'}
<form name="{$frmName}" method="post" action="{$frmUrl}">
  <table width="531" border="0" cellspacing="3" cellpadding="0">
    <tr> 
      <td colspan="3" class="textoblack"><strong>ACTIVIDAD</strong></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="3">{$errors}</td>
    </tr>
    <tr> 
      <td rowspan="{if $act.ubicacion and $act.actividad}3{elseif $act.ubicacion or $act.actividad}2{else}0{/if}"> 
        <img src="/img/800x600/calendario/ico-actividad.gif" width="36" height="36" hspace="2"> 
      </td>
      <td width="80" class="item"><strong>Asunto</strong></td>
      <td class="texto"><strong>{$act.asunto}</strong></td>
    </tr>
    {if $act.ubicacion} 
    <tr> 
      <td class="p-up-2 item">Ubicaci&oacute;n</td>
      <td class="p-up-2 texto">{$act.ubicacion}</td>
    </tr>
    {/if} {if $act.actividad} 
    <tr> 
      <td valign="top" class="p-up-2 item">Descripci&oacute;n</td>
      <td class="texto">{$act.actividad}</td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td width="40" rowspan="{if $act.fin}2{else}0{/if}"><img src="/img/800x600/calendario/ico-clock.gif" width="36" height="36" hspace="2"></td>
      <td class="item"><strong>Inicio</strong></td>
      <td class="sub-item">{$act.inicio}</td>
    </tr>
    {if $act.fin} 
    <tr> 
      <td class="item"><strong>Fin</strong></td>
      <td class="sub-item">{$act.fin}</td>
    </tr>
    {/if} 
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td rowspan="4" align="center"><img src="/img/800x600/calendario/ico-person.gif" width="12" height="12"></td>
      <td valign="top" class="gerundio
	  item"><strong>Participantes</strong></td>
      <td class="sub-item"> {section name=i loop=$parts}{$parts[i]}<br>
        {/section} </td>
    </tr>
    <tr> 
      <td colspan="2" class="item"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Asignado por</strong></td>
      <td class="textogray">{$act.creador}</td>
    </tr>
    <tr> 
      <td class="item"><strong>Asignado el</strong></td>
      <td class="textogray">{$act.fecCreado}</td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td align="center" valign="top"><img src="/img/800x600/calendario/ico-lock.gif" width="16" height="16" vspace="3"></td>
      <td valign="top" class="p-up-2 item"><strong>Privado</strong></td>
      <td class="sub-item"> {if $act.privado} Esta actividad s&oacute;lo es vista 
        por Ud. {else} Esta actividad es vista por todas las personas a las cuales 
        comparte su calendario. {/if} </td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
	{if $showSubmit}
    <tr> 
      <td colspan="3" align="center"> <input type="submit" class="ipbot1" value="Modificar Actividad"> 
        <input name="accion" type="hidden" value="{$accion.MODIFICA_ACTIVIDAD}"> 
        <input name="idAct" type="hidden" value="{$act.id}">
		<input name="date" type="hidden" value="{$date}">
	  </td>
    </tr>
	{/if}
  </table>
</form>