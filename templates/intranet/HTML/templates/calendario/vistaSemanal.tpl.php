{include file='calendario/botonera.tpl.php'}
<br>
<table width="531" border="0" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
  <tr align="center" bgcolor="#E6FFFF"> 
    <td colspan="2" class="item pad2"> 
      <table width="100" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="20"><a href="{$frmUrl}?accion={$accion.VISTA_SEMANAL}&date={$fechaAnt}"><img src="/img/800x600/cal_prev_black.gif" width="16" height="15" hspace="5" vspace="2" border="0"></a></td>
          <td align="center" class="textoblack" nowrap><strong>Semana del {$diaIni} 
            al {$diaFin}</strong></td>
          <td width="20"><a href="{$frmUrl}?accion={$accion.VISTA_SEMANAL}&date={$fechaPos}"><img src="/img/800x600/cal_next_black.gif" width="16" height="15" hspace="5" vspace="2" border="0"></a></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="50%" align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[1].fecha}&nbsp;</strong></td>
    <td width="50%" align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[4].fecha}</strong></td>
  </tr>
  <tr> 
    <td height="70" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[1].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[1].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
    <td height="70" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[4].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[4].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[2].fecha}&nbsp;</strong></td>
    <td align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[5].fecha}</strong></td>
  </tr>
  <tr> 
    <td height="70" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[2].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[2].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
    <td height="70" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[5].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[5].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[3].fecha}&nbsp;</strong></td>
    <td align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[6].fecha}&nbsp;</strong></td>
  </tr>
  <tr> 
    <td height="70" rowspan="3" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[3].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[3].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
    <td height="30" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[6].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[6].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td align="right" bgcolor="#EBEBEB" class="item pad2"><strong>{$semana[0].fecha}&nbsp;</strong></td>
  </tr>
  <tr> 
    <td height="30" valign="top" bgcolor="#EBEBEB"> <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">
        {foreach from=$semana[0].todoDia item=acti} 
        <tr> 
          <td align="center" bgcolor="#FFFFFF">{if substr($acti.hora,0,1)=='a'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}<a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a>{if 
            substr($acti.hora,0,1)=='d'}<img src="/img/800x600/calendario/clock/{$acti.hora}.gif" width="13" height="13" hspace="2">{/if}</td>
        </tr>
        {/foreach} 
        <tr> 
          <td height="100%" valign="top" bordercolor="#00FFFF" bgcolor="#FFFFEA" class="texto"> 
            {foreach from=$semana[0].actividades item=acti} &nbsp;{$acti.time} 
            <a href="{$frmUrl}?accion={$accion.MUESTRA_ACTIVIDAD}&idAct={$acti.id}&date={$date}">{$acti.asunto}</a><br>
            {foreachelse} &nbsp; {/foreach} </td>
        </tr>
      </table></td>
  </tr>
</table>
