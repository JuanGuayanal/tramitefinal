<script src="/sitradocV3/js/src/trabajadores/buscador.js"></script>
<br>
<form name="{$frmName}" action="{$frmUrl}" method="post" id="frmBuscarDocTrab" >
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="{$accion.BUSCA_DOCTRAB}">
  <input name="menu" type="hidden" id="menu" value="{$menuPager}">
  <input name="subMenu" type="hidden" id="subMenu" value="{$subMenuPager}">
    <input name="fecIniDir" type="hidden" id="fecIniDir" value="{$fecIniDir}">
    <div id="mensaje_sistema">
	{if $indInt}
    	{if $claseindInt=='SIN DOCUMENTO'}
		<font size=2 color="#CC0000"><strong><center>
		Respondido
		</center></strong><br></font>
        {else}
		<font size=2 color="#CC0000"><strong><center>
		El n&uacute;mero es {$claseindInt} {$indInt} 
		</center></strong><br></font>
        {/if}
	{/if}	
    </div>
  <table width="810" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#FFFFFF" class="tabla-login">
    <tr>
      <td height="20" align="center" class="textoblack">&nbsp;</td>
      <td align="center" class="textoblack">&nbsp;</td>
      <td align="center" class="textoblack">&nbsp;</td>
    </tr>
    <tr> 
      <td height="20" colspan="3" align="center" class="textoblack"><img src="/img/800x600/cab.yellow.gif" alt="" width="100%" height="1"/></td>
    </tr>
    <tr> 
      <td align="left" class="textoblack"><label><input name="tipEstado" type="radio" value="1" {if ($tipEstado==1||!$tipEstado)} checked {/if} >
              Pendientes</label>        <label> 
              <input name="tipEstado" type="radio" value="2" {if ($tipEstado==2)} checked {/if} >
              Derivados</label></td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="3" align="left" class="textoblack"> </td>
      <td align="center" valign="middle">&nbsp;</td>
    </tr>
    <tr> 
      <td align="left" class="textoblack"><strong>B&uacute;squeda general</strong></td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
    </tr>
    <tr>
      <td align="left" bgcolor="#F2FBFF">
        <input type="checkbox" name="checkTodos" value="1" id="marcar_todos" />
Todos
<input type="checkbox" name="checkAsunto" value="1" id="checkAsunto" class="marcar_uno" />
Asunto
<input type="checkbox" name="checkRazon" value="1" id="checkRazon" class="marcar_uno" />
Raz&oacute;n Social
<input type="checkbox" name="checkTrab" value="1" id="checkTrab" class="marcar_uno" />
Ubicaci&oacute;n <input name="nroTD" type="text" class="iptxtn" value="{$nroTD}" size="20" maxlength="100" id="nroTD"  placeholder="T&eacute;rmino a buscar"/></td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
      <td rowspan="5" align="center" valign="middle" bgcolor="#F2FBFF"><img src="/img/800x600/btnBuscar3.gif" width="70" height="20" border="0" id="btn_buscar" class="mano"/></td>
    </tr>
    <tr> 
      <td height="2" align="left" class="textoblack" bgcolor="#F2FBFF">Desde:
        <input name="FechaIni" type="text" class="iptxtn datepicker" id="FechaIni" value="{$FechaIni}" tabindex="4" onkeypress="ninguna_letra();" size="10" />
Hasta:
<input name="FechaFin" type="text" class="iptxtn datepicker" id="FechaFin" value="{$FechaFin}"  tabindex="5" onkeypress="ninguna_letra();" size="12" /></td>
      <td height="2" align="left" class="textoblack">&nbsp;</td>
    </tr>
    <tr> 
      <td align="left" class="textoblack"><strong>B&uacute;squeda espec&iacute;fica</strong></td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
    </tr>
    <tr> 
      <td  align="left" bgcolor="#F2FBFF"><input name="checkExt" id="checkExt" type="checkbox" value="1" checked="checked">&nbsp;Externos&nbsp;
        <input name="checkInt" id="checkInt" type="checkbox" value="1" checked="checked">&nbsp;Internos
        <input name="checkAceptados" id="checkAceptados" type="checkbox" value="1" checked="checked">&nbsp;Aceptados&nbsp;	
        <input name="checkNoAceptados" id="checkNoAceptados" type="checkbox" value="1" checked="checked">
        &nbsp;No aceptados</td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
      <td width="1" align="center" class="textoblack">&nbsp;</td>
    </tr>
    <tr>
      <td align="left" bgcolor="#F2FBFF"><select name="tipodDoc" class="ipseln" id="tipodDoc">{$selTipodDoc}</select>
        &nbsp;&nbsp;
        <input name="indicativo" type="text" id="indicativo" class="iptxtn" value="{$indicativo}" size="6" maxlength="100"  placeholder="N&deg; Doc." />
        &nbsp;&nbsp;
        <select name="anyo3" class="ipseln" id="anyo3">
          <option value="none">Todos</option>
        </select>
        &nbsp;&nbsp;
        <select name="siglasDep" class="ipseln" id="siglasDep">{$selSiglasDep}</select></td>
      <td align="left" valign="bottom" bgcolor="#F2FBFF">&nbsp;</td>
    </tr>
			<tr> 
              <td colspan="12"><img src="/img/800x600/cab.yellow.gif" alt="" width="100%" height="2"/></td>
            </tr>
    <tr>
    <td colspan="10"><table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr> 
            <td colspan="2"><img src="/img/800x600/cab.yellow.gif" alt="" width="100%" height="1"/></td>
          </tr>
<tr id="menu_opciones">
	<td align="left"></td>
	<td colspan=10 valign="middle" align="right"><a href="#" id="btn_aceptar"><img src="/img/ico_suge2.gif" width="18" height="18" border="0" align="absmiddle" alt="ACEPTAR DOCUMENTO(s)"><strong>Aceptar 
              </strong></a>{if $nivel==2} &nbsp;|&nbsp;<a href="#" id="btn_delegar"><img src="/img/800x600/ico-gente12.gif" width="18" height="18" border="0" align="absmiddle" alt="ASIGNAR DOCUMENTO(s)"><strong>Asignar 
              </strong></a>{/if}{if $nivel==2} &nbsp;|&nbsp;<a href="#" id="btn_derivar"><img src="/img/800x600/ico_responder.gif" width="18" height="18" border="0" align="absmiddle" alt="RESPONDER DOCUMENTO(s)"><strong>Responder
              </strong></a>{/if}{if $nivel==3} &nbsp;|&nbsp;<a href="#" id="btn_derivar"><img src="/img/800x600/ico_responder.gif" width="18" height="18" border="0" align="absmiddle" alt="RESPONDER DOCUMENTO(s)"><strong>Responder
              </strong></a>{/if} &nbsp;|&nbsp;<a href="#" id="btn_finalizar"><img src="/img/firma22.gif" width="18" height="18" border="0" align="absmiddle" alt="ARCHIVAR DOCUMENTO(s)"><strong>Archivar 
              </strong></a><!-- &nbsp;|&nbsp;<a href="javascript:AgregaAvance(document.frmSearchExtended2,'frmAddObsDocTrab','SumarioTrab')"><img src="/img/documentos32.gif" width="18" height="18" border="0" align="absmiddle" alt="AGREGAR AVANCE(s)"><strong>Agregar 
              avance </strong></a>--> 
			  
			</td>
    </tr>
       
		            <tr> 
            <td colspan="2"><hr width="100%" size="1"></td>
          </tr></table>
          </td>
    </tr>
    <tr>
	<td colspan=10>
		<table id="b_buscador" style="width:100%;">
        <thead><tr>
            <th>Referencia</th>
            <th>Raz&oacute;n Social</th><th>Asunto</th><th>Fecha</th><th>Estado</th><th>Ubicaci&oacute;n</th><th align="left"><input type="checkbox" id="todos_registros"></th></tr></thead>
        <tbody>
        </tbody>
        </table>
    </td>
    </tr>
  </table>
</form>
<div id="modal_deracciones" class="std_oculto">
<div class="std_form">
		<h1>ACCIONES</h1>
        <table>
    	{$acciones}
        </table><br />
<div><span class="std_button" id="btn_aceptarAcciones">Aceptar</span></div>
    </div>
</div>
<div id="modal_derivar" class="std_oculto">
<div class="std_form">
		<h1 id="titulo_modal"></h1>
    	<form id="form_derivar" action="#">
        <p> 
		  <label>Documento</label>
		</p>
        <p>
		<table id="list_derdocumentos">
        <thead><tr>
            <th>Documento</th>
            <th>x</th></tr></thead>
        <tbody>
        </tbody>
        </table>
        </p>
        <p> 
		  <label for="sel_derclasedoc">Tipo de documento</label>
            <select id="sel_derclasedoc" class="std_select">{$selTipodDocA}</select>
		</p>
        <p> 
		  <label for="txt_derasunto">Asunto<br><br><br><br><br><br></label>
            <textarea id="txt_derasunto" required="required" class="iptxtn" cols="50" rows="3"></textarea>
		</p>
        <p> 
		  <label for="txt_derobs">Observaciones<br><br><br><br><br><br></label>
            <textarea id="txt_derobs" class="iptxtn" cols="50" rows="3"></textarea>
		</p>
        <p class="std_oculto">
		  <label for="sel_derdestino">Responder a</label>
            <select id="sel_derdestino" class="std_select">{$selCoordinador}</select>
		</p>
        <p class="opt_asignar"> 
		  <label for="sel_trabajador">Trabajador</label>
           <select id="sel_trabajador" class="std_select">{$selTrabajador}</select> <span class="std_button" id="btn_agregartrab">Agregar</span>
		</p>       
        <p>
        <div class="opt_asignar">
		<table id="list_trabajadores">
        <thead><tr>
            <th>Nombre</th>
            <th>Acciones</th>
            <th>Observaciones</th>
            <th>x</th></tr></thead>
        <tbody>
        </tbody>
        </table>
        </div>
        </p>
        <p><br />
        <span class="std_button opt_derivar" id="btn_derderivaragrupar">Responder</span> <span class="std_button opt_reiterar" id="btn_derreiterar">Reiterar</span><span class="std_button opt_asignar" id="btn_asignar">Asignar</span> <span class="std_button" id="btn_dercancelar">Cancelar</span>
</p>
    	</form>
    </div>
</div>
<div id="modal_finalizar" class="std_oculto">
<div class="std_form">
		<h1>ARCHIVAR DOCUMENTO</h1>
    	<form id="form_finalizar" action="#">
        <p> 
		  <label for="txt_finobs">Motivo de archivo</label>
            <textarea id="txt_finobs" required="required" class="iptxtn" cols="50" rows="3"></textarea>
		</p>
        <p><span class="std_button" id="btn_finfinalizar">Archivar</span> <span class="std_button" id="btn_fincancelar">Cancelar</span>
</p>
    	</form>
    </div>
</div>