  <tr> 
    <td valign="top"> 
      <table width="779" border="0" cellspacing="0" cellpadding="0">
        <tr> 
		  {if $blockLeft}
          <td width="190" align="center">&nbsp;</td>
          <td width="18">&nbsp;</td>
          <td width="571">&nbsp;</td>
		  {else}
          <td colspan="3">&nbsp;</td>
  		  {/if}		  
        </tr>
        <tr> 
		  {if $blockLeft}
          <td width="190" align="center"><img src="/img/800x600/n.sumario.gif" width="200" height="37"></td>
          
      <td width="18">&nbsp;</td>
          <td width="571" align="right" valign="middle" class="textoblack"><b>Sistema de Tr�mite Documentario</b></td>
		  {else}
          <td colspan="3" align="right" valign="middle"><img src="/img/800x600/n.operaciones.gif" align="absmiddle" alt="" vspace="1" /></td>
  		  {/if}		  
        </tr>
        <tr> 
		  {if $blockLeft}
          <td width="190" align="center" valign="top">
		  <table width="180" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" border="0" cellpadding="0" cellspacing="1">
                <tr> 
                  <td><img src="/img/800x600/cab.yellow.gif" alt="" width="100%" height="1"/></td>
                </tr>
                <tr> 
                  <td align="left" colspan="2"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Estimado(a) 
                    {$director}:</font></td>
                </tr>
                <tr> 
                  <td align="left" colspan="2"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">A 
                    la fecha el(la) DM mantiene</font><strong><font color="#FF0000" size="3" face="Verdana, Arial, Helvetica, sans-serif"> 
                    {$TotalEnCurso}</font></strong><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
                    documento(s) pendiente(s) de tr�mite.</font></td>
                </tr>
                <tr> 
                  <td align="left" colspan="2"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;</font></td>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td bgcolor="#D1E0EF"> 
              <table width="180" border="0" cellpadding="1" cellspacing="1">
                <tr> 
                  <td align="center" bgcolor="#FFE188"><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tipo<br>
                    Documento </font></b></td>
                  <td width="30" align="center" valign="top" bgcolor="#FFE188" title="Total de documentos existentes en la dependencia o en poder del usuario"> 
                    <p><b><font size="1" face="Verdana, Arial, Helvetica, sans-serif">E<br>
                      n<br>
                      <br>
                      C<br>
                      u<br>
                      r<br>
                      s<br>
                      o </font></b></p></td>
                  <td width="30" align="center" valign="top" bgcolor="#FFFFCC"><b><font color="#003399" size="1" face="Verdana, Arial, Helvetica, sans-serif">V<br>
                    e<br>
                    n<br>
                    c<br>
                    i<br>
                    d<br>
                    o<br>
                    s </font><font color="#003399" size="1" face="Verdana, Arial, Helvetica, sans-serif"><br />
                    </font></b></td>
                </tr>
                <tr bgcolor="#F2FBFF"> 
                  <td><strong><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Externos</font></strong></td>
                  <td align="center"><font color="#009966" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$ExternosCurso}</font></td>
                  <td align="center"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;-&nbsp;</font></td>
                </tr>
                <tr bgcolor="#F2FBFF"> 
                  <td><strong><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Expedientes</font></strong></td>
                  <td align="center"><font color="#009966" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$ExpedientesCurso}</font></td>
                  <td align="center"><font color="#FF0000" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$ExpedientesVencido}</font></td>
                </tr>
                <tr bgcolor="#F2FBFF"> 
                  <td><strong><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">Internos</font></strong></td>
                  <td align="center"><font color="#009966" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$InternosCurso}</font></td>
                  <td align="center"><font color="#666666" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;-&nbsp;</font></td>
                </tr>
                <tr bgcolor="#BED5FA"> 
                  <td align="center" bgcolor="#FFE188"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif">TOTAL</font></strong></td>
                  <td align="center" bgcolor="#FFE188"><font color="#009966" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$TotalEnCurso}</font></td>
                  <td align="center" bgcolor="#FFFFCC"><font color="#FF0000" size="1" face="Verdana, Arial, Helvetica, sans-serif">{$ExpedientesVencido}</font></td>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
        </table>{$menuLeft}</td>
          
      <td width="18" align="center" valign="top" background="/img/800x600/f_sepmenu2n.gif">&nbsp;</td>
          <td align="center" valign="top">
		  {else}
          <td align="center" valign="top" colspan="3">
  		  {/if}
		  <br />