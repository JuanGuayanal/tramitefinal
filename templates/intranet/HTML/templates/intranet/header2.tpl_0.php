<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - {if $titulo}{$titulo}{/if}</title>
<link href="/estilos/4/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/JER/JER.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<link rel="stylesheet" href="/styles/theme.css" type="text/css" />


<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/home_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/interna_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/menu_01_APS.js"></script>

<script src="/js/intranet.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/JSCookMenu.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/theme.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/mambojavascript.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/tiny_mce.js" language="JavaScript1.2" type="text/javascript"></script>

</head>

<body>
<div class="WEB_MAQUE_Alineacion1">
	<div class="WEB_MAQUE_Fondo1">
    	<div class="WEB_MAQUE_f1_c1">
        	<div class="WEB_CONTE_cabecera">
                	<div class="WEB_CONTE_bloqueSuperior1">
                   	  <div class="WEB_CONTE_bloque1 GEN_floatLeft">
                   		<div class="WEB_CONTE_tituloSuperior" style="background-image:none;"></div>
                            <div class="WEB_CONTE_actualizacion"></div>
                      </div>
                        <div class="WEB_CONTE_bloque2 GEN_floatLeft">
                          <div class="WEB_CONTE_opcionesSuperior">
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoInicio"><a href="/intranet.php">Inicio</a></div>
                           	    <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoPortal"><a href="http://www.CONVENIO_SITRADOC.gob.pe" target="_blank">Portal</a></div>
                                <div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoCorreo"><a href="http://mail.CONVENIO_SITRADOC.gob.pe" target="_blank">Correo</a></div>
                              	<div class="WEB_CONTE_opcionesIcono WEB_CONTE_opcionesIconoSalir"><a href="/exit.php">Salir</a></div>
                            <div class="GEN_clearLeft"></div>
                          </div>
                      </div>
                    <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_bloqueSuperior2">
                    	<div class="WEB_CONTE_usuario GEN_floatLeft">
                        	<div class="WEB_CONTE_usuarioParte1 GEN_floatLeft">
                        	  <p class="WEB_texto5">+   Bienvenido: <span class="WEB_texto6">&nbsp;&nbsp;{$userIntra}@{$domiIntra}</span></p>
                        	</div>
                            <div class="WEB_CONTE_usuarioParte2 GEN_floatLeft"></div>
                            <br class="GEN_clearLeft" />
                        </div>
							<div class="WEB_CONTE_usuarioParte3 GEN_floatRight">
						{$groupPage}
						<?php /*?><a href="/{$sugePage}"><img src="/img/lb_suge.gif" alt="Enviar sugerencias" width="76" height="17" name="ico_hea1" vspace="0" border="0"></a><?php */?>&nbsp;&nbsp;&nbsp;
							</div>
                        <!-- <div class="WEB_CONTE_buscador GEN_floatLeft">
                                <input type="text" name="textfield" id="buscador" />
                                <div class="WEB_CONTE_buscadorBoton"></div>
                        </div> --> 
                        <div class="GEN_clearLeft"></div>
                    </div>
                	
                </div>
    	</div>
    </div>
    <div class="WEB_MAQUE_Fondo2">
    	<div class="WEB_MAQUE_f2_c1">
        	<div class="WEB_CONTE_barra">
            	<div class="WEB_CONTE_bloqueBarraIzquierdo">
            	  
            	</div>
                <div class="WEB_CONTE_bloqueBarraDerecho">
                  
                </div>
                <div class="GEN_clearBoth"></div>
            </div>
        </div>
    </div>
    <div class="WEB_MAQUE_Fondo3">
    	<div class="WEB_MAQUE_f3_c1">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#E5E5E5">&nbsp;</td>
    <td bgcolor="#FFFFFF">