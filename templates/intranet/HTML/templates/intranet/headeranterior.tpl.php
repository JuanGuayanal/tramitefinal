<html>
<head>
<title>Intranet MIDIS {if $titulo}{$titulo}{/if}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="/styles/intranet.css" type="text/css" />
<link rel="stylesheet" href="/styles/theme.css" type="text/css" />
{section name=i loop=$styles}
<link rel="stylesheet" href="/styles/{$styles[i]}.css" type="text/css" />
{/section}
<script src="/js/intranet.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/JSCookMenu.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/theme.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/mambojavascript.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/tiny_mce.js" language="JavaScript1.2" type="text/javascript"></script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"{if $onLoad} onLoad="{$onLoad}"{/if}>
<table width="779" border="0" cellspacing="0" cellpadding="0"{if $indIndex} background="/img/800x600/bg_index.gif"{/if}>
  <tr>
    <td>       
    <table width="779" border="0" cellspacing="0" cellpadding="0">
      <tr>          
          <td width="199" valign="top"><img src="/img/800x600/cabecera1-1.gif" width="199" height="86" alt="" /></td>
          <td width="471">            
            <table width="580" border="0" cellspacing="0" cellpadding="0">
              <tr>                
                <td valign="top" background="/img/800x600/f_header1.gif"> 
                  <table width="580" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td rowspan="4" height="70" class="menhea" valign="middle" width="109"><img src="/img/800x600/cabecera1-2.gif" width="109" height="70" alt="" /></td>
                      <td bgcolor="#464D95" height="25" class="menhea" valign="middle"> 
                      <!-- Aqui Empieza Items del Menu Institucional -->
                      {$modInsti}
                      <!-- Aqui Termina Items del Menu Institucional -->
                      </td>
                    </tr>
                    <tr> 
                      <td height="1"></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#3270AB" height="25" class="menhea"> 
                        <!-- Aqui Empieza Items del Menu Personal -->
                        {$modPers}
                        <!-- Aqui Termina Items del Menu Personal -->
                      </td>
                    </tr>
                    <tr> 
                      <td height="19" align="right"><span class="userIntra"><strong>&nbsp;&nbsp;{$userIntra}@{$domiIntra} (En l&iacute;nea)&nbsp;&nbsp;</strong></span></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="right">
                  <table border="0" cellspacing="0" cellpadding="0" width="580">
                  <tr> 
                      <td class="fecha" valign="middle" width="258"><b>{if $indIndex}&nbsp;{else}{$fecHoy}{/if}</b></td>
                      <!--<td width="113"><img src="/img/lb_icos2.gif" name="lb_ico" width="113" height="17" id="lb_ico" alt="" /></td>-->
                      <td width="14">&nbsp;</td>
                      
                    <td>{$groupPage}</td>
                      <td width="4">&nbsp;</td>
                  <?php /*?>    <td width="18"><a href="/{$sugePage}"><img src="/img/lb_suge.gif" name="ico_hea1" width="76" height="17" border="0" id="ico_hea1" alt="" /></a></td><?php */?>
											<!--<td width="18"><a href="/{$sugePage}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('lb_ico','','/img/lb_suge.gif','ico_hea1','','/img/ico_suge_o.gif',0)"><img src="/img/ico_suge.gif" name="ico_hea1" width="18" height="17" border="0" id="ico_hea1" alt="" /></a></td>-->
                      <td width="20">&nbsp;</td>
                      <td width="135" valign="top"><img src="/img/800x600/panel-logout.gif" width="135" height="16" usemap="#maplogout" border="0" alt="" /></td>
                    </tr>
                  </table>
              </td>
              </tr>
            </table>
          </td>
      </tr>
      </table>
    </td>
  </tr>
<map name="maplogout" id="maplogout"> 
  <area shape="rect" coords="4,0,83,16" href="/{$indexPage}" alt="" />
  <area shape="rect" coords="84,0,135,16" href="/{$exitPage}" alt="" />
</map>
