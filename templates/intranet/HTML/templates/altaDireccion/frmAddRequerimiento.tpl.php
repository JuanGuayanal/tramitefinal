{$jscript}
<br>
<form action="{$frmUrl}" method="post" name="{$frmName}" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    {if $errors} 
    <tr> 
      <td colspan="3" class="item">{$errors} </td>
    </tr>
    {/if} 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item">{$FechaActual}&nbsp;&nbsp;&nbsp;{$HoraActual} {$id} </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Requerimiento</strong></td>
      <td> <textarea name="observaciones" cols="90" rows="4" class="iptxt1" id="observaciones" >{if !$observaciones}Sin Observaciones{else}{$observaciones}{/if}</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Destino</strong></td>
      <td> <select name="coddep" class="ipsel2" >
          <option value="none"{if $coddep==none} selected{/if}>Todos</option>
		  <option value="16"{if $coddep==16} selected{/if}>DVM-PE</option>
		  <option value="36"{if $coddep==36} selected{/if}>VMI</option>
		  <option value="50"{if $coddep==50} selected{/if}>DM-ASES</option>
		  <option value="5"{if $coddep==5} selected{/if}>SG</option>
        </select> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Plazo</strong></td>
      <td><select name="dia_ini" class="ipsel2" id="dia_ini">
      {$selDiaIng}
	    </select> <select name="mes_ini" class="ipsel2" id="mes_ini">
	  {$selMesIng}
        </select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
      {$selAnyoIng}
        </select></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr	
    ><tr> 
      <td colspan="3" class="item"> <div align="right">
			    </div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Agregar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','{$frmUrl}?accion={$accion.SUMARIO_DIR}&menu={$accion.SUMARIO_DIR}&subMenu={$accion.SUMARIO_DIR}');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="{$accion.AGREGA_REQUERIMIENTO}"> 
        <input name="menu" type="hidden" id="menu" value="{$menuPager}">
		<input name="id" type="hidden" id="id" value="{$id}">
		{section name=i loop=$ids} 
		
        <!--<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">-->
		<input name="ids[]" type="hidden" id="ids[]" value="{$ids[i]}">
		
		{/section}
		
		{if $tipDocumento}<input name="tipDocumento" type="hidden" id="tipDocumento" value="{$tipDocumento}">{/if}
		{if $tipBusqueda}<input name="tipBusqueda" type="hidden" id="tipBusqueda" value="{$tipBusqueda}">{/if}
		{if $fecIniDir}<input name="fecIniDir" type="hidden" id="fecIniDir" value="{$fecIniDir}">{/if}
		{if $fecFinDir}<input name="fecFinDir" type="hidden" id="fecFinDir" value="{$fecFinDir}">{/if}
		{if $asunto2}<input name="asunto2" type="hidden" id="asunto2" value="{$asunto2}">{/if}
		{if $indicativo2}<input name="indicativo2" type="hidden" id="indicativo2" value="{$indicativo2}">{/if}
		{if $observaciones2}<input name="observaciones2" type="hidden" id="observaciones2" value="{$observaciones2}">{/if}
		{if $nroTD}<input name="nroTD" type="hidden" id="nroTD" value="{$nroTD}">{/if}
		{if $page}<input name="page" type="hidden" id="page" value="{$page}">{/if}
		{if ($siglasDep&& $siglasDep!='none')}<input name="siglasDep" type="hidden" id="siglasDep" value="{$siglasDep}">{/if}
		{if ($tipodDoc&& $tipodDoc!='none')}<input name="tipodDoc" type="hidden" id="tipodDoc" value="{$tipodDoc}">{/if}
		{if $procedimiento}<input name="procedimiento" type="hidden" id="procedimiento" value="{$procedimiento}">{/if}
		{if ($anyo3&& $anyo3!='none')}<input name="anyo3" type="hidden" id="anyo3" value="{$anyo3}">{/if}
		 </td>
    </tr>
  </table>
</form>