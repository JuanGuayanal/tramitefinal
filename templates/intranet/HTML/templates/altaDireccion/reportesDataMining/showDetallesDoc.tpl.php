<html>
<head>
<title>Documentos : {$doc.desc}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.Estilo1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; font-weight: bold; }
-->
</style>
<!-- {/literal} -->
<script language="JavaScript">
<!--
{literal} 
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function cargar(pForm,a,m)
{
	//var texto=pForm.tipDocumento.value;
	//var texto2=pForm.tipBusqueda.value;
	var id=pForm.id.value;
		pForm.action="index.php?accion=showDetallesAccion&id="+id+"&a="+a+"&m="+m;
	    //pForm.bsubmit.disabled=true;
		pForm.submit();
}
/*
function SubmitFormm(pForm,pAccion){
	pForm.accion.value=pAccion
	pForm.bsubmit.disabled=true;
	pForm.submit()
	return false
}
*/
{/literal}
-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<form name="{$frmName}" action="{$frmUrl}" method="post" target="_self">
  <input name="accion" type="hidden" id="accion" value="{$accion.MUESTRA_DETALLE_FLUJODOCDIR}">
  <input name="id" type="hidden" id="id" value="{$id}">
  <input name="fechai" type="hidden" id="fechai" value="{$fechai}">
  <input name="fechaf" type="hidden" id="fechaf" value="{$fechaf}">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="{$frmUrl}?accion={$accion.IMPRIME_DETALLE_DIR}&id={$id}"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="5" class="textoblack"><strong><img src="/img/ico-detalle.gif" width="24" height="24" hspace="4" align="absmiddle"><u>DETALLES 
            DEL DOCUMENTO PRINCIPAL</u></strong> <br> <strong>{$nroExp}&nbsp;&nbsp;</strong></td>
        </tr>
		
        <tr> 
          <td width="135" class="textoblack"><strong>N&deg; Documento</strong></td>
          <td class="texto" width="110%">{$regDoc}</td>
          <td width="75%" class="texto">&nbsp;</td>
          <td width="75%" class="texto">&nbsp;</td>
          <td width="75%" class="texto">{if ($coddepTrab==13||$coddepTrab==1)}<input type="submit" class="texto" value="Nuevo requerimiento" onClick="cargar(document.{$frmName},5)">{/if}</td>
        </tr>
		{if $a==5}
        <tr> 
          <td rowspan="2">&nbsp;</td>
		  <td rowspan="2" class="textoblack"><input name="opcionX1" type="checkbox" id="checkbox" value="1" {if $opcionX1==1} checked {/if}>
				Todos</td>
		  <td colspan="2" rowspan="2" class="textogray"><textarea name="requerimiento1" cols="60" rows="4" class="iptxt1" id="requerimiento1" >{$requerimiento1}</textarea></td>
		  <td  class="texto"><input name="fecha1" type="text" class="iptxt1" id="fecha1" value="{$fecha1}"></td>
        </tr>
        <tr>
          <td  class="texto">Fecha de Requerimiento </td>
        </tr>
        <tr> 
          <td rowspan="2">&nbsp;</td>
		  <td rowspan="2" class="textoblack"><input name="opcionX2" type="checkbox" id="checkbox" value="1" {if $opcionX2==1} checked {/if}>
				SG</td>
		  <td colspan="2" rowspan="2" class="texto"><textarea name="requerimiento2" cols="60" rows="4" class="iptxt1" id="requerimiento2" >{$requerimiento2}</textarea></td>
		  <td class="texto"><input name="fecha2" type="text" class="iptxt1" id="fecha2" value="{$fecha2}"></td>
        </tr>
        <tr>
          <td class="texto">Fecha de Requerimiento </td>
        </tr>
        <tr> 
          <td rowspan="2">&nbsp;</td>
		  <td rowspan="2" class="textoblack"><input name="opcionX3" type="checkbox" id="checkbox" value="1" {if $opcionX3==1} checked {/if}>
				DVM-PE</td>
		  <td colspan="2" rowspan="2" class="texto"><textarea name="requerimiento3" cols="60" rows="4" class="iptxt1" id="requerimiento3" >{$requerimiento3}</textarea></td>
		  <td class="texto"><input name="fecha3" type="text" class="iptxt1" id="fecha3" value="{$fecha3}"></td>
        </tr>
        <tr>
          <td class="texto">Fecha de Requerimiento </td>
        </tr>
        <tr> 
          <td rowspan="2">&nbsp;</td>
		  <td rowspan="2" class="textoblack"><input name="opcionX4" type="checkbox" id="checkbox" value="1" {if $opcionX4==1} checked {/if}>
				VMI</td>
		  <td colspan="2" rowspan="2" class="texto"><textarea name="requerimiento4" cols="60" rows="4" class="iptxt1" id="requerimiento4" >{$requerimiento4}</textarea></td>
		  <td class="texto"><input name="fecha4" type="text" class="iptxt1" id="fecha4" value="{$fecha4}"></td>
        </tr>
        <tr>
          <td class="texto">Fecha de Requerimiento</td>
        </tr>
        <tr> 
          <td rowspan="2">&nbsp;</td>
		  <td rowspan="2" class="textoblack"><input name="opcionX5" type="checkbox" id="checkbox" value="1" {if $opcionX5==1} checked {/if}>
				DM-ASES</td>
		  <td colspan="2" rowspan="2" class="texto"><textarea name="requerimiento5" cols="60" rows="4" class="iptxt1" id="requerimiento5" >{$requerimiento5}</textarea></td>
		  <td class="texto"><input name="fecha5" type="text" class="iptxt1" id="fecha5" value="{$fecha5}"></td>
        </tr>
        <tr>
          <td class="texto">Fecha de Requerimiento </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
		  <td class="textoblack">&nbsp;				</td>
		  <td colspan="2" class="texto">&nbsp;</td>
		  <td class="texto"><input type="submit" class="texto" value="Guardar" onClick="cargar(document.{$frmName},6)"></td>
        </tr>		
		{/if}
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="5" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>SITUACI&Oacute;N 
              DEL DOCUMENTO</strong></div></td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Situaci&oacute;n Actual en SITRADOC </strong></td>
          <td class="texto">{if $sitActual}Finalizado en {$sitActual}{else}Pendiente(No ha sido finalizado){/if}</td>
          <td class="texto">{if $fechaMaxPlazo!=""}<strong>Plazo M&aacute;ximo:</strong> {$fechaMaxPlazo}{/if}</td>
          <td class="texto"><strong>{if !$sitActual}{/if}</strong></td>
          <td class="texto">{if !$sitActual}{$nroDias}{/if}</td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td class="textoblack"><strong>Ubicaci&oacute;n Actual enSITRADOC </strong></td>
          <td class="texto">{$ubiActual}</td>
          <td class="texto"><strong>{if $nroDiasTupa}N&deg; de D&iacute;as catalogados 
            en TUPA{/if}</strong></td>
          <td class="texto">{if $nroDiasTupa}{$nroDiasTupa}&nbsp;{/if}</td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Resumen</strong></td>
          <td class="texto">{$resumen}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td class="textoblack"><strong>Nro. Memorando </strong></td>
          <td class="texto">{$nroMemo}</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td class="textoblack"><strong>Estado del documento Virtual </strong></td>
          <td class="textored"><strong>{if ($flag==4)}ATENDIDO{else}PENDIENTE{/if}</strong></td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="5" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>REQUERIMIENTOS DEL MINISTRO </strong></div></td>
        </tr>
        <tr> 
          <td colspan="5" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		{section name=i loop=$Exp}
        <tr> 
          <td colspan="5" class="textoblack"><strong>REQUERIMIENTO N� {$smarty.section.i.iteration}</strong></td>
        </tr>
        <tr> 
         	<td colspan="5" class="textoblack">
		  		<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
					<tr><td colspan="5"><table width="100%">
						  <tr>
								<td width="20%" class="textoblack">Requerimiento</td>
								<td width="60%" class="textoblack">{$Exp[i].req}</td>
								<td width="20%" class="textoblack">{if ($Exp[i].rpta==""&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Agregar Respuesta" onClick="cargar(document.{$frmName},1,{$Exp[i].id})">{/if}</td>
				  		  </tr>
						  <tr bordercolor="#000000">
							<td class="textoblack">Fecha de Requerimiento</td>
							<td class="textoblack">{$Exp[i].fecha1}</td>
							<td class="textoblack">{if ($Exp[i].rpta!=""&& $Exp[i].flag==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}
						    <input type="submit" class="texto" value="Finalizar" onClick="cargar(document.{$frmName},2,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr bordercolor="#000000">
								<td class="textoblack">Responsable:{$Exp[i].destino}</td>
								<td class="textoblack">&nbsp;</td>
								<td class="textoblack">&nbsp;</td>
						  </tr>
				  </table></td></tr>	  
			  </table>
			</td>
        </tr>
		{if (($a==1&& $Exp[i].id==$m)||$Exp[i].rpta!="")}
        <tr> 
          <td colspan="5" class="textoblack"><strong>RESPUESTA</strong></td>
        </tr>
        <tr> 
         	<td colspan="5" class="textoblack">
		  		<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
					<tr><td colspan="5"><table width="100%">
						  <tr>
							<td class="textoblack" width="20%">Respuesta</td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta</td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($Exp[i].flagEmail==1&& $Exp[i].rpta2==""&& $a!=7&& $Exp[i].flag!=1&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},7,{$Exp[i].id})">{/if}</td>
						  </tr>
						  
							
							{if (($a>=7&& $a<=15&& $Exp[i].id==$m)||($Exp[i].rpta2!=""))}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 2 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta2[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones"  {if $Exp[i].flagEmail2==1} disabled{/if}>{if !$Exp[i].rpta2}Sin Respuesta{else}{$Exp[i].rpta2}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail2==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},71,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 2 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR2!=""}{$Exp[i].fecR2}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail2==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},72,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail2==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail2==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($Exp[i].flagEmail2==1&& $Exp[i].rpta3==""&& $a!=8&& $Exp[i].flag!=1&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},8,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if (($a>=8&& $a<=15&& $Exp[i].id==$m)||($Exp[i].rpta3!=""))}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 3 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta3[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail3==1} disabled{/if}>{if !$Exp[i].rpta3}Sin Respuesta{else}{$Exp[i].rpta3}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail3==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},81,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 3 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR3!=""}{$Exp[i].fecR3}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail3==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},82,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail3==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail3==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td height="42" class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($Exp[i].flagEmail3==1&& $Exp[i].rpta4==""&& $a!=9&& $Exp[i].flag!=1&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},9,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if (($a>=9&& $a<=15&& $Exp[i].id==$m)||($Exp[i].rpta4!=""))}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 4 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta4[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail4==1} disabled{/if}>{if !$Exp[i].rpta4}Sin Respuesta{else}{$Exp[i].rpta4}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail4==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},91,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 4 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR4!=""}{$Exp[i].fecR4}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail4==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},92,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail4==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail4==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($Exp[i].flagEmail4==1&& $Exp[i].rpta5==""&& $a!=10&& $Exp[i].flag!=1&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},10,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if (($a>=10&& $a<=15&& $Exp[i].id==$m)||($Exp[i].rpta5!=""))}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 5 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta5[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" >{if !$Exp[i].rpta5}Sin Respuesta{else}{$Exp[i].rpta5}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail5==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},101,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 5 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR5!=""}{$Exp[i].fecR5}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail5==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},102,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($Exp[i].flagEmail5==1&& $Exp[i].rpta6==""&& $a!=11&& $Exp[i].flag!=1&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},11,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if ($a>=11&& $a<=15)}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 6 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta6[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 6 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($a==11)}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},12,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if ($a>=12&& $a<=15)}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 7 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta7[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 7 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($a==12)}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},13,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if ($a>=13&& $a<=15)}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 8 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta8[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 8 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($a==13)}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},14,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if ($a>=14&& $a<=15)}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 9 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta9[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 9 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($a==14)}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},15,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
							{if ($a>=15&& $a<=15)}
						  <tr>
							<td class="textoblack" width="20%">Respuesta 10 </td>
							<td width="60%" colspan="2" class="textoblack"><textarea name="respuesta10[{$Exp[i].id}]" cols="60" rows="4" class="iptxt1" id="observaciones" {if $Exp[i].flagEmail==1} disabled{/if}>{if !$Exp[i].rpta}Sin Respuesta{else}{$Exp[i].rpta}{/if}</textarea></td>
							<td class="textoblack" width="20%">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Actualizar Borrador" onClick="cargar(document.{$frmName},3,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">Fecha de Respuesta 10 </td>
							<td colspan="2" class="textoblack">{if $Exp[i].fecR!=""}{$Exp[i].fecR}{else}No se ha respondido{/if}</td>
							<td class="textoblack">{if ($Exp[i].flagEmail==0&& ($coddepTrab==13||$coddepTrab==$Exp[i].coddepDestino))}<input type="submit" class="texto" value="Enviar" onClick="cargar(document.{$frmName},4,{$Exp[i].id})">{/if}</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored"><strong>{if $Exp[i].flag==1}ATENDIDO{else}PENDIENTE{/if}&nbsp;{if $Exp[i].flagEmail==1}-SE HA ENVIADO POR MAIL{/if}</strong></td>
							<td class="textored"><div align="right"><strong>{if $Exp[i].flagEmail==0}BORRADOR{/if}</strong></div></td>
							<td class="textoblack">&nbsp;</td>
						  </tr>
						  <tr>
							<td class="textoblack">&nbsp;</td>
							<td class="textored">&nbsp;</td>
							<td class="textored"><div align="right"></div></td>
							<td class="textoblack">{if ($a==15&& $Exp[i].flagEmail==1)}<input type="submit" class="texto" value="Nueva Respuesta" onClick="cargar(document.{$frmName},16,{$Exp[i].id})">{/if}</td>
						  </tr>							
							{/if}
														
					</table></td></tr>
				</table>
			</td>
        </tr>
		{/if}
		{sectionelse}
		
		{/section}
		<!--
        <tr> 
          <td class="textoblack"><strong>N&deg;</strong></td>
          <td class="textoblack"><strong>DEPENDENCIA</strong></td>
          <td class="textoblack"><strong>SUB DEPENDENCIA</strong></td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>TIPO DE DOCUMENTO</strong></td>
          <td class="textoblack"><strong>ASUNTO</strong></td>
          <td class="textoblack"><strong>FOLIOS</strong></td>
          <td class="textoblack"><strong>OBSERVACI&Oacute;N</strong></td>
          <td class="textoblack"><strong>FECHA Y HORA</strong></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {section name=i loop=$Exp} 
        <tr> 
          <td class="textoblack">{$smarty.section.i.iteration}</td>
          <td class="textoblack">{$Exp[i].depDest}</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack">{$Exp[i].tipDoc}&nbsp;{$Exp[i].ind}</td>
          <td class="textoblack">{$Exp[i].asu}</td>
          <td class="textoblack">{$folio}</td>
          <td class="textoblack">{$Exp[i].obs}</td>
          <td class="textoblack">{$Exp[i].fecha}</td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {sectionelse} 
        <tr> 
          <td colspan="9" class="textoblack"> <div align="center"><strong>No existe 
              informaci&oacute;n acerca del flujo del Documento</strong></div></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        {/section}
        <tr> 
          <td colspan="2" class="texto">&nbsp;</td>
          <td colspan="7" class="texto">&nbsp;</td>
        </tr>
		--> 

        <tr> 
          <td colspan="3" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
            </table></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <b><u>INFORMES:</u></b><br>
            Oficina de Tecnolog&iacute;a de la Informaci&oacute;n OGTIE - Ministerio de Desarrollo e Inclusi&oacute;n Social<br> 
            <b>Telf. 616-2222 Anexo 680-331 </b><br>
            Email:<b> <a href="mailto:sitradoc@midis.gob.pe">sitradoc@midis.gob.pe</a></b><br> 
          </td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <div align="right"><em class="textored"><strong>Actualizado 
              al {$FechaActual}&nbsp;{$HoraActual}</strong> </em></div></td>
        </tr>
        <tr> 
          <td colspan="12" align="right">&nbsp;</td>
        </tr>
      </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
    </td>
  </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
</form>
</body>
</html>