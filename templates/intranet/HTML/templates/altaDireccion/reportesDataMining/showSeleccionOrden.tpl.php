<html>
<head>
<title>Documentos : {$doc.desc}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- {literal} -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<!-- {/literal} -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
{$jscript}
<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? 'tr-check' : 'tr-nocheck'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}

-->
</script>
<!-- {/literal} -->

<form name="{$frmName}" action="{$frmUrl}" method="post" target="_blank">
  <input name="accion" type="hidden" id="accion" value="{$accion.MUESTRA_REPORT1_NIVELIV}">
  <input name="RZ" type="hidden" id="RZ" value="{$RZ}">
  <input name="coddep" type="hidden" id="coddep" value="{$coddep}">
  <input name="sit" type="hidden" id="sit" value="{$sit}">
  <input name="tipDoc" type="hidden" id="tipDoc" value="{$tipDoc}">
  <input name="fecha1" type="hidden" id="fecha1" value="{$fecha1}">
  <input name="fecha2" type="hidden" id="fecha2" value="{$fecha2}">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7" >
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6" >
        <tr align="center"> 
          <td height="50" colspan="2" class="textoblack"><strong>REPORTE DE LA ALTA DIRECCI&Oacute;N </strong></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>CRITERIOS DE SELECCI&Oacute;N:</strong></td>
          </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>&nbsp;&nbsp;&nbsp;ESTADO</strong></td>
          <td width="75%" class="textoblack"><!--<select name="estado" class="texto" id="estado">
										<option value="1"{if $estado==1} selected{/if}>Todos</option>	
									<option value="2"{if $estado==2} selected{/if}>Pendiente</option>
									<option value="3"{if $estado==3} selected{/if}>Finalizado</option>
									<option value="4"{if $estado==4} selected{/if}>Vencido</option>
								 </select>-->
			<input type="checkbox" name="checkboxx2" value="2">
              <strong>Pendientes</strong>
            &nbsp;&nbsp;
            <input type="checkbox" name="checkboxx3" value="3">
              <strong>Finalizados</strong>
              <input type="checkbox" name="checkboxx4" value="4">
              <strong>Vencidos</strong>
            
              <input type="checkbox" name="checkboxx" value="1">
              <strong>Todos</strong>					 
		  </td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>&nbsp;&nbsp;&nbsp;ADMINISTRADO</strong></td>
          <td class="texto"><input name="RZ" type="text" class="texto" value="{$RZ}" size="40" maxlength="100">&nbsp;&nbsp;(Digite todo o parte de la Raz&oacute;n Social)</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>&nbsp;&nbsp;&nbsp;ASUNTO</strong></td>
          <td class="texto"><input name="asunto" type="text" class="texto" value="{$asunto}" size="40" maxlength="100"></td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>&nbsp;&nbsp;&nbsp;FECHA DE DESPACHO </strong></td>
          <td class="textoblack"><input name="fechai" type="text" class="texto" value="{$fechai}" size="20" maxlength="10">&nbsp;y el &nbsp;<input name="fechaf" type="text" class="texto" value="{$fechaf}" size="20" maxlength="10"></td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong></strong></td>
          <td class="textoblack"><input type="submit" class="texto"value="Generar"></td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp;</td>
  </tr>
  
</table>
</form>
</body>
</html>