<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<!-- FOOTER LEFT "Impreso el: {php}echo date('d/m/Y H:i:s');{/php}" -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><b><div align="center"><font color="#000000" size="6" face="Arial">REPORTE DE DOCUMENTOS
                {if $estado==1}PENDIENTES
                	{if $trabajador=='0'} DE TODOS LOS TRABAJADORES{/if}
                    {if $trabajador>0} DE {$nombre}{/if}
                {/if}
                {if $estado==2}PENDIENTES DELEGADOS / DERIVADOS A 
                	{if $trabajador=='0'}TODOS LOS TRABAJADORES{/if}
                    {if $trabajador>0}{$nombre}{/if}
                {/if}
                {if $estado==3 && $tipo_trabajador==1}GENERADOS{/if}
                {if $estado==3 && $tipo_trabajador==2}GENERADOS / DERIVADOS / ENVIADO<br>{$session_trab}{/if}
                {if $estado==4 && $tipo_trabajador==1}ARCHIVADOS
                	{if $trabajador=='0'}DE TODOS LOS TRABAJADORES{/if}
					{if $trabajador>0}POR {$nombre}{/if}
                {/if}
                {if $estado==4 && $tipo_trabajador==2}ARCHIVADOS POR<br>{$session_trab}{/if}
                {if $estado==11}PENDIENTES DEL CONGRESO DE LA REPUBLICA
                	{if $trabajador=='0'} EN TODOS LOS TRABAJADORES{/if}
                    {if $trabajador>0} EN {$nombre}{/if}
                {/if}
                {if $estado==11}PENDIENTES DEL CONGRESO DE LA REPUBLICA  
                	{if $trabajador=='0'} EN TODOS LOS TRABAJADORES{/if}
                    {if $trabajador>0} EN {$nombre}{/if}
                {/if}
    	</font>
      </div></b>
    <hr width="100%" size="1" noshade>  </td>
  </tr>
</table>
                    {if $estado==1}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th></tr></thead>
                        <tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].observaciones}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].acciones}</td><td>{$lista[i].destino}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==11}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><th>DESTINATARIO DE<br>CORRESPONDENCIA</th></tr></thead>
                        <tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].observaciones}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].acciones}</td><td>{$lista[i].destino}</td><td>{$lista[i].correspondencia}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==2}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                       <thead><tr bgcolor="#999999"><th>N&deg;</th>
                       <th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA CREACI&Oacute;N DE REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>UBICACI&Oacute;N</th>
                         <th>OBSERVACIONES Y/O REFERENCIAS</th></tr></thead>
                        <tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].fecha_creacion}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].destino}</td><<td>{$lista[i].observaciones}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==3 && $tipo_trabajador==1}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><th>DESTINATARIO DE<br>CORRESPONDENCIA</th><th>ESTADO</th></tr></thead><tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].observaciones}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].acciones}</td><td>{$lista[i].destino}</td><td>{$lista[i].correspondencia}</td><td>{$lista[i].estado}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==3 && $tipo_trabajador==2}
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>EXPEDIENTE</th><th>FECHA DE DERIVACI&Oacute;N</th><th>DESTINO</th></tr></thead><tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].observaciones}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].destino}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==4 && $tipo_trabajador==1}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>TRABAJADOR</th><th>MOTIVO DE<br>ARCHIVO</th><th>AVANCE</th></tr></thead><tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].fecha_creacion}</td><td>{$lista[i].fecha_recepcion}</td><td>{$lista[i].fecha_fin}</td><td>{$lista[i].trabajador_fin}</td><td>{$lista[i].motivo_fin}</td><td>{$lista[i].avance}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==4 && $tipo_trabajador==2}
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>MOTIVO DE<br>ARCHIVO</th><th>AVANCE</th></tr></thead><tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].fecha_creacion}</td><td>{$lista[i].fecha_recepcion}</td><td>{$lista[i].fecha_fin}</td><td>{$lista[i].motivo_fin}</td><td>{$lista[i].avance}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
                    {if $estado==5}
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>ESTADO</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th></tr></thead>
                        <tbody>
                        {section name=i loop=$lista}
                        <tr><td>{$smarty.section.i.iteration}</td><td>{$lista[i].estado}</td><td>{$lista[i].documento}</td><td>{$lista[i].numero}</td><td>{$lista[i].asunto}</td><td>{$lista[i].observaciones}</td><td>{$lista[i].fecha_derivacion}</td><td>{$lista[i].documento_inicial}</td><td>{$lista[i].razon_social}</td><td>{$lista[i].acciones}</td><td>{$lista[i].destino}</td></tr>
                        {/section}</tbody>
                    </table>
                    {/if}
<p align="right"></p>
</body>
</html>
