<!-- {literal}  -->
<script language="JavaScript">
<!--
ie = document.all?1:0
function mL(E,cName){
	if (ie){
		while (E.tagName!="TD")
			E=E.parentElement;
	}else{
		while (E.tagName!="TD")
			E=E.parentNode;
	}
	E.className = cName;
}
-->
</script>
<style type="text/css">
<!--
.tab-in-menu2 {
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #C0C0C0;
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #C0C0C0;
}
.tab-in-menu2 TD{
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #C0C0C0;
}
.td-menu2{
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #C0C0C0;
	padding-left: 20px;
}
.td-in-menu2{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #424242;
	text-decoration: none;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #C0C0C0;
	padding-right: 5px;
	padding-left: 5px;
}
.td-in-menu2-over{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #424242;
	text-decoration: none;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #C0C0C0;
	background-color: #FFFFFF;
	padding-right: 5px;
	padding-left: 5px;
}
.lnk-menu2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #424242;
	text-decoration: none;
	padding-right: 5px;
	padding-left: 5px;
}
.lnk-menu2 a,
.td-in-menu2 a,
.td-in-menu2-over a{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #424242;
	text-decoration: none;
	padding-right: 5px;
	padding-left: 5px;
}
.lnk-menu2 a:hover,
.td-in-menu2 a:hover,
.td-in-menu2-over a:hover{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #C0C0C0;
	text-decoration: none;
	padding-right: 5px;
	padding-left: 5px;
}
-->
</style>
<!-- {/literal} -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#024467" class="tab-menu2">
  <tr>
    <td class="td-menu2">&nbsp;</td>   
    <td valign="bottom">
	  <table width="100%" height="20" border="0" cellpadding="0" cellspacing="0" class="tab-IN-menu2">
        <tr> {section name=i loop=$menuItems} {if $menuItems[i].stat} 
          <td align="center" bgcolor="#F7F7F7" class="lnk-menu2"><a href="{$frmUrl}?accion={$menuItems[i].val}&subMenu={$menuItems[i].val}&menu={$menuPager}"> 
            {$menuItems[i].label}</a></td>
          {else} 
          <td align="center" bgcolor="#E2E2E2" class="td-in-menu2"><a href="{$frmUrl}?accion={$menuItems[i].val}&subMenu={$menuItems[i].val}&menu={$menuPager}" onMouseOver="mL(this,'td-in-menu2-over')" onMouseOut="mL(this,'td-in-menu2')"> 
            {$menuItems[i].label}</a></td>
          {/if} {/section} </tr>
      </table></td>
    <td width="100%" class="td-menu2">&nbsp;</td>
  </tr>
</table>
