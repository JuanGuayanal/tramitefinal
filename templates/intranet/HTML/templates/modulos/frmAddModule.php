<form name="{$frmName}" method="post" action="{$frmUrl}">
  <input type="hidden" name="accion" value="{$accion.}">	  
  <table width="540" border="0" cellspacing="0" cellpadding="2">
    <tr> 
      <td colspan="2" class="td-lb-login" width="540"><hr width="100%" size="1"> 
        <b>DEL M&Oacute;DULO</b> <hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td width="100" class="texto"><b>T&iacute;tulo Resumido</b></td>
            <td width="170"><input name="des_titulocorto" type="text" class="ip-login contenido" value="<?echo (empty($datos[0]))? $row[0] : $datos[0];?>" size="30" maxlength="50"></td>
            <td width="100" class="texto">T&iacute;tulo Completo</td>
            <td width="170"><input name="des_titulolargo" type="text" class="ip-login contenido" value="<?echo (empty($datos[1]))? $row[1] : $datos[1];?>" size="30" maxlength="150"></td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto"><b>M&oacute;dulo Padre</b></td>
            <td>
              <?
		if(!empty($datos[3])&&!is_null($datos[3]))
			$arg_tipMod = $datos[3];
		elseif(!empty($row[3])&&!is_null($row[3]))
			$arg_tipMod = $row[3];
		else
			$arg_tipMod = 1;
		
		$this->insertaSelectModulo('id_modulopadre', (empty($datos[2])) ? $row[2] : $datos[2], $arg_tipMod);
		      ?>
            </td>
            <td class="texto"><b>Tipo</b></td>
            <td> 
              <?
		$sql_st = "SELECT id_modulotipo, des_modulotipo FROM modulo_tipo WHERE ind_activo IS true ORDER BY 2";
		echo $this->muestraSelectUniversal('id_modulotipo', $sql_st, (empty($datos[3])) ? $row[3] : $datos[3], true);
              ?>
            </td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto">Descripci&oacute;n</td>
            <td colspan="3" width="440"><textarea name="des_modulo" cols="75" rows="3" class="ip-login contenido"><?echo (empty($datos[4]))? $row[4] : $datos[4];?></textarea></td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto">Imagen</td>
            <td><input name="img_modulo" type="text" class="ip-login contenido" value="<?echo (empty($datos[5]))? $row[5] : $datos[5];?>" size="30" maxlength="50"></td>
            <td class="texto">&Iacute;cono</td>
            <td><input name="ico_modulo" type="text" class="ip-login contenido" value="<?echo (empty($datos[6]))? $row[6] : $datos[6];?>" size="30" maxlength="50"></td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto">Num. Orden</td>
            <td><input name="ord_modulo" type="text" class="ip-login contenido" value="<?echo (empty($datos[7]))? $row[7] : $datos[7];?>" size="4" maxlength="4"></td>
            <td class="texto"><strong>Activo</strong></td>
            <td> 
              <? $this->insertaIndActivoFrm((empty($datos[8]))? $row[8] : $datos[8]); ?>
            </td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="td-lb-login"><hr width="100%" size="1">
        <b>DE LA UBICACI&Oacute;N (URL)</b> 
        <hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-lb-login">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td width="100" class="texto"><b>Path Absoluto</b></td>
            <td width="170"> <input name="des_path" type="text" class="ip-login contenido" value="<?echo (empty($datos[9]))? $row[9] : $datos[9];?>" size="30" maxlength="250"> 
            </td>
            <td width="100" class="texto"><strong>Target</strong></td>
            <td width="170">
              <? $this->insertaSelectLinkTarget('des_target', (empty($datos[10]))? $row[10] : $datos[10]); ?>
            </td>
          </tr>
          <tr> 
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto">Atributos Link</td>
            <td colspan="3" width="440"><input name="des_atributostarget" type="text" class="ip-login contenido" value="<?echo (empty($datos[11]))? $row[11] : $datos[11];?>" size="75" maxlength="250"></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td colspan="2" width="540">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="td-lb-login" width="540"><hr width="100%" size="1">
        <b>DE LOS USUARIOS CON DERECHO</b> 
        <hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td width="540">&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto" width="540"><b>Dependencia</b></td>
      <td> 
<?
		$this->insertaDependenciaFrm($id_dependencia,"id_dependencia",true,true);
?>
      </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td class="texto"><b>Sub Dependencia</b></td>
      <td> 
<?
		$this->insertaSubdependenciaFrm("id_subdependencia",true);
		$this->insertaSubdependenciaScript("document.${frmName}.id_subdependencia","document.${frmName}.id_dependencia",$id_subdependencia);
?>
      </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" width="540"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
<?
		$usuarios = $this->insertaUsuariosFrm($id_subdependencia);
?>
            <td width="250" align="center" class="texto">Usuarios ( <? echo $usuarios[0]; ?> 
              )</td>
            <td width="40" align="center">&nbsp;</td>
            <td width="250" align="center" class="texto"><b>Usuarios con Derechos</b></td>
          </tr>
          <tr> 
            <td width="250" align="center"> <select id="cod_usuario_muestra" name="cod_usuario_muestra[]" size="10" multiple class="ip-login contenido">
                <option>-------------------------------------</option>
                <? echo $usuarios[1]; ?> </select> </td>
            <td width="40" align="center"> 
<?
		$this->insertaScriptItemsSelectForm("document.forms['${frmName}'].elements['cod_usuario_muestra[]']", "document.forms['${frmName}'].elements['cod_usuarios[]']");
?>
            </td>
            <td width="250" align="center"> <select name="cod_usuarios[]" size="10" multiple class="ip-login contenido">
                <option>-------------------------------------</option>
                <?
		if(count($cod_usuario)>0){
			$this->consultaUsuariosModuloFrm($cod_usuario);
		}else{
			if(!is_null($id))
				$this->consultaUsuariosModuloFrm(NULL,$id);
		}
?>
              </select> </td>
          </tr>
        </table></td>
    </tr>
    <?
		if(!is_null($id)){
?>
    <tr> 
      <td class="texto" colspan="2" width="540">&nbsp; </td>
    </tr>
    <tr> 
      <td colspan="2" width="540" class="td-lb-login"><hr width="100%" size="1"> 
        <b>DE LA MODIFICACI&Oacute;N</b> <hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td class="texto" colspan="2" width="540">&nbsp; </td>
    </tr>
    <tr> 
      <td class="texto" colspan="2" width="540"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td class="texto" width="100"><b>Usuario</b></td>
            <td width="170" class="texto"> 
              <?=$row[14];?>
            </td>
            <td width="100">&nbsp;</td>
            <td width="170">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto" width="100">&nbsp;</td>
            <td width="170">&nbsp;</td>
            <td width="100">&nbsp;</td>
            <td width="170">&nbsp;</td>
          </tr>
          <tr> 
            <td class="texto" width="100"><b>Creaci&oacute;n</b></td>
            <td width="170" class="texto"> 
              <?=$row[12];?>
            </td>
            <td width="100" class="texto"><b> Modificaci&oacute;n</b></td>
            <td width="170" class="texto"> 
              <?=$row[13];?>
            </td>
          </tr>
          <tr> 
            <td class="texto" width="100"> <input type="hidden" name="id" value="<?=$id;?>"> 
            </td>
            <td width="170">&nbsp;</td>
            <td width="100">&nbsp;</td>
            <td width="170">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <?
		}
?>
    <tr> 
      <td class="texto" colspan="2" align="center">
	    <input type="submit" value="<? echo (empty($id)) ? "INGRESAR DATOS" : "MODIFICAR DATOS"; ?>" class="but-login" onClick="document.<?=$frmName;?>.accion.value=<? echo ($accion == ACCION_INSERTAR_FALSE) ? ACCION_INSERTAR_TRUE : ACCION_MODIFICAR_TRUE;?>;seleccionaItemSelectForm(2,document.forms['<?=$frmName;?>'].elements['cod_usuarios[]']);MM_validateForm('des_titulocorto','Titulo Corto','R','des_path','Path Absoluto','R','ord_modulo','','N');return document.MM_returnValue">
        &nbsp;&nbsp;&nbsp; <input type="reset" value="BORRAR DATOS" class="but-login"> 
      </td>
    </tr>
    <tr> 
      <td class="texto">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>