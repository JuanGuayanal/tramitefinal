<?php /* Smarty version 2.5.0, created on 2014-05-24 12:25:28
         compiled from correspondencia/configuracion/prioridad.tpl.php */ ?>
<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>TIPO DE PRIORIDAD PARA CORRESPONDENCIA</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=insertaPrioridadCorr">
        <p> 
			<label for="password">Prioridad</label>
			<input name="prioridad" required="required" type="text" class="std_input" />
		</p>
        <p>
          <button class="std_button" id="btn_CambiarContrasena" rel="titulo">Agregar Prioridad</button></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=actualizarPrioridadCorr">
<table class="std_grilla">
	<tr><th>N&deg;</th>
	<th>Prioridad</th><th colspan="2">Activo</th></tr>
<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['prioridad']) ? count($this->_tpl_vars['prioridad']) : max(0, (int)$this->_tpl_vars['prioridad']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<tr>
	<td><?php echo $this->_sections['i']['iteration']; ?>
<input type="hidden" name="arreglo[id_prioridad][]" value="<?php echo $this->_tpl_vars['prioridad'][$this->_sections['i']['index']]['id_prioridad']; ?>
" /></td>
    <td><input type="text" value="<?php echo $this->_tpl_vars['prioridad'][$this->_sections['i']['index']]['prioridad']; ?>
" name="arreglo[prioridad][]"/></td>
    <td><?php if ($this->_tpl_vars['prioridad'][$this->_sections['i']['index']]['activo'] == 1): ?><img src="/img/800x600/activo.png" /><?php else: ?><img src="/img/800x600/inactivo.png" /><?php endif; ?></td>
    <td><select class="sel_activar" name="arreglo[activo][]">
    <option value="1" <?php if ($this->_tpl_vars['prioridad'][$this->_sections['i']['index']]['activo'] == 1): ?>selected="selected"<?php endif; ?>>ACTIVO</option>
    <option value="0" <?php if ($this->_tpl_vars['prioridad'][$this->_sections['i']['index']]['activo'] == 0): ?>selected="selected"<?php endif; ?>>INACTIVO</option></select>
    </td>
    
</tr>
<?php endfor; endif; ?>
</table>
<div class="center"><p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Guardar Cambios</button></p></div>
</form>
</div>
<script src="/sitradocV3/js/src/correspondencia/configuracion/prioridad.js"></script>