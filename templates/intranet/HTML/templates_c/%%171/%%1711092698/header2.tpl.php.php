<?php /* Smarty version 2.5.0, created on 2014-12-23 11:24:55
         compiled from intranet/header2.tpl.php */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Intranet - <?php if ($this->_tpl_vars['titulo']): ?><?php echo $this->_tpl_vars['titulo']; ?>
<?php endif; ?></title>
<link href="/estilos/4/INI.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/WEB.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/GEN.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/NOT.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/EVE.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/JER/JER.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/MEN/MEN_01.css" rel="stylesheet" type="text/css" />
<link href="/estilos/4/ZOP/ZOP.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/styles/intranet.css" type="text/css">
<link rel="stylesheet" href="/styles/theme.css" type="text/css" />


<script type="text/javascript" src="/libreriajs/jquery.js"></script>
<script type="text/javascript" src="/libreriajs/scripts.js"></script>
<script type="text/javascript" src="/libreriajs/home_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/interna_jquery1.js"></script>
<script type="text/javascript" src="/libreriajs/menu_01_APS.js"></script>

<script src="/js/intranet.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/JSCookMenu.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/theme.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/mambojavascript.js" language="JavaScript1.2" type="text/javascript"></script>
<script src="/js/menuJS/tiny_mce.js" language="JavaScript1.2" type="text/javascript"></script>

<script src="/sitradocV3/js/plugins/DataTables-1.9.4/media/js/jquery.dataTables.min.js"></script>
<link href="/sitradocV3/js/plugins/DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/sitradocV3/css/estilos.css" type="text/css" />
	<link rel="stylesheet" href="/sitradocV3/js/plugins/jquery-ui-1.8.6/demos/demos.css">
    <link rel="stylesheet" href="/sitradocV3/js/plugins/jquery-ui-1.8.6/themes/smoothness/jquery-ui.css">
	<!--<script src="/sitradocV3/js/plugins/jquery-ui-1.8.6/external/jquery.bgiframe-2.1.2.js"></script>-->
	<script src="/sitradocV3/js/plugins/jquery-ui-1.8.6/ui/jquery-ui.js"></script>
    <script src="/sitradocV3/js/plugins/jquery-ui-1.8.6/ui/jquery.ui.datepicker.js"></script>
    <script src="/sitradocV3/js/plugins/jquery-ui-1.8.6/ui/jquery.ui.datepicker-es.js"></script>
	<!--<link rel="stylesheet" href="/sitradocV3/css/normalize.css" type="text/css" />-->
</head>

<body>
<div class="WEB_MAQUE_Alineacion1">
	<div class="WEB_MAQUE_Fondo1">
    	<div class="WEB_MAQUE_f1_c1">
        	<div class="WEB_CONTE_cabecera">
                	<div class="WEB_CONTE_bloqueSuperior1">
                   	  <div class="WEB_CONTE_bloque1 GEN_floatLeft">
<!--                   		<div class="WEB_CONTE_tituloSuperior" style="background-image:none;"></div>      09/04/2013  -->
                   		<div class="WEB_CONTE_tituloSuperior"><img src="/images/0/0/imagen_titulo_home.gif" alt="" width="500" height="35" /> </div>
                            <div class="WEB_CONTE_actualizacion"></div>
                      </div>
                        <div class="WEB_CONTE_bloque2 GEN_floatLeft">
                          <div class="WEB_CONTE_opcionesSuperior">
<ul id="menup">
                          	<li class="nivel1"><a href="/institucional/aplicativos/oad/sitradoc.php"><img src="/img/icono_inicio.gif" /><br />Inicio</a></li>
                            <li><a href="http://www.cunamas.gob.pe/" target="_blank"><img src="/img/icono_portal_MIDIS.jpg" /><br />Portal</a></li>
                            <li><a href="http://correo.cunamas.gob.pe/" target="_blank"><img src="/img/icono_correo.jpg" /><br />Correo</a></li>
                             <li><a href="#"><img src="/img/ayuda.jpg" /><br />Ayuda</a>
                             	
                             </li>
                            <li><a href="/exit.php"><img src="/img/icono_salir.jpg" /><br />Salir</a></li>
                          </ul>
                            <div class="GEN_clearLeft"></div>
                          </div>
                      </div>
                    <div class="GEN_clearLeft"></div>
                    </div>
                    <div class="WEB_CONTE_bloqueSuperior2">
                    	<div class="WEB_CONTE_usuario GEN_floatLeft">
                        	<div class="WEB_CONTE_usuarioParte1 GEN_floatLeft">
                        	  <p class="WEB_texto5">+   Bienvenido: <span class="WEB_texto6">&nbsp;&nbsp;<?php echo $this->_tpl_vars['userIntra']; ?>
@<?php echo $this->_tpl_vars['domiIntra']; ?>
</span></p>
                        	</div>
                            <div class="WEB_CONTE_usuarioParte2 GEN_floatLeft"></div>
                            <br class="GEN_clearLeft" />
                        </div>
<!--							<div class="WEB_CONTE_usuarioParte3 GEN_floatRight">
						<?php echo $this->_tpl_vars['groupPage']; ?>

				<a href="/<?php echo $this->_tpl_vars['sugePage']; ?>
"><img src="/img/lb_suge.gif" alt="Enviar sugerencias" width="76" height="17" name="ico_hea1" vspace="0" border="0"></a>&nbsp;&nbsp;&nbsp;
							</div>-->
                        <!-- <div class="WEB_CONTE_buscador GEN_floatLeft">
                                <input type="text" name="textfield" id="buscador" />
                                <div class="WEB_CONTE_buscadorBoton"></div>
                        </div> --> 
                        <div class="GEN_clearLeft"></div>
                    </div>
                	
                </div>
    	</div>
    </div>
    <div class="WEB_MAQUE_Fondo2">
    	<div class="WEB_MAQUE_f2_c1">
        	<div class="WEB_CONTE_barra">
            	<div class="WEB_CONTE_bloqueBarraIzquierdo">
            	  <!--<a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=Perfil_FormCambiarContrasena">CAMBIAR CONTRASEŅA</a>-->
            	</div>
                <div class="WEB_CONTE_bloqueBarraDerecho">
                  
                </div>
                <div class="GEN_clearBoth"></div>
            </div>
        </div>
    </div>
    <div class="WEB_MAQUE_Fondo3">
    	<div class="WEB_MAQUE_f3_c1">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="" bgcolor="#E5E5E5">&nbsp;</td>
    <td width="8" bgcolor="#E5E5E5">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#E5E5E5">&nbsp;</td>
    <td bgcolor="#FFFFFF">