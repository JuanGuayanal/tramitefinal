<?php /* Smarty version 2.5.0, created on 2014-03-05 09:27:56
         compiled from oad/tramite/showDetalles/printDetallesDoc.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'default', 'oad/tramite/showDetalles/printDetallesDoc.tpl.php', 120, false),)); ?><html>
<head>
<title>Documentos : <?php echo $this->_tpl_vars['doc']['desc']; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- <?php echo ' -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.Estilo1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; font-weight: bold; }
-->
</style>
<!-- '; ?>
 -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['IMPRIME_DETALLE_DIR']; ?>
&id=<?php echo $this->_tpl_vars['id']; ?>
"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">

	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="7" class="textoblack"><strong><img src="/img/ico-detalle.gif" width="24" height="24" hspace="4" align="absmiddle"><u>DETALLES 
            DEL DOCUMENTO PRINCIPAL</u></strong> <br> <strong><?php echo $this->_tpl_vars['nroExp']; ?>
&nbsp;&nbsp;</strong><br>
            <strong>C&oacute;digo Interno: <?php echo $this->_tpl_vars['id']; ?>
&nbsp;&nbsp;</strong></td>
        </tr>
        <tr> 
          <td width="135" colspan="2" class="textoblack"><strong>N&deg; Documento</strong></td>
          <td colspan="2" class="texto" width="110%"><?php if ($this->_tpl_vars['idTipoDoc'] != 4): ?><?php echo $this->_tpl_vars['nroExp']; ?>
<?php else: ?><?php echo $this->_tpl_vars['indicativo']; ?>
<?php endif; ?></td>
          <td width="75%" class="texto">&nbsp;</td>
          <td width="75%" class="texto"><strong>Fecha de Ingreso</strong></td>
          <td width="75%" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['fecRec'], 'NO ESPECIFICADO'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>

        </tr>
        <tr> 
          <td width="135" colspan="2" class="textoblack"><strong>Asunto</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_tpl_vars['asunto']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php if ($this->_tpl_vars['idTipoDoc'] != 4): ?>
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS 
              DE LA EMPRESA</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Raz&oacute;n Social</strong></td>
          <td colspan="2" class="texto"><?php echo $this->_tpl_vars['RazonSocial']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>RUC</strong></td>
          <td class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['ruc'], 'NO ESPECIFICADO'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Direcci&oacute;n Legal</strong></td>
          <td colspan="3" class="texto"><?php echo $this->_tpl_vars['direccion']; ?>
</td>
          <td class="texto"><strong>Tel&eacute;fono</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['telefono']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php if (( $this->_tpl_vars['email'] != "" && $this->_tpl_vars['email'] != ' ' )): ?>
        <tr> 
          <td colspan="2" class="textoblack"><strong>E-mail</strong></td>
          <td colspan="3" class="texto"><?php echo $this->_tpl_vars['email']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php endif; ?>
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS 
              GENERALES DEL DOCUMENTO</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>N&deg; de Documento</strong></td>
          <td width="110%" class="texto"><?php echo $this->_tpl_vars['nroExp']; ?>
</td>
          <td width="38%" class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Folios</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['folio']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Indicativo</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_tpl_vars['indicativo']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['anex']): ?>
		<tr> 
 <!--         <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>ANEXOS  DEL DOCUMENTO</strong></div></td>  23/04/2013  -->
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DOCUMENTOS ASOCIADOS</strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['anex']) ? count($this->_tpl_vars['anex']) : max(0, (int)$this->_tpl_vars['anex']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <tr> 
<!--          <td colspan="2" class="textoblack"><strong>Anexo Nro. <?php echo $this->_sections['i']['iteration']; ?>
</strong></td>     23/04/2013-->
          <td colspan="2" class="textoblack"><strong>Doc.Asociado Nro. <?php echo $this->_sections['i']['iteration']; ?>
</strong></td>
          <td width="110%" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['num'], 'No posee n�mero'); ?>
</td>
          <td width="38%" class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Ingreso </strong></td>
          <td class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['fecIng'], 'No especificado'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Remitente</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['remitente'], 'No especificado'); ?>
</td>
        </tr>
		 <tr> 
          <td colspan="2" class="textoblack"><strong>Tipo de Doc.</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['tip'], 'No especificado'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Nro. Doc.</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['obs'], 'No especificado'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Asunto</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['cont'], 'No especificado'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <!--
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        
        <tr> 
          <td colspan="2" class="textoblack"><strong>Recepcionado por</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['deperecep'], 'No especificado'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>-->
        <tr> 
          <td colspan="2" class="textoblack"><strong>Derivado a</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['anex'][$this->_sections['i']['index']]['depedest'], 'No especificado'); ?>
</td>
        </tr>
		 <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php endfor; endif; ?>
		<?php endif; ?>
		
       <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>SITUACI&Oacute;N 
              DEL DOCUMENTO</strong>fff</div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Situaci&oacute;n Actual</strong></td>
          <td colspan="2" class="texto"><?php if ($this->_tpl_vars['idEstado'] == 9): ?>NOTIFICADO<?php else: ?><?php if ($this->_tpl_vars['sitActual']): ?>Finalizado en <?php echo $this->_tpl_vars['sitActual']; ?>
<?php if ($this->_tpl_vars['control'] == 1): ?> EL <?php echo $this->_tpl_vars['fechaFinaDepe']; ?>
<?php endif; ?><?php else: ?>Pendiente(No ha sido finalizado)<?php endif; ?><?php endif; ?></td>
          <td class="texto"><?php if ($this->_tpl_vars['fechaMaxPlazo'] != ""): ?><strong><font color="#FF0000">Plazo M&aacute;ximo:</font></strong> <font color="#FF0000"><?php echo $this->_tpl_vars['fechaMaxPlazo']; ?>
</font><?php endif; ?></td>
          <td class="texto"><strong><?php if (! $this->_tpl_vars['sitActual']): ?>N&deg; de D&iacute;as en Tr&aacute;mite<?php endif; ?></strong></td>
          <td class="texto"><?php if (! $this->_tpl_vars['sitActual']): ?><?php echo $this->_tpl_vars['nroDias']; ?>
<?php endif; ?></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong><?php if (( $this->_tpl_vars['control'] == 1 || $this->_tpl_vars['coddep'] == 5 )): ?>Dependencia que efect�a el seguimiento<?php else: ?>Direcci&oacute;n Receptora<?php endif; ?></strong></td>
          <td colspan="3" class="texto"><?php echo $this->_tpl_vars['dependencia']; ?>
</td>
          <td class="texto"><strong><?php if ($this->_tpl_vars['nroDiasTupa']): ?>N&deg; de D&iacute;as catalogados 
            en TUPA<?php endif; ?></strong></td>
          <td class="texto"><?php if ($this->_tpl_vars['nroDiasTupa']): ?><?php echo $this->_tpl_vars['nroDiasTupa']; ?>
<?php endif; ?></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Fecha de aceptaci&oacute;n </strong></td>
          <td colspan="2" class="texto"><?php if (( $this->_tpl_vars['auditRecibido'] && $this->_tpl_vars['auditRecibido'] != ' a las ' )): ?><?php echo $this->_tpl_vars['auditRecibido']; ?>
<?php else: ?>No se ha recibido a&uacute;n el documento<?php endif; ?></td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong><?php if (( $this->_tpl_vars['nroDias'] && ! $this->_tpl_vars['sitActual'] && $this->_tpl_vars['idTipoDoc'] == 2 )): ?>Retraso<?php endif; ?></strong></td>
          <td class="texto"><?php if (( $this->_tpl_vars['nroDias'] && ! $this->_tpl_vars['sitActual'] && $this->_tpl_vars['idTipoDoc'] == 2 )): ?><?php if (( $this->_tpl_vars['Retrazo'] < 0 )): ?>No existe Retraso<?php else: ?><?php echo $this->_tpl_vars['Retrazo']; ?>
<?php endif; ?><?php endif; ?></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Avances</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['avance'], 'No posee avances'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Tratamiento</strong></td>
          <td colspan="5" class="texto"><?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['trat']) ? count($this->_tpl_vars['trat']) : max(0, (int)$this->_tpl_vars['trat']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> - <?php echo $this->_tpl_vars['trat'][$this->_sections['i']['index']]['desc']; ?>
<?php endfor; endif; ?></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php if ($this->_tpl_vars['user2']): ?>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Finalizado por </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['user2']; ?>
</td>
          <td class="texto">Observaciones</td>
          <td class="texto"><span class="item"><?php echo $this->_tpl_vars['observaciones']; ?>
</span></td>
          <td class="texto">Fecha de finalizaci&oacute;n </td>
          <td class="texto"><?php echo $this->_tpl_vars['fecha1']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Fecha de Archivaje </strong></td>
          <td class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['fecha2'], 'No se ha archivado'); ?>
</td>
          <td class="texto">Nivel 1 </td>
          <td class="texto"><span class="item"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['nivel1'], 'No se ha archivado'); ?>
</span></td>
          <td class="texto">Nivel 2 </td>
          <td class="texto"><span class="item"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['nivel2'], 'No se ha archivado'); ?>
</span> <span class="item"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['nivel3'], 'No se ha archivado'); ?>
</span></td>
        </tr>
		
	
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>

		<?php endif; ?>

<!--		<tr> 
		<td colspan="2"  class="textoblack"><strong>Estado:</strong></td>
		<td class="texto" colspan="5"><b>
				<span class="item">
				<?php if ($this->_tpl_vars['idEstado'] == 9): ?>
					NOTIFICADO
				<?php else: ?>
					<?php if ($this->_tpl_vars['sitActual']): ?>
						FINALIZADO EN <?php echo $this->_tpl_vars['sitActual']; ?>
 	<?php if ($this->_tpl_vars['control'] == 1): ?> EL <?php echo $this->_tpl_vars['fechaFinaDepe']; ?>
<?php endif; ?>
					<?php else: ?>
					PENDIENTE 
					<?php endif; ?>
				<?php endif; ?>
				</span>
				</b>
		</td>
		</tr>-->

		<?php if ($this->_tpl_vars['siglas_pendientes'] != ""): ?>
<!--		<tr> 
		  <td  colspan="2" class="textoblack"><strong>Pendiente en:</strong></td>
		  <td class="texto" colspan="5"><span class="item"><?php echo $this->_tpl_vars['siglas_pendientes']; ?>
&nbsp;&nbsp;</span></td>
		</tr>-->		
		<?php endif; ?>



		<?php if (( $this->_tpl_vars['resol'] || $this->_tpl_vars['noti'] )): ?>
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>DATOS GENERALES DE LA RESOLUCI&Oacute;N Y/O CORRESPONDENCIA(S) </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php if ($this->_tpl_vars['resol']): ?>
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['resol']) ? count($this->_tpl_vars['resol']) : max(0, (int)$this->_tpl_vars['resol']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <tr> 
          <td colspan="2" class="textoblack"><strong><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['tipResol']; ?>
</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['nroResol']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Firma </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['fFirma']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Sumilla</strong></td>
          <td colspan="3" class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['sumilla']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="2" class="textoblack"><strong>Fecha de Inicio </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['fInicio']; ?>
</td>
          <td class="texto">Fecha de Fin </td>
          <td class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['fFin']; ?>
</td>
          <td class="texto"><strong>Fecha de Publicaci&oacute;n </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['resol'][$this->_sections['i']['index']]['fPub']; ?>
</td>
        </tr>
		
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<?php endfor; endif; ?>
		<?php endif; ?>		
		<?php if ($this->_tpl_vars['noti']): ?>
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['noti']) ? count($this->_tpl_vars['noti']) : max(0, (int)$this->_tpl_vars['noti']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
        <tr> 
          <td colspan="2" class="textoblack"><strong><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['tipCorrespondencia']; ?>
</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['nroCor']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Destinatario</strong></td>
          <td colspan="3" class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['destinatario']; ?>
</td>
          <td class="texto"><span class="textoblack"><strong>Domicilio</strong></span></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['domicilio']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Tipo de Mensajer&iacute;a </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['tipMensajeria']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de entrega al Courier </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['fecEntCourier']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Estado de la Norificaci&oacute;n </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['estado']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Fecha de Notificaci&oacute;n </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['fecNoti']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="2" class="textoblack"><strong>Recibido por </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['nomPerActo']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto"><strong>Identificado con </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['noti'][$this->_sections['i']['index']]['perActo']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx">&nbsp;</td>
        </tr>
		<?php endfor; endif; ?>
		<?php endif; ?>
		<?php endif; ?>
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>FLUJO 
              ENTRE DEPENDENCIAS </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		<!--
        <tr> 
          <td class="textoblack"><strong>N&deg;</strong></td>
          <td class="textoblack"><strong>DEPENDENCIA</strong></td>
          <td class="textoblack"><strong>SUB DEPENDENCIA</strong></td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack"><strong>TIPO DE DOCUMENTO</strong></td>
          <td class="textoblack"><strong>ASUNTO</strong></td>
          <td class="textoblack"><strong>FOLIOS</strong></td>
          <td class="textoblack"><strong>OBSERVACI&Oacute;N</strong></td>
          <td class="textoblack"><strong>FECHA Y HORA</strong></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['Exp']) ? count($this->_tpl_vars['Exp']) : max(0, (int)$this->_tpl_vars['Exp']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
        <tr> 
          <td class="textoblack"><?php echo $this->_sections['i']['iteration']; ?>
</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['depDest']; ?>
</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack">&nbsp;</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['tipDoc']; ?>
&nbsp;<?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['ind']; ?>
</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['asu']; ?>
</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['folio']; ?>
</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['obs']; ?>
</td>
          <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['fecha']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <?php endfor; else: ?> 
        <tr> 
          <td colspan="9" class="textoblack"> <div align="center"><strong>No existe 
              informaci&oacute;n acerca del flujo del Documento</strong></div></td>
        </tr>
        <tr> 
          <td colspan="9" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <?php endif; ?>
        <tr> 
          <td colspan="2" class="texto">&nbsp;</td>
          <td colspan="7" class="texto">&nbsp;</td>
        </tr>
		--> 
        <tr> 
          <td colspan="7" class="texto"><table width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC">
              <tr> 
                <td class="textoblack"><strong>DOCUMENTO </strong></td>
                <td class="textoblack"><strong>INDICATIVO</strong></td>
                <td class="Estilo1">FECHA</td>
                <td class="textoblack" ><strong>ASUNTO</strong></td>
                <td class="textoblack"><strong>OBSERVACION</strong></td>
                <td class="textoblack"><strong>AVANCE</strong></td>
                <td class="textoblack"><strong>ORIGEN</strong></td>
				<td class="textoblack"><strong>DESTINO</strong></td>
                <td class="textoblack"><strong>MOTIVO DE FINALIZACION</strong></td>
              </tr>
              <!--<tr>  COMENTADO POR CESAR ORTIZ PUNTO 3,4,5 E1
                <td class="textoblack"><?php echo $this->_tpl_vars['claseDoc']; ?>
</td>
                <td class="textoblack" width="60"><?php echo $this->_tpl_vars['indicativo']; ?>

						<?php if ($this->_tpl_vars['idEstado'] == 9): ?>&nbsp;<?php else: ?>
							<?php if ($this->_tpl_vars['sitActual']): ?>&nbsp;<?php else: ?>					
								<?php if (( $this->_tpl_vars['contador_exp_sig'] == 0 )): ?><b>(*)</b><?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
				
				</td>
                <td class="textoblack"><?php echo $this->_tpl_vars['fecDer']; ?>
</td>
                <td class="textoblack" width="60"><?php echo $this->_tpl_vars['asunto']; ?>
</td>
                <td class="textoblack"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['obs'], 'No especificado'); ?>
<?php if (( $this->_tpl_vars['idTipoDoc'] == 4 && $this->_tpl_vars['fechaPlazo'] != "" )): ?>. Fecha de Plazo: <?php echo $this->_tpl_vars['fechaPlazo']; ?>
<?php endif; ?></td>
                <td class="textoblack"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['avance'], 'No especificado'); ?>
</td>
                <td class="textoblack"><?php echo $this->_tpl_vars['depOrigen']; ?>
</td>
				<td class="textoblack"><?php if ($this->_tpl_vars['depDestino']): ?><?php echo $this->_tpl_vars['depDestino']; ?>
<?php else: ?><?php echo $this->_tpl_vars['depDestino2']; ?>
<?php endif; ?></td>
              </tr>-->
        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['Exp']) ? count($this->_tpl_vars['Exp']) : max(0, (int)$this->_tpl_vars['Exp']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
                <tr> 
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['cla']; ?>
</td>
                  <td class="textoblack"><?php if (( $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['cla'] == 'SIN DOCUMENTO' && $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['depo'] == 'DM' && $this->_tpl_vars['claseDoc'] == 'OTROS' )): ?><?php echo $this->_tpl_vars['indicativo']; ?>
<?php else: ?><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['ind']; ?>
<?php endif; ?>
				  	<?php if (( $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['valRuta'] == 1 )): ?><a href="<?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['rutaI']; ?>
" target="_blank"><img src="/img/800x600/ico-acrobat.gif" alt="[ VER ARCHIVO ]" width="15" height="15" hspace="5" border="0" align="absmiddle" class="mano" onClick="hL(this);"></a><?php endif; ?>
					<?php if (( $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['sen_pendiente'] != "" )): ?><b><br /><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['sen_pendiente']; ?>
</b><?php endif; ?>
				  </td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['fDer']; ?>
</td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['asu']; ?>
</td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['obs']; ?>
<?php if ($this->_tpl_vars['Exp'][$this->_sections['i']['index']]['fPlazo'] != ""): ?>. 
                    Fecha de Plazo: <?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['fPlazo']; ?>
<?php endif; ?></td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['avance']; ?>
</td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['depo']; ?>
</td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['depd']; ?>
</td>
                  <td class="textoblack"><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['fecFin']; ?>
<br /><?php echo $this->_tpl_vars['Exp'][$this->_sections['i']['index']]['modFin']; ?>
</td>
                </tr>
			  <?php endfor; endif; ?>
            </table></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
		
        <tr> 
          <td colspan="7" bgcolor="#cedfea" class="textoblack"> <div align="center"><strong>FLUJO 
              ENTRE TRABAJADORES </strong></div></td>
        </tr>
        <tr> 
          <td colspan="7" class="texto"><table width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC">
              <tr>
			    <td class="textoblack"><strong>TRABAJADOR</strong></td> 
                <td class="textoblack"><strong>FECHA DE DERIVACI&Oacute;N </strong></td>
                <td class="Estilo1">FECHA DE ACEPTACI&Oacute;N </td>
				<td class="textoblack"><strong>ASUNTO</strong></td>
                <td class="textoblack" ><strong>OBSERVACION</strong></td>
                <td class="textoblack"><strong>AVANCE</strong></td>
                <td class="textoblack"><strong>DOCUMENTO GENERADO </strong></td>
				
              </tr>
        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['doc']) ? count($this->_tpl_vars['doc']) : max(0, (int)$this->_tpl_vars['doc']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
              <tr> 
                <td class="textoblack" width="60"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['nombre']; ?>
</td>
                <td class="textoblack"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['diaEnvio']; ?>
 <?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['horaEnvio']; ?>
</td>
                <td class="textoblack"><?php if ($this->_tpl_vars['doc'][$this->_sections['i']['index']]['diaRec']): ?><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['diaRec']; ?>
 <?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['horaRec']; ?>
<?php else: ?><?php if ($this->_tpl_vars['doc'][$this->_sections['i']['index']]['estado'] == 'V'): ?>Delegaci&oacute;n m&uacute;ltiple<?php else: ?>No lo ha recibido a&uacute;n<?php endif; ?><?php endif; ?></td>
                <td class="textoblack"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index']]['link'], ' '); ?>
<?php if (( $this->_tpl_vars['coddep'] != 5 )): ?><br>Acci&oacute;n: <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index']]['obsSecre'], ' '); ?>
<?php endif; ?></td>
				<td class="textoblack" width="60"><?php if (( $this->_tpl_vars['doc'][$this->_sections['i']['index_next']]['obs'] ) != ""): ?><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index_next']]['obs'], 'No Posee observaciones'); ?>
<?php else: ?><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index']]['obsSecre'], ' '); ?>
<?php endif; ?><?php if ($this->_tpl_vars['doc'][$this->_sections['i']['index']]['fPlazo'] != ""): ?>. Fecha de Plazo: <?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['fPlazo']; ?>
<?php endif; ?></td>
                <td class="textoblack"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index']]['avance'], 'No Posee avances'); ?>
</td>
                <td class="textoblack"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index_next']]['idOficio'], 'No ha generado documento'); ?>
</td>
              </tr>
			  <?php endfor; else: ?>
              <tr> 
                <td colspan="7" class="textoblack"><div align="center"><strong>No existe informaci&oacute;n 
                    acerca del flujo entre trabajadores </strong></div></td>
              </tr>
			  <?php endif; ?>
            </table></td>
        </tr>
        <tr> 
          <td colspan="7" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <b><u>INFORMES:</u></b><br>
            Oficina de Gesti&oacute;n Documentaria y Atenci&oacute;n al Ciudadano - Ministerio de Desarrollo e Inclusi&oacute;n Social <br> 
            <b>Telf. 209-8000 Anexo 4003 / 1001 / 1015</b><br>
            Email:<b> <a href="mailto:sitradoc@midis.gob.pe">sitradoc@midis.gob.pe</a></b><br> 
          </td>
        </tr>
        <tr> 
          <td colspan="12" class="textoblack"> <div align="right"><em class="textored"><strong>Actualizado 
              al <?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
</strong> </em></div></td>
        </tr>
        <tr> 
          <td colspan="12" align="right">&nbsp;</td>
        </tr>
      </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/images/0/0/logoCONVENIO_SITRADOC.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="400" height="30"></td>
	  </tr>
	</table>
    </td>
  </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
<?php echo '
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
// window.onafterprint = function() {window.close()}  06/06/2013
	window.onafterprint = function() {return false;}
function handle_error()
{
    window.alert(\'Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.\');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\\"WB\\" WIDTH=\\"0\\" HEIGHT=\\"0\\" CLASSID=\\"CLSID:");
    document.write(wbvers + "\\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    \' Just tidy up when we leave to be sure we aren\'t
    \' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    \' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

\' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
'; ?>

</body>
</html>