<?php /* Smarty version 2.5.0, created on 2013-05-07 10:07:13
         compiled from oad/tramite/nuevaVersionFormularios/formsOadaCorrespondencia/frmEntregaCourier.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<script language="JavaScript">
<!--
<?php echo '
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
'; ?>

-->
</script>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" >
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="4" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> <br>
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td colspan="2" class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 
      </td>
      <td colspan="5"><div align="center"><strong></strong></div></td>
    </tr>
	<tr> 
	  <td class="item"> <strong>Fec. Entrega al Courier</strong></td>
		  <td colspan="3" align="left"><select name="dia_ing" class="ipseln" id="dia_ing">
		  <?php echo $this->_tpl_vars['selDiaIng']; ?>

			</select> <select name="mes_ing" class="ipseln" id="mes_ing">
		  <?php echo $this->_tpl_vars['selMesIng']; ?>

			</select> <select name="anyo_ing" class="ipseln" id="anyo_ing">
		  <?php echo $this->_tpl_vars['selAnyoIng']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha en que se entrega al Courier." width="20" height="20" align="top"> 
		  </td>
	</tr>
			<tr>
				<td class="item"><strong>Tipo de Mensajer&iacute;a</strong></td>
				<td colspan="3" class="item" align="left"><input name="radio2" type="radio" value="1" checked>&nbsp;<strong>Mensajer&iacute;a Local</strong>&nbsp;&nbsp;
				<input type="radio" name="radio2" value="0">&nbsp;<strong>Mensajer&iacute;a Nacional </strong></td>
			</tr>
    <tr> 
      <td colspan="4" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4"> 
		<input name="bbSubmit" type="Submit" class="submitV2" value="Entregar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_NOTIFICACION']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_NOTIFICACION']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_NOTIFICACION']; ?>
');return document.MM_returnValue"> 
		<input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['ENTREGA_COURIER']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
		<input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['ids']) ? count($this->_tpl_vars['ids']) : max(0, (int)$this->_tpl_vars['ids']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<input name="ids[]" type="hidden" id="ids[]" value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">
		<?php endfor; endif; ?>
	  </td>
    </tr>
  </table>
</form>