<?php /* Smarty version 2.5.0, created on 2013-01-18 11:23:30
         compiled from objsform/select.tpl.php */ ?>
  <?php echo $this->_tpl_vars['jscript']; ?>
 
  <?php if ($this->_tpl_vars['none']): ?>
  <option value="<?php echo $this->_tpl_vars['none']['val']; ?>
"><?php echo $this->_tpl_vars['none']['label']; ?>
</option>
  <?php endif; ?>
  <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['options']) ? count($this->_tpl_vars['options']) : max(0, (int)$this->_tpl_vars['options']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
  <option value="<?php echo $this->_tpl_vars['options'][$this->_sections['i']['index']]['val']; ?>
" <?php echo $this->_tpl_vars['options'][$this->_sections['i']['index']]['sel']; ?>
><?php echo $this->_tpl_vars['options'][$this->_sections['i']['index']]['label']; ?>
</option>
  <?php endfor; endif; ?>