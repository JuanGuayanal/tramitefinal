<?php /* Smarty version 2.5.0, created on 2014-06-04 17:09:06
         compiled from correspondencia/reporteXLS2.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'string_format', 'correspondencia/reporteXLS2.tpl.php', 43, false),)); ?>	<table>
    	<tr>
        	<td colspan="2" rowspan="2"><img src="http://tramite.midis.gob.pe/img/pie-logo.gif"></td>
        </tr>
    </table>
	<table border="1">
    <?php if ($this->_tpl_vars['coddep'] == 43): ?>
    	<tr>
        	<td bgcolor="#CCCCCC">GU&Iacute;A DE RECEPCI&Oacute;N</td>
			<td bgcolor="#CCCCCC">GU&Iacute;A DE ADMISI&Oacute;N</td>
            <td bgcolor="#CCCCCC">FECHA DE RECOJO</td>
            <td bgcolor="#CCCCCC">TIPO DE DOC</td>
            <td bgcolor="#CCCCCC">DOCUMENTO</td>
            <td bgcolor="#CCCCCC">DEPENDENCIA</td>
            <td bgcolor="#CCCCCC">CONSIGNADO</td>
            <td bgcolor="#CCCCCC">DIRECCI&Oacute;N</td>
            <td bgcolor="#CCCCCC">DPTO</td>
            <td bgcolor="#CCCCCC">PROVINCIA</td>
            <td bgcolor="#CCCCCC">DISTRITO</td>
            <td bgcolor="#CCCCCC">&Aacute;MBITO</td>
            <td bgcolor="#CCCCCC">PRIORIDAD</td>
            <td bgcolor="#CCCCCC">PENALIDAD</td>
            <td bgcolor="#CCCCCC">FECHA DE RECEPCI&Oacute;N</td>
            <td bgcolor="#CCCCCC">RETORNO DE CARGO</td>
            <td bgcolor="#CCCCCC">DIAS DE RETRASO</td>
            <td bgcolor="#CCCCCC">ESTADO DE LA DILIGENCIA</td>
            <td bgcolor="#CCCCCC">FECHA NOTIFICACI&Oacute;N</td>
            <td bgcolor="#CCCCCC">MOTIVO</td>
            <td bgcolor="#CCCCCC">OBSERVACIONES</td>
            <td bgcolor="#CCCCCC">USUARIO</td>
            <td bgcolor="#CCCCCC">REFERENCIA</td>
            <td bgcolor="#CCCCCC">USUARIO DESCARGA</td>
            <td bgcolor="#CCCCCC">FECHA DESCARGA</td>
            <td bgcolor="#CCCCCC">FORMA ENVIO</td>
            <td bgcolor="#CCCCCC">ESTADO DOCUMENTO</td>
            <td bgcolor="#CCCCCC">PLAZO DE ENTREGA</td>
            <td bgcolor="#CCCCCC">PLAZO DE RETORNO DE CARGO</td>
        </tr>
<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<tr>
        	<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['guia_recepcion']; ?>
</td>
			<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['guia_admision']; ?>
</td>
            <td><?php echo $this->_run_mod_handler('string_format', true, $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecEntCourier'], "%s"); ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['claseDoc']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numDoc']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['remitente']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destinatario']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['domicilio']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['departamento']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['provincia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['distrito']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['tipoMensajeria']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['prioridad']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['penalidad']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecRec']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecCargo']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['retraso']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estadoDiligencia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecNotificacion']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['obs']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['usuario']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['referencia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['usuario2']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecDescarga']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['formaEnvio']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estadoDoc']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['tiempo_entrega']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['tiempo_devolucion']; ?>
</td>
        </tr>
<?php endfor; endif; ?>
 	<?php else: ?>
    	<tr>
            <td bgcolor="#CCCCCC">FECHA DE RECOJO</td>
            <td bgcolor="#CCCCCC">TIPO DE DOC</td>
            <td bgcolor="#CCCCCC">DOCUMENTO</td>
            <td bgcolor="#CCCCCC">DEPENDENCIA</td>
            <td bgcolor="#CCCCCC">CONSIGNADO</td>
            <td bgcolor="#CCCCCC">DIRECCI&Oacute;N</td>
            <td bgcolor="#CCCCCC">DPTO</td>
            <td bgcolor="#CCCCCC">PROVINCIA</td>
            <td bgcolor="#CCCCCC">DISTRITO</td>
            <td bgcolor="#CCCCCC">&Aacute;MBITO</td>
            <td bgcolor="#CCCCCC">PRIORIDAD</td>
            <td bgcolor="#CCCCCC">FECHA DE RECEPCI&Oacute;N</td>
            <td bgcolor="#CCCCCC">RETORNO DE CARGO</td>
            <td bgcolor="#CCCCCC">ESTADO DE LA DILIGENCIA</td>
            <td bgcolor="#CCCCCC">FECHA NOTIFICACI&Oacute;N</td>
            <td bgcolor="#CCCCCC">MOTIVO</td>
            <td bgcolor="#CCCCCC">OBSERVACIONES</td>
            <td bgcolor="#CCCCCC">USUARIO</td>
            <td bgcolor="#CCCCCC">REFERENCIA</td>
            <td bgcolor="#CCCCCC">FORMA ENVIO</td>
            <td bgcolor="#CCCCCC">ESTADO DOCUMENTO</td>
        </tr>
<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<tr>
            <td><?php echo $this->_run_mod_handler('string_format', true, $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecEntCourier'], "%s"); ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['claseDoc']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numDoc']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['remitente']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destinatario']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['domicilio']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['departamento']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['provincia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['distrito']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['tipoMensajeria']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['prioridad']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecRec']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecCargo']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estadoDiligencia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecNotificacion']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['obs']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['usuario']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['referencia']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['formaEnvio']; ?>
</td>
            <td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estadoDoc']; ?>
</td>
        </tr>
<?php endfor; endif; ?>
    <?php endif; ?>
    </table>
    