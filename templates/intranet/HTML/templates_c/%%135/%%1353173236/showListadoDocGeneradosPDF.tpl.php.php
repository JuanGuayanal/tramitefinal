<?php /* Smarty version 2.5.0, created on 2013-12-17 11:12:32
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/reportes/showListadoDocGeneradosPDF.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'default', 'oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/reportes/showListadoDocGeneradosPDF.tpl.php', 39, false),)); ?><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<!-- HEADER LEFT "$LOGOIMAGE" -->
<!-- FOOTER LEFT "<?php echo $this->_tpl_vars['fechaGen']; ?>
" -->
<!-- REPORTE DE DOCUMENTOS DERIVADOS A OTRA DEPENDENCIA -->
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS GENERADOS EN LA DEPENDENCIA DE ACUERDO A LA FECHA DE SALIDA <?php if ($this->_tpl_vars['desFechaIni'] != $this->_tpl_vars['desFechaFin']): ?>ENTRE EL
				<?php echo $this->_tpl_vars['desFechaIni']; ?>
 Y EL <?php echo $this->_tpl_vars['desFechaFin']; ?>
<?php else: ?>DEL <?php echo $this->_tpl_vars['desFechaFin']; ?>
<?php endif; ?></font></strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
    					<td width="21%" bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
    					<td width="8%" bgcolor="#999999" class="textoblack"> <div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
   					   <td width="20%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
   					   <td width="5%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA DE DERIVACI&Oacute;N</font></strong></div></td>
   					   <td width="5" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
    				   <td width="8%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">OFICINA DESTINO</font></strong></div></td>
    				   <td width="8%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ACCIONES</font></strong></div></td>
					   <td width="20%" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">DESTINATARIO DE CORRESPONDENCIA</font></strong></div></td>
  					 </tr>
  					 
  					<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  					 <tr> 
    					<td width="5%" class="item"><div align="center"><font size="4" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></div></td>
    					<td width="21%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['tipDoc']; ?>
</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind']; ?>
</font></td>
    					<td width="20%" class="item"><font size="4" face="Arial"><font size="4" face="Arial"><?php if ($this->_tpl_vars['list'][$this->_sections['i']['index']]['asunto'] == ""): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['asunto2']; ?>
<?php else: ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['asunto']; ?>
<?php endif; ?></font></font></td>
    					<td width="5%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecCrea']; ?>
</font></td>
    					<td width="5" class="item"><font size="4" face="Arial"><?php if ($this->_tpl_vars['list'][$this->_sections['i']['index']]['ref'] == "" && $this->_tpl_vars['list'][$this->_sections['i']['index']]['numTram'] == ""): ?><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['list'][$this->_sections['i']['index']]['refsinRep'], 'No especificado'); ?>
<?php else: ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ref']; ?>
<br><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['numTram']; ?>
<?php endif; ?></font></td>
    					<td width="8%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['dep']; ?>
</font></td>
                        <td width="8%" class="item"><font size="4" face="Arial"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['accionesHojaDerivacion']; ?>
</font></font></td>
						<td width="20%" class="item"><font size="4" face="Arial"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['obs']; ?>
</font></font></td>
  					 </tr>
  					<?php endfor; else: ?> 
  					 <tr> 
    					<td colspan="<?php if ($this->_tpl_vars['sit'] == 2): ?>9<?php else: ?>9<?php endif; ?>" class="textored" width="100%"><div align="center"><strong><font size="4" face="Arial">NO SE HAN ENCONTRADO RESULTADOS</font></strong></div></td>
  					 </tr>
  					<?php endif; ?> 
		  	      </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
</table>
</body>
</html>