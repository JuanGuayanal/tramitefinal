<?php /* Smarty version 2.5.0, created on 2014-11-06 15:38:09
         compiled from utilitarios/frmResponderVariosDocumentos.tpl.php */ ?>
<script src="/sitradocV3/js/src/utilitarios/frmResponderVariosDocumentos.js"></script>
<br />
<input type="hidden" value="<?php echo $this->_tpl_vars['coddep']; ?>
" id="i_coddep" />
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php" id="frm_responder">
<table width="725" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
<tr>
	<td colspan="2" class="item-sep"><strong>Responder con un solo documento varios documentos.</strong></td>
</tr>
<tr>
	<td class="item" colspan="2" align="left"><button id="std_btn_buscar_documento" class="submitV2">Buscar Documento(s)</button></td>
</tr>
<tr>
	<td colspan="2">
<div>
	<div class="stp_opt_tabla_lista">
      	<div class="left">
        	<span id="t_detalle_documento" class="std_det">Cantidad de documentos: <strong>0</strong></span>
        </div>
        <div class="rigth">
      		<span id="t_eliminar_documento" class="std_opt">Eliminar marcados</span>
        </div>
        <div class="limpiar"></div>
      </div>
      <div class="str_div_lista200">
      
            <table id="t_documentos" width="100%" class="str_tabla_lista">
                <thead>
                    <tr>
                        <th width="70">DOCUMENTO</th>
                        <th width="190">INDICATIVO</th>
                        <th>ASUNTO</th>
                      	<th width="60">FECHA</th>
                        <th width="20"><input type="checkbox" id="t_documento_seleccionar_todos"></th>
                    </tr>
                </thead>
                <tbody id="body_t_documentos"></tbody>
            </table>
       </div>
</div>
</td>
<tr> 
	<td colspan="2"><hr width="100%" size="1"></td>
</tr>
</tr>
<tr>
	<td class="item" align="left"> <strong>Clase de Documento</strong></td>
    <td align="left"><select class="ipseln" id="idClaseDoc" name="idClaseDoc"><option value="0"></option></select></td>
</tr>
<tr id="id_tipo_tratamiento" align="left">
	<td></td>
	<td><table width="500" align="left" id="acc"><?php echo $this->_tpl_vars['acciones']; ?>
</table></td>
</tr>
<tr>
	<td class="item" align="left"><strong>Asunto</strong></td>
    <td align="left"><textarea name="asunto" cols="80" rows="3" class="iptxtn" id="asunto"></textarea></td>
</tr>
<tr>
	<td class="item" align="left"><strong>Observaciones</strong></td>
    <td class="item" align="left"><textarea name="observaciones" cols="65" rows="5" class="iptxtn" id="observaciones"></textarea></td>
</tr>
<tr> 
	<td colspan="2"><hr width="100%" size="1"></td>
</tr>
<tr>
	<td class="item" align="left"><strong>Destino(s):</strong></td>
    <td class="item" align="left"><select class="ipseln" id="idDependencias" name="idDependencias"><option value="0"></option></select><button class="submitV2" id="b_agregar_destinatario">+</button></td>
</tr>				 
<tr>
	<td class="td-encuesta" align="left" colspan="2">
      <div class="stp_opt_tabla_lista">
      	<div class="left">
        	<span id="t_detalle_destinatraio" class="std_det">Cantidad de destinatarios: <strong>0</strong></span>
        </div>
        <div class="rigth">
      		<span id="t_eliminar_destinatraio" class="std_opt">Eliminar marcados</span>
        </div>
        <div class="limpiar"></div>
      </div>
      <div class="str_div_lista200">
            <table id="t_destinatarios" width="100%" class="str_tabla_lista">
                <thead>
                    <tr>
                        <th>DEPENDENCIA</th>
                        <th><input type="checkbox" id="t_destinatario_seleccionar_todos"></th>
                    </tr>
                </thead>
                <tbody id="body_t_destinatarios"></tbody>
            </table>
       </div>
	  </td>
</tr>
<tr> 
	<td colspan="2"><hr width="100%" size="1"></td>
</tr>
<tr> 
	<td colspan="2" align="center"><input type="submit" id="responder" class="submitV2" value="Responder"/> &nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onclick="MM_goToURL('parent','/institucional/aplicativos/oad/sitradocV2/index.php?accion=searchDocDir&amp;menu=SumarioDir&amp;subMenu=searchDocDir');return document.MM_returnValue"></td>
</tr>
</table>
<input name="accion" type="hidden" id="accion" value="DerivaMultDocMultDest">
</form>