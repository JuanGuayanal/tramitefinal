<?php /* Smarty version 2.5.0, created on 2013-05-22 15:51:10
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/utilitarios/frmGeneraReiterativo.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'date_format', 'oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/utilitarios/frmGeneraReiterativo.tpl.php', 358, false),)); ?><?php echo $this->_tpl_vars['jscript']; ?>


<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
<?php echo '
function CalendarHide(){
	MM_showHideLayers(\'popCalIni\',\'\',\'hide\')
	MM_showHideLayers(\'popCalFin\',\'\',\'hide\')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function cargar(pForm,a)
{
		pForm.action="index.php?"+a;
	    pForm.submit();
}
function ConfirmaRegistro(pForm){
	if(confirm(\'¿Desea Guardar este Registro?\')){
		MM_validateForm(\'idClaseDoc\',\'Clase de Documento\',\'sel\',\'dependencia\',\'Dependencia\',\'sel\',\'asunto\',\'Asunto\',\'R\',\'observaciones\',\'Observaciones\',\'R\''; ?>
<?php if ($this->_tpl_vars['dependencia'] == 7 && $this->_tpl_vars['radio'] == 1): ?>,'domicilio','Domicilio','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'<?php endif; ?>);
		return document.MM_returnValue;<?php echo '
	}else
		return false;
}
 '; ?>

  function AddTrab(a)<?php echo '{'; ?>

	if(document.MM_returnValue)<?php echo '{'; ?>

		var frm = document.<?php echo $this->_tpl_vars['frmName']; ?>

		var j = frm.destinatario[frm.destinatario.selectedIndex].value
		
		
		if(j==99999)<?php echo '{'; ?>

			frm.wantsnewDest.value =1
				frm.tipPersonaNew.value = frm.tipPersona[frm.tipPersona.selectedIndex].value
				frm.tipIdentNew.value = frm.tipIdent[frm.tipIdent.selectedIndex].value
				frm.rucNew.value = frm.ruc.value			
		<?php echo '}'; ?>
else<?php echo '{'; ?>

				frm.tipPersonaNew.value = 55//Asignando un valor cualquiera 
				frm.tipIdentNew.value = 55//Asignando un valor cualquiera 
				frm.rucNew.value = 55//Asignando un valor cualquiera 			
			<?php echo '}'; ?>

		
			var kk = frm.otroDomi[frm.otroDomi.selectedIndex].value
			if(kk==99999)<?php echo '{'; ?>

				frm.wantsnewDomi.value =1
			<?php echo '}'; ?>
else<?php echo '{'; ?>

				frm.wantsnewDomi.value =2
			<?php echo '}'; ?>
		
		
		
		
		frm.idDestNew.value = frm.destinatario[frm.destinatario.selectedIndex].value
		frm.codDepaNew.value = frm.codDepa[frm.codDepa.selectedIndex].value
		frm.codProvNew.value = frm.codProv[frm.codProv.selectedIndex].value
		frm.codDistNew.value = frm.codDist[frm.codDist.selectedIndex].value
		frm.domicilioNew.value = frm.domicilio.value
		frm.personaNew.value = frm.observaciones.value
		frm.action = frm.action + a
		frm.accion.value = '<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
'
		frm.submit()
	<?php echo '}'; ?>
else
		return document.MM_returnValue
<?php echo '}'; ?>


-->
</script>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onsubmit="return ConfirmaRegistro(document.<?php echo $this->_tpl_vars['frmName']; ?>
)">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="5" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td colspan="4" class="item-sep"><strong>Generaci&oacute;n de un documento a una dependencia de SITRADOC en base a una referencia que se encuentra fuera de la dependencia</strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="texto td-encuesta" align="left"><label> 
              <input name="GrupoOpciones1" type="radio" value="1" <?php if (( $this->_tpl_vars['GrupoOpciones1'] == 1 || ! $this->_tpl_vars['GrupoOpciones1'] )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
              <strong>Externo - Expediente</strong></label>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" <?php if (( $this->_tpl_vars['GrupoOpciones1'] == 2 )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
              <strong>Documento Interno</strong></label></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el <?php if (( $this->_tpl_vars['GrupoOpciones1'] == 1 || ! $this->_tpl_vars['GrupoOpciones1'] )): ?>Nro. de Tr&aacute;mite<?php else: ?>Nro. de documento<?php endif; ?></strong></td>
      <td align="left"><?php if (( $this->_tpl_vars['GrupoOpciones1'] == 1 || ! $this->_tpl_vars['GrupoOpciones1'] )): ?>
	  			<input name="nroTD" type="text" class="iptxtn" id="nroTD" value="<?php echo $this->_tpl_vars['nroTD']; ?>
" size="30">-
				<select name="anyo3" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2002"<?php if ($this->_tpl_vars['anyo3'] == 2002): ?> selected<?php endif; ?>>2002</option>
					<option value="2003"<?php if ($this->_tpl_vars['anyo3'] == 2003): ?> selected<?php endif; ?>>2003</option>
					<option value="2004"<?php if ($this->_tpl_vars['anyo3'] == 2004): ?> selected<?php endif; ?>>2004</option>
					<option value="2005"<?php if ($this->_tpl_vars['anyo3'] == 2005): ?> selected<?php endif; ?>>2005</option>
					<option value="2006"<?php if ($this->_tpl_vars['anyo3'] == 2006): ?> selected<?php endif; ?>>2006</option>
					<option value="2007"<?php if ($this->_tpl_vars['anyo3'] == 2007): ?> selected<?php endif; ?>>2007</option>
					<option value="2008"<?php if ($this->_tpl_vars['anyo3'] == 2008): ?> selected<?php endif; ?>>2008</option>
					<option value="2009"<?php if ($this->_tpl_vars['anyo3'] == 2009): ?> selected<?php endif; ?>>2009</option>
					<option value="2010"<?php if ($this->_tpl_vars['anyo3'] == 2010): ?> selected<?php endif; ?>>2010</option>
					<option value="2011"<?php if ($this->_tpl_vars['anyo3'] == 2011): ?> selected<?php endif; ?>>2011</option>
					<option value="2012"<?php if ($this->_tpl_vars['anyo3'] == 2012): ?> selected<?php endif; ?>>2012</option>-->
					<option value="2013"<?php if ($this->_tpl_vars['anyo3'] == 2013): ?> selected<?php endif; ?>>2013</option>
				</select>
	      <?php else: ?>
	  			<select name="tipoDocc" class="ipseln" id="select">
        			<?php echo $this->_tpl_vars['selTipoDocc']; ?>

				</select>
				<input name="numero2" type="text" class="iptxtn" value="<?php echo $this->_tpl_vars['numero2']; ?>
" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option>
					<!--<option value="2004"<?php if ($this->_tpl_vars['anyo2'] == 2004): ?> selected<?php endif; ?>>2004</option>
					<option value="2005"<?php if ($this->_tpl_vars['anyo2'] == 2005): ?> selected<?php endif; ?>>2005</option>
					<option value="2006"<?php if ($this->_tpl_vars['anyo2'] == 2006): ?> selected<?php endif; ?>>2006</option>
					<option value="2007"<?php if ($this->_tpl_vars['anyo2'] == 2007): ?> selected<?php endif; ?>>2007</option>
					<option value="2008"<?php if ($this->_tpl_vars['anyo2'] == 2008): ?> selected<?php endif; ?>>2008</option>
					<option value="2009"<?php if ($this->_tpl_vars['anyo2'] == 2009): ?> selected<?php endif; ?>>2009</option>
					<option value="2010"<?php if ($this->_tpl_vars['anyo2'] == 2010): ?> selected<?php endif; ?>>2010</option>
					<option value="2011"<?php if ($this->_tpl_vars['anyo2'] == 2011): ?> selected<?php endif; ?>>2011</option>
					<option value="2012"<?php if ($this->_tpl_vars['anyo2'] == 2012): ?> selected<?php endif; ?>>2012</option>-->
					<option value="2013"<?php if ($this->_tpl_vars['anyo2'] == 2013): ?> selected<?php endif; ?>>2013</option>					
				</select> -MIDIS/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					<?php echo $this->_tpl_vars['selSiglasDep2']; ?>

       			</select>
	  	  <?php endif; ?>&nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="submitV2" onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php if (( ! $this->_tpl_vars['exito'] || $this->_tpl_vars['exito'] == 0 )): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene el acceso para generar un reiterativo!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['idDocumento'] > 0): ?>
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><?php if ($this->_tpl_vars['numTram']): ?><?php echo $this->_tpl_vars['numTram']; ?>
<br><?php endif; ?><?php echo $this->_tpl_vars['asunto2']; ?>
</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Ubicaci&oacute;n Actual </strong> </td>
      <td align="left"><?php echo $this->_tpl_vars['UbiActual']; ?>
</td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Clase de Documento</strong></td>
      <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
        <?php echo $this->_tpl_vars['selClaseDoc']; ?>

        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td height="25" class="item" ><strong>Derivar a:</strong></td>
      <td align="left"><select name="dependencia" class="ipseln" id="dependencia" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
        <?php echo $this->_tpl_vars['selDependencia']; ?>

        </select>
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php if (( $this->_tpl_vars['dependencia'] == 7 )): ?>
    <tr> 
      <td class="item"><strong>&iquest;Genera correspondencia ?</strong> </td>
      <td align="left"><input name="radio" type="radio" value="1" <?php if (( $this->_tpl_vars['radio'] == 1 )): ?> checked<?php endif; ?> onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')"> 
        <strong>Si</strong> <input type="radio" name="radio" value="0" <?php if (( $this->_tpl_vars['radio'] == 0 || ! $this->_tpl_vars['radio'] )): ?> checked<?php endif; ?> onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')"><strong>No</strong></td>
      <td colspan="3"><div align="center"></div></td>
    </tr>
	<?php endif; ?>	
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="80" rows="3" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['asunto']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php if ($this->_tpl_vars['radio'] != 1): ?>
    <tr> 
      <td class="item"><strong><?php if (( $this->_tpl_vars['dependencia'] == 7 && $this->_tpl_vars['radio'] == 1 )): ?>Destinatario<?php else: ?>Observaciones<?php endif; ?></strong> </td>
      <td align="left"><textarea name="observaciones" cols="80" rows="3" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['observaciones']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
	<!--
	<?php if (( $this->_tpl_vars['dependencia'] == 7 && $this->_tpl_vars['radio'] == 1 )): ?>
    <tr> 
      <td class="texto td-encuesta"><strong>Ubigeo</strong> </td>
      <td class="item">
	  		<select name="codDepa" class="ipseln" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
					<?php echo $this->_tpl_vars['selDepa']; ?>

            </select>
			<select name="codProv" class="ipseln" id="select2" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
					<?php echo $this->_tpl_vars['selProv']; ?>

            </select>
			<select name="codDist" class="ipseln" id="select" >
					<?php echo $this->_tpl_vars['selDist']; ?>

              </select>
	  </td>
      <td><strong></strong></td>
    </tr>	
    <tr> 
      <td class="item"><strong>Domicilio</strong> </td>
      <td><textarea name="domicilio" cols="80" rows="3" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['domicilio']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
	-->
	<?php if (( $this->_tpl_vars['dependencia'] == 7 && $this->_tpl_vars['radio'] == 1 )): ?>
    <tr> 
      <td colspan="4"><hr width="100%" size="1"></td>
    </tr>	
    <tr> 
      <td class="item-sep" colspan="4"><strong>M&oacute;dulo para generar una o muchas notificaciones y/o correspondencias.</strong></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta"><a name="der3" id="der3"></a><strong>Posee datos?</strong> </td>
      <td class="item" align="left"><label> 
              <input name="GrupoOpciones2" type="radio" value="1" <?php if (( $this->_tpl_vars['GrupoOpciones2'] == 1 )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
              <strong>B&uacute;squeda por Raz&oacute;n Social</strong></label>
			  <label> 
              <input name="GrupoOpciones2" type="radio" value="2" <?php if (( $this->_tpl_vars['GrupoOpciones2'] == 2 )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
              <strong>B&uacute;squeda por RUC</strong></label></td>
      <td><strong></strong></td>
    </tr>
    <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['soft']) ? count($this->_tpl_vars['soft']) : max(0, (int)$this->_tpl_vars['soft']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
	<tr> 
      <td class="texto td-encuesta"><strong></strong> </td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['RZ']; ?>
<br /><?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['desDepartamento']; ?>
&nbsp;<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['desProvincia']; ?>
&nbsp;<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['desDistrito']; ?>
<br />
	  <?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['departamento']; ?>
<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['provincia']; ?>
<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['distrito']; ?>
:<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['domicilio']; ?>
<br />
	  <?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['t']; ?>
<br /><?php if (( $this->_tpl_vars['soft'][$this->_sections['i']['index']]['id'] == 99999 )): ?><?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['tipPersona']; ?>
-<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['tipIdent']; ?>
- Nro. Documento: <?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['ruc']; ?>
<?php endif; ?></td>
      <td class="item"><img src="/img/800x600/ico.eliminar.gif" width="12" height="12" border="0" alt="Eliminar" onClick="hL(this);submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')" class="mano"> 
              <input name="idDest[]" type="hidden" id="idDest[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['id']; ?>
">
			  <input name="personas[]" type="hidden" id="personas[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['RZ']; ?>
">
			  <input name="domicilios[]" type="hidden" id="domicilios[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['domicilio']; ?>
">
			  <input name="departamentos[]" type="hidden" id="departamentos[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['departamento']; ?>
">
			  <input name="provincias[]" type="hidden" id="provincias[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['provincia']; ?>
">
			  <input name="distritos[]" type="hidden" id="distritos[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['distrito']; ?>
">
			  <input name="nuevoDomicilios[]" type="hidden" id="nuevoDomicilios[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['t']; ?>
">
			  <input name="tiposPersonas[]" type="hidden" id="tiposPersonas[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['tipPersona']; ?>
">
			  <input name="tiposIdentificaciones[]" type="hidden" id="tiposIdentificaciones[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['tipIdent']; ?>
">
			  <input name="rucs[]" type="hidden" id="rucs[]" value="<?php echo $this->_tpl_vars['soft'][$this->_sections['i']['index']]['ruc']; ?>
">
			  			  </td>
      <td></td>
    </tr>
	<?php endfor; endif; ?>	
	<?php endif; ?>
	<?php if (( $this->_tpl_vars['GrupoOpciones2'] > 0 && $this->_tpl_vars['radio'] == 1 )): ?>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item" align="left"><input name="RZoRUC" type="text" class="iptxtn" value="<?php echo $this->_tpl_vars['RZoRUC']; ?>
" size="35" maxlength="50">
	  	&nbsp;&nbsp;<input type="button" name="BuscaRZ" value="BuscarRZ" class="submitV2" onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
	  </td>
      <td><strong></strong></td>
    </tr>
	<?php endif; ?>
	<?php if (( $this->_tpl_vars['radio'] == 1 && $this->_tpl_vars['GrupoOpciones2'] > 0 )): ?>
    <tr> 
      <td class="texto td-encuesta"><strong><a name="der2" id="der2"></a>Escoja destinatario</strong> </td>
      <td colspan="2" class="item" align="left"><select name="destinatario" class="ipsel2" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
		<?php echo $this->_tpl_vars['selDestinatario']; ?>

		<?php if (( $this->_tpl_vars['GrupoOpciones2'] > 0 && $this->_tpl_vars['RZoRUC'] == "" )): ?><option value="none" >Seleccione una opción</option><?php endif; ?>
		<?php if (( $this->_tpl_vars['GrupoOpciones2'] > 0 && $this->_tpl_vars['RZoRUC'] != "" && $this->_tpl_vars['BuscarRZ'] == 1 )): ?><option value="99999" <?php if ($this->_tpl_vars['destinatario'] == '99999'): ?> selected<?php endif; ?>>Agregar nuevo destinatario</option><?php endif; ?>
              </select></td>
    </tr>
		<?php if ($this->_tpl_vars['destinatario'] == 99999): ?>
		<tr> 
		  <td class="texto td-encuesta"><strong>Llenar datos</strong> </td>
		  <td colspan="2" class="item" align="left"><select name="tipPersona" class="ipseln" id="tipPersona" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
        <?php echo $this->_tpl_vars['selTipoPersona']; ?>

        </select>&nbsp;<select name="tipIdent" class="ipseln" id="tipIdent" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der3')">
        <?php echo $this->_tpl_vars['selTipoIdentificacion']; ?>

        </select>&nbsp;
		<input name="ruc" type="text" class="iptxtn" id="ruc" value="<?php echo $this->_tpl_vars['ruc']; ?>
" size="20" maxlength="<?php if ($this->_tpl_vars['tipIdent'] == '1'): ?>8<?php elseif ($this->_tpl_vars['tipIdent'] == '8'): ?>11<?php else: ?>25<?php endif; ?>" onKeyPress="solo_num();"></td>
		</tr>
		<?php endif; ?>	
	<?php endif; ?>
	<?php if ($this->_tpl_vars['radio'] == 1): ?>		
    <tr> 
      <td class="item"><strong><?php if ($this->_tpl_vars['radio'] == 1): ?>Destinatario<?php else: ?>Observaciones<?php endif; ?></strong> </td>
      <td class="item" align="left"><textarea name="observaciones" cols="65" rows="5" class="iptxtn" id="observaciones" onKeyPress="solo_carac();"><?php if (! $this->_tpl_vars['observaciones']): ?>Sin Observaciones<?php else: ?><?php echo $this->_tpl_vars['observaciones']; ?>
<?php endif; ?></textarea></td>
      <td><?php if ($this->_tpl_vars['radio'] == 1): ?><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div><?php endif; ?></td>
    </tr>
	<?php endif; ?>
	<?php if (( $this->_tpl_vars['destinatario'] > 0 && $this->_tpl_vars['radio'] == 1 )): ?>
    <tr> 
      <td class="texto td-encuesta"><a name="der5" id="der5"></a><strong>Otras Direcciones</strong> </td>
      <td class="item" align="left"><select name="otroDomi" class="ipsel2" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der5')">
		<?php echo $this->_tpl_vars['selDomAlt']; ?>

		<option value="99999" <?php if ($this->_tpl_vars['otroDomi'] == '99999'): ?> selected<?php endif; ?>>Agregar nueva dirección</option>
              </select></td>
    </tr>
	<?php endif; ?>	
	<?php if ($this->_tpl_vars['dependencia'] == 7 && $this->_tpl_vars['radio'] == 1): ?>
	
    <tr> 
      <td class="texto td-encuesta"><a name="der4" id="der4"></a><strong>Ubigeo</strong> </td>
      <td class="item" align="left">
	  		<select name="codDepa" class="ipsel2" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der4')">
					<?php echo $this->_tpl_vars['selDepa']; ?>

            </select>
			<select name="codProv" class="ipsel2" id="select2" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
');cargar(document.<?php echo $this->_tpl_vars['frmName']; ?>
,'#der4')">
					<?php echo $this->_tpl_vars['selProv']; ?>

            </select>
			<select name="codDist" class="ipsel2" id="select" >
					<?php echo $this->_tpl_vars['selDist']; ?>

              </select>
	  </td>
      <td><strong></strong></td>
    </tr>	
	
    <tr> 
      <td class="texto td-encuesta"><strong>Domicilio</strong> </td>
      <td class="item" align="left"><textarea name="domicilio" cols="45" rows="4" class="iptxtn" id="domicilio" onKeyPress="solo_carac();"><?php if (! $this->_tpl_vars['domicilio']): ?>Sin Domicilio<?php else: ?><?php echo $this->_tpl_vars['domicilio']; ?>
<?php endif; ?></textarea>
	  <!--<input name="bDestinatario" type="button" class="submitV2" id="bDestinatario" value="Agregar" onClick="MM_validateForm('observaciones','Destinatario','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel'<?php if ($this->_tpl_vars['destinatario'] == 99999): ?>,'tipPersona','Tipo Persona','Sel','tipIdent','Tipo Identificación','Sel','ruc','RUC o DNI','R'<?php endif; ?>);return AddTrab('#der3');"> 
              <input name="idDestNew" type="hidden" id="idDestNew">
			  <input name="codDepaNew" type="hidden" id="codDepaNew">
			  <input name="codProvNew" type="hidden" id="codProvNew">
			  <input name="codDistNew" type="hidden" id="codDistNew">
			  <input name="personaNew" type="hidden" id="personaNew">
			  <input name="domicilioNew" type="hidden" id="domicilioNew">
			  <?php if ($this->_tpl_vars['destinatario'] == 99999): ?><input name="wantsnewDest" type="hidden" id="wantsnewDest"><?php endif; ?>
			  <input name="wantsnewDomi" type="hidden" id="wantsnewDomi">			  	  
			  
			  	<input name="tipPersonaNew" type="hidden" id="tipPersonaNew">
				<input name="tipIdentNew" type="hidden" id="tipIdentNew">
				<input name="rucNew" type="hidden" id="rucNew">-->
	  </td>
      <td><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
	<?php if (( $this->_tpl_vars['radio'] != 1 && $this->_tpl_vars['dependencia'] > 0 && $this->_tpl_vars['dependencia'] != 49 )): ?>
		<tr>
		  <td>&nbsp;</td>
		<td class="item" align="left"><input name="OpcionConocimiento" type="checkbox" id="checkbox" value="1" <?php if ($this->_tpl_vars['OpcionConocimiento'] == 1): ?> checked <?php endif; ?> onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERA_REITERATIVO']; ?>
')">
			&iquest;Es de conocimiento? (Si hace check se finalizará automáticamente en la oficina receptora.)</td>
		<td></td>
		</tr>
	<?php endif; ?>			
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha de Plazo</strong> </td>
      <td class="item" align="left"><input name="desFechaIni" type="text" class="iptxtn" id="desFechaIni" value="<?php echo $this->_tpl_vars['desFechaIni']; ?>
" tabindex="4" onKeyPress="ninguna_letra();" />
    		<input name="fecDesembarqueIni" type="hidden" id="fecDesembarqueIni" value="<?php echo $this->_tpl_vars['datos']['fecDesIni']; ?>
" />&nbsp;&nbsp;
				<a href="javascript:;"  onclick="fPopCalendar('Ini',document.<?php echo $this->_tpl_vars['frmName']; ?>
.desFechaIni,document.<?php echo $this->_tpl_vars['frmName']; ?>
.desFechaIni,popCalIni,<?php echo $this->_run_mod_handler('date_format', true, time(), '%Y'); ?>
,'<?php echo date('n') ?>',<?php echo $this->_run_mod_handler('date_format', true, time(), '%e'); ?>
);return false"><img src="/img/800x600/calendario/ico-cal.gif" alt="[Haga Click para visualizar calendario ayuda]" width="16" height="16" border="0" hspace="5" /></a></td>
      <td><strong></strong></td>
    </tr>
    <tr>
      <td class="lb1Frm"></td>
      <td><div id="popCalIni" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td nowrap="nowrap" class="tdSep1"></td>
      <td class="lb1Frm"></td>
      <td><div id="popCalFin" style="position:absolute; width:200px; height:115px; z-index:1; visibility:hidden" onclick="event.cancelBubble=true"></div></td>
      <td></td>
    </tr>	
	
	<?php endif; ?>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="submitV2" value="Generar" > 
<!--        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['SUMARIO_DIR']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_DIR']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['SUMARIO_DIR']; ?>
');return document.MM_returnValue">  22/05/2013 -->
       &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_REASIGNA_DOCUMENTO']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['FRM_REASIGNA_DOCUMENTO']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_REASIGNA_DOCUMENTO']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['GENERA_REITERATIVO']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idDocumento" type="hidden" id="idDocumento" value="<?php echo $this->_tpl_vars['idDocumento']; ?>
">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		<?php if ($this->_tpl_vars['GrupoOpciones2'] > 0): ?><input name="BuscarRZ" type="hidden" id="BuscarRZ" value="1"><?php endif; ?>
		 </td>
    </tr>
  </table>
</form>