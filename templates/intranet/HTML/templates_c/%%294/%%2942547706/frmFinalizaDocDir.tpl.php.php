<?php /* Smarty version 2.5.0, created on 2013-01-23 11:38:55
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/frmFinalizaDocDir.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<br>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" >
  <table width="680" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	 <tr> 
      <td colspan="5" class="item-sep" align="left"><strong>Los siguientes datos (*) s�lo se llenar�n para el caso de Expedientes.</strong></td>
    </tr>
	<tr> 
      <td class="item"><strong>&iquest;Finalizado definitivamente?</strong></td>
      <td class="item" align="left"><input name="finalizacionDefinitiva" type="radio" value="1">Si
	  		&nbsp;<input name="finalizacionDefinitiva" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Estado del Expediente</strong></td>
      <td class="item" align="left"><input name="estadoExpediente" type="radio" value="1" checked>Favorable/ Firme/ Consentido para el administrado
	  		&nbsp;<input name="estadoExpediente" type="radio" value="2">Desfavorable
			&nbsp;<input name="estadoExpediente" type="radio" value="3">No aplica
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>&iquest;Sujeto a Proceso Judicial?</strong></td>
      <td class="item" align="left"><input name="esttadoProcesoJudicial" type="radio" value="1">Si
	  		&nbsp;<input name="esttadoProcesoJudicial" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
	
    <tr> 
      <td class="item"> <strong>Observaciones</strong></td>
      <td align="left"> <textarea name="observaciones" cols="85" rows="4" class="iptxtn" id="observaciones" ><?php if (! $this->_tpl_vars['observaciones']): ?><?php if (( $this->_tpl_vars['codigoDependenciaF'] == 13 || $this->_tpl_vars['codigoDependenciaF'] == 39 )): ?>Finalizado por aplicaci�n de silencio administrativo positivo<?php else: ?>Sin Observaciones<?php endif; ?><?php else: ?><?php echo $this->_tpl_vars['observaciones']; ?>
<?php endif; ?></textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="textored"> <div align="left"><strong>(*): Datos a registrarse s&oacute;lo para el caso de Expedientes.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Si finaliza definitivamente el expediente, no se podr&aacute; reactivar ni se podr&aacute; agregar adjuntos al expediente.
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las observaciones son obligatorias para cualquier tipo de documento.</strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Finalizar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['BUSCA_DOCDIR']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_DIR']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['BUSCA_DOCDIR']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['FINALIZA_DOCDIR']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idFin" type="hidden" id="idFin" value="<?php echo $this->_tpl_vars['idFin']; ?>
">
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['ids']) ? count($this->_tpl_vars['ids']) : max(0, (int)$this->_tpl_vars['ids']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
		
        <!--<input name="ids[]" type="hidden" id="ids[]" value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">-->
		<input name="ids[]" type="hidden" id="ids[]" value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">
		
		<?php endfor; endif; ?>
		
		<input name="FechaIni" type="hidden" id="FechaIni" value="<?php echo $this->_tpl_vars['FechaIni']; ?>
">
		<input name="FechaFin" type="hidden" id="FechaFin" value="<?php echo $this->_tpl_vars['FechaFin']; ?>
">
		<?php if ($this->_tpl_vars['tipEstado'] > 0): ?><input name="tipEstado" type="hidden" id="tipEstado" value="<?php echo $this->_tpl_vars['tipEstado']; ?>
"><?php endif; ?>
		<input name="checkTodos" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkTodos']; ?>
">
		<input name="checkAsunto" type="hidden" id="checkAsunto" value="<?php echo $this->_tpl_vars['checkAsunto']; ?>
">
		<input name="checkRazon" type="hidden" id="checkRazon" value="<?php echo $this->_tpl_vars['checkRazon']; ?>
">
		<input name="checkTrab" type="hidden" id="checkTrab" value="<?php echo $this->_tpl_vars['checkTrab']; ?>
">		
		
		<?php if ($this->_tpl_vars['tipDocumento']): ?><input name="tipDocumento" type="hidden" id="tipDocumento" value="<?php echo $this->_tpl_vars['tipDocumento']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['tipBusqueda']): ?><input name="tipBusqueda" type="hidden" id="tipBusqueda" value="<?php echo $this->_tpl_vars['tipBusqueda']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecIniDir']): ?><input name="fecIniDir" type="hidden" id="fecIniDir" value="<?php echo $this->_tpl_vars['fecIniDir']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecFinDir']): ?><input name="fecFinDir" type="hidden" id="fecFinDir" value="<?php echo $this->_tpl_vars['fecFinDir']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['asunto2']): ?><input name="asunto2" type="hidden" id="asunto2" value="<?php echo $this->_tpl_vars['asunto2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['indicativo2']): ?><input name="indicativo2" type="hidden" id="indicativo2" value="<?php echo $this->_tpl_vars['indicativo2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['observaciones2']): ?><input name="observaciones2" type="hidden" id="observaciones2" value="<?php echo $this->_tpl_vars['observaciones2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['nroTD']): ?><input name="nroTD" type="hidden" id="nroTD" value="<?php echo $this->_tpl_vars['nroTD']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['page']): ?><input name="page" type="hidden" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['siglasDep'] && $this->_tpl_vars['siglasDep'] != 'none' )): ?><input name="siglasDep" type="hidden" id="siglasDep" value="<?php echo $this->_tpl_vars['siglasDep']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['tipodDoc'] && $this->_tpl_vars['tipodDoc'] != 'none' )): ?><input name="tipodDoc" type="hidden" id="tipodDoc" value="<?php echo $this->_tpl_vars['tipodDoc']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['procedimiento']): ?><input name="procedimiento" type="hidden" id="procedimiento" value="<?php echo $this->_tpl_vars['procedimiento']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['anyo3'] && $this->_tpl_vars['anyo3'] != 'none' )): ?><input name="anyo3" type="hidden" id="anyo3" value="<?php echo $this->_tpl_vars['anyo3']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['codigoInterno']): ?><input name="codigoInterno" type="hidden" id="codigoInterno" value="<?php echo $this->_tpl_vars['codigoInterno']; ?>
"><?php endif; ?>
		 </td>
    </tr>
  </table>
</form>