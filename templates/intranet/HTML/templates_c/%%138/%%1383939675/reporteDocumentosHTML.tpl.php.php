<?php /* Smarty version 2.5.0, created on 2016-09-21 10:11:38
         compiled from reportes/reporteDocumentosHTML.tpl.php */ ?>
<html>
<head>
<title>LISTADO DE DOCUMENTOS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- <?php echo ' -->
<style type="text/css">
<!--
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
.textored a{
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #A0A0A0; 
	line-height: normal
}
.textored a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sitra {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #003366;
	background-color: #FEF7C1;
	border: 1px #E9E9E9;
}
.tabla{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
	border-collapse:collapse;
	width:98%;
}
.tabla tr th{background: #FEF7C1;border:solid 1px #999; height:25px;}
.tabla tr td{ border:solid 1px #CCC;}
@media print {
	 @page { margin: 10px; }
	.oculto, #oculto{ display:none; }
	.tabla thead{display:table-header-group;}
	.tabla tbody{display:table-row-group;}
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function desArchivar(idDocumento){
    $.ajax({
            async:false,
            url: \'/institucional/aplicativos/oad/sitradocV2/index.php?accion=rearchivaDoc\',
            type:\'POST\',
            data:{id:idDocumento},
            success: function(data){
                if(data.trim()==\'EXITO\'){
                    alert(\'Transacci\\u00f3n relizada satisfactoriamente\');
                    window.location.reload();
                }
                else alert(\'Se produjo un error al momento de realizar la transacci\\u00f3n.\');
            }
        });
    
}
//-->
</script>
<script src="/sitradocV3/js/plugins/jquery-ui-1.8.6/jquery-1.4.3.js"></script>
<script>
	$(document).ready(function(){
		$(\'#imprimir_seleccionados\').click(function(e){
			e.preventDefault();
			$(\'.oculto\').removeClass(\'oculto\');
			$(\'.tabla tbody tr\').each(function() {
                if($(this).children(\'td:last-child\').children(\'input\').is(\':checked\')){
					
				}else{
					$(this).addClass(\'oculto\');
				}
				$(this).children(\'td:last-child\').addClass(\'oculto\');
            });
			$(\'.tabla thead tr th:last-child\').addClass(\'oculto\');
			window.print();
		})
		$(\'#imprimir\').click(function(e){
			e.preventDefault();
			$(\'.oculto\').removeClass(\'oculto\');
			$(\'.tabla tbody tr\').each(function() {
                $(this).children(\'td:last-child\').addClass(\'oculto\');
            });
			$(\'.tabla thead tr th:last-child\').addClass(\'oculto\');
			window.print();
		})
		$(\'#reporte_pdf\').click(function(e){
			e.preventDefault();
			$(\'input[name=tipo_reporte]\').val(1);
			$(\'form[name=frmPrint]\').submit();
		})
		$(\'#reporte_excel\').click(function(e){
			e.preventDefault();
			$(\'input[name=tipo_reporte]\').val(2);
			$(\'form[name=frmPrint]\').submit();
		})
	});
</script>
<!-- '; ?>
 -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<form name="frmPrint" action="/institucional/aplicativos/oad/sitradocV2/index.php" method="post" target="_blank">
<input name="accion" type="hidden" id="accion" value="ListadoReportess">
<input name="desde_reporte" type="hidden" value="1">
<input name="tipo_reporte" type="hidden" value="0">

<input name="tipoDocumento" type="hidden" value="<?php echo $this->_tpl_vars['tipoDocumento']; ?>
">
<input name="status" type="hidden" value="<?php echo $this->_tpl_vars['estado']; ?>
">
<input name="coddep" type="hidden" value="<?php echo $this->_tpl_vars['coddep']; ?>
">
<input name="destino" type="hidden" value="<?php echo $this->_tpl_vars['destino']; ?>
">
<input name="clase_doc" type="hidden" value="<?php echo $this->_tpl_vars['clase_doc']; ?>
">
<input name="trabajador" type="hidden" value="<?php echo $this->_tpl_vars['trabajador']; ?>
">
<input name="desFechaIni" type="hidden" value="<?php echo $this->_tpl_vars['fecini']; ?>
">
<input name="desFechaFin" type="hidden" value="<?php echo $this->_tpl_vars['fecfin']; ?>
">
<input name="especialista" type="hidden" value="<?php echo $this->_tpl_vars['especialista']; ?>
">

<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tbody><tr id="oculto"> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA" class="contenido"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr> 
            <td class="texto"><b><a href="#" id="imprimir_seleccionados"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir lo seleccionado</a><a href="#" id="imprimir"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a>&nbsp;<a href="#" id="reporte_pdf"><img src="/img/800x600/ico-acrobat.gif" width="20" height="20" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir en PDF</a>&nbsp;<a href="#" id="reporte_excel"><img src="/img/800x600/ico_excel2.jpg" width="23" height="23" hspace="2" vspace="2" border="0" align="absmiddle">Exportar 
              a Excel</a></b></td>
         <td align="right" class="texto"><b><a href="javascript:window.close()">Cerrar</a>&nbsp;</b></td>
        </tr>
      </tbody></table>
	 </td>
  </tr>
  <tr> 
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        <tbody><tr> 
          <td align="left" bgcolor="#FFFFFF"><img src="/images/0/0/logo.gif" width="272" height="30"></td>
          <td align="right" bgcolor="#FFFFFF"></td>
        </tr>
      </tbody></table>
	</td>
  </tr>
  <tr> 
    <td height="8" bgcolor="#FFFFFF">
	   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
       	
        	<tbody><tr align="center"> 
         		<td height="40" colspan="2" class="textoblack"><b>REPORTE DE DOCUMENTOS
                <?php if ($this->_tpl_vars['estado'] == 1): ?>PENDIENTES
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> DE TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> DE <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 2): ?>PENDIENTES DELEGADOS / DERIVADOS A 
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?>TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?><?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>GENERADOS<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>GENERADOS / DERIVADOS / ENVIADO<br><?php echo $this->_tpl_vars['session_trab']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>ARCHIVADOS
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?>DE TODOS LOS TRABAJADORES<?php endif; ?>
					<?php if ($this->_tpl_vars['trabajador'] > 0): ?>POR <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>ARCHIVADOS POR<br><?php echo $this->_tpl_vars['session_trab']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 11): ?>PENDIENTES DEL CONGRESO DE LA REPUBLICA
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> EN TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> EN <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 11): ?>PENDIENTES DEL CONGRESO DE LA REPUBLICA  
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> EN TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> EN <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                </b></td>
        	</tr>
            <tr> 
                <td align="center">
                	<!--INICIO CONTENIDO--> 
                    <?php if ($this->_tpl_vars['estado'] == 1): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE CREACI&Oacute;N<br>REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th>
                        <th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><!--<th>AVANCE</th>--><th></th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td>
                        <!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td>-->
                        <td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 11): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS</th><th>FECHA DE CREACI&Oacute;N<br>REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><!--<th>DESTINATARIO DE<br>CORRESPONDENCIA</th>--><!--<th>AVANCE</th>--><th></th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['correspondencia']; ?>
</td>-->
                        <!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td>-->
                        <td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 2): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA CREACI&Oacute;N DE REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>UBICACI&Oacute;N</th>
                          <th>OBSERVACIONES Y/O REFERENCIAS</th><th></th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS</th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><!--<th>DESTINATARIO DE<br>CORRESPONDENCIA</th>--><!--<th>ESTADO</th>--><th></th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['correspondencia']; ?>
</td>--><!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estado']; ?>
</td>--><td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS</th><th>EXPEDIENTE</th><th>FECHA DE DERIVACI&Oacute;N</th><th>DESTINO</th><th></th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>TRABAJADOR</th><th>MOTIVO DE<br>ARCHIVACION</th><!--<th>AVANCE</th>--><th></th><th></th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['trabajador_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo_fin']; ?>
</td><!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td>--><td><input type="checkbox"></td><td><input type="button" value="Desarchivar" onclick="desArchivar(<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
)" /></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>MOTIVO DE<br>ARCHIVO</th><!--<th>AVANCE</th>--><th></th><th></th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo_fin']; ?>
</td><!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td>--><td><input type="checkbox"></td><td><input type="button" value="BTN" onclick="desArchivar(<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
)"/></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 5): ?>
                    <table class="tabla">
                        <thead><tr><th>N&deg;</th><th>ESTADO</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE CREACI&Oacute;N<br>REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><!--<th>AVANCE</th>--><th></th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estado']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['padre']; ?>
" target="_blank"><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</a></td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td>
                        <!--<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td>-->
                        <td><input type="checkbox"></td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
					<!--FIN CONTENIDO-->  
                </td>
            </tr>
		   </tbody></table>
		 </td>
	</tr>
  	<tr> 
    	<td> 
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
        		<tbody><tr> 
          			<td align="left" bgcolor="#FFFFFF" class="textored"><strong><i>Actualizado al <?php echo $this->_tpl_vars['fechaGen']; ?>
</i></strong></td>
          			<td align="right" bgcolor="#FFFFFF"></td>
        		</tr>
      		</tbody></table>
		</td>
  	</tr>
</tbody></table>
</form>

</body>