<?php /* Smarty version 2.5.0, created on 2015-01-19 16:54:15
         compiled from oad/tramite/nuevaVersionFormularios/formsTrabajadores/utilitarios/frmAnulaDocTrab.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>


<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
<?php echo '
function CalendarHide(){
	MM_showHideLayers(\'popCalIni\',\'\',\'hide\')
	MM_showHideLayers(\'popCalFin\',\'\',\'hide\')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm(\'�Desea Anular este Registro?\')){
		MM_validateForm(\'observaciones\',\'Observaciones\',\'R\');
		return document.MM_returnValue;
	}else
		return false;
}
 '; ?>

-->
</script>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onsubmit="return ConfirmaRegistro(document.<?php echo $this->_tpl_vars['frmName']; ?>
)">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="5" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?>
    <tr> 
      <td colspan="4" class="item-sep"><strong>Anulaci&oacute;n de un documento creado por el trabajador (S&oacute;lo si fuese el &uacute;ltimo documento generado y no haya sido aceptado).</strong></td>
    </tr>	 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el Nro. de documento</strong></td>
      <td align="left"><select name="tipoDocc" class="ipseln" id="select">
        			<?php echo $this->_tpl_vars['selTipoDocc']; ?>

				</select>
				<input name="numero2" type="text" class="iptxtn" value="<?php echo $this->_tpl_vars['numero2']; ?>
" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">TODOS</option><?php echo $this->_tpl_vars['selAnio']; ?>

				</select> 
				-MIDIS/PNCM/<?php echo $this->_tpl_vars['siglasDepe2']; ?>

	  	  &nbsp;&nbsp;
	      <input type="button" name="Busca" value="Buscar" class="std_button" onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_ANULA_DOC_TRAB']; ?>
')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php if (( ! $this->_tpl_vars['exito'] || $this->_tpl_vars['exito'] == 0 )): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene opci&oacute;n para anular el documento!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	<?php endif; ?>
    <?php if ($this->_tpl_vars['estado'] == 1): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>El documento ya fue ACEPTADO.</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['estado'] == 2): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>El documento no es el &uacute;ltimo generado.</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <?php endif; ?>
    <?php if ($this->_tpl_vars['estado'] == 3): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>No se existe el documento.</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <?php endif; ?>
	<?php if ($this->_tpl_vars['idDocumento'] > 0): ?>
    <tr> 
      <td class="item"><strong>Datos del documento</strong> </td>
      <td align="left"><?php echo $this->_tpl_vars['claseDocumento']; ?>
 <?php echo $this->_tpl_vars['indicativo']; ?>
 <?php echo $this->_tpl_vars['auditmod']; ?>
 <strong><?php if ($this->_tpl_vars['idEstadoDoc'] == 5): ?>FINALIZADO<?php else: ?>ACTIVO<?php endif; ?></strong>
	  		<br><?php if ($this->_tpl_vars['contador'] > 1): ?><strong>No se podr�a anular ya que ya fue enviado a otro trabajador</strong><?php endif; ?>
	  </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="80" rows="4" class="iptxtn" id="textarea" readonly="readonly"><?php echo $this->_tpl_vars['asunto']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td align="left"><textarea name="observaciones" cols="80" rows="5" class="iptxtn" id="textarea" readonly="readonly"><?php echo $this->_tpl_vars['observaciones']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>	
	<?php endif; ?>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"><?php if ($this->_tpl_vars['estado'] == 0 && ! ( ! $this->_tpl_vars['exito'] || $this->_tpl_vars['exito'] == 0 )): ?><input name="bSubmit" type="Submit" class="std_button" value="Anular" ><?php endif; ?>
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_TRAB']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['ANULA_DOC_TRAB']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idDocumento" type="hidden" id="idDocumento" value="<?php echo $this->_tpl_vars['idDocumento']; ?>
">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		<input name="contador" type="hidden" id="contador" value="<?php echo $this->_tpl_vars['contador']; ?>
">
		<input name="contador2" type="hidden" id="contador2" value="<?php echo $this->_tpl_vars['contador2']; ?>
">
		 </td>
    </tr>
  </table>
</form>