<?php /* Smarty version 2.5.0, created on 2013-02-14 11:40:21
         compiled from oad/tramite/showDetalleDoc.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'default', 'oad/tramite/showDetalleDoc.tpl.php', 133, false),)); ?><html>
<head>
<title>Documentos : <?php echo $this->_tpl_vars['doc']['desc']; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- <?php echo ' -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
-->
</style>
<!-- '; ?>
 -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF"> 
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="2" class="textoblack"><strong><?php echo $this->_tpl_vars['descripcion']; ?>
&nbsp;&nbsp;<?php echo $this->_tpl_vars['indicativo']; ?>
</strong></td>
        </tr>
        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['doc']) ? count($this->_tpl_vars['doc']) : max(0, (int)$this->_tpl_vars['doc']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
        <tr> 
          <td class="textoblack">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Tipo de Documento</strong></td>
          <td width="75%" class="texto"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['claseDoc']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Nro de Documento</strong></td>
          <td width="75%" class="texto"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['nroDoc']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td width="130" class="textoblack"><strong>Asunto</strong></td>
          <td width="75%" class="texto"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['asunto']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Observaciones</strong></td>
          <td class="texto"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['doc'][$this->_sections['i']['index']]['obs'], 'NO ESPECIFICADO'); ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Fecha de Creaci&oacute;n</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['fecCreacion']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="textoblack"><strong>Usuario</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['doc'][$this->_sections['i']['index']]['usuario']; ?>
</td>
        </tr>
        <tr> 
          <td colspan="2" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <?php endfor; else: ?> 
        <tr> 
          <td colspan="2" class="texto"><div align="center"><strong>No se tiene 
              información de los adjuntos a este Documento</strong></div></td>
        </tr>
        <?php endif; ?> 
        <tr> 
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
</body>
</html>