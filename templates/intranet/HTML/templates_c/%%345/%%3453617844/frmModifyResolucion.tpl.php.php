<?php /* Smarty version 2.5.0, created on 2013-03-20 14:39:25
         compiled from oad/tramite/nuevaVersionFormularios/resoluciones/frmModifyResolucion.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<script language="JavaScript">
<!--
<?php echo '
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm(\'�Desea Guardar este Registro?\')){
		MM_validateForm(\'TipoResol\',\'Tipo de Resoluci�n\',\'Sel\',\'sumilla\',\'Sumilla\',\'R\',\'dia_firma\',\'D�a de la Firma de la Resoluci�n\',\'Sel\',\'mes_firma\',\'Mes de la Firma de la Resoluci�n\',\'Sel\',\'anyo_firma\',\'A�o de la Firma de la Resoluci�n\',\'Sel\''; ?>
<?php if ($this->_tpl_vars['opcion2'] == 1): ?>,'campo4','Procedimiento','R','destinatario','Destinatario','R','codDepa','Departamento','Sel','codProv','Provincia','Sel','codDist','Distrito','Sel','DomProc','Domicilio Procesal','R','vigencia','La vigencia del acto notificado','Sel','R','folio','Folio','R'<?php endif; ?>);
		return document.MM_returnValue;<?php echo '
	}else
		return false;
}
 '; ?>

-->
</script>
<br>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" enctype="multipart/form-data" onsubmit="return ConfirmaRegistro(document.<?php echo $this->_tpl_vars['frmName']; ?>
)">
<input name="valorRecarga" type="hidden" id="valorRecarga">
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td colspan="2" class="item"><strong> </strong></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong> </td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA RESOLUCI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><strong>Tipo de Resoluci&oacute;n</strong> </td>
      <td align="left"><select name="TipoResol" class="ipseln" id="TipoResol">
        <?php echo $this->_tpl_vars['selTipoResol']; ?>

        </select>&nbsp;<input name="nroResol" type="text" class="iptxtn" id="nroResol" value="<?php echo $this->_tpl_vars['nroResol']; ?>
" size="35" maxlength="255" disabled></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="td-encuesta texto"><strong>Asunto </strong></td>
      <td align="left"><textarea name="sumilla" cols="70" rows="4" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['sumilla']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
    	<td class="texto td-encuesta"> <strong>Fec. Inicio</strong></td>
		  <td align="left"><select name="dia_ini" class="ipseln" id="dia_ini">
		  <?php echo $this->_tpl_vars['selDiaIni']; ?>

			</select> <select name="mes_ini" class="ipseln" id="mes_ini">
		  <?php echo $this->_tpl_vars['selMesIni']; ?>

			</select> <select name="anyo_ini" class="ipseln" id="anyo_ini">
		  <?php echo $this->_tpl_vars['selAnyoIni']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Inicio." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
	</tr>
		<tr> 
      	  
      <td class="texto td-encuesta"> <strong>Fec. Fin</strong></td>
		  <td align="left"><select name="dia_fin" class="ipseln" id="dia_fin">
		  <?php echo $this->_tpl_vars['selDiaFin']; ?>

			</select> <select name="mes_fin" class="ipseln" id="mes_fin">
		  <?php echo $this->_tpl_vars['selMesFin']; ?>

			</select> <select name="anyo_fin" class="ipseln" id="anyo_fin">
		  <?php echo $this->_tpl_vars['selAnyoFin']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Fin." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  		  
		</tr>
		
	<tr> 
      <td class="texto td-encuesta"> <strong>Fec. Firma</strong></td>
		  <td align="left"><select name="dia_firma" class="ipseln" id="dia_firma">
		  <?php echo $this->_tpl_vars['selDiaFirma']; ?>

			</select> <select name="mes_firma" class="ipseln" id="mes_firma">
		  <?php echo $this->_tpl_vars['selMesFirma']; ?>

			</select> <select name="anyo_firma" class="ipseln" id="anyo_firma">
		  <?php echo $this->_tpl_vars['selAnyoFirma']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Firma." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>		  		  
	</tr>
		
		<tr>     	  
      <td class="texto td-encuesta"> <strong>Fec. Publicaci&oacute;n</strong></td>
		  <td align="left"><select name="dia_pub" class="ipseln" id="dia_pub">
		  <?php echo $this->_tpl_vars['selDiaPub']; ?>

			</select> <select name="mes_pub" class="ipseln" id="mes_pub">
		  <?php echo $this->_tpl_vars['selMesPub']; ?>

			</select> <select name="anyo_pub" class="ipseln" id="anyo_pub">
		  <?php echo $this->_tpl_vars['selAnyoPub']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicaci�n." width="20" height="20" align="top"> 
		  </td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>		  
		</tr>
	<?php if (( $this->_tpl_vars['codigoDependencia'] == 5 )): ?>
	<tr> 
      <td class="td-encuesta texto"><strong>Archivo</strong></td>
      <td align="left"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
      <td colspan="3" class="item"><div align="center"><strong></strong></div></td>
    </tr>
	<?php endif; ?>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item" align="left"><font color="#FF0000"><strong><span class="item"><font color="#FF0000"><strong><input name="opcion2" type="checkbox" id="checkbox" value="1" <?php if ($this->_tpl_vars['opcion2'] == 1): ?> checked <?php endif; ?> onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">&iquest;Va a generar Notificaci�n?</strong></font></span></strong></font></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php if ($this->_tpl_vars['opcion2'] == 1): ?>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr> 
      <td colspan="2" class="td-encuesta texto"><strong> DATOS DE LA NOTIFICACI&Oacute;N </strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Procedimiento</strong></span> </td>
      <td class="item" align="left">
	  	<textarea name="campo4" cols="70" rows="12" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['campo4']; ?>
</textarea>
	  	<!--<input name="campo4" type="text" class="iptxtn" id="campo4" value="<?php echo $this->_tpl_vars['campo4']; ?>
" size="35" maxlength="255">-->
	   &nbsp;<img src="/img/800x600/ico-info3.gif" alt="Indica el procedimiento administrativo sobre el cual ha reca�do la resoluci�n o acto que se notifica." width="20" height="20" align="top">
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item" align="left"><strong><input name="opcionTN" type="checkbox" id="checkbox" value="1" <?php if ($this->_tpl_vars['opcionTN'] == 1): ?> checked <?php endif; ?> onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">&iquest;Nuevo m�todo?</strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php if ($this->_tpl_vars['opcionTN'] == 1): ?>
    <tr> 
      <td class="texto td-encuesta">&nbsp; </td>
      <td class="item" align="left"><input name="RZoRUC" type="text" class="iptxtn" value="<?php echo $this->_tpl_vars['RZoRUC']; ?>
" size="30" maxlength="50">
	  	&nbsp;&nbsp;<input type="button" name="BuscaRZ" value="BuscarRZ" class="submitV2" onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.valorRecarga.value=1;submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">
	  </td>
      <td><strong></strong></td>
    </tr>
	<?php endif; ?>	
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Destinatario</strong></span> </td>
      <td class="item" align="left"><textarea name="destinatario" cols="70" rows="3" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['destinatario']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php if ($this->_tpl_vars['idPersona'] > 0): ?>
    <tr> 
      <td class="texto td-encuesta"><strong>Otras Direcciones</strong> </td>
      <td class="item" align="left"><select name="otroDomi" class="ipseln" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">
		<?php echo $this->_tpl_vars['selDomAlt']; ?>

		<!--<option value="99999" <?php if ($this->_tpl_vars['otroDomi'] == '99999'): ?> selected<?php endif; ?>>Agregar nueva direcci�n</option>-->
              </select></td>
    </tr>
	<?php endif; ?>	
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Ubigeo</strong></span> </td>
      <td class="item" align="left">
	  		<select name="codDepa" class="ipseln" id="select3" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">
					<?php echo $this->_tpl_vars['selDepa']; ?>

            </select>
			<select name="codProv" class="ipseln" id="select2" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_MODIFICA_RESOL_ESP']; ?>
')">
					<?php echo $this->_tpl_vars['selProv']; ?>

            </select>
			<select name="codDist" class="ipseln" id="select" >
					<?php echo $this->_tpl_vars['selDist']; ?>

              </select>	  
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Domicilio Procesal</strong></span> </td>
      <td class="item" align="left"><textarea name="DomProc" cols="70" rows="3" class="iptxtn" id="DomProc" ><?php echo $this->_tpl_vars['DomProc']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Vigencia del Acto Notificado</strong></span> </td>
      <td class="item" align="left"><select name="vigencia" class="ipseln" id="campo1" >
          <option value="none"<?php if ($this->_tpl_vars['vigencia'] == 'none'): ?> selected<?php endif; ?>>Seleccione una 
          opci�n</option>
          <option value="1"<?php if ($this->_tpl_vars['vigencia'] == 1): ?> selected<?php endif; ?>>Desde la fecha de su 
          emisi�n</option>
          <option value="2"<?php if ($this->_tpl_vars['vigencia'] == 2): ?> selected<?php endif; ?>>Desde antes de su emisi�n 
          (eficacia anticipada)</option>
          <option value="3"<?php if ($this->_tpl_vars['vigencia'] == 3): ?> selected<?php endif; ?>>Desde el d�a de su notificaci�n</option>
          <option value="4"<?php if ($this->_tpl_vars['vigencia'] == 4): ?> selected<?php endif; ?>>Desde la fecha indicada 
          en la Resoluci�n</option>
        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>Agota la v&iacute;a administrativa </strong></span> </td>
      <td class="item" align="left"><input name="radio" type="radio" value="1" checked> 
        <strong>Si</strong> <input type="radio" name="radio" value="0"> 
        <strong>No</strong></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td rowspan="3" class="texto td-encuesta"><span class="item"><strong>Recurso Administrativo</strong></span> </td>
      <td class="item" align="left"><input type="checkbox" name="checkbox" value="1">
        Reconsideraci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
      <td colspan="3" rowspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
    <tr>
      <td class="item" align="left"><input type="checkbox" name="checkbox2" value="1">
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr>
      <td class="item" align="left"><input type="checkbox" name="checkbox3" value="1">
        Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;.</td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"><span class="item"><strong>N&uacute;mero de Folios</strong></span> </td>
      <td class="item" align="left"><input name="folio" type="text" class="iptxtn" onKeyPress="solo_num();" value="<?php if ($this->_tpl_vars['folio'] == ''): ?>1<?php else: ?><?php echo $this->_tpl_vars['folio']; ?>
<?php endif; ?>" size="4" maxlength="2"></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
		
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Modificar"> 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_RESOLUCION']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_RESOLUCION']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_RESOLUCION']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['MODIFICA_RESOL_ESP']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
"> 
		<input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
        <?php if (( $this->_tpl_vars['idResolucion'] > 0 )): ?> 
        <input name="idResolucion" type="hidden" id="idResolucion" value="<?php echo $this->_tpl_vars['idResolucion']; ?>
">
        <?php endif; ?> 
		<input name="idPersona" type="hidden" id="idPersona" value="<?php echo $this->_tpl_vars['idPersona']; ?>
">
		</td>
    </tr>
  </table>
</form>