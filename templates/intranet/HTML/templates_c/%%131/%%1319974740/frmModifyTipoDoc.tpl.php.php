<?php /* Smarty version 2.5.0, created on 2013-02-28 10:25:35
         compiled from oad/tramite/frmModifyTipoDoc.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<script language="JavaScript">
<!--
<?php echo '
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
function ConfirmaRegistro(pForm){
	if(confirm(\'�Desea Guardar este Registro?\')){
		MM_validateForm(\'motivo\',\'Motivo\',\'R\''; ?>
<?php if ($this->_tpl_vars['idTipoDoc'] == 1): ?>,'tipProced','Clase de Procedimiento','Sel','procedimiento','Procedimiento','Sel'<?php else: ?>,'asunto','Asunto','R'<?php endif; ?>);
		return document.MM_returnValue;<?php echo '
	}else
		return false;
}
'; ?>

-->
</script>
<br>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onsubmit="return ConfirmaRegistro(document.<?php echo $this->_tpl_vars['frmName']; ?>
)">
  <table width="700" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>N&uacute;mero de TD </strong></td>
      <td align="left"><?php echo $this->_tpl_vars['nroOTD']; ?>
</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Indicativo</strong></td>
      <td align="left"> <input name="indicativo" type="text" class="iptxtn" id="indicativo" value="<?php echo $this->_tpl_vars['indicativo']; ?>
" size="35" maxlength="80" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>	
	<?php if ($this->_tpl_vars['tipoPersona'] == 2): ?>
	<tr> 
      <td class="item"><strong>Raz&oacute;n Social</strong> </td>
      <td align="left"><textarea name="razonsocial" cols="70" rows="3" class="iptxtn" id="textarea" readonly="readonly"><?php echo $this->_tpl_vars['razonsocial']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<?php else: ?>
    <tr> 
      <td class="item"> <strong>Nombres</strong></td>
      <td align="left"> <input name="nombres" type="text" class="iptxtn" id="nombres" value="<?php echo $this->_tpl_vars['nombres']; ?>
" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Apellidos</strong></td>
      <td align="left"> <input name="apellidos" type="text" class="iptxtn" id="apellidos" value="<?php echo $this->_tpl_vars['apellidos']; ?>
" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<?php endif; ?>
    <tr> 
      <td class="item"> <strong>RUC</strong></td>
      <td class="item" align="left"> <input name="ruc" type="text" class="iptxtn" id="ruc" value="<?php echo $this->_tpl_vars['ruc']; ?>
" size="35" maxlength="255" readonly=""></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Direcci&oacute;n</strong> </td>
      <td align="left"><textarea name="direccion" cols="70" rows="3" class="iptxtn" id="direccion" readonly="readonly"><?php echo $this->_tpl_vars['direccion']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Dependencia</strong> </td>
      <td align="left"><?php echo $this->_tpl_vars['dependencia']; ?>
</td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000"></font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong></strong> </td>
      <td align="left">Status Inicial: <?php if ($this->_tpl_vars['idTipoDoc'] == 1): ?> EXTERNO <?php else: ?> EXPEDIENTE<?php endif; ?>
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>		
	<tr> 
      <td class="item"><strong>SE CAMBIA A</strong> </td>
      <td align="left">	  <?php if ($this->_tpl_vars['idTipoDoc'] == 2): ?>
	  		  <label> 
              <input name="GrupoOpciones1" type="radio" value="1" <?php if (( $this->_tpl_vars['idTipoDoc'] == 2 )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_CAMBIA_TIPO_DOCUMENTO_X']; ?>
')">
              <strong>Documento Externo</strong></label>
			  <?php endif; ?>
			  <?php if ($this->_tpl_vars['idTipoDoc'] == 1): ?>
			  <label> 
              <input name="GrupoOpciones1" type="radio" value="2" <?php if (( $this->_tpl_vars['idTipoDoc'] == 1 )): ?> checked <?php endif; ?> onClick="document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_CAMBIA_TIPO_DOCUMENTO_X']; ?>
')">
              <strong>Expediente</strong></label>
			  <?php endif; ?>
	  </td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php if ($this->_tpl_vars['idTipoDoc'] == 1): ?>
	<tr> 
      <td class="item"><strong>Clase de Procedimiento</strong> </td>
      <td align="left"><select name="tipProced" class="ipseln" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_CAMBIA_TIPO_DOCUMENTO_X']; ?>
')"><?php echo $this->_tpl_vars['selTipoProcedimiento']; ?>
</select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Procedimiento</strong></td>
      <td align="left"><select name="procedimiento" class="ipseln" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_CAMBIA_TIPO_DOCUMENTO_X']; ?>
')">
																<?php echo $this->_tpl_vars['selProcedimiento']; ?>

																  </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Dependencia</strong></td>
      <td align="left"><select name="dependencia2" class="ipseln" id="dependencia2">
					  <?php echo $this->_tpl_vars['selDependencia']; ?>

						 </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
	<?php if (( $this->_tpl_vars['idTipoDoc'] == 2 )): ?>
	<tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="70" rows="3" class="iptxtn" id="asunto" ><?php echo $this->_tpl_vars['asunto']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
	<tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td align="left"><textarea name="observaciones" cols="70" rows="4" class="iptxtn" id="observaciones" ><?php echo $this->_tpl_vars['observaciones']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<tr> 
      <td class="item"><strong>Motivo del cambio</strong> </td>
      <td align="left"><textarea name="motivo" cols="70" rows="4" class="iptxtn" id="motivo" ><?php echo $this->_tpl_vars['motivo']; ?>
</textarea></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>	
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submit" value="Cambiar Tipo Documento" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submit" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCUMENTO']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCUMENTO']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['CAMBIA_TIPO_DOCUMENTO_X']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
		<input name="idTipoDoc" type="hidden" id="idTipoDoc" value="<?php echo $this->_tpl_vars['idTipoDoc']; ?>
">
		<input name="tipoPersona" type="hidden" id="tipoPersona" value="<?php echo $this->_tpl_vars['tipoPersona']; ?>
">
		<input name="dependencia" type="hidden" id="dependencia" value="<?php echo $this->_tpl_vars['dependencia']; ?>
">
		<?php if ($this->_tpl_vars['id']): ?><input name="id" type="hidden" id="id" value="<?php echo $this->_tpl_vars['id']; ?>
"><?php endif; ?>
		 </td>
    </tr>
  </table>
</form>