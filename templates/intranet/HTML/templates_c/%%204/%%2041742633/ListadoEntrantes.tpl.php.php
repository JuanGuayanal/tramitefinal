<?php /* Smarty version 2.5.0, created on 2013-01-23 18:05:57
         compiled from oad/tramite/ListadoEntrantes.tpl.php */ ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Oficina General de Tecnología de Información y Estadística</title>
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" HEADER RIGHT "Actualizado al <?php echo $this->_tpl_vars['fechaGen2']; ?>
<?php echo $this->_tpl_vars['hora']; ?>
s"--> 
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b>
          <font color="#000000"> <font size="6">LISTADO DE DOCUMENTOS DE ACUERDO A LA FECHA DE INGRESO A LA DEPENDENCIA DEL <?php if ($this->_tpl_vars['fecInicio2'] == $this->_tpl_vars['fecFin2']): ?><?php echo $this->_tpl_vars['fecInicio2']; ?>
<?php else: ?><?php echo $this->_tpl_vars['fecInicio2']; ?>
 AL <?php echo $this->_tpl_vars['fecFin2']; ?>
<?php endif; ?>
          <!--ENTRE LAS FECHAS <?php echo $this->_tpl_vars['fecInicio']; ?>
 y <?php echo $this->_tpl_vars['fecFin']; ?>
 -->
          </font> </font></b></font> 
      </div>
      <hr width="100%" size="1" noshade>  </td>
  </tr>
</table>
<?php if ($this->_tpl_vars['tipodeDocc']): ?><font size="5">TIPO DE DOCUMENTO: <?php echo $this->_tpl_vars['tipodeDocc']; ?>
</font><br><?php endif; ?>
<?php if ($this->_tpl_vars['status'] == 2): ?><font size="5">ACTIVOS EN LA DEPENDENCIA</font><br><?php endif; ?>
<?php if ($this->_tpl_vars['status'] == 3): ?><font size="5">FINALIZADOS EN LA DEPENDENCIA</font><br><?php endif; ?>
<?php if ($this->_tpl_vars['status'] == 4): ?><font size="5">UBICADOS EN OTRA DEPENDENCIA</font><br><?php endif; ?>

<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999"> 
    <td width="5%"> <div align="center"><font size="5"><strong>N&deg;</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td width="37%"> <div align="center"><font size="5"><strong>PROVIENE</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>N&deg; DOCUMENTO</strong></font></div></td>
    <td width="10%"> <div align="center"><font size="5"><strong>FECHA</strong></font></div></td>
    <td width="46%"> <div align="center"><font size="5"><strong>ASUNTO</strong></font></div></td>
    <td width="20%"> <div align="center"><font size="5"><strong>OBSERVACIONES</strong></font></div></td>
	<?php if ($this->_tpl_vars['status'] == 2): ?>
	<td width="5%"> <div align="center"><font size="5"><strong>DIAS UTILES</strong></font></div></td>
	<?php endif; ?>
	<!--<td width="20%"> <div align="center"><font size="5"><strong>DETALLES</strong></font></div></td>-->
	<!--<td width="10%"> <div align="center"><font size="5"><strong>FIRMA</strong></font></div></td>-->
  </tr>
  <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  <tr> 
    <td width="5%"><div align="center"><font size="5"><?php echo $this->_sections['i']['iteration']; ?>
</font></div></td>
    <td width="10%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroTram']; ?>
</font></td>
    <td width="37%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['remi']; ?>
<br>
      <?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['remi2']; ?>
</font></td>
    <td width="10%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind']; ?>
<br>
      <?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind'] != $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind2'] )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind2']; ?>
<?php endif; ?></font></td>
    <td width="10%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecha']; ?>
</font></td>
    <td width="46%"><div align="center"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['asunto']; ?>
</font></div></td>
    <td width="20%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['obs']; ?>
<?php if ($this->_tpl_vars['list'][$this->_sections['i']['index']]['per']): ?>/<?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['per']; ?>
<?php endif; ?></font></td>
	<?php if ($this->_tpl_vars['status'] == 2): ?>
	<td width="5%"> <div align="center"><font size="5"><strong><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['dias']; ?>
</strong></font></div></td>
	<?php endif; ?>
	<!--<td width="20%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['detalle']; ?>
</font></td>-->
	<!--<td width="10%"><font size="5">&nbsp;</font></td>-->
  </tr>
  <?php endfor; else: ?> 
  <tr> 
    <td colspan="<?php if ($this->_tpl_vars['status'] == 2): ?>8<?php else: ?>7<?php endif; ?>" width="100%"><div align="center"><font size="6"><strong>No 
        se han ingresado Documentos a la Oficina en la(s) fecha(s) dada(s)</strong></font></div></td>
  </tr>
  <?php endif; ?> 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  <?php echo $this->_tpl_vars['fechaGen2']; ?>
<?php echo $this->_tpl_vars['hora']; ?>
s</i></font></p>
</body>
</html>