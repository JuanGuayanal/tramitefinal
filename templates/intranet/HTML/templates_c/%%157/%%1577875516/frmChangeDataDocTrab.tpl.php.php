<?php /* Smarty version 2.5.0, created on 2015-01-19 16:55:52
         compiled from oad/tramite/nuevaVersionFormularios/formsTrabajadores/utilitarios/frmChangeDataDocTrab.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>


<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript">
<!--
<?php echo '
function CalendarHide(){
	MM_showHideLayers(\'popCalIni\',\'\',\'hide\')
	MM_showHideLayers(\'popCalFin\',\'\',\'hide\')
}

document.onclick = CalendarHide

function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
function ConfirmaRegistro(pForm){
	if(confirm(\'�Desea Guardar este Registro?\')){
		MM_validateForm(\'asunto\',\'Asunto\',\'R\',\'observaciones\',\'Observaciones\',\'R\');
		return document.MM_returnValue;
	}else
		return false;
}
 '; ?>

-->
</script>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onsubmit="return ConfirmaRegistro(document.<?php echo $this->_tpl_vars['frmName']; ?>
)">
<br>
  <table width="720" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="5" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?>
    <tr> 
      <td colspan="4" class="item-sep"><strong>Cambio en el asunto y / o observaciones de un documento creado por el trabajador.</strong></td>
    </tr>	 
    <tr> 
      <td class="item"><strong>Fecha</strong></td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"> <strong>Digite el Nro. de documento</strong></td>
      <td align="left"><select name="tipoDocc" class="ipseln" id="select">
        			<?php echo $this->_tpl_vars['selTipoDocc']; ?>

				</select>
				<input name="numero2" type="text" class="iptxtn" value="<?php echo $this->_tpl_vars['numero2']; ?>
" size="5" maxlength="100">
				 -
				<select name="anyo2" class="ipseln" id="select">
					<option value="none">Todos</option><?php echo $this->_tpl_vars['selAnio']; ?>

				</select> 
				-MIDIS/PNCM/
				<select name="siglasDepe2" class="ipseln" id="siglasDepe2">
					<?php echo $this->_tpl_vars['selSiglasDep2']; ?>

       			</select>-<?php echo $_SESSION['cod_usuario']; ?>
	  	  &nbsp;&nbsp;
	  <input type="button" name="Busca" value="Buscar" class="std_button" onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_CAMBIA_DATOS_DOC_TRAB']; ?>
')"></td>		
	  </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php if (( ! $this->_tpl_vars['exito'] || $this->_tpl_vars['exito'] == 0 )): ?>
    <tr> 
      <td height="25" class="item" >&nbsp;</td>
      <td align="left"><span class="item"><font color="#FF0000"><strong>&iexcl;No se puede interactuar con el documento, no tiene opci&oacute;n para modificar los datos del documento, quiz&aacute; est&eacute; anulado!</strong></font></span></td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['idDocumento'] > 0): ?>
    <tr> 
      <td class="item"><strong>Datos del documento</strong> </td>
      <td align="left"><?php echo $this->_tpl_vars['claseDocumento']; ?>
 <?php echo $this->_tpl_vars['indicativo']; ?>
 <?php echo $this->_tpl_vars['auditmod']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Asunto</strong> </td>
      <td align="left"><textarea name="asunto" cols="80" rows="4" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['asunto']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="item"><strong>Observaciones</strong> </td>
      <td align="left"><textarea name="observaciones" cols="80" rows="5" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['observaciones']; ?>
</textarea></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>	
	<?php endif; ?>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="5"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="5"> <input name="bSubmit" type="Submit" class="std_button" value="Guardar cambios" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_TRAB']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['CAMBIA_DATOS_DOC_TRAB']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idDocumento" type="hidden" id="idDocumento" value="<?php echo $this->_tpl_vars['idDocumento']; ?>
">
		<input name="Buscar" type="hidden" id="Buscar" value="1">
		 </td>
    </tr>
  </table>
</form>