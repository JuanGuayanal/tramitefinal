<?php /* Smarty version 2.5.0, created on 2015-01-04 22:49:03
         compiled from oad/tramite/nuevaVersionFormularios/formsTrabajadores/frmFinalizaDocTrab.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<br>
<div style="width:700px;">
<div class="std_form">
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" >
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td class="texto td-encuesta label">Fecha</td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
	 <tr class="std_oculto"> 
      <td colspan="5" class="item-sep" align="left"><strong>Los siguientes datos (*) s�lo se llenar�n para el caso de Expedientes.</strong></td>
    </tr>
	<tr class="std_oculto"> 
      <td class="item"><strong>&iquest;Finalizado definitivamente?</strong></td>
      <td class="item" align="left"><input name="finalizacionDefinitiva" type="radio" value="1">Si
	  		&nbsp;<input name="finalizacionDefinitiva" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item"><strong>Estado del Expediente</strong></td>
      <td class="item" align="left"><input name="estadoExpediente" type="radio" value="1" checked>Favorable/ Firme/ Consentido para el administrado
	  		&nbsp;<input name="estadoExpediente" type="radio" value="2">Desfavorable
			&nbsp;<input name="estadoExpediente" type="radio" value="3">No aplica
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item"><strong>&iquest;Sujeto a Proceso Judicial?</strong></td>
      <td class="item" align="left"><input name="esttadoProcesoJudicial" type="radio" value="1">Si
	  		&nbsp;<input name="esttadoProcesoJudicial" type="radio" value="2" checked>No
	  </td>
      <td colspan="3" class="textored"><div align="center"><strong>(*)</strong></div></td>
    </tr>
	
    <tr> 
      <td class="texto td-encuesta label" valign="middle">Observaciones</td>
      <td align="left"> <textarea name="observaciones" cols="50" rows="4" class="iptxtn" id="observaciones" ><?php if (! $this->_tpl_vars['observaciones']): ?>Sin Observaciones<?php else: ?><?php echo $this->_tpl_vars['observaciones']; ?>
<?php endif; ?></textarea> 
      </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr class="std_oculto"> 
      <td colspan="3" class="textored"> <div align="left"><strong>(*): Datos a registrarse s&oacute;lo para el caso de Expedientes.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Si finaliza definitivamente el expediente, no se podr&aacute; reactivar ni se podr&aacute; agregar adjuntos al expediente.
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las observaciones son obligatorias para cualquier tipo de documento.</strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="std_button" value="Archivar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_TRAB']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['FINALIZA_DOCTRAB']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idFinTrab" type="hidden" id="idFinTrab" value="<?php echo $this->_tpl_vars['idFinTrab']; ?>
">
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['ids']) ? count($this->_tpl_vars['ids']) : max(0, (int)$this->_tpl_vars['ids']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
		
        <!--<input name="ids[]" type="hidden" id="ids[]" value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">-->
		<input name="ids[]" type="hidden" id="ids[]" value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">
		
		<?php endfor; endif; ?>
		
		<?php if (isset($this->_sections['j'])) unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($this->_tpl_vars['ids2']) ? count($this->_tpl_vars['ids2']) : max(0, (int)$this->_tpl_vars['ids2']);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?> 
		<input name="ids2[]" type="hidden" id="ids2[]" value="<?php echo $this->_tpl_vars['ids2'][$this->_sections['j']['index']]; ?>
">
		<?php endfor; endif; ?>		
		
		<?php if ($this->_tpl_vars['tipDocumento']): ?><input name="tipDocumento" type="hidden" id="tipDocumento" value="<?php echo $this->_tpl_vars['tipDocumento']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['tipBusqueda']): ?><input name="tipBusqueda" type="hidden" id="tipBusqueda" value="<?php echo $this->_tpl_vars['tipBusqueda']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecIniTrab2']): ?><input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="<?php echo $this->_tpl_vars['fecIniTrab2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecFinTrab2']): ?><input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="<?php echo $this->_tpl_vars['fecFinTrab2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['asunto2']): ?><input name="asunto2" type="hidden" id="asunto2" value="<?php echo $this->_tpl_vars['asunto2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['indicativo']): ?><input name="indicativo" type="hidden" id="indicativo" value="<?php echo $this->_tpl_vars['indicativo']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['observaciones2']): ?><input name="observaciones2" type="hidden" id="observaciones2" value="<?php echo $this->_tpl_vars['observaciones2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['nroTD']): ?><input name="nroTD" type="hidden" id="nroTD" value="<?php echo $this->_tpl_vars['nroTD']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['page']): ?><input name="page" type="hidden" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['siglasDep'] && $this->_tpl_vars['siglasDep'] != 'none' )): ?><input name="siglasDep" type="hidden" id="siglasDep" value="<?php echo $this->_tpl_vars['siglasDep']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['tipodDoc'] && $this->_tpl_vars['tipodDoc'] != 'none' )): ?><input name="tipodDoc" type="hidden" id="tipodDoc" value="<?php echo $this->_tpl_vars['tipodDoc']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['anyo3'] && $this->_tpl_vars['anyo3'] != 'none' )): ?><input name="anyo3" type="hidden" id="anyo3" value="<?php echo $this->_tpl_vars['anyo3']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['FechaIni']): ?><input name="FechaIni" type="hidden" id="FechaIni" value="<?php echo $this->_tpl_vars['FechaIni']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['FechaFin']): ?><input name="FechaFin" type="hidden" id="FechaFin" value="<?php echo $this->_tpl_vars['FechaFin']; ?>
"><?php endif; ?>		
		<input name="checkTodos" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkTodos']; ?>
">
		<input name="checkAsunto" type="hidden" id="checkAsunto" value="<?php echo $this->_tpl_vars['checkAsunto']; ?>
">
		<input name="checkRazon" type="hidden" id="checkRazon" value="<?php echo $this->_tpl_vars['checkRazon']; ?>
">
		<input name="checkTrab" type="hidden" id="checkTrab" value="<?php echo $this->_tpl_vars['checkTrab']; ?>
">
		<input name="FechaIni" type="hidden" id="FechaIni" value="<?php echo $this->_tpl_vars['FechaIni']; ?>
">
		<input name="FechaFin" type="hidden" id="FechaFin" value="<?php echo $this->_tpl_vars['FechaFin']; ?>
">		
		 </td>
    </tr>
  </table>
</form>
</div>
</div>