<?php /* Smarty version 2.5.0, created on 2014-03-17 18:23:38
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/reportes/showListadoDocPDF.tpl.php */ ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<!-- HEADER LEFT "$LOGOIMAGE" -->
<!-- FOOTER LEFT "<?php echo $this->_tpl_vars['fechaGen']; ?>
" -->
<!--SI ES PENDIENTE-->
<?php if (( $this->_tpl_vars['status'] == 1 )): ?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS PENDIENTES<?php if (( $this->_tpl_vars['nombresTrabajador'] && $this->_tpl_vars['nombresTrabajador'] != ' ' )): ?><br><?php echo $this->_tpl_vars['nombresTrabajador']; ?>
<?php endif; ?></font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="5%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="9%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA DERIVACI&Oacute;N</font></strong></div></td>
        <td width="8%" class="item"><div align="center"><strong><font size="5" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
	<tr> 
		<td width="5%"  class="item"><font size="4" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></td>
		<td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['docRecibido']; ?>
</font></td>
        <td width="9%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['numDocRec']; ?>
</font></font></td>
        <td width="15%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['tup']; ?>
</font></td>
        <td width="9%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroTD']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['razSoc']; ?>
</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngP']; ?>
</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngD']; ?>
</font></td>
        <td width="8%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fechaAccep']; ?>
</font></td>
        <td width="9%" class="item"><font size="4" face="Arial"><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] && $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] != "" )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe']; ?>
/<br /><?php endif; ?><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != "" && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != 'NULL' && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != ' ' )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab']; ?>
<?php else: ?>DIRECTOR<?php endif; ?></font></td>
        <td width="9%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['avance']; ?>
</font></td>
 	</tr>
	<?php endfor; else: ?> 
	<tr> 
		<td colspan="11" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	<?php endif; ?>
</table>
<!--SI ES PENDIENTE CONGRESO-->
<?php elseif (( $this->_tpl_vars['status'] == 11 )): ?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS PENDIENTES DEL CONGRESO DE LA REPUBLICA <?php if (( $this->_tpl_vars['nombresTrabajador'] && $this->_tpl_vars['nombresTrabajador'] != ' ' )): ?><br><?php echo $this->_tpl_vars['nombresTrabajador']; ?>
<?php endif; ?></font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="5%" class="item"><div align="center"><strong><font size="4" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="4" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="9%" class="item"><div align="center"><strong><font size="4" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="4" face="Arial">ASUNTO</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="4" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="4" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="8%"  class="item"><div align="center"><strong><font size="4" face="Arial">FECHA DERIVACI&Oacute;N</font></strong></div></td>
        <td width="8%" class="item"><div align="center"><strong><font size="4" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="9%"  class="item"><div align="center"><strong><font size="4" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
	<tr> 
		<td width="5%"  class="item"><font size="3" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></td>
		<td width="10%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['docRecibido']; ?>
</font></td>
        <td width="9%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['numDocRec']; ?>
</font></font></td>
        <td width="15%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['tup']; ?>
</font></td>
        <td width="9%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroTD']; ?>
<?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind'] && $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind'] != "" )): ?><br>
                    <?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind']; ?>
<?php endif; ?></font></td>
        <td width="10%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['razSoc']; ?>
</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngP']; ?>
</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngD']; ?>
</font></td>
        <td width="8%"  class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fechaAccep']; ?>
</font></td>
        <td width="9%" class="item"><font size="3" face="Arial"><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] && $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] != "" )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe']; ?>
/<br /><?php endif; ?><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != "" && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != 'NULL' && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != ' ' )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab']; ?>
<?php else: ?>DIRECTOR<?php endif; ?></font></td>
        <td width="9%" class="item"><font size="3" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['avance']; ?>
</font></td>
 	</tr>
	<?php endfor; else: ?> 
	<tr> 
		<td colspan="11" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	<?php endif; ?>
</table>
<!--SI ES FINALIZADO-->
<?php elseif (( $this->_tpl_vars['status'] == 4 )): ?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
	<tr align="center"> 
		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">REPORTE DE DOCUMENTOS FINALIZADOS <?php if (( $this->_tpl_vars['nombresTrabajador'] && $this->_tpl_vars['nombresTrabajador'] != ' ' )): ?><br><?php echo $this->_tpl_vars['nombresTrabajador']; ?>
<?php endif; ?></font></strong></td>
	</tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	<tr style="height: 25px" class="item-sitra" align="center" bgcolor="#999999"> 
		<td width="3%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
		<td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">DOCUMENTO</font></strong></div></td>
        <td width="10%" class="item"><div align="center"><strong><font size="5" face="Arial">N&deg; DOC.</font></strong></div></td>
        <td width="15%" class="item"><div align="center"><strong><font size="5" face="Arial">ASUNTO</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
        <td width="7%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA CREACI&Oacute;N REGISTRO</font></strong></div></td>
        <td width="7%"  class="item"><div align="center"><strong><font size="5" face="Arial">FECHA RECEPCI&Oacute;N</font></strong></div></td>
        <td width="7%" class="item"><div align="center"><strong><font size="5" face="Arial">FECHA FINALIZACI&Oacute;N</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">MOTIVO DE FINALIZACI&Oacute;N</font></strong></div></td>
        <td width="10%"  class="item"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
 	</tr>
	<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
	<tr> 
	  <td width="3%"  class="item"><font size="4" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></td>
		<td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['docRecibido']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['numDocRec']; ?>
</font></font></td>
        <td width="15%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['tup']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroTD']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['razSoc']; ?>
</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngP']; ?>
</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngD']; ?>
</font></td>
      <td width="7%"  class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecT']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != "" && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != 'NULL' && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != ' ' )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab']; ?>
<?php else: ?>DIRECTOR<?php endif; ?></font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['motivo_finalizacion']; ?>
</font></td>
        <td width="10%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['avance']; ?>
</font></td>
 	</tr>
	<?php endfor; else: ?> 
	<tr> 
		<td colspan="12" class="textored"><div align="center"><strong>NO SE HAN ENCONTRADO RESULTADOS</strong></div></td>
	</tr>
	<?php endif; ?>
</table>
<?php else: ?>
<table width="100%" border="0" align="center" cellpadding="0" bgcolor="#F7F7F7">
  <tr> 
    <td height="8" bgcolor="#FFFFFF"> 
	   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="6">
        	<tr align="center"> 
         		<td height="50" colspan="2" class="textoblack"><strong><font size="7" face="Arial">LISTADO DE DOCUMENTOS<?php if (( $this->_tpl_vars['nombresTrabajador'] && $this->_tpl_vars['nombresTrabajador'] != ' ' )): ?><br><?php echo $this->_tpl_vars['nombresTrabajador']; ?>
<?php endif; ?></font></strong></td>
        	</tr>
			<tr>
      			<td>
				   <table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  					 <tr bgcolor="#999999"> 
    					<td colspan="3" class="textoblack" bgcolor="#999999"> <div align="center"></div>
      														<div align="center"></div>
      														<div align="center"><strong><font size="6" face="Arial">DOCUMENTO</font></strong></div>
					   </td>
   					   <td width="30%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">RAZ&Oacute;N SOCIAL</font></strong></div></td>
   					   <td width="5%"rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA INGRESO MIDIS</font></strong></div></td>
   					   <td width="5%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial"><?php if ($this->_tpl_vars['sit'] == 4): ?>FECHA DE DERIVACI�N<?php else: ?>FECHA INGRESO DEPENDENCIA<?php endif; ?></font></strong></div></td>
					   <?php if (( $this->_tpl_vars['status'] == 1 || $this->_tpl_vars['status'] == 9 || $this->_tpl_vars['status'] == 5 )): ?><td width="5" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">ACEPTACI�N</font></strong></div></td><?php endif; ?>
    					<?php if ($this->_tpl_vars['status'] == 1): ?><td width="5" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">D�AS �TILES</font></strong></div></td><?php endif; ?>
						<?php if ($this->_tpl_vars['status'] == 4): ?><td width="8%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">FECHA DE T&Eacute;RMINO</font></strong></div></td><?php endif; ?>
					   <?php if (! $this->_tpl_vars['nombresTrabajador']): ?><td width="40%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">TRABAJADOR</font></strong></div></td><?php endif; ?>
					   <td width="20%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">COMENTARIO</font></strong></div></td>
  						<td width="20%" rowspan="2" class="item" bgcolor="#999999"><div align="center"><strong><font size="5" face="Arial">AVANCE</font></strong></div></td>
                     </tr>
  					 <tr bgcolor="#999999"> 
   					   <td class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">N&deg;</font></strong></div></td>
   					   <td width="35%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial"><?php if ($this->_tpl_vars['tipDoc'] == 2): ?>TUPA<?php else: ?>ASUNTO<?php endif; ?></font></strong></div></td>
   					   <td width="8%" class="item" bgcolor="#999999"> <div align="center"><strong><font size="5" face="Arial">REFERENCIA</font></strong></div></td>
  					 </tr>
  					<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  					 <tr> 
    					<td width="5%" class="item"><div align="center"><font size="4" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></div></td>
    					<td width="35%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['tup']; ?>
</font></td>
    					<td width="8%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroTD']; ?>
<?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind'] && $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind'] != "" )): ?> - <?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ind']; ?>
<?php endif; ?></font></td>
    					<td width="30%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['razSoc']; ?>
</font></td>
    					<td width="5%" class="item"><font size="4" face="Arial"><?php if ($this->_tpl_vars['tipDoc'] == 4): ?>NO TIENE<?php else: ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngP']; ?>
<?php endif; ?></font></td>
    					<td width="5%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecIngD']; ?>
</font></td>
						<?php if (( $this->_tpl_vars['status'] == 1 || $this->_tpl_vars['status'] == 9 || $this->_tpl_vars['status'] == 5 )): ?><td width="5" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fechaAccep']; ?>
</font></td><?php endif; ?>
    					<?php if ($this->_tpl_vars['status'] == 1): ?><td width="5" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecT']; ?>
</font></td><?php endif; ?>
						<?php if ($this->_tpl_vars['status'] == 4): ?><td width="8%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecT']; ?>
</font></td><?php endif; ?>
						<?php if (! $this->_tpl_vars['nombresTrabajador']): ?><td width="40%" class="item"><font size="4" face="Arial"><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] && $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe'] != "" )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['ubicacionDepe']; ?>
/<?php endif; ?><?php if (( $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != "" && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != 'NULL' && $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab'] != ' ' )): ?><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['trab']; ?>
<?php else: ?>DIRECTOR<?php endif; ?></font></td><?php endif; ?>
						<td width="20%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['comentario']; ?>
</font></td>
  						  <td width="20%" class="item"><font size="4" face="Arial"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['avance']; ?>
</font></td>
                	 </tr>
  					<?php endfor; else: ?> 
  					 <tr> 
    					<td colspan="<?php if ($this->_tpl_vars['sit'] == 2): ?>8<?php else: ?>9<?php endif; ?>" class="textored"><div align="center"><strong><font size="4" face="Arial">NO SE HAN ENCONTRADO RESULTADOS</font></strong></div></td>
  					 </tr>
  					<?php endif; ?> 
		  	      </table>
			    </td>
           </tr>
		   </table>
		 </td>
	</tr>
</table>
<?php endif; ?>
</body>
</html>