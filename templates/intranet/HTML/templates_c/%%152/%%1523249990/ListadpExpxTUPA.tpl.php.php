<?php /* Smarty version 2.5.0, created on 2013-02-14 19:14:35
         compiled from oad/tramite/ListadpExpxTUPA.tpl.php */ ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="7" face="Arial"><b><br>
        <font color="#000000" size="6"> N&Uacute;MERO DE EXPEDIENTES POR PROCEDIMIENTO TUPA INGRESADOS A LA DEPENDENCIA</font></b></font><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6"> <?php if ($this->_tpl_vars['fecInicio2'] != $this->_tpl_vars['fecFin2']): ?> ENTRE EL <?php echo $this->_tpl_vars['fecInicio2']; ?>
 Y EL <?php echo $this->_tpl_vars['fecFin2']; ?>
<?php else: ?>DEL <?php echo $this->_tpl_vars['fecInicio2']; ?>
<?php endif; ?></font></font></b></font><br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<br>
<font size="7">PROCEDIMIENTO: <?php echo $this->_tpl_vars['descripcion']; ?>
</font>
<br>
<font size="7">DEPENDENCIA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $this->_tpl_vars['siglasDep']; ?>
</font>
<br>
<br>
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" bgcolor="#000000">
  <tr bgcolor="#999999"> 
    <td width="5%" align="center"><font size="5"><strong>N&deg;</strong></font><font color="#000000" size="5" face="Arial">&nbsp;</font></td>
    <td width="45%" align="center"><font size="5"><strong>DEPENDENCIA</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>RECIBIDOS</strong></font></td>
	<td width="10%" align="center"><font size="5"><strong>DERIVADOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>FINALIZADOS</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>EN PROCESO</strong></font></td>
    <td width="10%" align="center"><font size="5"><strong>CORRESPONDENCIA</strong></font></td>
  </tr>
  <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  <tr> 
    <td align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial"><?php echo $this->_sections['i']['iteration']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['dep']; ?>
</font></td>
	<td bgcolor="#FFFFFF"><font color="#000000" size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['cant']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['der']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroFin']; ?>
</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['dif']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['nroCor']; ?>
</font></td>
  </tr>
  <?php endfor; else: ?> 
  <tr> 
    <td colspan="11" align="center" bgcolor="#FFFFFF"><font size="5"><strong>NO 
      EXISTEN EXPEDIENTES INGRESADOS EN LA(S) FECHA(S) DADA(S) </strong></font></td>
  </tr>
  <?php endif; ?>
  <!--
  <tr> 
    <td colspan="2" align="center" bgcolor="#FFFFFF"><font color="#000000" size="5" face="Arial">TOTALES</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><?php echo $this->_tpl_vars['derivados']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5"><?php echo $this->_tpl_vars['sumaDer']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><div align="center"><font color="#000000" size="5"><?php echo $this->_tpl_vars['finalizados']; ?>
</font></div></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><?php echo $this->_tpl_vars['operacion']; ?>
</font></td>
    <td bgcolor="#FFFFFF"><font color="#000000" size="5" ><?php echo $this->_tpl_vars['correspondencia']; ?>
</font></td>
  </tr>
  -->
</table>
<p>&nbsp;</p>
<p><br>
</p>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  <?php echo $this->_tpl_vars['fechaGen']; ?>
<?php echo $this->_tpl_vars['hora']; ?>
s</i></font></p>
</body>
</html>