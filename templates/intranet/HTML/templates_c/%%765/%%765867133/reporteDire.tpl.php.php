<?php /* Smarty version 2.5.0, created on 2013-02-08 20:51:27
         compiled from oad/tramite/reporteDire.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'default', 'oad/tramite/reporteDire.tpl.php', 31, false),)); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><br>
        <font color="#000000" size="6"><u><strong>DETALLES DEL DOCUMENTO EXTERNO O EXPEDIENTE</strong></u><strong><br>
        <!--<?php echo $this->_tpl_vars['numTramDoc']; ?>
--></strong></font></p>      
      <!--<hr width="100%" size="1" noshade>--></td>
  </tr>
</table>

  
<table width="95%" border="1" align="center">
  <tr> 
    <td width="21%"><font color="#000000" size="5"><strong>N&deg; DOCUMENTO</strong></font></td>
    <td width="35%" ><font color="#000000" size="7"><strong><?php echo $this->_tpl_vars['numTramDoc']; ?>
</strong></font></td>
    <td width="9%" ><font color="#000000" size="5"><strong>TIPO DE PERSONA</strong></font></td>
    <td width="9%" ><font color="#000000" size="5"><?php echo $this->_tpl_vars['TipoPer']; ?>
</font></td>
    <td width="21%"><font color="#000000" size="5"><strong>FECHA DE INGRESO</strong></font></td>
    <td width="23%"><font color="#000000" size="5"><?php echo $this->_tpl_vars['fecRec']; ?>
</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>ASUNTO</strong></font></td>
    <td colspan="5"><font color="#000000" size="5"><?php if ($this->_tpl_vars['idTipoDoc'] == 2): ?><?php echo $this->_tpl_vars['proced']; ?>
<?php else: ?><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['asunto'], 'No 
      especificado'); ?>
<?php endif; ?></font><font color="#0066CC" size="5">&nbsp;</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>OBSERVACIONES</strong></font></td>
    <td colspan="5"><font color="#000000" size="5"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['observaciones'], 'No 
      especificado'); ?>
</font><font color="#0066CC" size="5">&nbsp;</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#333333" size="6"><strong>DATOS 
        DE LA EMPRESA</strong></font></div></td>
  </tr>
  <tr> 
    <td width="21%"><font color="#000000" size="5"><strong>RAZ&Oacute;N SOCIAL</strong></font></td>
    <td width="35%" colspan="3" ><font color="#000000" size="5"><?php echo $this->_tpl_vars['persona']; ?>
</font></td>
    <td width="21%"><font color="#000000" size="5"><strong>RUC</strong></font></td>
    <td width="23%"><font color="#000000" size="5"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['ruc'], 'No especificado'); ?>
</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>DIRECCI&Oacute;N LEGAL</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['direccion'], 'No especificado'); ?>
</font><font color="#0066CC" size="5">&nbsp;</font><font color="#000000" size="5">&nbsp;</font><font color="#3270aa" size="4">&nbsp;</font><font color="#0066CC" size="5">&nbsp;</font></td>
    <td><font color="#000000" size="5"><strong>TEL&Eacute;FONO</strong></font></td>
    <td><font color="#000000" size="5"><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['telefono'], 'No especificado'); ?>
</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#000000" size="6"><strong>DATOS 
        GENERALES DEL DOCUMENTO</strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>N&deg; DE DOCUMENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_tpl_vars['numTramDoc']; ?>
</font></td>
    <td><font color="#000000" size="5"><strong>INDICATIVO-OFICIO</strong></font></td>
    <td><font color="#000000" size="5"><?php echo $this->_tpl_vars['indicativo']; ?>
</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>TIPO DE DOCUMENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_tpl_vars['descripcion']; ?>
</font></td>
    <td><font color="#000000" size="5"><strong>N&deg; FOLIOS</strong></font></td>
    <td><font color="#000000" size="5"><?php echo $this->_tpl_vars['folios']; ?>
</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>PROCEDENCIA</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_tpl_vars['proce']; ?>
</font></td>
    <td><font color="#000000" size="5"><strong>FECHA DE INGRESO</strong></font></td>
    <td><font color="#000000" size="5"><?php echo $this->_tpl_vars['fecRec']; ?>
</font></td>
  </tr>
  <tr bgcolor="#999999"> 
    <td colspan="6"> <div align="center"><font color="#000000" size="6"><strong>SITUACI&Oacute;N 
        DEL DOCUMENTO </strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>DIRECCI&Oacute;N RECEPTORA</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_tpl_vars['dep']; ?>
</font></td>
    <td><font color="#000000" size="5"><strong>N&deg; D&Iacute;AS EN TR&Aacute;MITE</strong></font></td>
    <td><font color="#000000" size="5"><?php echo $this->_tpl_vars['nroDias']; ?>
</font></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>TRATAMIENTO</strong></font></td>
    <td colspan="3"><font color="#000000" size="5"><?php echo $this->_tpl_vars['sitAct']; ?>
</font></td>
    <td><font color="#000000" size="5"><strong><?php if ($this->_tpl_vars['nroDiasTupa']): ?>N&deg; D&Iacute;AS 
      CATALOGADO EN TUPA <?php endif; ?></strong></font></td>
    <td><font color="#000000" size="5"><?php if ($this->_tpl_vars['nroDiasTupa']): ?><?php echo $this->_tpl_vars['nroDiasTupa']; ?>
<?php endif; ?></font></td>
  </tr>
  <?php if ($this->_tpl_vars['Retrazo'] && $this->_tpl_vars['idTipoDoc'] == 2): ?>
  <tr> 
    <td colspan="4"><font color="#000000" size="5">&nbsp;</font><font color="#3270aa" size="5">&nbsp;</font></td>
    <td><font color="#000000" size="5"><strong>RETRAZO </strong></font></td>
    <td><font color="#000000" size="5"><?php if ($this->_tpl_vars['Retrazo']): ?><?php echo $this->_tpl_vars['Retrazo']; ?>
<?php endif; ?></font></td>
  </tr>
  <?php endif; ?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="95%" border="1" align="center">
  <tr bgcolor="#999999"> 
    <td colspan="7"> <div align="center"><font color="#000000" size="6"><strong>FLUJO 
        DOCUMENTARIO</strong></font></div></td>
  </tr>
  <tr> 
    <td><font color="#000000" size="5"><strong>CLASE DE DOCUMENTO</strong></font></td>
    <td><font color="#000000" size="5"><strong>INDICATIVO</strong></font></td>
    <td><font color="#000000" size="5"><strong>ASUNTO</strong></font></td>
    <td><font color="#000000" size="5"><strong>OBSERVACIONES</strong></font></td>
    <td><font color="#000000" size="5"><strong>DEPENDENCIA ORIGEN</strong></font></td>
    <td><font color="#000000" size="5"><strong>DEPENDENCIA DESTINO</strong></font></td>
    <td><font color="#000000" size="5"><strong>FECHA DE DERIVACI&Oacute;N</strong></font></td>
  </tr>
  <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['dir1']) ? count($this->_tpl_vars['dir1']) : max(0, (int)$this->_tpl_vars['dir1']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  <tr> 
    <td><font color="#0066CC" size="5">&nbsp;</font><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['cla']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['ind']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['asu']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['obs']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['depo']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['depd']; ?>
</font></td>
    <td><font size="5"><?php echo $this->_tpl_vars['dir1'][$this->_sections['i']['index']]['fec']; ?>
</font></td>
  </tr>
  <?php endfor; else: ?> 
  <tr> 
    <td colspan="7" align="center" bgcolor="#FFFFFF"><strong><font size="5">El 
      Documento no se ha derivado a otra dependencia</font></strong></td>
  </tr>
  <?php endif; ?> 
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  <?php echo $this->_tpl_vars['fechaGen']; ?>
<?php echo $this->_tpl_vars['hora']; ?>
s</i></font></p>
</body>
</html>