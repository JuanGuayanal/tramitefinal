<?php /* Smarty version 2.5.0, created on 2013-03-12 17:16:21
         compiled from oad/tramite/directorio/printDetallesPersona.tpl.php */ ?>
<html>
<head>
<title>Documentos : <?php echo $this->_tpl_vars['doc']['desc']; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- <?php echo ' -->
<style type="text/css">
<!--
hr {
	background-color: #D6D6D6;
	border: 1px #D6D6D6 solid
}
.item {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #265682
}
.sub-item {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0099FF
}
.texto {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #3270aa;
	text-decoration: none
}
.textored {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #FF3333;
	text-decoration: none
}
.textogray {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #A0A0A0;
	text-decoration: none
}
.textoblack {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	text-decoration: none
}
.bordesup2 {
border: solid; border-width: 1px 0px; border-color: #cedfea black
}
.contenido {
	color: #556ba5;
	font-size: 11px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
.contenido2 {
	color: #556ba5;
	font-size: 10px;
	font-family: Arial, Helvetica, sans-serif;
	border: 1px solid;
}
a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px; 
	color: #333366; 
	line-height: normal
}
a:hover {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11px; color: #3270aa; 
	text-decoration: underline; 
	line-height: normal
}
a:visited {
	line-height: normal
}
.mano {
	cursor: hand
}
.bg-ab-rx {
	background-position: center;
	background-repeat:     repeat-x;
}
.item-sep {
	background-color: #FFFFEA;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #265682;
	border: 1px solid #979797;
	padding: 4px;
}
-->
</style>
<!-- '; ?>
 -->
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#F7F7F7">
  <tr> 
    <td height="20" align="right" bordercolor="#EAEAEA" bgcolor="#FFFFEA"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="item"><strong><a href="<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['IMPRIME_FLUJO']; ?>
&id=<?php echo $this->_tpl_vars['id']; ?>
"><img src="/img/800x600/ico_print.gif" width="17" height="17" hspace="2" vspace="2" border="0" align="absmiddle">Imprimir</a></strong></td>
          <td align="right" class="item"><strong><a href="javascript:window.close()">Cerrar</a>&nbsp; 
            </strong></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordesup2">
	  <tr> 
		  <td align="left"><img src="/img/pie-logo.gif" width="124" height="30"></td>
		  <td align="right"><img src="/img/pie-serv-portal.gif" width="312" height="30"></td>
	  </tr>
	</table>
	</td>
	</tr>
  <tr> 
    <td height="100%" valign="top" bgcolor="#FFFFFF">
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="6">
        <tr align="center"> 
          <td height="50" colspan="6" class="textoblack"><p align="center"><strong>OFICINA DE TR&Aacute;MITE DOCUMENTARIO </strong></p>
            <p><strong><?php if ($this->_tpl_vars['idTipoDoc'] != 4): ?><img src="/img/ico-detalle.gif" width="24" height="24" hspace="4" align="absmiddle"><u><?php echo $this->_tpl_vars['numTram']; ?>
</u><?php endif; ?>
          </strong></p></td>
        </tr>
        <tr> 
          <td colspan="6" class="textoblack">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="6" bgcolor="#cedfea" class="textoblack"><div align="center"><strong>DATOS GENERALES DE LA PERSONA </strong></div></td>
        </tr>
        <tr>
          <td class="texto"><strong>SECTOR</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['sector']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr>
          <td class="texto"><strong>TIPO DE PERSONA</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['tipPersona']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>RAZ&Oacute;N SOCIAL</strong></td>
          <td colspan="5" class="texto"><?php echo $this->_tpl_vars['RazonSocial']; ?>
</td>
        </tr>
        <tr> 
          <td class="texto"><strong>DEPARTAMENTO</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['departamento']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>PROVINCIA</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['provincia']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>DISTRITO</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['distrito']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>DIRECCI&Oacute;N</strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['direccion']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>IDENTIFICACI&Oacute;N </strong></td>
          <td class="texto"><?php echo $this->_tpl_vars['tipIdent']; ?>
&nbsp;<?php echo $this->_tpl_vars['nroDoc']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>TEL&Eacute;FONO</strong></td>
          <td class="texto"><?php if (( $this->_tpl_vars['telefono'] != "" && $this->_tpl_vars['telefono'] != ' ' )): ?><?php echo $this->_tpl_vars['telefono']; ?>
<?php else: ?>No especificado<?php endif; ?></td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td class="texto"><strong>E-MAIL</strong></td>
          <td class="texto"><?php if (( $this->_tpl_vars['email'] != "" && $this->_tpl_vars['email'] != ' ' )): ?><?php echo $this->_tpl_vars['email']; ?>
<?php else: ?>No especificado<?php endif; ?></td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
		<?php if ($this->_tpl_vars['RepLegal']): ?>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td colspan="6" bgcolor="#cedfea" class="textoblack"><div align="center"><strong>DATOS DE REPRESENTANTE LEGAL</strong></div></td>
        </tr>
        <tr> 
          <td class="texto"><strong></strong>NOMBRE</td>
          <td class="texto"><?php echo $this->_tpl_vars['RepLegal']; ?>
</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="6" background="/img/apps/bg_horiz.gif" class="bg-ab-rx"><img src="/img/apps/bg_blank.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td class="texto">INDENTIFICACI&Oacute;N </td>
          <td class="texto"><?php if (( $this->_tpl_vars['numRepLegal'] != "" && $this->_tpl_vars['numRepLegal'] != ' ' )): ?><?php echo $this->_tpl_vars['tipIdentRepLegal']; ?>
&nbsp;<?php echo $this->_tpl_vars['numRepLegal']; ?>
<?php else: ?>No especificado<?php endif; ?></td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
          <td class="texto">&nbsp;</td>
        </tr>
		<?php endif; ?>
      </table> 
    </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFEA">&nbsp; </td>
  </tr>
</table>
<?php echo '
<script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert(\'Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.\');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\\"WB\\" WIDTH=\\"0\\" HEIGHT=\\"0\\" CLASSID=\\"CLSID:");
    document.write(wbvers + "\\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    \' Just tidy up when we leave to be sure we aren\'t
    \' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    \' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

\' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
'; ?>

</body>
</html>