<?php /* Smarty version 2.5.0, created on 2013-03-20 18:05:20
         compiled from oad/tramite/reporteNotificacion.tpl.php */ ?>
<?php $this->_load_plugins(array(
array('modifier', 'default', 'oad/tramite/reporteNotificacion.tpl.php', 11, false),)); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6">NOTIFICACION <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['notificacion'], 'NO DISPONIBLE'); ?>
</font></font></b></font>
      </div>
      <hr width="50%" size="1" noshade>  </td>
  </tr>
</table>
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td class="item"><p><font size="+3">El <?php if ($this->_tpl_vars['coddep'] == 5): ?>Secretario General<?php else: ?><?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['funcionario'], 'NO DISPONIBLE'); ?>
<?php endif; ?> del Ministerio de la 
        Producci&oacute;n, sito en Calle Uno Oeste N&deg; 60, Urbanizaci&oacute;n Corpac, 
        San Isidro, Departamento de Lima, de acuerdo a la Ley del Procedimiento 
        Administrativo General, Ley N&deg; 27444, cumple con notificar personalmente la
        <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['acto'], 'NO DISPONIBLE'); ?>
, expedida por <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['detalles'], 'NO DISPONIBLE'); ?>
, en el procedimiento <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['procedimiento'], 'NO DISPONIBLE'); ?>
, 
        a:</font></p>
      <p><font size="+3">DESTINATARIO (s) : <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['destinatario'], 'NO DISPONIBLE'); ?>
</font></p>
      <p><font size="+3">DOMICILIO PROCESAL : <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['domicilio'], 'NO DISPONIBLE'); ?>
</font></p>
      <p><font size="+3"><strong>Marcar con &quot;X&quot; la opci&oacute;n que 
        corresponda:</strong></font></p>
      <p><font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado 
        entra en vigencia</strong> :</font><br><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde 
        la fecha de su emisi&oacute;n <?php if ($this->_tpl_vars['vigencia'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia 
        anticipada)<?php if ($this->_tpl_vars['vigencia'] == 2): ?>(X)<?php else: ?>( )<?php endif; ?></font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n 
        <?php if ($this->_tpl_vars['vigencia'] == 3): ?>(X)<?php else: ?>( )<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n 
        <?php if ($this->_tpl_vars['vigencia'] == 4): ?>(X)<?php else: ?>( )<?php endif; ?></font> </p>
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</strong> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($this->_tpl_vars['estado'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>SI &nbsp;&nbsp;<?php if ($this->_tpl_vars['estado'] == 0): ?>(X)<?php else: ?>( )<?php endif; ?>NO</font> </p>
		
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El administrado podr&aacute; interponer Recurso 
        administrativo de</strong>:</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que 
        lo expidi&oacute;<?php if ($this->_tpl_vars['recurso1'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?> ; &nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o 
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para 
        que se eleve al superior jer&aacute;rquico <?php if ($this->_tpl_vars['recurso2'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>;&nbsp;&nbsp; 
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para 
        que se eleve al superior jer&aacute;rquico<?php if ($this->_tpl_vars['recurso3'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>;</font> 
      </p>
		<p>
        <font size="+3">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as &uacute;tiles 
        (h&aacute;biles consecutivos) contados desde el d&iacute;a siguiente de 
        su fecha de su Notificaci&oacute;n.</font> <br>
        <font size="+3">Se adjunta copia autenticada u original (en su caso) del 
        texto &iacute;ntegro del acto notificado con <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['folio'], 'NO DISPONIBLE'); ?>
 folios.</font></p>
      <p>&nbsp;</p>
      <p><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($this->_tpl_vars['coddep'] == 5): ?>Secretario General<?php else: ?>FIRMA Y SELLO<?php endif; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;Fecha:<?php if ($this->_tpl_vars['coddep'] == 5): ?><br>&nbsp;&nbsp;&nbsp;Ministerio de la Producci&oacute;n<?php endif; ?></font><br>
      <hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que recibe 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3"><strong>FECHA 
        DE NOTIFICACI&Oacute;N</strong> : El acto notificado fue recibido el <?php if ($this->_tpl_vars['persona']): ?><?php echo $this->_tpl_vars['fecha']; ?>
<?php else: ?>..........................<?php endif; ?>, a horas <?php if ($this->_tpl_vars['hora']): ?><?php echo $this->_tpl_vars['hora']; ?>
<?php else: ?>............<?php endif; ?>, 
        por <?php if ($this->_tpl_vars['nombrePersona']): ?><?php echo $this->_tpl_vars['nombrePersona']; ?>
<?php else: ?>......................................................................<?php endif; ?>, 
        identificado con <?php if ($this->_tpl_vars['persona']): ?><?php echo $this->_tpl_vars['persona']; ?>
<?php else: ?>..........................................<?php endif; ?>.</font> 
        <br>
        <font size="+3">Relaci&oacute;n con destinatario .................................................................................................</font> 
        <br>
        <br>
      </p>
      <p align="center"><font size="+3">.................................</font><font size="+3">..........</font><br>
        <font size="+3">Firma del que recibe</font><br>
      <font size="+3">y Sello (si tuviera)</font> </p>
		<hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que realiza 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3">Notificaci&oacute;n 
        rechazada (indicar): <?php if (( $this->_tpl_vars['flag'] == 2 )): ?><?php echo $this->_tpl_vars['Observaciones']; ?>
 <?php else: ?>...............................................................................<?php endif; ?></font> 
        <br>
        <font size="+3">Notificaci&oacute;n no practicada (marcar la opci&oacute;n):</font><br>
        <font size="+3">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Desconocer lugar de posible 
        notificación (domicilio procesal): &nbsp;&nbsp;&nbsp;&nbsp; <?php if ($this->_tpl_vars['motivo'] == 1): ?>(X) 
        <?php else: ?>.....<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Imposibilidad practicar la notificación:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;<?php if ($this->_tpl_vars['motivo'] == 2): ?>(X)<?php else: ?>.....<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Otras circunstancias (indicar):<?php if ($this->_tpl_vars['motivo'] == 3): ?>(X) 
        <?php echo $this->_tpl_vars['Observaciones']; ?>
<?php else: ?> ....................................................................<?php endif; ?> 
        </font> </p>
      <p><br>
        <font size="+3">NOTIFICADOR</font><font size="+2"></font> 
      </p>
      <p><font size="+3">Firma: .............................................. 
        Nombre: ........................................................ DNI: 
        </font><font size="+3">..........</font><font size="+3">....................</font><font size="+3">..........</font></p>
    </td>
  </tr>
</table>

<!--
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><div align="center"><font color="#265682" size="3" face="Arial"><b><font color="#000000"><font size="6">NOTIFICACI&Oacute;N <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['notificacion'], 'NO DISPONIBLE'); ?>
</font></font></b></font>
      </div>
    <hr width="50%" size="1" noshade>  </td>
  </tr>
</table>

  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td class="item"><p><font size="+3">El</font><font size="+3"> <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['funcionario'], 'NO DISPONIBLE'); ?>
 del Ministerio de la 
        Producci&oacute;n, sito en Calle Uno Oeste N&deg; 60, Urbanizaci&oacute;n Corpac, 
        San Isidro, Departamento de Lima, de acuerdo a la Ley del Procedimiento 
        Administrativo General, Ley N&deg; 27444, cumple con notificar personalmente la
        <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['acto'], 'NO DISPONIBLE'); ?>
, expedida por <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['detalles'], 'NO DISPONIBLE'); ?>
, en el procedimiento <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['procedimiento'], 'NO DISPONIBLE'); ?>
, 
        a:</font></p>
      <p><font size="+3">DESTINATARIO (s) : <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['destinatario'], 'NO DISPONIBLE'); ?>
</font></p>
      <p><font size="+3">DOMICILIO PROCESAL : <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['domicilio'], 'NO DISPONIBLE'); ?>
</font></p>
      <p><font size="+3"><strong>Marcar con &quot;X&quot; la opci&oacute;n que 
        corresponda:</strong></font></p>
      <p><font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado 
        entra en vigencia</strong> :</font><br><font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde 
        la fecha de su emisi&oacute;n <?php if ($this->_tpl_vars['vigencia'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde antes de su emisi&oacute;n (eficacia 
        anticipada)<?php if ($this->_tpl_vars['vigencia'] == 2): ?>(X)<?php else: ?>( )<?php endif; ?></font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde el d&iacute;a de notificaci&oacute;n 
        <?php if ($this->_tpl_vars['vigencia'] == 3): ?>(X)<?php else: ?>( )<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desde la fecha indicada en la Resoluci&oacute;n 
        <?php if ($this->_tpl_vars['vigencia'] == 4): ?>(X)<?php else: ?>( )<?php endif; ?></font> </p>
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El acto notificado agota la v&iacute;a administrativa</strong> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($this->_tpl_vars['estado'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>SI &nbsp;&nbsp;<?php if ($this->_tpl_vars['estado'] == 0): ?>(X)<?php else: ?>( )<?php endif; ?>NO</font> </p>
		
      <p> <font size="+3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El administrado podr&aacute; interponer Recurso 
        administrativo de</strong>:</font> <br>
        <font size="+3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reconsideraci&oacute;n ante el mismo &oacute;rgano que 
        lo expidi&oacute;<?php if ($this->_tpl_vars['recurso1'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?> ; &nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o 
        Apelaci&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute; para 
        que se eleve al superior jer&aacute;rquico <?php if ($this->_tpl_vars['recurso2'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>;&nbsp;&nbsp; 
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revisi&oacute;n ante el mismo &oacute;rgano que lo expidi&oacute;, para 
        que se eleve al superior jer&aacute;rquico<?php if ($this->_tpl_vars['recurso3'] == 1): ?>(X)<?php else: ?>( )<?php endif; ?>;</font> 
      </p>
		<p>
        <font size="+3">El t&eacute;rmino para interponer los Recursos Administrativos 
        descritos se podr&aacute; efectuar hasta 15 d&iacute;as &uacute;tiles 
        (h&aacute;biles consecutivos) contados desde el d&iacute;a siguiente de 
        su fecha de su Notificaci&oacute;n.</font> <br>
        <font size="+3">Se adjunta copia autenticada u original (en su caso) del 
        texto &iacute;ntegro del acto notificado con <?php echo $this->_run_mod_handler('default', true, @$this->_tpl_vars['folio'], 'NO DISPONIBLE'); ?>
 folios.</font></p>
      <p>&nbsp;</p>
      <p><font size="+3">&nbsp;&nbsp;&nbsp;FIRMA Y SELLO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;Fecha:</font><br>
      <hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que recibe 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3"><strong>FECHA 
        DE NOTIFICACI&Oacute;N</strong> : El acto notificado fue recibido el <?php if ($this->_tpl_vars['persona']): ?><?php echo $this->_tpl_vars['fecha']; ?>
<?php else: ?>..........................<?php endif; ?>, a horas <?php if ($this->_tpl_vars['hora']): ?><?php echo $this->_tpl_vars['hora']; ?>
<?php else: ?>............<?php endif; ?>, 
        por <?php if ($this->_tpl_vars['nombrePersona']): ?><?php echo $this->_tpl_vars['nombrePersona']; ?>
<?php else: ?>......................................................................<?php endif; ?>, 
        identificado con <?php if ($this->_tpl_vars['persona']): ?><?php echo $this->_tpl_vars['persona']; ?>
<?php else: ?>..........................................<?php endif; ?>.</font> 
        <br>
        <font size="+3">Relaci&oacute;n con destinatario .................................................................................................</font> 
        <br>
        <br>
      </p>
      <p align="center"><font size="+3">.................................</font><font size="+3">..........</font><br>
        <font size="+3">Firma del que recibe</font><br>
      <font size="+3">y Sello (si tuviera)</font> </p>
		<hr width="100%" size="1">
      <font size="+3"><em><strong>(Para ser llenado por la persona que realiza 
      la notificaci&oacute;n)</strong></em></font> <p><font size="+3">Notificaci&oacute;n 
        rechazada (indicar): <?php if (( $this->_tpl_vars['flag'] == 2 )): ?><?php echo $this->_tpl_vars['Observaciones']; ?>
 <?php else: ?>...............................................................................<?php endif; ?></font> 
        <br>
        <font size="+3">Notificaci&oacute;n no practicada (marcar la opci&oacute;n):</font><br>
        <font size="+3">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Desconocer lugar de posible 
        notificación (domicilio procesal): &nbsp;&nbsp;&nbsp;&nbsp; <?php if ($this->_tpl_vars['motivo'] == 1): ?>(X) 
        <?php else: ?>.....<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Imposibilidad practicar la notificación:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;<?php if ($this->_tpl_vars['motivo'] == 2): ?>(X)<?php else: ?>.....<?php endif; ?> <br>
        &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;Otras circunstancias (indicar):<?php if ($this->_tpl_vars['motivo'] == 3): ?>(X) 
        <?php echo $this->_tpl_vars['Observaciones']; ?>
<?php else: ?> ....................................................................<?php endif; ?> 
        </font> </p>
      <p><br>
        <font size="+3">NOTIFICADOR</font><font size="+2"></font> 
      </p>
      <p><font size="+3">Firma: .............................................. 
        Nombre: ........................................................ DNI: 
        </font><font size="+3">..........</font><font size="+3">....................</font><font size="+3">..........</font></p>
    </td>
  </tr>
</table>
-->
</body>
</html>