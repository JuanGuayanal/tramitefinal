<?php /* Smarty version 2.5.0, created on 2013-11-09 11:10:35
         compiled from oad/tramite/ListadoExp.tpl.php */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<p>&nbsp;</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p align="center"><font color="#265682" size="3" face="Arial"><b><br>
        <font color="#000000"> <font size="6">LISTADO DE EXPEDIENTES ENTRE EL <?php echo $this->_tpl_vars['fecInicio2']; ?>
 Y <?php echo $this->_tpl_vars['fecFin2']; ?>
</font> <br>
        </font></b></font> <br>
      
      <hr width="100%" size="1" noshade> </p> </td>
  </tr>
</table>
<table border="1" width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr bgcolor="#999999">
  	<td bgcolor="#999999"><div align="center"><font size="5"><strong>N&deg;</strong></font></div></td> 
    <td width="10%"> 
      <div align="center"><font size="5"><strong>N&deg; DE TR&Aacute;MITE</strong></font></div></td>
    <td width="8%"> 
      <div align="center"><font size="5"><strong>FECHA DE RECEPCI&Oacute;N</strong></font></div></td>
    <td width="12%"> 
      <div align="center"><font size="5"><strong>INDICATIVO</strong></font></div></td>
    <td width="25%"> 
      <div align="center"><font size="5"><strong>REMITENTE</strong></font></div></td>
    <td width="3%"><font size="5"><strong>FOLIOS</strong></font></td>
    <td width="42%"> 
      <div align="center"><font size="5"><strong>PROCEDIMIENTO</strong></font></div></td>
  </tr>
  <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['list']) ? count($this->_tpl_vars['list']) : max(0, (int)$this->_tpl_vars['list']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
  <tr>
  	<td><div align="center"><font size="5"><?php echo $this->_sections['i']['iteration']; ?>
</font></div></td> 
    <td width="10%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['numTram']; ?>
</font></td>
    <td width="8%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['fecRec']; ?>
</font></td>
    <td width="12%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['oficio']; ?>
</font></td>
    <td width="25%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['razSoc']; ?>
</font></td>
    <td width="3%"><div align="center"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['folio']; ?>
</font></div></td>
    <td width="42%"><font size="5"><?php echo $this->_tpl_vars['list'][$this->_sections['i']['index']]['proc']; ?>
</font></td>
  </tr>
  <?php endfor; else: ?>  
  <tr>
    <td colspan="6"><div align="center"><font size="6"><strong>No se han ingresado 
        expedientes en la(s) fecha(s) dada(s).</strong></font></div></td>
  </tr>
  <?php endif; ?>
</table>
<p>&nbsp;</p>
<p align="right"><font color="#FF0000" size="5" face="Arial"><i>Actualizado al 
  <?php echo $this->_tpl_vars['fechaGen']; ?>
<?php echo $this->_tpl_vars['hora']; ?>
s</i></font></p>
</body>
</html>