<?php /* Smarty version 2.5.0, created on 2015-01-19 16:18:36
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/reportesDocDir.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<!-- <?php echo '  -->
<script language="JavaScript" type="text/javascript" src="/js/calendarMatrix.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/convert.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function CalendarHide(){
	MM_showHideLayers(\'popCalFin\',\'\',\'hide\')
	MM_showHideLayers(\'popCalIni\',\'\',\'hide\')
}

document.onclick = CalendarHide
-->
</script>
<script language="JavaScript">
<!--
ie = document.all?1:0
function hL(E){
	var cTR = (E.checked) ? \'tr-check\' : \'tr-nocheck\'
	if (ie){
		while (E.tagName!="TABLE")
			E=E.parentElement;
	}else{
		while (E.tagName!="TABLE")
			E=E.parentNode;
	}
	E.className = cTR;
}
function ninguna_letra(){
		//if ((event.keyCode < 48 || event.keyCode > 57)) {
				event.returnValue = false;
		//}
}
-->
</script>
<!-- '; ?>
 -->
<script src="/sitradocV3/js/src/reportes/frmReporteDocumentos.js"></script>
<br>
<div style="width:700px;">
<div class="std_form">
<form name="<?php echo $this->_tpl_vars['frmName']; ?>
" action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" target="_blank" >
  <input name="page" type="hidden" id="page2">
  <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['GENERAREPORTEDIR']; ?>
">
  <input name="GrupoOpciones1" type="hidden" id="GrupoOpciones1" value="21">
  <input name="versionAnt" type="hidden" id="versionAnt" > 
  <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla-encuestas">
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="label"><b>Tipo de Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipoDocc" class="ipseln" id="tipDoc" onChange="document.<?php echo $this->_tpl_vars['frmName']; ?>
.versionAnt.value=2;document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERAREPORTEDIR']; ?>
')">
          <option >Todos</option>
          <option value="1"<?php if ($this->_tpl_vars['tipoDocc'] == 1): ?> selected<?php endif; ?>>Documentos Externos</option>
          <option value="2"<?php if ($this->_tpl_vars['tipoDocc'] == 2): ?> selected<?php endif; ?>>Expedientes</option>
          <option value="4"<?php if ($this->_tpl_vars['tipoDocc'] == 4): ?> selected<?php endif; ?>>Documentos Internos</option>
        </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="label"><b>Estado del Documento</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="status" class="ipseln" id="status" onChange="document.<?php echo $this->_tpl_vars['frmName']; ?>
.versionAnt.value=2;document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERAREPORTEDIR']; ?>
')">
          <!--<option>Todos</option>-->
          <!--<option value="5" <?php if ($this->_tpl_vars['status'] == 5): ?> selected<?php endif; ?> class="std_oculto">Todos</option>-->
          <option value="1" <?php if ($this->_tpl_vars['status'] == 1): ?> selected<?php endif; ?>>Pendientes</option>
          <option value="11" <?php if ($this->_tpl_vars['status'] == 11): ?> selected<?php endif; ?> class="std_oculto">Pendientes (Congreso)</option>
          <option value="2" <?php if ($this->_tpl_vars['status'] == 2): ?> selected<?php endif; ?>>Asignados a un trabajador</option>
          <option value="3" <?php if ($this->_tpl_vars['status'] == 3): ?> selected<?php endif; ?>>Documentos Generados</option>
          <option value="4" <?php if ($this->_tpl_vars['status'] == 4): ?> selected<?php endif; ?>>Archivados</option>
		  <?php if (( ( $this->_tpl_vars['coddep'] == 13 || $this->_tpl_vars['coddep'] == 2 ) && $this->_tpl_vars['tipoDocc'] == 2 )): ?><option value="9" <?php if ($this->_tpl_vars['status'] == 9): ?> selected<?php endif; ?>>Pendientes que han ingresado a partir del 04/01/08</option><?php endif; ?>
		  <?php if (( ( $this->_tpl_vars['coddep'] == 13 || $this->_tpl_vars['coddep'] == 2 ) && $this->_tpl_vars['tipoDocc'] == 2 )): ?><option value="8" <?php if ($this->_tpl_vars['status'] == 8): ?> selected<?php endif; ?>>Finalizados a partir del 04/01/08</option><?php endif; ?>
		  <?php if (( $this->_tpl_vars['coddep'] == 1 || $this->_tpl_vars['coddep'] == 13 )): ?><option value="6" <?php if ($this->_tpl_vars['status'] == 6): ?> selected<?php endif; ?>>Registros asignados</option><?php endif; ?>
		  <?php if (( $this->_tpl_vars['coddep'] == 13 && $this->_tpl_vars['tipoDocc'] == 2 )): ?><option value="7" <?php if ($this->_tpl_vars['status'] == 7): ?> selected<?php endif; ?>>Estadísticas</option><?php endif; ?>
        </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
	<?php if (( $this->_tpl_vars['status'] == 3 )): ?>
    <tr> 
      <td align="right" class="label">Por tipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipoDoc" class="ipseln" id="tipoDoc" onChange="document.<?php echo $this->_tpl_vars['frmName']; ?>
.versionAnt.value=2;document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERAREPORTEDIR']; ?>
')">
        <?php echo $this->_tpl_vars['selTipoDoc']; ?>

		<?php if (( $this->_tpl_vars['codigoDependenciaF'] == 1 )): ?>
			<option value="994" <?php if (( $this->_tpl_vars['tipoDoc'] == 994 )): ?> selected <?php endif; ?>>Memorando DM Ases</option>
			<option value="995" <?php if (( $this->_tpl_vars['tipoDoc'] == 995 )): ?> selected <?php endif; ?>>Cargo DM Ases</option>
			<option value="996" <?php if (( $this->_tpl_vars['tipoDoc'] == 996 )): ?> selected <?php endif; ?>>Informe DM Ases</option>
			<option value="998" <?php if (( $this->_tpl_vars['tipoDoc'] == 998 )): ?> selected <?php endif; ?>>Nota DM Ases</option>
			<option value="997" <?php if (( $this->_tpl_vars['tipoDoc'] == 997 )): ?> selected <?php endif; ?>>Oficio DM Ases</option>
			<option value="999" <?php if (( $this->_tpl_vars['tipoDoc'] == 999 )): ?> selected <?php endif; ?>>Nota DM J.Gab.</option>
		<?php endif; ?>
       </select>
	  	&nbsp;&nbsp;
	  </td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
	<?php endif; ?>
    <?php if (( $this->_tpl_vars['status'] == 7 || $this->_tpl_vars['status'] == 1 || $this->_tpl_vars['status'] == 8 || $this->_tpl_vars['status'] == 9 )): ?> 
    <tr class="std_oculto"> 
      <td align="right" class="textoblack"><b>Por tipo de aprobación</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="tipAprobacion" class="ipseln" id="tipAprobacion" onChange="document.<?php echo $this->_tpl_vars['frmName']; ?>
.versionAnt.value=2;document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERAREPORTEDIR']; ?>
')">
          <option >Todos</option>
          <option value="1"<?php if ($this->_tpl_vars['tipAprobacion'] == 1): ?> selected<?php endif; ?>>Silencio Positivo</option>
          <option value="4"<?php if ($this->_tpl_vars['tipAprobacion'] == 4): ?> selected<?php endif; ?>>Silencio Negativo</option>
          <option value="2"<?php if ($this->_tpl_vars['tipAprobacion'] == 2): ?> selected<?php endif; ?>>Automático</option>
		  <option value="3"<?php if ($this->_tpl_vars['tipAprobacion'] == 3): ?> selected<?php endif; ?>>Por definir</option>
       </select></td>
    </tr>
    <tr class="std_oculto"> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    <?php endif; ?>	
	<?php if (( ( $this->_tpl_vars['status'] == 3 || $this->_tpl_vars['status'] == 7 || $this->_tpl_vars['status'] == 8 || $this->_tpl_vars['status'] == 9 || ( $this->_tpl_vars['status'] == 1 && ( $this->_tpl_vars['coddep'] == 13 || $this->_tpl_vars['coddep'] == 2 || $this->_tpl_vars['coddep'] == 36 || $this->_tpl_vars['coddep'] == 16 || $this->_tpl_vars['coddep'] == 5 ) && $this->_tpl_vars['tipoDocc'] == 2 ) ) ) || ( $this->_tpl_vars['status'] == 11 ) || ( $this->_tpl_vars['status'] == 1 && $this->_tpl_vars['coddep'] == 12 )): ?> 
    <tr> 
      <td align="right" class="label"><b>Por Unidad</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="depInicial" class="ipseln" id="depInicial" onChange="document.<?php echo $this->_tpl_vars['frmName']; ?>
.versionAnt.value=2;document.<?php echo $this->_tpl_vars['frmName']; ?>
.target='_self';submitForm('<?php echo $this->_tpl_vars['accion']['FRM_GENERAREPORTEDIR']; ?>
')">
		<?php echo $this->_tpl_vars['selDepInicial']; ?>

       </select></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    <?php endif; ?>
    <?php if (( $this->_tpl_vars['status'] != 3 && $this->_tpl_vars['status'] != 7 )): ?> 
    <tr> 
      <td align="right" class="label"><b>Por trabajador</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" class="tree"><select name="trabajador" class="ipseln" id="trabajador"><option value="vacio"></option>
        <?php echo $this->_tpl_vars['selTrabajador']; ?>

       </select></td>
    </tr>
    <tr> 
      <td align="center" class="tree">&nbsp;</td>
      <td align="center" class="tree">&nbsp;</td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td align="right" class="label">Por Fechas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="left" class="label"><span class="label">Desde</span> <input name="desFechaIni" type="text" class="iptxtn datepicker" id="desFechaIni" tabindex="4" onKeyPress="ninguna_letra();" value="<?php echo $this->_tpl_vars['desFechaIni']; ?>
" size="10" /> <span class="label">hasta</span> <input name="desFechaFin" type="text" class="iptxtn datepicker" id="desFechaFin" tabindex="5" onKeyPress="ninguna_letra();" value="<?php echo $this->_tpl_vars['desFechaFin']; ?>
" size="10" /></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top"><input name="bsubmit" type="submit" class="std_button" value="Generar" /></td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
    <tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <!--<tr> 
      <td align="right" class="textoblack">&nbsp;</td>
      <td colspan="2" align="center" class="trees"><a href="/institucional/aplicativos/oad/sitradocV2/index.php?accion=frmGeneraReporteDir&versionAnt=1"><b>Otros 
        Reportes</b></a></td>-->
    </tr>
  </table>
</form>
</div>
</div>