<?php /* Smarty version 2.5.0, created on 2015-02-23 14:22:17
         compiled from correspondencia/configuracion/servicio_prioridad.tpl.php */ ?>
<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>SERVICIO - PRIORIDAD PARA CORRESPONDENCIA</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=insertaServicioPriCorr">
        <p> 
			<label for="">Descripci&oacute;n</label>
			<input name="descripcion" required="required" type="text" class="std_input" />
		</p>
        <p> 
			<label>Servicio</label>
			<select name="servicio"><?php echo $this->_tpl_vars['selServicio']; ?>
</select>
		</p>
        <p> 
			<label>Prioridad</label>
			<select name="prioridad"><?php echo $this->_tpl_vars['selPrioridad']; ?>
</select>		</p>
        <p> 
			<label for="">Tiempo de entrega</label>
			<input name="entrega" required="required" type="text" class="std_input" />
		</p>
        <p> 
			<label for="">Tiempo de devoluci&oacute;n</label>
			<input name="devolucion" required="required" type="text" class="std_input" />
		</p>
        <p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Agregar Parametros</button></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
<form method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=actualizarServicioPriCorr">
<table class="std_grilla">
	<tr><th>N&deg;</th><th>Servicio - Prioridad</th><th>Servicio</th><th>Prioridad</th><th>Tiempo de Entrega</th><th>Tiempo de Devoluci&oacute;n</th><th colspan="2">Activo</th></tr>
<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['serv_pri']) ? count($this->_tpl_vars['serv_pri']) : max(0, (int)$this->_tpl_vars['serv_pri']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
<tr>
	<td><?php echo $this->_sections['i']['iteration']; ?>
<input type="hidden" name="arreglo[id_serv_pri][]" value="<?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['id_serv_pri']; ?>
" /></td>
    <td><input type="text" value="<?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['descripcion']; ?>
" name="arreglo[descripcion][]"/></td>
    <td><?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['servicio']; ?>
</td>
    <td><?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['prioridad']; ?>
</td>
    <td><input type="text" value="<?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['entrega']; ?>
" name="arreglo[entrega][]"/></td>
    <td><input type="text" value="<?php echo $this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['devolucion']; ?>
" name="arreglo[devolucion][]"/></td>
    <td><?php if ($this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['activo'] == 1): ?><img src="/img/800x600/activo.png" /><?php else: ?><img src="/img/800x600/inactivo.png" /><?php endif; ?></td>
    <td><select class="sel_activarsp" name="arreglo[activo][]">
    <option value="1" <?php if ($this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['activo'] == 1): ?>selected="selected"<?php endif; ?>>ACTIVO</option>
    <option value="0" <?php if ($this->_tpl_vars['serv_pri'][$this->_sections['i']['index']]['activo'] == 0): ?>selected="selected"<?php endif; ?>>INACTIVO</option></select>
    </td>
    
</tr>
<?php endfor; endif; ?>
</table>
<div class="center"><p><button class="std_button" id="btn_CambiarContrasena" rel="titulo">Guardar Cambios</button></p></div>
</form>
</div>
<script src="/sitradocV3/js/src/correspondencia/configuracion/servicio.js"></script>