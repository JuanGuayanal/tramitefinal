<?php /* Smarty version 2.5.0, created on 2014-11-06 12:28:55
         compiled from perfil/cambiar_contrasena.tpl.php */ ?>
<div class="center">
<div id="std_cuerpoCambiarContra">
    <div class="std_form">
		<h1>CAMBIAR CONTRASE&Ntilde;A</h1>
    	<form id="form_actualizar" method="post" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=Perfil_CambiarContrasena">
        <p> 
			<label for="password">Contrase&ntilde;a actual</label>
			<input id="password" name="password" required="required" type="password" class="std_input" />
		</p>
		<p> 
			<label for="npassword">Nueva contrase&ntilde;a</label>
			<input id="npassword" name="npassword" required="required" type="password" class="std_input"/> 
		</p>
		<p> 
			<label for="repassword">Verifique nueva contrase&ntilde;a</label>
			<input id="repassword" name="repassword" required="required" type="password" class="std_input"/> 
		</p>
        <p><input type="submit" class="std_button" id="btn_CambiarContrasena" value="Cambiar contrase&ntilde;a" /></p>
    	</form>
    </div>
</div>
<div class="limpiar"></div>
</div>
<script type="text/javascript" src="/sitradocV3/js/src/frmCambiar_contrasena.js"></script>