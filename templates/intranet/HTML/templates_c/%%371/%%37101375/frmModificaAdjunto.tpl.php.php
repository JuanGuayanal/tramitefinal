<?php /* Smarty version 2.5.0, created on 2015-01-28 15:32:33
         compiled from oad/tramite/nuevaVersionFormularios/formsOada/frmModificaAdjunto.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<script language="JavaScript">
<!--
<?php echo '
function solo_num(){
	if ((event.keyCode < 48 || event.keyCode > 57)) {
			event.returnValue = false;
	}
}
'; ?>

-->
</script>
<script src="/sitradocV3/js/src/mesadepartes/frmModAdjunto.js"></script>
<div style="width:750px;">
<div class="std_form">
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onSubmit="MM_validateForm('destino','Dependencia destino','Sel','Persona','Persona destino','Sel','tipDocumento','El tipo de Documento','Sel','contenido','El Contenido','R','folios','N�mero de folios','R');return document.MM_returnValue">
  <table width="700" border="0" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="4" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> <br>
      <td width="25%" class="item" align="left"><strong>No del documento </strong></td>
      <td colspan="3" class="item" align="left"><?php echo $this->_tpl_vars['nroadjunto']; ?>
</td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Fecha de Emisi&oacute;n</strong></td>
      <td width="26%" class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
</td>
      <td width="23%" class="item" align="left"><strong>Hora del Emisi�n</strong></td>
      <td width="26%" class="item" align="left"><?php echo $this->_tpl_vars['HoraActual']; ?>
</td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
	<tr>
    	<td class="item" align="left" width="134"><strong>Remitente</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn ui-autocomplete-input" id="personal" size="80" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"><input type="hidden" value="<?php echo $this->_tpl_vars['id_persona']; ?>
" name="id_persona" id="id_persona"/></td>
	</tr>
	<tr> 
		<td class="item" align="left" valign="middle"><strong>Apellidos y Nombres<br>Raz�n Social</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_domicilio" disabled="disabled" class="iptxtn" id="_domicilio"><?php echo $this->_tpl_vars['razon_social']; ?>
</textarea></td>
	</tr>
	<tr> 
		<td class="item" align="left"><strong>DNI / RUC</strong></td>
		<td colspan="3" align="left"><input type="text" class="iptxtn" id="_ruc" disabled="disabled" value="<?php echo $this->_tpl_vars['ruc']; ?>
"></td>
	</tr>
    <tr>
		<td class="item" align="left" valign="middle"><strong>Domicilio</strong></td>
		<td colspan="3" align="left"><textarea cols="50" rows="2" name="_domicilio" disabled="disabled" class="iptxtn" id="_domicilio"><?php echo $this->_tpl_vars['direccion']; ?>
</textarea></td>
	</tr>
    <tr> 
      <td class="item" align="left"><strong>Tipo de Documento</strong></td>
      <td align="left"> <select name="tipDocumento" class="ipseln" id="tipDocumento" style="width:190px;">
	  <?php echo $this->_tpl_vars['selDocumento']; ?>

       </select> </td>
      <td class="item"><div align="right"><strong>N&deg; de Folios</strong></div></td>
      <td align="left"><input name="folios" type="text" class="iptxtn" onKeyPress="solo_num();" value="<?php if ($this->_tpl_vars['folios'] == ''): ?>1<?php else: ?><?php echo $this->_tpl_vars['folios']; ?>
<?php endif; ?>" size="4" maxlength="5"></td>
    </tr>
    <tr> 
      <td class="item"><strong>Documento Externo</strong></td>
      <td colspan="3" align="left"> <input type="text" name="observaciones" style="width:190px;" class="iptxtn" value="<?php echo $this->_tpl_vars['observaciones']; ?>
" />
      </td>
    </tr>
    <tr> 
      <td class="item" align="left" valign="middle"><strong>Asunto</strong></td>
      <td colspan="3" align="left"> <textarea name="contenido" cols="50" rows="3" class="iptxtn"><?php echo $this->_tpl_vars['contenido']; ?>
</textarea> 
      </td>
    </tr>
    <tr> 
      <td class="item" align="left"><strong>Destino</strong></td>
      <td colspan="3" align="left"> <select name="Destino" class="ipseln" style="width:340px;" id="dependencia">
	  <?php echo $this->_tpl_vars['selDestino']; ?>

       </select> <button id="std_btn_agrega_dep" class="std_button">Agregar</button></td>
    </tr>
    <tr class="std_oculto"> 
      <td class="item" align="left"><strong>Se&ntilde;or</strong></td>
      <td colspan="3" align="left"> <select name="Persona" class="ipseln" style="width:190px;">
	  <?php echo $this->_tpl_vars['selPersona']; ?>

	   </select> </td>
    </tr>
    <tr>
    	<td colspan="4">
                 <table id="lista_dependencias">
                     <thead>	
                        <tr>
                            <th width="90%">UNIDAD</th>
                            <th width="90%">ACCIONES</th>
                            <th>&nbsp;</th>
                        </tr>
                   	</thead>
                    <tbody>
    	    <?php if (isset($this->_sections['customer'])) unset($this->_sections['customer']);
$this->_sections['customer']['name'] = 'customer';
$this->_sections['customer']['loop'] = is_array($this->_tpl_vars['DependenciasAct']) ? count($this->_tpl_vars['DependenciasAct']) : max(0, (int)$this->_tpl_vars['DependenciasAct']);
$this->_sections['customer']['show'] = true;
$this->_sections['customer']['max'] = $this->_sections['customer']['loop'];
$this->_sections['customer']['step'] = 1;
$this->_sections['customer']['start'] = $this->_sections['customer']['step'] > 0 ? 0 : $this->_sections['customer']['loop']-1;
if ($this->_sections['customer']['show']) {
    $this->_sections['customer']['total'] = $this->_sections['customer']['loop'];
    if ($this->_sections['customer']['total'] == 0)
        $this->_sections['customer']['show'] = false;
} else
    $this->_sections['customer']['total'] = 0;
if ($this->_sections['customer']['show']):

            for ($this->_sections['customer']['index'] = $this->_sections['customer']['start'], $this->_sections['customer']['iteration'] = 1;
                 $this->_sections['customer']['iteration'] <= $this->_sections['customer']['total'];
                 $this->_sections['customer']['index'] += $this->_sections['customer']['step'], $this->_sections['customer']['iteration']++):
$this->_sections['customer']['rownum'] = $this->_sections['customer']['iteration'];
$this->_sections['customer']['index_prev'] = $this->_sections['customer']['index'] - $this->_sections['customer']['step'];
$this->_sections['customer']['index_next'] = $this->_sections['customer']['index'] + $this->_sections['customer']['step'];
$this->_sections['customer']['first']      = ($this->_sections['customer']['iteration'] == 1);
$this->_sections['customer']['last']       = ($this->_sections['customer']['iteration'] == $this->_sections['customer']['total']);
?>
    	    <tr valign="middle">
            
           <td><input type="hidden" value="<?php echo $this->_tpl_vars['DependenciasAct'][$this->_sections['customer']['index']]['id']; ?>
" name="dependencias_multiples[]"/><?php echo $this->_tpl_vars['DependenciasAct'][$this->_sections['customer']['index']]['dependencia']; ?>
</td>
           <td><input type="hidden" name="arr_derlista[acciones][]" value="<?php echo $this->_tpl_vars['DependenciasAct'][$this->_sections['customer']['index']]['idacciones']; ?>
"/><input type="text" class="text_deracciones iptxtn" value="<?php echo $this->_tpl_vars['DependenciasAct'][$this->_sections['customer']['index']]['acciones']; ?>
"/></td>
           <td><?php if ($this->_tpl_vars['DependenciasAct'][$this->_sections['customer']['index']]['aceptado'] == ''): ?><a href="#" class="btn_eliminardep"> X </a><?php endif; ?></td>
    	      </tr>
    	    <?php endfor; endif; ?>
                    </tbody>
                 </table>
        </td>
    </tr>
    <tr> 
      <td colspan="4"><hr size="1"></td>
    </tr>
    <tr align="center"> 
  <!--    <td colspan="4"> <input type="submit" name="Submit" value="Modificar Adjunto" class="submitV2">           23/04/2013--->
      <td colspan="4"> <input type="submit" name="Submit" value="Modificar Documento Asociado" class="std_button" id="modificar_adjunto"> 
        &nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCUMENTO']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCUMENTO']; ?>
');return document.MM_returnValue">
        &nbsp; <input name="imphr" type="button" class="std_button" value="Imprimir Hoja de Tr&aacute;mite" onClick="window.open('/institucional/aplicativos/oad/sitradocV2/index.php?accion=HojaRuta&id=<?php echo $this->_tpl_vars['idDocPadre']; ?>
','HOJA DE TRAMITE','height=700,width=800,scrollTo,resizable=1,scrollbars=1,location=0');return false;"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['MODIFICA_ADJUNTO']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
		<input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="reLoad" type="hidden" id="reLoad" value="1">
        <input name="idAdj" type="hidden" id="idAdj" value="<?php echo $this->_tpl_vars['idAdj']; ?>
"> 
        <input name="idDocPadre" type="hidden" id="idDocPadre" value="<?php echo $this->_tpl_vars['idDocPadre']; ?>
"> 
		<input name="nroadjunto" type="hidden" id="nroadjunto" value="<?php echo $this->_tpl_vars['nroadjunto']; ?>
"></td>
    </tr>
  </table>
</form>
</div>
</div>
<div id="modal_deracciones" class="std_oculto">
<div class="std_form">
		<h1>ACCIONES</h1>
        <table>
    	<?php echo $this->_tpl_vars['acciones']; ?>

        </table><br />
<div><span class="std_button" id="btn_aceptarAcciones">Aceptar</span></div>
    </div>
</div>