<?php /* Smarty version 2.5.0, created on 2016-09-20 15:53:32
         compiled from utilitarios/frmReasignar.tpl.php */ ?>
<script src="/sitradocV3/js/src/utilitarios/frmReasignar.js"></script>
<?php 
if(isset($_GET['mensaje'])){
	echo '<div id="mensaje"><br /><table width="100%" border="0" cellspacing="0" cellpadding="4" bgcolor="#FFFFEA"><tbody><tr><td width="15" valign="top" align="center"><img src="/img/800x600/ico-info2.gif" width="15" alt="[Información del Proceso]" height="12"></td>
    <td class="textored" align="center"><strong>LA EJECUCI&Oacute;N DE LA TAREA HA SIDO LLEVADA A CABO CON &Eacute;XITO</strong>';
    if($_GET['mensaje']!=''){ echo '<br><strong>'.$_GET['mensaje'].'</strong>';}
    
    echo '</td><td align="right"><a href="#" id="cerrar_mensaje">x</a></td></tr></tbody></table></div>';
}

 ?>
<br>
<form method="post" id="frm_reasignar" action="/institucional/aplicativos/oad/sitradocV2/index.php?accion=reasignaDoc">
<table align="left" style="border: dashed 1px #CCC; margin-left:62px;" width="725px">

<tr>
	<td align="left">Termino a Buscar</td>
    <td align="left"><select class="ipseln" id="clase"><?php echo $this->_tpl_vars['selClaseDocumento']; ?>
</select> <input name="indicativo" type="text" class="iptxtn" size="15" id="indicativo"> <select class="ipseln" id="anio"><?php echo $this->_tpl_vars['selAnio']; ?>
</select> <select class="ipseln" id="unidad"><?php echo $this->_tpl_vars['selSiglas']; ?>
</select><input name="termino" type="text" class="std_oculto" size="10" id="termino"> <button class="std_button" id="buscar">Buscar</button></td>
</tr>
<tr>
	<td></td>
	<td align="left"></td>
</tr>
</table>
<div class="limpiar"></div><br /><br />
<table width="100%">
<tr valign="top">
<td align="left" colspan="2">
<table id="b_documentos" width="600">
<thead>
<tr>
	<th>FECHA DE ASIGNACION</th>
	<th>TIPO DOCUMENTO</th>
	<th>N&deg; DOC.</th>
	<th>REFERENCIA</th>
	<th>TRABAJADOR</th>
    <th><input type="checkbox" id="marcar_todos" /></th>
</tr>
</thead>
</table>
</td>
<td rowspan="3"><p>&nbsp;</p>
      <p>&nbsp;</p></td>
<td rowspan="3" align="left"><select name="trabajadores" id="trabajadores" class="ipseln" style="width:150px;"></select><button name="agregar" class="submitV2" id="agregar"> + </button>
<br />
<table id="b_trabajadores">
<thead>
<tr>
	<th>TRABAJADOR</th>
    <th></th>
</tr>
</thead>
</table>
<br /><br /><br /><div class="limpiar"></div>
<input type="submit" id="btn_reasignar" class="std_button" value="Asignar" /></td>
</tr>
<tr align="left" class="std_oculto">
  <td valign="top">&iquest;Deasea reasignar<br />con documento?</td><td> Si <input type="radio" name="requiere_documento" value="1" /> &nbsp; No <input type="radio" name="requiere_documento" value="0" checked="checked" id="requiere_documento"/> &nbsp; <select name="id_clase_documento" id="id_clase_documento" class="std_oculto ipseln"><?php echo $this->_tpl_vars['selClaseDocumento']; ?>
</select></td>
<tr class="std_oculto" id="tr_asunto" align="left">
  <td valign="middle">Asunto</td><td><textarea name="asunto" cols="94" rows="5" class="iptxtn"></textarea><br /><br /></td></tr>
<tr align="left" class="std_oculto">
  <td valign="middle">Observaciones</td><td><textarea name="obs" cols="94" rows="5" class="iptxtn"></textarea><br /><br /></td></tr>
<tr id="tr_acciones" align="left" class="std_oculto"><td valign="top">Acciones a Realizar &nbsp;&nbsp;&nbsp;</td>
    <td><table width="500" align="left" id="acc"><?php echo $this->_tpl_vars['acciones']; ?>
</table></td>
</tr>
<tr align="left"><td valign="top">&nbsp;</td>
    <td><input type="submit" id="btn_eliminar_asignacion" class="std_button" value="Eliminar Asignaci&oacute;n" /> (Se elimina la asignaci&oacute;n para los registros marcados con check)</td>
</tr>
</table>
</form>