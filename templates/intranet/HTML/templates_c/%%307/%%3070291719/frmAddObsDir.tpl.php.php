<?php /* Smarty version 2.5.0, created on 2013-01-22 10:13:12
         compiled from oad/tramite/nuevaVersionFormularios/formsDireccionesGenerales/frmAddObsDir.tpl.php */ ?>
<br>
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" onSubmit="MM_validateForm('observaciones','Observaciones','R');return document.MM_returnValue">
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td class="texto td-encuesta"><strong>Fecha</strong></td>
      <td class="item"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta"> <strong>Avances</strong></td>
      <td align="left"> <textarea name="observaciones" cols="65" rows="5" class="iptxtn" id="observaciones" ><?php echo $this->_tpl_vars['observaciones']; ?>
</textarea> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
    <tr align="center"> 
      <td colspan="3"> <input name="bSubmit" type="Submit" class="submitV2" value="Guardar" > 
        &nbsp;&nbsp; <input name="cancel" type="button" class="submitV2" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['BUSCA_DOCDIR']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_TRAB']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['BUSCA_DOCDIR']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['ADDOBS_DOCDIR']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="idObsDir" type="hidden" id="idObsDir" value="<?php echo $this->_tpl_vars['idObsDir']; ?>
">
		<input name="nroTD" type="hidden" id="nroTD" value="<?php echo $this->_tpl_vars['nroTD']; ?>
">
		<input name="page" type="hidden" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
">
		<input name="FechaIni" type="hidden" id="FechaIni" value="<?php echo $this->_tpl_vars['FechaIni']; ?>
">
		<input name="FechaFin" type="hidden" id="FechaFin" value="<?php echo $this->_tpl_vars['FechaFin']; ?>
">
		<?php if ($this->_tpl_vars['tipEstado'] > 0): ?><input name="tipEstado" type="hidden" id="tipEstado" value="<?php echo $this->_tpl_vars['tipEstado']; ?>
"><?php endif; ?>		
		<?php if ($this->_tpl_vars['checkTodos'] == 1): ?><input name="checkTodos" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkTodos']; ?>
"><?php endif; ?> 
		<?php if ($this->_tpl_vars['checkTrab'] == 1): ?><input name="checkTrab" type="hidden" id="checkTrab" value="<?php echo $this->_tpl_vars['checkTrab']; ?>
"><?php endif; ?> 
		<?php if ($this->_tpl_vars['checkRazon'] == 1): ?><input name="checkRazon" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkRazon']; ?>
"> <?php endif; ?>
		<?php if ($this->_tpl_vars['checkAsunto'] == 1): ?><input name="checkAsunto" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkAsunto']; ?>
"> <?php endif; ?>
		 </td>
    </tr>
  </table>
</form>