<?php /* Smarty version 2.5.0, created on 2015-01-21 17:05:01
         compiled from reportes/reporteDocumentosPDF.tpl.php */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<!-- HEADER LEFT "$LOGOIMAGE" --> 
<!-- FOOTER LEFT "Impreso el: <?php echo date('d/m/Y H:i:s'); ?>" -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><b><div align="center"><font color="#000000" size="6" face="Arial">REPORTE DE DOCUMENTOS
                <?php if ($this->_tpl_vars['estado'] == 1): ?>PENDIENTES
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> DE TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> DE <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 2): ?>PENDIENTES DELEGADOS / DERIVADOS A 
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?>TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?><?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>GENERADOS<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>GENERADOS / DERIVADOS / ENVIADO<br><?php echo $this->_tpl_vars['session_trab']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>ARCHIVADOS
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?>DE TODOS LOS TRABAJADORES<?php endif; ?>
					<?php if ($this->_tpl_vars['trabajador'] > 0): ?>POR <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>ARCHIVADOS POR<br><?php echo $this->_tpl_vars['session_trab']; ?>
<?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 11): ?>PENDIENTES DEL CONGRESO DE LA REPUBLICA
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> EN TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> EN <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['estado'] == 11): ?>PENDIENTES DEL CONGRESO DE LA REPUBLICA  
                	<?php if ($this->_tpl_vars['trabajador'] == '0'): ?> EN TODOS LOS TRABAJADORES<?php endif; ?>
                    <?php if ($this->_tpl_vars['trabajador'] > 0): ?> EN <?php echo $this->_tpl_vars['nombre']; ?>
<?php endif; ?>
                <?php endif; ?>
    	</font>
      </div></b>
    <hr width="100%" size="1" noshade>  </td>
  </tr>
</table>
                    <?php if ($this->_tpl_vars['estado'] == 1): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 11): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><th>DESTINATARIO DE<br>CORRESPONDENCIA</th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['correspondencia']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 2): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                       <thead><tr bgcolor="#999999"><th>N&deg;</th>
                       <th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA CREACI&Oacute;N DE REGISTRO</th><th>FECHA DE DERIVACI&Oacute;N</th><th>UBICACI&Oacute;N</th>
                         <th>OBSERVACIONES Y/O REFERENCIAS</th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><<td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th><th>DESTINATARIO DE<br>CORRESPONDENCIA</th><th>ESTADO</th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['correspondencia']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estado']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 3 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>EXPEDIENTE</th><th>FECHA DE DERIVACI&Oacute;N</th><th>DESTINO</th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 1): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>TRABAJADOR</th><th>MOTIVO DE<br>ARCHIVO</th><th>AVANCE</th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['trabajador_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 4 && $this->_tpl_vars['tipo_trabajador'] == 2): ?>
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>FECHA DE CREACI&Oacute;N</th><th>FECHA DE RECEPCI&Oacute;N</th><th>FECHA DE ARCHIVO</th><th>MOTIVO DE<br>ARCHIVO</th><th>AVANCE</th></tr></thead><tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_creacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_recepcion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['motivo_fin']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['avance']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['estado'] == 5): ?>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead><tr bgcolor="#999999"><th>N&deg;</th><th>ESTADO</th><th>DOCUMENTO</th><th>N&deg; DOC</th><th>ASUNTO</th>
                        <th>OBSERVACIONES Y/O REFERENCIAS<br></th><th>FECHA DE DERIVACI&Oacute;N</th><th>EXPEDIENTE</th><th>RAZON<br>SOCIAL</th><th>ACCIONES</th><th>OFICINA DESTINO<br>/TRABAJADOR</th></tr></thead>
                        <tbody>
                        <?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['lista']) ? count($this->_tpl_vars['lista']) : max(0, (int)$this->_tpl_vars['lista']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                        <tr><td><?php echo $this->_sections['i']['iteration']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['estado']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['numero']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['asunto']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['observaciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['fecha_derivacion']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['documento_inicial']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['razon_social']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['acciones']; ?>
</td><td><?php echo $this->_tpl_vars['lista'][$this->_sections['i']['index']]['destino']; ?>
</td></tr>
                        <?php endfor; endif; ?></tbody>
                    </table>
                    <?php endif; ?>
<p align="right"></p>
</body>
</html>