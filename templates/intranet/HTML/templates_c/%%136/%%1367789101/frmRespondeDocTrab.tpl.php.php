<?php /* Smarty version 2.5.0, created on 2015-01-04 22:44:52
         compiled from oad/tramite/nuevaVersionFormularios/formsTrabajadores/frmRespondeDocTrab.tpl.php */ ?>
<?php echo $this->_tpl_vars['jscript']; ?>

<script language="JavaScript">
<!--
<?php echo '
function cargar(pForm,a)
{
	//var texto33=pForm.Observaciones.value;
	//var texto55=pForm.tipo2.value;	 
		//alert(texto33+" "+texto66);	 
		//alert(texto33);
		pForm.action="index.php?"+a;
	    pForm.submit();
}
'; ?>

-->
</script>
<br>
<div style="width:700px;">
<div class="std_form">
<form action="<?php echo $this->_tpl_vars['frmUrl']; ?>
" method="post" name="<?php echo $this->_tpl_vars['frmName']; ?>
" enctype="multipart/form-data" onSubmit="MM_validateForm('asunto','Asunto','R');return document.MM_returnValue">
		<?php if (isset($this->_sections['i'])) unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($this->_tpl_vars['ids']) ? count($this->_tpl_vars['ids']) : max(0, (int)$this->_tpl_vars['ids']);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?> 
			<input name="ids[]" type="hidden"  value="<?php echo $this->_tpl_vars['ids'][$this->_sections['i']['index']]; ?>
">
		<?php endfor; endif; ?>
  <table width="650" border="0" align="center" cellpadding="2" cellspacing="4" class="tabla-encuestas">
    <?php if ($this->_tpl_vars['errors']): ?> 
    <tr> 
      <td colspan="3" class="item"><?php echo $this->_tpl_vars['errors']; ?>
 </td>
    </tr>
    <?php endif; ?> 
    <tr> 
      <td class="texto td-encuesta label" align="left">Fecha</td>
      <td class="item" align="left"><?php echo $this->_tpl_vars['FechaActual']; ?>
&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['HoraActual']; ?>
 </td>
      <td colspan="3"><div align="center"><strong></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left">Clase de Documento</td>
      <td align="left"><select name="idClaseDoc" class="ipseln" id="idClaseDoc" onChange="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_RESPONDE_DOCTRAB']; ?>
');">
        <?php echo $this->_tpl_vars['selClaseDoc']; ?>

        </select></td>
      <td colspan="3" class="item"><div align="center"><strong><font color="#FF0000">(*)</font></strong></div></td>
    </tr>
	<?php if (( ( $this->_tpl_vars['justo'] == 'T' || $this->_tpl_vars['a'] ) && $this->_tpl_vars['nro_doc'] )): ?>
    <tr> 
      <td class="texto td-encuesta" align="left"> <strong>N&deg; Documento/Oficio</strong></td>
      <td align="left"> <input name="nro_doc" type="text" class="iptxtn" id="nro_doc" value="<?php echo $this->_tpl_vars['nro_doc']; ?>
" size="35" maxlength="255"> 
      </td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
	<?php endif; ?>
    <tr class="std_oculto"> 
      <td height="25" class="texto td-encuesta" align="left"><strong>Responder a</strong></td>
      <td align="left"><select name="trabajador" class="ipseln" id="trabajador">
        <?php echo $this->_tpl_vars['selTrabajador']; ?>

        </select></td>
      <td colspan="3"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left" valign="middle">Asunto </td>
      <td align="left"><textarea name="asunto" cols="50" rows="3" class="iptxtn" id="textarea" ><?php echo $this->_tpl_vars['asunto']; ?>
</textarea></td>
      <td colspan="3" valign="middle"><div align="center"><strong><font color="#FF0000" size="1">(*)</font></strong></div></td>
    </tr>
    <tr> 
      <td class="texto td-encuesta label" align="left" valign="middle"><?php if ($this->_tpl_vars['justo'] == 'T'): ?>Agregar observaciones<?php else: ?>Observaciones<?php endif; ?></td>
      <td class="item" align="left"><textarea name="observaciones" cols="50" rows="3" class="iptxtn" id="observaciones" ><?php if (! $this->_tpl_vars['observaciones']): ?>Sin Observaciones<?php else: ?><?php echo $this->_tpl_vars['observaciones']; ?>
<?php endif; ?></textarea></td>
      <td colspan="3"><strong></strong></td>
    </tr>
	<?php if ($this->_tpl_vars['email'] == 'jtumay'): ?>
    <tr> 
      <td class="texto td-encuesta" align="left"><strong>Archivo</strong></td>
      <td class="item" align="left"><input type="file" name="adjunto" class="ip-login contenido" size="35"></td>
      <td colspan="3"><strong></strong></td>
    </tr>	
	<?php endif; ?>
    <tr> 
      <td colspan="3" class="item"> <div align="right"><strong><font color="#FF0000">(*): 
          Obligatorio</font></strong></div></td>
    </tr>
	<!-- Tipo de doc=2||=4 => Se formula una resolución cuando se trata de Expediente y algunos casos de Doc Interno -->
	<?php if (! $this->_tpl_vars['justo'] && ! $this->_tpl_vars['a'] && ( $this->_tpl_vars['tipDoc'] == 2 || $this->_tpl_vars['tipDoc'] == 4 ) && $this->_tpl_vars['dire'] == 2): ?>
	<tr>
	<td colspan="3" class="item"><strong><a name="der" id="der"></a> 
				<input name="ProyResol" type="checkbox" id="ProyResol" value="1" onClick="submitForm('<?php echo $this->_tpl_vars['accion']['FRM_RESPONDE_DOCTRAB']; ?>
');" <?php if ($this->_tpl_vars['ProyResol']): ?> checked<?php endif; ?>>
        Formular Resoluci&oacute;n Ahora</strong></td>
			</tr>    <tr> 
      <td colspan="3"><hr width="100%" size="1"></td>
    </tr>
			<?php if ($this->_tpl_vars['ProyResol']): ?> 
				<tr> 
      				<td class="item"> <strong>Tipo de Resoluci&oacute;n:</strong></td>
      				<td colspan="2"><select name="TipoResol" class="ipsel1" id="TipoResol">
        			<?php echo $this->_tpl_vars['selTipoResol']; ?>

        			</select></td>
				</tr>
				<tr> 
      				<td class="item"> <strong>Sumilla:</strong></td>
      				<td colspan="2"><textarea name="sumilla" cols="54" class="iptxt1" id="textarea2" ><?php echo $this->_tpl_vars['sumilla']; ?>
</textarea> 
      </td>
				</tr>
		<tr> 
      	  <td class="item"> <strong>Fec. Inicio</strong></td>
		  <td colspan="3"><select name="dia_ini" class="ipsel2" id="dia_ini">
		  <?php echo $this->_tpl_vars['selDiaIni']; ?>

			</select> <select name="mes_ini" class="ipsel2" id="mes_ini">
		  <?php echo $this->_tpl_vars['selMesIni']; ?>

			</select> <select name="anyo_ini" class="ipsel2" id="anyo_ini">
		  <?php echo $this->_tpl_vars['selAnyoIni']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Inicio." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Fin</strong></td>
		  <td colspan="3"><select name="dia_fin" class="ipsel2" id="dia_fin">
		  <?php echo $this->_tpl_vars['selDiaFin']; ?>

			</select> <select name="mes_fin" class="ipsel2" id="mes_fin">
		  <?php echo $this->_tpl_vars['selMesFin']; ?>

			</select> <select name="anyo_fin" class="ipsel2" id="anyo_fin">
		  <?php echo $this->_tpl_vars['selAnyoFin']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Fin." width="20" height="20" align="top"> 
		  </td>
		</tr>
		<tr> 
      	  
      <td class="item"> <strong>Fec. Publicaci&oacute;n</strong></td>
		  <td colspan="3"><select name="dia_pub" class="ipsel2" id="dia_pub">
		  <?php echo $this->_tpl_vars['selDiaPub']; ?>

			</select> <select name="mes_pub" class="ipsel2" id="mes_pub">
		  <?php echo $this->_tpl_vars['selMesPub']; ?>

			</select> <select name="anyo_pub" class="ipsel2" id="anyo_pub">
		  <?php echo $this->_tpl_vars['selAnyoPub']; ?>

			</select> <img src="/img/800x600/ico-info3.gif" alt="Corresponde a la fecha de Publicación." width="20" height="20" align="top"> 
		  </td>
		</tr>
				<tr> 
      				<td colspan="3"><hr width="100%" size="1"></td>
    			</tr>
				<?php endif; ?> 
		<?php endif; ?>
    <tr align="center"> 
      <td colspan="3"><?php if (( $this->_tpl_vars['idClaseDoc'] > 0 )): ?><input name="bSubmit" type="Submit" class="std_button" value="Responder" ><?php endif; ?>
        &nbsp;&nbsp; <input name="cancel" type="button" class="std_button" value="Cancelar" onClick="MM_goToURL('parent','<?php echo $this->_tpl_vars['frmUrl']; ?>
?accion=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
&menu=<?php echo $this->_tpl_vars['accion']['SUMARIO_TRAB']; ?>
&subMenu=<?php echo $this->_tpl_vars['accion']['FRM_BUSCA_DOCTRAB']; ?>
');return document.MM_returnValue"> 
        <input name="accion" type="hidden" id="accion" value="<?php echo $this->_tpl_vars['accion']['RESPONDE_DOCTRAB']; ?>
"> 
        <input name="menu" type="hidden" id="menu" value="<?php echo $this->_tpl_vars['menuPager']; ?>
">
        <input name="subMenu" type="hidden" id="subMenu" value="<?php echo $this->_tpl_vars['subMenuPager']; ?>
">
		<input name="id" type="hidden" id="id" value="<?php echo $this->_tpl_vars['id']; ?>
"> 
		<input name="tipDoc" type="hidden" id="tipDoc" value="<?php echo $this->_tpl_vars['tipDoc']; ?>
">
		<input name="tipBus" type="hidden" id="tipBus" value="<?php echo $this->_tpl_vars['tipBus']; ?>
">
		<input name="a" type="hidden" id="a" value="<?php echo $this->_tpl_vars['a']; ?>
">
		<?php if ($this->_tpl_vars['justo'] == 'T'): ?><input name="justo" type="hidden" id="justo" value="<?php echo $this->_tpl_vars['justo']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['tipDocumento']): ?><input name="tipDocumento" type="hidden" id="tipDocumento" value="<?php echo $this->_tpl_vars['tipDocumento']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['tipBusqueda']): ?><input name="tipBusqueda" type="hidden" id="tipBusqueda" value="<?php echo $this->_tpl_vars['tipBusqueda']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecIniTrab2']): ?><input name="fecIniTrab2" type="hidden" id="fecIniTrab2" value="<?php echo $this->_tpl_vars['fecIniTrab2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['fecFinTrab2']): ?><input name="fecFinTrab2" type="hidden" id="fecFinTrab2" value="<?php echo $this->_tpl_vars['fecFinTrab2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['asunto2']): ?><input name="asunto2" type="hidden" id="asunto2" value="<?php echo $this->_tpl_vars['asunto2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['indicativo']): ?><input name="indicativo" type="hidden" id="indicativo" value="<?php echo $this->_tpl_vars['indicativo']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['observaciones2']): ?><input name="observaciones2" type="hidden" id="observaciones2" value="<?php echo $this->_tpl_vars['observaciones2']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['nroTD']): ?><input name="nroTD" type="hidden" id="nroTD" value="<?php echo $this->_tpl_vars['nroTD']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['page']): ?><input name="page" type="hidden" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['siglasDep'] && $this->_tpl_vars['siglasDep'] != 'none' )): ?><input name="siglasDep" type="hidden" id="siglasDep" value="<?php echo $this->_tpl_vars['siglasDep']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['tipodDoc'] && $this->_tpl_vars['tipodDoc'] != 'none' )): ?><input name="tipodDoc" type="hidden" id="tipodDoc" value="<?php echo $this->_tpl_vars['tipodDoc']; ?>
"><?php endif; ?>
		<?php if (( $this->_tpl_vars['anyo3'] && $this->_tpl_vars['anyo3'] != 'none' )): ?><input name="anyo3" type="hidden" id="anyo3" value="<?php echo $this->_tpl_vars['anyo3']; ?>
"><?php endif; ?>
		<?php if ($this->_tpl_vars['codigoInterno']): ?><input name="codigoInterno" type="hidden" id="codigoInterno" value="<?php echo $this->_tpl_vars['codigoInterno']; ?>
"><?php endif; ?>
		<input name="checkTodos" type="hidden" id="checkTodos" value="<?php echo $this->_tpl_vars['checkTodos']; ?>
">
		<input name="checkAsunto" type="hidden" id="checkAsunto" value="<?php echo $this->_tpl_vars['checkAsunto']; ?>
">
		<input name="checkRazon" type="hidden" id="checkRazon" value="<?php echo $this->_tpl_vars['checkRazon']; ?>
">
		<input name="checkTrab" type="hidden" id="checkTrab" value="<?php echo $this->_tpl_vars['checkTrab']; ?>
">
		<input name="FechaIni" type="hidden" id="FechaIni" value="<?php echo $this->_tpl_vars['FechaIni']; ?>
">
		<input name="FechaFin" type="hidden" id="FechaFin" value="<?php echo $this->_tpl_vars['FechaFin']; ?>
">		
		 </td>
    </tr>
  </table>
</form>
</div>
</div>