<?
// Inicializacion de Funciones JavaScript
$this->insertaFechaPapeletaScript($dias)
?>
  
<table width="540" cellspacing="2" cellpadding="2" bgcolor="#F7F7F7">
  <form name="<?=$this->frmName;?>" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
    <tr> 
      <td colspan="6" background="../../../img/800x600/papasis/bg_horiz.gif" class="bg-ar-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td rowspan="9" width="1" background="../../../img/800x600/papasis/bg_vert.gif" class="bg-iz-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
      <td colspan="4" align="right" class="texto">&nbsp;</td>
      <td rowspan="9" width="1" background="../../../img/800x600/papasis/bg_vert.gif" class="bg-de-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td align="right" class="texto"><strong>Tiene Autorizaci&oacute;n<br>
        para Ausentarse por</strong></td>
      <td colspan="3"> 
        <? $this->muestraSelectUniversal('cod_motivo', $sql_st_cod_modivo, $this->cod_motivo, false, true, "ipsel1"); ?>
      </td>
    </tr>
    <tr> 
      <td width="200" align="right" class="texto"><strong>Desde el dia</strong></td>
      <td width="100"> 
        <? $this->insertaFechaForm('dia_ini','mes_ini','anyo_ini',$this->frmName,$anyos[0],$anyos[count($anyos)-1],'NULL','NULL','NULL','ipsel1',true,false,false); ?>
      </td>
      <td width="20" align="center" class="texto"><strong>de</strong></td>
      <td width="220"> 
        <? $this->insertaHoraForm('hora_ini','min_ini','NULL','NULL',15,'ipsel1'); ?>
        <table width="50" height="22" align="left" cellpadding="0" cellspacing="0">
          <tr> 
            <td class="texto">&nbsp;<strong>Horas</strong></td>
          </tr>
        </table> </td>
    </tr>
    <tr> 
      <td align="right" class="texto"><strong>Al dia</strong></td>
      <td> 
        <? $this->insertaFechaForm('dia_fin','mes_fin','anyo_fin',$this->frmName,$anyos[0],$anyos[count($anyos)-1],'NULL','NULL','NULL','ipsel1',true,false,false); ?>
      </td>
      <td align="center" class="texto"><strong>de</strong></td>
      <td class="texto"> 
        <? $this->insertaHoraForm('hora_fin','min_fin','NULL','NULL',15,'ipsel1'); ?>
        <table width="50" height="22" align="left" cellpadding="0" cellspacing="0">
          <tr> 
            <td class="texto">&nbsp;<strong>Horas</strong></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td align="right" class="texto"><strong>Destino</strong></td>
      <td colspan="3"><input name="des_destino" type="text" id="des_destino" size="50" maxlength="250" class="iptxt1"></td>
    </tr>
    <tr> 
      <td align="right" class="texto"><strong>Asunto (Doc. de Referencia)</strong></td>
      <td colspan="3"><textarea name="des_asunto" cols="50" rows="3" id="des_asunto" class="iptxt1"></textarea></td>
    </tr>
    <tr> 
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr align="center"> 
      <td colspan="4"><input name="submit" type="submit" class="ipbot1" value="Enviar"  onClick="MM_validateForm('cod_motivo','Tipo de Autorización','Sel','dia_ini,mes_ini,anyo_ini','Fecha de Inicio','Date','dia_fin,mes_fin,anyo_fin','Fecha de Termino','Date','hora_ini,min_ini','Hora de Inicio','Hour','hora_fin,min_fin','Hora de Termino','Hour','dia_ini,mes_ini,anyo_ini,hora_ini,min_ini,dia_fin,mes_fin,anyo_fin,hora_fin,min_fin','La Fecha+Hora Inicio debe ser menor a la Fecha+Hora de Termino','datetimeMen','des_destino','Destino','R','des_asunto','Asunto','R');return document.MM_returnValue"> 
        &nbsp;&nbsp; <input type="reset" name="Submit" value="Limpiar Datos" class="ipbot1"> 
        <input name="accion" type="hidden" value="<?=$this->arr_accion[CREAR];?>"> 
        <input name="subAccion" type="hidden" value="<?=$this->arr_subAccion[CREAR_INSERTAR];?>"></td>
    </tr>
    <tr align="center"> 
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="6" background="../../../img/800x600/papasis/bg_horiz.gif" class="bg-ab-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
  </form>
</table>
