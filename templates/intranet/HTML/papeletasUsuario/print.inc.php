<html>
<head>
<title>Papeleta de Salida</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<?=$abs_path;?>/styles/intranet.css" type="text/css">
</head>
<body leftmargin="1" topmargin="1">  
<table width="540" border="0" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000" class="bdr-1px">
  <tr> 
      <td colspan="3" class="bg-ar-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td width="1" class="bg-iz-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
      <td 1width="538"><table width="100%" cellpadding="2" cellspacing="0" bordercolor="#000000" class="bdr-1px">
        <tr> 
            <td colspan="3" class="bg-ar-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
          </tr>
          <tr> 
            <td width="1" class="bg-iz-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
            <td bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" background="../../../img/800x600/bg_logoCONVENIO_SITRADOCFF_bw.gif" class="bg-med-nr">
              <tr> 
                <td colspan="2"><img src="../../../img/800x600/menb_rrhh.gif" width="211" height="37"></td>
              </tr>
              <tr> 
                <td width="30%">&nbsp;</td>
                <td width="70%" align="right" class="textoblack"><strong>Tarjeta 
                  N&deg; &nbsp;&nbsp;&nbsp;</strong></td>
              </tr>
              <tr> 
                <td colspan="2" align="center"><img src="../../../img/800x600/papasis/img_titpapaleta_bw.gif" width="441" height="19" vspace="4"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>El (la) servidor(a):</strong></td>
                <td class="textoblack">{$des_trabajador}</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>De la (Dependencia):</strong></td>
                <td class="textoblack">{$des_dependencia}</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>Tiene Autorizaci&oacute;n para 
                  ausentarse por:</strong></td>
                <td class="textoblack"><strong>{$des_motivo}</strong></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>Fecha de Permiso:</strong></td>
                <td class="textoblack"><strong>{$fecha}</strong></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>Destino:</strong></td>
                <td class="textoblack"> <strong>{$des_destino}</strong> </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td class="textoblack"><strong>Asunto:</strong></td>
                <td class="textoblack"> {$des_asunto} </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td align="right" class="textoblack"> 
                  Lima, {$fec_aprobacion}
                  &nbsp;&nbsp;&nbsp;&nbsp; </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table> </td>
            <td width="1" class="bg-de-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
          </tr>
          <tr> 
            <td colspan="3" class="bg-ab-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
          </tr>
        </table></td>
      <td width="1" class="bg-de-ry"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td colspan="3" class="bg-ab-rx"><img src="../../../img/800x600/papasis/bg_blank.gif" width="1" height="1"></td>
    </tr>
  </table>
  <script language="JavaScript" type="text/javascript">
<!--

var da = (document.all) ? 1 : 0;
var pr = (window.print) ? 1 : 0;
var mac = (navigator.userAgent.indexOf("Mac") != -1);

function printWin()
{
    if (pr) {
        // NS4+, IE5+
        window.print();
    } else if (!mac) {
        // IE3 and IE4 on PC
        VBprintWin();
    } else {
        // everything else
        handle_error();
    }
}

window.onerror = handle_error;
window.onafterprint = function() {window.close()}

function handle_error()
{
    window.alert('Su navegador no admite la opci�n de impresi�n. Presione Control/Opci�n + P para imprimir.');
    return true;
}

if (!pr && !mac) {
    if (da) {
        // This must be IE4 or greater
        wbvers = "8856F961-340A-11D0-A96B-00C04FD705A2";
    } else {
        // this must be IE3.x
        wbvers = "EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B";
    }

    document.write("<OBJECT ID=\"WB\" WIDTH=\"0\" HEIGHT=\"0\" CLASSID=\"CLSID:");
    document.write(wbvers + "\"> </OBJECT>");
}

// -->
</script>
  <script language="VBSCript" type="text/vbscript">
<!--

sub window_onunload
    on error resume next
    ' Just tidy up when we leave to be sure we aren't
    ' keeping instances of the browser control in memory
    set WB = nothing
end sub

sub VBprintWin
    OLECMDID_PRINT = 6
    on error resume next

    ' IE4 object has a different command structure
    if da then
        call WB.ExecWB(OLECMDID_PRINT, 1)
    else
        call WB.IOleCommandTarget.Exec(OLECMDID_PRINT, 1, "", "")
    end if
end sub

' -->
</script>
  <script language="JavaScript" type="text/javascript">
<!--
printWin();
// -->
</script>
</body>
</html>