<html>
<head>
<title>Observaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<?=$abs_path;?>/styles/intranet.css" type="text/css">
</head>
<body leftmargin="1" topmargin="1">
<table width="200" border="0" cellspacing="2" cellpadding="4">
  <tr>
    <td bordercolor="#ACACAC" bgcolor="#F7F7F7" class="texto bdr-1px"><strong><img src="../../../img/800x600/stat_false.gif" alt="[ Desaprobada ]" width="24" height="24" align="absmiddle">PAPELETA 
      DESAPROBADA</strong></td>
  </tr>
  <tr>
    <td bordercolor="#D6D6D6" class="textored bdr-1px"><strong>Observaci&oacute;n:<br>
        <span class="textogray"><?echo (empty($des_observacion)||is_null($des_observacion)) ? 'No hay Observacion alguna' : $des_observacion; ?></span> 
        </strong><br>
      <br>
      <strong>Fecha de Calificación:</strong><br>
	  <span class="textogray"><? setlocale (LC_TIME, "es_ES");
		 echo ucfirst(strftime("%A %d de %B del %Y", strtotime($fec_aprobacion))); ?></span>
</td>
  </tr>
</table>
</body>
</html>
