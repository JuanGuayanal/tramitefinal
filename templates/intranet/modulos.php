<?
include('mimemail/htmlMimeMail.php');
//****************************************************************/
/* Funcion para mostrar la Fecha Actual							 */
//****************************************************************/

function muestraFechaActual(){
	print (ucwords(strtolower(strftime ("%A %d %B del %Y"))));
}

function muestraTipoCambioDolar(){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select des_compra, des_venta ".
				"from dolar_cambio ".
				"WHERE ind_activo IS true ".
				"ORDER BY fec_cdolar DESC ".
				"LIMIT 1";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$tipoCambio= 'Compra: <span class="WEB_texto4"> S/. '.number_format($row[0],3). '</span> Venta: <span class="WEB_texto4">S/. '.number_format($row[1],3).'</span>';
			}
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close();
	}
	return $tipoCambio;
}

function muestraValorUIT(){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select des_valor ".
				"from uit ".
				"WHERE cod_veconomico='uit' and ind_activo IS true ";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$uit= number_format($row[0],0);
			}
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close();
	}
	return $uit;
}


/*****************************************************************/
/* Funcion para el envio de una Sugerencia                       */
/* Modulo Sugerencias  										     */
/*****************************************************************/
function enviaSugerencia($titulo, $mensaje){
	global $EMAIL_SUGERENCIA, $MAIL_DOMAIN;
	$email_from = $_SESSION['cod_usuario'] . "@$MAIL_DOMAIN";
	$from = "From: $email_from\r\nReply-to: $email_from\r\nBcc: intranet@midis.gob.pe";
	if(mail('',$titulo,$mensaje, $from, '-f' . $email_from))
		return 1;
	else
		return 0;	
}

/*****************************************************************/
/* Funcion para Mostrar la Frase de la Semanal                   */
/* Modulo Frase Semanal										     */
/*****************************************************************/
function muestraFrase(){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select des_frase ".
				"from frase ".
				"WHERE ind_publicacion IS true and ind_activo IS true ".
				"ORDER BY id_frase DESC ".
				"LIMIT 1";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
?>
  
<table width="232" border="0" cellspacing="0" cellpadding="0">
  <tr>
	  <td width="7" height="7"><img src="/img/800x600/cdr-frase-eari.gif" width="7" height="7"></td>
	  
    <td height="7" background="/img/800x600/cdr-frase-ar.gif" width="218"> 
      <div height="7"></div>
	  </td>
	  <td width="7" height="7"><img src="/img/800x600/cdr-frase-eard.gif" width="7" height="7"></td>
	</tr>
	<tr>
	  <td background="/img/800x600/cdr-frase-iz.gif">&nbsp;</td>	  
      <td class="td-frase texto" align="center"> 
<?
				echo '<i>'. $row[0] . '</i>';
?>
      </td>
	  <td background="/img/800x600/cdr-frase-de.gif">&nbsp;</td>
	</tr>
	<tr>
	  <td width="7" height="7"><img src="/img/800x600/cdr-frase-eabi.gif" width="7" height="7"></td>
	  <td height="7" background="/img/800x600/cdr-frase-ab.gif">
		<div height="7"></div>
	  </td>
	  <td height="7" width="7"><img src="/img/800x600/cdr-frase-eabd.gif" width="7" height="7"></td>
	</tr>
</table>
<?
			}
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close();
	}
}


/*****************************************************************/
/* Funciones para el envio de Mensajes Grupales                  */
/* Modulo Mensajes Grupales 								     */
/*****************************************************************/
function muestraGruposMail(){
	global $type_db, $userdb, $passdb, $db;

	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select id_grupo, des_grupo ".
				"from grupos_email ".
				"WHERE ind_activo IS true ".
				"ORDER BY des_grupo";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow()) {
				echo '<option value="' . $row[0] . '">' . $row[1] . '</option>';
			}
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close();
	}
}

function enviaGruposMail($id_grupo,$titulo,$mensaje){
	global $MAIL_DOMAIN, $type_db, $userdb, $passdb, $db;
	
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select eml_grupo ".
				"from grupos_email ".
				"WHERE ind_activo IS true and id_grupo=$id_grupo";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow())
				$email_grupo = str_replace("," ,"@$MAIL_DOMAIN,", $row[0]) . "@$MAIL_DOMAIN";
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close();
	}
	
	$email_from = $_SESSION['cod_usuario'] . "@$MAIL_DOMAIN";

	// Envia el correo de Texto plano con el Adjunto
	$mail = new htmlMimeMail();
	$mail->setSubject($titulo);
	$mail->setText($mensaje);
	if(is_uploaded_file($_FILES['file_adjunto']['tmp_name'])){
		$attachment = $mail->getFile($_FILES['file_adjunto']['tmp_name']);
		$mail->addAttachment($attachment, $_FILES['file_adjunto']['name'], $_FILES['file_adjunto']['type']);
	}
	$mail->setFrom($email_from);
	$mail->setBcc($email_grupo);
	$mail->setReturnPath($email_from);
	$result = $mail->send(array(''));	

	return $result;
}


/*********************************************************/
/* Detalle de Dependencia 								 */
/* Modulo Organigrama                              		 */
/*********************************************************/

function detalle_organi(){
	global $type_db, $serverdb, $userdb, $passdb, $db, $_GET;
	// Busqueda los datos de la Dependencia Seleccionada
	$conn = & ADONewConnection($type_db['mssql']);
	$conn->debug = false;
	if(!$conn->Connect($serverdb['mssql'], $userdb['mssql'][0], $passdb['mssql'][0], $db['mssql'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st="select dependencia as dep, piso as piso, siglas as sig, directo as drto, anexo as anex, central as ctrl, email as mail ".
				"from dbo.h_dependencia ".
				"where codigo_dependencia=".$_GET["cod"];
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			while ($row = $rs->FetchRow()) {
?> 
<table cellpadding="0" cellspacing="0" border="0" width="380" align="right">
  <tr>
    <td bgcolor="#FFFFFF"> 
      <table width="380" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td colspan="2" align="center" bgcolor="#3270AA"><b> 
            <?php if(trim($row[0])!=""){ echo $row[0]; }else{ echo "&nbsp;";}?></b>
          </td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap><b>PISO</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[1])!=""){ echo $row[1]; }else{ echo "&nbsp;";}?></b></td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap><b>SIGLAS</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[2])!=""){ echo $row[2]; }else{ echo "&nbsp;";}?></b></td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap><b>CENTRAL</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[5])!=""){ echo $row[5]; }else{ echo "&nbsp;";}?></b></td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap height="24"><b>ANEXO</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[4])!=""){ echo $row[4]; }else{ echo "&nbsp;";}?></b></td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap><b>TELF. 
            DIRECTO</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[3])!=""){ echo $row[3]; }else{ echo "&nbsp;";}?></b></td>
        </tr>
        <tr> 
          <td class="con-tabla" bgcolor="#BED6EB" width="100" nowrap><b>EMAIL</b></td>
          <td class="con-tabla2" bgcolor="#CCCCCC"><b><?php if(trim($row[6])!=""){ echo "<A href=\"mailto:".$row[6]."\" class=\"pres-noti\">".$row[6]."</A>"; }else{ echo "&nbsp;";}?></b></td>
        </tr>
      </table>
	</td>
	</tr>
</table>
<?
			}
			unset($row);
			$rs->Close(); # optional
		}
		$conn->Close(); 
	}
}

/*********************************************************/
/* Funciones del Modulo Encuestas                                                       */
/*********************************************************/

function pollMain($idpoll) {
	global $type_db, $userdb, $passdb, $db;
	if(!isset($idpoll))
	  $idpoll = 1;
	// Selecci�n de la Encuesta con el $idpoll indicado, asi como de sus respectivas opciones
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
?>
<form name="frm_encuesta" method="post" action="encuestas.php" onSubmit="return stat_encuesta(this);">
  <input type="hidden" name="idpoll" value="<?=$idpoll;?>">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_left" background="/img/800x600/bg_tab_encuesta.gif">
    <tr> 
	  <td><img src="/img/800x600/cab_tab_encuesta.gif" width="224" height="23"></td>
	</tr>
	<tr> 
	  <td class="bg_dercen" background="/img/800x600/img_bg_encuesta.gif" > 
        <table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
          <?
		$sql_st = "select tit_encuesta from encuesta_desc where id_encuesta=$idpoll";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$titpoll = $row[0];
?>
          <tr> 
            <td colspan="2" class="texto"><br>
              <b> 
              <?echo $titpoll;?>
              </b></td>
          </tr>
          <tr> 
            <td colspan="2" class="td-nada" height="8">&nbsp;</td>
          </tr>
          <?
			}
			unset($row);
			$rs->Close();
		}
		$sql_st = 	"select id_opcion, des_opcion, cnt_opcion ".
						"from encuesta_datos ".
						"where id_encuesta=$idpoll and ind_activo IS true ".
						"order by id_opcion ASC";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()) {
?>
          <tr class="tr-encuesta"> 
            <td width="10" class="td-encuesta"> 
              <input type="radio" name="idoption" value="<?echo $row[0];?>">
            </td>
            <td class="texto" width="220"> 
              <?echo $row[1];?>
            </td>
          </tr>
          <?
			}
			unset($row);
			$rs->Close();
		}
		$conn->Close();
?>
          <tr> 
            <td colspan="2" class="texto">&nbsp;</td>
          </tr>
          <tr align="center"> 
            <td colspan="2" class="texto"> 
              <input type="submit" name="votar" value="&nbsp;Votar&nbsp;" class="but-login">
            </td>
          </tr>
          <tr> 
            <td colspan="2" class="texto">&nbsp;</td>
          </tr>
        </table>
      </td></tr>
  </table>
</form>
<?
	}
}

function pollLatest(){
	global $type_db, $userdb, $passdb, $db;
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st = "SELECT id_encuesta ".
					 "from encuesta_desc ".
					 "WHERE ind_activo IS true ".
					 "ORDER BY id_encuesta DESC";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$idpoll = $row[0];
			}
			unset($row);
			$rs->Close();
		}
		$conn->Close();
	}
	return $idpoll;
}

function pollNewest(){
	$idpoll = pollLatest();
	pollMain($idpoll);
}

function pollCollector($idpoll, $idoption){
	global $type_db, $userdb, $passdb, $db;
	$stat_votacion=0;
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
		$sql_st = "SELECT count(*) ".
					 "from encuesta_integridad ".
					 "WHERE cod_usuario='" . $_SESSION['cod_usuario'] . "' and id_encuesta=$idpoll";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$stat_votacion = $row[0];
			}
			unset($row);
			$rs->Close();
		}
		if(!$stat_votacion){
			$upd_st = "UPDATE encuesta_datos SET cnt_opcion=cnt_opcion+1 WHERE (id_encuesta=$idpoll) AND (id_opcion=$idoption)";
			if ($conn->Execute($upd_st) === false) {
				print 'error actualizando: '.$conn->ErrorMsg().'<BR>';
			}
//			unset($upd_st);
			$ins_st = "insert into encuesta_integridad(cod_usuario, id_encuesta) values('" . $_SESSION['cod_usuario'] . "', $idpoll)";
			if ($conn->Execute($ins_st) === false) {
				print 'error actualizando: '.$conn->ErrorMsg().'<BR>';
			}
			unset($ins_st);
		}else{
			echo "<br><br><p align=\"center\" class=\"tit-documento\">Ud. ya ha realizado una votacion para esta encuesta.<br>S�lo se permite una por usuario.</p>";
		}
		$conn->Close();
	}
}

function pollResults($idpoll){
	global $type_db, $userdb, $passdb, $db;
	$BarScale = 150;
	if(!isset($idpoll)) $idpoll = 1;
	// Selecci�n de la Encuesta con el $idpoll indicado, asi como de sus respectivas opciones
	$conn = & ADONewConnection($type_db['postgres']);
	$conn->debug = false;
	if(!$conn->Connect("dbname=" . $db['postgres'][0] . " user=" . $userdb['postgres'][0] . " password=" . $passdb['postgres'][0]))
		print $conn->ErrorMsg();
	else{
?>
  <table width="531" border="0" cellspacing="0" cellpadding="0" class="tabla-encuestas">
    <tr>
      <td><img src="/img/800x600/cab-encuestas.gif" width="220" height="46" border="0" vspace="0" hspace="0"></td>
    </tr>
    <tr>
      <td align="center" valign="top">
        
      <table width="500" border="0" cellspacing="0" cellpadding="2">
        <?
		$sql_st = "SELECT tit_encuesta from encuesta_desc where id_encuesta=$idpoll";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$titpoll = $row[0];
?>
        <tr> 
            <td colspan="2" class="texto"><b> 
              <?echo $titpoll;?>
              </b></td>
        </tr>
          <tr> 
            <td colspan="2" class="td-nada td-encuesta" height="8">&nbsp;</td>
          </tr>
          <?
			}
			unset($row);
			$rs->Close();
		}
		$sql_st = "SELECT SUM(cnt_opcion) from encuesta_datos where id_encuesta=$idpoll";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			if ($row = $rs->FetchRow()) {
				$sum = $row[0];
			}
			unset($row);
			$rs->Close();
		}
		$sql_st = 	"select id_opcion, des_opcion, cnt_opcion ".
						"from encuesta_datos ".
						"where id_encuesta=$idpoll and ind_activo IS true ".
						"order by id_opcion ASC";
		$rs = & $conn->Execute($sql_st);
		unset($sql_st);
		if (!$rs){
			print $conn->ErrorMsg();
		}else{
			while($row = $rs->FetchRow()) {
				if($sum) {
					$percent = 100 * $row[2] / $sum;
				} else {
					$percent = 0;
				}
//				$percentInt = (int)$percent * 4 * $BarScale;
				$percentInt = (int)$percent * $BarScale / 100;
				$percent2 = (int)$percent;
				if ($percent > 0) {
					$barra = "<img src=\"img/800x600/leftbar.gif\" height=14 width=7>";
					$barra .= "<img src=\"img/800x600/mainbar.gif\" height=14 width=$percentInt Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/rightbar.gif\" height=14 width=7>";
				} else {
					$barra = "<img src=\"img/800x600/leftbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/mainbar.gif\" height=14 width=3 Alt=\"$percent2 %\">";
					$barra .= "<img src=\"img/800x600/rightbar.gif\" height=14 width=7 Alt=\"$percent2 %\">";
				}
?>
          <tr class="tr-encuesta"> 
            <td class="texto td-encuesta" width="210"> 
              <?echo $row[1];?>
            </td>
            <td class="texto td-encuesta" width="290"> 
              <?=$barra;?>
              &nbsp;&nbsp; 
              <?printf("%.2f %% (%d)", $percent, $row[2]);?>
            </td>
          </tr>
          <?
			}
			unset($row);
			$rs->Close();
		}
		$conn->Close();
?>
          <tr> 
            <td colspan="2" class="texto">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="texto">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<?
	}
}

?>

